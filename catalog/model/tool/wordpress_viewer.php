<?php
class ModelToolWordpressViewer extends Model {
    
    private $wp_link;
    
    static $wp_siteurl=FALSE;
    
    private function connect()
    {
        $this->wp_link=mysql_connect($this->config->get('wordpress_host'), $this->config->get('wordpress_user'), html_entity_decode($this->config->get('wordpress_pass')),TRUE);
        mysql_selectdb($this->config->get('wordpress_database'),$this->wp_link);
		mysql_query('SET NAMES utf8',$this->wp_link);
        return is_resource($this->wp_link);
    }
    
    public function is_connected()
    {
        if ((!$this->config->get('wordpress_user')) OR (!$this->config->get('wordpress_database'))  OR (!$this->config->get('wordpress_host')) OR (!$this->config->get('wordpress_pass')))
        {
            return FALSE;
        }
        if (!$this->connect())
        {
            return FALSE;
        }
        
        mysql_query('SHOW CREATE TABLE '.$this->config->get('wordpress_table_prefix').'posts',$this->wp_link);
        if (mysql_errno()!=0)
        {
            return FALSE;
        }
        
        return TRUE;
    }
    
    public function get_posts($params)
    {
        if (!$this->wp_siteurl)
        {
            $result=mysql_query('SELECT option_value FROM '.$this->config->get('wordpress_table_prefix').'options WHERE option_name="siteurl"',$this->wp_link);
            $this->wp_siteurl=mysql_fetch_assoc($result);
            $this->wp_siteurl=$this->wp_siteurl['option_value'];
        }
        
        $params['source']=explode('_',$params['source']);
        
        switch($params['source'][0])
        {
            case 'cat':
            {
                $result=mysql_query('SELECT post_content,post_title,post_name,ID
                                     FROM '.$this->config->get('wordpress_table_prefix').'term_relationships
                                     LEFT JOIN '.$this->config->get('wordpress_table_prefix').'posts ON '.$this->config->get('wordpress_table_prefix').'posts.ID = '.$this->config->get('wordpress_table_prefix').'term_relationships.object_id AND post_type="post" AND post_status="publish"
                                     WHERE term_taxonomy_id='.$params['source'][1].'
                                     ORDER BY post_date DESC
                                     LIMIT '.$params['articles_amount'],$this->wp_link);
                break;
            }
            case 'tag':
            {
                $result=mysql_query('SELECT post_content,post_title,post_name,ID
                                     FROM '.$this->config->get('wordpress_table_prefix').'term_relationships
                                     LEFT JOIN '.$this->config->get('wordpress_table_prefix').'posts ON '.$this->config->get('wordpress_table_prefix').'posts.ID = '.$this->config->get('wordpress_table_prefix').'term_relationships.object_id AND post_type="post" AND  post_status="publish"
                                     WHERE term_taxonomy_id='.$params['source'][1].'
                                     ORDER BY post_date DESC
                                     LIMIT '.$params['articles_amount'],$this->wp_link);
                break;
            }
            case 'recent':
            {
                $result=mysql_query('SELECT post_content,post_title,post_name,ID
                                     FROM '.$this->config->get('wordpress_table_prefix').'posts
                                     WHERE post_type="post" AND post_status="publish"
                                     ORDER BY post_date DESC
                                     LIMIT '.$params['articles_amount'],$this->wp_link);
                break;
            }
        }
        $posts=array();
        while($post=mysql_fetch_assoc($result))
        {
            $posts[]=$post;
        }
        
        return array('content'=>$posts,'site_url'=>$this->wp_siteurl);
    }
}
?>