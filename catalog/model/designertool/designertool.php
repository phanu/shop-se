<?php
class ModelDesignertoolDesignertool extends Model {
	
	public function getProduct($rawproduct_id=0) {			
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "rawproduct AS rp INNER JOIN rawproduct_description AS rpd ON rp.rawproduct_id = rpd.rawproduct_id  WHERE rp.rawproduct_id = '" . (int)$rawproduct_id . "' AND rpd.language_id='".$this->config->get('config_language_id')."'");		
		return $query->row;		
	}
        
        public function getColor(){  
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "color AS c INNER JOIN colordesc AS cd ON c.id = cd.colorId  WHERE c.status = '1' AND cd.language_id='".$this->config->get('config_language_id')."' ORDER BY c.sort_order ASC");		
            return $query->rows;
        }
        
        public function getArtwork($artworkId){ 
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "artwork AS a INNER JOIN artwork_description AS ad ON a.artwork_id = ad.artwork_id  WHERE a.status = '1' AND ad.language_id='".$this->config->get('config_language_id')."' AND a.artwork_category_id='".$artworkId."'");            
            return $query->rows;
        }
        
        public function getArtworkCategories(){  
            $query = $this->db->query("SELECT ac.artwork_category_id, acd.name FROM " . DB_PREFIX . "artwork_category AS ac INNER JOIN artwork_category_description AS acd ON ac.artwork_category_id = acd.artwork_category_id  WHERE ac.status = '1' AND acd.language_id='".$this->config->get('config_language_id')."'");		
            return $query->rows;
        } 
        
        public function getFonts(){ 
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "font WHERE status = '1' AND language_id = '" . (int)$this->config->get('config_language_id') . "' ");		
            return $query->rows;
        }
        
         public function getFontdesc($fontId){
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "fontdesc WHERE font_id='".(int)$fontId."'  ");		
            return $query->rows;
        }
        
        public function getDefaultArtworkCategoryId(){ 
            $query = $this->db->query("SELECT artwork_category_id FROM ".DB_PREFIX. "artwork_category WHERE isDefault='1'");
            return $query->row['artwork_category_id'];
        }
        
        public function addDesign($data){
            $model = uniqid('Custom-');
            $sku = uniqid('SKU-');
            $productName = $data['productName'].'_'.$model;
            $imagearray = $data['encodedArr'];
            //echo "<pre>"; print_r($data); exit;
            mysql_set_charset('utf8');
            $this->db->query("INSERT INTO " . DB_PREFIX . "product SET model = '" . $this->db->escape($model) . "', sku = '" . $this->db->escape($sku) . "', quantity = '9999', minimum = '1',  price = '" . (float)$data['price'] . "', weight = '0.00', status = '1', date_added = NOW(), is_customize=1");
            $product_id = $this->db->getLastId();
            $productimage = $this->createImage($imagearray);
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($productimage) . "'");           
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($productimage) . "' WHERE product_id = '" . (int)$product_id . "'");            
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "'");            
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$data['laguageId'] . "', name = '" . $this->db->escape($productName) . "', description='' ");
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '255'");
            $this->db->query("INSERT INTO " . DB_PREFIX . "design_product_data SET product_id = '" . (int)$product_id . "', rawproduct_id = '" . (int)$data['productId'] . "', language_id = '" . (int)$data['laguageId'] . "', fabric_array = '" . $data['fabricData'] . "', pdf_array = '" . serialize($data['tempArr']) . "', info_array = '" . json_encode($data['infoArray']) . "', price='".$data['price']."' ");
            
            $returnproduct = array();
            $returnproduct['product_id'] = $product_id;
            $returnproduct['image'] = HTTP_IMAGE.$productimage;
            $returnproduct['name'] = $productName;
            return $returnproduct;
            return $product_id;            
        }
        
        
        protected function createImage($imagearray){
            $byteString = substr($imagearray, 22);
            $imageName = $this->getImageName('designProductImage.png');
            $img = 'data/product/' . $imageName;
            $string = base64_decode($byteString);
            $target = DIR_TOOLSETUP_PRODUCT  . $imageName;
                       
            $img_file = fopen($target, "w");
            fwrite($img_file, $string);
            @chmod($target, 0777);
            fclose($img_file);
            
            $productImage = DIR_IMAGE . 'data/product/' . $imageName;            
            exec(IMAGE_MAGICK_CODE . ' ' . $target . ' -resize 550x550! ' . $productImage . '');
            
            unlink($target);
            return $img;
            
        }


        protected function getImageName($imageName) {
            if ($imageName != '') {
               $randNo = date("mdis");
               $randVal = uniqid('productimage_') . $randNo;
               $extExp = explode(".", $imageName);
               $countExp = count($extExp);
               $extName = $extExp[$countExp - 1];
               $imageName = $randVal . "." . $extName;
            }
            return $imageName;
         }
         
         public function getDesignById($designId){
             $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "design_product_data WHERE product_id = '".(int)$designId."' ");
             return $query->row;
         }

		
}
?>
