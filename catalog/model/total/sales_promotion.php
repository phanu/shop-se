<?php
class ModelTotalSalesPromotion extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		
			$this->load->language('total/sales_promotion');
			$this->load->model('checkout/sales_promotion');
			 
			$sales_promotion_info = $this->model_checkout_sales_promotion->getSalesPromotion();
										
			if ($sales_promotion_info) {
	
				foreach ($sales_promotion_info as $result) { 
				
				$discount_total = 0;
				$sp_discount = $result['discount'];
				$sp_quantity_total = $result['quantity_total'];
				$sp_quantity_sale = $result['quantity_sale'];
				$sp_quantity_buy = $result['quantity_buy'];
				$sp_base = $result['sp_base'];
				$sp_base_total = $result['sp_base_total'];
				$sp_base_quantity = $result['sp_base_quantity'];
				$sp_product = $result['sp_product'];
				$sp_product_total = $result['sp_product_total'];
				$sp_product_quantity = $result['sp_product_quantity'];
				$sp_product_buy = $result['sp_product_buy'];
				$sp_product_buy_total = $result['sp_product_buy_total'];
				$sp_product_buy_quantity = $result['sp_product_buy_quantity'];
				$sp_shipping = $result['shipping'];
				$sp_quantity_type = $result['quantity_type'];
				
				// Cart Products
				$sp_cart_product = $sp_base;
				$sp_cart_total = $sp_base_total;
				$sp_cart_quantity = $sp_base_quantity;		
											
				if($sp_quantity_buy && $sp_quantity_sale) {
				
					if($sp_product && $sp_product_buy) {
						if($sp_product == $sp_product_buy) {
							$discount_factor = ((floor($sp_product_quantity/($sp_quantity_sale+$sp_quantity_buy)))*$sp_quantity_sale);
						}
						else {
						$discount_factor = min(floor($sp_product_buy_quantity/$sp_quantity_buy), floor($sp_product_quantity/$sp_quantity_sale))*$sp_quantity_sale;
						}
					}
					
					else if($sp_product_buy){
						if($sp_product_buy == $sp_cart_product) {
						$discount_factor = min(floor($sp_product_buy_quantity/$sp_quantity_buy), floor(($sp_cart_quantity)/$sp_quantity_sale))*$sp_quantity_sale;
						}
						else {
						$discount_factor = min(floor($sp_product_buy_quantity/$sp_quantity_buy), floor(($sp_cart_quantity-$sp_product_buy_quantity)/$sp_quantity_sale))*$sp_quantity_sale;
						}
					}
					
					else if($sp_product) {
						$discount_factor = min(floor($sp_product_quantity/$sp_quantity_sale), floor(($sp_cart_quantity)/($sp_quantity_buy+$sp_quantity_sale)))*$sp_quantity_sale;	
					}
					
					else {
						$discount_factor = ((floor($sp_cart_quantity/($sp_quantity_buy+$sp_quantity_sale)))*$sp_quantity_sale);
					}
					
					// Discount Multiplier 
					if($sp_quantity_type == 'F') {
						$discount_factor = $sp_quantity_sale;
					}
				}
				
				else if($sp_product && (!$sp_quantity_sale)) {
					$discount_factor = $sp_product_quantity;
					}
				
				else if($sp_quantity_sale) {
					$discount_factor = $sp_quantity_sale;
					}
				
				else {
					$discount_factor = $sp_cart_quantity;
					}

				// Condition for Products On Sale
				if($sp_product) {
					$sp_discount_product = $sp_product;
					}
				else {
					$sp_discount_product = $sp_base;
					}
				
				if ($result['type'] == 'F') {
					if($sp_quantity_buy || $sp_quantity_sale) {
						$discount_total = $sp_discount*$discount_factor;
					}
					else {
						$discount_total = $sp_discount;
					}
					//$sub_total = $this->cart->getSubTotal();
					$sub_total = $sp_base_total;
					$tax_discount_rate = $sp_discount/$sub_total;
					
					foreach ($sp_discount_product['product_id'] as $key => $val) {
						$tax_rates = $this->tax->getRates($sp_discount_product['total'][$key], $sp_discount_product['tax_class_id'][$key]);
							foreach ($tax_rates as $tax_rate) {
								if ($tax_rate['type'] == 'P') {
									$taxes[$tax_rate['tax_rate_id']] -= ($tax_rate['amount']*$tax_discount_rate);
									}
								}
							}
						}
				else {
					if($sp_quantity_sale || $sp_product || $sp_quantity_buy || $sp_product_buy) {
						asort($sp_discount_product['price']);
						foreach ($sp_discount_product['price'] as $key => $val) {
							if($discount_factor>0) {
							if($discount_factor >= $sp_discount_product['quantity'][$key]) {
								$discount_total += (($sp_discount_product['total'][$key]*($sp_discount))/100);
								if ($sp_discount_product['tax_class_id'][$key]) {
								$tax_rates = $this->tax->getRates($sp_discount_product['total'][$key] - ($sp_discount_product['total'][$key] - $discount_total), $sp_discount_product['tax_class_id'][$key]);
							
							foreach ($tax_rates as $tax_rate) {
								
								if ($tax_rate['type'] == 'P') {
									$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
									}
								if (($tax_rate['type'] == 'F') && ($sp_discount == '100.0000')) {
									$taxes[$tax_rate['tax_rate_id']] -= ($tax_rate['amount']*$discount_factor);
									}
								}
								}
								$discount_factor -= $sp_discount_product['quantity'][$key];
								}	
							else{
							$discount_total += (((($sp_discount_product['price'][$key])*($discount_factor))*($sp_discount))/100);
							if ($sp_discount_product['tax_class_id'][$key]) {
								$tax_rates = $this->tax->getRates($sp_discount_product['total'][$key] - ($sp_discount_product['total'][$key] - $discount_total), $sp_discount_product['tax_class_id'][$key]);
							
							foreach ($tax_rates as $tax_rate) {
								
								if ($tax_rate['type'] == 'P') {
									$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
									}
								if (($tax_rate['type'] == 'F') && ($sp_discount == '100.0000')) {
									$taxes[$tax_rate['tax_rate_id']] -= ($tax_rate['amount']*$discount_factor);
									}
								}
								}
							$discount_factor -= $sp_discount_product['quantity'][$key];
									}
								}
							
							}
						}
					else {
						$sub_total = $sp_cart_total;
						$discount_total = ((($sub_total)*($sp_discount))/100);
						foreach ($sp_discount_product['price'] as $key => $val) {
						$tax_rates = $this->tax->getRates($sp_discount_product['total'][$key], $sp_discount_product['tax_class_id'][$key]);
							foreach ($tax_rates as $tax_rate) {
								if ($tax_rate['type'] == 'P') {
									$taxes[$tax_rate['tax_rate_id']] -= (($tax_rate['amount']*$sp_discount)/100);
									}
								}
							}
						}
					}
				
				if ($result['shipping'] && isset($this->session->data['shipping_method'])) {
					if (!empty($this->session->data['shipping_method']['tax_class_id'])) {
						$tax_rates = $this->tax->getRates($this->session->data['shipping_method']['cost'], $this->session->data['shipping_method']['tax_class_id']);
						foreach ($tax_rates as $tax_rate) {
							if ($tax_rate['type'] == 'P') {
								$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
									}
							}
						}
					$discount_total += $this->session->data['shipping_method']['cost'];	
					}
	
				$total_promotion_sales_data[] = array(
					'code'       => 'sales_promotion',
        			'title'      => sprintf($this->language->get('text_sales_promotion'), $result['name']),
	    			'text'       => $this->currency->format(-$discount_total),
        			'value'      => -$discount_total,
					'sort_order' => $this->config->get('sales_promotion_sort_order')
      			);
				
				}
	
			foreach ($total_promotion_sales_data as $key => $row) {
				$value[$key]  = $row['value'];
				}
						
				if($this->config->get('sales_promotion_multiple') == 1) {
					// Multiple Promotions
					foreach ($total_promotion_sales_data as $key => $row) {
						
						$row_value = $row['value'];
						$row_text = $row['text'];
						if($total <= 0) {
							$row_value = 0;
							$row_text = $this->currency->format($row_value);
							}
								
						$total_data[] = array(
						'code'       => $row['code'],
						'title'      => $row['title'],
						'text'       => $row_text,
						'value'      => $row_value,
						'sort_order' => $row['sort_order']
						);
						
						$total += $row_value;
						}
					}
				else {
					// Single Promotion
					// Sort for most benefited promotion
					array_multisort($value, SORT_ASC, $total_promotion_sales_data);		
				
					$total_data[] = array(
						'code'       => $total_promotion_sales_data[0]['code'],
						'title'      => $total_promotion_sales_data[0]['title'],
						'text'       => $total_promotion_sales_data[0]['text'],
						'value'      => $total_promotion_sales_data[0]['value'],
						'sort_order' => $total_promotion_sales_data[0]['sort_order']
					);
	
					$total += $total_promotion_sales_data[0]['value'];		
					}
				}
		}
		
		
		public function confirm($order_info, $order_total) {
		$code = '';
		
		$start = strpos($order_total['title'], '(') + 1;
		$end = strrpos($order_total['title'], ')');
		
		if ($start && $end) {  
			$code = substr($order_total['title'], $start, $end - $start);
		}	
		
		$this->load->model('checkout/sales_promotion');
		
		$sales_promotion_id = $this->model_checkout_sales_promotion->getSalesPromotionId($code);
		
		if ($sales_promotion_id) {
			$this->model_checkout_sales_promotion->redeem($sales_promotion_id, $order_info['order_id'], $order_info['customer_id'], $order_total['value']);	
		    }						
	    }
		
}
?>
