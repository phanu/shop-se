<?php
class ModelTotalOneCheckOutPaymentFee extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		
		if(substr(VERSION,0,5) == '1.5.0'){
			$payment_fee_setting = unserialize($this->config->get('onecheckout_payment_fee'));
		}else{
			$payment_fee_setting = $this->config->get('onecheckout_payment_fee');
		}
		
		if ($this->config->get('onecheckout_status') && isset($this->session->data['payment_method']['code']) && isset($payment_fee_setting[$this->session->data['payment_method']['code']]) && $payment_fee_setting[$this->session->data['payment_method']['code']]['status'] && $this->cart->hasProducts()) {
						
					if (isset($this->session->data['guest']['payment']['country_id']) && $this->session->data['guest']['payment']['country_id']) {
					
						$country_id = $this->session->data['guest']['payment']['country_id'];
						$zone_id = $this->session->data['guest']['payment']['zone_id'];
					
					} elseif (isset($this->session->data['payment_address_id']) && $this->session->data['payment_address_id']) {
				
						$address_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$this->session->data['payment_address_id'] . "'");
						$country_id = $address_query->row['country_id'];
						$zone_id = $address_query->row['zone_id'];
					 
					} else {
				
						$country_id = 0;
						$zone_id = 0;
				
					}
							
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$payment_fee_setting[$this->session->data['payment_method']['code']]['geozoneid'] . "' AND country_id = '" . (int)$country_id . "' AND (zone_id = '" . (int)$zone_id . "' OR zone_id = '0')");
					
					if ($total > $payment_fee_setting[$this->session->data['payment_method']['code']]['total']) {
						$status = false;
					} elseif (!$payment_fee_setting[$this->session->data['payment_method']['code']]['geozoneid']) {
						$status = true;
					} elseif ($query->num_rows) {
						$status = true;
					} else {
						$status = false;
					}
					
					if (($status) && ($this->cart->getSubTotal() > 0)) {
						
						$this->load->language('onecheckout/checkout');
						
						//$paymentfee = (($total + $this->config->get('onecheckout_payment_fee_fixed_fee')) / (1 - ($this->config->get('onecheckout_payment_fee_percentage')/100))) - $total;
						$paymentfee = ((float)$payment_fee_setting[$this->session->data['payment_method']['code']]['percentage']*$total)/100 + (float)$payment_fee_setting[$this->session->data['payment_method']['code']]['fixedfee'];
						
						$total_data[] = array( 
							'code'       => 'onecheckout_payment_fee',
							'title'      => $this->language->get('text_onecheckout_payment_fee'),
							'text'       => $this->currency->format($paymentfee),
							'value'      => $paymentfee,
							'sort_order' => $payment_fee_setting[$this->session->data['payment_method']['code']]['sortorder']
						);

						if ($payment_fee_setting[$this->session->data['payment_method']['code']]['taxclassid']) {
							$this->load->model('onecheckout/checkout');	
							$version_int = $this->model_onecheckout_checkout->versiontoint();
							if($version_int >= 1513){
								if (!isset($taxes[$payment_fee_setting[$this->session->data['payment_method']['code']]['taxclassid']])) {
								$tax_rates = $this->tax->getRates($paymentfee, $payment_fee_setting[$this->session->data['payment_method']['code']]['taxclassid']);
										
								foreach ($tax_rates as $tax_rate) {
									if (!isset($taxes[$tax_rate['tax_rate_id']])) {
										$taxes[$tax_rate['tax_rate_id']] = $tax_rate['amount'];
									} else {
										$taxes[$tax_rate['tax_rate_id']] += $tax_rate['amount'];
									}
								}
								}
							} else {
								if (!isset($taxes[$payment_fee_setting[$this->session->data['payment_method']['code']]['taxclassid']])) {
								$taxes[$payment_fee_setting[$this->session->data['payment_method']['code']]['taxclassid']] = $paymentfee / 100 * $this->tax->getRate($payment_fee_setting[$this->session->data['payment_method']['code']]['taxclassid']);
								} else {
								$taxes[$payment_fee_setting[$this->session->data['payment_method']['code']]['taxclassid']] += $paymentfee / 100 * $this->tax->getRate($payment_fee_setting[$this->session->data['payment_method']['code']]['taxclassid']);
								}
							}
							
						}

						$total += $paymentfee;
					}				
			
		}
	}
}
?>