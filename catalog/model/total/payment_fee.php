<?php
//==============================================================================
// Multiple Payment Fee v4.00
// 
// Author: Cofran
// E-mail: franco.iglesias@gmail.com
// Website: http://www.wachipato.com/
// Support: http://www.wachipato.com/support
//==============================================================================

class ModelTotalPaymentFee extends Model {
	private $type = 'total';
	private $name = 'payment_fee';

	public function getTotal(&$total_data, &$total, &$taxes)
	{
		$oc_version = (!defined('VERSION')) ? 140 : (int)substr(str_replace('.', '', VERSION), 0, 3);
		$keycode = ($oc_version < 151) ? 'id' : 'code';

		if (isset($this->session->data['payment_address_id'])) {
			$this->load->model('account/address');
			$address = $this->model_account_address->getAddress($this->session->data['payment_address_id']);
		} elseif (isset($this->session->data['guest']['payment'])) {
			$address = $this->session->data['guest']['payment'];
		}

		$status = true;

		if ( !$this->config->get($this->name . '_status') || !isset($address) )
		{
			$status = false;
		}

		if (isset($address))
		{
			$current_geozones = array();
			$geozones = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '0' OR zone_id = '" . (int)$address['zone_id'] . "')");
			foreach ($geozones->rows as $geozone) {
				$current_geozones[] = $geozone['geo_zone_id'];
			}

			$results = unserialize($this->config->get($this->name));

			$payment_fee = array();

			foreach($results as $key => $result)
			{
				if ( (!empty($this->session->data['payment_method']) && $this->session->data['payment_method'][$keycode] == $result['payment_id']) && (!empty($current_geozones) && in_array($result['geo_zone_id'], $current_geozones) || ($result['geo_zone_id'] == 0)) && ($result['min'] && ($this->cart->getSubTotal() >= (float)$result['min'])) && ($result['max'] && ($this->cart->getSubTotal() <= (float)$result['max'])) )
				{
					$status = true;

					if ( $result['geo_zone_id'] != 0 )
					{
						$payment_fee = $result; break;
					}
					else
					{
						$payment_fee = $result;
					}
				}
			}

			if ($status && !empty($payment_fee))
			{
				$language_id = $this->db->query("SELECT language_id FROM " . DB_PREFIX . "language WHERE code = '" . $this->session->data['language'] . "'");

				if ( empty($payment_fee['fee_p']) )
				{
					$total_fee = $total + $payment_fee['fee_f'];
				}
				elseif ( !empty($payment_fee['fee_p']) && empty($payment_fee['fee_f']) && $payment_fee['pct'] == 'complex' )
				{
					$total_fee = - ((($payment_fee['fee_p'] / 100) * $total) / (($payment_fee['fee_p'] / 100) - 1));
				}
				elseif ( empty($payment_fee['fee_p']) && !empty($payment_fee['fee_f']) && $payment_fee['pct'] == 'complex' )
				{
					$total_fee = - (($payment_fee['fee_f'] / 100 * $total) / ($payment_fee['fee_f'] - 1));
				}
				elseif ( !empty($payment_fee['fee_p']) && !empty($payment_fee['fee_f']) && $payment_fee['pct'] == 'complex' )
				{
					$total_fee = - ($payment_fee['fee_f'] / (($payment_fee['fee_p'] / 100) - 1)) - ((($payment_fee['fee_p'] / 100) * $total) / (($payment_fee['fee_p'] / 100) - 1));
				}
				elseif ( !empty($payment_fee['fee_p']) && !empty($payment_fee['fee_f']) && $payment_fee['pct'] == 'basic' )
				{
					$total_fee = (($payment_fee['fee_p'] * $total) / 100) + $payment_fee['fee_f'];
				}
				elseif ( !empty($payment_fee['fee_p']) && empty($payment_fee['fee_f']) && $payment_fee['pct'] == 'basic' )
				{
					$total_fee = (($payment_fee['fee_p'] * $total) / 100);
				}

				$total_data[] = array(
					'id'			=> $this->name,
					'code'			=> $this->name,
					'title'			=> $payment_fee['description'][$language_id->row['language_id']]['name'],
					'text'			=> ($payment_fee['type_rule'] != 'fee' ? '-' : '') . $this->currency->format($total_fee),
					'value'			=> $total_fee,
					'sort_order'	=> $this->config->get($this->name . '_sort_order')
				);

				$tax_rates = $this->tax->getRates($total_fee, $payment_fee['tax_class_id']);
				
				foreach ($tax_rates as $tax_rate) {
					if (!isset($taxes[$tax_rate['tax_rate_id']])) {
						$taxes[$tax_rate['tax_rate_id']] = $tax_rate['amount'];
					} elseif ( $payment_fee['type_rule'] == 'fee' ) {
						$taxes[$tax_rate['tax_rate_id']] += $tax_rate['amount'];
					} else {
						$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];					
					}
				}

				if ( $payment_fee['type_rule'] == 'fee' )
				{
					$total += $total_fee;
				}
				else
				{
					$total -= $total_fee;
				}
			}
		}
	}
}
?>