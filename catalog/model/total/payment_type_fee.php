<?php
class ModelTotalPaymentTypeFee extends Model {

	private $name = '';

	public function getTotal(&$total_data, &$total, &$taxes) {

		$this->name = basename(__FILE__, '.php');
		if ($this->config->get($this->name . '_status') && $this->cart->getSubTotal() && ($this->cart->getSubTotal() < $this->config->get($this->name . '_total'))) {

		 	// Get Address Data (Model)
		    $address = array();
			if (isset($this->session->data['payment_address_id']) && $this->session->data['payment_address_id']) { // Normal checkout
				$this->load->model('account/address');
				$address = $this->model_account_address->getAddress($this->session->data['payment_address_id']);
			} else { // Guest checkout
				$address = (isset($this->session->data['guest'])) ? $this->session->data['guest'] : array();
			}

			$country_id	= (isset($address['country_id'])) ? $address['country_id'] : 0;
			$zone_id 	= (isset($address['zone_id'])) ? $address['zone_id'] : 0;
			//

			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get($this->name . '_geo_zone_id') . "' AND country_id = '" . (int)$country_id . "' AND (zone_id = '" . (int)$zone_id . "' OR zone_id = '0')");
			if (!$this->config->get($this->name . '_geo_zone_id')) {
        		$status = TRUE;
      		} elseif ($query->num_rows) {
      		  	$status = TRUE;
      		} else {
     	  		$status = FALSE;
			}

			if (!isset($this->session->data['payment_method'])) {
				$status = FALSE;
			}

		 	if (!$status) { return; }
		 	//

		 	if (!$total) { $total = $this->cart->getSubTotal(); }

		 	$this->load->language('total/' . $this->name);
			$this->load->model('localisation/currency');


			// Get list of allowed services
			$query = $this->db->query("select `value` from " . DB_PREFIX . "setting where `group` = '" . $this->name . "' AND `key` = '" . $this->name . "_types'");
			$typeArr = explode(',', $query->row['value']);
			if (!isset($this->session->data['payment_method']['id'])) { $this->session->data['payment_method']['id'] = $this->session->data['payment_method']['code']; } //v14x backwards compatible
			if (!in_array($this->session->data['payment_method']['id'], $typeArr)) {
				return;
			}

			if (strpos($this->config->get($this->name . '_amount'),'%') !== false) {
				$fee = (trim($this->config->get($this->name . '_amount'),'%'))/100;
				$value = $total * $fee;
			} else {
				$value = $this->config->get($this->name . '_amount');
			}

			$total_data[] = array(
				'code'		 => $this->name, //v15x
        		'title'      => $this->language->get('text_payment_type'),
        		'text'       => $this->currency->format($value),
        		'value'      => $value,
				'sort_order' => $this->config->get($this->name . '_sort_order')
			);

			if ($this->config->get($this->name . '_tax_class_id')) {
				if (method_exists($this->document, 'addBreadcrumb')) { // v14x
					if (!isset($taxes[$this->config->get($this->name . '_tax_class_id')])) {
						$taxes[$this->config->get($this->name . '_tax_class_id')] = $value / 100 * $this->tax->getRate($this->config->get($this->name . '_tax_class_id'));
					} else {
						$taxes[$this->config->get($this->name . '_tax_class_id')] += $value / 100 * $this->tax->getRate($this->config->get($this->name . '_tax_class_id'));
					}
				} else { // v15x
					$tax_rates = $this->tax->getRates($value, $this->config->get($this->name . '_tax_class_id'));

					foreach ($tax_rates as $tax_rate) {
						if (!isset($taxes[$tax_rate['tax_rate_id']])) {
							$taxes[$tax_rate['tax_rate_id']] = $tax_rate['amount'];
						} else {
							$taxes[$tax_rate['tax_rate_id']] += $tax_rate['amount'];
						}
					}
				}
			}

			$total += $value;
		}
	}
}
?>