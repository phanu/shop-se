<?php
class ModelModuleSalesMotivator extends Model {
	
	public function getTotalCustomersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
		
		return $query->row['total'];
	}
	
	public function addCustomer($data) {
		
		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}
		
		$sql = "INSERT INTO " . DB_PREFIX . "customer SET store_id = '" . (int)$this->config->get('config_store_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', password = '" . $this->db->escape(md5($data['password'])) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', customer_group_id = '" . (int)$customer_group_id . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '1', date_added = NOW()";
		
      	$this->db->query($sql);
      	
		$customer_id = $this->db->getLastId();
			
      	$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "'");
		
		$address_id = $this->db->getLastId();

      	$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
		
		$this->language->load('mail/sales_motivator');
		
		$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));
		
		$message = sprintf($this->language->get('text_welcome'), $this->config->get('config_name')) . "\n\n";
		
		$message .= $this->language->get('text_login');
		$message .= $this->url->link('account/login', '', 'SSL') . "\n\n";
		$message .= $this->language->get('text_login_info') . "\n";
		$message .= sprintf($this->language->get('text_login_email'), $data['email']) . "\n";
		$message .= sprintf($this->language->get('text_login_password'), $data['password']) . "\n\n";
		$message .= $this->language->get('text_services') . "\n\n";
		$message .= $this->language->get('text_thanks') . "\n";
		$message .= $this->config->get('config_name');
		
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');				
		$mail->setTo($data['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
		$mail->send();
		
		// Send to main admin email if new account email is enabled
		if ($this->config->get('config_account_mail')) {
			$message  = $this->language->get('text_signup') . "\n\n";
			$message .= $this->language->get('text_website') . ' ' . $this->config->get('config_name') . "\n";
			$message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "\n";
			$message .= $this->language->get('text_lastname') . ' ' . $data['lastname'] . "\n";
			
			
			if ($data['company']) {
				$message .= $this->language->get('text_company') . ' '  . $data['company'] . "\n";
			}
			
			$message .= $this->language->get('text_email') . ' '  .  $data['email'] . "\n";
			$message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";
			
			$mail->setTo($this->config->get('config_email'));
			$mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();
			
			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_emails'));
			
			foreach ($emails as $email) {
				if (strlen($email) > 0 && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}
	}
	
	public function generateRandomPassword( $length = 8){
		$password = "";
		$start = 97; // ascii code for a
		$stop  = 122; // ascii code for z
		
		while (strlen ($password) != $length ){
			$password.= chr(rand($start, $stop));
		}
			
		return $password;
	}
	
	public function hasReward() {
		$sql = "SELECT COUNT(*) as total FROM " . DB_PREFIX . "sales_motivator_share WHERE customer_id = '" . (int)$this->customer->getId() . "' and product_id = '" . $this->session->data['sm_product_id'] . "'";
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	public function isTimeShareLimitOK() {
		
		$time_last_reward = $this->getDateTimeOfLastReward();
		
		if ($time_last_reward != false) {
			$sql = "SELECT TIMESTAMPDIFF(HOUR, '" . $time_last_reward . "', NOW()) as hours_past";
 
			$query = $this->db->query($sql);
			
			if ($query->row['hours_past'] < $this->config->get('sm_limit_share_reward')) {
				return false;
			}
		}

		return true;	
	}
	
	private function getDateTimeOfLastReward() {
		$sql = "SELECT date_added as time_last_reward FROM " . DB_PREFIX . "sales_motivator_share WHERE customer_id = '" . (int)$this->customer->getId() . "'";
		
		$query = $this->db->query($sql);
		
		if ($query->num_rows) {
			return $query->row['time_last_reward'];
		}

		return false;	
	}
	
	public function addReward() {
		$share_reward_amount = $this->getShareRewardAmount((int)$this->session->data['sm_product_id']);
		
		$sql = "INSERT INTO " . DB_PREFIX . "sales_motivator_share
				SET customer_id = '" . (int)$this->customer->getId() . "',
					product_id  = '" . (int)$this->session->data['sm_product_id'] . "',
					credit_amount = '" . (float)$share_reward_amount . "',
					date_added  = NOW()";
					
		$this->db->query($sql);
		$share_id = $this->db->getLastId();
		
		if ($this->config->get('sm_remove_credit_status')) {
			$sql = "UPDATE " . DB_PREFIX . "sales_motivator_share
					SET date_expire = DATE_ADD(CURRENT_DATE(), INTERVAL " . $this->config->get('sm_remove_credit_days') . " DAY)
					WHERE share_id ='" . (int)$share_id . "'";
			
			$this->db->query($sql);	
		}

		$this->addTransaction($this->customer->getId(), '', $share_reward_amount);	
	}
	
	private function getShareRewardAmount($product_id) {
		$reward_amount = 0;
		
		if ($this->config->get('sm_share_reward_type') == 'F') {
			$reward_amount = (float)$this->config->get('sm_share_reward');
		} else {
		
			$this->load->model('catalog/product');
			
			$product_info = $this->model_catalog_product->getProduct($product_id);
			
			if ($product_info) {
				if ((float)$product_info['special']) {
					$price = round($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), 2);
				} else {
					$price = round($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), 2);
				}
				
				$reward_amount = round(((float)$this->config->get('sm_share_reward') / 100) * $price, 2);
			}
		}
		
		return $reward_amount;
	}
	
	public function addTransaction($customer_id, $description = '', $amount = '', $order_id = 0) {
		$customer_info = $this->getCustomer($customer_id);
		
		if ($customer_info) { 
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$customer_id . "', order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($description) . "', amount = '" . (float)$amount . "', date_added = NOW()");

			$this->language->load('mail/sales_motivator');
						
			$message  = sprintf($this->language->get('text_transaction_received'), $this->currency->format($amount, $this->config->get('config_currency'))) . "\n\n";
			$message .= sprintf($this->language->get('text_transaction_total'), $this->currency->format($this->getTransactionTotal($customer_id)));
			
			if ($description) {
				$message .= "\n\n" . $description;
			}
			
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');
			$mail->setTo($customer_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject(html_entity_decode(sprintf($this->language->get('text_transaction_subject'), $this->config->get('config_name')), ENT_QUOTES, 'UTF-8'));
			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();
		}
	}
	
	public function addNegativeTransaction($customer_id, $description = '', $amount = '', $order_id = 0) {
		$transactions_total = $this->getTransactionTotal($customer_id);	
		
		if ($transactions_total > 0) {	
			if ($transactions_total - abs($amount) < 0) {
				$amount = -$transactions_total;
			}
			
			$customer_info = $this->getCustomer($customer_id);
			
			if ($customer_info) { 
				$this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$customer_id . "', order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($description) . "', amount = '" . (float)$amount . "', date_added = NOW()");

				$this->language->load('mail/sales_motivator');
				
				$message  = sprintf($this->language->get('text_transaction_removed'), $this->currency->format(abs($amount), $this->config->get('config_currency')), $this->config->get('sm_remove_credit_days')) . "\n\n";
				$message .= sprintf($this->language->get('text_transaction_total'), $this->currency->format($this->getTransactionTotal($customer_id)));
				
				if ($description) {
					$message .= "\n\n" . $description;
				}
				
				if ($this->config->get('sm_remove_credit_notification')) { 
					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->hostname = $this->config->get('config_smtp_host');
					$mail->username = $this->config->get('config_smtp_username');
					$mail->password = $this->config->get('config_smtp_password');
					$mail->port = $this->config->get('config_smtp_port');
					$mail->timeout = $this->config->get('config_smtp_timeout');
					$mail->setTo($customer_info['email']);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender($this->config->get('config_name'));
					$mail->setSubject(html_entity_decode(sprintf($this->language->get('text_transaction_subject'), $this->config->get('config_name')), ENT_QUOTES, 'UTF-8'));
					$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
					$mail->send();
				}
			}
		}	
	}
	
	public function getTransactionTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "'");
	
		return $query->row['total'];
	}
	
	public function getCustomer($customer_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
	
		return $query->row;
	}
	
	public function addInvite($invite_id, $product_id, $order_id) {
		$sql = "INSERT INTO " . DB_PREFIX . "sales_motivator_invite 
				SET invite_id = '" . $this->db->escape($invite_id) . "',
					product_id= '" . (int)$product_id . "',
					order_id  = '" . (int)$order_id . "',
					rewarded  = '0'";
		
		$this->db->query($sql);	
	}
	
	public function getCustomersForReward() {
	
		$customers_for_reward_data = array();
		
		$sql = "SELECT smi.*, c.customer_id, c.email, c.firstname, c.lastname, CONCAT(o.firstname, ' ', o.lastname) as order_name, o.total as order_total FROM " . DB_PREFIX . "sales_motivator_invite smi
				LEFT JOIN " . DB_PREFIX . "customer c ON (smi.invite_id = md5(c.customer_id))
				LEFT JOIN `" . DB_PREFIX . "order` o ON (smi.order_id = o.order_id)
				WHERE o.order_status_id IN (" . implode(',', $this->config->get('sm_order_final_status')) . ") AND smi.rewarded = 0";
		
		//echo "SQL =" . $sql;
		
		$query = $this->db->query($sql);

		if ($query->num_rows) {
			foreach ($query->rows as $invtoreward) {
				if ($invtoreward['email']) {
					$go_forward = true;
					
					if ($this->config->get('sm_friend_order_behaviour') && !$this->isProductInOrder($invtoreward['product_id'], $invtoreward['order_id'])) {
						$go_forward = false;
					}
					
					if ($go_forward) {
						if (isset( $customers_for_reward_data[$invtoreward['customer_id']] )) {
							$customers_for_reward_data[$invtoreward['customer_id']]['friends_info'][] = array (
								'order_id'    => $invtoreward['order_id'],
								'name'        => $invtoreward['order_name'],
								'order_total' => $invtoreward['order_total']
							);
							
						} else {
					
							$customers_for_reward_data[$invtoreward['customer_id']] = array(
								'customer_id' => $invtoreward['customer_id'],
								'firstname'   => $invtoreward['firstname'],
								'lastname'    => $invtoreward['lastname'],
								'email'       => $invtoreward['email'],
								'friends_info'=> array(
									array (
									'order_id'    => $invtoreward['order_id'],
									'name'        => $invtoreward['order_name'],
									'order_total' => $invtoreward['order_total']
									)
								)
							);
						}	
					}
				}	
			}	
		}		
		
		//print '<pre>'; print_r($customers_for_reward_data); print '</pre>';
		
		return $customers_for_reward_data;
	}
	
	public function getCustomersForRemoveReward() {
		$sql = "SELECT sms.customer_id, GROUP_CONCAT(sms.share_id) AS expired_share_ids, SUM(sms.credit_amount) AS credit_remove 
				FROM " . DB_PREFIX . "sales_motivator_share sms 
				WHERE DATE(sms.date_expire) = DATE(NOW()) AND sms.credit_removed = 0
				AND (SELECT COUNT(order_id) FROM `" . DB_PREFIX ."order` o 
						WHERE o.customer_id = sms.customer_id AND o.order_status_id IN (" . implode(',', $this->config->get('sm_remove_credit_order_status')) . ")
						AND (DATE(o.date_added) >= DATE(sms.date_added) AND DATE(o.date_added) <= DATE(NOW()))
					) = 0
				GROUP BY customer_id";
				
		//echo "SQL =" . $sql;
		
		$query = $this->db->query($sql);

		return $query->rows;	
	}
	
	public function setShareIDSAsRemoved($share_ids) {
		$this->db->query("UPDATE " . DB_PREFIX . "sales_motivator_share SET credit_removed = 1 WHERE share_id IN (" . $share_ids . ") AND credit_removed = 0");
	}
	
	public function isProductInOrder($product_id, $order_id) {
		$sql = "SELECT COUNT(*) as total FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "' AND product_id = '" . (int)$product_id . "'";
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	public function setAsRewarded($orders) {
		
		if ($orders) {
			foreach ($orders as $order) {
				$sql = "UPDATE " . DB_PREFIX . "sales_motivator_invite SET rewarded = 1 WHERE order_id = '" . (int)$order['order_id'] . "'";
		
				$this->db->query($sql);
			}	
		}
	}
}
?>