<?php 
class ModelModuleFeefo extends Model {
	public function getSetting($group, $store_id = 0) {
		$data = array(); 
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `group` = '" . $this->db->escape($group) . "'");
		
		foreach ($query->rows as $result) {
			if (!$result['serialized']) {
				$data[$result['key']] = $result['value'];
			} else {
				$data[$result['key']] = unserialize($result['value']);
			}
		}

		return $data;
	}
	
	public function getFeefoComments($data = array()) {
		$result = array();
		if (empty($data['product_id'])) return $result;
		
		$settings = $this->getSetting('feefo');
		
		$url = 'http://www.feefo.com/feefo/xmlfeed.jsp';
		
		$params = array(
			'logon' => $settings['data']['Settings']['Logon'],
			'json' => 'true',
			'vendorref' => $data['product_id']
		);
		
		$url .= '?' . http_build_query($params, '', '&');
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$reply = json_decode(curl_exec($ch), true);
		curl_close($ch);
		
		return $reply;
	}
}
?>