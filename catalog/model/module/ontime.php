<?php
class ModelModuleOntime extends Model {
	
	public function getSetting($key) {

		$query = $this->db->query("SELECT `value`, `serialized` FROM " . DB_PREFIX . "ontime WHERE `key` = '" . $this->db->escape($key) . "'");
		
		return $query->row['serialized'] ? unserialize($query->row['value']) : $query->row['value'];
	}
	
	public function getCustomHolidays() {
	
		$dates = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ontime_holidays WHERE `date` >= CURDATE() ORDER BY `date` ASC");
		
		foreach($query->rows as $holiday) {
		
			$dates[] = $holiday['date'];
		}
		
		return $dates;
	}
}