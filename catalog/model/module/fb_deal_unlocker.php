<?php
class ModelModuleFBDealUnlocker extends Model {
	
	public function getDeals(){
		$sql = "SELECT d.*, dd.name as deal_name, dd.description as deal_description FROM " . DB_PREFIX . "fb_deal d
				LEFT JOIN " . DB_PREFIX . "fdu_deal_description dd ON (d.deal_id = dd.deal_id)
				WHERE date_start <= NOW() AND date_stop >= NOW() AND status=1 
				AND dd.language_id='" . (int)$this->config->get('config_language_id') . "' 
				ORDER BY deal_id DESC";
				
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getDealsEndToday(){
		$sql = "SELECT * FROM " . DB_PREFIX . "fb_deal WHERE DATE(date_stop) = DATE(NOW()) AND status = 1";
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getDeal($deal_id){
		$sql = "SELECT d.*, dd.name as deal_name, dd.description as deal_description, pd.name as product_name FROM " . DB_PREFIX . "fb_deal d 
				LEFT JOIN " . DB_PREFIX . "fdu_deal_description dd ON (d.deal_id = dd.deal_id)
				LEFT JOIN " . DB_PREFIX . "product p ON (d.product_id = p.product_id)
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
				WHERE d.deal_id ='" . (int)$deal_id . "' 
				AND pd.language_id ='" . (int)$this->config->get('config_language_id') ."'  
				AND dd.language_id ='" . (int)$this->config->get('config_language_id') ."' 
				ORDER BY d.deal_id DESC";
		$query = $this->db->query($sql);
		
		return $query->row;
	}
	
	public function addDealUser($data){
		$sql = "INSERT INTO " . DB_PREFIX . "fdu_user 
				SET fb_id     ='" . $data['id'] . "',
				    firstname ='" . $this->db->escape($data['first_name']) . "',
				    lastname  ='" . $this->db->escape($data['last_name']) . "',
				    email     ='" . $this->db->escape($data['email']) . "'";
		
		$this->db->query($sql);	
	}
	
	public function editDealUser($fb_id, $data){
		$sql = "UPDATE " . DB_PREFIX . "fdu_user 
				SET fb_id     ='" . $data['id'] . "',
				    firstname ='" . $this->db->escape($data['first_name']) . "',
				    lastname  ='" . $this->db->escape($data['last_name']) . "',
				    email     ='" . $this->db->escape($data['email']) . "'
				WHERE fb_id = '" . $fb_id . "'" ;
		
		$this->db->query($sql);	
	}
	
	public function getDealUser($fb_id){
		$sql = "SELECT * FROM " . DB_PREFIX . "fdu_user WHERE fb_id='" . $fb_id . "'";
		$query = $this->db->query($sql);
		
		return $query->row;
	}
	
	public function alreadyLiked($fb_id, $deal_id){
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "fdu_user_to_deal WHERE fb_id ='" . $fb_id . "' AND deal_id ='" . (int)$deal_id . "' AND `like` != 0";
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	public function getTotalFbUsersByFbId($fb_id){
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX ."fdu_user WHERE fb_id='" . $fb_id . "'";
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	public function getTotalActionsByFbIdAndDealId($fb_id, $deal_id){
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "fdu_user_to_deal WHERE fb_id='" . $fb_id . "' AND deal_id = '" . (int)$deal_id . "'";
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	public function getShareInviteTotals($fb_id, $deal_id) {
		$sql = "SELECT share,invite FROM " . DB_PREFIX . "fdu_user_to_deal WHERE fb_id='" . $fb_id . "' AND deal_id = '" . (int)$deal_id . "'";
		
		$query = $this->db->query($sql);
		
		return $query->row;
	}
	
	public function addAction($data){
		$sql = "INSERT INTO " . DB_PREFIX . "fdu_user_to_deal 
				SET fb_id      ='" . $this->db->escape($data['id']) . "',
					deal_id    ='" . (int)$data['deal_id'] . "',
					date_added =NOW()";
					
		if ($data['point_action'] == 'like') {
			$sql .= ', `like` = 1';
		}
		
		if ($data['point_action'] == 'unlike') {
			$sql .= ', `like` = 0';
		}
		
		if ($data['point_action'] == 'share') {
			$sql .= ', `share` = 1';
		}
		
		if ($data['point_action'] == 'invite') {
			$sql .= ', `invite` =' . $data['no_invited_friends'];
		}
		
		$this->db->query($sql);
	}
	
	public function editAction($data){
		$sql = "UPDATE " . DB_PREFIX . "fdu_user_to_deal SET "; 
					
		if ($data['point_action'] == 'like') {
			$sql .= '`like` = 1';
		}
		
		if ($data['point_action'] == 'unlike') {
			$sql .= '`like` = 0';
		}
		
		if ($data['point_action'] == 'share') {
			$sql .= 'share = share + 1';
		}
		
		if ($data['point_action'] == 'invite') {
			$sql .= 'invite = invite + ' . $data['no_invited_friends'];
		}
		
		$sql .= " WHERE fb_id='" . $data['id'] . "' AND deal_id='" . (int)$data['deal_id'] . "'";
		
		
		$this->db->query($sql);
	}
	
	public function getTotalPointsByFbIdAndDeal($fb_id, $deal_id){
		$total = 0;
		
		$sql = "SELECT * FROM " . DB_PREFIX . "fdu_user_to_deal WHERE fb_id='" . $fb_id . "' AND deal_id='" . (int)$deal_id . "' LIMIT 0,1";
		
		$query = $this->db->query($sql);
		
		if ($query->num_rows){
			$total = $this->config->get('fdu_points_like') * $query->row['like'] + $this->config->get('fdu_points_share') * $query->row['share'] + $this->config->get('fdu_points_invite') * $query->row['invite'];
		}
		
		return $total;
	}
	
	public function getTotalRegisteredUsersByDealId($deal_id){
		$sql = "SELECT COUNT(*) as total FROM " . DB_PREFIX . "fdu_user_to_deal WHERE deal_id = '" . (int)$deal_id . "' AND (`like` != 0 OR share !=0 OR invite != 0)"; 
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	public function getDealRequiredPeople($deal_id){
		$sql = "SELECT person_unlock FROM " . DB_PREFIX . "fb_deal WHERE deal_id ='" . (int)$deal_id . "'";
		
		$query = $this->db->query($sql);
		
		return $query->row['person_unlock'];
	}
	
	public function getDealLastParticipants($deal_id) {
		$sql = "SELECT *, CONCAT(fdu.firstname, ' ', fdu.lastname) AS name, fdutd.like * " . $this->config->get('fdu_points_like') . "+ fdutd.share * " . $this->config->get('fdu_points_share') . " + fdutd.invite * " . $this->config->get('fdu_points_invite') . " AS points  FROM " . DB_PREFIX . "fdu_user fdu 
				LEFT JOIN " . DB_PREFIX . "fdu_user_to_deal fdutd ON (fdu.fb_id = fdutd.fb_id) 
				WHERE fdutd.deal_id = '" . (int)$deal_id . "' 
				ORDER BY fdutd.date_added DESC";
		
		$query = $this->db->query($sql);
		
		return $query->rows;	
	}
	
	public function getOtherParticipants($deal_id, $winner_fb_id){
		$sql = "SELECT *, CONCAT(fdu.firstname, ' ', fdu.lastname) AS name, fdutd.like * " . $this->config->get('fdu_points_like') . "+ fdutd.share * " . $this->config->get('fdu_points_share') . " + fdutd.invite * " . $this->config->get('fdu_points_invite') . " AS points  FROM " . DB_PREFIX . "fdu_user fdu 
				LEFT JOIN " . DB_PREFIX . "fdu_user_to_deal fdutd ON (fdu.fb_id = fdutd.fb_id) 
				WHERE fdutd.deal_id = '" . (int)$deal_id . "' AND fdutd.fb_id != '" . $winner_fb_id . "' 
				ORDER BY fdutd.date_added DESC";
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function disableDeal($deal_id){
		$sql = "UPDATE " . DB_PREFIX . "fb_deal SET status=0 WHERE deal_id='" . (int)$deal_id . "'";
		
		$this->db->query($sql);
	}
	
	public function chooseWinner($deal_id){
		$deal_info = $this->getDeal($deal_id);
		
		$ranking = $this->getDealRanking($deal_id, $deal_info['choose_winner']);
		
		if ($deal_info['choose_winner'] == 'random'){
			shuffle($ranking);
			$winner_info = end($ranking);
		} else {
			$winner_info = $ranking[0];
		}
		
		return $winner_info['fb_id'];
	}
	
	public function addWinner($deal_id, $fb_id){
		$sql = "INSERT INTO " . DB_PREFIX . "fdu_winner 
				SET deal_id ='" . (int)$deal_id ."',
					fb_id   ='" . $this->db->escape($fb_id) . "',
					date_added = NOW() ";
		
		if (!$this->config->get('fdu_approve_winner')){
			$sql .= ", approved = 1";
		}		
		
		$this->db->query($sql);
	}
	
	public function getDealRanking($deal_id){
		$sql = "SELECT fb_id, `like` * " . $this->config->get('fdu_points_like') . "+ share * " . $this->config->get('fdu_points_share') . " + invite * " . $this->config->get('fdu_points_invite') . " AS points
				FROM " . DB_PREFIX . "fdu_user_to_deal 
				WHERE deal_id='" . (int)$deal_id . "'
				ORDER BY points DESC";
		
		$query = $this->db->query($sql);
			
		return $query->rows;	
	}
	
	public function getCurrentPrice($product_id) {

		$customer_group_id = $this->config->get('config_customer_group_id');

		$query = $this->db->query("SELECT DISTINCT *, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$customer_group_id . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");
		
		if ($query->num_rows) {
		
			if ($query->row['special']){
				return $query->row['special'];
			} else {
				return $query->row['price'];
			}
			
		} else {
			return 0;
		}
	}
	
	public function sendNotificationEmail($fb_id, $deal_id){
		$winner_info = $this->getDealUser($fb_id);
		$deal_info   = $this->getDeal($deal_id);
		$other_participants = $this->getOtherParticipants($deal_id, $fb_id);
		
		$find = array(
			'{deal_id}',
			'{firstname}',
			'{lastname}',
			'{email}',
			'{discount_value}', 
			'{prize_name}',
			'{coupon_code}',
			'{winner_firstname}',
			'{winner_lastname}',
			'{store_name}',
			'{store_email}',
			'{store_telephone}'
		);
		
		$coupon_code = $this->generateCode();
		$product_current_price = round($this->getCurrentPrice($deal_info['product_id']),2);
		
		$coupon_info = array(
			'code'       => $coupon_code,
			'discount'   => ($deal_info['prize_type'] == 'win_discount') ? round($deal_info['prize_percent'],2) : $product_current_price,
			'product_id' => $deal_info['product_id']
		);
		
		if ($deal_info['prize_type'] == 'win_discount'){
			$discount_value = round($deal_info['prize_percent'],2); 
		} else {
			$discount_value = round($product_current_price,2); 
		}
		
		$fdu_mail = $this->config->get('fdu_mail');
		$deal_mail = $fdu_mail[$this->config->get('config_language_id')];
		
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');	
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));		
		
		
		// *** send mail to winner
		if ($winner_info){
			$this->addCoupon($coupon_info, $winner_info);
			
			$replace = array(
				'deal_id'          => $deal_id,
				'firstname'        => $winner_info['firstname'],
				'lastname'         => $winner_info['lastname'],
				'email'            => $winner_info['email'],
				'discount_value'   => $discount_value,
				'prize_name'       => $deal_info['product_name'],
				'coupon_code'      => $coupon_code,
				'winner_firstname' => $winner_info['firstname'],
				'winner_lastname'  => $winner_info['lastname'],
				'store_name'       => $this->config->get('config_name'),
				'store_email'      => $this->config->get('config_email'),
				'store_telephone'  => $this->config->get('config_telephone')
			);
			
			$subject = str_replace($find, $replace, html_entity_decode($deal_mail['winner_subject'], ENT_QUOTES, 'UTF-8'));
			$message = str_replace($find, $replace, html_entity_decode($deal_mail['winner_message'], ENT_QUOTES, 'UTF-8'));
			
			echo "Sending mail to winner: " . $winner_info['email'] . "<br />";
			
			if (isset($winner_info['email']) && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $winner_info['email'])) {
				$mail->setTo($winner_info['email']);
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				$mail->setHtml(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->send();
			}	
		}
		// *** stop send mail to winner
		
		if ($this->config->get('fdu_notification_type') && $winner_info && $other_participants) {  // send mail to all participants
			foreach($other_participants as $participant){
				
				$replace = array(
					'deal_id'          => $deal_id,
					'firstname'        => $participant['firstname'],
					'lastname'         => $participant['lastname'],
					'email'            => $participant['email'],
					'discount_value'   => $discount_value,
					'prize_name'       => $deal_info['product_name'],
					'coupon_code'      => $coupon_code,
					'winner_firstname' => $winner_info['firstname'],
					'winner_lastname'  => $winner_info['lastname'],
					'store_name'       => $this->config->get('config_name'),
					'store_email'      => $this->config->get('config_email'),
					'store_telephone'  => $this->config->get('config_telephone')
				);
				
				$subject = str_replace($find, $replace, html_entity_decode($deal_mail['participant_subject'], ENT_QUOTES, 'UTF-8'));
				$message = str_replace($find, $replace, html_entity_decode($deal_mail['participant_message'], ENT_QUOTES, 'UTF-8'));
				
				echo "Sending mail to participant: " . $participant['email'] . "<br />"; 
				
				if (isset($participant['email']) && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $participant['email'])) {
					$mail->setTo($participant['email']);
					$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
					$mail->setHtml(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
					$mail->send();
				}	
			}
		}
	}
	
	public function notifyAdminApproveWinner($fb_id, $deal_id){
		
		$winner_info = $this->getDealUser($fb_id);
		$deal_info   = $this->getDeal($deal_id);
		
		$find = array(
			'{deal_id}',
			'{firstname}',
			'{lastname}',
			'{email}',
			'{discount_value}', 
			'{prize_name}',
			'{coupon_code}',
			'{winner_firstname}',
			'{winner_lastname}',
			'{store_name}',
			'{store_email}',
			'{store_telephone}'
		);
		
		$product_current_price = round($this->getCurrentPrice($deal_info['product_id']),2);
		
		if ($deal_info['prize_type'] == 'win_discount'){
			$discount_value = round($deal_info['prize_percent'],2); 
		} else {
			$discount_value = round($product_current_price,2); 
		}
		
		$fdu_mail = $this->config->get('fdu_mail');
		$deal_mail = $fdu_mail[$this->config->get('config_language_id')];
		
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');
		$mail->setTo($this->config->get('config_email'));		
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		
		if ($winner_info){
		
			$replace = array(
				'deal_id'          => $deal_id,
				'firstname'        => $winner_info['firstname'],
				'lastname'         => $winner_info['lastname'],
				'email'            => $winner_info['email'],
				'discount_value'   => $discount_value,
				'prize_name'       => $deal_info['product_name'],
				'coupon_code'      => '*******',
				'winner_firstname' => $winner_info['firstname'],
				'winner_lastname'  => $winner_info['lastname'],
				'store_name'       => $this->config->get('config_name'),
				'store_email'      => $this->config->get('config_email'),
				'store_telephone'  => $this->config->get('config_telephone')
			);
			
			echo "Sending mail - admin  to approve winner<br />";
			
			$subject = str_replace($find, $replace, html_entity_decode($deal_mail['admin_subject'], ENT_QUOTES, 'UTF-8'));
			$message = str_replace($find, $replace, html_entity_decode($deal_mail['admin_message'], ENT_QUOTES, 'UTF-8'));
			
			$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			$mail->setHtml(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();
		}
		
	}
	
	public function addCoupon($coupon_info, $winner_info){
	
		$sql = "INSERT INTO " . DB_PREFIX . "coupon 
				SET name          ='" . $this->db->escape('FDU ' . $winner_info['firstname'] . ' ' . $winner_info['lastname']) . "',
				    code          ='" . $this->db->escape($coupon_info['code']) . "',
					type          ='F',
					discount      ='" . (float)$coupon_info['discount'] . "',
					logged        = 1,
					date_start    = CURRENT_DATE(),
					date_end      = DATE_ADD(CURRENT_DATE(), INTERVAL 60 DAY),
					uses_total    = 1,
					uses_customer = 1,
					status        = 1,
					date_added    = NOW()";

		$query = $this->db->query($sql);		
		
		$coupon_id = $this->db->getLastId();
		
		$sql = "INSERT INTO " . DB_PREFIX . "coupon_product 
				SET coupon_id  ='" . $this->db->escape($coupon_id) ."',
				    product_id ='" . $this->db->escape($coupon_info['product_id']) ."'";
					
		$this->db->query($sql);			
	}
	
	private function generateCode(){
		$code = 'FDU';
		$temp_len = strlen($code);
		$diff = 10 - $temp_len;
		$ucode = md5(time());
		$code .= substr($ucode,0, $diff);
		
		return strtoupper($code);
	}
}
?>