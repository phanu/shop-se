<?php

class ModelModuleNotifyWhenArrives extends Model {

    public function getProductStatusId($product_id) {

	#1.5.0 does not return product status id
	if ($this->config->get('notify_when_arrives_installed')) {

	    $sql = "
            SELECT stock_status_id from " . DB_PREFIX . "product as product
            WHERE
            product_id = '" . (int) $product_id . "'
	     ";

	    $query = $this->db->query($sql);

	    if ($query->row) {

		return (int) $query->row['stock_status_id'];
	    }
	}
    }

    public function getOutOfStockList() {

	$result = array();

	if ($this->config->get('notify_when_arrives_installed')) {

	    $sql = "
            SELECT product_id from " . DB_PREFIX . "product as product
            WHERE
            product.quantity <= '0'
	    AND
	    product.stock_status_id NOT IN ('" . implode("','", (array) $this->config->get('notify_when_arrives_skip_status')) . "')
	    ";

	    $query = $this->db->query($sql);


	    foreach ($query->rows as $product) {

		$result[] = $product['product_id'];
	    }
	}

	return $result;
    }

    public function register($email, $product_id, $option_id, $option_value_id) {

	if ($this->config->get('notify_when_arrives_installed')) {

	    $query = $this->db->query("SELECT * FROM
                            notify_when_arrives 
                            WHERE
                            product_id = '" . (int) $product_id . "'
                            AND
			     option_id = '" . (int) $option_id . "'
                            AND
			     option_value_id = '" . (int) $option_value_id . "'
                            AND
                            store_id = '" . (int) $this->config->get('config_store_id') . "'
                            AND
                            email = '" . $email . "'
                            AND
                            notified = '0'");

	    if ($query->num_rows) {

		return true;
	    }


	    return $this->db->query("INSERT INTO
                            notify_when_arrives 
                            SET
                            product_id = '" . (int) $product_id . "',
                            store_id = '" . (int) $this->config->get('config_store_id') . "',
			    language_id = '" .  (int)$this->config->get('config_language_id')  . "',
                            email = '" . $email . "',
			    option_value_id = '" . $option_value_id . "',
			    option_id = '" . $option_id . "',
                            notified = '0'");
	}
    }

}

?>