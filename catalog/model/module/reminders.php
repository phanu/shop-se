<?php
class ModelModuleReminders extends Model
{
	public function getOrder($order_id)
	{
		$order_query = $this->db->query("SELECT *, (SELECT CONCAT(c.firstname, ' ', c.lastname) FROM ".DB_PREFIX."customer c WHERE c.customer_id = o.customer_id) AS customer FROM `".DB_PREFIX."order` o WHERE o.order_id = '".(int)$order_id."'");

		if ($order_query->num_rows) {
			$this->load->model('localisation/language');
			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);
			
			if ($language_info) {
				$language_code = $language_info['code'];
				$language_filename = $language_info['filename'];
				$language_directory = $language_info['directory'];
			} else {
				$language_code = '';
				$language_filename = '';
				$language_directory = '';
			}
			
			return array(
				'order_id'          => $order_query->row['order_id'],
				'invoice_no'        => $order_query->row['invoice_no'],
				'invoice_prefix'    => $order_query->row['invoice_prefix'],
				'store_id'          => $order_query->row['store_id'],
				'store_name'        => $order_query->row['store_name'],
				'store_url'         => $order_query->row['store_url'],
				'customer_id'       => $order_query->row['customer_id'],
				'customer'          => $order_query->row['customer'],
				'customer_group_id'	=> $order_query->row['customer_group_id'],
				'firstname'         => $order_query->row['firstname'],
				'lastname'          => $order_query->row['lastname'],
				'telephone'         => $order_query->row['telephone'],
				'fax'               => $order_query->row['fax'],
				'email'             => $order_query->row['email'],
				'comment'           => $order_query->row['comment'],
				'order_status_id'   => $order_query->row['order_status_id'],
				'commission'        => $order_query->row['commission'],
				'language_id'       => $order_query->row['language_id'],
				'language_code'     => $language_code,
				'language_filename' => $language_filename,
				'language_directory'=> $language_directory,				
				'date_added'        => $order_query->row['date_added'],
				'date_modified'     => $order_query->row['date_modified'],
			);
		} else {
			return false;
		}
	}

	public function addOrderHistory($order_id, $data)
	{
        $order_info = $this->getOrder($order_id);

		if (!empty($order_info)) {
			if (isset($data['change']) && $data['change']) {
				if (isset($data['comment'])) $comment = $this->db->escape(strip_tags($data['comment']));
				else $comment = "";
			
				if (!$order_info['invoice_no'] && $data['order_status_id']){
					$query = $this->db->query("SELECT MAX(invoice_no) AS invoice_no FROM `".DB_PREFIX."order` WHERE invoice_prefix = '".$this->db->escape($order_info['invoice_prefix'])."'");
	
					if ($query->row['invoice_no']) $invoice_no = (int)$query->row['invoice_no'] + 1;
					else $invoice_no = 1;
			
					$this->db->query("UPDATE `".DB_PREFIX."order` SET invoice_no = '".(int)$invoice_no."', invoice_prefix = '".$this->db->escape($order_info['invoice_prefix'])."' WHERE order_id = '".(int)$order_id."'");
				}

				$this->db->query("UPDATE `".DB_PREFIX."order` SET order_status_id = '".(int)$data['order_status_id']."', date_modified = NOW() WHERE order_id = '".(int)$order_id."'");
			
				$this->db->query("INSERT INTO ".DB_PREFIX."order_history SET order_id = '".(int)$order_id."', order_status_id = '".(int)$data['order_status_id']."', notify = '".(isset($data['notify']) ? (int)$data['notify'] : 0)."', comment = '".$comment."', date_added = NOW()");

				$status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$data['order_status_id'] . "' AND language_id = '" . (int)$order_info['language_id'] . "'");
			
				$order_info['order_status'] = $status_query->row['name'];
			}

      		if (isset($data['notify']) && $data['notify']) {
				$subject = $data['subject_'.$order_info['language_id']];
				$message = $data['message_'.$order_info['language_id']];
				$sender = $this->config->get('reminders_sender_'.$order_info['language_id']);
				
				if (empty($sender)) $sender = $order_info['store_name'];
				
				if ($this->config->get('reminders_add_signature')) $signature = $this->config->get('reminders_signature_'.$order_info['language_id']);
				else $signature = "";
	
				foreach ($order_info as $key => $value) {
					$subject = str_replace("##".$key."##", $order_info[$key], $subject);
					$sender = str_replace("##".$key."##", $order_info[$key], $sender);
					$message = str_replace("##".$key."##", $order_info[$key], $message);
					$signature = str_replace("##".$key."##", $order_info[$key], $signature);				
				}			

				if (preg_match("/##order_url##/", $message)) {
					$message = str_replace("##order_url##", $this->url->link('account/order/info', 'order_id='.$order_id, 'SSL'), $message);
					$signature = str_replace("##order_url##", $this->url->link('account/order/info', 'order_id='.$order_id, 'SSL'), $signature);
				}
	
				// HTML Mail

				$template = new Template();
			
				$template->data['title'] = html_entity_decode($sender, ENT_QUOTES, 'UTF-8');
				$template->data['message'] = $message;
				$template->data['signature'] = $signature;

				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template').'/template/module/reminders.tpl')) {
					$html = $template->fetch($this->config->get('config_template').'/template/module/reminders.tpl');
				} else {
					$html = $template->fetch('default/template/module/reminders.tpl');
				}

				// Text mail

				$text  = $message.'\n\n';
				$text .= $signature;

				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');
				$mail->setTo($order_info['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender(html_entity_decode($sender, ENT_QUOTES, 'UTF-8'));
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				$mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
				$mail->setText(html_entity_decode(str_replace("<br>", "\n", $text), ENT_QUOTES, 'UTF-8'));
				$mail->send();
			}
		}
	}

    public function getExpiredOrders($data)
    {
    	$query = "";
    	
        if (!empty($data['days']) && !empty($data['status_from'])) {
            $query = $this->db->query("SELECT order_id FROM `".DB_PREFIX."order` WHERE order_status_id = '".(int)$data['status_from']."' AND date_added <  SUBDATE(NOW(),". (int)$data['days'].")");
            return $query->rows;
        } else {
            return array();
        }
    }
    
    public function getExpiringOrders($data)
    {
    	$query = "";
    	
        if (!empty($data['days']) && !empty($data['expire']) && !empty($data['status_from'])) {
            $query = $this->db->query("SELECT order_id FROM `".DB_PREFIX."order` WHERE order_status_id = '".(int)$data['status_from']."' AND STR_TO_DATE(date_added, '%Y-%m-%d') = SUBDATE(STR_TO_DATE(NOW(), '%Y-%m-%d'), ".((int)$data['expire'] - (int)$data['days']).")");
            return $query->rows;
        } else {
            return array();
        }
    }
    
    public function getOrdersToRemind($data)
    {
    	$query = "";
    	
        if (!empty($data['days']) && !empty($data['order_status_id'])) {
            $query = $this->db->query("SELECT order_id FROM `".DB_PREFIX."order` WHERE order_status_id = '".(int)$data['order_status_id']."' AND STR_TO_DATE(date_added, '%Y-%m-%d') = SUBDATE(STR_TO_DATE(NOW(), '%Y-%m-%d'), ".(int)$data['days'].")");
            return $query->rows;
        } else {
            return array();
        }
    }

    public function getOrdersToFollow($data) {
        if (!empty($data['days']) && !empty($data['order_status_id'])) {
            $query = $this->db->query("SELECT order_id FROM `".DB_PREFIX."order` WHERE order_status_id = '".(int)$data['order_status_id']."' AND STR_TO_DATE(date_modified, '%Y-%m-%d') = SUBDATE(STR_TO_DATE(NOW(), '%Y-%m-%d'), ".(int)$data['days'].")");
            return $query->rows;
        } else {
            return array();
        }
    }    

    public function getDelayedOrders($data)
    {
    	$query = "";
    	
        if (!empty($data['days']) && !empty($data['status_from'])) {
            $query = $this->db->query("SELECT order_id FROM `".DB_PREFIX."order` WHERE order_status_id = '".(int)$data['status_from']."' AND STR_TO_DATE(date_modified, '%Y-%m-%d') = SUBDATE(STR_TO_DATE(NOW(), '%Y-%m-%d'), ".(int)$data['days'].")");
            return $query->rows;
        } else {
            return array();
        }
    }
             
    public function getOrdersToNotice($data)
    {
    	$query = "";
    	
        if (!empty($data['days']) && !empty($data['status_from'])) {
        	$todate = date('Y-m-d', (strtotime('now') - $data['days'] * 24 * 60 * 60));

        	if ($data['recurring'] == '0') {
            	$query = $this->db->query("SELECT order_id FROM `".DB_PREFIX."order` WHERE order_status_id = '".(int)$data['status_from']."' AND STR_TO_DATE(date_added, '%Y-%m-%d') = '".$todate."'");
        	} else {
        		$query = $this->db->query("SELECT order_id, STR_TO_DATE(date_added, '%Y-%m-%d') AS date FROM `".DB_PREFIX."order` WHERE order_status_id = '".(int)$data['status_from']."' AND STR_TO_DATE(date_added, '%Y-%m-%d') < '".$todate."'");
        		
        		if ($query->rows) {
        			foreach($query->rows as $row) {
        				$now = strtotime(date('Y-m-d', strtotime('now')));
        				$order = strtotime($row['date']);
        				$multiplier = ($now - $order)/(($data['days'] + 1) * 24 * 60 * 60);

        				if (is_int($multiplier)) $orders[] = $row['order_id'];
        			}
        			
        			if (!empty($orders)) $query = $this->db->query("SELECT order_id FROM `".DB_PREFIX."order` WHERE order_id IN (".implode(',', $orders).")");
        		}

        	}
			
			if (!empty($query)) return $query->rows;
			else return array();
        } else {
            return array();
        }
    }                
}

?>