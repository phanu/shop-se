<?php
class ModelModuleDeleteOrders extends Model
{
    public function getOrdersToDelete()
    {
    	if ($this->config->get('config_delete_missing')) $missing = " OR order_status_id = '0'";
    	else $missing = "";
    	
		$query = $this->db->query("SELECT order_id FROM `".DB_PREFIX."order` WHERE (order_status_id = '".(int)$this->config->get('config_delete_status_id')."' AND date_modified < SUBDATE(NOW(),". (int)$this->config->get('config_delete_days')."))".$missing);
		
		return $query->rows;
    }
    
	public function deleteOrder($order_id)
	{
		if ($this->config->get('config_delete_restock'))
		{
			$order_query = $this->db->query("SELECT * FROM `".DB_PREFIX."order` WHERE order_id = '".(int)$order_id."'");

			if ($order_query->num_rows)
			{
				$product_query = $this->db->query("SELECT * FROM `".DB_PREFIX."order_product` WHERE order_id = '".(int)$order_id."'");

				foreach($product_query->rows as $product)
				{
					$this->db->query("UPDATE `".DB_PREFIX."product` SET quantity = (quantity + ".(int)$product['quantity'].") WHERE product_id = '".(int)$product['product_id']."' AND subtract = '1'");

					$option_query = $this->db->query("SELECT * FROM `".DB_PREFIX."order_option` WHERE order_id = '".(int)$order_id."' AND order_product_id = '".(int)$product['order_product_id']."'");

					foreach ($option_query->rows as $option)
					{
						$this->db->query("UPDATE `".DB_PREFIX."product_option_value` SET quantity = (quantity + ".(int)$product['quantity'].") WHERE product_option_value_id = '".(int)$option['product_option_value_id']."' AND subtract = '1'");
					}
				}
			}
		}

		$this->db->query("DELETE FROM `".DB_PREFIX."order` WHERE order_id = '".(int)$order_id."'");
		$this->db->query("DELETE FROM `".DB_PREFIX."order_product` WHERE order_id = '".(int)$order_id."'");
      	$this->db->query("DELETE FROM `".DB_PREFIX."order_option` WHERE order_id = '".(int)$order_id."'");
		$this->db->query("DELETE FROM `".DB_PREFIX."order_download` WHERE order_id = '".(int)$order_id."'");
		
      	$this->db->query("DELETE FROM `".DB_PREFIX."order_total` WHERE order_id = '".(int)$order_id."'");
		$this->db->query("DELETE FROM `".DB_PREFIX."order_history` WHERE order_id = '".(int)$order_id."'");
		
		$this->db->query("DELETE FROM `".DB_PREFIX."customer_transaction` WHERE order_id = '".(int)$order_id."'");
		$this->db->query("DELETE FROM `".DB_PREFIX."customer_reward` WHERE order_id = '".(int)$order_id."'");
		$this->db->query("DELETE FROM `".DB_PREFIX."affiliate_transaction` WHERE order_id = '".(int)$order_id."'");
	}
}

?>