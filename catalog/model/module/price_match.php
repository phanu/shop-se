<?php
class ModelModulePriceMatch extends Model {
	
	public function addPriceMatch($data){
		$this->load->model('catalog/product');
		
		$product_info = $this->model_catalog_product->getProduct((int)$data['price_match_product_id']);
		
		if ($data['price_match_competitor_price'] < $data['price_match_product_price']) {
			$diff = $data['price_match_product_price'] - $data['price_match_competitor_price'];
			$refine = $this->currency->format( $this->tax->getTax( $diff ,$product_info['tax_class_id']) , $this->session->data['currency'], '', false);
		} else {
			$refine = 0;
		}
		
		$start_search = $data['price_match_competitor_price'] - ($data['price_match_product_tax'] - $refine);
		
		$this->tax->calculate($data['price_match_product_price_with_tax'] - $data['price_match_product_price'], $product_info['tax_class_id'], $this->config->get('config_tax'));
		
		if ($product_info['tax_class_id']){ 
			$competitor_price = $this->findPriceWithoutTax($product_info['tax_class_id'], $start_search , (float)$data['price_match_competitor_price']);
		} else {
			$competitor_price = (float)$data['price_match_competitor_price'];
		}
	
		$sql = "INSERT INTO " . DB_PREFIX . "price_match 
				SET product_id         = '" . (int)$data['price_match_product_id'] . "',
				    customer_name      = '" . $this->db->escape($data['price_match_customer_name']) . "',
				    customer_email     = '" . $this->db->escape($data['price_match_customer_email']) . "',
				    customer_telephone = '" . $this->db->escape($data['price_match_customer_telephone']) . "',
				    customer_group_id  = '" . $this->db->escape($data['price_match_customer_group_id']) . "',
					competitor_link    = '" . $this->db->escape($data['price_match_competitor_link']) . "',
					comment            = '" . $this->db->escape($data['price_match_message']) . "',
					competitor_price   = '" . (float)$competitor_price. "',
					our_price          = '" . (float)$data['price_match_product_price'] . "',
					currency_code      = '" . $this->db->escape($this->session->data['currency'])."',
					date_added         = NOW()";
		
		$this->db->query($sql);

		// mail to admin
		$this->language->load('mail/price_match');
		
		$this->load->model('catalog/product');
		
		$product_info = $this->model_catalog_product->getProduct($data['price_match_product_id']);
		
		$subject = sprintf($this->language->get('text_subject'), $product_info['name']);
		
		$message = $this->language->get('text_hi') . "\n\n";
		
		$message .= sprintf($this->language->get('text_inform'), $data['price_match_customer_name'], $product_info['name']) . "\n\n";
		$message .= sprintf($this->language->get('text_customer_name'), $data['price_match_customer_name']) . "\n";
		$message .= sprintf($this->language->get('text_customer_email'), $data['price_match_customer_email']) . "\n";
		$message .= sprintf($this->language->get('text_customer_telephone'), $data['price_match_customer_telephone']) . "\n";
		$message .= sprintf($this->language->get('text_competitor_link'), $data['price_match_competitor_link']) . "\n";
		$message .= sprintf($this->language->get('text_choosen_currency'), $this->session->data['currency']) . "\n";
		$message .= sprintf($this->language->get('text_competitor_price'), $data['price_match_competitor_price']) . "\n";
		$message .= sprintf($this->language->get('text_your_price'), $data['price_match_product_price'] + $data['price_match_product_tax']) . "\n";
		$message .= sprintf($this->language->get('text_comment'), html_entity_decode($data['price_match_message'], ENT_QUOTES, 'UTF-8'));

		
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');				
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
		
		$admin_emails = explode(",", str_replace(" ", "", $this->config->get('price_match_admin_mail')));
		
		foreach($admin_emails as $admin_mail){
			$mail->setTo($admin_mail);
			$mail->send();
		}		
	}
	
	private function findPriceWithoutTax($tax_class_id, $start_value, $desired_value ){
		for ($current_value=$start_value; $current_value < $desired_value; $current_value+=0.01){
			$calculated_value = $this->currency->format($this->tax->calculate($current_value, $tax_class_id, $this->config->get('config_tax')), $this->session->data['currency'],'', false);
			
			if ($calculated_value == $desired_value){
				return $current_value;
			}
		}
		
		return $start_value;
	}
}
?>