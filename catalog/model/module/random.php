<?php
#############################################################################################
#  Module Random Products for Opencart 1.5.1.x From HostJars http://opencart.hostjars.com 	#
#############################################################################################
class ModelModuleRandom extends Model {
	public function getRandomProducts($cats, $limit=8) {
	
		$sql = "SELECT p.product_id, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'"; 
		if ($cats) {
			$sql .= ' AND (';
			$categories = explode('^', $cats);
			$cat_temp = array();
			foreach ($categories as $cat_id) {
				$cat_temp[] = "p2c.category_id = '" . $cat_id . "'"; 
			}
			$sql .= implode(' OR ', $cat_temp);
			$sql .= ')';
		}
		$sql .= " ORDER BY RAND() LIMIT " . $limit;
		$query = $this->db->query($sql);

		return $query->rows;

	}
}
?>