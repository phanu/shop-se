<?php
class ModelTicketInstall extends Model {
	public function install() {
		$this->db->query("
			CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "ticket(
				ticket_id int NOT NULL AUTO_INCREMENT,
				PRIMARY KEY(ticket_id),
				ticket_subject text,
				ticket_status varchar(255),
				ticket_created datetime,
				ticket_department_id int,
				ticket_customer_id int,
				ticket_user_id int,
				ticket_order_id int,
				ticket_last_update varchar(255)
			)
			DEFAULT CHARACTER SET utf8   
			COLLATE utf8_general_ci;
		");
		
		
$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "ticket_config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_status` int(11) DEFAULT NULL,
  `order_required` int(11) DEFAULT NULL,
  `formdata` text,
  `clientcreated` text NOT NULL,
  `admincreateticket` text NOT NULL,
  `adminrepliedticket` text NOT NULL,
  `clientsubject` text NOT NULL,
  `admincreatesubject` text NOT NULL,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2") ;
$this->db->query("
INSERT INTO `" . DB_PREFIX . "ticket_config` (`config_id`, `order_status`, `order_required`, `formdata`, `clientcreated`, `admincreateticket`, `adminrepliedticket`, `clientsubject`, `admincreatesubject`) VALUES
(1, 1, 1, 'a:0:{}', '&lt;p&gt;You created a new Ticket&lt;/p&gt;\r\n', '&lt;p&gt;Your ticket ius replied by admin&amp;nbsp;&lt;/p&gt;\n', '&lt;p&gt;&lt;span style=&quot;font-size: 13px;&quot;&gt;Your ticket ius replied by admin&amp;nbsp;&lt;/span&gt;&lt;/p&gt;\n', 'Client ticket Crated', 'Admin ticket created')");
	$this->db->query("
			CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "department(
				department_id int NOT NULL AUTO_INCREMENT,
				PRIMARY KEY(department_id),
				default_user_id int,
				department_name varchar(255)
			)
			DEFAULT CHARACTER SET utf8   
			COLLATE utf8_general_ci;
		");
		
		$this->db->query("
			CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "ticket_message(
				message_id int NOT NULL AUTO_INCREMENT,
				PRIMARY KEY(message_id),
				content text,
				is_user boolean,
				created datetime,
				ticket_id int,
				file varchar(255)
			)
			DEFAULT CHARACTER SET utf8   
			COLLATE utf8_general_ci;
		");
		
		print "Database is installed";
	}	
}
?>