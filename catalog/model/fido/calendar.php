<?php
class ModelFidoCalendar extends Model {
	public function getCalendar() {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "calendar c LEFT JOIN " . DB_PREFIX . "calendar_description cd ON (c.calendar_id = cd.calendar_id) LEFT JOIN " . DB_PREFIX . "calendar_to_store c2s ON (c.calendar_id = c2s.calendar_id) WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1' AND YEAR(c.start_date) = '" . date('Y') . "' ORDER BY c.start_date");
		return $query->rows;
	}

	public function getById($calendar_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "calendar c LEFT JOIN " . DB_PREFIX . "calendar_description cd ON (c.calendar_id = cd.calendar_id) LEFT JOIN " . DB_PREFIX . "calendar_to_store c2s ON (c.calendar_id = c2s.calendar_id) WHERE c.calendar_id = '" . (int)$calendar_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");
		return $query->row;
	}

	public function getAllEvents() {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "calendar c LEFT JOIN " . DB_PREFIX . "calendar_description cd ON (c.calendar_id = cd.calendar_id) LEFT JOIN " . DB_PREFIX . "calendar_to_store c2s ON (c.calendar_id = c2s.calendar_id) WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1' AND YEAR(c.start_date) = '" . date('Y') . "' ORDER BY c.start_date");
		return $query->rows;
	}

	public function getEventProducts($calendar_id) {
		$query = $this->db->query("SELECT *, p.product_id AS product_id , p.price AS price, ps.price AS special FROM " . DB_PREFIX . "calendar_products cp LEFT JOIN " . DB_PREFIX . "product p ON (cp.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "product_special ps ON (p.product_id = ps.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND p.status = '1' AND cp.calendar_id = '" . (int)$calendar_id . "'");
		if ($query->rows) {
			return $query->rows;
		} else {
			return FALSE;
		}
	}

	public function checkStart($date) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "calendar c LEFT JOIN " . DB_PREFIX . "calendar_to_store c2s ON (c.calendar_id = c2s.calendar_id) WHERE c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1' AND c.start_date = '" . $date . "' GROUP BY c.start_date");
		return $query->rows;
	}

	public function countEvents($date) {
     	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "calendar c LEFT JOIN " . DB_PREFIX . "calendar_to_store c2s ON (c.calendar_id = c2s.calendar_id) WHERE c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.start_date = '" . $date . "'");
		return $query->row['total'];
	}

	public function getEventId($date) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "calendar c LEFT JOIN " . DB_PREFIX . "calendar_to_store c2s ON (c.calendar_id = c2s.calendar_id) WHERE c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1' AND start_date = '" . $date . "'");
		return $query->row['calendar_id'];
	}

	public function getSetting($key, $store_id = 0) {
		$data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `key` = '" . $this->db->escape($key) . "' AND store_id = '" . (int)$store_id . "'");

		if ($query->row) {
			if (!$query->row['serialized']) {
				$data['value'] = $query->row['value'];
			} else {
				$data['value'] = unserialize($query->row['value']);
			}
		}

		if (isset($data['value'])) {
			return $data['value'];
		} else {
			return null;
		}
	}
}
?>
