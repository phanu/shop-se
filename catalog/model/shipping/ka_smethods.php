<?php
/*
	Project: Custom Shipping Methods
	Author : karapuz <support@ka-station.com>

	Version: 1 ($Revision: 23 $)
*/
class ModelShippingKaSMethods extends Model {

	protected $address, $weight, $subtotal, $products_total;
	protected $smethod;

	public function initMethod($smethod) {
	
		$this->smethod = $smethod;
		if (empty($this->smethod['descr'])) {
			$this->smethod['descr'] = $this->smethod['name'];
		}
		
	}
		
  	public function getQuote($address) {

  		if (empty($this->smethod)) {
  			return false;
  		}
  	
  		$this->address        = $address;
		$this->weight         = (float)$this->cart->getWeight();
		$this->subtotal       = (float)$this->cart->getSubTotal();
		$this->products_total = (int)$this->cart->countProducts();
  		
		$this->language->load('shipping/ka_smethods');

		$rate = $this->getRate($this->smethod['shipping_method_id']);

		if (empty($rate)) {
			return array();
		}
		
		$code = 'ka_smethods_' . $this->smethod['shipping_method_id'];

		$quote_data['ka_smethods_' . $this->smethod['shipping_method_id']] = array(
			'code'         => $code . '.' . $code,
			'title'        => $this->smethod['descr'],
			'cost'         => $rate['cost'],
			'tax_class_id' => $this->smethod['tax_class_id'],
			'text'         => $this->currency->format($this->tax->calculate($rate['cost'], $this->smethod['tax_class_id'], $this->config->get('config_tax')))
		);
		
		$method_data = array();
	
		if ($quote_data) {
      		$method_data = array(
        		'code'       => $code,
        		'title'      => $this->smethod['name'],
        		'quote'      => $quote_data,
				'sort_order' => $this->smethod['sort_order'],
        		'error'      => false
      		);
		}

		return $method_data;
  	}
  	
  	/*
  		function requires some class parameters defined before it is called.
  	
  		Returns:
  			array - on success with elements
  				[cost] -> shipping cost
  				
  			false - no rates available  	
  	
  	*/  	
	protected function getRate($method_id) {
	
		$qry = $this->db->query("SELECT * FROM " . DB_PREFIX . "ka_shipping_rates WHERE			
			shipping_method_id = '" . (int)$method_id . "'
			AND (weight_min <= " . $this->weight . " AND (weight_max >= " . $this->weight . " OR weight_max = 0))
			AND (subtotal_min <= " . $this->subtotal . " AND (subtotal_max >= " . $this->subtotal . " OR subtotal_max = 0))
			"
		);
		
		if (empty($qry->rows)) {
			return false;
		}
		
		$best_rate = false;
		foreach ($qry->rows as $rate) {
		
			$cost = 0;
			// check geo zone
			if ($rate['geo_zone_id']) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone
					WHERE geo_zone_id = '" . (int)$rate['geo_zone_id'] . "' 
						AND country_id = '" . (int)$this->address['country_id'] . "' 
						AND (zone_id = '" . (int)$this->address['zone_id'] . "' OR zone_id = '0')"
				);
			
				if (empty($query->rows)) {
					continue;
				}
			}
			
			// calculate flat charge
			$cost += $rate['flat_charge'];	
						
			// calculate per item charge
			$cost += ($this->products_total * $rate['per_item_charge']);
			
			// calculate per weight charge
			$cost += ($this->weight * $rate['per_weight_charge']);
			
			if (empty($best_rate) || $best_rate['cost'] > $cost) {
				$best_rate         = $rate;
				$best_rate['cost'] = $cost;
			}
		}
		
		return $best_rate;
	}
}
?>