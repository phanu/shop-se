<?php
class ModelAffiliateBanner extends Model {
    public function getCategories($parent_id = 0) {
        /** create tables */   
        $this->CheckIfCreatedAllTables();

		$category_data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC");
		foreach ($query->rows as $result) {
			$category_data[] = array(
				'category_id' => $result['category_id'],
				'name'        => $this->getPath($result['category_id'], $this->config->get('config_language_id')),
				'status'  	  => $result['status'],
				'sort_order'  => $result['sort_order']
			);
		
			$category_data = array_merge($category_data, $this->getCategories($result['category_id']));
		}	
		return $category_data;
	}

    public function getManufacturers() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer m LEFT JOIN " . DB_PREFIX . "manufacturer_to_store m2s ON (m.manufacturer_id = m2s.manufacturer_id) WHERE m2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY name");
		$manufacturer_data = $query->rows;
        return $manufacturer_data ;
    }

    public function GetProductsPurchased($banner_id, $period){
        $query = $this->db->query ("
                SELECT SUM(quantity) AS c
                FROM `".DB_PREFIX."order`, 
                      ".DB_PREFIX."ubanner,
                      ".DB_PREFIX."order_product
                WHERE `".DB_PREFIX."order`.banner_id = `".DB_PREFIX."ubanner`.ubanner_id 
                AND `".DB_PREFIX."order`.banner_id =".(int)$banner_id."
                AND ".DB_PREFIX."order_product.order_id = `".DB_PREFIX."order`.order_id
                AND date_added > DATE_SUB(NOW(), INTERVAL $period DAY) 
                GROUP BY `".DB_PREFIX."order`.banner_id ");   
        if (isset($query->row['c']))
            return $query->row['c'];
        else 
            return 0;                              
    }

    public function AddTransfer(){
        if ( isset($this->request->get['_b']) ){
            $banner_id = (int)$this->request->get['_b'];
               $this->db->query ("
                INSERT INTO ".DB_PREFIX."ubanner_statistics 
                SET type   = 'transfer',
                banner_id  = '".(int)$banner_id."',
                bcount     = '1',
                time_date  = NOW()");                             
        }
    }

    public function wasShow($banner_id){
       $this->db->query ("
        INSERT INTO ".DB_PREFIX."ubanner_statistics 
        SET type   = 'showcount',
        banner_id  = '".(int)$banner_id."',
        bcount     = 1,
        time_date  = NOW()");  
        
    }   
    
    public function ProductsWasShow($banner_id,$c){
       $this->db->query ("
        INSERT INTO ".DB_PREFIX."ubanner_statistics 
        SET type   = 'productswasshow',
        banner_id  = '".(int)$banner_id."',
        bcount     = '".(int)$c."',
        time_date  = NOW()");          
    }       
    
    public function getColorSchemes(){
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ubanner_colorschemes");
        return $query->rows;
    }    
    
    public function EditBanner($data){

        if (isset($data['categories']) && count($data['categories'])>0){}else 
            $data['categories'] = array();            

        if (isset($data['manufacturers']) && count($data['manufacturers'])>0){}else 
            $data['manufacturers'] = array();      
       
        $this->db->query ("UPDATE ".DB_PREFIX."ubanner SET
            affiliate_id = '".(int)$this->affiliate->isLogged()."',  
            type         = '". $this->db->escape($data['type'])."', 
            categories   = '".serialize($data['categories'])."',
            manufacturers= '".serialize($data['manufacturers'])."',
            color_scheme = '". $this->db->escape($data['color_scheme'])."',
            layout       = '". $this->db->escape($data['layout'])."',
            banner_title = '". $this->db->escape($data['banner_title'])."',
            domain_name  = '". $this->db->escape($data['domain_name'])."', 
            sc_border 	 = '". $this->db->escape($data['border'])."',
            sc_title 	 = '". $this->db->escape($data['title'])."',
            sc_title_background = '". $this->db->escape($data['title_background'])."',	
            sc_background  = '". $this->db->escape($data['background'])."',	
            sc_text        = '". $this->db->escape($data['text'])."', 	
            sc_link        = '". $this->db->escape($data['link'])."', 	
            sc_link_hover  = '". $this->db->escape($data['link_hover'])."', 	
            sc_link_bottom = '". $this->db->escape($data['link_bottom'])."', 	
            sc_footer_background = '". $this->db->escape($data['footer_background'])."', 	
            sc_link_footer = '". $this->db->escape($data['link_footer'])."',
            language       = '". $this->db->escape($data['language'])."'
            WHERE ubanner_id = ".$data['banner_id']);

        return $data['banner_id'] ;           
    }
   
    public function DeleteBanner($banner_id){
        $this->db->query ("DELETE FROM `".DB_PREFIX."ubanner` WHERE `ubanner_id` = ".(int)$banner_id);        
    }   
    
    public function addBanner($data){

        if (isset($data['categories']) && count($data['categories'])>0){}else 
            $data['categories'] = array();            

        if (isset($data['manufacturers']) && count($data['manufacturers'])>0){}else 
            $data['manufacturers'] = array();    
        
        $this->db->query ("INSERT INTO ".DB_PREFIX."ubanner SET
            affiliate_id = '".(int)$this->affiliate->isLogged()."',  
            type         = '". $this->db->escape($data['type'])."', 
            categories   = '".serialize($data['categories'])."',
            manufacturers= '".serialize($data['manufacturers'])."',
            color_scheme = '". $this->db->escape($data['color_scheme'])."',
            layout       = '". $this->db->escape($data['layout'])."',
            banner_title = '". $this->db->escape($data['banner_title'])."',
            domain_name  = '". $this->db->escape($data['domain_name'])."', 
            sc_border 	 = '". $this->db->escape($data['border'])."',
            sc_title 	 = '". $this->db->escape($data['title'])."',
            sc_title_background = '". $this->db->escape($data['title_background'])."',	
            sc_background  = '". $this->db->escape($data['background'])."',	
            sc_text        = '". $this->db->escape($data['text'])."', 	
            sc_link        = '". $this->db->escape($data['link'])."', 	
            sc_link_hover  = '". $this->db->escape($data['link_hover'])."', 	
            sc_link_bottom = '". $this->db->escape($data['link_bottom'])."', 	
            sc_footer_background = '". $this->db->escape($data['footer_background'])."', 	
            sc_link_footer = '". $this->db->escape($data['link_footer'])."',
            store_id       = '".(int)$this->config->get('config_store_id')."',
            language       = '". $this->db->escape($data['language'])."'");
        $banner_id = $this->db->getLastId();  
        return $banner_id;           
    }    

    public function getCode($affiliate_id){
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate 
         WHERE affiliate_id = '" . (int)$affiliate_id . "' ");
        return $query->row;
    }

    public function GetLayouts(){
        $query = $this->db->query ("SELECT * FROM ".DB_PREFIX."ubanner_layouts");
        return $query->rows;
    }
    
    public function GetBanner($banner_id){
        $query = $this->db->query ("
            SELECT * FROM ".DB_PREFIX."ubanner
            LEFT JOIN ".DB_PREFIX."ubanner_layouts ON ".DB_PREFIX."ubanner_layouts.layouts_id = ".DB_PREFIX."ubanner.layout
            WHERE ".DB_PREFIX."ubanner.ubanner_id = ".(int)$banner_id);
        return $query->row;
    }
    
    public function GetStatistics($banner_id, $type, $period){
        $query = $this->db->query ("
            SELECT SUM(bcount) AS bcount FROM " . DB_PREFIX . "ubanner_statistics
            WHERE banner_id = '".(int)$banner_id."'
            AND type = '".$this->db->escape($type)."'
            AND time_date > DATE_SUB(NOW(), INTERVAL $period DAY)
            GROUP BY banner_id
        ");
        if (isset ($query->row['bcount']))
            return $query->row['bcount'];
        else 
            return 0;             
    }
    public function HowManyMoney($banner_info){
        $products = $this->getProducts(array(), $banner_info);
        $commision = 0;
        $perc = ((int)$this->config->get('config_commission'))/100 ;
        foreach($products AS $product){
            $commision=$commision+$product['price']*$perc;
        }
        return $commision;
    }
    
    public function getProducts($data, $banner_info, $return_all_count=0){
        $categories    = unserialize($banner_info['categories']);
        $manufacturers = unserialize($banner_info['manufacturers']);
        
        if ( count($categories) == 0 ) $categories[] = 0;
        if ( count($manufacturers) == 0 ) $manufacturers[] = 0;
        
        $sql = " SELECT DISTINCT   
                    p.product_id,
                    pd.name,
                    p.image,
                    p.price,
                    p.tax_class_id,
                    pd.description FROM ".DB_PREFIX."product p 
                 LEFT JOIN ".DB_PREFIX."product_description pd ON (p.product_id = pd.product_id) 
                 LEFT JOIN ".DB_PREFIX."product_to_store p2s ON (p.product_id = p2s.product_id) 
				 LEFT JOIN ".DB_PREFIX."product_to_category p2c ON (p.product_id = p2c.product_id) ";	
			
		$sql .= " WHERE pd.language_id = '" . (int)$banner_info['language'] . "' 
                  AND p.status = '1' 
                  AND p.date_available <= NOW() 
                  AND p2s.store_id = '" . (int)$banner_info['store_id'] . "'"; 
		$sql .= " AND p2c.category_id IN (" . implode(', ', $categories) . ")";	

        if (count($this->getManufacturers())>0){
            $sql .= " AND p.manufacturer_id IN (" . implode(', ', $manufacturers) . ")";	
        }
        
        /** Most Recent Products */                
        if ($banner_info['type'] == 1 ){
            $sql .= " ORDER BY p.date_added DESC";
        }
        
        /** Most Recent Featured Products + Random Featured Products */    
        if ($banner_info['type'] == 2 || $banner_info['type'] == 2){
            $featured_product = explode(',', $this->config->get('featured_product'));	

            if (count($featured_product)>0){
                $sql .= " AND p.product_id IN (". $this->config->get('featured_product').")  ";
                if ($banner_info['type'] == 4)
                $sql .= " ORDER BY p.date_added DESC";
            } else {
                return array();                                
            }	
        }   
             
        /** Most Recent Special Offers + Random Special Offers */    
        if ($banner_info['type'] == 3 || $banner_info['type'] == 5){
            $sql_sp = "SELECT DISTINCT ps.product_id FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$banner_info['store_id']."' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) GROUP BY ps.product_id";
            $query_special = $this->db->query($sql_sp);
            $special_products = $query_special->rows;
            $sp_array = array();
            if (count($special_products)>0){
                foreach ($query_special->rows AS $special)
                    $sp_array[]=$special['product_id'];
            } else {
                return array();   
            }
            $sql .= " AND p.product_id IN (".implode(', ', $sp_array) .")  ";
            if ($banner_info['type'] == 5)
            $sql .= " ORDER BY p.date_added DESC";                
        }                
                 
        /** Random */     
        if ($banner_info['type'] == 6 ){
            $sql .= "  ";
        }
        
        /** show all */   
        if ($banner_info['type'] == 7 ){
            $sql .= " ORDER BY p.product_id ";
        }   
        
        if ($return_all_count){
            /** --------------------------------------------------------- */
            $query = $this->db->query($sql);
            $will_be_show = $query->num_rows;
            if ( $this->config->has('config_maxbanner') ){
                $maxbanner = $this->config->get('config_maxbanner');
                if ( $query->num_rows>=$maxbanner ) return $maxbanner;
            }
            return $will_be_show; 
            /** --------------------------------------------------------- */
        } else {
            /** --------------------------------------------------------- */
    		if (isset($data['start']) || isset($data['limit'])) {
    			if ($data['start'] < 0) {
    				$data['start'] = 0;
    			}				
    
    			if ($data['limit'] < 1) {
    				$data['limit'] = 1;
    			}	
    		
    			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
    		}      
            
            $query = $this->db->query($sql);
            return $query->rows; 
            /** --------------------------------------------------------- */                   
        }         
    }
    
    public function GetSpecialPrice($product_id, $banner_info){
        $query = $this->db->query("SELECT  ps.price FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$banner_info['store_id']."' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) 
        AND  p.product_id = '".(int)$product_id."'
        ORDER BY ps.`price` ASC");     
        if ( isset($query->row['price'] ))
            return $query->row['price'];
        else             
            return 0;   
    } 
    
    public function GetBanners($data){
        $sql = "
            SELECT * FROM ".DB_PREFIX."ubanner
            LEFT JOIN ".DB_PREFIX."ubanner_layouts ON ".DB_PREFIX."ubanner_layouts.layouts_id = ".DB_PREFIX."ubanner.layout
            WHERE ".DB_PREFIX."ubanner.affiliate_id = ".(int)$this->affiliate->isLogged();
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}       
         
        $query = $this->db->query ($sql);    
        return $query->rows;                
    }    
    
    public function GetTotalBanners(){
        $query = $this->db->query ("
            SELECT COUNT(DISTINCT ".DB_PREFIX."ubanner.ubanner_id) AS total FROM ".DB_PREFIX."ubanner
            LEFT JOIN ".DB_PREFIX."ubanner_layouts ON ".DB_PREFIX."ubanner_layouts.layouts_id = ".DB_PREFIX."ubanner.layout
            WHERE ".DB_PREFIX."ubanner.affiliate_id = ".(int)$this->affiliate->isLogged());  
		
		if (isset($query->row['total'])) {
			return $query->row['total'];
		} else {
			return 0;	
		}            
    }    
 
    private function CheckIfCreatedAllTables(){
        $q = $this->db->query("SHOW COLUMNS FROM `".DB_PREFIX."order` LIKE 'banner_id'");
        if ( $q->num_rows != 1){
            $this->db->query("ALTER TABLE `".DB_PREFIX."order` ADD `banner_id` INT( 10 ) NOT NULL AFTER `affiliate_id`"); 

            $query = $this->db->query("
                CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ubanner` (
                  `ubanner_id` int(11) NOT NULL auto_increment,
                  `affiliate_id` int(11) NOT NULL,
                  `type` varchar(5) NOT NULL,
                  `categories` text NOT NULL,
                  `manufacturers` text NOT NULL,
                  `layout` varchar(5) NOT NULL,
                  `color_scheme` varchar(10) NOT NULL,
                  `sc_border` varchar(10) NOT NULL,
                  `sc_title` varchar(10) NOT NULL,
                  `sc_title_background` varchar(10) NOT NULL,
                  `sc_background` varchar(10) NOT NULL,
                  `sc_text` varchar(10) NOT NULL,
                  `sc_link` varchar(10) NOT NULL,
                  `sc_link_hover` varchar(10) NOT NULL,
                  `sc_link_bottom` varchar(10) NOT NULL,
                  `sc_footer_background` varchar(10) NOT NULL,
                  `sc_link_footer` varchar(10) NOT NULL,
                  `banner_title` varchar(255) NOT NULL,
                  `domain_name` varchar(255) NOT NULL,
                  `store_id` int(10) NOT NULL,
                  `language` int(5) NOT NULL,
                  PRIMARY KEY  (`ubanner_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8  AUTO_INCREMENT=1");
        
            $query = $this->db->query("
               CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ubanner_statistics` (
                  `statistics_id` int(11) NOT NULL auto_increment,
                  `banner_id` int(10) NOT NULL,
                  `type` varchar(50) NOT NULL,
                  `bcount` int(15) NOT NULL,
                  `time_date` datetime NOT NULL,
                  PRIMARY KEY  (`statistics_id`)
                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8  AUTO_INCREMENT=1");        
        
            $query = $this->db->query("
                CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ubanner_colorschemes` (
                  `colorschemes_id` int(11) NOT NULL auto_increment,
                  `name` varchar(20) NOT NULL,
                  `border` varchar(10) NOT NULL,
                  `title` varchar(255) NOT NULL,
                  `title_background` varchar(10) NOT NULL,
                  `background` varchar(10) NOT NULL,
                  `text` varchar(10) NOT NULL,
                  `link` varchar(10) NOT NULL,
                  `link_hover` varchar(10) NOT NULL,
                  `link_bottom` varchar(10) NOT NULL,
                  `footer_background` varchar(10) NOT NULL,
                  `link_footer` varchar(10) NOT NULL,
                  PRIMARY KEY  (`colorschemes_id`)
                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8  AUTO_INCREMENT=6");
        
            $query  = $this->db->query("SELECT colorschemes_id FROM `".DB_PREFIX."ubanner_colorschemes`");
            if ($query->num_rows == 0){
                $this->db->query("
                    INSERT INTO `".DB_PREFIX."ubanner_colorschemes` (`colorschemes_id`, `name`, `border`, `title`, `title_background`, `background`, `text`, `link`, `link_hover`, `link_bottom`, `footer_background`, `link_footer`) VALUES
                    (1, 'Light Grey', '#C1C5C8', '#484A4D', '#EAECEF', '#F7F8F8', '#000000', '#3B60AF', '#D91179', '#0A2D76', '#EAECEF', '#4D5059'),
                    (2, 'Light Warm Yellow', '#F0C082', '#633B1B', '#F0D5A6', '#FBF2E6', '#000000', '#A25018', '#D11919', '#633B1B', '#F0D5A6', '#633B1B'),
                    (3, 'Light Green Blue', '#ACC0C7', '#394A59', '#ACC0C7', '#F0F5F7', '#000000', '#2A5375', '#D41313', '#000000', '#ACC0C7', '#394A59'),
                    (4, 'Light Green', '#A3D869', '#235937', '#BCE08A', '#F1F9F2', '#000000', '#009A3D', '#235937', '#005F26', '#BCE08A', '#235937'),
                    (5, 'Light Grey Blue', '#729ABD', '#002859', '#92B1CD', '#E7F0F8', '#000000', '#20558A', '#5381AC', '#103A64', '#92B1CD', '#002859')");
            }
            $query = $this->db->query("
                CREATE TABLE IF NOT EXISTS `".DB_PREFIX."ubanner_layouts` (
                  `layouts_id` int(11) NOT NULL auto_increment,
                  `name` varchar(255) NOT NULL,
                  `width` varchar(10) NOT NULL,
                  `height` varchar(10) NOT NULL,
                  `onpage` varchar(10) NOT NULL,
                  `item_width` varchar(10) NOT NULL,
                  `image` varchar(10) NOT NULL,
                  `WidgetBottomLink_margin` varchar(20) NOT NULL,
                  `pr_i_des_margin_left` varchar(20) NOT NULL,
                  `WidgetData_height` varchar(20) NOT NULL,
                  `pr_i_height` varchar(20) NOT NULL,
                  `description_length` varchar(10) NOT NULL,
                  `shopnamelength` int(11) NOT NULL,
                  PRIMARY KEY  (`layouts_id`)
                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8  AUTO_INCREMENT=12");
            $query  = $this->db->query("SELECT layouts_id FROM `".DB_PREFIX."ubanner_layouts`");
            if ($query->num_rows == 0){
                $this->db->query("
                    INSERT INTO `".DB_PREFIX."ubanner_layouts` (`layouts_id`, `name`, `width`, `height`, `onpage`, `item_width`, `image`, `WidgetBottomLink_margin`, `pr_i_des_margin_left`, `WidgetData_height`, `pr_i_height`, `description_length`, `shopnamelength`) VALUES
                    (1, 'Leaderboard (798 x 102)', '796px', '100px', '4', '199px', '48px', '1px 12px 3px 12px', '53px', '48px', 'inherit', '50', 50),
                    (2, 'Banner (468 x 98)', '468px', '98px', '2', '234px', '46px', '1px 12px 3px 12px', '53px', '48px', 'inherit', '50', 50),
                    (3, 'Banner (468 x 98)', '234px', '98px', '1', '234px', '46px', '1px 5px 3px', '53px', '48px', 'inherit', '50', 20),
                    (4, 'Skyscraper (120 x 600)', '120px', '600px', '3', '120px', '112px', '1px 5px 3px', '0px', '530px', '175px', '50', 12),
                    (5, 'Wide Skyscraper (160 x 600)', '160px', '600px', '8', '160px', '45px', '1px 5px 3px', '0px', '530px', '66px', '50', 20),
                    (6, 'Small Rectangle (180 x 150)', '180px', '150px', '1', '180px', '90px', '1px 5px 3px', '0px', '95px', '90px', '80', 10),
                    (7, 'Vertical Banner (120 x 240)', '120px', '240px', '1', '120px', '112px', '1px 5px 3px', '0px', '210px', '175px', '50', 12),
                    (8, 'Small Square (200 x 200)', '200px', '200px', '2', '200px', '65px', '1px 5px 3px', '0px', '145px', '72px', '80', 15),
                    (9, 'Square (250 x 250)', '250px', '250px', '3', '250px', '60px', '1px 5px 3px', '0px', '200px', '66px', '100', 20),
                    (10, 'Medium Rectangle (300 x 250)', '300px', '250px', '3', '300px', '60px', '1px 5px 3px', '0px', '200px', '66px', '120', 20),
                    (11, 'Large Rectangle (336 x 280)', '336px', '280px', '3', '336px', '70px', '1px 5px 3px', '0px', '226px', '75px', '200', 20)");
            }
        }        
    }  
    
	public function getPath($category_id) {
		$query = $this->db->query("SELECT name, parent_id FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC");
		
		if ($query->row['parent_id']) {
			return $this->getPath($query->row['parent_id'], $this->config->get('config_language_id')) . $this->language->get('text_separator') . $query->row['name'];
		} else {
			return $query->row['name'];
		}
	}    
}