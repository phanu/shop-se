<?php

class ModelReportLowStockReport extends Model {

	public function checkLowStock($products) {
		$product_data = array();
		foreach ($products as $product) {
			$reorder = false;
			$option_data = array();
			$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product['product_id'] . "'");
			$order_id = $product['order_id'];
			$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$product['order_id'] . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");
			if ($order_option_query->num_rows) {
				foreach ($order_option_query->rows as $option) {
					$product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "'");
					foreach ($product_option_value_query->rows as $option_value) {
						if ($option_value['quantity'] < 0 || $option_value['quantity'] <= $option_value['reorder_alarm']) {
							$reorder = true;
							if ($option['type'] != 'file') {
								$option_name = $option['name'] . " : " . $option['value'];
							} else {
								$option_name = $option['name'] . " : " . utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
							}
							$quantity = $option_value['quantity'];
							$order_quantity = $product['quantity'];
							$reorder_quantity = $option_value['reorder_quantity'];
							$product_name = $product['name'];
							$product_model = $product['model'];
						}
					}
				}
			} else {
				foreach ($product_query->rows as $row) {
					if ($row['quantity'] < 0 || $row['quantity'] <= $row['reorder_alarm']) {
						$reorder = true;
						$option_name = "";
						$quantity = $row['quantity'];
						$order_quantity = $product['quantity'];
						$reorder_quantity = $row['reorder_quantity'];
						$product_name = $product['name'];
						$product_model = $product['model'];
					}
				}
			}
			if ($reorder == true) {
				$product_data[] = array(
					'name'			=> $product_name,
					'model'			=> $product_model,
					'option_name'	=> $option_name,
					'quantity'		=> $quantity,
					'order_qty'		=> $order_quantity,
					'reorder_qty'	=> $reorder_quantity
				);
			}
		}
		if (!empty($product_data)) {
			$this->language->load('report/low_stock_report');
			$html = "<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01//EN' 'http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd'>";
			$html .= "<html>";
			$html .= "<head>";
			$html .= "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>";
			$html .= "<title>" . $this->language->get('text_low_stock_heading') . "</title>";
			$html .= "<style type='text/css'>";
			$html .= "body {";
			$html .= "	color: #000000;";
			$html .= "	font-family: Arial, Helvetica, sans-serif;";
			$html .= "}";
			$html .= "body, td, th, input, textarea, select, a {";
			$html .= "	font-size: 12px;";
			$html .= "}";
			$html .= "#container {";
			$html .= "	width: 680px;";
			$html .= "}";
			$html .= "table.list {";
			$html .= "	border-collapse: collapse;";
			$html .= "	width: 100%;";
			$html .= "	border-top: 1px solid #DDDDDD;";
			$html .= "	border-left: 1px solid #DDDDDD;";
			$html .= "	margin-bottom: 20px;";
			$html .= "}";
			$html .= "table.list td {";
			$html .= "	border-right: 1px solid #DDDDDD;";
			$html .= "	border-bottom: 1px solid #DDDDDD;";
			$html .= "}";
			$html .= "table.list thead td {";
			$html .= "	background-color: #EFEFEF;";
			$html .= "	padding: 0px 5px;";
			$html .= "}";
			$html .= "table.list thead td a, .list thead td {";
			$html .= "	text-decoration: none;";
			$html .= "	color: #222222;";
			$html .= "	font-weight: bold;";
			$html .= "}";
			$html .= "table.list tbody td a {";
			$html .= "	text-decoration: underline;";
			$html .= "}";
			$html .= "table.list tbody td {";
			$html .= "	vertical-align: top;";
			$html .= "	padding: 0px 5px;";
			$html .= "}";
			$html .= "table.list .left {";
			$html .= "	text-align: left;";
			$html .= "	padding: 7px;";
			$html .= "}";
			$html .= "table.list .right {";
			$html .= "	text-align: right;";
			$html .= "	padding: 7px;";
			$html .= "}";
			$html .= "table.list .center {";
			$html .= "	text-align: center;";
			$html .= "	padding: 7px;";
			$html .= "}";
			$html .= "</style>";
			$html .= "</head>";
			$html .= "<body style='font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;'>";
			$html .= "<div id='container'>";
			$html .= "<div style='width: 100%; margin-bottom: 30px;'>" . $this->language->get('text_message') . "</div>";
			$html .= "<table class='list'>";
			$html .= "<thead>";
			$html .= "<tr>";
			$html .= "<td class='left'>" . $this->language->get('column_name') . "</td>";
			$html .= "<td class='left'>" . $this->language->get('column_options') . "</td>";
			$html .= "<td class='left'>" . $this->language->get('column_model') . "</td>";
			$html .= "<td class='center'>" . $this->language->get('column_qty') . "</td>";
			$html .= "<td class='center'>" . $this->language->get('column_ordered_qty') . "</td>";
			$html .= "<td class='center'>" . $this->language->get('column_reorder_qty') . "</td>";
			$html .= "</tr>";
			$html .= "</thead>";
			$html .= "<tbody>";
			foreach ($product_data as $data) {
				$html .= "<tr>";
				$html .= "<td class='left'>" . $data['name'] . "</td>";
				$html .= "<td class='left'>" . $data['option_name'] . "</td>";
				$html .= "<td class='left'>" . $data['model'] . "</td>";
				$html .= "<td class='center'>" . $data['quantity'] . "</td>";
				$html .= "<td class='center'>" . $data['order_qty'] . "</td>";
				$html .= "<td class='center'>" . $data['reorder_qty'] . "</td>";
				$html .= "</tr>";
			}
			$html .= "</tbody>";
			$html .= "</table>";
			$html .= "</div>";
			$html .= "</body>";
			$html .= "</html>";
			$subject = sprintf($this->language->get('text_subject'), $order_id);
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($html);
			if ($this->config->get('low_stock_report_emails')) {
				$emails = explode(",", $this->config->get('low_stock_report_emails'));
				foreach ($emails as $email) {
					$this->log->write($email);
					if ($email && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
						$this->log->write('sent');
						$mail->setTo($email);
						$mail->send();
					}
				}
			} else {
				$mail->setTo($this->config->get('config_email'));
				$mail->send();
			}
		}
		return;
	}
	
}

?>