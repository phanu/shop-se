<?php
/*
	Project: Custom Shipping Methods
	Author : karapuz <support@ka-station.com>

	Version: 1 ($Revision: 23 $)
*/
class ModelSettingKaSMethods extends Model {
	
	public function getMethods($data = array()) {
	
		$store_id     = (int)$this->config->get('config_store_id');
		$language_id  = (int)$this->config->get("config_language_id");

		$where = "status = 1 AND language_id = '$language_id'";
		
		if (isset($data['shipping_method_id'])) {
			$where .= " AND ksm.shipping_method_id = " . (int)$data['shipping_method_id'];
		}
			
		if ($this->customer->isLogged()) {
			$customer_group_id = (int)$this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = (int)$this->config->get('config_customer_group_id');
		}
		
		$query = $this->db->query("SELECT ksm.*, ksmd.* FROM " . DB_PREFIX . "ka_shipping_methods ksm
			INNER JOIN " . DB_PREFIX . "ka_shipping_methods_description ksmd ON
				ksm.shipping_method_id = ksmd.shipping_method_id
			INNER JOIN " . DB_PREFIX . "ka_shipping_methods_to_stores ksmts ON
				ksm.shipping_method_id = ksmts.shipping_method_id
				AND ksmts.store_id = $store_id
			INNER JOIN " . DB_PREFIX . "ka_shipping_methods_to_customer_groups ksmtcg ON
				ksm.shipping_method_id = ksmtcg.shipping_method_id
				AND ksmtcg.customer_group_id = $customer_group_id
			WHERE $where
			"		
		);
		
		$store_data = $query->rows;
	 
		return $store_data;
	}
	
}

?>