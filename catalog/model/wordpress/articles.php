<?php
class ModelWordPressArticles extends Model {
	
	public function getArticles($limit) {
		$query = $this->wpdb->query("SELECT * FROM " . DBWP_PREFIX . "posts where post_status =  'publish' order by ID DESC LIMIT " . (int)$limit );
		return $query->rows;
	}
	
}
?>