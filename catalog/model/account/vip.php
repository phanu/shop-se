<?php
class ModelAccountVip extends Model {
  public function getCustomerVip($customer_id) {
    $language_id = (int)$this->config->get("config_language_id");

    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer c LEFT JOIN " . DB_PREFIX . "vip_msg vm ON c.vip_id=vm.vip_id LEFT JOIN " . DB_PREFIX . "vip_level vl ON vm.vip_id=vl.vip_id WHERE c.customer_id='" . (int)$customer_id. "' AND vm.language_id='" . (int)$this->config->get("config_language_id") . "'");

    if (!empty($query->row["spending"])) {
      $customer_vip["name"] = $query->row["name"];
      $customer_vip["spending"] = $query->row["spending"];
      $customer_vip["discount"] = $query->row["discount"];
      $customer_vip["image"] = $query->row["image"];
      }
    else $customer_vip["spending"] = 0;

    $daterange = $this->getDateRange($this->config->get("vip_type"));
    $datequery = empty($daterange) ? 1 : "o.date_added BETWEEN '" . $daterange[2] . "' AND '" . $daterange[3] . "'";

    $orderquery = "ot.code='sub_total'";
    if ($this->config->get("vip_shipping")) $orderquery .= " OR ot.code='shipping'";
    if ($this->config->get("vip_tax")) $orderquery .= " OR ot.code='tax'";
    if ($this->config->get("vip_credit")) $orderquery .= " OR ot.code='credit'";
    if ($this->config->get("vip_reward")) $orderquery .= " OR ot.code='reward'";

    $query = $this->db->query("SELECT SUM(ot.value) AS total FROM " . DB_PREFIX . "order_total ot JOIN `" . DB_PREFIX . "order` o ON ot.order_id=o.order_id JOIN " . DB_PREFIX . "setting s ON s.value=o.order_status_id WHERE o.customer_id='" . (int)$customer_id . "' AND s.key='config_complete_status_id' AND ($orderquery) AND ($datequery)");

    $vip_init = $this->db->query("SELECT vip_init FROM " . DB_PREFIX . "customer WHERE customer_id='" . (int)$customer_id . "'")->row["vip_init"];

    $ordertotal = ($query->row["total"]) ? $query->row["total"] + $vip_init : $vip_init;

    $query = $this->db->query("SELECT vm.name, vl.discount, (vl.spending-$ordertotal) AS amt_needed FROM " . DB_PREFIX . "vip_level vl JOIN " . DB_PREFIX . "vip_msg vm ON vl.vip_id=vm.vip_id WHERE vm.language_id=$language_id AND vl.spending>$ordertotal ORDER BY spending ASC LIMIT 1");

    if ($query->num_rows) {
      $customer_vip["next_name"] = $query->row["name"];
      $customer_vip["next_discount"] = $query->row["discount"];
      $customer_vip["amt_needed"] = $query->row["amt_needed"];
      }

    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vip_msg WHERE vip_id=0 AND language_id=$language_id");
    $customer_vip["level_msg"] = $query->row["level_msg"];
    $customer_vip["discount_msg"] = $query->row["discount_msg"];
    $customer_vip["next_msg"] = $query->row["next_msg"];
    $customer_vip["highest_msg"] = $query->row["highest_msg"];

    return $customer_vip;
    }

  public function getVipDiscount($customer_id) {
    $query = $this->db->query("SELECT vl.discount FROM " . DB_PREFIX . "customer c JOIN " . DB_PREFIX . "vip_level vl ON c.vip_id=vl.vip_id WHERE c.customer_id='" . (int)$customer_id. "' AND vl.discount>0");

    if (empty($query->row["discount"])) return false;
    else return $query->row["discount"];
    }

  public function setCustomerVip($customer_id, $data) {
    $daterange = $this->getDateRange($data["vip_type"]);
    $datequery = empty($daterange) ? 1 : "o.date_added BETWEEN '" . $daterange[0] . "' AND '" . $daterange[1] . "'";

    $orderquery = "ot.code='sub_total'";
    if ($this->config->get("vip_shipping")) $orderquery .= " OR ot.code='shipping'";
    if ($this->config->get("vip_tax")) $orderquery .= " OR ot.code='tax'";
    if ($this->config->get("vip_credit")) $orderquery .= " OR ot.code='credit'";
    if ($this->config->get("vip_reward")) $orderquery .= " OR ot.code='reward'";

    $query = $this->db->query("SELECT SUM(ot.value) AS total FROM " . DB_PREFIX . "order_total ot JOIN `" . DB_PREFIX . "order` o ON ot.order_id=o.order_id JOIN " . DB_PREFIX . "setting s ON s.value=o.order_status_id WHERE o.customer_id='" . (int)$customer_id . "' AND s.key='config_complete_status_id' AND ($orderquery) AND ($datequery)");

    $vip_init = $this->db->query("SELECT vip_init FROM " . DB_PREFIX . "customer WHERE customer_id='" . (int)$customer_id . "'")->row["vip_init"];

    $ordertotal = ($query->row["total"]) ? $query->row["total"] + $vip_init : $vip_init;

    $query = $this->db->query("SELECT vip_id FROM " . DB_PREFIX . "vip_level WHERE spending<=$ordertotal ORDER BY spending DESC LIMIT 1");
    $customer_vip_id = ($query->num_rows) ? $query->row["vip_id"] : 0;

    $this->db->query("UPDATE " . DB_PREFIX . "customer SET vip_id='" . (int)$customer_vip_id . "', vip_total='" . (float)$ordertotal . "' WHERE customer_id='" . (int)$customer_id . "'");
    }

  private function getDateRange($type) {
    $daterange = array();

    if ($type == "monthly") {
      $daterange[0] = date("Y-m-d", strtotime("first day of last month")) . " 00:00:00";
      $daterange[1] = date("Y-m-d", strtotime("last day of last month")) . " 23:59:59";
      $daterange[2] = date("Y-m-d", strtotime("first day of this month")) . " 00:00:00";
      $daterange[3] = date("Y-m-d", strtotime("last day of this month")) . " 23:59:59";
      }
    elseif ($type == "quarterly") {
      $q0 = ceil(date("m")/3);
      if ($q0 == 1) {
        $daterange[0] = date("Y")-1 . "-10-01 00:00:00";
        $daterange[1] = date("Y")-1 . "-12-31 23:59:59";
        $daterange[2] = date("Y") . "-01-01 00:00:00";
        $daterange[3] = date("Y") . "-03-31 23:59:59";
        }
      elseif ($q0 == 2) {
        $daterange[0] = date("Y") . "-01-01 00:00:00";
        $daterange[1] = date("Y") . "-03-31 23:59:59";
        $daterange[2] = date("Y") . "-04-01 00:00:00";
        $daterange[3] = date("Y") . "-06-31 23:59:59";
        }
      elseif ($q0 == 3) {
        $daterange[0] = date("Y") . "-04-01 00:00:00";
        $daterange[1] = date("Y") . "-06-31 23:59:59";
        $daterange[2] = date("Y") . "-07-01 00:00:00";
        $daterange[3] = date("Y") . "-09-31 23:59:59";
        }
      elseif ($q0 == 4) {
        $daterange[0] = date("Y") . "-07-01 00:00:00";
        $daterange[1] = date("Y") . "-09-31 23:59:59";
        $daterange[2] = date("Y") . "-10-01 00:00:00";
        $daterange[3] = date("Y") . "-12-31 23:59:59";
        }
      }

    return $daterange;
    }

  public function getVipName($customer_id) {
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer c JOIN " . DB_PREFIX . "vip_msg vm ON c.vip_id=vm.vip_id WHERE c.customer_id='" . (int)$customer_id. "' AND vm.language_id='" . (int)$this->config->get("config_language_id") . "'");

    return (empty($query->row["name"]) ? "None" :  $query->row["name"]);
    }

  public function getVipTop() {
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vip_msg vm JOIN " . DB_PREFIX . "vip_level vl ON vm.vip_id=vl.vip_id WHERE vm.language_id='" . (int)$this->config->get("config_language_id") . "' ORDER BY vl.spending DESC LIMIT 1");

    return $query->rows;
    }

  }
?>
