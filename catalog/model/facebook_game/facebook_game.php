<?php
class ModelFacebookGameFacebookGame extends Model {

	public function addCustomer($data) {
      	$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET store_id = '" . (int)$this->config->get('config_store_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', password = '" . $this->db->escape(md5($data['password'])) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "', status = '1', date_added = NOW()");
      	
		$customer_id = $this->db->getLastId();
			
      	$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "'");
		
		$address_id = $this->db->getLastId();

      	$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
		
		if (!$this->config->get('config_customer_approval')) {
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET approved = '1' WHERE customer_id = '" . (int)$customer_id . "'");
		}	
		
		$this->language->load('mail/facebook_game_user');
		
		$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));
		
		$message = $this->language->get('text_welcome') . "\n\n";
		
		if (!$this->config->get('config_customer_approval')) {
			$message .= $this->language->get('text_login') . "\n\n";
		} else {
			$message .= $this->language->get('text_approval') . "\n\n";
		}
		
		$message .= sprintf($this->language->get('text_login_url'), $this->url->link('account/login', '', 'SSL')) . "\n";
		$message .= sprintf($this->language->get('text_login_data'), $data['email'], $data['password']) . "\n\n";
		
		$message .= $this->language->get('text_thanks') . "\n";
		$message .= $this->config->get('config_name');
		
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');				
		$mail->setTo($data['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject($subject);
		$mail->setText($message);
		$mail->send();
		
		// Send to main admin email if new account email is enabled
		if ($this->config->get('config_account_mail')) {
			$mail->setTo($this->config->get('config_email'));
			$mail->send();
			
			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_emails'));
			
			foreach ($emails as $email) {
				if (strlen($email) > 0 && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}
	}
	
	public function getTotalCustomersByEmail($email){
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE email='" . $this->db->escape($email) . "'";
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	public function getCustomerIDByEmail($email){
		$sql = "SELECT customer_id FROM " . DB_PREFIX . "customer WHERE email='" . $this->db->escape($email) . "'";

		$query = $this->db->query($sql);

		if ($query->num_rows){
			return $query->row['customer_id'];
		} 

		return 0;
	}
	
	public function move2Point($level, $moves){
		$point_scale = 1000;
		
		return round( ($level * $point_scale) / $moves );
	}
	
	public function addPoints($customer_id, $data){
		$points = $this->move2Point($data['level'], $data['moves']); 
		
		$sql = "INSERT INTO " . DB_PREFIX . "fg_user_point 
				SET `customer_id` ='" . (int)$customer_id . "',
					`fb_id`       ='" . $this->db->escape($data['id']) . "',
					`year`        = YEAR(NOW()), 
					`month`       = MONTH(NOW()),
					`day`         = DAY(NOW()),
					`week`        = WEEK(NOW(), " . $this->config->get('facebook_game_week_start') . "),
					`points`      ='" . (int)$points . "'"; 
					
		$this->db->query($sql);			
	}
	
	public function getUserPointsLastGame($customer_id){
		$sql = "SELECT points FROM " . DB_PREFIX . "fg_user_point 
				WHERE `customer_id`='" . $customer_id . "' ORDER BY user_point_id DESC LIMIT 0,1";
				
		$query = $this->db->query($sql);

		return $query->row['points'];	
	}
	
	public function getUserPointsToday($customer_id){
		$sql = "SELECT SUM(points) AS points FROM " . DB_PREFIX . "fg_user_point 
				WHERE `customer_id`='" . $customer_id . "' AND `year`= YEAR(NOW()) AND `month`= MONTH(NOW()) AND `day`= DAY(NOW())";
				
		$query = $this->db->query($sql);

		return $query->row['points'];	
	}
	
	public function getUserPointsThisWeek($customer_id){
		$sql = "SELECT SUM(points) AS points FROM " . DB_PREFIX . "fg_user_point 
				WHERE `customer_id`='" . $customer_id . "' AND `year`= YEAR(NOW()) AND `week`= WEEK(NOW())";
				
		$query = $this->db->query($sql);

		return $query->row['points'];	
	}
	
	public function getUserPointsThisMonth($customer_id){
		$sql = "SELECT SUM(points) AS points FROM " . DB_PREFIX . "fg_user_point 
				WHERE `customer_id`='" . $customer_id . "' AND `year`= YEAR(NOW()) AND `month`= MONTH(NOW())";
				
		$query = $this->db->query($sql);

		return $query->row['points'];	
	}
	
	public function generateRandomPassword( $length = 8){
		$password = "";
		$start = 97; // ascii code for a
		$stop  = 122; // ascii code for z
		
		while (strlen ($password) != $length ){
			$password.= chr(rand($start, $stop));
		}
			
		return $password;
	}
	
	public function getRanksToday($return_winner = false){
		$sql = "SELECT CONCAT(c.firstname, ' ', c.lastname) as name, c.customer_id, SUM(fgup.points) AS points, fgup.fb_id FROM " . DB_PREFIX . "fg_user_point fgup
				LEFT JOIN " . DB_PREFIX . "customer c ON (fgup.customer_id = c.customer_id)
				WHERE `year`= YEAR(NOW()) AND `month`= MONTH(NOW()) AND `day`= DAY(NOW()) 
				GROUP BY fgup.fb_id 
				ORDER BY SUM(fgup.points) DESC ";
		
		if ($return_winner){
			$sql .= " LIMIT 0,1";
		}
		
		$query = $this->db->query($sql);
		
		if ($return_winner){
			return $query->row;
		} else {	
			return $query->rows;	
		}	
	}
	
	public function getRanksWeek($return_winner = false){
		$sql = "SELECT CONCAT(c.firstname, ' ', c.lastname) as name, c.customer_id, SUM(fgup.points) AS points, fgup.fb_id FROM " . DB_PREFIX . "fg_user_point fgup
				LEFT JOIN " . DB_PREFIX . "customer c ON (fgup.customer_id = c.customer_id)
				WHERE `year`= YEAR(NOW()) AND `week`= WEEK(NOW())
				GROUP BY fgup.fb_id 
				ORDER BY SUM(fgup.points) DESC ";
		
		if ($return_winner){
			$sql .= " LIMIT 0,1";
		}	
			
		$query = $this->db->query($sql);

		if ($return_winner){
			return $query->row;
		} else {	
			return $query->rows;	
		}	
	}
	
	public function getRanksMonth($return_winner = false){
		$sql = "SELECT CONCAT(c.firstname, ' ', c.lastname) as name, c.customer_id, SUM(fgup.points) AS points, fgup.fb_id FROM " . DB_PREFIX . "fg_user_point fgup
				LEFT JOIN " . DB_PREFIX . "customer c ON (fgup.customer_id = c.customer_id)
				WHERE `year`= YEAR(NOW()) AND `month`= MONTH(NOW()) 
				GROUP BY fgup.fb_id 
				ORDER BY SUM(fgup.points) DESC ";
		
		if ($return_winner){
			$sql .= " LIMIT 0,1";
		}		
		
		$query = $this->db->query($sql);

		if ($return_winner){
			return $query->row;
		} else {	
			return $query->rows;	
		}	
	}
	
	
	// --- CRON FUNCTIONS ---
	
	public function isLastDayOfWeek(){
		if ($this->config->get('facebook_game_week_start') == 1) { // week start on Monday
			$lastDayIndex = 1; //Sunday
		} else {  // week start Sunday
			$lastDayIndex = 7; //Saturday
		}
		
		$query = $this->db->query("SELECT DAYOFWEEK(NOW()) AS day_index");
		
		if ($query->row['day_index'] == $lastDayIndex) {
			return true;
		}
		
		return false;
	}
	
	public function isLastDayOfMonth(){
		$query = $this->db->query("SELECT SUBSTR(LAST_DAY(NOW()),9,2) AS last_day, DAYOFMONTH(NOW()) AS today");
		
		if ($query->row['last_day'] == $query->row['today']) {
			return true;
		}
		
		return false;
	}
	
	public function getCustomer($customer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
		
		return $query->row;
	}
	
	public function addTransaction($customer_id, $description = '', $amount = '', $order_id = 0) {
		$customer_info = $this->getCustomer($customer_id);
		
		if ($customer_info) { 
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$customer_id . "', order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($description) . "', amount = '" . (float)$amount . "', date_added = NOW()");

			$this->language->load('mail/facebook_game_user');
			$store_name = $this->config->get('config_name');
					
			$message  = sprintf($this->language->get('text_transaction_received'), $this->currency->format($amount, $this->config->get('config_currency'))) . "\n\n";
			$message .= sprintf($this->language->get('text_transaction_total'), $this->currency->format($this->getTransactionTotal($customer_id)));
								
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');
			$mail->setTo($customer_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($store_name);
			$mail->setSubject($description);
			$mail->setText($message);
			$mail->send();
		}
	}
	
	public function getTransactionTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "'");
	
		return $query->row['total'];
	}
}
?>