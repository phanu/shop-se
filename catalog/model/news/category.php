<?php
class ModelNewsCategory extends Model {
	public function getCategory($article_category_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "news_category c 
		LEFT JOIN " . DB_PREFIX . "news_category_description cd ON (c.article_category_id = cd.article_category_id) 
		WHERE c.article_category_id = '" . (int)$article_category_id . "' 
		AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
		AND c.status = '1'");		
		return $query->row;
	}
	
	public function getCategories($parent_id = 0) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_category c 
		LEFT JOIN " . DB_PREFIX . "news_category_description cd ON (c.article_category_id = cd.article_category_id) 		
		WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
		AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

		return $query->rows;
	}
		
	public function getCategoriesByParentId($article_category_id) {
		$category_data = array();

		$category_query = $this->db->query("SELECT article_category_id FROM " . DB_PREFIX . "news_category WHERE parent_id = '" . (int)$article_category_id . "'");

		foreach ($category_query->rows as $category) {
			$category_data[] = $category['article_category_id'];

			$children = $this->getCategoriesByParentId($category['article_category_id']);

			if ($children) {
				$category_data = array_merge($children, $category_data);
			}			
		}

		return $category_data;
	}
			
	public function getCategoryLayoutId($article_category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_category_to_layout WHERE article_category_id = '" . (int)$article_category_id . "'");
		
		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return $this->config->get('config_layout_category');
		}
	}
					
	public function getTotalCategoriesByCategoryId($parent_id = 0) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_category c 
		WHERE c.parent_id = '" . (int)$parent_id . "' AND c.status = '1'");
		
		return $query->row['total'];
	}
}
?>