<?php
class ModelNewsManager extends Model {
	public function addArticle($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "news_article SET 
		author_id = '" . (int)$this->author->getId(). "', 
		 new_category = '" . $this->db->escape($data['new_category']). "', 
		 reference_url = '" . $this->db->escape($data['reference_url']). "', 
		 reference_title = '" . $this->db->escape($data['reference_title']). "', 
		 status = '" . (int)$this->config->get('config_author_article_status') . "', 
		 approved = '" . (int)$this->config->get('config_author_article_approved'). "', 
		 date_added = NOW()");
		
		$article_id = $this->db->getLastId();
		
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "news_article SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE article_id = '" . (int)$article_id . "'");
		}
		
		foreach ($data['article_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "news_article_description SET 
			article_id = '" . (int)$article_id . "',
			language_id = '" . (int)$language_id . "',
			name = '" . $this->db->escape($value['name']) . "',
			meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "',
			meta_description = '" . $this->db->escape($value['meta_description']) . "',
			description = '" . $this->db->escape($value['description']) . "',
			tag = '" . $this->db->escape($value['tag']) . "'");
		}
		
		if (isset($data['article_store'])) {
			foreach ($data['article_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "news_article_to_store SET article_id = '" . (int)$article_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		if (isset($data['article_image'])) {
			foreach ($data['article_image'] as $article_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "news_article_image SET article_id = '" . (int)$article_id . "', image = '" . $this->db->escape(html_entity_decode($article_image['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$article_image['sort_order'] . "'");
			}
		}
		
		if (isset($data['article_download'])) {
			foreach ($data['article_download'] as $download_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "news_article_to_download SET article_id = '" . (int)$article_id . "', download_id = '" . (int)$download_id . "'");
			}
		}
		
		if (isset($data['article_category'])) {
			foreach ($data['article_category'] as $article_category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "news_article_to_category SET article_id = '" . (int)$article_id . "', article_category_id = '" . (int)$article_category_id . "'");
			}
		}
		
		if (isset($data['related_article'])) {
			foreach ($data['related_article'] as $article_related_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "news_related_article WHERE article_id = '" . (int)$article_id . "' AND article_related_id = '" . (int)$article_related_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "news_related_article SET article_id = '" . (int)$article_id . "', article_related_id = '" . (int)$article_related_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "news_related_article WHERE article_id = '" . (int)$article_related_id . "' AND article_related_id = '" . (int)$article_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "news_related_article SET article_id = '" . (int)$article_related_id . "', article_related_id = '" . (int)$article_id . "'");
			}
		}
		
		if (isset($data['related_product'])) {
			foreach ($data['related_product'] as $product_related_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "news_related_product WHERE article_id = '" . (int)$article_id . "' AND product_related_id = '" . (int)$product_related_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "news_related_product SET article_id = '" . (int)$article_id . "', product_related_id = '" . (int)$product_related_id . "'");				
			}
		}

		if (isset($data['article_layout'])) {
			foreach ($data['article_layout'] as $store_id => $layout) {
				if ($layout['layout_id']) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "news_article_to_layout SET article_id = '" . (int)$article_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout['layout_id'] . "'");
				}
			}
		}
						
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'article_id=" . (int)$article_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}						
		$this->cache->delete('article');
		$desc_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_article_description WHERE article_id = '" . (int)$article_id . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
		$author =$this->author->getAuthor();
		$this->language->load('mail/article');		
		$subject = $this->language->get('text_subject');		
		$message = $this->language->get('text_dear'). "\n\n";		
		$message .= sprintf($this->language->get('text_author'), $author) . "\n\n";			
		$message .= sprintf($this->language->get('text_title'), $desc_query->row['name']) . "\n\n";	
		$message .= $this->language->get('text_thanks') . "\n";
		
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_smtp_host');
		$mail->username = $this->config->get('config_smtp_username');
		$mail->password = $this->config->get('config_smtp_password');
		$mail->port = $this->config->get('config_smtp_port');
		$mail->timeout = $this->config->get('config_smtp_timeout');	
				
		// Send to main admin email if new account email is enabled
		if ($this->config->get('config_notification_mail')) {
			$mail->setTo($this->config->get('config_notification_email'));
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();
			
			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_emails'));
			
			foreach ($emails as $email) {
				if (strlen($email) > 0 && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}
	}
	
	public function editArticle($article_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "news_article SET 
		  status = '" . (int)$data['status'] . "', 
		  new_category = '" . $this->db->escape($data['new_category']). "', 
		 reference_url = '" . $this->db->escape($data['reference_url']). "', 
		 reference_title = '" . $this->db->escape($data['reference_title']). "', 
		  date_modified = NOW() 
		  WHERE article_id = '" . (int)$article_id . "'
		  AND author_id = '" . (int)$this->author->getId(). "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "news_article SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE article_id = '" . (int)$article_id . "'");
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_article_description WHERE article_id = '" . (int)$article_id . "'");
		
		foreach ($data['article_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "news_article_description SET 
			article_id = '" . (int)$article_id . "', 
			language_id = '" . (int)$language_id . "',
			name = '" . $this->db->escape($value['name']) . "',
			meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "',
			meta_description = '" . $this->db->escape($value['meta_description']) . "',
			description = '" . $this->db->escape($value['description']) . "',
			tag = '" . $this->db->escape($value['tag']) . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "news_article_to_store WHERE article_id = '" . (int)$article_id . "'");

		if (isset($data['article_store'])) {
			foreach ($data['article_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "news_article_to_store SET article_id = '" . (int)$article_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_article_image WHERE article_id = '" . (int)$article_id . "'");
		
		if (isset($data['article_image'])) {
			foreach ($data['article_image'] as $article_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "news_article_image SET article_id = '" . (int)$article_id . "', image = '" . $this->db->escape(html_entity_decode($article_image['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$article_image['sort_order'] . "'");
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_article_to_download WHERE article_id = '" . (int)$article_id . "'");
		
		if (isset($data['article_download'])) {
			foreach ($data['article_download'] as $download_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "news_article_to_download SET article_id = '" . (int)$article_id . "', download_id = '" . (int)$download_id . "'");
			}
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_article_to_category WHERE article_id = '" . (int)$article_id . "'");
		
		if (isset($data['article_category'])) {
			foreach ($data['article_category'] as $article_category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "news_article_to_category SET article_id = '" . (int)$article_id . "', article_category_id = '" . (int)$article_category_id . "'");
			}		
		}
		/*related article*/ 
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_related_article WHERE article_id = '" . (int)$article_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_related_article WHERE article_related_id = '" . (int)$article_id . "'");

		if (isset($data['related_article'])) {
			foreach ($data['related_article'] as $article_related_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "news_related_article WHERE article_id = '" . (int)$article_id . "' AND article_related_id = '" . (int)$article_related_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "news_related_article SET article_id = '" . (int)$article_id . "', article_related_id = '" . (int)$article_related_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "news_related_article WHERE article_id = '" . (int)$article_related_id . "' AND article_related_id = '" . (int)$article_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "news_related_article SET article_id = '" . (int)$article_related_id . "', article_related_id = '" . (int)$article_id . "'");
			}
		}		
		/*related article*/ 
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_related_product WHERE article_id = '" . (int)$article_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_related_product WHERE product_related_id = '" . (int)$article_id . "'");
		if (isset($data['related_product'])) {
			foreach ($data['related_product'] as $product_related_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "news_related_product WHERE article_id = '" . (int)$article_id . "' AND product_related_id = '" . (int)$product_related_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "news_related_product SET article_id = '" . (int)$article_id . "', product_related_id = '" . (int)$product_related_id . "'");				
			}
		}
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_article_to_layout WHERE article_id = '" . (int)$article_id . "'");

		if (isset($data['article_layout'])) {
			foreach ($data['article_layout'] as $store_id => $layout) {
				if ($layout['layout_id']) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "news_article_to_layout SET article_id = '" . (int)$article_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout['layout_id'] . "'");
				}
			}
		}
						
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'article_id=" . (int)$article_id. "'");
		
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'article_id=" . (int)$article_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
						
		$this->cache->delete('article');
	}
	
	public function copyArticle($article_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "news_article p LEFT JOIN " . DB_PREFIX . "news_article_description pd ON (p.article_id = pd.article_id) WHERE p.article_id = '" . (int)$article_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		
		if ($query->num_rows) {
			$data = array();
			
			$data = $query->row;
			$data['viewed'] = '0';
			$data['keyword'] = '';
			$data['status'] = '0';
						
			$data = array_merge($data, array('article_description' => $this->getArticleDescriptions($article_id)));	
			$data = array_merge($data, array('article_image' => $this->getArticleImages($article_id)));		
			$data = array_merge($data, array('related_article' => $this->getArticleRelated($article_id)));
			$data = array_merge($data, array('related_product' => $this->getProductRelated($article_id)));
			$data = array_merge($data, array('article_category' => $this->getArticleCategories($article_id)));
			$data = array_merge($data, array('article_download' => $this->getArticleDownloads($article_id)));
			$data = array_merge($data, array('article_layout' => $this->getArticleLayouts($article_id)));
			$data = array_merge($data, array('article_store' => $this->getArticleStores($article_id)));
			
			$this->addArticle($data);
		}
	}
	
	public function deleteArticle($article_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_article WHERE article_id = '" . (int)$article_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_article_description WHERE article_id = '" . (int)$article_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_article_image WHERE article_id = '" . (int)$article_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_related_article WHERE article_id = '" . (int)$article_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_related_article WHERE article_related_id = '" . (int)$article_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_article_to_category WHERE article_id = '" . (int)$article_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_article_to_download WHERE article_id = '" . (int)$article_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_article_to_layout WHERE article_id = '" . (int)$article_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_article_to_store WHERE article_id = '" . (int)$article_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_comment WHERE article_id = '" . (int)$article_id . "'");
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'article_id=" . (int)$article_id. "'");
		
		$this->cache->delete('article');
	}
	
	public function getArticle($article_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias 
		WHERE query = 'article_id=" . (int)$article_id . "') AS keyword FROM " . DB_PREFIX . "news_article p 
		LEFT JOIN " . DB_PREFIX . "news_article_description pd ON (p.article_id = pd.article_id) 
		WHERE p.article_id = '" . (int)$article_id . "'  
		AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		//AND p.author_id = '" . (int)$this->author->getId(). "'  	
		return $query->row;
	}
	
	
	public function getArticles($data = array()) {
	$this->load->model('news/utf8');
		if ($data) {
			$sql = "SELECT * FROM ".DB_PREFIX."news_article p 
			LEFT JOIN ".DB_PREFIX."news_article_description pd ON (p.article_id = pd.article_id)";
			
		if (!empty($data['filter_article_category_id'])||!empty($data['filter_article_category'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "news_article_to_category p2c ON (p.article_id = p2c.article_id)";			
		}	
			$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.author_id = '" . $this->author->getId() . "'"; 
			
    		if(!empty($data['filter_name'])){
				$sql.=" AND LCASE(pd.name) LIKE '".$this->db->escape($this->model_news_utf8->utf8_strtolower($data['filter_name']))."%'";
			}
			//filter_category
			if (isset($data['filter_article_category']) && !is_null($data['filter_article_category'])) {
				$sql .= " AND p2c.article_category_id = '" . $this->db->escape($data['filter_article_category']) . "'";}
			
				//filter_date
			if (isset($data['filter_date']) && !is_null($data['filter_date'])) {
				$sql .= " AND p.date_added LIKE '" . $this->db->escape($data['filter_date']) . "%'";}
						
			if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
				$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
			}			
			if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
				$sql .= " AND p.approved = '" . (int)$data['filter_approved'] . "'";
			}
					
			if (!empty($data['filter_category_id'])) {
				if (!empty($data['filter_sub_category'])) {
					$implode_data = array();
					
					$implode_data[] = "category_id = '" . (int)$data['filter_category_id'] . "'";
					
					$this->load->model('news/category');
					
					$categories = $this->model_news_category->getCategories($data['filter_category_id']);
					
					foreach ($categories as $category) {
						$implode_data[] = "p2c.article_category_id = '" . (int)$category['category_id'] . "'";
					}
					
					$sql .= " AND (" . implode(' OR ', $implode_data) . ")";			
				} else {
					$sql .= " AND p2c.article_category_id = '" . (int)$data['filter_category_id'] . "'";
				}
			}
			
			$sql .= " GROUP BY p.article_id";
						
			$sort_data = array(
				'pd.name',
				'p.image',
				'p2c.article_category_id',
				'p.date_added',
				'p.status',
				'p.approved',
				'p.sort_order'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY pd.name";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
		
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}				

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}	
			
			$query = $this->db->query($sql);
		
			return $query->rows;
		} else {
			$article_data = $this->cache->get('article.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'));
		
			if (!$article_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_article p 
				LEFT JOIN " . DB_PREFIX . "news_article_description pd ON (p.article_id = pd.article_id) 
				WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
				AND p.author_id = '" . $this->author->getId() . "'				
				ORDER BY pd.name ASC");
	
				$article_data = $query->rows;
			
				$this->cache->set('article.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'), $article_data);
			}	
	
			return $article_data;
		}
	}
	
	public function getArticlesByCategoryId($article_category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_article p LEFT JOIN " . DB_PREFIX . "news_article_description pd ON (p.article_id = pd.article_id) LEFT JOIN " . DB_PREFIX . "news_article_to_category p2c ON (p.article_id = p2c.article_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.article_category_id = '" . (int)$article_category_id . "' ORDER BY pd.name ASC");
								  
		return $query->rows;
	} 
	
	public function getArticleDescriptions($article_id) {
		$article_description_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_article_description WHERE article_id = '" . (int)$article_id . "'");
		
		foreach ($query->rows as $result) {
			$article_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'sub_description'      => $result['sub_description'],
				'description'      => $result['description'],
				'meta_keyword'     => $result['meta_keyword'],
				'meta_description' => $result['meta_description'],
				'tag'              => $result['tag']
			);
		}
		
		return $article_description_data;
	}
	
	public function getArticleImages($article_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_article_image WHERE article_id = '" . (int)$article_id . "'");
		
		return $query->rows;
	}
	
			
	public function getArticleDownloads($article_id) {
		$article_download_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_article_to_download WHERE article_id = '" . (int)$article_id . "'");
		
		foreach ($query->rows as $result) {
			$article_download_data[] = $result['download_id'];
		}
		
		return $article_download_data;
	}

	public function getArticleStores($article_id) {
		$article_store_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_article_to_store WHERE article_id = '" . (int)$article_id . "'");

		foreach ($query->rows as $result) {
			$article_store_data[] = $result['store_id'];
		}
		
		return $article_store_data;
	}

	public function getArticleLayouts($article_id) {
		$article_layout_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_article_to_layout WHERE article_id = '" . (int)$article_id . "'");
		
		foreach ($query->rows as $result) {
			$article_layout_data[$result['store_id']] = $result['layout_id'];
		}
		
		return $article_layout_data;
	}
		
	public function getArticleCategories($article_id) {
		$article_category_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_article_to_category WHERE article_id = '" . (int)$article_id . "'");
		
		foreach ($query->rows as $result) {
			$article_category_data[] = $result['article_category_id'];
		}

		return $article_category_data;
	}

	public function getProductRelated($article_id) {
		$related_product_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_related_product WHERE article_id = '" . (int)$article_id . "'");
		
		foreach ($query->rows as $result) {
			$related_product_data[] = $result['product_related_id'];
		}
		
		return $related_product_data;
	}
	public function getArticleRelated($article_id) {
		$related_article_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_related_article WHERE article_id = '" . (int)$article_id . "'");
		
		foreach ($query->rows as $result) {
			$related_article_data[] = $result['article_related_id'];
		}
		
		return $related_article_data;
	}
	public function getTotalArticles($data = array()) {
	$this->load->model('news/utf8');
		$sql = "SELECT COUNT(DISTINCT p.article_id) AS total FROM " . DB_PREFIX . "news_article p LEFT JOIN " . DB_PREFIX . "news_article_description pd ON (p.article_id = pd.article_id)";

		if (!empty($data['filter_article_category_id'])||!empty($data['filter_article_category'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "news_article_to_category p2c ON (p.article_id = p2c.article_id)";			
		}
		 
		$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.author_id = '" . $this->author->getId() . "'";
		
		//filter_article_category
			if (isset($data['filter_article_category']) && !is_null($data['filter_article_category'])) {
				$sql .= " AND p2c.article_category_id = '" . $this->db->escape($data['filter_article_category']) . "'";}
		
		//filter_date
		if (isset($data['filter_date']) && !is_null($data['filter_date'])) {
				$sql .= " AND p.date_added LIKE '" . $this->db->escape($data['filter_date']) . "%'";}
								
		if (!empty($data['filter_name'])) {
			$sql .= " AND LCASE(pd.name) LIKE '" . $this->db->escape($this->model_news_utf8->utf8_strtolower($data['filter_name'])) . "%'";
		}
		
		if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
			$sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
		}
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_article_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$implode_data = array();
				
				$implode_data[] = "p2c.article_category_id = '" . (int)$data['filter_article_category_id'] . "'";
				
				$this->load->model('news/category');
				
				$categories = $this->model_news_category->getCategories($data['filter_article_category_id']);
				
				foreach ($categories as $category) {
					$implode_data[] = "p2c.article_category_id = '" . (int)$category['article_category_id'] . "'";
				}
				
				$sql .= " AND (" . implode(' OR ', $implode_data) . ")";			
			} else {
				$sql .= " AND p2c.article_category_id = '" . (int)$data['filter_article_category_id'] . "'";
			}
		}
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}	
	
	public function getTotalArticlesByDownloadId($download_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_article_to_download WHERE download_id = '" . (int)$download_id . "'");
		
		return $query->row['total'];
	}
	
	public function getTotalArticlesByAuthorId($author_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_article WHERE author_id = '" . (int)$this->author->getId(). "'");

		return $query->row['total'];
	}
	
	public function getTotalArticlesByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_article_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}
	public function getTotalArticle() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_article");
		
		return $query->row['total'];
	}	
	/*Category*/ 
	public function getCategories($parent_id = 0) {
		$category_data = $this->cache->get('article_category.' . (int)$this->config->get('config_language_id') . '.' . (int)$parent_id);
	
		if (!$category_data) {
			$category_data = array();
		
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_category c 
			LEFT JOIN " . DB_PREFIX . "news_category_description cd ON (c.article_category_id = cd.article_category_id) 
			WHERE c.parent_id = '" . (int)$parent_id . "' 
			AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
			GROUP BY c.parent_id, c.sort_order, cd.name ASC");
		
			foreach ($query->rows as $result) {			
				$total = $this->getTotalArticles(array('filter_article_category_id'  => $result['article_category_id']));
				$category_data[] = array(
					'article_category_id' => $result['article_category_id'],
					'name'        => $this->getPath($result['article_category_id'], $this->config->get('config_language_id')),
					'total'  	  =>' (' . $total . ')',	
					'status'  	  => $result['status'],
					'image'  	  => $result['image'],
					'sort_order'  => $result['sort_order']
				);
			
				$category_data = array_merge($category_data, $this->getCategories($result['article_category_id']));
			}	
	
			$this->cache->set('article_category.' . (int)$this->config->get('config_language_id') . '.' . (int)$parent_id, $category_data);
		}
		
		return $category_data;
	}
	
	public function getPath($article_category_id) {
		$query = $this->db->query("SELECT name, parent_id FROM " . DB_PREFIX . "news_category c LEFT JOIN " . DB_PREFIX . "news_category_description cd ON (c.article_category_id = cd.article_category_id) WHERE c.article_category_id = '" . (int)$article_category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC");
		
		if ($query->row['parent_id']) {
			return $this->getPath($query->row['parent_id'], $this->config->get('config_language_id')) . $this->language->get('text_separator') . $query->row['name'];
		} else {
			return $query->row['name'];
		}
	}
	/**/ 
	
	public function getProductsByCategoryId($category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) 
		WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
		AND p.status='1'
		AND p2c.category_id = '" . (int)$category_id . "' ORDER BY pd.name ASC");								  
		return $query->rows;
	} 
/*Product Category*/ 

	public function getProductCategories($parent_id = 0) {
			$product_category_data = array();
		
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c 
			LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) 
			WHERE c.parent_id = '" . (int)$parent_id . "' 
			AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
			GROUP BY c.parent_id, c.sort_order, cd.name ASC");
		
			foreach ($query->rows as $result) {			
				$total = $this->getTotalProducts(array('filter_category_id'  => $result['category_id']));
				$product_category_data[] = array(
					'category_id' => $result['category_id'],
					'name'        => $this->getProductPath($result['category_id'], $this->config->get('config_language_id')),
					'total'  	  =>'(' . $total . ')',	
					'status'  	  => $result['status'],
					'image'  	  => $result['image'],
					'sort_order'  => $result['sort_order']
				);
			
				$product_category_data = array_merge($product_category_data, $this->getProductCategories($result['category_id']));
			}	
	
		
		return $product_category_data;
	}
	
	public function getProductPath($category_id) {
		$query = $this->db->query("SELECT name, parent_id FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC");
		
		if ($query->row['parent_id']) {
			return $this->getProductPath($query->row['parent_id'], $this->config->get('config_language_id')) . $this->language->get('text_separator') . $query->row['name'];
		} else {
			return $query->row['name'];
		}
	}
	public function getTotalProducts($data = array()) {
	$this->load->model('news/utf8');
		$sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

		if (!empty($data['filter_category_id'])||!empty($data['filter_category'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";			
		}
		 
		$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		
		//filter_category
			if (isset($data['filter_category']) && !is_null($data['filter_category'])) {
				$sql .= " AND p2c.category_id = '" . $this->db->escape($data['filter_category']) . "'";}
		//filter_date
		if (isset($data['filter_date']) && !is_null($data['filter_date'])) {
				$sql .= " AND p.date_added LIKE '" . $this->db->escape($data['filter_date']) . "%'";}
								
		if (!empty($data['filter_name'])) {
			$sql .= " AND LCASE(pd.name) LIKE '" . $this->db->escape($this->model_news_utf8->utf8_strtolower($data['filter_name'])) . "%'";
		}
		
		if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
			$sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
		}
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$implode_data = array();
				
				$implode_data[] = "p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
				
				$this->load->model('catalog/category');
				
				$categories = $this->model_catalog_category->getCategories($data['filter_category_id']);
				
				foreach ($categories as $category) {
					$implode_data[] = "p2c.category_id = '" . (int)$category['category_id'] . "'";
				}
				
				$sql .= " AND (" . implode(' OR ', $implode_data) . ")";			
			} else {
				$sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
			}
		}
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}	
}
?>