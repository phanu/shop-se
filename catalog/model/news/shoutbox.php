<?php
class ModelNewsShoutbox extends Model {		
	public function addMessage( $data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "shoutbox SET author = '" . $this->db->escape($data['name']) . "', customer_id = '" . (int)$this->customer->getId() . "', message = '" . $this->db->escape($data['message']) . "', status = '1', date_added = NOW()");
	}
		
	public function getMessages($start = 0, $limit = 10) {
		$query = $this->db->query("SELECT shout_id, author, message, date_added FROM " . DB_PREFIX . "shoutbox WHERE status = '1' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);		
		return $query->rows;
	}
	public function getTotalMessages() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "shoutbox WHERE status = '1'");
		
		return $query->row['total'];
	}	
	public function clear_message() {
		$query = $this->db->query("DELETE FROM " . DB_PREFIX . "shoutbox WHERE status = '1'");		
	}
}
?>