<?php
class ModelNewsAuthor extends Model {		
	public function getAuthor($author_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_author WHERE author_id = '" . (int)$author_id . "'");
	
		return $query->row;	
	}
	
	public function editNewsletter($newsletter) {
		$this->db->query("UPDATE " . DB_PREFIX . "news_author SET newsletter = '" . (int)$newsletter . "' WHERE author_id = '" . (int)$this->author->getId() . "'");
	}
	public function getAuthorByToken($token) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_author WHERE token = '" . $this->db->escape($token) . "' AND token != ''");
		
		$this->db->query("UPDATE " . DB_PREFIX . "news_author SET token = ''");
		
		return $query->row;
	}
	public function getAuthors($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "news_author ";
			
			$sort_data = array(
				'author',
				'sort_order'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY author";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}				
					
			$query = $this->db->query($sql);
			
			return $query->rows;
		} else {
		
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_author ORDER BY author");
	
				$author_data = $query->rows;
			
		 
			return $author_data;
		}	
	} 
	/**/ 
	
	public function addAuthor($data) {		
      	$this->db->query("INSERT INTO " . DB_PREFIX . "news_author SET 
		store_id = '" . (int)$this->config->get('config_store_id') . "',
		author = '" . $this->db->escape($data['author']) . "',
		firstname = '" . $this->db->escape($data['firstname']) . "',
		lastname = '" . $this->db->escape($data['lastname']) . "',
		author_description = '" . $this->db->escape($data['author_description']) . "',
		email = '" . $this->db->escape($data['email']) . "',
		telephone = '" . $this->db->escape($data['telephone']) . "',
		website = '" . $this->db->escape($data['website']) . "',
		address = '" . $this->db->escape($data['address']) . "',
		city = '" . $this->db->escape($data['city']) . "',
		country_id = '" . (int)$data['country_id'] . "',
		zone_id = '" . (int)$data['zone_id'] . "',
		salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "',
		password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "',
		newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', 
		ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "',
		status = '" . $this->config->get('config_author_register_status'). "',
		approved = '" . $this->config->get('config_author_approved'). "', date_added = NOW()");
      	
		$author_id = $this->db->getLastId();
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "news_author SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE author_id = '" . (int)$author_id . "'");
		}
		
		
		$this->language->load('mail/author');
		$author_mail = new Mail();
		$author_mail->protocol = $this->config->get('config_mail_protocol');
		$author_mail->parameter = $this->config->get('config_mail_parameter');
		$author_mail->hostname = $this->config->get('config_smtp_host');
		$author_mail->username = $this->config->get('config_smtp_username');
		$author_mail->password = $this->config->get('config_smtp_password');
		$author_mail->port = $this->config->get('config_smtp_port');
		$author_mail->timeout = $this->config->get('config_smtp_timeout');		
		/*Author Email*/ 
		$author_subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));		
		$author_message = sprintf($this->language->get('text_welcome'), $this->config->get('config_name')) . "\n\n";		
		if ($this->config->get('config_author_register_status')==1&&$this->config->get('config_author_approved')==1) {
			$author_message .= $this->language->get('text_login') . "\n";
			$author_message .= $this->url->link('news/login', '', 'SSL') . "\n\n";
			$author_message .= $this->language->get('text_services') . "\n\n";
		} else {
			$author_message .= $this->language->get('text_approval') . "\n";
		}		
		$author_message .= $this->language->get('text_thanks') . "\n";
		$author_message .= $this->config->get('config_name');
				
		$author_mail->setTo($data['email']);
		$author_mail->setFrom($this->config->get('config_email'));
		$author_mail->setSender($this->config->get('config_name'));
		$author_mail->setSubject(html_entity_decode($author_subject, ENT_QUOTES, 'UTF-8'));
		$author_mail->setText(html_entity_decode($author_message, ENT_QUOTES, 'UTF-8'));
		$author_mail->send();
		
		// Send to main admin email if new account email is enabled
		
		$admin_mail = new Mail();
		$admin_mail->protocol = $this->config->get('config_mail_protocol');
		$admin_mail->parameter = $this->config->get('config_mail_parameter');
		$admin_mail->hostname = $this->config->get('config_smtp_host');
		$admin_mail->username = $this->config->get('config_smtp_username');
		$admin_mail->password = $this->config->get('config_smtp_password');
		$admin_mail->port = $this->config->get('config_smtp_port');
		$admin_mail->timeout = $this->config->get('config_smtp_timeout');	
		
		$admin_subject = sprintf($this->language->get('text_admin_subject'), $data['author']);		
		$admin_message = sprintf($this->language->get('text_dear'), $data['author']) . "\n\n";			
		$admin_message .= sprintf($this->language->get('text_display_name'), $data['author']) . "\n\n";	
		$admin_message .= sprintf($this->language->get('text_first_name'), $data['firstname']) . "\n\n";	
		$admin_message .= sprintf($this->language->get('text_last_name'), $data['lastname']) . "\n\n";	
		$admin_message .= sprintf($this->language->get('text_email'), $data['email']) . "\n\n\n";	
		$admin_message .= $this->language->get('text_thanks') . "\n";
		$admin_message .= 'Blog System';
		if ($this->config->get('config_notification_email')) {			
		$admin_mail->setTo($this->config->get('config_notification_email'));
		}else{	
		$admin_mail->setTo($this->config->get('config_email'));
		}
		$admin_mail->setFrom($this->config->get('config_email'));
		$admin_mail->setSender($this->config->get('config_name'));
		$admin_mail->setSubject(html_entity_decode($admin_subject, ENT_QUOTES, 'UTF-8'));
		$admin_mail->setText(html_entity_decode($admin_message, ENT_QUOTES, 'UTF-8'));
		$admin_mail->send();
		
	}
	
	public function editAuthor($data) {
		$this->db->query("UPDATE " . DB_PREFIX . "news_author SET
		author = '" . $this->db->escape($data['author']) . "',
		firstname = '" . $this->db->escape($data['firstname']) . "',
		lastname = '" . $this->db->escape($data['lastname']) . "',
		author_description = '" . $this->db->escape($data['author_description']) . "',
		email = '" . $this->db->escape($data['email']) . "',
		telephone = '" . $this->db->escape($data['telephone']) . "',
		website = '" . $this->db->escape($data['website']) . "',
		address = '" . $this->db->escape($data['address']) . "',
		city = '" . $this->db->escape($data['city']) . "',
		country_id = '" . (int)$data['country_id'] . "',
		zone_id = '" . (int)$data['zone_id'] . "',
		newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', 
		ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'
		WHERE author_id = '" . (int)$this->author->getId() . "'");
		
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "news_author SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE author_id = '" . (int)$this->author->getId() . "'");
		}
	}
	
	public function getAuthorByEmail($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_author WHERE email = '" . $this->db->escape($email) . "'");
		
		return $query->row;
	}
	public function getTotalAuthorsByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_author WHERE LOWER(email) = '" . $this->db->escape(strtolower($email)) . "'");
		
		return $query->row['total'];
	}
	public function getTotalAuthorsByUsername($username) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_author WHERE LOWER(author) = '" . $this->db->escape(strtolower($username)) . "'");		
		return $query->row['total'];
	}	
	public function getOtherAuthorsByUsername($username) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news_author WHERE 
		LOWER(author) = '" . $this->db->escape(strtolower($username)) . "'
		AND author_id != '" . (int)$this->author->getId() . "'");	
		return $query->row['total'];
	}
	public function editPassword($email, $password) {
      	$this->db->query("UPDATE " . DB_PREFIX . "news_author SET salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "' WHERE email = '" . $this->db->escape($email) . "'");
	}
}
?>