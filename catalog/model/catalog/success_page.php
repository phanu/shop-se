<?php
class ModelCatalogSuccessPage extends Controller {
	public function getSetting() {
		$data = array();

		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "setting` WHERE store_id = '" . (int)$this->config->get('config_store_id') . "' AND `" . (version_compare(VERSION, '2.0.1') < 0 ? 'group' : 'code') . "` = 'success_page'");

		foreach ($query->rows as $result) {
			$result['key'] = str_replace('success_page_', '', $result['key']);

			if (!$result['serialized']) {
				$data[$result['key']] = $result['value'];
			} else {
				if (version_compare(VERSION, '2.1') > 0) {
					$data[$result['key']] = json_decode($result['value'], true);
				} else {
					$data[$result['key']] = unserialize($result['value']);
				}
			}
		}

		return $data;
	}

	public function getOrderTax($order_id) {
		$query = $this->db->query("SELECT value FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' AND code = 'tax' LIMIT 1");

		return $query->row ? $query->row['value'] : 0;
	}

	public function getOrderProducts($order_id) {
		$query = $this->db->query("SELECT cd.name as category_name, op.* FROM " . DB_PREFIX . "category_description cd INNER JOIN " . DB_PREFIX . "product_to_category pc ON (pc.category_id = cd.category_id) INNER JOIN " . DB_PREFIX . "order_product op ON (pc.product_id = op.product_id) LEFT JOIN " . DB_PREFIX . "order_option oo ON (oo.order_product_id = op.order_product_id) WHERE op.order_id = '" . (int)$order_id . "' AND pc.product_id = op.product_id AND oo.order_id IS NULL GROUP BY op.order_product_id");

		return $query->rows;
	}

	public function getOrderProductOptions($order_id) {
		$query = $this->db->query("SELECT cd.name as category_name, op.*, oo.name as option_name, oo.value, oo.order_product_id, GROUP_CONCAT(DISTINCT oo.name, ': ', oo.value SEPARATOR ' - ') as options_data FROM " . DB_PREFIX . "category_description cd INNER JOIN " . DB_PREFIX . "product_to_category pc ON (pc.category_id = cd.category_id) INNER JOIN " . DB_PREFIX . "order_product op ON (pc.product_id = op.product_id) INNER JOIN " . DB_PREFIX . "order_option oo ON (op.order_product_id = oo.order_product_id) WHERE op.order_id = '" . (int)$order_id . "' AND pc.product_id = op.product_id AND op.order_product_id = oo.order_product_id GROUP BY oo.order_product_id");

		return $query->rows;
	}
}
?>