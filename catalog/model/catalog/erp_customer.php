<?php
################################################################################################
# Webservices xmlrpc Tab Opencart 1.5.1.x From Webkul  http://webkul.com    #
################################################################################################
class ModelCatalogErpCustomer extends Model {

    public function check_specific_address($id_address = false, $id_customer, $userId, $sock, $client, $db, $pwd){
        $erp_state_id=False;
        $erp_country_id=False;
        $erp_address = $this->search_address($id_address);
        if ($erp_address[0] > 0 AND $id_address) {
            return $erp_address[0];
        } else {
            $erp_cust_id = $this->check_specific_customer($id_customer,$sock, $client, $userId,$cart_user='Front End', $db, $pwd);
            if($id_address)
                $add = "AND a.address_id = '".$id_address."'";
            else
                $add = '';
            $data = $this->db->query("SELECT DISTINCT a.address_id, a.country_id, a.zone_id, a.customer_id, a.company, a.lastname, a.firstname, a.address_1, a.address_2, a.postcode, a.city, cu.telephone, cu.email, c.`name` AS country,c.iso_code_3 AS country_iso, s.name AS state, s.code AS state_iso,IFNULL(erp.`is_synch`, 0) AS is_synch, IFNULL(erp.`erp_address_id`, 0) AS erp_address_id
                FROM `".DB_PREFIX."customer` cu
                LEFT JOIN `".DB_PREFIX."address` a ON (a.`customer_id` = cu.`customer_id`) 
                LEFT JOIN `".DB_PREFIX."country` c ON (a.`country_id` = c.`country_id`)             
                LEFT JOIN `".DB_PREFIX."zone` s ON (s.`zone_id` = a.`zone_id`)
                LEFT JOIN `".DB_PREFIX."erp_address_merge` erp ON (erp.`opencart_address_id` = a.`address_id` AND erp.`customer_id` = a.`customer_id`) WHERE cu.customer_id='".$id_customer."' $add ")->rows;
            foreach ($data as $address_data) {
                $search_address = $this->search_address($address_data['address_id']);
                if ($search_address[0] <= 0) {
                    $erp_country_id = false;
                    $erp_state_id   = false;
                    if ($address_data['country_id']) {
                        // load another model functions
                        global $loader, $registry;
                        $loader->model('catalog/erp_country');
                        $country = $registry->get('model_catalog_erp_country');

                        $country_iso_code = $country->get_iso($address_data['country_id']);
                        $country_name     = $country->get_country_name($address_data['country_id']);
                        $erp_country_id   = $country->get_country($country_iso_code, $country_name, $userId, $sock, $client, $db, $pwd);
                    }
                    if ($address_data['zone_id']) {
                        // load another model functions
                        global $loader, $registry;
                        $loader->model('catalog/erp_state');
                        $state = $registry->get('model_catalog_erp_state');
                        $state_dtls   = $state->get_state_dtls($address_data['zone_id']);
                        $erp_state_id = $state->check_state($userId, $sock, $client, $state_dtls['code'], $state_dtls['name'], $state_dtls['country_id'], $db, $pwd);
                    }
                    $name = urlencode(html_entity_decode($address_data['firstname'].' '.$address_data['lastname']));
                    $address1 = urlencode(html_entity_decode($address_data['address_1']));
                    $address2 = urlencode(html_entity_decode($address_data['address_2']));
                    $city = urlencode(html_entity_decode($address_data['city']));
                    $email = urlencode(html_entity_decode($address_data['email']));
                    $context = array(
                        'opencart' => new xmlrpcval('opencart', "string")
                    );
                    $key = array(
                        'parent_id' => new xmlrpcval($erp_cust_id, "int"),
                        'name' => new xmlrpcval($name, "string"),
                        'email' => new xmlrpcval($email, "string"),
                        'street' => new xmlrpcval($address1, "string"),
                        'street2' => new xmlrpcval($address2, "string"),
                        'phone' => new xmlrpcval($address_data['telephone'], "int"),
                        'zip' => new xmlrpcval($address_data['postcode'], "string"),
                        'city' => new xmlrpcval($city, "string"),
                        'country_id' => new xmlrpcval($erp_country_id, "int"),
                        'state_id' => new xmlrpcval($erp_state_id, "int"),
                        'customer' => new xmlrpcval(false, "boolean"),
                        'use_parent_address' => new xmlrpcval(false, "boolean")
                    );
                    if ($search_address[0] == 0) {
                        $msg_ser = new xmlrpcmsg('execute');
                        $msg_ser->addParam(new xmlrpcval($db, "string"));
                        $msg_ser->addParam(new xmlrpcval($userId, "int"));
                        $msg_ser->addParam(new xmlrpcval($pwd , "string"));
                        $msg_ser->addParam(new xmlrpcval("res.partner", "string"));
                        $msg_ser->addParam(new xmlrpcval("create", "string"));
                        $msg_ser->addParam(new xmlrpcval($key, "struct"));
                        $msg_ser->addParam(new xmlrpcval($context, "struct"));
                        $resp = $client->send($msg_ser);
                        if (!$resp->faultCode()) {
                            $val = $resp->value()->me;
                            $this->addto_address_merge($address_data['address_id'], $val['int'], $address_data['customer_id']);
                            $add_to_openerp = $this->addto_openerp_merge($val['int'], $address_data['customer_id'], $address_data['address_id'], $userId, $client, $sock, $db, $pwd);
                            if ($add_to_openerp['is_error'] == 1) {
                                $error =  'Error Address not adding - specific address'. $address_data['address_id'].' ,';
                            }else{
                                return $val['int'];
                            }
                        }
                    }
                    if ($search_address[0] == -1) {
                        $update = $this->update_address($key, $address_data['address_id'], $search_address[1], $userId, $sock, $client, $db, $pwd);
                        if ($update['value'] != True) {
                            $error =  'Error Address not updating - specific address'. $address_data['address_id'].' ,';
                        }else{
                            return $search_address[1];
                        }
                    }
                }
            }
        }
    }

    //To search customer in customer_merge table.
    private function search_customer($customer_id){
        $check = $this->db->query("SELECT `is_synch`,`erp_customer_id`  from `" . DB_PREFIX . "erp_customer_merge` where opencart_customer_id = '$customer_id'")->row;

        if (isset($check['erp_customer_id']) AND $check['erp_customer_id'] > 0) {
            if ($check['is_synch'] == 1) {
                $check_arr[0] = -1;
                $check_arr[1] = $check['erp_customer_id'];
                return $check_arr;
            } else {
                $check_arr[0] = $check['erp_customer_id'];
                return $check_arr;
            }
        } else {
            $check_arr[0] = 0;
            return $check_arr;
        }
    }

    //To insert data in customer merge table
    private function addto_customer_merge($erp_customer_id, $customer_id, $cart_user = 'Front End'){
        $data = array(
            'erp_customer_id' => $erp_customer_id,
            'customer_id' => $customer_id,
            'created_by' => $cart_user
        );
        $this->db->query("INSERT INTO  `" . DB_PREFIX . "erp_customer_merge` SET erp_customer_id = '$erp_customer_id' , opencart_customer_id = '$customer_id' , created_by = '$cart_user', created_on = NOW() ");

        return $this->db->getLastId();
    }

    public function addto_openerp_merge($erp_id, $cart_id, $cart_add_id, $userId, $client, $sock , $db, $pwd){
        if ($cart_add_id == '-')
            {
                $type = "customer";
                $cart_add_id = 0;            
            }
        else           
            $type = "address";                
          
        $erp_partner_id = array(
                new xmlrpcval($erp_id, 'int')
        );

        $arrayVal = array(
            'opencart_customer_id' => new xmlrpcval($cart_id, "int"),
            'opencart_address_id' => new xmlrpcval($cart_add_id, "string"),
            'opencart_type' => new xmlrpcval($type, "string"),
            'need_sync' => new xmlrpcval('no', "string")
        );
        $msg_ser  = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval($db, "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval($pwd, "string"));
        $msg_ser->addParam(new xmlrpcval("res.partner", "string"));
        $msg_ser->addParam(new xmlrpcval("write", "string"));
        $msg_ser->addParam(new xmlrpcval($erp_partner_id, "array"));
        $msg_ser->addParam(new xmlrpcval($arrayVal, "struct"));
        $resp = $client->send($msg_ser);        
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            return array(
                'error_message' => $error_message,
                'is_error' => 1
            );
        } else {            
            return array(
                'is_error' => 0
            );
        }
    }
    
    public function search_address($id_address) {
        $erp_address = $this->db->query("SELECT `is_synch`,`erp_address_id`  from `" . DB_PREFIX . "erp_address_merge` where `opencart_address_id`='$id_address'")->row;        
        if (isset($erp_address['erp_address_id']) AND $erp_address['erp_address_id'] > 0) {
            if ($erp_address['is_synch'] == 0) {
                $arr[0] = -1;
                $arr[1] = $erp_address['erp_address_id'];
                return $arr;
            } else {                
                $arr[0] = $erp_address['erp_address_id'];                
                return $arr;
            }
        } else {
            $arr[0] = 0;
            return $arr;
        }
    }


    public function addto_address_merge($id_address, $erp_address_id, $id_customer, $cart_user = 'Front End'){

        $data = array(
            'erp_address_id' => $erp_address_id,
            'address_id' => $id_address,
            'created_by' => $cart_user,
            'id_customer' => $id_customer
        );
        
        $this->db->query("INSERT INTO  `" . DB_PREFIX . "erp_address_merge` SET erp_address_id = '$erp_address_id' , opencart_address_id = '$id_address' ,customer_id = '$id_customer', created_by = '$cart_user', created_on = NOW() ");
    }


    public function update_address($key, $id_address, $erp_id, $userId, $sock, $client, $db, $pwd){
        $context = array(
            'opencart' => new xmlrpcval('opencart', "string")
        );
        $erp_cust_id = array(
            new xmlrpcval($erp_id, 'int')
        );
        $msg_ser     = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval($db, "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval($pwd, "string"));
        $msg_ser->addParam(new xmlrpcval("res.partner", "string"));
        $msg_ser->addParam(new xmlrpcval("write", "string"));
        $msg_ser->addParam(new xmlrpcval($erp_cust_id, "array"));
        $msg_ser->addParam(new xmlrpcval($key, "struct"));
        $msg_ser->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_ser);       
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            return array(
                'error_message' => $error_message,
                'value' => False
            );
        } else {
            $this->db->query("UPDATE  `" . DB_PREFIX . "erp_address_merge` set `is_synch`=0 where `opencart_address_id`='$id_address'");
            return array(
                'value' => True
            );
        }
    }

    public function update_customer($data, $cart_id, $erp_id, $userId, $sock, $client, $db, $pwd){
        $context = array(
            'opencart' => new xmlrpcval('opencart', "string")
        );
        $erp_cust_id = array(
            new xmlrpcval($erp_id, 'int')
        );

        $key     = array(
            'name' => new xmlrpcval($data['firstname'] .' '. $data['lastname'], "string")                    
        );
        
        $msg_ser     = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval($db , "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval($pwd , "string"));
        $msg_ser->addParam(new xmlrpcval("res.partner", "string"));
        $msg_ser->addParam(new xmlrpcval("write", "string"));
        $msg_ser->addParam(new xmlrpcval($erp_cust_id, "array"));
        $msg_ser->addParam(new xmlrpcval($key, "struct"));
        $msg_ser->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_ser);        
        if ($resp->faultCode()) {
            
            $error_message = $resp->faultString();
            return array(
                'error_message' => $error_message,
                'value' => False
            );
        } else {
            $this->db->query("UPDATE `" . DB_PREFIX . "erp_customer_merge` set `is_synch`= 0 where opencart_customer_id = '$cart_id'");          
            return array(
                'value' => True
            );
        }
    }    

    public function check_specific_customer($id_customer, $sock, $client, $userId,$cart_user='Front End', $db, $pwd){
		$check_customer_id = $this->search_customer($id_customer);
		if ($check_customer_id[0] > 0)
			return $check_customer_id[0];
		else {
			    $customer_data = $this->db->query("SELECT `customer_id`,`firstname`,`lastname`,`email` FROM `" . DB_PREFIX . "customer`  WHERE `customer_id` = '".$id_customer."'")->row;
    		  
                $customer_data['firstname'] = urlencode(html_entity_decode($customer_data['firstname']));
                $customer_data['lastname']  = urlencode(html_entity_decode($customer_data['lastname']));
                $key                        = array(
                    'name' => new xmlrpcval($customer_data['firstname'] .' '. $customer_data['lastname'], "string"),
                    'email' => new xmlrpcval(urlencode(html_entity_decode($customer_data['email'])), "string"),
                    'is_company' => new xmlrpcval(true, "boolean")
                );

                $context = array(
                    'opencart' => new xmlrpcval('opencart', "string")
                );
                $msg_ser = new xmlrpcmsg('execute');
                $msg_ser->addParam(new xmlrpcval($db, "string"));
                $msg_ser->addParam(new xmlrpcval($userId, "int"));
                $msg_ser->addParam(new xmlrpcval($pwd, "string"));
                $msg_ser->addParam(new xmlrpcval("res.partner", "string"));
                $msg_ser->addParam(new xmlrpcval("create", "string"));
                $msg_ser->addParam(new xmlrpcval($key, "struct"));
                $msg_ser->addParam(new xmlrpcval($context, "struct"));
                $resp = $client->send($msg_ser);
                if (!$resp->faultCode()) {
                    $val            = $resp->value()->me;
                    //enter in to customer merge table ...........
                    $db_done        = $this->addto_customer_merge($val['int'], $customer_data['customer_id'], $cart_user);
                    $add_to_openerp = $this->addto_openerp_merge($val['int'], $customer_data['customer_id'], '-', $userId, $client, $sock, $db, $pwd);
                   return $val['int'];
                }
            }
        return -1;
	}
}
?>