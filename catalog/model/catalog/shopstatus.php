<?php
class ModelCatalogShopstatus extends Model {

	public function getAllProducts(){

		$query = $this->db->query("SELECT p.product_id FROM ". DB_PREFIX ."product p LEFT JOIN ".DB_PREFIX."product_to_store p2s ON(p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '".(int)$this->config->get('config_store_id')."'");

		if($query->num_rows){
	
		return $query->num_rows;

		}else{
		
		return 0;

		}

	}

	public function getAllCategories(){

		$query = $this->db->query("SELECT c.category_id FROM ". DB_PREFIX ."category c LEFT JOIN ".DB_PREFIX."category_to_store c2s ON(c.category_id = c2s.category_id) WHERE c.status = '1' AND c2s.store_id = '".(int)$this->config->get('config_store_id')."'");

		if($query->num_rows){
	
		return $query->num_rows;

		}else{
		
		return 0;

		}

	}

	public function getShopStart(){

		$query = $this->db->query("SELECT p.date_added FROM ". DB_PREFIX ."product p LEFT JOIN ".DB_PREFIX."product_to_store p2s ON(p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '".(int)$this->config->get('config_store_id')."' ORDER BY date_added ASC LIMIT 1");
		
		return $query->row['date_added'];
	}

	public function getShopNow(){

		$query1 = $this->db->query("SELECT p.date_modified FROM ". DB_PREFIX ."product p LEFT JOIN ".DB_PREFIX."product_to_store p2s ON(p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '".(int)$this->config->get('config_store_id')."' ORDER BY p.date_modified DESC LIMIT 1");

		$timetoint1 = strtotime($query1->row['date_modified']);
	
		$query2 = $this->db->query("SELECT p.date_added FROM ". DB_PREFIX ."product p LEFT JOIN ".DB_PREFIX."product_to_store p2s ON(p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '".(int)$this->config->get('config_store_id')."' ORDER BY p.date_added DESC LIMIT 1");

		$timetoint2 = strtotime($query2->row['date_added']);

		if($timetoint1>$timetoint2){
		return $query1->row['date_modified'];
		}else{
		return $query2->row['date_added'];
		}

	}
	public function getPageviews(){
		$query = $this->db->query("SELECT `value` as total FROM ".DB_PREFIX."setting WHERE `key` = 'shopstatus_pageviews' AND store_id = '".(int)$this->config->get('config_store_id')."'");

		return (int)$query->row['total'];
	}

	public function addPageviews(){
		$query = $this->db->query("SELECT `value` as total FROM ".DB_PREFIX."setting WHERE `key` = 'shopstatus_pageviews' AND store_id = '".(int)$this->config->get('config_store_id')."'");
		if($query->num_rows > 0 && $query->row['total']>'0'){//detect row
		$this->db->query("UPDATE ".DB_PREFIX."setting SET `value` = `value`+1 WHERE `key` = 'shopstatus_pageviews' AND store_id = '".(int)$this->config->get('config_store_id')."'");
		}else{//set initial value by get all products viewed.
		$views = $this->db->query("SELECT SUM(viewed) as total FROM ".DB_PREFIX."product p LEFT JOIN ".DB_PREFIX."product_to_store p2s ON(p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '".(int)$this->config->get('config_store_id')."'");
		$this->db->query("INSERT INTO ".DB_PREFIX."setting SET store_id = '".(int)$this->config->get('config_store_id')."' , `group` = 'shopstatus' , `key` = 'shopstatus_pageviews' , `value` = '".(int)$views->row['total']."' ,serialized = '0'");
		}
	}

	public function getVisitors(){
		$query = $this->db->query("SELECT all_value as total FROM ".DB_PREFIX."counter_values WHERE 1");

		return (int)$query->row['total'];
	}

	public function getLink(){
		return 'PGEgaHJlZj0iaHR0cDovL3d3dy5vcGVuY2FydDJ1LmNvbSIgdGl0bGU9Im9wZW5jYXJ0Ij48aW1nIHNyYz0iY2F0YWxvZy92aWV3L3RoZW1lL2RlZmF1bHQvaW1hZ2Uvc2hvcHN0YXR1cy5naWYiIGFsdD0ib3BlbmNhcnQiIGJvcmRlcj0iMCIgLz48L2E+';
	}
}
?>