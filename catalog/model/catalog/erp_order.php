<?php
class ModelCatalogErpOrder extends Model {

	public function check_spec_order($order_id, $userId, $client, $sock, $db, $pwd, $cart_user){
		
		$check_order_isSync = $this->db->query("SELECT id FROM ".DB_PREFIX."erp_order_merge WHERE opencart_order_id = '".$order_id."'")->row;
		
		if($check_order_isSync AND isset($check_order_isSync['id']))
			return;

		//load opencart order model and get order info
		$this->load->model('account/order');
		$This_order = $this->model_account_order->getOrder($order_id);

		//get currency id from orderdata
		$currency_id = $This_order['currency_id'];

		//load currency model and check specific 
		$this->load->model('catalog/erp_currency');
		$pricelist_id = $this->model_catalog_erp_currency->check_specific_currency($currency_id, $userId, $sock, $client , $db, $pwd);
		if(!$pricelist_id){
			return array(0,0,"Openerp pricelist id not found");
		}		
		//it will return an array of (partner_id, partner_invoice_id, partner_shipping_id)
		$erpAddressArray = $this->getErpOrderAddresses($This_order, $userId, $client, $sock, $db, $pwd, $cart_user);
		
		if(count(array_filter($erpAddressArray)) == 3 AND $erpAddressArray[1] > 0 AND $erpAddressArray[2] > 0){
			$partner_id = $erpAddressArray[0];
			$partner_invoice_id = $erpAddressArray[1];
			$partner_shipping_id = $erpAddressArray[2];
			$erp_carrier_id = false;

			//get carrier code for erp is shipping exists
			if($This_order['shipping_firstname']){		

				$shipping_code = $this->db->query("SELECT shipping_code FROM ".DB_PREFIX."order WHERE order_id = '".$order_id."'")->row;	

				if($shipping_code){
					$This_order['shipping_code'] = $shipping_code['shipping_code'];				
					$this->load->model('catalog/erp_carrier');
					$erp_carrier = $this->model_catalog_erp_carrier->check_specific_carrier($This_order['shipping_code'], $userId, $sock, $client,$db, $pwd);
					$erp_carrier_id  = $erp_carrier['erp_id'];	
				}			
			}
			
			$order_array =  array(
							'partner_id'=>new xmlrpcval($partner_id,"int"),
							'partner_invoice_id'=>new xmlrpcval($partner_invoice_id,"int"),
							'partner_shipping_id'=>new xmlrpcval($partner_shipping_id,"int"),
							'pricelist_id'=>new xmlrpcval($pricelist_id,"int"),
							'date_order'=>new xmlrpcval($This_order['date_added'],"string"),
							'opencart_order_id'	=>	new xmlrpcval($order_id, "int")	,
							'carrier_id'	=>	new xmlrpcval($erp_carrier_id, "int"),
							'cart_total'	=>	new xmlrpcval($This_order['total'], "string"),
						);
			$This_order_products = $this->cart->getProducts();
			$i = 0;
			$line_array	 =array();
			$currency_code = $This_order['currency_code'];
			$config_currency_code = $this->config->get('config_currency');
			foreach($This_order_products as $itm){
				$erp_tax_array = array();
				$item_desc = $itm['name'];
				$ItemBasePrice = $this->currency->convert($itm['price'], $config_currency_code, $currency_code);
				$context = array('db'  => $db,
								 'pwd' => $pwd,
								 'cart_user'  => $cart_user,
								 'lang_id' => 1);

				//load product model and get product info				
				$this->load->model('catalog/erp_product');

				//load tax model and get tax info				
				$this->load->model('catalog/erp_tax');

				$product_response = $this->model_catalog_erp_product->check_specific_product($itm['product_id'] ,$userId, $client, $sock, $context);
				
				if(!$product_response){
					return 'error product specific sync';
				}				
				$erp_product_id = $product_response['erp_id'];
				//add product options details in product description
				if($itm['option']){
					foreach($itm['option'] as $value){
						$item_desc = $item_desc .' name - ' . $value['name'] .' , value - '. $value['option_value'];
					}
				}

				//get tax from function
				$tax_class_id = $this->db->query("SELECT tax_class_id FROM ".DB_PREFIX."product WHERE product_id = '".$itm['product_id']."'")->row;				

				if($tax_class_id)			
				$tax_per_product = $this->tax->getRates($ItemBasePrice, $tax_class_id['tax_class_id']);

				foreach ($tax_per_product as $key => $value) {
					$erp_tax_id = $this->model_catalog_erp_tax->check_specific_tax($key, $client, $sock, $userId, $db, $pwd);			
					if($erp_tax_id)
						$erp_tax_array[] = new xmlrpcval($erp_tax_id,"int");
				}

				$line_array[$i] =  new xmlrpcval(array(
					'type'=>new xmlrpcval('Product', "string"),
					'product_id'=>new xmlrpcval($erp_product_id,"int"),
					'price_unit'=>new xmlrpcval($ItemBasePrice,"string"),
					'product_uom_qty'=>new xmlrpcval($itm['quantity'],"string"),
					'name'=>new xmlrpcval(urlencode(html_entity_decode($item_desc)),"string"),
					'tax_id'=>new xmlrpcval($erp_tax_array, "array"),
				),'struct');
				$i = $i+1;
				
			}		
			
			/******************** For Voucher ******************/
			$voucher_code = $this->db->query("SELECT v.amount,v.code FROM ".DB_PREFIX."voucher v WHERE v.order_id = '".$order_id."'")->row;

			if ($voucher_code){
				if(!$voucher_code['code'])
					$code = "Discount";
				else
					$code = $code['code'];
				$code = urlencode(html_entity_decode($code));
				$line_array[$i] =  new xmlrpcval(array(
							'name'=>new xmlrpcval($code,"string"),
							'type'=>new xmlrpcval('Voucher', "string"),
							'product_uom_qty'	=>	new xmlrpcval('1', "string"),
							'price_unit'=>new xmlrpcval(-$voucher_code['amount'],"double"),
					),"struct");
				$i=$i+1;
			}


			/******************** For Coupon ******************/			
			$coupon_info = $this->db->query("SELECT c.discount,c.code FROM ".DB_PREFIX."coupon_history ch LEFT JOIN " . DB_PREFIX . "coupon c ON (ch.coupon_id = c.coupon_id) WHERE ch.order_id = '".$order_id."'")->row;			
			if ($coupon_info){
				$coupon_code = $coupon_info['code'];				
				if(!$coupon_code)
					$code = "Discount";
				else
					$code = $coupon_code;

				$code = urlencode(html_entity_decode($code));
				$line_array[$i] =  new xmlrpcval(array(
							'name'=>new xmlrpcval($code,"string"),
							'type'=>new xmlrpcval('Coupon', "string"),
							'product_uom_qty'	=>	new xmlrpcval('1', "string"),
							'price_unit'=>new xmlrpcval(-$coupon_info['discount'],"double"),
					),"struct");
				$i=$i+1;
			}
						
			/******************** For Shipping ******************/
			if ($This_order['shipping_firstname']){
				$shipping_cost = 0.0;
				$erp_tax_array = array();
				$order_total = $this->db->query("SELECT title,value FROM ".DB_PREFIX."order_total WHERE order_id = '".$order_id."' AND code = 'shipping'")->row;
				$shipping_description = urlencode(html_entity_decode($This_order['shipping_method']));				

				if($order_total){					
					$order_shipping_title = $order_total['title'];
					$shipping_cost = $this->currency->convert($order_total['value'], $config_currency_code, $currency_code);
				}	

				$shipping_code_array = explode('.',$This_order['shipping_code']);
				$shipping_code = $shipping_code_array[0];
				$shipping_tax_class_id = $this->config->get($shipping_code.'_tax_class_id');
				if($shipping_tax_class_id){
					$shipping_tax = $this->tax->getRates($shipping_cost, $shipping_tax_class_id);
					foreach ($shipping_tax as $key => $value) {
						$erp_tax_id = $this->model_catalog_erp_tax->check_specific_tax($key, $client, $sock, $userId, $db, $pwd);
						if($erp_tax_id)
							$erp_tax_array[] = new xmlrpcval($erp_tax_id,"int");
					}
				}
				$line_array[$i] = new xmlrpcval(array(
						'name'=>new xmlrpcval($shipping_description,"string"),
						'type'=>new xmlrpcval('Shipping', "string"),
						'product_uom_qty'	=>	new xmlrpcval('1', "string"),
						'customer_id'=>new xmlrpcval(1,"int"),
						'price_unit'=>new xmlrpcval($shipping_cost,"double"),
						'tax_id'=>new xmlrpcval($erp_tax_array, "array")
					),"struct");
			}
			$msg1 = new xmlrpcmsg('execute');
			$msg1->addParam(new xmlrpcval($db, "string"));
			$msg1->addParam(new xmlrpcval($userId, "int"));
			$msg1->addParam(new xmlrpcval($pwd, "string"));
			$msg1->addParam(new xmlrpcval("extra.function", "string"));
			$msg1->addParam(new xmlrpcval("create_n_confirm_order", "string"));
			$msg1->addParam(new xmlrpcval($order_array, "struct"));
			$msg1->addParam(new xmlrpcval($line_array, "array"));
			$response  = $client->send($msg1);
			if (!$response->faultCode()){
				$val  = $response->value()->me;
				$error_message = 'Synced From Front OpenCart Order Id-'.$order_id;
				$this->log->write($error_message);
				$response_array = php_xmlrpc_decode($response->value()->me['array'][0]);
				$merge_array=array(
					'opencart_order_id' 	=> $response_array['opencart_order_id'],
					'erp_order_name' 	=> $response_array['erp_order_name'],
					'erp_order_id' 		=>$response_array['erp_order_id'],
					);
				$erp_order_id 	= $response_array['erp_order_id'];
				$this->db->query("INSERT INTO ".DB_PREFIX."erp_order_merge SET erp_order_id='$erp_order_id',opencart_order_id='$order_id',created_by='$cart_user',customer_id = '".$partner_id."'"); 
			}
			return array($order_id, $partner_id);
		}
		return array(0,0,"error occured during creating an openerp customer.");
	}

	public function getErpOrderAddresses($This_order, $userId, $client, $sock, $db, $pwd, $cart_user = 'cart user'){
		$partner_id = 0;
		$partner_shipping_id = 0;
		$partner_invoice_id = 0;

		$erp_country_id = false;
        $erp_state_id   = false;

        global $loader, $registry;
        
        $s = $p = array();
        
        if ($This_order['shipping_country_id']) {

			$s = array(
				'firstname' => $This_order['shipping_firstname'],
				'lastname' => $This_order['shipping_lastname'],
				'address_1' => $This_order['shipping_address_1'],
				'address_2' => $This_order['shipping_address_2'],
				'email' => $This_order['email'],
				'telephone' => $This_order['telephone'],
				'zip' => $This_order['shipping_postcode'],
				'city' => $This_order['shipping_city'],
				'country_id' => $This_order['shipping_country_id'],
				'state_id' => $This_order['shipping_zone_id'],	
				'customer_id' => $This_order['customer_id'],	
			);
        }

        if ($This_order['payment_zone_id']) {

            $p = array(
            			'firstname' => $This_order['payment_firstname'],
						'lastname' => $This_order['payment_lastname'],
						'address_1' => $This_order['payment_address_1'],
						'address_2' => $This_order['payment_address_2'],
						'email' => $This_order['email'],
						'telephone' => $This_order['telephone'],
						'zip' => $This_order['payment_postcode'],
						'city' => $This_order['payment_city'],
						'country_id' => $This_order['payment_country_id'],
						'state_id' => $This_order['payment_zone_id'],	
						'customer_id' => $This_order['customer_id'],	           
		           );
        }

        //if shipping is not added than shipping = payment 
        if(!$s)
        	$s = $p;
		// if customer is guest
		if(!$This_order['customer_id']){
			$customer_arr =  array(
					'is_company'=>new xmlrpcval(true,"boolean"),
					'name'=>new xmlrpcval(urlencode(html_entity_decode($This_order['firstname'].''.$This_order['lastname'])),"string"),
					'email'=>new xmlrpcval(urlencode(html_entity_decode($This_order['email'])),"string"),
				);

			$erp_customer_id = $this->AddGuestCustomerToErp($customer_arr, $userId, $client, $sock, $db, $pwd, $cart_user);
			
			if(isset($erp_customer_id['error_message'])){
				return array($erp_customer_id['error_message']);
			}

			$partner_id = $erp_customer_id['erp_id'];

			$isDifferent = $this->checkAddresses($s,$p);

			if($isDifferent == true){
				$partner_shipping_id = $this->createErpAddress($s, $partner_id, $userId, $client, $sock, $db, $pwd, $cart_user);
				$partner_invoice_id = $this->createErpAddress($p, $partner_id, $userId, $client, $sock, $db, $pwd, $cart_user );
			}else{
				$partner_invoice_id = $this->createErpAddress($s, $partner_id, $userId, $client, $sock, $db, $pwd, $cart_user);
				$partner_shipping_id = $partner_invoice_id;
			}			
		}
		// if customer is login
		if($This_order['customer_id'] > 0){

			//load opencart order model and get order info
			$loader->model('catalog/erp_customer');
            $customer = $registry->get('model_catalog_erp_customer');

            $partner_id = $customer->check_specific_customer($This_order['customer_id'], $sock, $client, $userId,$cart_user='Front End', $db, $pwd);
            if(!$partner_id)
            	return 'Error customer chk specific';            

			$address_id = $this->getAddressId($s);				
			if($address_id)
				$partner_shipping_id = $customer->check_specific_address($address_id, $This_order['customer_id'], $userId, $sock, $client, $db, $pwd);
			else
				return 'No Address Matched';

			$address_id = $this->getAddressId($p);	
			if($address_id)
				$partner_invoice_id = $customer->check_specific_address($address_id, $This_order['customer_id'], $userId, $sock, $client, $db, $pwd);	
			else
				return 'No Address Matched';
			
		}
		
		if($partner_invoice_id > 0 AND $partner_invoice_id > 0)
			return array($partner_id, $partner_invoice_id, $partner_shipping_id);
		else
			return array($partner_id, $partner_invoice_id, $partner_shipping_id);
	}

	public function checkAddresses($shipping,$payment){
		$flag = false;		

		if($shipping['state_id'] and $payment['state_id'] ){
			$s = '';
			$b = '';

			if($shipping[$s.'firstname'] != $payment[$b.'firstname'])
				$flag = true;			
			if($shipping[$s.'lastname'] != $payment[$b.'lastname'])
				$flag = true;
			if($shipping[$s.'address_1'] != $payment[$b.'address_1'])
				$flag = true;
			if($shipping[$s.'state_id'] != $payment[$b.'state_id'])
				$flag = true;			
			if($shipping[$s.'country_id'] != $payment[$b.'country_id'])
				$flag = true;
			if($shipping[$s.'city'] != $payment[$b.'city'])
				$flag = true;
			if($shipping[$s.'zip'] != $payment[$b.'zip'])
				$flag = true;
		}		
		return $flag;
	}

	public function getAddressId($data){
		
		$address_id = $this->db->query("SELECT address_id FROM ".DB_PREFIX."address WHERE firstname = '".$data['firstname']."' AND lastname = '". $data['lastname']."' AND address_1 = '".$data['address_1']."' AND address_2 = '".$data['address_2']."' AND  postcode = '".$data['zip']."' AND city = '".$data['city']."' AND country_id = '".$data['country_id']."' AND zone_id = '".$data['state_id']."' AND customer_id = '".$data['customer_id']."'")->row;
		if($address_id)
			return $address_id['address_id'];		
		else
			false;
	}

	//used to add guest customer to erp (nik added)
	public function AddGuestCustomerToErp($key, $userId, $client, $sock, $db, $pwd, $cart_user){

		$context = array( 
				'opencart' => new xmlrpcval('opencart', "string")
			);
        $msg_ser = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval($db, "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval($pwd, "string"));
        $msg_ser->addParam(new xmlrpcval("res.partner", "string"));
        $msg_ser->addParam(new xmlrpcval("create", "string"));
        $msg_ser->addParam(new xmlrpcval($key, "struct"));
		$msg_ser->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_ser);
        if (!$resp->faultCode()) {
            $val = $resp->value()->me;
            if(isset($val['int']) AND $val['int']){
            	return array(
                	'erp_id' => $val['int'],
                );
            }else{
            	return array(
                	'error_message' => 'No epr_id Returned',
                );
            }
        }else{
        	$error_message = $resp->faultString();
            return array(
                'error_message' => $error_message,
            );
        }
	}

	public function createErpAddress($data, $partner_id, $userId, $client, $sock, $db, $pwd, $cart_user ){
        global $loader, $registry;

         // load another model functions            
        $loader->model('catalog/erp_country');
        $country = $registry->get('model_catalog_erp_country');

        // load another model functions           
        $loader->model('catalog/erp_state');
        $state = $registry->get('model_catalog_erp_state');


		$context = array(
                    'opencart' => new xmlrpcval('opencart', "string")
                );

		$country_iso_code = $country->get_iso($data['country_id']);
        $country_name     = $country->get_country_name($data['country_id']);
        $erp_country_id   = $country->get_country($country_iso_code, $country_name, $userId, $sock, $client, $db, $pwd);
                
        $state_dtls   = $state->get_state_dtls($data['state_id']);
        $erp_state_id = $state->check_state($userId, $sock, $client, $state_dtls['code'], $state_dtls['name'], $state_dtls['country_id'], $db, $pwd);

       	$key = array(
                    'parent_id' => new xmlrpcval($partner_id, "int"),
                    'name' => new xmlrpcval($data['firstname'], "string"),
                    'email' => new xmlrpcval($data['email'], "string"),
                    'street' => new xmlrpcval($data['address_1'], "string"),
                    'street2' => new xmlrpcval($data['address_2'], "string"),
                    'phone' => new xmlrpcval($data['telephone'], "int"),                    
                    'zip' => new xmlrpcval($data['zip'], "string"),
                    'city' => new xmlrpcval($data['city'], "string"),
                    'country_id' => new xmlrpcval($erp_country_id, "int"),
                    'state_id' => new xmlrpcval($erp_state_id, "int"),
                    'customer' => new xmlrpcval(false, "boolean"),
                    'use_parent_address' => new xmlrpcval(false, "boolean")
                );
		$msg_ser = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval($db, "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval($pwd , "string"));
        $msg_ser->addParam(new xmlrpcval("res.partner", "string"));
        $msg_ser->addParam(new xmlrpcval("create", "string"));
        $msg_ser->addParam(new xmlrpcval($key, "struct"));
        $msg_ser->addParam(new xmlrpcval($context, "struct"));
        $resp = $client->send($msg_ser);  

        if (!$resp->faultCode()) {
        	$val = $resp->value()->me;
            return $val['int'];
        } else {
            return $resp->faultString();
        }
	}

	public function getRates($value, $tax_class_id, $customer_group_id,$shipping_address,$payment_address,$store_address) {
		$tax_rates = array();
		
		if (!$customer_group_id) {			
			$customer_group_id = $this->config->get('config_customer_group_id');
		}
				
		if ($shipping_address) {
			$tax_query = $this->db->query("SELECT tr2.tax_rate_id, tr2.name, tr2.rate, tr2.type, tr1.priority FROM " . DB_PREFIX . "tax_rule tr1 LEFT JOIN " . DB_PREFIX . "tax_rate tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) INNER JOIN " . DB_PREFIX . "tax_rate_to_customer_group tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) LEFT JOIN " . DB_PREFIX . "zone_to_geo_zone z2gz ON (tr2.geo_zone_id = z2gz.geo_zone_id) LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (tr2.geo_zone_id = gz.geo_zone_id) WHERE tr1.tax_class_id = '" . (int)$tax_class_id . "' AND tr1.based = 'shipping' AND tr2cg.customer_group_id = '" . (int)$customer_group_id . "' AND z2gz.country_id = '" . (int)$shipping_address['country_id'] . "' AND (z2gz.zone_id = '0' OR z2gz.zone_id = '" . (int)$shipping_address['zone_id'] . "') ORDER BY tr1.priority ASC");
			
			foreach ($tax_query->rows as $result) {
				$tax_rates[$result['tax_rate_id']] = array(
					'tax_rate_id' => $result['tax_rate_id'],
					'name'        => $result['name'],
					'rate'        => $result['rate'],
					'type'        => $result['type'],
					'priority'    => $result['priority']
				);
			}
		}

		if ($payment_address) {
			$tax_query = $this->db->query("SELECT tr2.tax_rate_id, tr2.name, tr2.rate, tr2.type, tr1.priority FROM " . DB_PREFIX . "tax_rule tr1 LEFT JOIN " . DB_PREFIX . "tax_rate tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) INNER JOIN " . DB_PREFIX . "tax_rate_to_customer_group tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) LEFT JOIN " . DB_PREFIX . "zone_to_geo_zone z2gz ON (tr2.geo_zone_id = z2gz.geo_zone_id) LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (tr2.geo_zone_id = gz.geo_zone_id) WHERE tr1.tax_class_id = '" . (int)$tax_class_id . "' AND tr1.based = 'payment' AND tr2cg.customer_group_id = '" . (int)$customer_group_id . "' AND z2gz.country_id = '" . (int)$payment_address['country_id'] . "' AND (z2gz.zone_id = '0' OR z2gz.zone_id = '" . (int)$payment_address['zone_id'] . "') ORDER BY tr1.priority ASC");
			
			foreach ($tax_query->rows as $result) {
				$tax_rates[$result['tax_rate_id']] = array(
					'tax_rate_id' => $result['tax_rate_id'],
					'name'        => $result['name'],
					'rate'        => $result['rate'],
					'type'        => $result['type'],
					'priority'    => $result['priority']
				);
			}
		}
		
		if ($store_address) {
			$tax_query = $this->db->query("SELECT tr2.tax_rate_id, tr2.name, tr2.rate, tr2.type, tr1.priority FROM " . DB_PREFIX . "tax_rule tr1 LEFT JOIN " . DB_PREFIX . "tax_rate tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) INNER JOIN " . DB_PREFIX . "tax_rate_to_customer_group tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) LEFT JOIN " . DB_PREFIX . "zone_to_geo_zone z2gz ON (tr2.geo_zone_id = z2gz.geo_zone_id) LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (tr2.geo_zone_id = gz.geo_zone_id) WHERE tr1.tax_class_id = '" . (int)$tax_class_id . "' AND tr1.based = 'store' AND tr2cg.customer_group_id = '" . (int)$customer_group_id . "' AND z2gz.country_id = '" . (int)$store_address['country_id'] . "' AND (z2gz.zone_id = '0' OR z2gz.zone_id = '" . (int)$store_address['zone_id'] . "') ORDER BY tr1.priority ASC");
			
			foreach ($tax_query->rows as $result) {
				$tax_rates[$result['tax_rate_id']] = array(
					'tax_rate_id' => $result['tax_rate_id'],
					'name'        => $result['name'],
					'rate'        => $result['rate'],
					'type'        => $result['type'],
					'priority'    => $result['priority']
				);
			}
		}			
		
		$tax_rate_data = array();
		
		foreach ($tax_rates as $tax_rate) {
			if (isset($tax_rate_data[$tax_rate['tax_rate_id']])) {
				$amount = $tax_rate_data[$tax_rate['tax_rate_id']]['amount'];
			} else {
				$amount = 0;
			}
			
			if ($tax_rate['type'] == 'F') {
				$amount += $tax_rate['rate'];
			} elseif ($tax_rate['type'] == 'P') {
				$amount += ($value / 100 * $tax_rate['rate']);
			}
		
			$tax_rate_data[$tax_rate['tax_rate_id']] = array(
				'tax_rate_id' => $tax_rate['tax_rate_id'],
				'name'        => $tax_rate['name'],
				'rate'        => $tax_rate['rate'],
				'type'        => $tax_rate['type'],
				'amount'      => $amount
			);
		}
		
		return $tax_rate_data;
	}

}
?>