<?php
class ModelCatalogPayment extends Model {

	public function getPayment($type) {
		$extension_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "extension WHERE `type` = '" . $this->db->escape($type) . "'");
		
		foreach ($query->rows as $result) {
			$extension_data[] = $result['code'];
		}
		
		return $extension_data;
	}//end fnc getPayment

	public function addPayment($data) {
      	$this->db->query("INSERT INTO " . DB_PREFIX . "payment SET 
		image		= '" . $this->db->escape($data['image']) . "', 
		name		= '" . $this->db->escape($data['name']) . "',
		email		= '" . $this->db->escape($data['email']) . "',
		telephone	= '" . $this->db->escape($data['telephone']) . "',
		total		= '" . (float)$data['total'] . "', 
		order_id	= '" . (int)$data['order_id'] . "', 
		bank		= '" . $this->db->escape($data['bank']) . "',
		paid_date	= '" . $this->db->escape($data['paid_date']) . "', 
		paid_time	= '" . $this->db->escape($data['paid_time']) . "', 
		enquiry		= '" . $this->db->escape($data['enquiry']) . "', 
		status = '0',
		date_added	= NOW()");
	}//end fnc addPayment

	public function isOrder($order_id,$email){

		$query = $this->db->query("SELECT order_id FROM `" .DB_PREFIX. "order` WHERE order_id = '".(int)$order_id."' AND email = '".$this->db->escape($email)."' AND order_status_id > 0");

		if($query->num_rows){
			return true;
		}else{
			return false;
		}
	}
}
?>