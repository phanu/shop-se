<?php
class ModelCatalogSerial extends Model {
    
    public function addOrderSerials($product_id, $order_id, $order_product_id, $quantity){
        
        // Get serials for product
        $serials = $this->getSerial($product_id, $quantity);
        if(!$serials) return false;
        if(!count($serials)) return false;
        
        $values = array();
        $todelete = array();
        
        $order_id = (int) $order_id;
        $order_product_id = (int) $order_product_id;
        
        foreach($serials as $s) {
        	$key = $this->db->escape($s['key']);
            $values[] = "('$order_id', '$key', '$order_product_id')";
            $todelete[] = (int) $s['id'];
        }
        $vals = implode(',', $values);
        $query = sprintf('
        INSERT INTO
            `%1$sserials_order` (`oid`, `key`, `pid`)
        VALUES
            %2$s
        ',
        DB_PREFIX,
        $vals);
        $this->deleteSerialsByID($todelete);
        $this->db->query($query);
        
        if($this->config->get('config_serial_email_admin') == 1) {
			$count = $this->getTotalSerials($product_id);
			if($count <= $this->config->get('config_serial_threshold')) {
				$this->load->model('catalog/product');
				
				$product = $this->model_catalog_product->getProduct($product_id);
				
				if(!$product) {
					return;
				}
				
				$this->load->language('product/serial');
				$text = $this->language->get('text_low_serial');
				$subject = $this->language->get('text_subject');
				
				$text = str_replace('{count}', $count, $text);
				$text = str_replace('{name}', $product['name'], $text);
				
				$mail = new Mail(); 
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');			
				$mail->setTo($this->config->get('config_email'));
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($this->config->get('config_name'));
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				$mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
				$mail->send();
			}
        }
    }
    
    public function deleteSerialsByID($keys) {
    	foreach($keys as $k => $key) {
    		$keys[$k] = (int) $key;
    	}
    	
        $keystr = implode(',', $keys);
        $query = sprintf('
            DELETE FROM
                `%1$sserials`
            WHERE
                `id` IN (%2$s)
            ',
            DB_PREFIX,
            $keystr
        );
        $this->db->query($query);
    }
    
    /**
     * ModelCatalogSerial::getSerial()
     * Gets the specified number of serial keys for a product
     * @param integer $product_id
     * @param integer $keys
     * @return mixed
     */
    public function getSerial($product_id, $keys = 1) {
        $query = sprintf('
            SELECT
                `s`.*
            FROM
                `%1$sserials` `s`
            WHERE
                `s`.`pid` = %2$s
            ORDER BY
                `s`.`id`
            LIMIT %3$s',
            DB_PREFIX,
            (int) $product_id,
            (int) $keys
        );
        $result = $this->db->query($query);
        if($result->num_rows) {
            return $result->rows;
        }
        return false;        
    }
    
    public function getOrderSerials($order_download_id) {
        $query = sprintf('
            SELECT
                `so`.`key`
            FROM
                `%1$sserials_order` `so`
            LEFT JOIN
                `%1$sorder_download` `o`
            ON
                `o`.`order_product_id` = `so`.`pid`
            WHERE
                `o`.`order_download_id` = %2$s
            ORDER BY
                `so`.`id`',
            DB_PREFIX,
            (int) $order_download_id
        );
        $result = $this->db->query($query);
        if($result->num_rows) {
            $res = array();
            foreach($result->rows as $row) {
                $res[] = $row['key'];
            }
            return $res;
        }
        return false;

    }
    
    public function getSerialsByProduct($order_product_id) {
        $query = sprintf('
            SELECT
                `so`.`key`
            FROM
                `%1$sserials_order` `so`
            WHERE
                `so`.`pid` = %2$s
            ORDER BY
                `so`.`id`',
            DB_PREFIX,
            (int) $order_product_id
        );
        $result = $this->db->query($query);
        if($result->num_rows) {
            $res = array();
            foreach($result->rows as $row) {
                $res[] = $row['key'];
            }
            return $res;
        }
        return false;

    }
    
    public function getTotalSerials($product_id) {
    	$query = sprintf('
            SELECT
                COUNT(`pid`) as `ttl`
            FROM
                `%1$sserials`
            WHERE
                `pid` = %2$s
            GROUP BY
                `pid`',
            DB_PREFIX,
            (int) $product_id
        );
        
        $result = $this->db->query($query);
        return empty($result->row['ttl']) ? 0 : $result->row['ttl'];
    }
    
}