<?php
################################################################################################
# Product xmlrpc Tab Opencart 1.5.1.x From Webkul  http://webkul.com    #
################################################################################################

class ModelCatalogErpProduct extends Model {

    public function check_all_products($userId, $client, $sock,$context){
        $db = $context['db'];
        $pwd = $context['pwd'];
        $cart_user = $context['cart_user'];
        $id_lang = $context['id_lang'];
        $is_error      = 0;
        $error_message = '';
        $ids = '';
        
        $data = $this->db->query("SELECT p.`weight`,p.`product_id`,p.`quantity`,p.`image`,p.`price`,p.`sku`,pd.`name`,pd.`description`,erp.`is_synch`,erp.`erp_product_id`
        FROM ". DB_PREFIX ."product p 
        LEFT JOIN `".DB_PREFIX."product_description` pd ON (pd.`product_id` = p.`product_id` AND pd.language_id =".$id_lang.")
        LEFT JOIN `".DB_PREFIX."erp_product_merge` erp ON (erp.`opencart_product_id` = p.product_id )        
        WHERE erp.`is_synch` IS NULL or erp.`is_synch` =1")->rows;
        if (count($data) == 0) {
            //Nothing to export
            return array(
                'is_error' => $is_error,
                'error_message' => $error_message,
                'value' => 0,
                '$ids' => $ids
            );
        }
        
        $temp_data =array();

        foreach ($data as $product_data){

                $erp_category_ids = $this->get_erp_category_id($userId, $client, $sock,$product_data['product_id'],$context);
                  
                $temp_data =array(
                            'name' => str_replace('+',' ',urlencode(html_entity_decode($product_data['name']))),
                            'description'=> str_replace('+',' ',urlencode(strip_tags(html_entity_decode($product_data['description'])))),
                            'price'=>$product_data['price'],
                            'weight'=>$product_data['weight'],
                            'image'=>$this->base64Image($product_data['image']),
                            'erp_category_ids'=>$erp_category_ids,
                            'sku'=>$product_data['sku'],
                            'cart_product_id'=>$product_data['product_id'],
                        );
        
                $product_quantity = $product_data['quantity'];
               
                if ($product_data['is_synch']!=1) 
                {  
                    $response_create = $this->create_product($userId, $client, $sock, $temp_data ,$context);
                    
                    if ($response_create['erp_id'] > 0) 
                        {
                            $this->addto_product_merge($response_create['erp_id'], $product_data['product_id'],$cart_user);
                            $this->create_product_quantity($userId, $client, $sock,$response_create['erp_id'],$product_quantity,$context);
                        }   
                        else 
                        {
                            $is_error = 1;
                            $error_message .= $response_create['error_message'] . ',';
                            $ids .= $product_data['product_id'] . ',';
                        }
                } 
                else 
                    {
                        $this->create_product_quantity($userId, $client, $sock,$product_data['erp_product_id'],$product_quantity,$context);
                        $update = $this->update_products($userId, $client, $sock, $temp_data,$product_data['erp_product_id'], $context); 
                        if ($update['value'] != True) 
                            {
                                $is_error = 1;
                                $error_message .= $update['error_message'] . ',';
                                $ids .= $product_data['product_id'] . ',';
                            }
                    }
        }
        return array(
            'is_error' => $is_error,
            'error_message' => $error_message,
            'value' => 1,
            'ids' => $ids
        );
    }

    public function create_product($userId, $client, $sock, $temp_data, $context){   
        $context_erp = array(
            'opencart' => new xmlrpcval('opencart', "string")
        );

        $type = 'product';
        if(isset($temp_data['type']))
            $type = $temp_data['type'];

        $arrayVal = array(
            'name' => new xmlrpcval($temp_data['name'],"string"),
            'list_price' => new xmlrpcval($temp_data['price'], "string"),
            'type' => new xmlrpcval($type, "string"),
            'categ_ids' => new xmlrpcval($temp_data['erp_category_ids'], "array"),
            'description' => new xmlrpcval($temp_data['description'], "string"),
            'default_code' => new xmlrpcval($temp_data['sku'], "string"),
            'weight' => new xmlrpcval($temp_data['weight'], "string"),
            'weight_net' => new xmlrpcval($temp_data['weight'], "string"),
            'opencart_id' => new xmlrpcval($temp_data['cart_product_id'], "int"),
            'method'=>new xmlrpcval('create', "string")
        );
        if(isset($temp_data['image'])){
            $arrayVal['image'] = new xmlrpcval($temp_data['image'], "string");
        }
        $msg_ser  = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval($context['db'], "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval($context['pwd'], "string"));
        $msg_ser->addParam(new xmlrpcval("extra.function", "string"));
        $msg_ser->addParam(new xmlrpcval("create_product", "string"));
        $msg_ser->addParam(new xmlrpcval($arrayVal,"struct"));
        $msg_ser->addParam(new xmlrpcval($context_erp, "struct"));
        $resp = $client->send($msg_ser);

        if ($resp->faultCode()) {
            $error_message = $resp->faultString();            
            return array(
                'error_message' => $error_message,
                'erp_id' => -1
            );
        } else {
            $val    = $resp->value()->me;
            $erp_id = $val['int'];
            return array(
                'erp_id' => $erp_id
            );
        }
    }

    //Code for update records
    public function update_products($userId, $client, $sock, $temp_data, $erp_product_id, $context){
        $context_erp = array(
            'opencart' => new xmlrpcval('opencart', "string")
        );
        $arrayVal         = array(
            'name' => new xmlrpcval($temp_data['name'], "string"),
            'list_price' => new xmlrpcval($temp_data['price'], "string"),
            'categ_ids' => new xmlrpcval($temp_data['erp_category_ids'], "array"),
            'description' => new xmlrpcval($temp_data['description'], "string"),
            'default_code' => new xmlrpcval($temp_data['sku'], "string"),
            'weight' => new xmlrpcval($temp_data['weight'], "string"),
            'weight_net' => new xmlrpcval($temp_data['weight'], "string"),
            'product_id'=>new xmlrpcval($erp_product_id, "int"),
            'method'=>new xmlrpcval('write', "string")
        );
        if(isset($temp_data['image'])){
            $arrayVal['image'] = new xmlrpcval($temp_data['image'], "string");
        }
        $msg_ser = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval($context['db'], "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval($context['pwd'], "string"));
        $msg_ser->addParam(new xmlrpcval("extra.function", "string"));
        $msg_ser->addParam(new xmlrpcval("create_product", "string"));
        $msg_ser->addParam(new xmlrpcval($arrayVal, "struct"));
        $msg_ser->addParam(new xmlrpcval($context_erp, "struct"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            return array(
                'error_message' => $error_message,
                'value' => False
            );
        } else {
            $this->db->query("UPDATE  `" . DB_PREFIX . "erp_product_merge` SET `is_synch`=0 where `opencart_product_id`='" . $temp_data['cart_product_id'] . "'");           
            
            return array(
                'value' => True
            );
        }
    }

    public function addto_product_merge($erp_product_id, $opencart_product_id,$cart_user = 'Default'){

       $this->db->query("INSERT INTO ".DB_PREFIX."erp_product_merge SET erp_product_id='".$erp_product_id."',opencart_product_id='".$opencart_product_id."',created_by='".$cart_user."'");
    }

    public function create_product_quantity($userId, $client, $sock,$erp_product_id,$product_quantity,$context){
        $arrayVal = array(
            'product_id' => new xmlrpcval($erp_product_id, "int"),
            'new_quantity' => new xmlrpcval($product_quantity, "string")
        );
        $msg_ser  = new xmlrpcmsg('execute');
        $msg_ser->addParam(new xmlrpcval($context['db'], "string"));
        $msg_ser->addParam(new xmlrpcval($userId, "int"));
        $msg_ser->addParam(new xmlrpcval($context['pwd'], "string"));
        $msg_ser->addParam(new xmlrpcval("extra.function", "string"));
        $msg_ser->addParam(new xmlrpcval("update_quantity", "string"));
        $msg_ser->addParam(new xmlrpcval($arrayVal, "struct"));
        $resp = $client->send($msg_ser);
        if ($resp->faultCode()) {
            $error_message = $resp->faultString();
            return;
        } else {
            return;
        }
    }

    public function get_erp_category_id($userId, $client, $sock,$cart_product_id,$context){	
        $db = $context['db'];
        $pwd = $context['pwd'];
        $cart_user = $context['cart_user'];
        $default_category_id = array(0 => new xmlrpcval(1, 'int'));
        $erp_category_ids = array();

        global $loader, $registry;

        $data = $this->db->query("SELECT `category_id` FROM ". DB_PREFIX ."product_to_category where product_id='".$cart_product_id."'")->rows;
        if (empty($data))
            return $default_category_id;
        else{
            $loader->model('catalog/erp_product_category');
            $category = $registry->get('model_catalog_erp_product_category'); 
            foreach ($data as $key => $value) {
                 $erp_category_id = $category->check_specific_category($value['category_id'], $userId, $client, $sock, $db, $pwd,$cart_user);
                 $erp_category_ids[] = new xmlrpcval((int)$erp_category_id, 'int');
            }             
            if (!$erp_category_ids)
                return $default_category_id;
            else
                return $erp_category_ids;
        }
    }

    // check to see if barcode is 13 digits long
    public function validate_ean13($barcode){
        if (!preg_match("/^[0-9]{13}$/", $barcode)) {
            return '';
        }
        $digits = $barcode;
        // 1. Add the values of the digits in the 
        // even-numbered positions: 2, 4, 6, etc.
        $even_sum = $digits[1] + $digits[3] + $digits[5] +
                    $digits[7] + $digits[9] + $digits[11];
        // 2. Multiply this result by 3.
        $even_sum_three = $even_sum * 3;
        // 3. Add the values of the digits in the 
        // odd-numbered positions: 1, 3, 5, etc.
        $odd_sum = $digits[0] + $digits[2] + $digits[4] +
                   $digits[6] + $digits[8] + $digits[10];
        // 4. Sum the results of steps 2 and 3.
        $total_sum = $even_sum_three + $odd_sum;
        // 5. The check character is the smallest number which,
        // when added to the result in step 4, produces a multiple of 10.
        $next_ten = (ceil($total_sum / 10)) * 10;
        $check_digit = $next_ten - $total_sum;
        // if the check digit and the last digit of the 
        // barcode are OK return true;
        if ($check_digit == $digits[12]) {
            return $barcode;
        }
        return '';
    }

    public function check_specific_product($product_id,$userId, $client, $sock, $context){
        $id_lang=1;
        $pd_id = $this->search_product($product_id);
        $error_message = '';
        if ($pd_id[0] <= 0) {
            $product_data          = $this->db->query("SELECT p.`weight`,p.`product_id`,p.`quantity`,p.`image`,p.`price`,p.`sku`,pd.`name`,pd.`description`,erp.`is_synch`,erp.`erp_product_id`
            FROM ". DB_PREFIX ."product p 
            LEFT JOIN `".DB_PREFIX."product_description` pd ON (pd.`product_id` = p.`product_id` AND pd.language_id =".$id_lang.")
            LEFT JOIN `".DB_PREFIX."erp_product_merge` erp ON (erp.`opencart_product_id` = p.product_id ) WHERE p.`product_id`='".$product_id."'")->row;           
            
            //load currency model and check specific 
            $this->load->model('catalog/erp_product_category');
            $erp_category_ids   = $this->get_erp_category_id($product_data['product_id'], $userId, $client, $sock , $context['db'], $context['pwd']);

            $product_array = array( 'cart_product_id'  => $product_id,  
                                    'price'            => $product_data['price'],           
                                    'description'      => urlencode(strip_tags(html_entity_decode($product_data['description']))),                           
                                    'image'            => $this->base64Image($product_data['image']),            
                                    'weight'           => $product_data['weight'],
                                    'name'             => urlencode(html_entity_decode($product_data['name'])),
                                    'erp_category_ids' => $erp_category_ids,
                                    'sku'     => $product_data['sku'],
                                  );

            if ($pd_id[0] == 0){
                $create = $this->create_product($userId, $client, $sock, $product_array, $context);

                if ($create['erp_id'] > 0){
                        $this->addto_product_merge($create['erp_id'], $product_id);
                        $this->create_product_quantity($userId, $client, $sock,$create['erp_id'],$product_data['quantity'],$context);
                        return array(
                            'erp_id' => $create['erp_id'],
                            'total_price' => $product_data['price'],
                            'is_error' => False,
                        );
                    }
                else
                    return array(
                    'is_error' => True,
                    );
                
            }else{
                $this->create_product_quantity($userId, $client, $sock,$pd_id[1],$product_data['quantity'],$context);
                $update = $this->update_products($userId, $client, $sock, $product_array, $pd_id[1],$context);
                if ($update['value'] != True){
                    $is_error = 1;
                    // $error_message .= $update['error_message'] . ','.  $product_id;
                    // // $ids .= $product_id . ',';

                    return array('is_error' => $update['error_message']  . ',' .  $product_id) ;
                }
                return array(
                    'erp_id'    => $pd_id[1],                    
                    'is_error' => False
                );
            }
        } 
        else 
            return array(
                'erp_id' => $pd_id[0],                
                'is_error' => False
                );
            
    }

    //To search product in product merge table.
    private function search_product($product_id){
        $product_id = $this->db->query("SELECT `erp_product_id`,`is_synch` from `" . DB_PREFIX . "erp_product_merge` where `opencart_product_id`='".$product_id."'")->row;

        if (isset($product_id['erp_product_id']) AND $product_id['erp_product_id'] > 0) {
            if ($product_id['is_synch'] == 1)
                return array(
                    -1,
                    $product_id['erp_product_id'],                    
                );
            else
                return array(
                    $product_id['erp_product_id'],                    
                );
        } else
            return array(
                0
            );
    }

    public function base64Image($path){
        $image_url = DIR_IMAGE.$path;
        // $type = pathinfo($image_url, PATHINFO_EXTENSION);
        // $data = file_get_contents($path);        
        // $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        // return $base64;
        
        $imageData = false;
        if (@file_get_contents($image_url)) {
            $content   = file_get_contents($image_url);
            $imageData = base64_encode($content);
        }
        return $imageData;        
    }

}
?>