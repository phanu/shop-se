<?php
class ModelCatalogFAQSystem extends Model {
	public function getCategories() {
		$sql = "SELECT c.*, cd.* FROM " . DB_PREFIX . "faq_category c 
				LEFT JOIN " . DB_PREFIX . "faq_category_description cd ON (c.category_id = cd.category_id)
				WHERE cd.language_id ='" . (int)$this->config->get('config_language_id') . "' AND c.status = 1 
				ORDER BY c.sort_order ASC";
		
		$query = $this->db->query($sql);

		return $query->rows;	
	}
	
	public function getQuestionsByCategoryId($category_id){
		$sql = "SELECT q.*, qd.* FROM " . DB_PREFIX . "faq_question q
				LEFT JOIN " . DB_PREFIX . "faq_question_description qd ON (q.question_id = qd.question_id)
				WHERE q.category_id ='" . (int)$category_id . "' AND qd.language_id ='" . (int)$this->config->get('config_language_id') . "' AND q.status = 1 
				ORDER BY q.sort_order ASC";
				
		$query = $this->db->query($sql);

		return $query->rows;	
	}
	
	public function proposeQuestion($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "faq_question SET status = 0");
		
		$question_id = $this->db->getLastId();
		
		$sql = "INSERT INTO " . DB_PREFIX . "faq_question_description 
				SET question_id ='" . (int)$question_id . "',
					language_id ='" . (int)$this->config->get('config_language_id') ."',
					title       ='" . $this->db->escape($data['question']) ."'";
					
		$this->db->query($sql);			
	}
}
?>