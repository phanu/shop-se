<div id="fg-controls">
	<div id="fg-next-selector"></div>
	<div id="fg-pager-selector"></div>
	<div id="fg-prev-selector"></div>
</div>

<div id="footer">
    <div class="product-grid" id="fg_ads">
      <?php foreach ($products as $product) { ?>
       <div class="no-borders">
	      <?php if ($product['price']) { ?>
		  <div class="price">
			<?php if (!$product['special']) { ?>
			<?php echo $product['price']; ?>
			<?php } else { ?>
			<span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
			<?php } ?>
		  </div>
		  <?php } ?>
		  <?php if ($product['thumb']) { ?>
		  <div class="image"><a href="<?php echo $product['href']; ?>" target="blank"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
		  <?php } ?>
		  <div class="name"><a href="<?php echo $product['href']; ?>" target="blank"><?php echo $product['name']; ?></a></div>
		</div>	
      <?php } ?>
    </div>
	
<?php if (count($products) > 1){ ?>
<script type="text/javascript">
  $(document).ready(function(){
	$('#fg_ads').bxSlider({
		speed: '500',
		pause: '10000',
		infiniteLoop: true,
		randomStart: true,
		pager: true,
		prevImage: '<?php echo $prev_image; ?>',
		nextImage: '<?php echo $next_image; ?>',
		prevSelector: '#fg-prev-selector',
		nextSelector: '#fg-next-selector',
		pagerType: 'short',
		pagerSelector: '#fg-pager-selector',
		displaySlideQty: 7,
		moveSlideQty: 7,
		auto: true,
		autoHover: true
	});
  });
</script>
<?php } ?>
 
</div>
</div>



<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '<?php echo $facebook_game_app_id; ?>', // App ID
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });
	
	FB.Event.subscribe('edge.create',
		function(response) {
			if ( $("#dialog-like").dialog("isOpen") === true ) {
				$("#dialog-like").dialog("close");
			}
		}
	);
	
	FB.Event.subscribe('auth.statusChange', function(response) {
		if (response.status == 'connected') {
			$('#login-message').hide();
			$('#game_board').show();
		} else {
			$('#login-message').show();
			$('#game_board').hide();
		}
	});
	
	FB.getLoginStatus(function(response) {
		if (response.status == 'connected') {
		
			$('#game_board').show();

			var user_id = response.authResponse.userID;
			var page_id = '<?php echo $facebook_game_page_id; ?>'; 
			var fql_query = 'SELECT uid FROM page_fan WHERE page_id =' + page_id +' and uid=' + user_id;
			var query = FB.Data.query(fql_query);

			query.wait(function(rows) {
				
				//alert('ROWS = ' + rows.length);
				
				if (rows.length == 1 && rows[0].uid == user_id) {
					 //$("#container_like").show();
					 // alert('already LIKED');

				} else {
					
				    /*
					$('#dialog-like').dialog({
						position: ['center', 10],
						width: 600,
						height: 400,
						modal: true,
						closeOnEscape: false,
						resizable: false
					});
					
					$(".ui-dialog-titlebar").hide();
					*/
				}
			});


		} else {
				// user is not logged
				$('#game_board').hide();
				$('#login-message').show();
		}
	});
	
  };

  // Load the SDK Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     d.getElementsByTagName('head')[0].appendChild(js);
   }(document));
</script>
   

</body></html>