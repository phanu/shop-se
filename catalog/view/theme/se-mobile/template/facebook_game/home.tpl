<?php echo $header; ?>
<div id="content"><?php echo $content_top; ?>

<style>
<?php foreach( $products_unique as $product ) { ?>
.imagep_<?php echo $product['product_id']; ?> {
	background: url('<?php echo $product['thumb']; ?>')  center center no-repeat;
}
<?php } ?>
</style>

<div id="game_board" style="width:<?php echo $board_css_width; ?>px">
	<div class="clear"></div>
	
	<?php $count = 0; ?>
	<?php foreach ($products as $product) { ?>
	<?php $count++; ?>
	<div class="card {toggle:'imagep_<?php echo $product['product_id']; ?>'}">					
		<div class="off" style="background: #fff url('<?php echo $logo; ?>') center center no-repeat;"></div>				
		<div class="on"><span class="price"><?php echo $product['price']; ?></span></div>
	</div>
	<?php if ($count == $board_cols) { ?>
	<div class="clear"></div>
	<?php } ?>
	<?php } ?>	
</div>

<?php //echo $content_bottom; ?></div>
<?php echo $footer; ?>