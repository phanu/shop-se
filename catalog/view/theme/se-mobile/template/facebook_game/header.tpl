<?php if (isset($_SERVER['HTTP_USER_AGENT']) && !strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6')) echo '<?xml version="1.0" encoding="UTF-8"?>'. "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" xml:lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />

<!--
<?php if ($facebook_game_charset) { ?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php } ?>
-->

<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<meta property="fb:app_id" content="<?php echo $facebook_game_app_id; ?>" />
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $theme_name; ?>/stylesheet/facebook_game_stylesheet.css" />
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/jquery/tabs.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.metadata.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.quickflip.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.facebook_game.js"></script>
<script type="text/javascript" src="catalog/view/javascript/facebook_game_common.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/swfobject.js"></script>

<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie7.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie6.css" />
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->

<script type="text/javascript">
	var flashvars = false;
	var attributes = {};
	var params = {
	  allowscriptaccess : "always",
	  wmode : "transparent",
	  menu: "false"
	};
	swfobject.embedSWF("<?php echo $swf_path; ?>", "sfx_movie", "1", "1", "9.0.0", "<?php echo $express_install_path; ?>", flashvars, params, attributes);
</script>

<?php echo $google_analytics; ?>
</head>
<body>
<div id="container">

<div id="floating-bar">
    <a href="<?php echo $home; ?>" id="home-button" class="trans-button" title="<?php echo $text_home; ?>"></a>
    <div id="help-button" class="trans-button" title="<?php echo $text_instruction; ?>"></div>
    <div id="rank-button" class="trans-button" title="<?php echo $text_rank; ?>"></div>
	
	<?php echo $fg_language; ?>
	
	<div id="control">
		<label><?php echo $text_level; ?></label>
		<select id="level_chooser">
		<?php for ($level=1; $level <= 6; $level++) { ?>
		<?php   if ($current_level == $level){ ?>	
			      <option value="<?php echo $level; ?>" selected="selected"><?php echo $level; ?></option>		
		<?php   } else { ?>
				  <option value="<?php echo $level; ?>"><?php echo $level; ?></option>	
        <?php   } ?> 		
        <?php } ?> 		
		</select>
		
		<label><?php echo $text_game_finished; ?> </label>
		<span>0</span>
				
		<label><?php echo $text_moves; ?></label>
		<span id="num_of_moves">0</span>
	</div>
</div>

<div id="sfx_movie">
	<h1>This page requires flash for full funcionality</h1>
	<p><a href="http://www.adobe.com/go/getflashplayer">
		<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
	</a></p>
</div>

<div id="login-message">
	<div class="warning" style="text-align:center;"><?php echo $text_login_required; ?> <a id="fb-login" class="button"><span><?php echo $button_ok_agree; ?></span></a></div>
</div>

<div id="dialog-like">
	<div class="fg-ld-header">
		<p><?php echo $text_unlock_game; ?></p>
		<p><?php echo $text_play_win; ?></p>
	</div>
	<div class="fg-ld-content">
		<div class="steps">
			<div class="step">
				<div class="name"><?php echo $text_how_to_play; ?></div>
				<div class="description"><?php echo $text_play_instruction; ?></div>
			</div>
			<div class="step">
				<div class="name"><?php echo $text_prizes; ?></div>
				<div class="description"><?php echo $text_prize_details; ?></div>
			</div>
		</div>
		<div class=""></div>	
	</div>
</div>

<div id="player_won">
	<div class="dialog-header"><?php echo $text_dtitle_win; ?></div>
	<div class="fg-pw-content">
		<div class="trophy"></div>
		<div class="message">
			<div id="win-points-description" class="description"></div>
			<div class="buttons">
				<div class="center"><a id="start_again" class="button"><span><?php echo $button_play_again; ?></span></a></div>
			</div>	
		</div>
	</div>
</div>

<div id="dialog-help">
	<div id="close-dialog-help" class="dialog-close"></div>
	<div class="dialog-header"><?php echo $text_dtitle_help; ?></div>
	<div class="fg-hd-content"><?php echo $text_help_instructions; ?><br /><?php echo $text_win_type; ?><br /><?php echo $text_prize_details; ?></div>
</div>

<div id="dialog-rank">
	<div id="close-dialog-rank" class="dialog-close"></div>
	<div class="dialog-header"><?php echo $text_dtitle_rank; ?></div>
	<div class="fg-rd-content">
		<div id="rank-table"></div>
	</div>
</div>
