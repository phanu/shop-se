<div id="tabs" class="htabs">
	<?php if ($show_today_rank) { ?>
	<a href="#tab-today-rank"><?php echo $tab_today; ?></a>
	<?php } ?>
	<?php if ($show_week_rank) { ?>
	<a href="#tab-week-rank"><?php echo $tab_week; ?></a>
	<?php } ?>
	<?php if ($show_month_rank) { ?>
	<a href="#tab-month-rank"><?php echo $tab_month; ?></a>
	<?php } ?>
</div>

<?php if ($show_today_rank) { ?>
<div id="tab-today-rank" class="tab-content">
	<table class="list">
		<thead>
			<tr>
				<td></td>
				<td class="center"><?php echo $column_participant; ?></td>
				<td class="center"><?php echo $column_points; ?></td>
			</tr>
		</thead>
		<tbody>
		<?php foreach($today_ranks as $participant) { ?>	
			<tr>
				<td class="left" width="1"><img src="http://graph.facebook.com/<?php echo $participant['fb_id']; ?>/picture" width="25" height="25" /></td>
				<td class="left"><?php echo $participant['name']; ?></td>
				<td class="center"><?php echo $participant['points']; ?></td>
			</tr>
		<?php } ?>	
		</tbody>
	</table>
</div>
<?php } ?>

<?php if ($show_week_rank) { ?>
<div id="tab-week-rank" class="tab-content">
	<table class="list">
		<thead>
			<tr>
				<td></td>
				<td class="center"><?php echo $column_participant; ?></td>
				<td class="center"><?php echo $column_points; ?></td>
			</tr>
		</thead>
		<tbody>
		<?php foreach($week_ranks as $participant) { ?>	
			<tr>
				<td class="left" width="1"><img src="http://graph.facebook.com/<?php echo $participant['fb_id']; ?>/picture" width="25" height="25" /></td>
				<td class="left"><?php echo $participant['name']; ?></td>
				<td class="center"><?php echo $participant['points']; ?></td>
			</tr>
		<?php } ?>	
		</tbody>
	</table>
</div>
<?php } ?>

<?php if ($show_month_rank) { ?>
<div id="tab-month-rank" class="tab-content">
	<table class="list">
		<thead>
			<tr>
				<td></td>
				<td class="center"><?php echo $column_participant; ?></td>
				<td class="center"><?php echo $column_points; ?></td>
			</tr>
		</thead>
		<tbody>
		<?php foreach($month_ranks as $participant) { ?>	
			<tr>
				<td class="left" width="1"><img src="http://graph.facebook.com/<?php echo $participant['fb_id']; ?>/picture" width="25" height="25" /></td>
				<td class="left"><?php echo $participant['name']; ?></td>
				<td class="center"><?php echo $participant['points']; ?></td>
			</tr>
		<?php } ?>	
		</tbody>
	</table>
</div>
<?php } ?>

<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script> 