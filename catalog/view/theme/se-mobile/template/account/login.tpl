<?php echo $header; ?>

      <div class="breadcrumbs animated fadeIn">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>

      <h2 class="product-title animated fadeIn"><?php echo $heading_title; ?></h2>

<?php if ($success) { ?>
            
            <script>
                        // create the notification
                        var notification = new NotificationFx({
                            message : '<p><?php echo $success; ?></p>',
                            layout : 'growl',
                            effect : 'slide',
                            type : 'notice'
                        });

                        notification.show();
                            
            </script>
<?php } ?>
<?php if ($error_warning) { ?>
            
            <script>
        
                        // create theotification
                        var notification = new NotificationFx({
                            message : '<p><?php echo $error_warning; ?></p>',
                            layout : 'growl',
                            effect : 'slide',
                            type : 'error'
                        });

                        notification.show();
                            
            </script>
<?php } ?>
      <!-- Login Section -->
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="login">
                <div class="page-block margin-bottom">
                    <span class="block semibold margin-bottom_low"><?php echo $text_returning_customer; ?></span>
                    <div class="input-field">
                        <input type="text" id="email" name="email">
                        <label for="email"><?php echo $entry_email; ?></label>
                    </div>
                    <div class="input-field">
                        <input type="password" id="passwd" name="password">
                        <label for="passwd"><?php echo $entry_password; ?></label>
                    </div>
                    <button class="btn orange margin-bottom_low" type="button" onclick="$('#login').submit();"><?php echo $button_login; ?></button>
                    <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                    <?php if ($redirect) { ?>
                    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                    <?php } ?>
                </div>
      </form>
      <!-- End Login Section -->
<script type="text/javascript">
$('#login input').keydown(function(e) {
  if (e.keyCode == 13) {
    $('#login').submit();
  }
});
</script>  
<?php echo $footer; ?>

