<?php echo $header; ?>
        
      <div class="breadcrumbs animated fadeIn">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>

      <h2 class="product-title animated fadeIn"><?php echo $heading_title; ?></h2>

  <h1 class="page-title"> สมาชิกหมายเลข : SE-<?php echo $customer_id; ?> </h1>

                <ul class="collapsible payment-summary" data-collapsible="accordion">
                    <li class="">
                        <div class="collapsible-header">
                            <span class="fa fa-caret-right"></span>
                            <div class="summary-item">
                                <span class="desc bold">ตรวจสอบสถานะ VIP </span>
                            </div>
                        </div>
                        <div class="collapsible-body" style="display: none;">
                            <?php echo $content_top; ?>
                        </div>
                    </li>
                </ul>
            <?php if ($success) { ?>

            <script>
        
                        // create the notification
                        var notification = new NotificationFx({
                            message : '<p><?php echo $success; ?></p>',
                            layout : 'growl',
                            effect : 'slide',
                            type : 'notice'
                        });

                        notification.show();
                            
            </script>
            <?php } ?>
 <!-- Contact Information Section -->
                <div class="panel-account margin-bottom">
                    <div class="heading"><?php echo $text_my_account; ?></div>
                    <div class="body">
                        <span class="block"></span>
                        <p><?php echo $text_edit; ?> - <a href="<?php echo $edit; ?>">คลิกเพื่อแก้ไข</a></p>
                        <p><?php echo $text_password; ?> - <a href="<?php echo $password; ?>">คลิกเพื่อแก้ไข</a></p>
                        <p><?php echo $text_address; ?> - <a href="<?php echo $address; ?>">คลิกเพื่อแก้ไข</a></p>
                        <p><?php echo $text_wishlist; ?> - <a href="<?php echo $wishlist; ?>">คลิกเพื่อแก้ไข</a></p>
                    </div>
                </div>
<!-- End Contact Information Section -->
 <!-- Contact Information Section -->
                <div class="panel-account margin-bottom">
                    <div class="heading"><?php echo $text_my_orders; ?></div>
                    <div class="body">
                        <span class="block"></span>
                        <p><?php echo $text_order; ?> - <a href="<?php echo $order; ?>">คลิกเพื่อแก้ไข</a></p>
                        <p><?php echo $text_download; ?> - <a href="<?php echo $download; ?>">คลิกเพื่อแก้ไข</a></p>
                        <p><?php echo $text_transaction; ?> - <a href="<?php echo $transaction; ?>">คลิกเพื่อแก้ไข</a></p>
                    </div>
                </div>
<!-- End Contact Information Section -->
                 <!-- Contact Information Section -->
                <div class="panel-account margin-bottom">
                    <div class="heading"><?php echo $text_my_newsletter; ?></div>
                    <div class="body">
                        <span class="block"></span>
                        <p><?php echo $text_newsletter; ?> - <a href="<?php echo $newsletter; ?>">คลิกเพื่อแก้ไข</a></p>
                    </div>
                </div>
<!-- End Contact Information Section -->
<?php echo $footer; ?>