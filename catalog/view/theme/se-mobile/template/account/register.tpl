<?php echo $header; ?>

      <div class="breadcrumbs animated fadeIn">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>
        
        <h1 class="page-title"><?php echo $heading_title; ?></h1>
        <span class="block semibold margin-bottom_low"><?php echo $text_account_already; ?></span>

    <?php if ($error_warning) { ?>
    <span class="highlight"><?php echo $error_warning; ?></span>
    <?php } ?>
            
                <!-- Custom (se-update.com) form container -->
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="register" class="shofy-form">
                    <p class="text-small"><span class="bold block"><?php echo $text_your_details; ?></span></p>
                        <div class="input-field">
                            <input type="text" name="firstname" id="firstname" value="<?php echo $firstname; ?>" />
                            <label for="firstname"><?php echo $entry_firstname; ?></label>
                              <?php if ($error_firstname) { ?>
                                  <code><?php echo $error_firstname; ?></code>
                              <?php } ?>
                        </div>
                        <div class="input-field">
                            <input type="text" name="lastname" id="lastname" value="<?php echo $lastname; ?>" />
                            <label for="lastname"><?php echo $entry_lastname; ?></label>
                              <?php if ($error_lastname) { ?>
                                  <code><?php echo $error_lastname; ?></code>
                              <?php } ?>
                        </div>
                   
                    <div class="input-field">
                        <input type="text" id="email" name="email" value="<?php echo $email; ?>">
                        <label for="email"><?php echo $entry_email; ?></label>
                        <?php if ($error_email) { ?>
                          <code><?php echo $error_email; ?></code>
                        <?php } ?>
                    </div>
                    <div class="input-field">
                        <input type="text" name="telephone" id="phone" value="<?php echo $telephone; ?>" />
                        <label for="phone"><?php echo $entry_telephone; ?></label>
                        <p class="text-small" style="margin-top:0px;">ตัวอย่าง 081-123-4567  (ใส่แค่หมายเลขเดียวเท่านั้น) </p>
                        <?php if ($error_telephone) { ?>
                        <code><?php echo $error_telephone; ?></code>
                        <?php } ?>
                    </div>
                    <div class="input-field">
                        <input type="text" name="fax" id="fax" value="<?php echo $fax; ?>" />
                        <label for="fax"><?php echo $entry_fax; ?></label>
                    </div>

                <p class="text-small"><span class="bold block"><?php echo $text_your_address; ?></span></p>
                    
                    <div class="input-field">
                        <input type="text" name="company" id="company" value="<?php echo $company; ?>" />
                        <label for="company"><?php echo $entry_company; ?></label>
                    </div>

                    <div class="input-field">
                        <input type="text" name="address_1" id="address_1" value="<?php echo $address_1; ?>" />
                        <label for="address_1"><?php echo $entry_address_1; ?></label><br/>
                        <p class="text-small" style="margin-top:0px;">***ช่องนี้ ควรมีเลขที่บ้าน แล้วตามด้วย หมู่ ซอย หรือถนน</p>
                        <?php if ($error_address_1) { ?>
                        <code><?php echo $error_address_1; ?></code>
                        <?php } ?>
                    </div>

                    <div class="input-field">
                        <input type="text" name="address_2" id="address_2" value="<?php echo $address_2; ?>" />
                        <label for="address_2"><?php echo $entry_address_2; ?></label>
                         <?php if ($error_address_2) { ?>
                        <code><?php echo $error_address_2; ?></code>
                        <?php } ?>
                    </div>

                    <div class="input-field">
                        <input type="text" name="city" id="city" value="<?php echo $city; ?>" />
                        <label for="city"><?php echo $entry_city; ?></label>
                         <?php if ($error_city) { ?>
                        <code><?php echo $error_city; ?></code>
                        <?php } ?>
                    </div>

                    <div class="input-field">
                        <input type="text" name="postcode" id="postcode" value="<?php echo $postcode; ?>" /><br/>
                        <label for="postcode"><?php echo $entry_postcode; ?></label>
                         <?php if ($error_postcode) { ?>
                        <code><?php echo $error_postcode; ?></code>
                        <?php } ?>
                    </div>

                    <div class="input-field">
                        <h3 class="out-label"><?php echo $entry_country; ?></h3>
                        <select class="browser-default" name="country_id" onchange="$('select[name=\'zone_id\']').load('index.php?route=account/register/zone&country_id=' + this.value + '&zone_id=<?php echo $zone_id; ?>');">
                              <option value=""><?php echo $text_select; ?></option>
                              <?php foreach ($countries as $country) { ?>
                              <?php if ($country['country_id'] == $country_id) { ?>
                              <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                              <?php } else { ?>
                              <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                              <?php } ?>
                              <?php } ?>
                        </select>
                            <?php if ($error_country) { ?>
                            <code><?php echo $error_country; ?></span>
                            <?php } ?>
                    </div>

                    <div class="input-field">
                        <h3 class="out-label"><?php echo $entry_zone; ?></h3>
                        <select class="browser-default" name="zone_id"></select>
                        <?php if ($error_zone) { ?>
                        <code><?php echo $error_zone; ?></code>
                        <?php } ?>
                    </div>

                    
                <p class="text-small"><span class="bold block"><?php echo $text_your_password; ?></span></p>

                    <div class="input-field">
                        <input type="password" name="password" id="passwd" value="<?php echo $password; ?>" />
                        <label for="passwd"><?php echo $entry_password; ?></label>
                            <?php if ($error_password) { ?>
                            <code><?php echo $error_password; ?></code>
                            <?php } ?>
                        
                    </div>

                    <div class="input-field">
                        <input type="password" name="confirm" id="confirm" value="<?php echo $confirm; ?>" />
                        <label for="confirm"><?php echo $entry_confirm; ?></label>
                            <?php if ($error_confirm) { ?>
                            <code><?php echo $error_confirm; ?></code>
                            <?php } ?>
                        
                    </div>

                    <div class="input-field">
                        <h3 class="out-label"><?php echo $text_newsletter; ?></h3>
                        <p>
                            <?php if ($newsletter == 1) { ?>
                                <input type="checkbox" id="test6" checked="checked" value="1" />
                                <?php } else { ?>
                                <input type="checkbox" id="test6" value="1" />
                                <?php } ?>
                                <label for="test6"><?php echo $entry_newsletter; ?></label>
                        </p>
                    </div>                    
                    <div class="input-field">
                        <button type="button" class="btn green" onclick="$('#register').submit();"><i class="fa fa-save"></i> Save</button>
                    </div>
                </div>
                <!-- End Custom Form -->
<script type="text/javascript"><!--
$('select[name=\'zone_id\']').load('index.php?route=account/register/zone&country_id=<?php echo $country_id; ?>&zone_id=<?php echo $zone_id; ?>');
</script> 
<?php echo $footer; ?>


