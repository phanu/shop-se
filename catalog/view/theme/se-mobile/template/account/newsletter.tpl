<?php echo $header; ?>

      <div class="breadcrumbs animated fadeIn">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>

      <h2 class="product-title animated fadeIn"><?php echo $heading_title; ?></h2>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="newsletter">
    <div class="content">
      <table class="form">
        <tr>
          <td><?php echo $entry_newsletter; ?></td>
          <td><?php if ($newsletter) { ?>
            <input type="radio" name="newsletter" value="1" checked="checked" id="defaulty" />
            <label for="defaulty"><?php echo $text_yes; ?></label>
            <input type="radio" name="newsletter" value="0" id="defaultn" />
            <label for="defaultn"><?php echo $text_no; ?></label>
            <?php } else { ?>
            <input type="radio" name="newsletter" value="1" id="defaulty" />
            <label for="defaulty"><?php echo $text_yes; ?></label>
            <input type="radio" name="newsletter" value="0" id="defaultn" checked="checked" />
            <label for="defaultn"><?php echo $text_no; ?></label>
            <?php } ?></td>
        </tr>
      </table>
    </div>
    <div class="buttons">
      <div class="left"><a href="<?php echo $back; ?>" class="btn light-blue darken-1"><span><?php echo $button_back; ?></span></a></div>
      <div class="center"><a onclick="$('#newsletter').submit();" class="btn light-blue darken-1"><span><?php echo $button_continue; ?></span></a></div>
    </div>
  </form><?php echo $footer; ?>