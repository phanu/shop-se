<?php echo $header; ?>

      <div class="breadcrumbs animated fadeIn">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>

      <h2 class="product-title animated fadeIn"><?php echo $heading_title; ?></h2>
  <?php if ($orders) { ?>
<!-- Order history listing (collapsible) -->
                <ul class="collapsible order-history" data-collapsible="accordion">
                  <?php foreach ($orders as $order) { ?>
                    <li>
                        <div class="collapsible-header">
                            <span class="indicator fa fa-caret-right"></span>
                            <?php $pos = strpos($order['status'], 'จัดส่ง'); ?>
                            <div class="order-status-<?php if($pos==true){ echo 'done'; } ?>">
                            <?php if($pos==true){ echo 'Done'; }else{ echo $order['status']; } ?>
                            <span class="fa <?php if($pos==true){ echo 'fa-check'; $active=1; }else{ echo 'fa-truck'; $active=2; } ?>"></span>
                            </div>
                            <div class="order-no">
                                <span class="block bold"><?php echo $text_order_id; ?> <a href="<?php echo $order['href']; ?>">#<?php echo $order['order_id']; ?></a></span>
                                <span class="block text-small"><?php echo $text_date_added; ?> <?php echo $order['date_added']; ?></span>
                            </div>
                        </div>
                        <div class="collapsible-body">
                            <ol>
                                <li>
                                    <div class="ctn">
                                        <h3><?php echo $text_customer; ?> <?php echo $order['name']; ?></h3>
                                        <span><?php echo $text_products; ?></span> <?php echo $order['products']; ?>
                                        <p><a href="<?php echo $order['href']; ?>" class="track-order"><?php echo $button_view; ?></a><br>
                                        <?php echo $text_total; ?> <?php echo $order['total']; ?></p>
                                    </div>
                                </li>
                            </ol>
                        </div>
                    </li>
                  <?php } ?>
                </ul>
                <!-- End Order history listing -->
           

  <span class="block"><?php echo $pagination; ?></span>
  <?php } else { ?>
  <span class="block"><?php echo $text_empty; ?></span>
  <?php } ?>

  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="btn light-blue darken-1"><span><?php echo $button_continue; ?></span></a></div>
  </div>
</div>
            <!-- END CONTENT CONTAINER -->
<?php echo $footer; ?>


