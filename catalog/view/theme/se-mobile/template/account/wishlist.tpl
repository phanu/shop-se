<?php echo $header; ?>

      <div class="breadcrumbs animated fadeIn">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>

      <h2 class="product-title animated fadeIn"><?php echo $heading_title; ?></h2>  
      
  <?php if ($products) { ?>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="wishlist">
     <!-- Cart Item List -->
                <ol class="cart-item cart-info">
                <?php foreach ($products as $product) { ?>
                    <li>
                        <!-- Cart Item  -->
                        <?php if ($product['thumb']) { ?>
                        <div class="thumb">
                            <img src="<?php echo $product['thumb']; ?>" alt="">
                        </div>
                        <?php } ?>
                        <div class="cart-delete" style="width:auto;">
                               <input type="checkbox" name="remove[]" value="<?php echo $product['product_id']; ?>" id="<?php echo $product['product_id']; ?>"/>
                               <label for="<?php echo $product['product_id']; ?>" >ลบ</label>
                        </div>
                        <div class="cart-detail">
                            <h3 class="product-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>
                            <?php if (!$product['stock']) { ?>
              <span class="stock">***</span>
              <?php } ?>
              <?php foreach ($product['option'] as $option) { ?>
                        - <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small><br />
                        <?php } ?>
                            <div class="price"><span>Price</span> 
                <?php if (!$product['special']) { ?>
                <price class="priceing"><?php echo $product['price']; ?></price>
                <?php } else { ?>
                 <span class="price-before"><?php echo $product['price']; ?></span> <price class="priceing"><?php echo $product['special']; ?></price>
                <?php } ?>
                            </div>
                        </div>
                    </li>
                    <!-- End Cart Item  -->
                <?php } ?>
              </ol>
  </form>
  <div class="buttons">
    <div class="left"><a href="<?php echo $back; ?>" class="btn light-blue darken-1"><span><?php echo $button_back; ?></span></a></div>
    <div class="center"><a onclick="$('#wishlist').submit();" class="btn light-blue darken-1"><span><?php echo $button_update; ?></span></a></div>
  </div>
  <?php } else { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <div class="buttons">
    <div class="center"><a href="<?php echo $continue; ?>" class="btn light-blue darken-1"><span><?php echo $button_continue; ?></span></a></div>
  </div>
  <?php } ?>  



<?php echo $footer; ?>