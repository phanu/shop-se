<?php echo $header; ?>

      <div class="breadcrumbs animated fadeIn">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>

      <!--<h2 class="product-title animated fadeIn"><?php echo $heading_title; ?></h2>-->
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>

  <!-- Tracking order navigation -->
                <div class="tracking-order-nav">
                    <span class="nav-left">
                    <span class="block"><?php echo $text_order_id; ?></span>
                    <span class="block semibold">#<?php echo $order_id; ?></span>
                    </span>
                    <span class="nav-right">
                    <span class="block"><?php echo $text_date_added; ?></span>
                    <span class="block semibold"><?php echo $date_added; ?></span>
                    </span>
                </div>
                <!-- End Tracking order navigation -->

                <!-- Tracking order list (collapsible) -->
                <ul class="collapsible tracking-order" data-collapsible="accordion">
                    <li>
                        <!-- Item #1 -->
                        <div class="collapsible-header">
                            <span class="indicator fa fa-caret-right"></span>
                            <div class="tracking-status">
                                <span class="fa <?php if($order_status_id=='3'||$order_status_id=='25'||$order_status_id=='26'){ echo 'fa-check'; $active=1; }else{ echo 'fa-truck'; $active=2; } ?>"></span> <?php echo $order_status_name; ?>
                            </div>
                            <div class="std-shipping">
                                <span class="block text-small"><?php echo $text_payment_method; ?> <?php echo $payment_method; ?></span>
                                <span class="block"><?php echo $text_shipping_method; ?> <?php echo $shipping_method; ?></span>
                                <span class="block semibold"><?php echo $history['date_modified']; ?></span>
                            </div>
                        </div>
                        <div class="collapsible-body">
                            <div class="tracking-process">
                                <div class="step">
                                    <div class="icon">
                                        <i class="fa fa-ship"></i>
                                    </div>
                                    <span class="name">Process</span>
                                </div>
                                <div class="step <?php if($active==1) echo 'active'; ?>">
                                    <div class="icon">
                                        <i class="fa fa-truck"></i>
                                    </div>
                                    <span class="name">Sending</span>
                                </div>
                                <div class="step <?php if($active==2) echo 'active'; ?>">
                                    <div class="icon">
                                        <i class="fa fa-check"></i>
                                    </div>
                                    <span class="name">Done</span>
                                </div>
                                <div class="lineconnect"></div>
                            </div>
                            <?php if ($histories) { ?>
                            <ol class="tracking-table">
                                <li class="heading">
                                    <?php echo $text_history; ?>
                                </li>
                                <?php foreach ($histories as $history) { ?>
                                <li>
                                    <span class="time"><?php echo $history['date_added']; ?></span> <a class="block semibold"><?php echo $history['status']; ?></a>
                                    <span class="block text-small">
                                    <?php echo $history['comment']; ?>
                                    </span>
                                </li>
                                <?php } ?>
                            </ol>
                            <?php } ?>
                            <span class="block semibold">รายการสินค้า:</span>
                            <ol class="package-contain">
                                <?php foreach ($products as $product) { ?>
                                <li>
                                    <div class="ctn">
                                        <h3><?php echo $product['name']; ?></h3>
                                        <p class="block text-small"><?php echo $column_model; ?> <?php echo $product['model']; ?> 
                                        <?php foreach ($product['option'] as $option) { ?> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?> <?php } ?><br>
                                        <span><?php echo $column_quantity; ?></span> <?php echo $product['quantity']; ?> 
                                        <span><?php echo $column_price; ?></span> <?php echo $product['price']; ?><br>
                                        <span><?php echo $column_total; ?></span><?php echo $product['total']; ?></p>
                                    </div>
                                </li>
                                <?php } ?>
                                <li>
                                  <?php foreach ($totals as $total) { ?>
                                   <?php if ($total['title'] == "จัดส่งด่วน Delivery") { $total['text'] = "ค่าจัดส่งตามระยะทาง"; } ?>
                                        <span class="block text-small"><b><?php echo $total['title']; ?>:</b> <?php echo $total['text']; ?></span>
                                   <?php } ?>
                                </li>
                            </ol>
                        </div>
                    </li>
                </ul>

                <div class="page-block checkout-shipping-block">
                    <h2 class="block-title">
                      <span>Shipping Address</span>
                    </h2>

                                <p>
                                    <span class="semibold"><?php echo $text_payment_address; ?></span>
                                    <a href="#change-address1" class="text-small modal-trigger">กดเพื่อดูที่อยู่บิล</a>
                                </p>
                                <?php if ($shipping_address) { ?>
                                <span class="block text-small">
                                  <?php echo $shipping_address; ?>
                                </span>
                                <?php }else{  echo $payment_address; } ?>
                                
                </div>

<?php if ($comment) { ?>
  <p><?php echo $text_comment; ?></p>
  <p><?php echo $comment; ?></p>
  <?php } ?>
  <div class="buttons">
    <div class="center"><a href="<?php echo $continue; ?>" class="btn light-blue darken-1"><span><?php echo $button_continue; ?></span></a></div>
  </div>
<?php echo $footer; ?>
<!-- Modal Structure -->
                                <div id="change-address1" class="modal modal-fixed-footer">
                                    <div class="modal-content">
                                        <h4><?php echo $text_payment_address; ?></h4>
                                        <p><?php echo $payment_address; ?></p>
                                    </div>
                                    <div class="modal-footer">
                                        <a class="modal-action modal-close btn-flat ">Close</a>
                                    </div>
                                </div>