<?php echo $header; ?>

      <div class="breadcrumbs animated fadeIn">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>

      <h2 class="product-title animated fadeIn"><?php echo $heading_title; ?></h2>
<?php if ($success) { ?>
            
            <script>
        
                        // create the notification
                        var notification = new NotificationFx({
                            message : '<p><?php echo $success; ?></p>',
                            layout : 'growl',
                            effect : 'slide',
                            type : 'notice'
                        });

                        notification.show();
                            
            </script>
<?php } ?>
<?php if ($error_warning) { ?>
            
            <script>
        
                        // create the notification
                        var notification = new NotificationFx({
                            message : '<p><?php echo $error_warning; ?></p>',
                            layout : 'growl',
                            effect : 'jelly',
                            type : 'notice'
                        });

                        notification.show();
                            
            </script>
<?php } ?>

  <h2><?php echo $text_address_book; ?></h2>
  <?php foreach ($addresses as $result) { ?>
  <div class="content">
    <table style="width: 100%;">
      <tr>
        <td><?php echo $result['address']; ?></td>
        <td style="text-align: right;"><a href="<?php echo $result['update']; ?>" class="btn light-blue darken-1"><span><?php echo $button_edit; ?></span></a> &nbsp; <a href="<?php echo $result['delete']; ?>" class="btn light-blue darken-1"><span><?php echo $button_delete; ?></span></a></td>
      </tr>
    </table>
  </div>
  <?php } ?>
  <div class="buttons">
    <div class="left"><a href="<?php echo $back; ?>" class="btn light-blue darken-1"><span><?php echo $button_back; ?></span></a></div>
    <div class="center"><a href="<?php echo $insert; ?>" class="btn light-blue darken-1"><span><?php echo $button_new_address; ?></span></a></div>
  </div>
<?php echo $footer; ?>