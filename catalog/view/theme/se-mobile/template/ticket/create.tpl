<?php echo $header; ?>

      <div class="breadcrumbs animated fadeIn">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>

      <h2 class="product-title animated fadeIn"><?php echo $heading_title; ?></h2>
  <div class="container" id="ticket-module" >
 <h1 style="float:left;margin-top: -5px;"><?php echo $heading_title; ?></h1>
 <a class="btn btn-success" style="color: white;float: right;" href="<?php echo $cancel; ?>"><span><?php echo $button_manage; ?></span></a>
        <div style="padding-top:50px;"></div>
       
<div class="">
<form method="post" action="<?php echo $action; ?>" enctype="multipart/form-data" id="form">
      	<div>
	        <div class="span2"><span class="required">*</span> <?php echo $entry_subject; ?></div>
	        <div ><input class="input-xxlarges" name="subject" type="text" value="<?php echo $subject; ?>" /></div>
              <?php if ($error_subject) { ?>
              <div class="span10 offset2"><span class="error"><?php echo $error_subject; ?></span></div>
              <?php } ?>
        </div>
            
        <div>
	        <div class="span2"><span class="required">*</span> <?php echo $entry_department; ?></div>
	        <div ><select name="department">
	        <?php foreach ($departments as $depart){ ?>
	        	<option <?php if ($depart['department_id'] == $department){ ?>selected="selected"<?php } ?> value="<?php echo $depart['department_id']; ?>"><?php echo $depart['department_name']; ?></option>
	        <?php } ?>
	        </select></div>
	    </div>
	    
	    <div>
	    	<div class="span2"><?php echo $entry_message; ?></div>
            <div ><textarea rows="3" name="message" id="message1"><?php echo $message; ?></textarea></div>
            <?php if ($error_message) { ?>
              <div class="span10 offset2"><span class="error"><?php echo $error_message; ?></span></div>
              <?php } ?>
	    </div>
	    <?php if($order_status){ ?>
	    <div>
	    	<div class="span2"><?php echo $entry_order; ?></div>
            <div ><select name="ticket_order_id">
            	<option value=""><?php echo $entry_empty_order ?></option>
            	<?php foreach($order as $row){ ?>
            		<option <?php if($row['order_id']==$ticket_order_id) echo "selected" ?> value="<?php echo $row['order_id'] ?>">#<?php echo $row['order_id'] ?></option>
            	<?php } ?>
            </select></div>
            <?php if ($error_order) { ?>
              <div class="span10 offset2"><span class="error"><?php echo $error_order; ?></span></div>
              <?php } ?>
	    </div>
	    <?php } ?>
	    
	    <div style="display:none;">
	        <div class="span2"><?php echo $entry_attach; ?></div>
	        <div class="span8 fileupload fileupload-new" data-provides="fileupload">
			  <div class="input-append">
			    <div class="uneditable-input span3">
				    <i class="icon-file fileupload-exists"></i> 
				    <span class="fileupload-preview"></span>
			    </div>
			    <span class="btn btn-file">
			    	<span class="fileupload-new"><?php echo $entry_select_file; ?></span>
			    	<span class="fileupload-exists"><?php echo $entry_change; ?></span>
			    	<input class="attach-file" type="file" name="file" />
			    </span>
			    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><?php echo $entry_remove; ?></a>
			  </div>
	        </div>
	        <?php if ($error_attach) { ?>
              <div class="span10 offset2"><span class="error"><?php echo $error_attach; ?></span></div>
              <?php } ?>
	    </div>
   
 <script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
<script type="text/javascript"><!--
CKEDITOR.replace('message1', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager',
	filebrowserUploadUrl: 'index.php?route=common/filemanager',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager'
});
//--></script> 
	    <script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
		 <script>
		    jQuery(function() {
		        for (var i=0; i<5; i++){
		        jQuery( "#datepicker_" + i ).datepicker();
		         jQuery( "#timepicker_" + i ).timepicker({
		                                                    hourGrid: 4,
		                                                    minuteGrid: 10
		                                            });
		       }
		    });
		    </script>
		

		<?php echo $formdata; ?>
		<div class="error span10"><?php echo $error_captcha; ?></div>
		<script type="text/javascript">
		 $('.contents .form').each(function( index ) {
			if($(this).html() == "") {
				$(this).parent().remove();
			}
			});
		jQuery(document).ready(function() {
			jQuery('#form').submit(function() {
			  return checkValidate();
			});
		});
		    function checkValidate(){
	    $flag = true;
	    jQuery('.require input').each(function(){
	    if(jQuery(this).val() === '' ) {
	    alert('Please enter ' + jQuery(this).parent().prev().text());
	    $flag = false;
	    jQuery(this).focus();
	    return false;
		}
		});
		if(!$flag) return false;
                                                                                        
                                                                                        
		jQuery('.require').each(function(){
			var checked = true;
			var element = '';
			jQuery(this).find('input[type=checkbox]').each(function(){
				checked = false;
				if(jQuery(this).prop('checked')) {
					element = jQuery(this);
					checked = true;
					return false;
				}
			});
			if(checked==false){
				alert('Please check ' + element.parent().prev().text());
				$flag = false;
				element.focus();
				return false;
			}
		});
		if(!$flag) return false;

		jQuery('.require').each(function(){
			var checked = true;
			var element = '';
			jQuery(this).find('input[type=radio]').each(function(){
				checked = false;
				if(jQuery(this).prop('checked')) {
					element = jQuery(this);
					checked = true;
					return false;
				}
			});
			if(checked==false){
				alert('Please check ' + element.parent().prev().text());
				$flag = false;
				element.focus();
				return false;
			}
		});
		if(!$flag) return false;

		jQuery('.require select[multiple=multiple]').each(function(){
			var checked = true;
			var element = jQuery(this);
			jQuery(this).find('option').each(function(){
				checked = false;
				if(jQuery(this).prop('selected')) {
					checked = true;
					return false;
				}
			});
			if(checked==false){
				alert('Please select' + element.parent().prev().text());
				$flag = false;
				element.focus();
				return false;
			}
		});
		if(!$flag) return false;
                                                                                        
                                                                                        
		jQuery('.require textarea').each(function(){
			if(jQuery(this).val() === '' ) {
				alert('Please enter ' + jQuery(this).parent().prev().text());
				$flag = false;
				jQuery(this).focus();
				return false;
			}
		});
		if(!$flag) return false;

		jQuery('.require select').each(function(){
			if(jQuery(this).val() === '' ) {
				alert('Please enter ' + jQuery(this).parent().prev().text());
				$flag = false;
				jQuery(this).focus();
				return false;
			}
		});
		if(!$flag) return false;

		    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			jQuery('.email  input').each(function(){
		 if(!emailReg.test(jQuery(this).val())) {
		 $flag = false; 

		            alert('Invalid Email ! ');
					jQuery(this).focus();
					return false; 
		        }
				
		});

		// return result
		if($flag){
		return true; 
		}else{
		return false; 
		}
     
	}
		</script>

	    <div style="clear:both;"></div>
	    <div class="span4 offset4" style="margin-top: 10px;margin-bottom:10px;">
	      <a class="btn btn-success btn-save" onclick="$('#form').submit();" ><span><?php echo $button_save; ?></span></a>
	      <a class="btn btn-danger" style="color: white;" href="<?php echo $cancel; ?>"><span><?php echo $button_cancel?></span></a>
	    </div>
      </form>

</div>
  
  <?php //echo $content_bottom; ?></div></div>
<?php echo $footer; ?>