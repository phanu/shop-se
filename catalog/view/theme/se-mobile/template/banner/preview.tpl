<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a  href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  
  <!-- Widget Start -->
  <div id="banner_privew">
<div id="Widget-<?=$banner_id?>"></div><script src="<?=$banner_load_link?>" type="text/javascript"></script>    
<!-- Widget End -->
  </div>
  
  <div class="banner-get-code-box">
  <h3><?=$banner_htmlcode_txt?></h3></h3>
  <label for="code_full_featured" class="help"><?=$banner_code_txt?></label>
    <table cellpadding="0" cellspacing="0">
      <tbody><tr>
        <td class="left-td">
        <?
      $txt = htmlspecialchars(
'<!-- Banner from '.HTTP_SERVER.' start -->
<div id="Widget-'.$banner_id.'"></div><script src="').$banner_load_link.htmlspecialchars('" type="text/javascript"></script>
<!-- Banner from '.HTTP_SERVER.' end -->');
      ?>
          <textarea onclick="$(this).select()" id="code_banner" rows="5"><?=$txt?></textarea>
        </td>
        <td>
          <?=$use_txt?><br /><br />
          <a href="<?=$create_new_link?>" class="button"><span><?=$create_new_text?></span></a>
           </td>
      </tr>  
      </tbody>
    </table>
  </div>
  
  <div class="buttons">
    <div class="left"><a href="<?=$back_link?>" class="button"><span><?=$back_to_options?></span></a></div>
        <div class="right"><a href="<?=$banners_list_link?>" class="button"><span><?=$banners_list_txt?></span></a></div>
  </div>
<?php echo $footer; ?>