if(!window.jQuery){
    var poll_<?=$banner_id?>;
    var timeout_<?=$banner_id?> = 100; 

    var headID = document.getElementsByTagName("head")[0];         
    var newScript = document.createElement('script');
    newScript.type = 'text/javascript';
    newScript.src = '<?=HTTP_SERVER?>system/bannerassets/jquery.min.js';
    headID.appendChild(newScript);


    poll_<?=$banner_id?> = function () {
      setTimeout(function () {
        timeout_<?=$banner_id?>--;
        if (typeof jQuery !== 'undefined') {
          start_load_<?=$banner_id?>();
        }
        else if (timeout_<?=$banner_id?> > 0) {
          poll_<?=$banner_id?>();
        }
        else {
          /* External library failed to load */
        }
      }, 100);
    };
    
    poll_<?=$banner_id?>();

} else {  
    jQuery(document).ready(function(){ 
        addBannerScript_<?=$banner_id?>();
    });
} 
function start_load_<?=$banner_id?>(){
    jQuery(document).ready(function(){ 
        addBannerScript_<?=$banner_id?>();
    });    
}

function addBannerScript_<?=$banner_id?>(){
<? require_once DIR_SYSTEM.'bannerassets/classy.js'; ?>
<? require_once DIR_SYSTEM.'bannerassets/jquery.tipTip.minified.js'; ?>

var Banner_<?=$banner_id?> = jQuery.Class.create({
    init : function(options){
		this.id=options.id;
        this.title=options.title;
        this.bm_link=options.bm_link;
        this.get_banner=options.get_banner;
        this.shop_name=options.shop_name;        
		this.TextError ="Can't load data. The server doesn't respond."; 
		this.TextError2="No products found.";
		this.CurrentPage=1;
        this.ProductsOnPage=options.onpage;
             
        this.ToolTip = 1; 
        this.Cache = {};     
        this.AddCSS();
        this.BuildBanner();
        _page = parseInt(this.CurrentPage);
        this.GetListing({ page : (this.CurrentPage) });
	},
    
    BuildBanner : function(){
        ID = '<?=$banner_id?>';
        tmpl = '   <div id="WidgetTitle-'+ID+'">'+this.title+'</div>';
        tmpl+= '   <div id="WidgetData-'+ID+'"></div>';
        tmpl+= '   <div class="WidgetBottom">';
        tmpl+= '        <div id="ControlPanel-'+ID+'">';
        tmpl+= '            <div style="visibility: hidden" id="ListBack-'+ID+'">&laquo; </div>';
        tmpl+= '            <div style="visibility: hidden" id="ListForward-'+ID+'"> &raquo;</div>';
        tmpl+= '        </div>';
        tmpl+= '        <div id="WidgetBottomLink-'+ID+'"><a target="_blank" rel="external" href="'+this.bm_link+'">Browse more from '+this.shop_name+'</a></div>';
        tmpl+= '        <div id="WidgetFooter-'+ID+'"><a target="_blank" rel="external" href="'+this.get_banner+'">Get Your Banner &raquo;</a></div>';
        tmpl+= '   </div>';
        jQuery('#Widget-<?=$banner_id?>').html(tmpl);  
        var _this = this;
        jQuery('#ListForward-<?=$banner_id?>').click(function(){
            _this.CycleForward();
        });
        jQuery('#ListBack-<?=$banner_id?>').click(function(){
            _this.CycleBack();
        });
    },
    
    AddCSS : function(){
        var s = document.createElement('link');
        s.rel="stylesheet";
        s.type = "text/css";
        s.href="<?=$get_css_link?>";
        jQuery('head').append(s);            
    },
    
    is_object : function ( mixed_var ){	
    	if(mixed_var instanceof Array) {
    		return false;
    	} else {
    		return (mixed_var !== null) && (typeof( mixed_var ) == 'object');
    	}
    },
    
    GetListing : function(ReqData){  
         /** check cash */
         page_ = ReqData['page'];
         if ( this.is_object(this.Cache[page_]) ) {
            data = this.Cache[page_];
            this.returnResponse_<?=$banner_id?>(data);
            return;
         }
         this.ShowLoad(); 
         var _this = this;
         returnResponse_<?=$banner_id?> = function(data){ 
            _this.returnResponse_<?=$banner_id?>(data);
         };   
         
         ReqDataUrl = '&';
         jQuery.each(ReqData, function(key,val){
            ReqDataUrl = ReqDataUrl+key+'='+val+'&'
         });

         var crossDomainUrl = '<?=$get_data_link?>'+ReqDataUrl+'pagelimit='+this.ProductsOnPage+'&jsoncallback=?';
         jQuery.ajax({ 
            url      : crossDomainUrl,
            dataType : 'jsonp'/*,
            error    : function (xhr, ajaxOptions, thrownError) {
                jQuery('#WidgetData-'+_this.id).html(_this.TextError+'<br />'+xhr+'<br />'+ajaxOptions+'<br />'+thrownError);
            }*/
         });
    },
    
    CycleForward : function (){
        _page = parseInt(this.CurrentPage)+1;
        this.GetListing({page : _page});
	},
	
	CycleBack : function (){
        _page = parseInt(this.CurrentPage)-1;
        this.GetListing({page : _page});
	},    
    
    returnResponse_<?=$banner_id?> : function(data){
        this.HideLoad();
        var _this = this;
        /** check for empty */
        if (data.products == 0){
            jQuery('#WidgetData-<?=$banner_id?>').hide().html(_this.TextError2).fadeIn('fast');  
            return ;          
        }
        /** add data to cash start*/
        page =parseInt( data.request.page );
        this.Cache[page] = data ;
        /** add data to cash END*/
        
        datatxt_<?=$banner_id?>='';
        jQuery.each(data.products, function(i,product){
            datatxt_<?=$banner_id?>+=_this.GetOneItem_<?=$banner_id?>(product);
        });
        
        datatxt_<?=$banner_id?>+='<div class=clr></div>';
        jQuery('#WidgetData-<?=$banner_id?>').hide().html(datatxt_<?=$banner_id?>).fadeIn('fast');
        
        this.AddToolTip();
        
        this.ManageControl_<?=$banner_id?>(data);
        if ( data.request.page ) this.CurrentPage = data.request.page;
    },
    
    ManageControl_<?=$banner_id?> : function(data){
        /** add control */
        if ( data.request.nextpage == 1 ){
            jQuery('#ListForward-<?=$banner_id?>').css('visibility','visible');
        } else {
            jQuery('#ListForward-<?=$banner_id?>').css('visibility','hidden');
        };  
        
        if ( data.request.page == 1 ){
            jQuery('#ListBack-<?=$banner_id?>').css('visibility','hidden');
        } else {
            jQuery('#ListBack-<?=$banner_id?>').css('visibility','visible');
        }      
    }, 
    
    AddToolTip : function(){
        if(this.ToolTip==1){
            jQuery(".p_tooltil").tipTip({
                'defaultPosition' : 'top',
                'delay' : '100'
            });
        }
    },
    
    GetOneItem_<?=$banner_id?> : function(product){ 
        tip_txt = '';
        if(this.ToolTip==1){
            tip_txt = '<div class=\'tip_i_wrap\'><div class=\'tip_i_price\'>'+product.price+product.special+'</div><img src=\''+product.thumb+'\' /></div>';
        }
        
        txt = '<div class="p_i_w">';
        txt+= ' <div class="pr_i">';  
        txt+= '  <div class="pr_i_image">';
        txt+= '      <a target="_blank" href="'+product.href+'" title="">';
        txt+= '         <img title="'+tip_txt+'" style="width:'+product.thumb_w+';height:'+product.thumb_h+'" class="p_tooltil" src="'+product.thumb+'"  alt="'+product.name+'" />';
        txt+= '     </a>';
        txt+= '  </div>';
        txt+= '  <div class="pr_i_des">';
        txt+= '      <div class="pr_i_des_h"><a target="_blank" href="'+product.href+'" title="'+product.name+'">'+product.name+'</a></div>';
        
        txt+= '  </div>';               
        txt+= ' </div>';
        txt+= '</div>';
        return txt;
    },
    
    ShowLoad : function(){
        /*jQuery('#WidgetData-'+this.id).fadeOut('fast', function(){
            jQuery(this).html('loading...').show();
        });*/
        jQuery('#WidgetData-'+this.id).html('loading...').show();
    },
    
    HideLoad:function(){
        
    }   
    
});
  
/* jQuery(document).ready(function(){ */
    jQuery('#Widget-<?=$banner_id?>').html('Loading...');  
    wid_<?=$banner_id?> = new Banner_<?=$banner_id?>({
        'id'         : '<?=$banner_id?>',
        'title'      : '<?=$banner_title?>',
        'bm_link'    : '<?=$bm_link?>',
        'get_banner' : '<?=$get_banner?>',
        'onpage'     : '<?=$onpage?>',
        'shop_name'  : "<?=$shop_name?>"
    });    
/* }); */
}