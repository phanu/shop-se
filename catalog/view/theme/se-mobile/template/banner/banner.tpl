<?php echo $header; ?>

      <div class="breadcrumbs animated fadeIn">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>

      <h2 class="product-title animated fadeIn"><?php echo $heading_title; ?></h2>
  <h1><?php echo $heading_title; ?></h1>
  <form id="create_banner" method="post" action="<?=$get_banner_action?>">
  <h2><span class="required">*</span> <?=$heading_title_option?></h2>
  <div class="content">
    <select id="banners_types" name="type">
          <option value=""><?=$select_banner_type?></option>
          <option <?=(($bi['type']==1)?'selected=""':'')?> value="1"><?=$most_recent_products?></option>
          <option <?=(($bi['type']==2)?'selected=""':'')?> value="2"><?=$featured_products?></option>
          <option <?=(($bi['type']==3)?'selected=""':'')?> value="3"><?=$recent_special?></option>
          <option <?=(($bi['type']==4)?'selected=""':'')?> value="4"><?=$random_featured?></option>
          <option <?=(($bi['type']==5)?'selected=""':'')?> value="5"><?=$random_special?></option>
          <option <?=(($bi['type']==6)?'selected=""':'')?> value="6"><?=$random?></option>
          <option <?=(($bi['type']==7)?'selected=""':'')?> value="7"><?=$all_products?></option>
     </select>   
     <?php if ($error_type) { ?>
        <span class="error"><?=$error_type?></span>
     <?php } ?> 
  </div>
  <h2><span class="required">*</span> <?=$heading_categories?></h2>
  <div class="content">
    <div class="scrollbox">
      <? $class = 'odd';?>
      <? $banner_categories = unserialize($bi['categories']); ?>
      <? $c=1; foreach ($categories as $category) { ?>
      <?  $class = ($class == 'even' ? 'odd' : 'even'); ?>
      <div class="<?php echo $class; ?>">
            <? if($c==1) {?>
            <input <?if(in_array(0,$banner_categories)) echo 'checked=""' ?> type="checkbox" name="categories[]" value="0"  />
            <strong><?=$without_category?></strong>
            <? $c++; } else { ?> 
                <input <?if(in_array($category['category_id'],$banner_categories)) echo 'checked=""' ?> type="checkbox" name="categories[]" value="<?php echo $category['category_id']; ?>"  />
                <?php echo $category['name']; ?>
            <? } ?>
      </div>
      <?php $c++; } ?>
    </div>
    <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a></td>
    <?php if ($error_categories) { ?>
        <br />
        <span class="error"><?=$error_categories?></span>
     <?php } ?> 
  </div>  
  
  <? if ($manufacturers){ ?>
  <h2><span class="required">*</span> <?=$heading_manufacturer?></h2>
  <div class="content">
    <div class="scrollbox">
      <? $class = 'odd';?>
      <? $banner_manufacturers = unserialize($bi['manufacturers']); ?>
      <? $c=1; foreach ($manufacturers as $manufacturer) { ?>
      <?  $class = ($class == 'even' ? 'odd' : 'even'); ?>
      <div class="<?php echo $class; ?>">
            <? if($c==1) {?>
                <input <?if(in_array(0, $banner_manufacturers)) echo 'checked=""' ?> type="checkbox" name="manufacturers[]" value="0"  />
                <strong><?=$without_manufacturer?></strong>
            <? $c++; } else { ?>       
                <input <?if(in_array($manufacturer['manufacturer_id'], $banner_manufacturers)) echo 'checked=""' ?> type="checkbox" name="manufacturers[]" value="<?=$manufacturer['manufacturer_id']?>"  />
                <?php echo $manufacturer['name']; ?>
            <? } ?>
      </div>
      <?php $c++; } ?>
    </div>
    <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a></td>
    <?php if ($error_manufacturers) { ?>
        <br />
        <span class="error"><?=$error_manufacturers?></span>
     <?php } ?>   
  </div>   
  <? } ?>
  
  <h2><span class="required">*</span> <?=$layout_header?></h2>
  <div class="content">
    <select id="banner_layout" class="inputbox" name="layout">
          <option value=""><?=$select_a_layout?></option>
          <option <?=(($bi['layout']==1)?'selected=""':'')?> value="1"><?=$sl_leaderboard?></option>
          <option <?=(($bi['layout']==2)?'selected=""':'')?> value="2"><?=$sl_banner?></option>
          <option <?=(($bi['layout']==3)?'selected=""':'')?> value="3"><?=$sl_half_banner?></option>
          <option <?=(($bi['layout']==4)?'selected=""':'')?> value="4"><?=$sl_skyscraper?></option>
          <option <?=(($bi['layout']==5)?'selected=""':'')?> value="5"><?=$sl_wide_skyscraper?></option>
          <option <?=(($bi['layout']==6)?'selected=""':'')?> value="6"><?=$sl_small_rectangle?></option>
          <option <?=(($bi['layout']==7)?'selected=""':'')?> value="7"><?=$sl_vertical_banner?></option>
          <option <?=(($bi['layout']==8)?'selected=""':'')?> value="8"><?=$sl_small_square?></option>
          <option <?=(($bi['layout']==9)?'selected=""':'')?> value="9"><?=$sl_square?></option>
          <option <?=(($bi['layout']==10)?'selected=""':'')?> value="10"><?=$sl_medium_rectangle?></option>
          <option <?=(($bi['layout']==11)?'selected=""':'')?> value="11"><?=$sl_large_rectangle?></option>
     </select> [ <a class="bannerlayout_tip" href="image/bannerlayout.png"><?=$sl_preview_banner?></a> ]  
     <script>
      $(document).ready(function(){
            $(".bannerlayout_tip").colorbox({
                width : '800px'
            });    
      });     
     </script>
     <?php if ($error_layout) { ?><br />
        <span class="error"><?=$error_layout?></span>
     <?php } ?> 
  </div>  

  <h2><span class="required">*</span> Select a Color Scheme</h2>
  <div class="content">
    <select id="color_scheme" class="inputbox" name="color_scheme">
        <option value=""> - Select a Color Scheme - </option>
        <option <?=(($bi['color_scheme']==1)?'selected=""':'')?> value="1">Light Grey</option>
        <option <?=(($bi['color_scheme']==2)?'selected=""':'')?> value="2">Light Warm Yellow</option>
        <option <?=(($bi['color_scheme']==3)?'selected=""':'')?> value="3">Light Green Blue</option>
        <option <?=(($bi['color_scheme']==4)?'selected=""':'')?> value="4">Light Green</option>
        <option <?=(($bi['color_scheme']==5)?'selected=""':'')?> value="5">Light Grey Blue</option>
    </select><br />

<? foreach($colorSchemes AS $k=>$colorScheme){ $k++; ?>
    <input type="hidden" id="border_<?=$k?>" value="<?=$colorScheme['border']?>" />
    <input type="hidden" id="title_<?=$k?>" value="<?=$colorScheme['title']?>" />
    <input type="hidden" id="title_background_<?=$k?>" value="<?=$colorScheme['title_background']?>" />
    <input type="hidden" id="background_<?=$k?>" value="<?=$colorScheme['background']?>" />
    <input type="hidden" id="text_<?=$k?>" value="<?=$colorScheme['text']?>" />
    <input type="hidden" id="link_<?=$k?>" value="<?=$colorScheme['link']?>" />
    <input type="hidden" id="link_hover_<?=$k?>" value="<?=$colorScheme['link_hover']?>" />
    <input type="hidden" id="link_bottom_<?=$k?>" value="<?=$colorScheme['link_bottom']?>" />
    <input type="hidden" id="footer_background_<?=$k?>" value="<?=$colorScheme['footer_background']?>" />
    <input type="hidden" id="link_footer_<?=$k?>" value="<?=$colorScheme['link_footer']?>" />
<? } ?>
<br />
<table style="width: 100%;" id="color_scheme_colors">
    <tbody>
   <tr>
      <td class="ftitle" id="id_border"><span class="required">*</span> <?=$cs_border?></td>
   </tr>
   <tr>
      <td><input type="text" id="border" style="width: 155px" maxlength="7" class="inputbox" size="15" value="<?=$bi['sc_border']?>" name="border"/>&nbsp;&nbsp;<button class="widget-color-button" id="border_button" style="background: <?=$bi['sc_border']?>;" lang="border" type="button">...</button>
     <?php if ($error_border) { ?><br />
        <span class="error"><?=$error_border?></span>
     <?php } ?>       
      </td>
   </tr>
   
   <tr>
      <td class="ftitle" id="id_title"><span class="required">*</span> <?=$cs_title?></td>
   </tr>
   <tr>
      <td><input type="text" id="title" style="width: 155px" maxlength="7" class="inputbox" size="15" value="<?=$bi['sc_title']?>" name="title"/>&nbsp;&nbsp;<button class="widget-color-button" id="title_button" style="background: <?=$bi['sc_title']?>;" lang="title" type="button">...</button>
     <?php if ($error_title) { ?><br />
        <span class="error"><?=$error_title?></span>
     <?php } ?>        
      </td>
   </tr>
   
   <tr>
      <td class="ftitle" id="id_title_background"><span class="required">*</span> <?=$cs_title_background?></td>
   </tr>
   <tr>
      <td><input type="text" id="title_background" style="width: 155px" maxlength="7" class="inputbox" size="15" value="<?=$bi['sc_title_background']?>" name="title_background"/>&nbsp;&nbsp;<button style="background: <?=$bi['sc_title_background']?>;" class="widget-color-button" id="title_background_button" lang="title_background" type="button">...</button>
     <?php if ($error_title_background) { ?><br />
        <span class="error"><?=$error_title_background?></span>
     <?php } ?>         
      </td>
   </tr>
   
   <tr>
      <td class="ftitle" id="id_background"><span class="required">*</span> <?=$cs_background?></td>
   </tr>
   <tr>
      <td><input type="text" id="background" style="width: 155px" maxlength="7" class="inputbox" size="15" value="<?=$bi['sc_background']?>" name="background"/>&nbsp;&nbsp;<button class="widget-color-button" id="background_button" style="background: <?=$bi['sc_background']?>;" lang="background"  type="button">...</button>
     <?php if ($error_background) { ?><br />
        <span class="error"><?=$error_background?></span>
     <?php } ?>         
      </td>
   </tr>
   
   <tr>
      <td class="ftitle" id="id_text"><span class="required">*</span> <?=$cs_text?></td>
   </tr>
   <tr>
      <td><input type="text" id="text" style="width: 155px" maxlength="7" class="inputbox" size="15" value="<?=$bi['sc_text']?>" name="text" readonly="true"/>&nbsp;&nbsp;<button class="widget-color-button" id="text_button" style="background: <?=$bi['sc_text']?>" lang="text" type="button">...</button>
     <?php if ($error_text) { ?><br />
        <span class="error"><?=$error_text?></span>
     <?php } ?>    
      </td>
   </tr>
   
   <tr>
      <td class="ftitle" id="id_link"><span class="required">*</span> <?=$cs_link?></td>
   </tr>
   <tr>
      <td><input type="text" id="link" style="width: 155px" maxlength="7" class="inputbox" size="15" value="<?=$bi['sc_link']?>" name="link"/>&nbsp;&nbsp;<button class="widget-color-button" id="link_button" lang="link" style="background: <?=$bi['sc_link']?>;" type="button">...</button>
     <?php if ($error_link) { ?><br />
        <span class="error"><?=$error_link?></span>
     <?php } ?>       
      </td>
   </tr>
   
   <tr>
      <td class="ftitle" id="id_link_hover"><span class="required">*</span> <?=$cs_link_hover?></td>
   </tr>
   <tr>
      <td><input type="text" id="link_hover" style="width: 155px" maxlength="7" class="inputbox" size="15" value="<?=$bi['sc_link_hover']?>" name="link_hover"/>&nbsp;&nbsp;<button class="widget-color-button" id="link_hover_button"  style="background: <?=$bi['sc_link_hover']?>;"  lang="link_hover" type="button">...</button>
     <?php if ($error_link_hover) { ?><br />
        <span class="error"><?=$error_link_hover?></span>
     <?php } ?>        
      </td>
   </tr>
   
   <tr>
      <td class="ftitle" id="id_link_bottom"><span class="required">*</span> <?=$cs_link_bottom?></td>
   </tr>
   <tr>
      <td><input type="text" id="link_bottom" style="width: 155px" maxlength="7" class="inputbox" size="15" value="<?=$bi['sc_link_bottom']?>" name="link_bottom"/>&nbsp;&nbsp;<button class="widget-color-button" id="link_bottom_button" style="background: <?=$bi['sc_link_bottom']?>;" lang="link_bottom" type="button">...</button>
     <?php if ($error_link_bottom) { ?><br />
        <span class="error"><?=$error_link_bottom?></span>
     <?php } ?>         
      </td>
   </tr>
   
   <tr>
      <td class="ftitle" id="id_footer_background"><span class="required">*</span> <?=$cs_footer_background?></td>
   </tr>
   <tr>
      <td><input type="text" id="footer_background" style="width: 155px" maxlength="7" class="inputbox" size="15" value="<?=$bi['sc_footer_background']?>" name="footer_background"/>&nbsp;&nbsp;<button style="background: <?=$bi['sc_footer_background']?>;" class="widget-color-button" id="footer_background_button" lang="footer_background" type="button">...</button>
     <?php if ($error_footer_background) { ?><br />
        <span class="error"><?=$error_footer_background?></span>
     <?php } ?>         
      </td>
   </tr>
   
   <tr>
      <td class="ftitle" id="id_link_footer"><span class="required">*</span> <?=$cs_link_footer?></td>
   </tr>
   <tr>
      <td><input type="text" id="link_footer" style="width: 155px" maxlength="7" class="inputbox" size="15" value="<?=$bi['sc_link_footer']?>" name="link_footer"/>&nbsp;&nbsp;<button class="widget-color-button" style="background: <?=$bi['sc_link_footer']?>;" id="link_footer_button" lang="link_footer" type="button">...</button>
     <?php if ($error_link_footer) { ?><br />
        <span class="error"><?=$error_link_footer?></span>
     <?php } ?>           
      </td>
   </tr>
   </tbody></table>
    <script>
    $('#color_scheme').change(function(){
        id_scheme = $(this).val();
        $('#border').val( $('#border_'+id_scheme).val() );
        $('#title').val( $('#title_'+id_scheme).val() );
        $('#title_background').val( $('#title_background_'+id_scheme).val() );
        $('#background').val( $('#background_'+id_scheme).val() );
        $('#text').val( $('#text_'+id_scheme).val() );
        $('#link').val( $('#link_'+id_scheme).val() );
        $('#link_hover').val( $('#link_hover_'+id_scheme).val() );
        $('#link_bottom').val( $('#link_bottom_'+id_scheme).val() );
        $('#footer_background').val( $('#footer_background_'+id_scheme).val() );
        $('#link_footer').val( $('#link_footer_'+id_scheme).val() );
        
        $('#border_button').css('background', $('#border_'+id_scheme).val() );
        $('#title_button').css('background', $('#title_'+id_scheme).val() );
        $('#title_background_button').css('background', $('#title_background_'+id_scheme).val() );
        $('#background_button').css('background', $('#background_'+id_scheme).val() );
        $('#text_button').css('background', $('#text_'+id_scheme).val() );
        $('#link_button').css('background', $('#link_'+id_scheme).val() );
        $('#link_hover_button').css('background', $('#link_hover_'+id_scheme).val() );
        $('#link_bottom_button').css('background', $('#link_bottom_'+id_scheme).val() );
        $('#footer_background_button').css('background', $('#footer_background_'+id_scheme).val() );
        $('#link_footer_button').css('background', $('#link_footer_'+id_scheme).val() );        
    });
    
    $('.widget-color-button').ColorPicker({
	onSubmit: function(hsb, hex, rgb, el) {
        $('#'+$(el).attr('lang')).val('#'+hex);
        $(el).css('background', '#'+hex );
		$(el).ColorPickerHide();  
	},
	onBeforeShow: function () {
	    id_input = $(this).attr('lang');
		$(this).ColorPickerSetColor( $('#'+id_input).val());
	}
    });
    </script>
  </div>  

    <h2><span class="required">*</span> <?=$banner_title?></h2>
     <div class="content">
        <input style="width: 155px;" value="<?=$bi['banner_title']?>" type="text" name="banner_title" id="banner_title" />   
     <?php if ($error_banner_title) { ?><br />
        <span class="error"><?=$error_banner_title?></span>
     <?php } ?>                
     </div>

    <h2><span class="required">*</span> <?=$banner_domain_name?></h2>
     <div class="content">
        <input value="<?=$bi['domain_name']?>" style="width: 155px;" type="text" name="domain_name" id="domain_name" />   
     <?php if ($error_domain_name) { ?><br />
        <span class="error"><?=$error_domain_name?></span>
     <?php } ?>              
     </div>
     
     <h2><span class="required">*</span> <?=$banner_language?></h2>
     <div class="content">
         <select id="banner_language" class="inputbox" name="language">
            <option value=""><?=$banner_language_select?></option>
            <? foreach($languages AS $language){ ?>
            <option <?=(($bi['language']===$language['language_id'])?'selected=""':'')?> value="<?=$language['language_id']?>"><?=$language['name']?></option>
            <? } ?>
         </select>                 
     <?php if ($error_language) { ?><br />
        <span class="error"><?=$error_language?></span>
     <?php } ?>              
     </div>
    <? if ($banner_id){ ?>
     <h2><span class="required">*</span> <?=$banner_code_txt?></h2>
     <div class="content banner-get-code-box">
        <label for="code_full_featured" class="help"><?=$banner_code_txt?></label>
        <input type="hidden" value="<?=$banner_id?>" name="banner_id" />
        <table cellpadding="0" cellspacing="0">
          <tbody><tr>
            <td class="left-td">
            <?
          $txt = htmlspecialchars('<div id="Widget-'.$banner_id.'"></div><script src="').$this->url->link('affiliate/showbanner/load', 'b='.$banner_id).htmlspecialchars('" type="text/javascript"></script>');
          ?>
              <textarea onclick="$(this).select()" id="code_banner" rows="5"><?=$txt?></textarea>
            </td>
            <td>
              <?=$use_txt?><br />
               </td>
          </tr>  
          </tbody>
        </table>
     </div>    
    <? } ?>
  <div class="buttons">
    <div class="left">
        <input type="submit" value="<?=$get_code_txt?>" class="button" />
    </div>
  </div>    
  </form><?php echo $footer; ?>