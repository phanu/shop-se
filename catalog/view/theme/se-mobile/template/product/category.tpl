﻿<?php echo $header; ?>
<?php //echo $content_top_se_mobile; ?>
<!-- CONTENT CONTAINER -->

	<!-- Product breadcrumb -->
	<div class="breadcrumbs animated fadeIn">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
	<!-- End Product breadcrumb -->
	<h1 class="page-title animated fadeIn"><?php echo $heading_title; ?></h1>
	<?php if ($thumb || $description) { ?>
	<div class="category-info">
		<?php if ($thumb) { ?>
		<div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></div>
		<?php } ?>
		<?php if ($description) { ?>
		<div class="incategory"><?php echo $description; ?></div>
		<?php } ?>
	</div>
	<?php } ?>
	<!-- Category Listing -->
	<ol class="category-list animated fadeLeft">
		<?php $i=1; foreach ($categories as $category) { ?>
		<li><!-- Category list item # -->
		<div class="thumb">
			<a href="<?php echo $category['href']; ?>">
				<img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>">
			</a>
		</div>
		<div class="category-ctn">
			<div class="cat-name">
				<a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
			</div>
		</div>
		</li><!-- End Category list item # -->
		<?php $i++; } ?>
	</ol>
	<div class="clear"></div><!-- Use this class (.clear) to clearing float -->
	<?php if ($products) { ?>
	<!-- Product navigation -->
	<div class="product-list-navigation animated fadeInRight">
		<div class="product-num"><?php echo $pagination; ?></div>
		<div class="sorting-nav">
			<span class="label"><?php echo $text_limit; ?></span>
			<div class="sorting-dropdown">
				<select  class="browser-default" onchange="location = this.value;">
					<?php foreach ($limits as $limits) { ?>
					<?php if ($limits['value'] == $limit) { ?>
					<option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
					<?php } ?>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>
	<!-- End Product navigation -->
	
	<!-- Product Small List -->
	<ol class="product-small-list animated fadeInLeft">
		<?php foreach ($products as $product) { ?>
		<li>
			<!-- Item # -->
			<?php if ($product['thumb']) { ?>
			<div class="thumb">
				<?php if ($product['quantity'] <= 0) { ?>
				<a href="<?php echo $product['href']; ?>">
					<img src="<?php echo $product['soldout']; ?>" alt="<?php echo $product['name']; ?>" class="soldout"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
				</a>
				<?php }else{ ?>
				<a href="<?php echo $product['href']; ?>">
					<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
				</a>
				<?php } ?>
			</div>
			<?php } ?>
			<div class="product-ctn">
				<div class="product-name">
					<a href="<?php echo $product['href']; ?>">
						<?php echo $product['name']; ?>
					</a>
				</div>
				<?php if ($product['zero_price'] > 0) { ?>
				<div class="price">
					<?php if ($product['special']) { ?>
					<span class="price-before">
						<?php
							echo $text_price; echo $product['price'];
						?>
					</span>
					<span class="price-current"><?php echo $text_price; ?> <?php echo $product['special']; ?></span>
					<?php }else{ ?>
					<span class="price-current"><?php echo $text_price;?> <?php echo $product['price']; ?></span>
					<?php } ?>
					<?php if ($product['tax']) { ?>
					<span class="price-current"><?php echo $text_tax;?> <?php echo $product['tax']; ?></span>
					<?php } ?>
				</div>
				<?php }else{ ?>
				<div class="price">
					<img src="./image/call_for_price.png" align="absmiddle" /> <?php echo $text_zero_price; ?>
				</div>
				<?php } ?>
				<?php if (($product['quantity'] <= 0)&&($product['stock_status'] == "สินค้าหยุดการผลิต")) { ?>
				<div class="cart"><a class="zero_button"><span><?php echo $product['stock_status']; ?></span></a></div>
				<?php } else {?>
				<div class="cart"><a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button"><span><?php echo $button_cart; ?></span></a></div>
				<?php } ?>
				<div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist; ?></a></div>
			</div>
			
		</li>
		<!-- End Item # -->
		<?php } ?>
	</ol>

<?php if (!$categories && !$products) { ?>
<h1><?php echo $text_empty; ?></h1>
<a href="<?php echo $continue; ?>"><button class="btn green block" type="button">register</button></a>
<?php } ?>
<?php } ?>
</div>
<?php ////echo $content_bottom; ?>
<script type="text/javascript"><!--
/*function display(view) {
	if (view == 'list') {
		$('.product-grid').attr('class', 'product-list');
		
		$('.product-list > div').each(function(index, element) {
			html  = '<div class="right">';
			html += '  <div class="cart">' + $(element).find('.cart').html() + '</div>';
			html += '  <div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
			
						html += '</div>';
			
			html += '<div class="left">';
			
			var image = $(element).find('.image').html();
			
			if (image != null) {
				html += '<div class="image">' + image + '</div>';
			}
			
			var price = $(element).find('.price').html();
			
			if (price != null) {
				html += '<div class="price">' + price  + '</div>';
			}
					
			html += '  <div class="name">' + $(element).find('.name').html() + '</div>';
			html += '  <div class="description">' + $(element).find('.description').html() + '</div>';
			
				
			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="rating">' + rating + '</div>';
			}
				
			
			
			html += '</div>';
						
			$(element).html(html);
				});
		
$('.display').html('<b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display(\'grid\');"><?php echo $text_grid; ?></a>');

$.cookie('display', 'list');
} else {
$('.product-list').attr('class', 'product-grid');

$('.product-grid > div').each(function(index, element) {
html = '';

var image = $(element).find('.image').html();

if (image != null) {
html += '<div class="image">' + image + '</div>';
}

html += '<div class="name">' + $(element).find('.name').html() + '</div>';
html += '<div class="description">' + $(element).find('.description').html() + '</div>';

var rating = $(element).find('.rating').html();

if (rating != null) {
html += '<div class="rating">' + rating + '</div>';
}


var price = $(element).find('.price').html();

if (price != null) {
html += '<div class="price">' + price  + '</div>';
}





html += '<div class="cart">' + $(element).find('.cart').html() + '</div>';
html += '<div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';


$(element).html(html);
});

$('.display').html('<b><?php echo $text_display; ?></b> <a onclick="display(\'list\');"><?php echo $text_list; ?></a> <b>/</b> <?php echo $text_grid; ?>');

$.cookie('display', 'grid');
}
}
view = $.cookie('display');
if (view) {
display(view);
} else {
display('grid');
}
//-->*/
</script> <?php echo $footer; ?>