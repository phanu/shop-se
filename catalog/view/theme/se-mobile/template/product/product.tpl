<?php echo $header;  ?>
<?php if ($nwa_enabled){ ?>
      
          <script type="text/javascript">

        var nwa_options= <?php echo json_encode($nwa_options);?>;
        var nwa_title ='';

          </script>
      
      <?php } ?>
<?php //echo $content_top_se_mobile; ?>
  
<div class="content-header">
      <!-- Product breadcrumb -->
      <div class="breadcrumbs animated fadeIn">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>
    <!-- End Product breadcrumb -->
    <h2 class="product-title animated fadeIn"><?php echo $heading_title; ?></h2>
     <!-- Product title -->
    <div class="product-info">
        <?php if ($thumb || $images) { ?>
        <!-- Product thumbnail slider -->
        <ul class="product-slider animated fadeInRight">
            <!-- Single thumbnail -->
            <?php if ($thumb) { ?>
            <li>
                <a class="fullscreen-icon swipebox" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>">
                    <i class="fa fa-expand"></i>
                </a>
                <?php if ($product_info['quantity'] <= 0) { ?>
   <img src="<?php echo $soldout; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" class="soldout" /></a>
            <?php } ?>
                <img src="<?php echo $popup; ?>" alt="img" />
            </li>
            <?php } ?>
            <?php foreach ($images as $image) { ?>
            <li>
                <a class="fullscreen-icon swipebox" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>">
                    <i class="fa fa-expand"></i>
                </a>
                <?php if ($product_info['quantity'] <= 0) { ?>
   <img src="<?php echo $soldout; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" class="soldout" /></a>
            <?php } ?>
                <img src="<?php echo $image['popup']; ?>" alt="img" />
            </li>
            <?php } ?>
        </ul>
        <!-- End single thumbnail -->
        
        <div class="slick-thumbs">
            <!-- Small thumb indicator -->
            <ul>
            <?php if ($thumb) { ?>
                <li>
                  <?php if ($product_info['quantity'] <= 0) { ?>
   <img src="<?php echo $soldout; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" class="soldout" /></a>
            <?php } ?>
                  <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" />
                </li>
            <?php } ?>
            <?php foreach ($images as $image) { ?>
                <li>
                <?php if ($product_info['quantity'] <= 0) { ?>
   <img src="<?php echo $soldout; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" class="soldout" /></a>
            <?php } ?>
                    <img src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" />
                </li>
            <?php } ?>
            </ul>
        </div>
        <!-- End small thumb indicator -->
        <?php } ?>
        <!-- End Product thumbnail slider -->

          <!-- Product meta -->
          <div class="product-meta animated fadeInUp">
              <div class="pdescription">
                  <?php if ($manufacturer) { ?>
                      <span><?php echo $text_manufacturer; ?></span> 
                      <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a>
                  <?php } ?>
                  <br><span><?php echo $text_model; ?></span> <?php echo $model; ?>
                  <br><span>SKU : </span><?php echo $product_info['sku']; ?>
                  <br><span><?php echo $text_stock; ?></span> 
                  <!-- Beside .in-stock class and .out-of-stock class -->
                    <span class="availability <?php if($stock=="มีสินค้า พร้อมจัดส่ง"){ echo 'in-stock'; }else{ echo 'out-of-stock'; }?>">
                          <?php echo $stock; ?>
                      </span>
                  <br>
                  <?php if ( $this->customer->getCustomerGroupId()==15){ ?>
                    <font color="red"> <span>จำนวนคงเหลือ : </span> <?php echo $product_info['quantity'] ?></font><br />
                  <?php } ?>
              </div>
              
              <?php if ($price) { ?>
              <div class="price1">
                  <span class="pprice1"><?php echo $text_price; ?> </span>
                  <?php if (!$special) { ?>
                    <?php echo $price; ?>
                  <?php }else { ?>
                      <span class="price-before">
                      <?php echo $price; ?>
                      </span>
                      <?php echo $special; ?>
                      <?php if ($tax) { ?>
                      <br /><span><?php echo $text_tax; ?> <?php echo $tax; ?></span> <br />
                      <?php } ?>
                      <?php if ($discounts) { ?>
                      <br />
                      <div class="discount">
                        <?php foreach ($discounts as $discount) { ?>
                        <?php echo sprintf($text_discount, $discount['quantity'], $discount['price']); ?><br />
                        <?php } ?>
                      </div>
                      <?php } ?>
                
                <?php } ?>
                </div>
              <?php } ?>

              <div class="price">
              </div>

              <?php if ($product_info['quantity'] > 0) { ?>
                      <font class="remind"> หากต้องการรับสินค้าที่ <a href="http://shop.se-update.com/SE-Update-Shop-Map" target="_blank"><b>หน้าร้าน</b></a> <br>กรุณาโทรจองสินค้าล่วงหน้า 1 วัน ที่ 092-540-3300</font>
              <?php } ?>
              <br>



<?php if ($options) { ?>
<div class="poptions">
  <ul class="collapsible payment-summary" data-collapsible="accordion">
    <li class="">
      <div class="collapsible-header">
        <span class="fa fa-caret-right"></span>
        <div class="summary-item">
          <span class="desc bold"><?php echo $text_option; ?></span>
          <span class="value bold"> </span>
        </div>
      </div>
      <div class="collapsible-body" style="display: none;">
        <div>หากสี หรือลายใดสินค้าหมด ท่านสามารถเลือกที่สินค้าหมด<br>เพื่อทำการกรอกอีเมลให้กับระบบ เมื่อสินค้ามาระบบจะทำการแจ้งให้ท่านทราบทางอีเมล<br>
        <br></br>  </div>
        <div class="options">
          <h2><?php echo $text_option; ?></h2>
          <br />
          <?php foreach ($options as $option) { ?>
          <?php if ($option['type'] == 'select') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br />
            <select name="option[<?php echo $option['product_option_id']; ?>]">
              <option value=""><?php echo $text_select; ?></option>
              <?php foreach ($option['option_value'] as $option_value) { ?>
              <option <?php if ($option_value['quantity'] <= 0) ; ?> value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                
                <?php if ( $this->customer->getCustomerGroupId()==15){?>
                <font color="red"> จำนวนคงเหลือ <?php echo $option_value['quantity']; ?></font>
                <?php } ?>
                
                <?php } ?>
                <?php if ($option_value['quantity'] <= 0) echo ' - สินค้าหมด'; ?>
              </option>
              <?php } ?>
            </select>
          </div>
          <br />
          <?php } ?>
          <ol class="payment-method">
          <?php if ($option['type'] == 'radio') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br />
            <?php foreach ($option['option_value'] as $option_value) { ?>
            <li>
            <input type="radio" <?php if ($option_value['quantity'] <= 0) ; ?> name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" <?php if ($nwa_enabled){ ?>
          <?php if ($option['type'] == 'select') { ?>
        name="option[<?php echo $option['product_option_id']; ?>]" onchange="$(this).nwaOption('<?php echo isset($option['option_value'][0]['option_id']) ? $option['option_value'][0]['option_id']: $option['option_id'];?>',$('option:selected', this).val(),'<?php echo $option['name']; ?>',$('option:selected', this).text());" 
          <?php }else{ ?>
        name="option[<?php echo $option['product_option_id']; ?>]" onchange="$(this).nwaOption('<?php echo isset($option_value['option_id']) ? $option_value['option_id']: $option['option_id'];?>',$(this).val(),'<?php echo $option['name']; ?>','<?php echo $option_value['name']; ?>');" 
          <?php } ?>
      <?php }else{ ?>
        name="option[<?php echo $option['product_option_id']; ?>]"
      <?php } ?> />
            <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
              <?php if ($option_value['price']) { ?>
              (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
              
              <?php if ( $this->customer->getCustomerGroupId()==15){?>
              <font color="red"> จำนวนคงเหลือ <?php echo $option_value['quantity']; ?></font>
              <?php } ?>
              
              <?php } ?>
              <?php if ($option_value['quantity'] <= 0) echo ' - สินค้าหมด'; ?>
            </label>
            <br />
            </li>
            <?php } ?>
          </div>
          <br />
          <?php } ?>
          </ol>
          <?php if ($option['type'] == 'checkbox') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br />
            <?php foreach ($option['option_value'] as $option_value) { ?>
            <input type="checkbox" <?php if ($option_value['quantity'] <= 0) echo 'disabled'; ?> name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" <?php if ($nwa_enabled){ ?>
          name="option[<?php echo $option['product_option_id']; ?>][]" onchange="$(this).nwaOption('<?php echo isset($option_value['option_id']) ? $option_value['option_id']: $option['option_id'];?>',$(this).val(),'<?php echo $option['name']; ?>','<?php echo $option_value['name']; ?>');" 
      <?php }else{ ?>
          name="option[<?php echo $option['product_option_id']; ?>
      <?php } ?> />
            <label for="option-<?php echo $option_value['product_option_value_id']; ?>"> <?php echo $option_value['name']; ?>
              <?php if ($option_value['price']) { ?>
              (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
              
              <?php if ( $this->customer->getCustomerGroupId()==15){?>
              <font color="red"> จำนวนคงเหลือ <?php echo $option_value['quantity']; ?></font>
              <?php } ?>
              
              <?php } ?>
              <?php if ($option_value['quantity'] <= 0) echo ' - สินค้าหมด'; ?>
            </label>
            <br />
            <?php } ?>
          </div>
          <br />
          <?php } ?>
          <?php if ($option['type'] == 'text') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br />
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
          </div>
          <br />
          <?php } ?>
          <?php if ($option['type'] == 'textarea') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br />
            <textarea type="text" name="option[<?php echo $option['product_option_id']; ?>]" cols="40" rows="5"><?php echo $option['option_value']; ?></textarea>
          </div>
          <br />
          <?php } ?>
          <?php if ($option['type'] == 'file') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br />
            <a id="option-<?php echo $option['product_option_id']; ?>" class="button"><span><?php echo $button_upload; ?></span></a>
            <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="option-<?php echo $option['product_option_id']; ?>" />
          </div>
          <br />
          <?php } ?>
          <?php if ($option['type'] == 'date') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br />
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="date" />
          </div>
          <br />
          <?php } ?>
          <?php if ($option['type'] == 'datetime') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br />
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="datetime" />
          </div>
          <br />
          <?php } ?>
          <?php if ($option['type'] == 'time') { ?>
          <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br />
            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="time" />
          </div>
          <br />
          <?php } ?>
          <?php } ?>
        </div>
        
      </div>
    </li>
  </ul>
</div>
<?php } ?>
          <?php if ($options) { ?>
          <?php foreach ($options as $option) { ?>
                    <span id="error-option-<?php echo $option['product_option_id']; ?>" ></span>
          <?php } } ?>
              
          <!-- End Product meta -->
      <!-- End Product Header -->
      <!-- Product navigation -->
      <div class="product-action margin-bottom">
          
          <div class="qty">
          <span><?php echo $text_qty; ?></span> <input type="number" value="<?php echo $minimum; ?>" name="quantity">
          </div>
          <input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
          <?php if (($product_info['quantity'] <= 0)) { ?>
                <a class="btn grey btn-block" id="button-cart"><?php echo $stock; ?></a>
          <?php } else { ?>
                <a class="btn green btn-block margin-bottom_low" id="button-cart" >หยิบใส่ตระกร้า</a>
          <?php } ?>
          
          <a onclick="addToWishList('<?php echo $product_id; ?>');" class="btn grey btn-block" ><?php echo $button_wishlist; ?></a>
          <?php if ($minimum > 1) { ?>
          <div class="minimum"><?php echo $text_minimum; ?></div>
          <?php } ?>
      </div>
      <!-- End Product navigation -->
      <!-- Product share -->
      <div class="product-share">
          <a href="#" class="fb"><i class="fa fa-facebook"></i></a>
          <a href="#" class="tw"><i class="fa fa-twitter"></i></a>
          <a href="#" class="gplus"><i class="fa fa-google-plus"></i></a>
          <a href="#" class="pint"><i class="fa fa-pinterest"></i></a>
      </div>
      <!-- End Product share -->
      <!-- Product tabs -->
      <div class="product-tabs">
          <ul class="tabs">
              <li class="tab"><a class="active" href="#detail"><?php echo $tab_attribute; ?>  </a></li>
              <li class="tab"><a href="#related"><?php echo $tab_related; ?> (<?php echo count($products); ?>)</a></li>
          </ul>
      </div>
      <!-- End Product tabs -->
      <!-- Product content -->
      <div class="product-content">
          <!-- Product detail tabs -->
            <div class="tab-content" id="detail">
                <?php echo $description; ?>
            </div>
            <div class="tab-content" id="related">
            <ol class="product-small-list animated fadeInLeft">
                <?php foreach ($products as $product) { ?>
                <li><!-- Item #1 -->
                    <?php if ($product['thumb']) { ?>
                    <div class="thumb">
                        <?php if ($product['quantity'] <= 0) { ?>
    <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['soldout']; ?>" alt="<?php echo $product['name']; ?>" class="soldout" /></a>
    <?php } ?>
    <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
                    </div>
                    <?php } ?>
                    <div class="product-ctn">
                        <div class="product-name">
                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                        </div>
                        <?php if ($product['price']) { ?>
                        <div class="price">
                          <?php if (!$product['special']) { ?>
                          <span class="price-current"><?php echo $product['price']; ?></span>
                          <?php } else { ?>
                            <span class="price-before"><?php echo $product['price']; ?></span>
                            <span class="price-current"><?php echo $product['special']; ?></span>
                          <?php } ?>
                        </div>
                        <?php } ?>
                    </div>
                </li><!-- End Item #1 -->
                <?php } ?>
            </ol>
        </div>
      </div>
      <!-- End Product detail tabs -->
      <!-- Product review list tabs -->
</div>

  <script type="text/javascript"><!--
  $('#button-cart').bind('click', function() {
    $.ajax({
      url: 'index.php?route=checkout/cart/update',
      type: 'post',
      data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea, .product-info input[type=\'number\']'),
      //data: { quantity: 1, combo_id: 0, product_id: "<?php echo $product_id; ?>" },
      dataType: 'json',
      success: function(json) {
        $('.success, .warning, .attention, information, .error').remove();
        
        if (json['error']) {
          if (json['error']['warning']) {
           // $('#menu').after('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
              
            // $('.warning').fadeIn('slow');
              // create theotification
                        var notification = new NotificationFx({
                            message : '<p>' + json['error']['warning'] + '</p>',
                            layout : 'growl',
                            effect : 'slide',
                            type : 'error'
                        });

                        notification.show();
          }
          
          for (i in json['error']) {
            $('#error-option-' + i).after('<span class="error">' + json['error'][i] + '</span>');

          }
        }  
              
        if (json['success']) {
          /*
          $('#menu').after('<div class="attention" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
            
          $('.attention').fadeIn('slow');
            
          $('#cart_total').html(json['total']);
                  
          var image = $('#image').offset();
          var cart  = $('#cart').offset();
    
          $('#image').before('<img src="' + $('#image').attr('src') + '" id="temp" style="position: absolute; top: ' + image.top + 'px; left: ' + image.left + 'px;" />');
    
          params = {
            top : cart.top + 'px',
            left : cart.left + 'px',
            opacity : 0.0,
            width : $('#cart').width(),   
            height : $('#cart').height()
          };    
    
          $('#temp').animate(params, 'slow', false, function () {
            $('#temp').remove();
          });   
*/
            
           
        
                        // create the notification
                        var notification = new NotificationFx({
                            message : '<p>' + json['success'] + '</p>',
                            layout : 'growl',
                            effect : 'slide',
                            type : 'notice'
                        });

                        notification.show();

                        $('.cart-badge').html(json['total']);
                        $('#yourcart').html(json['output']);
      
         
        } 
      }
    });
  });
  //--></script>
  <?php if ($options) { ?>
  <script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
  <?php foreach ($options as $option) { ?>
      <?php if ($option['type'] == 'file') { ?>
      <script type="text/javascript"><!--
      new AjaxUpload('#option-<?php echo $option['product_option_id']; ?>', {
        action: 'index.php?route=product/product/upload',
        name: 'file',
        autoSubmit: true,
        responseType: 'json',
        onSubmit: function(file, extension) {
          $('#option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" id="loading" style="padding-left: 5px;" />');
        },
        onComplete: function(file, json) {
          //$('.error').remove();
          
          if (json.success) {
            var notification = new NotificationFx({
                            message : '<p>' + json.success + '</p>',
                            layout : 'growl',
                            effect : 'slide',
                            type : 'notice'
                        });

            notification.show();

            $('.cart-badge').html(json['total']);
            // $('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json.file);
          }
          
          if (json.error) {
           //$('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json.error + '</span>');
            var notification = new NotificationFx({
                            message : '<p>' + json.error + '</p>',
                            layout : 'growl',
                            effect : 'slide',
                            type : 'error'
                        });

            notification.show();
          }
          
          //$('#loading').remove(); 
        }
      });
      //--></script>
      <?php } ?>
    <?php } ?>
  <?php } ?>
  
  <script type="text/javascript">
<!--
  $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

  $('#button-review').bind('click', function() {
    $.ajax({
      type: 'POST',
      url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
      dataType: 'json',
      data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
      beforeSend: function() {
        $('.success, .warning').remove();
        $('#button-review').attr('disabled', true);
        $('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
      },
      complete: function() {
        $('#button-review').attr('disabled', false);
        $('.attention').remove();
      },
      success: function(data) {
        if (data.error) {
          $('#review-title').after('<div class="warning">' + data.error + '</div>');
        }
        
        if (data.success) {
          $('#review-title').after('<div class="success">' + data.success + '</div>');
                  
          $('input[name=\'name\']').val('');
          $('textarea[name=\'text\']').val('');
          $('input[name=\'rating\']:checked').attr('checked', '');
          $('input[name=\'captcha\']').val('');
        }
      }
    });
  });
  //-->
  </script> 
</div></div>
<?php echo $footer; ?>
