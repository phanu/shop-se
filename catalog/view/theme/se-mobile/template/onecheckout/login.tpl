				<div class="page-block margin-bottom">
                    <div class="input-field">
                        <input type="text" id="email" name="email">
                        <label for="email"><?php echo $entry_email; ?></label>
                    </div>
                    <div class="input-field">
                        <input type="password" id="passwd" name="password">
                        <label for="passwd"><?php echo $entry_password; ?></label>
                    </div>
                    <button class="btn orange margin-bottom_low" type="button" id="button-login"><?php echo $button_login; ?></button>
                    <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                    
                </div>
	
	<?php if (!$this->customer->isLogged()) { ?>
<a class="ocx-facebook-login-trigger ocx-stay-here ocx-fbl-button ocx-rounded ocx-icon" href="javascript:void(0);"><?php echo $this->config->get('facebook_login_button_name_' . $this->config->get('config_language_id')); ?></a>
<?php } ?>

<script type="text/javascript"><!--
$('#login input').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#button-login').click();
	}
});
//--></script>   