<div id="reg-cpanle" class="divclear">
  <div class="left">
  	<span class="required">*</span> <?php echo $entry_password; ?><br />
  	<input type="password" name="password" value="" class="small-field" /><br />
  </div>
  <div class="right">
  	<span class="required">*</span> <?php echo $entry_confirm; ?> <br />
  	<input type="password" name="confirm" value="" class="small-field" /><br />
  </div>
  <div style="clear: both; padding-top: 15px; border-top: 1px solid #EEEEEE;">
  <input type="checkbox" name="newsletter" value="1" id="newsletter"<?php if ($this->config->get('onecheckout_check_newsletter')) { ?> checked="checked"<?php } ?> />
  <label for="newsletter"><?php echo $entry_newsletter; ?></label>
  <br />
  <?php if ($shipping_required) { ?>
  <input type="checkbox" name="shipping_address" value="1" id="shipping"<?php if ($this->config->get('onecheckout_check_deliveryaddress')) { ?> checked="checked"<?php } ?> />
  <label for="shipping"><?php echo $entry_shipping; ?></label>
  <br />
  <?php } else { ?>
  <input type="checkbox" name="shipping_address" value="1" id="shipping" checked="checked" style="display:none;" />
  <?php } ?>
  <br />
  </div>
<?php if ($text_agree) { ?>
<div class="buttons">  
    <input type="checkbox" name="agree" value="1" /><?php echo $text_agree; ?>
</div>
<?php } ?>
<script type="text/javascript"><!--


$(document).on('blur', '#payment-address input[name=\'password\']', function(){
	valiform("payment","password","");
});

$(document).on('focus', '#payment-address input[name=\'password\']', function(){
	errorremove("payment","password");
});

$(document).on('blur', '#payment-address input[name=\'confirm\']', function(){
	valiform("payment","confirm",", #payment-address input[name=\'password\']");
});

$(document).on('focus', '#payment-address input[name=\'confirm\']', function(){
	errorremove("payment","confirm");
});
//--></script> 
</div>