<?php if (isset($_SERVER['HTTP_USER_AGENT']) && !strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6')) echo '<?xml version="1.0" encoding="UTF-8"?>'. "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" xml:lang="<?php echo $lang; ?>">
<head>
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>"/>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="x-ua-compatible">
    <meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="yes" name="apple-touch-fullscreen">
    <meta name="author" content="">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="apple-touch-fullscreen" content="YES">
    <meta http-equiv="cleartype" content="on">
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>" />
    <?php } ?>
    <?php if ($icon) { ?>
    <link href="<?php echo $icon; ?>" rel="icon" />
    <?php } ?>
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/se-mobile/css/ns-default.css"/>
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/se-mobile/css/ns-style-growl.css"/>
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/se-mobile/css/style.css">
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/se-mobile/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/se-mobile/stylesheet/facebook_login.css">
    
    <script type="text/javascript"  src="catalog/view/theme/se-mobile/js/jquery-3.1.1.js"></script>
    <script type="text/javascript"  src="catalog/view/theme/se-mobile/js/jquery.jcarousel.min.js"></script>
    <script type="text/javascript"  src="catalog/view/theme/se-mobile/js/nwa.js"></script>
    <script type="text/javascript"  src="catalog/view/theme/se-mobile/js/sliding-cart/common.js"></script>  
    <script type="text/javascript"  src="catalog/view/theme/se-mobile/js/jquery.facebooklogin.js"></script>  
    <script type="text/javascript" src="catalog/view/javascript/jquery/ui/external/jquery.cookie.js"></script>

    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-22788676-3', 'se-update.com');
  ga('send', 'pageview');
</script>
</head>
<body>
    <script src="catalog/view/theme/se-mobile/js/modernizr.custom.js"></script>
    <script src="catalog/view/theme/se-mobile/js/classie.js"></script>
    <script src="catalog/view/theme/se-mobile/js/notificationFx.js"></script>
    <script type="text/javascript"  src="catalog/view/theme/se-mobile/js/isearch.js"></script>
<?php echo $isearch;  ?>
    <div id="main">
        <header>
            <div class="top-navbar">
                <div class="top-navbar-left">
                    <a href="#" id="menu-left" data-activates="slide-out-left">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <div class="top-navbar-right">
                    <a class="dropdown-button" href="tel:02-026-3135">
                        <i class="fa fa-phone-square"></i>
                    </a>
                    <?php if (!$logged) { ?>
                    <a class="dropdown-button" data-activates="dropdown1">
                        <i class="fa fa-user-circle-o"></i>
                    </a>
                    <ul id="dropdown1" class="dropdown-content">
                        <li><a href="<?php echo $account; ?>"><i class="fa fa-user"></i> เข้าสู่ระบบ</a></li>
                        <li><a href="/index.php?route=account/register"><i class="fa fa-history"></i> สมัครสมาชิก</a></li>
                    </ul>
                    <?php }else{ ?>
                    <a class="dropdown-button" data-activates="dropdown1" >
                        <i class="fa fa-user-circle-o"></i>
                    </a>
                    <ul id="dropdown1" class="dropdown-content">
                        <li><a href="/index.php?route=account/account"><i class="fa fa-user"></i> บัญชี</a></li>
                        <li><a href="/index.php?route=account/order"><i class="fa fa-history"></i> ประวัติสั่งซื้อ</a></li>
                        <li><a href="/index.php?route=account/wishlist"><i class="fa fa-star-o"></i> รายการโปรด</a></li>
                        <!--<li><a href="panel-account.php"><i class="fa fa-star-o"></i> รายการโปรด</a></li>-->
                        <li class="divider"></li>
                        <li><a href="/index.php?route=account/logout"><i class="fa fa-sign-out"></i> ออกจากระบบ</a></li>
                    </ul>
                    <?php } ?>
                    <a href="#" id="menu-right" data-activates="slide-out-right">
                        <span class="cart-badge"><?php echo $text_items; ?></span>
                        <i class="fa fa-shopping-basket"></i>
                    </a>
                </div>
                <div class="site-title">
                    <?php if ($logo) { ?>
                    <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
                    <?php }else{ ?>
                    <h1>se-update.com</h1>
                    <?php } ?>
                </div>
            </div>
            <!-- LEFT SIDEBAR -->
            <div id="slide-out-left" class="side-nav">     <!-- Form Search -->
                <div class="top-left-nav">
                    <div class="searchbar">
                        <i class="fa fa-search"></i>
                        <?php if ($filter_name) { ?>
                        <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" />
                        <?php } else { ?>
                        <input type="text" name="filter_name" value="<?php echo $text_search; ?>" onclick="this.value = '';" onkeydown="this.style.color = '#ffffff';" />
                        <?php } ?>
                    </div>
                </div>
                <!-- End Form Search -->
                <!-- App/Site Menu -->

                <?php if ($categories) { ?>
                <div id="main-menu">
                    <ul>
                    <?php foreach ($categories as $category) { ?>
                        <li <?php if ($category['children']) echo 'class="has-sub"'; ?>>
                        <a href="<?php echo $category['href']; ?>">
                        <i class="fa fa-mobile"></i><?php echo $category['name']; ?></a>
                            <?php if ($category['children']) { ?>
                            <?php for ($i = 0; $i < count($category['children']);) { ?>
                                    <ul>
                                    <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
                                    <?php for (; $i < $j; $i++) { ?>
                                    <?php if (isset($category['children'][$i])) { ?>
                                        <li><a href="<?php echo $category['children'][$i]['href']; ?>">
                                        <?php echo $category['children'][$i]['name']; ?></a></li>
                                    <?php } ?>
                                    <?php } ?>
                                    </ul>
                            <?php } ?>
                        <?php } ?>
                        </li>
                    <?php } ?>
                      </ul>
                <?php } ?>
                <!-- End Site/App Menu -->
                </div>
            </div>
            <!-- END LEFT SIDEBAR -->
            <!-- RIGHT SIDEBAR -->
            <div id="slide-out-right" class="side-nav">
                <!-- TABS -->
                <div class="sidebar-tabs">
                    <!-- Tabs Menu -->
                    <ul class="tabs">
                        <li class="tab"><a class="active" href="#yourcart">ตระกร้าสินค้า</a></li>
                       <!-- <li class="tab"><a href="#favitem">รายการโปรด</a></li>-->
                    </ul>
                    <!-- End Tabs Menu -->
                </div>
                <!-- Right Sidebar Tabs Content -->
                <div class="sidebar-tabs_content">
                    <!-- Your Cart Tabs -->
                    <div id="yourcart">
                        <?php echo $cart_on_page; ?>
                    </div>
                    <!-- End Your Cart Tabs -->
                    <!-- Fav item
                    <div id="favitem">
                        <!-- Your Cart Tabs -->
                    <!--<div id="yourcart">
                        <?php //echo $wishlist_on_page; ?>
                        <ol class="cart-item">
                            <li>
                                <div class="thumb">
                                    <img src="images/80x80.png" alt="">
                                </div>
                                <div class="cart-delete">
                                    <a href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                                <div class="cart-detail">
                                    <h3 class="product-name"><a href="product.php">เคสหนัง Xperia XZ Nillkin QIN</a></h3>
                                    <div class="price">
                                        <span>ราคา</span> 390.-
                                    </div>
                                    <div class="qty">
                                        <span>จำนวน</span>
                                        <input type="number" value="1">
                                    </div>
                                </div>
                            </li>
                        </ol>
                        <div class="cart-action">
                            <a href="#" class="btn green btn-block">หยิบใส่ตระกร้า</a>
                        </div>
                    </div>-->
                    <!-- End Your Cart Tabs 
                    </div>
                </div>
                <!-- End Right Sidebar Tabs Content -->
            </div>
            <!-- END RIGHT SIDEBAR -->
            
        </header>


<?php echo $content_top_se_mobile; 

$homepage = "/";
$currentpage = $_SERVER['REQUEST_URI'];
if($homepage==$currentpage||'/index.php?route=common/home'==$currentpage) { 

echo $slideshow;

}


?>

<div id="page"> 

<!-- CONTENT CONTAINER -->
<div class="content-container animated fadeInUp">