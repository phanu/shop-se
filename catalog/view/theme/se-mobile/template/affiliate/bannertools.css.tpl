@import url(http://fonts.googleapis.com/css?family=Open+Sans:600,400);
<? require_once DIR_SYSTEM.'bannerassets/tipTip.css'; ?>

#Widget-<?=$bid?> {
	font-family: 'Open Sans', Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	border-width: 1px;
	border-style: solid;
	border-color: <?=$sc_border?>;
	width: <?=$width?>;
	height: <?=$height?>;
	background: <?=$sc_background?>;
	position: relative;
	font-weight: 100;
	line-height: 1.4;
	overflow: hidden;
}
#Widget-<?=$bid?>,
#Widget-<?=$bid?> a,
#Widget-<?=$bid?> a:link,
#Widget-<?=$bid?> a:active,
#Widget-<?=$bid?> a:visited,
#Widget-<?=$bid?> div,
#Widget-<?=$bid?> p {
    font-family: 'Open Sans', Verdana, Arial, Helvetica, sans-serif;
}
#Widget-<?=$bid?> a,
#Widget-<?=$bid?> a img {
    outline: none;
    border: none;
}
#Widget-<?=$bid?> > #WidgetTitle-<?=$bid?> {
	padding: 1px;	
	text-align: center;
	font-size: 12px;
	font-weight: bold;
	color: <?=$sc_title?>;
	background-color: <?=$sc_title_background?>;
    overflow: hidden;
}
#WidgetData-<?=$bid?> {	
	text-align: center;
    padding: 2px 0 0 0;
	color: <?=$text?>;
	overflow: hidden;
	z-index: 100;
    height: <?=$WidgetData_height?>;
}
#WidgetData-<?=$bid?> a:link,
#WidgetData-<?=$bid?> a:visited,
#WidgetData-<?=$bid?> a:active{
	color: <?=$sc_link?>;    
    text-decoration: underline;
}	
#WidgetData-<?=$bid?> a:hover{
    color: <?=$sc_link_hover?>;    
    text-decoration: none; 
}

#WidgetData-<?=$bid?> .pr_i_des_h a:link,
#WidgetData-<?=$bid?> .pr_i_des_h a:visited,
#WidgetData-<?=$bid?> .pr_i_des_h a:active{
    font-size: 11px!important;	
    line-height: 15.4px!important;
}	


#ControlPanel-<?=$bid?> {	
	color: #2A5375;
	float: right;
	display: block;
	padding: 0 8px 0 12px;
	z-index: 200;
}
#WidgetPages-<?=$bid?> {
	font-size: 10px;
	display: block;
	float: left;
	vertical-align: middle;
	line-height: 19px;
	color: #2A5375;
}
#Widget-<?=$bid?> .Active-<?=$bid?> {
	color: #2A5375;
	cursor: pointer;
}
#Widget-<?=$bid?> .Nactive-<?=$bid?> {
	color: #CCCCCC;
	cursor: default;
}

#Widget-<?=$bid?> .WidgetBottom {
	font-size: 9px;
	text-align: left;
	position: absolute;
	bottom: 0;
	left: 0;
	width: 100%;
}
#ListBack-<?=$bid?>, #ListForward-<?=$bid?>{
    line-height: 12px;
    padding: 0 3px;
	font-size: 15px;
	float: left;
	display: block;  
    cursor: pointer;  
    color: <?=$sc_link_bottom?>;
}
#WidgetBottomLink-<?=$bid?> {  
	margin: <?=$WBL_margin?>;
	line-height: 1.3;
}
#WidgetBottomLink-<?=$bid?> a,
#WidgetBottomLink-<?=$bid?> a:link,
#WidgetBottomLink-<?=$bid?> a:active,
#WidgetBottomLink-<?=$bid?> a:visited {
	color: <?=$sc_link_bottom?>;
	text-decoration: underline;
	font-weight: 100;
    font-size: 9px!important;
    line-height: 11.7px!important;    
}
#WidgetBottomLink-<?=$bid?> a:hover{
	text-decoration: none;
}
#WidgetFooter-<?=$bid?> {
	padding: 1px;
	text-align: center;
	background-color: <?=$sc_footer_background?>;
}
#WidgetFooter-<?=$bid?> a,
#WidgetFooter-<?=$bid?> a:link,
#WidgetFooter-<?=$bid?> a:active,
#WidgetFooter-<?=$bid?> a:visited {
	color: <?=$sc_link_footer?>;
	text-decoration: underline;
	font-weight: 100;
    font-size: 9px!important;
    line-height: 12.6px!important;
}
#WidgetFooter-<?=$bid?> a:hover {
	text-decoration: none;
}

#Widget-<?=$bid?> .Listings-<?=$bid?> {
	text-align: left;
	margin: 5px 12px 0 12px;
	float: left;
    width: 240px;
	height: auto;
	z-index: 300;
}

a:link.Link-<?=$bid?>,
a:visited.Link-<?=$bid?>,
a:active.Link-<?=$bid?>,
a:focus.Link-<?=$bid?> {
	color: #2A5375;
	text-decoration: none;
	text-decoration : underline;
}

a:hover.Link-<?=$bid?> {
	text-decoration : underline;
	color: #D41313;
}
#Widget-<?=$bid?> .p_i_w{
    float: left;
    width: <?=$item_width?>;
}
#Widget-<?=$bid?> .pr_i{
    padding: 0 4px;
    text-align: left;
    height: <?=$pr_i_height?>;
}

#Widget-<?=$bid?>  .pr_i_image{
    float: left;
    margin-right: 4px;
    border: 0px;
}
#Widget-<?=$bid?>  .pr_i_des_h{
    font-size: 10px;
    margin-bottom: 2px;
}
#Widget-<?=$bid?>  .pr_i_des{
    margin-left: <?=$pr_i_des_margin_left?>;
}
#Widget-<?=$bid?>  .pr_i_des_t{
    font-size: 10px;
    line-height: 11px;
}
#Widget-<?=$bid?> .clr{
    clear: both;
    height: 0px;
    height: 0%;
    font-size: 0px;
    font-size: 0%;
}
.tip_i_wrap{
    position: relative;
}
.tip_i_price{
    position: absolute;
    bottom: 3px;
    right: 0px;
    padding: 4px;
    background: #ff6600;
    color: #fff;
    font-weight: bold;
    font-size: 11px;
}
.tip_i_price span{
    padding-right: 5px;
    text-decoration: line-through;
    font-size: 9px;
}