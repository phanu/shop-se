<div id="statistics_wrap">
    <h2><?=$txt_commission_for?> <?=$ID?></h2>
    <table id="statistics">
    	<tr>            
    		<th class="left"><?=$txt_will_be_shown?></th>
            <td class="center"><?=$products?></td>
    	</tr>
    	<tr>            
    		<th class="left"><?=$txt_how_much_1?></th>
            <td class="center"><?=$money_1?></td>
    	</tr>        
    	<tr>            
    		<th class="left"><?=$txt_how_much_2?></th>
            <td class="center"><?=$money_2?></td>
    	</tr>
    	<tr>            
    		<th class="left"><?=$txt_how_much_3?></th>
            <td class="center"><?=$money_3?></td>
    	</tr>            
    	<tr>            
    		<th class="left"><?=$txt_how_much_5?></th>
            <td class="center"><?=$money_5?></td>
    	</tr>            
    	<tr>            
    		<th class="left"><?=$txt_how_much_10?></th>
            <td class="center"><?=$money_10?></td>
    	</tr>            
    </table>	  
</div>