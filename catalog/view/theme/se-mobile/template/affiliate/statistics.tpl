<div id="statistics_wrap">
    <h2><?=$txt_statistics_for?> <?=$ID?></h2>
    <table id="statistics">
    	<thead>
            <tr>
        		<th></th>
                <td><?=$txt_last_day?></td>
                <td><?=$txt_7_day?></td>
                <td><?=$txt_30_days?></td>
                <td><?=$txt_1_year?></td>
        	</tr>
        </thead>
    	<tr>            
    		<th class="left"><?=$txt_showcount?></th>
            <td class="center"><?=$showcount_1?></td>
            <td class="center"><?=$showcount_7?></td>
            <td class="center"><?=$showcount_30?></td>
            <td class="center"><?=$showcount_365?></td>
    	</tr>
    	<tr>        		
            <th class="left"><?=$txt_transfer?></th>
            <td class="center"><?=$transfer_1?></td>
            <td class="center"><?=$transfer_7?></td>
            <td class="center"><?=$transfer_30?></td>
            <td class="center"><?=$transfer_365?></td>
    	</tr>
    	<tr>        
            <th class="left"><?=$txt_productswasshow?></th>
            <td class="center"><?=$productswasshow_1?></td>
            <td class="center"><?=$productswasshow_7?></td>
            <td class="center"><?=$productswasshow_30?></td>
            <td class="center"><?=$productswasshow_365?></td>
    	</tr>
    	<tr>        
            <th class="left"><?=$txt_productspurchased?></th>
            <td class="center"><?=$productspurchased_1?></td>
            <td class="center"><?=$productspurchased_7?></td>
            <td class="center"><?=$productspurchased_30?></td>
            <td class="center"><?=$productspurchased_365?></td>
    	</tr>
    </table>	
</div>