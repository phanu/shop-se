<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a  href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <? if($banners_count>0){ ?>
  <table style="width: 100%;" class="bannerlist">
        <tr>
            <th class="center"><?=$num_txt?></th>
            <th class="center"><?=$id_txt?></th>
            <th class="center"><?=$banner_title?></th>
            <th><?=$txt_layout?></th>
            <th class="center"><?=$txt_how_many?></th>
            <th class="center"><?=$txt_statistics?></th>
            <th class="center"><?=$txt_calculator?></th>
            <th class="center"><?=$action_txt?></th>            
        </tr>
        <? $r=($page-1)*20+1; foreach($banners AS $banner){ ?> 
        <tr>
            <td class="center"><?=$r?></td>
            <td class="center"><?=$banner['ubanner_id']?></td>
            <td><?=$banner['banner_title']?></td>
            <td><?=$banner['layout_name']?></td>  
            <td class="center"><?=$banner['pr_count']?></td>
            <td class="center"><a class="statistics" href="<?=$banner['statistics']?>"><img title="Click for statistics" src="image/statistics2.png" /></a></td>
            <td class="center"><a class="calculator" href="<?=$banner['calculator']?>"><img title="Click for calculator" src="image/calculator.png" /></a></td>
            <td class="center"><a href="<?=$banner['href']?>"><?=$edit_txt?></a> / <a class="delete_banner" href="<?=$banner['href_delete']?>"><?=$delete_txt?></a> / <a class="a_preview_<?=$banner['layout']?>" href="<?=$banner['href_preview']?>"><?=$preview_txt?></a></td>
        </tr>
        <? $r++;} ?>
  </table>
  <script>
  $(".statistics").colorbox();
  $(".calculator").colorbox();
  $('.delete_banner').click(function(){
    if (confirm("<?=$delete_quest?>") ){
        return true;
    } else {
        return false;
    };
  });
  $(document).ready(function(){
    <? foreach($layouts AS $layout){ ?>
        $(".a_preview_<?=$layout['layouts_id']?>").colorbox({
            iframe : true, 
            width  : '<?=((substr($layout['width'],0,-2)+50).'px')?>',
            height : '<?=((substr($layout['height'],0,-2)+80).'px')?>'
        });    
    <? } ?>
  });
  </script>
  <div class="pagination">
    <?=$pagination?>
  </div>
  <? } else { ?>
  <div class="content">
    <?=$list_empty?>
  </div>
  <? } ?>
  <div class="buttons">
    <div class="left">
        <a href="<?=$create_new_link?>" class="button"><span><?=$create_new_text?></span></a>
    </div>
  </div>
<?php echo $footer; ?>