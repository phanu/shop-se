<?php
require_once("../../../../../../config.php");	
$img_name = $_GET['img'];
$token = $_GET['token'];
$img_dir = '';
if(isset($img_name) && $img_name != ''){
	if($token == 'u'){
		$img_dir = DIR_IMAGE . 'data/upload_image/';
	}
	else {
		$img_dir = DIR_IMAGE . 'data/upload_logo/';
	}
	$ext = explode('.',$img_name);
	if(strtolower($ext[1]) == 'jpg')
	{
		header("Content-type: image/jpeg");
		echo  file_get_contents($img_dir . $img_name);
	}
	else if(strtolower($ext[1]) == 'png'){
		header("Content-type: image/png");
		echo  file_get_contents($img_dir . $img_name);
	}
	else if(strtolower($ext[1]) == 'gif'){
		header("Content-type: image/gif");
		echo  file_get_contents($img_dir . $img_name);
	}
}


?>