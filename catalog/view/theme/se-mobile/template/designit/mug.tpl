<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Custom Mobile Case Design Tool </title>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css">
<script type='text/javascript' src="http://jqueryrotate.googlecode.com/files/jQueryRotate.2.1.js"></script>
<script type="text/javascript" src="<?php echo designdic; ?>javascript/jquery/tabs.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo designdic; ?>javascript/ajaxupload.3.5.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo designdic; ?>javascript/jscolor.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo designdic; ?>javascript/html2canvas.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo designdic; ?>javascript/tooltipster.css" />
<link rel="stylesheet" type="text/css" href="<?php echo designdic; ?>javascript/tooltipster-light.css" />

<style type="text/css">
body{
	background:<?php echo $page_bg_color; ?>;
}
#clist{
	margin:12px;
}
#show_text_panel,#show_image_panel,#show_custom_product_panel,#show_addart_panel,#show_name_number_panel{
	display:none;
}
#main_content {
	margin:auto;
}
.btnstyle {
	background: none repeat scroll 0 0 #FFFFFF;
	border: 1px solid #FFFFFF;
	border-radius: 8px 8px 8px 8px;
	color: red;
	display: block;
	font-size: 12px;
	font-weight: bold;
	width: 100px;
	text-align:center;
	text-decoration:none;
}
.btnstyle:hover {
	background: none repeat scroll 0 0 #CCCCCC;
	border: 1px solid #FFFFFF;
	border-radius: 8px 8px 8px 8px;
	color: red;
	display: block;
	font-size: 12px;
	font-weight: bold;
	width: 100px;
}
#left_placeholder {
	width:33.4%;
	height:100%;
	background:url('catalog/view/t_shirt_theme/<?php echo $temp; ?>/background.png');
	float:left;
}
#right_placeholder {
	width:66.4%;
	height:100%;
	background:url('catalog/view/t_shirt_theme/<?php echo $temp; ?>/background.png');
	float:right;
}
#design_div {
	clear:both;
	width:100%;
	height:100%;
	/*box-shadow:0px 0px 10px #000000;*/
}
.header_text {
	color:<?php echo $back_color; ?>;
	margin-left:8px;
	margin-bottom:8px;
	/*font-style:<?php echo $cust_price_text_bold; ?>;*/
	font-size:18px;
	font-family:Verdana,Arial,sans-serif !important;
	font-weight:bold;
	text-align:left;
}
.spacer {
	margin-bottom:15px;
}
.tab_inner_box {
	margin-left:8px;
}
/*Tab*/
	.htabs {
	/*padding: 0px 0px 0px 10px;*/
	padding: 0px 0px 0px 0px;
	height: 30px;
	line-height: 16px;
	/*border-bottom: 1px solid #DDDDDD;*/
	margin-bottom: 15px;
}
.htabs a {
	border-top: 1px solid #DDDDDD;
	border-left: 1px solid #DDDDDD;
	border-right: 1px solid #DDDDDD;
	background: #FFFFFF url('<?php echo designdic; ?>image/tab.png') repeat-x;
	padding: 7px 15px 6px 15px;
	float: left;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: bold;
	text-align: center;
	text-decoration: none;
	color: #fff;
	margin-right: 2px;
	display: none;
	border-radius:8px;
}
.htabs a.selected {
	padding-bottom: 6px;
	background: #ccc;
	-moz-border-radius: 8px;
    -webkit-border-radius: 8px;
    -khtml-border-radius: 8px;
    border-radius: 8px;;
}
#upload {
	/*background:#F1E3C5;
	border-radius:7px;
	text-align:center;
	display:inline-block;*/
	margin-top:15px;
	margin-left:45px;/*box-shadow:0 3px 0 -1px #F5EBD6 inset, 0 -2px 0 0 #E2C587 inset*/
}
#tab-clip {
	clear:both;
}
.carts {
	float:left;
	margin:5px;
}
#tab-clip img {
	cursor:pointer;
}

#design_area {
	width:65%;
	height:57%;
	margin:0 auto;
	float:left;
	margin-top:3%;
}
#thumb {
	margin-top:30px;
	clear:both;
	padding-left:38.5%;
	font-size:13px;
}
#thumb div {
	float:left;
	margin:7px;
	cursor:pointer;
}
#thumb img {
	width:34.5px;
	height:44.87px;
	outline:none !important;
}
/**/
	.drag {
	width: 100px;
	height: 100px;
	border: 1px solid black;
	cursor: pointer;
	border-radius: 10px;
	text-algin: center;
	background-color: lightpink;
}
.resize {
	position:absolute;/*border-color:black;
    border-style:solid;
    border-width:1px;*/
}
.ui-resizable-se {
	width: 7px;
	height: 7px;
	background-color: #D0F2F0;
	border-color:#45C1B1;
	border-style:solid;
	border-width:1px;
	bottom:0px;
	right:0px;
}
.ui-resizable-sw {
	width: 7px;
	height: 7px;
	background-color: #D0F2F0;
	border-color:#45C1B1;
	border-style:solid;
	border-width:1px;
	bottom:0px;
	left:0px;
}
.ui-resizable-nw {
	width: 7px;
	height: 7px;
	background-color: #D0F2F0;
	border-color:#45C1B1;
	border-style:solid;
	border-width:1px;
	top:0px;
	left:0px;
}
.ui-resizable-ne {
	width: 12px;
	height: 12px;
	cursor:pointer;
	/*background-color: #D0F2F0;
	 border-color:#45C1B1;
	 border-style:solid;
	 border-width:1px;*/
	 background:url(delete.png) !important;
	top:0px;
	right:0px;
}
#front {
	width:500px;
	height:500px;
	margin:auto;
}

#image_placeholder_front {
	width:500px;
	height:500px;
	background-position:center;
}

#image_placeholder_back_tra{
	background:url('<?php echo $mug_front_image_big; ?>') no-repeat;
	width:500px;
	height:500px;
	margin:auto;
	position:relative;
	pointer-events: none;
	overflow:hidden;
}
.tab_inner_box span {
	color:<?php echo $block_color; ?>;
	font-weight:bold;
	font-size:12px;
}
select {
	font-size:12px;
}
input[type='text'] {
	font-size:11px;
}
#top_bar {
	background:url('<?php echo designdic; ?>image/tab.png') repeat;
	height:30px;
	width:100%;
	clear:both;
}
#top_bar div {
	float:left;
	padding-left:10px;
}
#bottom_panel {
	clear:both;
	width:100%;
	padding-top:15px;
	float:right;
}
#bottom_panel b {
	color:<?php echo $block_color; ?>;
}
#imageHolder {
	display:none;
}
.error {
	color:red;
	font-weight:bold;
}
#addprocess {
	display:none;
}
input[type="text"], input.text, textarea {
	background: none repeat scroll 0 0 #FFFFFF;
	border: 1px solid #CED5D9;
	border-radius: 3px 3px 3px 3px;
	box-shadow: 0 1px 2px rgba(0, 0, 0, 0.15) inset;
	color: #7A8F99;
	font: 12px sans-serif;
	padding: 6px;
}
input[type="text"]:focus, input.text:focus, textarea:focus {
	border-color: #49C1FD;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset, 0 0 3px #3DC0FF;
	outline: medium none;
}
select {
	background: none repeat scroll 0 0 #FFFFFF;
	border: 1px solid #CED5D9;
	border-radius: 3px 3px 3px 3px;
	box-shadow: 0 1px 2px rgba(0, 0, 0, 0.15) inset;
	color: #7A8F99;
	font: 12px sans-serif;
	padding: 3px;
}
select:focus {
	border-color: #49C1FD;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset, 0 0 3px #3DC0FF;
	outline: medium none;
}

textarea{resize: none;}

/*Add to cart button themming*/
	
.theme_a {
	background: -moz-linear-gradient(center top, #EDEDED 5%, #DFDFDF 100%) repeat scroll 0 0 #EDEDED;
	border: 1px solid #DCDCDC;
	border-radius: 6px 6px 6px 6px;
	box-shadow: 0 1px 0 0 #FFFFFF inset;
	color: #777777;
	display: inline-block;
	font-family: Times New Roman;
	font-size: 15px;
	font-style: normal;
	font-weight: bold;
	height: 31px;
	line-height: 31px;
	text-align: center;
	text-decoration: none;
	text-indent: 0;
	text-shadow: 1px 1px 0 #FFFFFF;
	width: 100px;
	cursor:pointer;
	-moz-border-radius: 8px;
    -webkit-border-radius: 8px;
    -khtml-border-radius: 8px;
    border-radius: 8px;;
}
.theme_a:hover {
	background: -moz-linear-gradient(center top, #DFDFDF 5%, #EDEDED 100%) repeat scroll 0 0 #DFDFDF;
}
.theme_a:active {
	position: relative;
	top: 1px;
}

.theme_b {
    background: linear-gradient(to bottom, #2DABF9 5%, #0688FA 100%) repeat scroll 0 0 #2DABF9;
    border: 1px solid #0B0E07;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 0 -3px 7px 0 #29BBFF inset;
    color: #FFFFFF;
    display: inline-block;
    font-size: 15px;
    font-weight: normal;
    padding: 6px 23px;
    text-decoration: none;
    text-shadow: 0 1px 0 #263666;
	-moz-border-radius: 8px;
    -webkit-border-radius: 8px;
    -khtml-border-radius: 8px;
    border-radius: 8px;;
}
.theme_b:hover {
    background: linear-gradient(to bottom, #0688FA 5%, #2DABF9 100%) repeat scroll 0 0 #0688FA;
}
.theme_b:active {
    position: relative;
    top: 1px;
}

.theme_c {
    background: linear-gradient(to bottom, #3D94F6 5%, #1E62D0 100%) repeat scroll 0 0 #3D94F6;
    border: 1px solid #0B0E07;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 0 -3px 7px 0 #29BBFF inset;
    color: #FFFFFF;
    display: inline-block;
    font-size: 15px;
    font-weight: normal;
    padding: 6px 23px;
    text-decoration: none;
    text-shadow: 0 1px 0 #263666;
	cursor:pointer;
	-moz-border-radius: 8px;
    -webkit-border-radius: 8px;
    -khtml-border-radius: 8px;
    border-radius: 8px;;
}
.theme_c:hover {
    background: linear-gradient(to bottom, #0688FA 5%, #2DABF9 100%) repeat scroll 0 0 #1E62D0;
}
.theme_c:active {
    position: relative;
    top: 1px;
}

.theme_c {
    background: linear-gradient(to bottom, #3D94F6 5%, #1E62D0 100%) repeat scroll 0 0 #3D94F6;
    border: 1px solid #0B0E07;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 0 -3px 7px 0 #29BBFF inset;
    color: #FFFFFF;
    display: inline-block;
    font-size: 15px;
    font-weight: normal;
    padding: 6px 23px;
    text-decoration: none;
    text-shadow: 0 1px 0 #263666;
	cursor:pointer;
	-moz-border-radius: 8px;
    -webkit-border-radius: 8px;
    -khtml-border-radius: 8px;
    border-radius: 8px;;
}
.theme_c:hover {
    background: linear-gradient(to bottom, #0688FA 5%, #2DABF9 100%) repeat scroll 0 0 #1E62D0;
}
.theme_c:active {
    position: relative;
    top: 1px;
}

.theme_d {
    background: linear-gradient(to bottom, #F24537 5%, #C62D1F 100%) repeat scroll 0 0 #F24537;
    border: 1px solid #0B0E07;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 0 1px 0 0 #F5978E inset;
    color: #FFFFFF;
    display: inline-block;
    font-size: 15px;
    font-weight: normal;
    padding: 6px 23px;
    text-decoration: none;
    text-shadow: 1px 1px 0 #810E05;
	cursor:pointer;
	-moz-border-radius: 8px;
    -webkit-border-radius: 8px;
    -khtml-border-radius: 8px;
    border-radius: 8px;;
}
.theme_d:hover {
    background: linear-gradient(to bottom , #C62D1F 5%, #F24537 100%) repeat scroll 0 0 #C62D1F;
}
.theme_d:active {
    position: relative;
    top: 1px;
}

.theme_e {
    background: linear-gradient(to bottom, #9DCE2C 5%, #8CB82B 100%) repeat scroll 0 0 #9DCE2C;
    border: 1px solid #0B0E07;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 0 1px 0 0 #C1ED9C inset;
    color: #FFFFFF;
    display: inline-block;
    font-size: 15px;
    font-weight: normal;
    padding: 6px 23px;
    text-decoration: none;
    text-shadow: 1px 1px 0 #689324;
	cursor:pointer;
	-moz-border-radius: 8px;
    -webkit-border-radius: 8px;
    -khtml-border-radius: 8px;
    border-radius: 8px;;
}
.theme_e:hover {
    background: linear-gradient(to bottom, #8CB82B 5%, #9DCE2C 100%) repeat scroll 0 0 #8CB82B;
}
.theme_e:active {
    position: relative;
    top: 1px;
}

.theme_f {
    background:  linear-gradient(to bottom, #F6B33D 5%, #D29105 100%) repeat scroll 0 0 #F6B33D;
    border: 1px solid #0B0E07;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 0 1px 0 0 #FED897 inset;
    color: #FFFFFF;
    display: inline-block;
    font-size: 15px;
    font-weight: normal;
    padding: 6px 23px;
    text-decoration: none;
    text-shadow: 1px 1px 0 #CD8A15;
	cursor:pointer;
	-moz-border-radius: 8px;
    -webkit-border-radius: 8px;
    -khtml-border-radius: 8px;
    border-radius: 8px;;
}
.theme_f:hover {
    background: linear-gradient(to bottom , #D29105 5%, #F6B33D 100%) repeat scroll 0 0 #D29105
}
.theme_f:active {
    position: relative;
    top: 1px;
}

.theme_g {
    background: linear-gradient(to bottom , #C579FF 5%, #A341EE 100%) repeat scroll 0 0 #C579FF;
    border: 1px solid #0B0E07;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 0 1px 0 0 #E6CAFC inset;
    color: #FFFFFF;
    display: inline-block;
    font-size: 15px;
    font-weight: normal;
    padding: 6px 23px;
    text-decoration: none;
    text-shadow: 1px 1px 0 #8628CE;
	cursor:pointer;
	-moz-border-radius: 8px;
    -webkit-border-radius: 8px;
    -khtml-border-radius: 8px;
    border-radius: 8px;;
}
.theme_g:hover {
    background: linear-gradient(to bottom , #A341EE 5%, #C579FF 100%) repeat scroll 0 0 #A341EE;
}
.theme_g:active {
    position: relative;
    top: 1px;
}

.theme_h {
    background: linear-gradient(to bottom , #F9F9F9 5%, #E9E9E9 100%) repeat scroll 0 0 #F9F9F9;
    border: 1px solid #DCDCDC;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 0 1px 0 0 #FFFFFF inset;
    color: #666666;
    display: inline-block;
    font-size: 15px;
    font-weight: normal;
    padding: 6px 23px;
    text-decoration: none;
    text-shadow: 1px 1px 0 #FFFFFF;
	cursor:pointer;
	-moz-border-radius: 8px;
    -webkit-border-radius: 8px;
    -khtml-border-radius: 8px;
    border-radius: 8px;;
}
.theme_h:hover {
    background: linear-gradient(to bottom , #E9E9E9 5%, #F9F9F9 100%) repeat scroll 0 0 #E9E9E9;
}
.theme_h:active {
    position: relative;
    top: 1px;
}

.theme_i {
    background: linear-gradient(to bottom , #3D94F6 5%, #1E62D0 100%) repeat scroll 0 0 #3D94F6;
    border: 1px solid #337FED;
    border-radius: 3px 3px 3px 3px;
    box-shadow: 0 1px 0 0 #97C4FE inset;
    color: #fff;
    display: inline-block;
    font-size: 15px;
    font-weight: normal;
    padding: 6px 23px;
    text-decoration: none;
    text-shadow: 1px 1px 0 #1570CD;
	cursor:pointer;
	-moz-border-radius: 8px;
    -webkit-border-radius: 8px;
    -khtml-border-radius: 8px;
    border-radius: 8px;;
}
.theme_i:hover {
    background: linear-gradient(to bottom , #1E62D0 5%, #3D94F6 100%) repeat scroll 0 0 #1E62D0;
}
.theme_i:active {
    position: relative;
    top: 1px;
}

#top_menu{
	width:228px;
	height:55px;
	margin-left:38%;
	background:<?php echo $menu_back_color; ?>;
	border-radius:6px;
	clear:both;
	color:<?php echo $menu_text_color; ?>;
	text-transform:uppercase;
}
.div_child{
	font-size:13px;
}
#top_menu .div_child{
	height:100%;
	width:75px;
	border-right:solid 1px #fff;
	float:left;
	cursor:pointer;
}
#top_menu .div_child:hover{
	background:<?php echo $menu_hover_bg_color; ?>;
}
#top_menu_a{
	border-radius:6px 0px 0px 6px;
	background:url('<?php echo designdic; ?>icon/help.png') no-repeat;
}
#top_menu_a:hover,#top_menu_b:hover,#top_menu_c:hover,#top_menu_d:hover{
	color:<?php echo $menu_text_hover_color; ?>;
}

#top_menu_d{
	border-radius:0px 6px 6px 0px;
}
#left_menu{
	width:185px;
	height:255px;
	background:<?php echo $menu_back_color; ?>;
	border-radius:6px;
	margin-top:100px;
	float:left;
	margin-left:10px;
	color:<?php echo $menu_text_color; ?>;
	text-transform:uppercase;
}
#left_menu div{
	width:100%;
	height:50px;
	border-bottom:1px solid #fff;
	cursor:pointer;
	font-size:13px;
}
#left_menu div:hover{
	background:<?php echo $menu_hover_bg_color; ?>;;
	color:<?php echo $menu_text_hover_color; ?>;
}
#left_menu_a{
	border-radius:6px 6px 0px 0px;
}

#left_menu_d{
	border-radius:0px 0px 6px 6px;
}
#left_menu table{
	margin-left:10px;
	padding-top:10px;
}
table td{
	border:none;
}
img{
	border:0;
}

#right_panel{
	width:230px;
	height:auto;
	background:<?php echo $right_panel_bg; ?>;
	border-radius:8px;
	float:right;
	margin-right:20px;
	margin-top:100px;
	border:solid 1px #ccc;
	padding-bottom:15px;
}
hr { display: block; height: 1px;
    border: 0; border-top: 1px solid #ccc;
    margin: 1em 0; padding: 0; 
}
#add_to_cart{
	display:block;width:200px;
	height:38px;background:<?php echo $add_cart_bg_color; ?>;
	cursor:pointer;color:#<?php echo $add_cart_text_color; ?>;
	border-radius:6px;font-weight:bold;
	font-size:20px;text-align:center;padding-top:10px;
	color:<?php echo $add_cart_text_color; ?>;
}

.str_bg{
	float:left;
	border:solid 1px #ccc;
	border-radius:6px;
	padding:2px;
}

.tooltipster-content{
	/*border-radius: 1px;
	box-shadow: 0px 0px 0px 8px rgba(0,0,0,0.5);*/
	border: solid 5px #666;
}
.delete_icon{
		background:url('<?php echo designdic; ?>/icons/delete_new.png');
		width:11px;
		height:14px;
		display:none;
		background-repeat:no-repeat;
		z-index:10000;
		position:absolute;
		cursor:pointer;
	}
</style>
</head>
<body>
<div id="main_content">
  <div id="design_div">
  	<div style='position:absolute;left:0;top:0;margin:25px 0px 0px 25px;'>
  		<div><img src="<?php echo $logo; ?>" /></div>
        <div style="margin-top:10px;"><table><tr><td style="padding-bottom:3px;"><a style="color:#007F7B;font-weight:bold;text-decoration:none;" href="<?php echo $back_url; ?>">BACK TO STORE</a></td></tr></table></div>
    </div>
    <div id="top_menu">
      <div id="top_menu_a" class="div_child">
        <div style="text-align:center;padding-top:10px;" onClick="$('#show_help').show();"><img src='<?php echo designdic; ?>/icons/help.png' /><br/>
          <?php echo $top_1_text; ?></div>
      </div>
      <div id="top_menu_b" class="div_child">
        <div onClick="showCurrentSidePreview();" style="text-align:center;padding-top:10px;"><img src='<?php echo designdic; ?>/icons/preview.png' /><br/>
          <?php echo $top_2_text; ?></div></a>
      </div>
      <!--<div id="top_menu_c" class="div_child">
        <div onClick="zoomimage();" style="text-align:center;padding-top:10px;"><img src='<?php echo designdic; ?>/icons/zoom.png' /><br/>
          ZOOM</div>
      </div>-->
      <div id="top_menu_d" class="div_child">
        <div onClick="resetCurrentDesign();"  style="text-align:center;padding-top:10px;"><img src='<?php echo designdic; ?>/icons/trash.png' /><br/>
          <?php echo $top_3_text; ?></div>
      </div>
    </div>
    <div id="left_menu">
      <div onClick="showpanel(1);" id="left_menu_a">
        <table>
          <tr>
            <td><div style="background:URL('<?php echo designdic; ?>icons/iphone.png');width:22px;height:22px;border:0;" ></div></td>
            <td>&nbsp;<?php echo $left_1_text; ?></td>
          </tr>
        </table>
       </div>
       <div onClick="showpanel(5);" id="left_menu_e">
        <table>
          <tr>
            <td><div style="background:URL('<?php echo designdic; ?>icons/bg.png');width:22px;height:22px;border:0;" ></div></td>
            <td>&nbsp;<?php echo $left_2_text; ?></td>
          </tr>
        </table>
       </div>
      <div onClick="showpanel(2);" id="left_menu_b">
         <table>
          <tr>
            <td><img src='<?php echo designdic; ?>/icons/add-text.png' /></td>
            <td>&nbsp;<?php echo $left_3_text; ?></td>
          </tr>
        </table>
      </div>
      <div onClick="showpanel(3);" id="left_menu_c">
        <table>
          <tr>
            <td><img src='<?php echo designdic; ?>/icons/add-art.png' /></td>
            <td>&nbsp;<?php echo $left_4_text; ?></td>
          </tr>
        </table>
      </div>
      <div onClick="showpanel(4);" id="left_menu_d">
        <table>
          <tr>
            <td><img src='<?php echo designdic; ?>/icons/upload.png' /></td>
            <td>&nbsp;<?php echo $left_5_text; ?></td>
          </tr>
        </table>
      </div>
    </div>
     <div id="big_loader" style="display:none;position:absolute;top:0;margin:300px 0px 0px 580px;"><img src="<?php echo designdic; ?>images/al_big.gif" /></div>
	 <div id="add_loader" style="display:none;position:absolute;top:0;margin:300px 0px 0px 430px;z-index:1111111111;"><img src="<?php echo designdic; ?>images/wait.gif" /></div>
	<div id="design_area">
      <div id="front" style="display:block;" onmouseover='hideCorner(1);' onmouseout='hideCorner(2);'>
		<div id="image_placeholder_front">
			<div id="image_placeholder_back_tra"></div>
        	<div id="addimage"></div>
		</div>
      </div>
    </div>
  </div>
  
  <div id="right_panel">
  	<div style="font-weight:bold; font-size:18px;color:#8B7F7C;margin:5px 0px 0px 10px; "><?php echo $right_box_header; ?></div>
	<hr>
	<div>
		<?php $str = 0; if(isset($options)) { ?>
        <form name="frmDesign" id="frmDesignUpload" enctype="multipart/form-data" style="margin:0px">
          <div class="product-info">
            <table>
              <tr id="opt_area">
                <?php $z=0; foreach ($options as $option) { if($z == 2){break;}?>
                <td align="center" style="padding-right:8px;"><?php if ($option['type'] == 'select') { ?>
                  <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                    <?php if ($option['required']) { ?>
                    <span class="required">*</span>
                    <?php } ?>
                    <b><?php echo $option['name']; ?>:</b>
                    <select name="option[<?php echo $option['product_option_id']; ?>]">
                      <option value=""><?php echo $text_select; ?></option>
                      <?php foreach ($option['option_value'] as $option_value) { ?>
                      <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                      <?php if ($option_value['price']) { ?>
                      (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                      <?php } ?>
                      </option>
                      <?php } ?>
                    </select>
                  </div>
                  <?php }?></td>
                <?php $z += 1;}?>
              </tr>
            </table>
            <input type="hidden" name="product_id" id="product_id" value="<?php echo $pId; ?>" />
          </div>
		  <div style="clear:both;"></div>
		  <div style="margin-top:10px;">
		  	<table>
				<tr>
					<td style="color:#7F7F7F;padding-left:6px;"><?php echo $add_text_2; ?></td>
					<td><input style="color:#007F7B;font-weight:bold;font-size:20px;text-align:center;" id="quantity" name="quantity" type="text" value="0" size="1" /></td>
					<td><div style="padding-left:8px;font-style:italic;color:#7F7F7F;" align="center"><?php echo $add_text_3; ?> <span id="min_pices" style="color:#007F7B;font-weight:bold;font-size:18px;"><?php echo $min_cust_qty; ?></span> <?php echo $add_text_4; ?></div></td>
				</tr>
			</table>
		  </div>
		 
		  <div style="margin-top:10px;margin-left:3px;font-size:40px; color:#343434;font-weight:bold;">
		  	<table>
				<tr>
					<td id="m_price"><?php echo $manual_price; ?></td>
					<td style="padding-left:20px;font-style:italic;color:#7F7F7F;font-size:18px !important;"><?php echo $add_text_1; ?></td>
				</tr>
			</table>
		  </div>
		  <br>
		  <div align="center">
		  	<a id="add_to_cart" onClick="addMultiCart();">Add to Cart</a>
		  </div>
		   <input type="hidden" id="size_total_rows" value="<?php echo $str; ?>" />
		   <input type="hidden" id="min_qty_value" value="<?php echo $min_cust_qty; ?>" />
		   
        </form>
        <?php }?>
	</div>
  </div>
  
  <div style="clear:both;"></div>
  
</div>
<div title="Design Preview" id="imageHolder"></div>
<div title="Zoom View" style="display:none;" class='zoom' id="imageHolder2"></div>
<form method="post" enctype="multipart/form-data" id="sform">
  <input type="hidden" name="arrayobjectfront" id="arrayobjectfront" />
</form>
<input type="hidden" name="sidechnage" id="sidechnage" value="front" />
<input type="hidden" name="uniqid" id="uniqid" value="<?php echo $uniqid; ?>" />
<input type="hidden" id="hdn_total_prices" value="" />

<input type="hidden" id="manual_price" value="<?php echo $manual_price; ?>" />


<script type="text/javascript">
	function addMultiCart(){
		var total_pices = $('#min_qty_value').val();
		var minqty = $('#quantity').val();
		if(minqty > 0 && parseInt(minqty) >= parseInt(total_pices)){
			$('#add_loader').show();
			saveEachImage();
			setTimeout('storeHidden()',4000);
			setTimeout('addCartProduct()',6000);
		}
		else{
			//here alert work
			$('#add_loader').hide();
			alert('<?php echo $min_alert; ?> ' + total_pices);
		}
	}
	
	function gotocartpage(){
		window.location = 'index.php?route=checkout/cart';
	}
</script>


<script type="text/javascript">
  
  var design_work_front = new Array();
  
  function makeObjDragAndResize(id,box){
  	window.zindex = 30;
    $("#"+id).resizable({
		handles: 'se',
		stop: function( event, ui ) {
			updateArrayObjectById(id,'resize_width', ui.size.width);
			updateArrayObjectById(id,'resize_height', ui.size.height);
		}
	});
    $("#"+id).parent().draggable({
        containment: "#" + box,
		scroll: false,
		cursor: 'move',
		stop: function(event, ui) {
			var top = jQuery("#"+id).offset().top - jQuery("#"+box).offset().top;
			var left = jQuery("#"+id).offset().left - jQuery("#"+box).offset().left;
			updateArrayObjectById(id,'drag_top', top.toFixed(2));
			updateArrayObjectById(id,'drag_left', left.toFixed(2));
		}
    });
    /*$("#"+id).rotate({
        bind: {
            dblclick: function() {
                $(this).data('angle', $(this).data('angle')+90);
                var w = $(this).css('width');
                $(this).parent().rotate({ animateTo: $(this).data('angle')});
				$('#image_rotate_angle').val($(this).data('angle'));
				updateArrayObjectById(id,'rotate_angle',$(this).data('angle'));
            }
        }
    });*/
  }
    
  function storeIntoArray(id,image,drag_top,drag_left,resize_height,resize_width,rotate_angle){
  	//removeOldFromArray(id);
	var myObject = {
	  'id' : id,
	  'image' : image ,
	  'drag_top' : drag_top ,
	  'drag_left' : drag_left ,
	  'resize_height' : resize_height ,
	  'resize_width' : resize_width ,
	  'rotate_angle' : rotate_angle ,
	};
	if($('#sidechnage').val() == 'front'){
		design_work_front.push(myObject);
	}
  }
  
  function removeImage(id){
	$('#' + id).remove();
	removeOldFromArray(id);	
  }

  function addImageToRemovePanel(id,src){
  	$("#image_coll").append("<div id=ss_"+ id +" class='deleteDiv'><a href='javascript:void(0);' onclick=removeImage('" + id + "');><img id="+ id +" class='imgremove' width='40' height='40' data-angle='0' src=" + src + " /></a></div>");
  }
  
function copyImageBeforeSendPanel(obj){
	$('#added_logo_loader').show();
	$.ajax({
		type: "POST",
		url: '<?php echo designdic; ?>copyimage.php',
		//data: 'simage=' + obj.attributes[0].nodeValue.split('/'),
		data: 'simage=' + obj,
		success: function(msg) {
			//success
			if(msg != '-1'){
				setTimeout("sendImageToPanel('" + msg + "')",1000);
			}
			else{
				alert('Error Occured, please reload the again');
				$('#added_logo_loader').hide();
			}
		},
	});
  }
  
  function sendImageToPanel(image){
	var uniqId = uniqueid();
	var image_url = '<?php echo HTTP_SERVER; ?>/image/data/upload_image/' + image;
	$("#addimage").append("<div style='position:absolute;top:35%;left:44%;'><img id="+ uniqId +" class='resize' width='80' height='80' data-angle='0' src=" + image_url + " /></div>");	
	makeObjDragAndResize(uniqId,$('#sidechnage').val());
	storeIntoArray(uniqId,image,0,0,0,0,0);
	$("#"+uniqId).before("<div title='Delete' id=rmv_" + uniqId +" onclick=removeImage('" + uniqId + "'); class='delete_icon'></div>");
	hideCorner(2);
	$('#added_logo_loader').hide();
  }
  
  function sendImageToPanelAfterUplaod(image_name){
	var uniqId = uniqueid();
	var image_url = '<?php echo HTTP_SERVER; ?>image/data/upload_image/' + image_name;
	//var image_url = '<?php echo designdic; ?>read_image.php?token=u&img=' + image_name;
	$("#addimage").append("<div style='position:absolute;top:35%;left:44%;'><img id="+ uniqId +" class='resize' width='80' height='80' data-angle='0' src=" + image_url + " /></div>");	
	makeObjDragAndResize(uniqId,$('#sidechnage').val());
	storeIntoArray(uniqId,image_name,0,0,0,0,0);
	$("#"+uniqId).before("<div title='Delete' id=rmv_" + uniqId +" onclick=removeImage('" + uniqId + "'); class='delete_icon'></div>");
	hideCorner(2);
  }
  
  function updateArrayObjectById(id,update_key,update_value){
	if($('#sidechnage').val() == 'front'){
		for(i=0;i<design_work_front.length;i++){
			if(typeof design_work_front[i] != "undefined" && $.trim(design_work_front[i]['id']) == $.trim(id)){
				design_work_front[i][update_key] = update_value;
			}	
		}
	} 
  }
  
  function removeOldFromArray(id){
  	if($('#sidechnage').val() == 'front'){
		for(i=0;i<design_work_front.length;i++){
			if(typeof design_work_front[i] != "undefined" && $.trim(design_work_front[i]['id']) == $.trim(id)){
				delete design_work_front[i];
			}
		}
	}
  }
  
  function storeHidden(){
  	$('#arrayobjectfront').val(JSON.stringify(design_work_front));

 $.ajax({
	   type: "POST",
	   url: '<?php echo designdic; ?>marge.php?did=' + $('#uniqid').val(),
	   data: $("#sform").serialize(),
	   dataType: 'json',
	   success: function(json) {
			//after success
		}
	 });
  }
  
  function setImage(side){
  	$('#sidechnage').val(side);
  	if(side == 'front'){
		$('#front').show();
	} 
  }
  
function saveIntoArrayAfterConvertTextToImage(uniqId){
	var image_url = '<?php echo HTTP_SERVER; ?>image/data/upload_image/' + uniqId + '.png';
	var image_name = uniqId + '.png';
	$("#addimage").append("<div style='position:absolute;top:35%;left:44%;'><img id="+ uniqId +" class='resize' width='150' height='45' data-angle='0' src="+ image_url +" /></div>");	
	makeObjDragAndResize(uniqId,$('#sidechnage').val());
	storeIntoArray(uniqId,image_name,0,0,0,0,0);
	$("#"+uniqId).before("<div title='Delete' id=rmv_" + uniqId +" onclick=removeImage('" + uniqId + "'); class='delete_icon'></div>");
	hideCorner(2);
}

function createTextToImage(obj){
	var uniqId = uniqueid();
	createAjaxImage($('#designtext').val(),uniqId);
	setTimeout("saveIntoArrayAfterConvertTextToImage('" + uniqId + "')",1000);
}

function createAjaxImage(text,guid){
	$('#ajaxloader').show();
	var str = "<?php echo designdic; ?>png_text.php?col=" + $('#tcolor').val() + "&bg=F24B17&ft=" + $('#optfont').val() + "&guid=" + guid;
	$.ajax({
		type: "POST",
		url: str,
		data: 'str=' + text,
		success: function(msg) {
			setTimeout("$('#ajaxloader').hide()",2000);
		},
	});
}


function addCartProduct(){
    $.ajax({
           type: "POST",
           url: 'index.php?route=checkout/cart/add',
           data: $("#frmDesignUpload").serialize() + '&option[customId]='+$('#uniqid').val(),
		   dataType: 'json',
           success: function(json) {
				$('.success, .warning, .attention, information, .error').remove();
				
				if (json['error']) {
					$('#addprocess').hide();
					if (json['error']['option']) {
						for (i in json['error']['option']) {
							$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
						}
					}
				} 
				
				if (json['success']) {
					//window.parent.$('#designitpopup').dialog('close');
					//parent.addCartStyle(json['success'],json['total']);
					gotocartpage();
				}
				$('#add_loader').hide();	
			}
         });

    return false;
}

</script>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
//--></script>
<script type="text/javascript">
jQuery(function(){
	var btnUpload=$('#upload');
	//var status=$('#status');
	new AjaxUpload(btnUpload, {
		action: '<?php echo designdic; ?>upload.php',
		name: 'uploadfile',
		onSubmit: function(file, ext){
			 $('#alimageupload').show();
			 if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
				alert('Only JPG, PNG or GIF files are allowed');
				return false;
			}
		},
		onComplete: function(file, response){
			if(response != 'error' && response != '-9'){
				sendImageToPanelAfterUplaod(response);
			}
			else if(response == '-9'){
				alert('Only JPG, PNG or GIF files are allowed');
			}
			else{
				alert('upload error, please try again');
			}
			$('#alimageupload').hide();	
		}
	});
	
});

function uniqueid(){
    var idstr=String.fromCharCode(Math.floor((Math.random()*25)+65));
    do {                
        var ascicode=Math.floor((Math.random()*42)+48);
        if (ascicode<58 || ascicode>64){
            idstr+=String.fromCharCode(ascicode);    
        }                
    } while (idstr.length<32);

    return (idstr);
}

function delaysaveImage(side){
	setImage(side);
	html2canvas($('#'+side), {
		proxy: '<?php echo designdic; ?>html2canvasproxy.php',
		onrendered: function(canvas) {var dataURL = canvas.toDataURL("image/png");$.post('<?php echo designdic; ?>saveimage.php?did=' + $('#uniqid').val(),{image: dataURL,side: side},function(data){});}
	});
}
function saveEachImage(){
	delaysaveImage('front');
}



function showCurrentSidePreview(){
	$('#big_loader').show();
	html2canvas($('#'+$('#sidechnage').val()), {
		proxy: '<?php echo designdic; ?>html2canvasproxy.php',
		onrendered: function(canvas) {
			var dataURL = canvas.toDataURL("image/png");
			jQuery("#imageHolder").dialog({
			 modal: true
			 , width: 550
			 , height: 575
			 , resizable: false
			 , draggable: false
			});
			jQuery("#imageHolder").html("<img src='"+dataURL+"' />");
			$('#big_loader').hide();
		}
	});
}


function hideCorner(token){
	if(token == 1){jQuery('.ui-resizable-se').show();$(".delete_icon").show();}
	else{jQuery('.ui-resizable-se').hide();$(".delete_icon").hide();}
}


function loadImage(catid){
	$('#added_logo_loader').show();
	var str = 'index.php?route=designit/designit/getclipartimage&catid=' + catid;
	$.ajax({
		type: "GET",
		url: str,
		success: function(output) {
			$('#showimages').html(output);
			$('#showimages').show();
			$('#catlist').hide();
			$('#added_logo_loader').hide();
		},
	});
}


//here change customize product
function changeImage(productid){
	$('#product_id').val(productid);
	$('#big_loader').show();
	var bg_html = '';
	var str = 'index.php?route=designit/designit/getAjaxCustomImage&product_id=' + productid;
	$.ajax({
		dataType: 'json',
		url: str,
		success: function(json) {
			//big
			$('#image_placeholder_front').css('background-image','url('+ json['image_big']['front'] +')');
			
			//min pices order
			$('#min_pices').html(json['product']['min_pic']);
			$('#min_qty_value').val(json['product']['min_pic']);
			for(i = 0; i<json['product']['bg'].length;i++){				
				bg_html += "<div id='clist' class='str_bg'><a onclick='chnageBGImage("+json['product']['bg'][i]['pg_id']+");' href='javascript:void(0);'><img src='"+json['product']['bg'][i]['image']+"'></a><input type='hidden' value='"+json['product']['bg'][i]['image2']+"' id='bg_"+json['product']['bg'][i]['pg_id']+"' /></div>";
			}
			
			$('#me_bg').html(bg_html);
			$('#m_price').html(json['product']['manual_price']);
			//option
			$('#opt_area').html(json['product']['option']);
			/*$('#size_total_rows').val(json['product']['total_size']);
			$('#uniqid').val(json['product']['uniqid']);*/
			
			//default work
			//$('#txt_total_pices').val(0);
			$('#min_qty_value').val(json['product']['min_pic']);
			
			$('#big_loader').hide();
		},
	});
}


function switchpanel(){
	$('#showimages').html('');
	$('#showimages').hide();
	$('#catlist').show();	
}

function showpanel(token){
	if(token == 1){
		$('#show_custom_product_panel').show();
		$('#show_text_panel').hide();
		$('#show_image_panel').hide();
		$('#show_addart_panel').hide();
		$('#show_custom_product_bg').hide();
	}
	else if(token == 2){
		$('#show_custom_product_panel').hide();
		$('#show_text_panel').show();
		$('#show_image_panel').hide();
		$('#show_addart_panel').hide();
		$('#show_custom_product_bg').hide();
	}
	else if(token == 3){
		$('#show_custom_product_panel').hide();
		$('#show_text_panel').hide();
		$('#show_image_panel').hide();
		$('#show_addart_panel').show();
		$('#show_custom_product_bg').hide();
	}
	else if(token == 4){
		$('#show_custom_product_panel').hide();
		$('#show_text_panel').hide();
		$('#show_image_panel').show();
		$('#show_addart_panel').hide();
		$('#show_custom_product_bg').hide();
	}
	else if(token == 5){
		$('#show_custom_product_panel').hide();
		$('#show_text_panel').hide();
		$('#show_image_panel').hide();
		$('#show_addart_panel').hide();
		$('#show_custom_product_bg').show();
	}
}

function close_tip(id){
	$('#'+id).hide();
}

function resetCurrentDesign(){
  for(i=0;i<design_work_front.length;i++){
		if(typeof design_work_front[i] != "undefined"){
			$('#' + design_work_front[i]['id']).remove();
			delete design_work_front[i];
		}
	}
} 

function zoomimage(){
	$('#big_loader').show();
	html2canvas($('#printme'), {
		proxy: '<?php echo designdic; ?>html2canvasproxy.php',
		onrendered: function(canvas) {
			var dataURL = canvas.toDataURL("image/png");
			jQuery("#imageHolder2").dialog({
			 modal: true
			 , width: 550
			 , height: 600
			 , resizable: false
			 , draggable: false
			});
			jQuery("#imageHolder2").html("<img src='"+dataURL+"' />");
			$('#big_loader').hide();
		}
	});
} 

function chnageBGImage(pg_id){
	var b_url = $('#bg_' + pg_id).val();
	$('#image_placeholder_front').css('background-image','url('+ b_url +')');
}
                                      
$(document).ready(function(){
	document.getElementById('image_placeholder_back_tra').style.zIndex="1001";	
});
</script>

<div id="show_text_panel" style="max-width: 600px; transition-duration: 350ms; animation-duration: 350ms; top: 180px; left: 128px;" class="tooltipster-base tooltipster-light tooltipster-fade-show tooltipster-dying"><div class="tooltipster-content"><div style="float:left" class="header_text"><?php echo $left_popup_text_3; ?></div><div style="float:right;"><a onClick="close_tip('show_text_panel');" href="javascript:void(0);"><img src='<?php echo designdic; ?>icons/close.png'/></a></div><div style="clear:both;"></div><div><table cellspacing="2" cellpadding="2"><tbody><tr><td><span>Font:</span></td><td>

<select name="optfont" id="optfont">
<?php foreach($font_coll as $fontall) { ?>
	<option value="<?php echo $fontall['font']; ?>"><?php echo $fontall['font']; ?></option>     	
<?php } ?>
</select>


</td></tr><tr><td><span>Color:</span></td><td><input type="text" class="color" id="tcolor" size="5"></td></tr><tr><td valign="top"><span>Text:</span></td><td><textarea cols="20" rows="2" id="designtext" placeholder="Write Your Text" name="designtext"></textarea></td></tr><tr><td>&nbsp;</td><td><input type="button" onClick="createTextToImage();" value="Next" class="theme_a">&nbsp;&nbsp; <img style="display:none;" id="ajaxloader" src="<?php echo designdic; ?>images/al.gif"></td></tr></tbody></table></div></div><div style="" class="tooltipster-arrow-right tooltipster-arrow"><span style="margin-left: -1px; border-color: rgb(204, 204, 204);;" class="tooltipster-arrow-border"></span><span style="border-color:rgb(237, 237, 237);"></span></div></div>


<div id="show_addart_panel" style="z-index:999999;width:580px;height:350px;transition-duration: 350ms; animation-duration: 350ms; top: 165px; left: 128px;" class="tooltipster-base tooltipster-light tooltipster-fade-show tooltipster-dying"><div class="tooltipster-content"><div style="float:left" class="header_text"><?php echo $left_popup_text_4; ?></div><div style="float:right;"><a onClick="close_tip('show_addart_panel');" href="javascript:void(0);"><img src='<?php echo designdic; ?>icons/close.png'/></a></div><div style="clear:both;"></div><div>

<div style="float:left;width:125px;min-height:300px;border-right:solid 1px #ccc;">
     <br/>
	 <table>
	    <?php foreach($categorylist as $catlist) { ?>
        	<tr>
				<td><a href="javascript:void(0);" onClick="loadImage(<?php echo $catlist['caid']; ?>);"><img src="<?php echo $catlist['category_image']; ?>" /></a></td>
					<td><a style="color:<?php echo $art_text_color; ?>;text-decoration:none;font-weight:bold;font-size:12px;" href="javascript:void(0);" onClick="loadImage(<?php echo $catlist['caid']; ?>);"><?php echo $catlist['category_name']; ?></a>
				</td>
			</tr>
        <?php } ?>
	</table>
 </div>
 <div id='added_logo_loader' style="display:none;position:absolute;top:0;left:0;margin-left:325px; margin-top:150px;">
		<img style="width:32px !important; height:32px !important;" src="<?php echo designdic; ?>images/al.gif" />
</div>

<div id="showimages" style="float:left;margin-left:20px; width:400px;">

</div>

</div></div><div style="" class="tooltipster-arrow-right tooltipster-arrow"><span style="margin-left: -1px; border-color: rgb(204, 204, 204);;" class="tooltipster-arrow-border"></span><span style="border-color:rgb(237, 237, 237);"></span></div></div>

<div id="show_name_number_panel" style="width:300px;height:450px;transition-duration: 350ms; animation-duration: 350ms; top: 165px; left: 128px;" class="tooltipster-base tooltipster-light tooltipster-fade-show tooltipster-dying"><div class="tooltipster-content"><div style="float:left" class="header_text">Name & Number</div><div style="float:right;"><a onClick="close_tip('show_name_number_panel');" href="javascript:void(0);"><img src='<?php echo designdic; ?>icons/close.png'/></a></div><div style="clear:both;"></div><div>

</div></div><div style="" class="tooltipster-arrow-right tooltipster-arrow"><span style="margin-left: -1px; border-color: rgb(204, 204, 204);;" class="tooltipster-arrow-border"></span><span style="border-color:rgb(237, 237, 237);"></span></div></div>

<div id="show_image_panel" style="z-index:999999;width:240px; transition-duration: 350ms; animation-duration: 350ms; top: 340px; left: 160px;" class="tooltipster-base tooltipster-light tooltipster-fade-show tooltipster-dying"><div class="tooltipster-content"><div style="float:left" class="header_text"><?php echo $left_popup_text_5; ?></div><div style="float:right;"><a onClick="close_tip('show_image_panel');" href="javascript:void(0);"><img src='<?php echo designdic; ?>icons/close.png'/></a></div><div style="clear:both;"></div><div align="left" style="display:block;width:100px;" class="theme_a" id="upload">Upload Image</div><div style="margin-left:20px;margin-top:15px;"><img style="display:none;" id="alimageupload" src="<?php echo designdic; ?>images/al.gif"></div></div><div style="" class="tooltipster-arrow-right tooltipster-arrow"><span style="margin-left: -1px; border-color: rgb(204, 204, 204);;" class="tooltipster-arrow-border"></span><span style="border-color:rgb(237, 237, 237);"></span></div></div>


<div id="show_custom_product_panel" style="z-index:999999;border: solid 5px #666;width:550px;overflow:auto; transition-duration: 350ms;height:376px; animation-duration: 350ms; top: 101px; left: 100px;" class="tooltipster-base tooltipster-light tooltipster-fade-show tooltipster-dying"><div class="tooltipster-content" style="border: none !important;"><div style="float:left" class="header_text"><?php echo $left_popup_text_1; ?></div><div style="float:right;"><a onClick="close_tip('show_custom_product_panel');" href="javascript:void(0);"><img src='<?php echo designdic; ?>icons/close.png'/></a></div><div style="clear:both;">
</div>

<?php foreach($cust_list as $cps) { ?><div id='clist' style='float:left;border:solid 1px #ccc;border-radius:6px;padding:2px;'><a onclick='changeImage(<?php echo $cps['product_id']; ?>);' href='javascript:void(0);'><img src='<?php echo $cps['image']; ?>'></a></div><?php } ?>

</div><div style="" class="tooltipster-arrow-right tooltipster-arrow"><span style="margin-left: -1px; border-color: rgb(204, 204, 204);" class="tooltipster-arrow-border"></span><span style="border-color:rgb(237, 237, 237);top:50% !important;"></span></div></div>


<div id="show_custom_product_bg" style="z-index:999999;width:550px;transition-duration: 350ms;display:none; animation-duration: 350ms; top: 50px; left: 100px;" class="tooltipster-base tooltipster-light tooltipster-fade-show tooltipster-dying"><div class="tooltipster-content"><div style="float:left" class="header_text"><?php echo $left_popup_text_2; ?></div><div style="float:right;"><a onClick="close_tip('show_custom_product_bg');" href="javascript:void(0);"><img src='<?php echo designdic; ?>icons/close.png'/></a></div><div style="clear:both;">
</div>

<div id="me_bg">
<?php foreach($pbg_list as $cps) { ?><div id='clist' class='str_bg'><a onClick="chnageBGImage('<?php echo $cps['pg_id']; ?>');" href='javascript:void(0);'><img src='<?php echo $cps['image']; ?>'></a><input type="hidden" value="<?php echo $cps['image2']; ?>" id="bg_<?php echo $cps['pg_id']; ?>" /></div><?php } ?>
</div>

</div><div style="" class="tooltipster-arrow-right tooltipster-arrow"><span style="margin-left: -1px; border-color: rgb(204, 204, 204);;" class="tooltipster-arrow-border"></span><span style="border-color:rgb(237, 237, 237);top:50% !important;"></span></div></div>




<div id="show_help" style="transition-duration: 350ms;display:hide;display:none;animation-duration: 350ms; top: 70px; left: 350px;min-height:200px;width:400px;" class="tooltipster-base tooltipster-light tooltipster-fade-show tooltipster-dying">

<div class="tooltipster-content" style="line-height:22px;">
<?php echo $help_text; ?>
<div style="float:right;margin:0px 5px 5px 0px;"><a onClick="close_tip('show_help');" href="javascript:void(0);"><img src='<?php echo designdic; ?>icons/close.png'/></a></div>
</div>

<div style="" class="tooltipster-arrow-bottom tooltipster-arrow"><span style="border-color:rgb(255, 255, 255);"></span></div></div>



</body>
</html>
