<?php echo $header; ?>

      <div class="breadcrumbs animated fadeIn">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>

      <h2 class="product-title animated fadeIn"><?php echo $heading_title; ?></h2>
  <h1><?php echo $heading_title; ?></h1>
  <?php if (isset($calendar_info)) { ?>
    <div class="content" <?php if ($image) { echo 'style="min-height: ' . $min_height . 'px;"'; } ?>>
      <?php if ($image) { ?>
        <img align="right" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
      <?php } ?>
      <h3 style="margin-top: 0px;"><?php echo $title; ?></h3>
      <?php echo $description; ?>
    </div>
    <?php if ($products) { ?>
    <div class="box">
      <div class="box-heading"><?php echo $heading_products; ?></div>
      <div class="box-content">
        <div class="box-product">
          <?php foreach ($products as $product) { ?>
          <div>
            <?php if ($product['thumb']) { ?>
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
            <?php } ?>
            <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
            <?php if ($product['price']) { ?>
            <div class="price">
              <?php if (!$product['special']) { ?>
              <?php echo $product['price']; ?>
              <?php } else { ?>
              <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
              <?php } ?>
            </div>
            <?php } ?>
            <?php if ($product['rating']) { ?>
            <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
            <?php } ?>
            <div class="cart"><a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button"><span><?php echo $button_cart; ?></span></a></div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <?php } ?>
    <div class="buttons">
      <div class="right"><a onclick="location='<?php echo $continue; ?>'" class="btn light-blue darken-1"><span><?php echo $button_continue; ?></span></a></div>
    </div>
  <?php } elseif (isset($calendar_data)) { ?>
    <div class="content" style="padding-bottom: 10px;">
      <?php foreach ($calendar_data as $event) { ?>
        <table>
          <tr>
            <td width="150px" align="left"><?php echo date('F j', strtotime($event['start_date'])); ?></td>
            <td><a href="<?php echo $event['href']; ?>"><?php echo $event['title']; ?></a></td>
          </tr>
        </table>
      <?php } ?>
    </div>
    <div class="buttons">
      <div class="right"><a onclick="location = '<?php echo $continue; ?>'" class="btn light-blue darken-1"><span><?php echo $button_continue; ?></span></a></div>
    </div>
  <?php } ?>
  <?php //echo $content_bottom; ?>
</div>
<?php echo $footer; ?>
