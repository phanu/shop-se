<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<style>
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active{
border:1px solid #CCCCCC !important;
}
</style>
<div id="content"><?php echo $content_top; ?>
  <table width="96%" border="0" cellpadding="2" cellspacing="2" align="center">
    <tr>
      <td align="left"><div class="listing_breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
          <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
          <?php } ?>
        </div></td>
    </tr>
    <tr>
      <td><h1 style="text-transform:uppercase;margin:0px;"><?php echo $heading_title; ?></h1></td>
    </tr>
    <tr>
      <td><hr></td>
    </tr>
  </table>
  <div id="innerinfopage">
    <div id="bigcont_bg" align="center">
      <div class="content" style="margin:0px !important;padding:0px;">
        <?php if (isset($topics)) { ?>
        <div class="demo" style="width:92%">
          <div id="accordion">
            <?php foreach ($topics as $topic) { ?>
            <h3 style="text-align:left;text-transform:uppercase;"><a href="#" style="color:#000000;"><?php echo $topic['title']; ?></a></h3>
            <div  style="text-align:left;background:none;"> <?php echo $topic['faq_dec']; ?> </div>
            <?php } ?>
          </div>
        </div>
        <?php } ?>
        <script>
	$(function() {
		$( "#accordion" ).accordion();
	});
	</script>
      </div>
    </div>
  </div>
  <?php //echo $content_bottom; ?> </div>
<div id="contentimg"></div>
<?php echo $footer; ?> 