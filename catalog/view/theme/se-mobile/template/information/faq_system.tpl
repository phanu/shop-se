<?php echo $header; ?>

      <div class="breadcrumbs animated fadeIn">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>

      <h2 class="product-title animated fadeIn"><?php echo $heading_title; ?></h2>
  <h1><?php echo $heading_title; ?></h1>

  <?php if ($allow_propose) { ?>
    <div class="attention"><?php echo $text_info; ?></div>
	
	<div id="faq-propose-dialog" title="<?php echo $text_propose; ?>">
		<table class="form" id="form-propose">      
			<tr>
			  <td><span class="required">*</span> <?php echo $entry_question; ?></td>
			  <td><textarea name="question" id="faq-question" cols="60" rows="6"></textarea></td>
			</tr>
			<tr>
			  <td><span class="required">*</span> <?php echo $entry_captcha; ?></td>
			  <td><input type="text" name="captcha" id="faq-captcha" value="" /><br />
				  <img src="index.php?route=product/product/captcha" alt="" id="faq-captcha-image" />
			  </td>
			</tr>
		</table>	
		<div class="buttons">
		  <div class="right">
			<a class="button" id="faq-save-dialog"><span><?php echo $button_save_dialog; ?></span></a>
		  </div>
		</div>
	</div>
  <?php } ?>
  
  <?php if ($faq) { ?>
	<?php foreach($faq as $category) { ?>
			<div class="faq-category-name" id="faq-cat-<?php echo $category['category_id']; ?>"><?php echo $category['category_name']; ?></div>
			<div id="faq-cat-content-<?php echo $category['category_id']; ?>" class="faq-category-content">
				<?php foreach($category['questions'] as $qa_info){ ?>
					<div id="faq-question-<?php echo $qa_info['question_id']; ?>" class="faq-question"><a><?php echo $qa_info['question']; ?></a></div>
					<div id="faq-answer-<?php echo $qa_info['question_id']; ?>" class="faq-answer"><?php echo $qa_info['answer']; ?></div>
				<?php } ?>
			</div>
	<?php } ?>
  <?php } ?>
  
  <?php //echo $content_bottom; ?></div>
  
<script type="text/javascript">
$('.faq-question a').bind('click', function(){
	$('#' + $(this).parent().attr('id').replace('faq-question-', 'faq-answer-')).toggle();
});

$('#faq-propose').bind('click', function(){
	$('.warning').remove();
	$('.success').remove();

	$('#faq-propose-dialog').dialog({
		modal: true,
		width: 500,
		height: 390
	});
});

$('#faq-save-dialog').bind('click', function(){
	$.ajax({
		type: 'POST',
		url : 'index.php?route=information/faq_system/propose',
		data: $('#form-propose textarea, #form-propose input[type=\'text\']'),
		dataType: 'json',
		success: function(json){
			$('.warning').remove();
			$('.success').remove();
			
			if (json['error']){
				$('#form-propose').before('<div class="warning">' + json['error'] + '</div>');
				$('.warning').fadeIn('slow');
			}
			
			if (json['success']){
				$('#form-propose').before('<div class="success">' + json['success'] + '</div>');
				$('.success').fadeIn('slow');
				
				$('#faq-captcha-image').attr('src', 'index.php?route=product/product/captcha&random=' + Math.random());
				$('#faq-question').val('');
				$('#faq-captcha').val('');
				
				setTimeout(function() {
					$('#faq-propose-dialog').dialog("close");
				}, 2000);
			}
		}		
	});
});
</script>  
  
<?php echo $footer; ?>