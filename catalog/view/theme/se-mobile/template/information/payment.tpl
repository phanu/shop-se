<?php echo $header; ?>



      <h2 class="product-title animated fadeIn">แจ้งชำระเงิน</h2>

  <?php if($is_member){/*permission require*/ ?>
  <?php   if($isLogged){/*logged*/ ?>

  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="payment">
   <h2 class="product-title animated fadeIn"><?php echo $text_location; ?></h2>
    <div class="contact-info">
      <div class="content"><div class="left"><b><?php echo $text_address; ?></b>
        <?php echo $store; ?>
        <?php echo $address; ?></div>
      <div class="right">
        <?php if ($store_telephone) { ?>
        <b><?php echo $text_telephone; ?></b>
        <?php echo $store_telephone; ?>
        
        <?php } ?>
        <?php if ($fax) { ?>
        <b><?php echo $text_fax; ?></b>
        <?php echo $fax; ?>
        <?php } ?>
      </div>
    </div>
    </div>
    <h4 style="margin-top: 20px; margin-bottom: 10px; font-weight: bold; "><?php echo $heading_title; ?></h4>
    <div class="content">
    <b><?php echo $entry_name; ?></b>
    <input type="text" name="name" value="<?php echo $name; ?>" />
    
    <?php if ($error_name) { ?>
    <span class="error"><?php echo $error_name; ?></span>
    <?php } ?>
    
    <b><?php echo $entry_email; ?></b>
    <input type="text" name="email" value="<?php echo $email; ?>" />
    
    <?php if ($error_email) { ?>
    <span class="error"><?php echo $error_email; ?></span>
    <?php } ?>
    
    <b><?php echo $entry_telephone; ?></b>
    <input type="text" name="telephone" value="<?php echo $telephone; ?>" />
    
    <?php if ($error_telephone) { ?>
    <span class="error"><?php echo $error_telephone; ?></span>
    <?php } ?>
    
    <!-- Mods -->
    <b><?php echo $entry_bank; ?></b>
              <select name="bank">
                <option value="FALSE" selected="selected"><?php echo $text_select_bank; ?></option>
		<?php foreach($payments as $payment){ ?>
		<?php if($payment['name'] == $bank){ ?>
		<option value="<?php echo $payment['name']; ?>" selected="selected"><?php echo $payment['name']; ?></option> 
		<?php }else{ ?>
		<option value="<?php echo $payment['name']; ?>"><?php echo $payment['name']; ?></option> 
		<?php } ?>
		<?php } ?>
             </select>
    
    <?php if ($error_bank) { ?>
    <span class="error"><?php echo $error_bank; ?></span>
    <?php } ?>
    
    <b><?php echo $entry_order; ?></b>
    <input type="text" name="order_id" value="<?php echo $order_id; ?>" />
    
     หากท่านไม่ทราบรหัสคำสั่งซื้อ นั่นอาจเกิดจากท่านทำรายการสั่งซื้อไม่สำเร็จ โปรดตรวจสอบวิธีการสั่งซื้ออีกครั้งที่ http://goo.gl/sJnnt
    
    <?php if ($error_order) { ?>
    <span class="error"><?php echo $error_order; ?></span>
    <?php } ?>
        
    <b><?php echo $entry_total; ?></b>
    <input type="text" name="total" value="<?php echo $total; ?>" />
    
    <?php if ($error_total) { ?>
    <span class="error"><?php echo $error_total; ?></span>
    <?php } ?>
    
    <b><?php echo $entry_paid_date; ?></b>
    <input type="text" name="paid_date" value="<?php echo $paid_date; ?>" class="datepicker"/>
    
    <?php if ($error_paid_date) { ?>
    <span class="error"><?php echo $error_paid_date; ?></span>
    <?php } ?>
    
    <b><?php echo $entry_paid_time; ?></b>
    <input type="text" name="paid_time" value="<?php echo $paid_time; ?>" />
    
    <?php if ($error_paid_time) { ?>
    <span class="error"><?php echo $error_paid_time; ?></span>
    <?php } ?>
    
    <b><?php echo $entry_image; ?>(<?php echo $file_size; ?> KB)</b>
          <input type="button" value="<?php echo $entry_image; ?>" id="button-attach" class="button">
          <input type="hidden" name="image" value="" />
    
    <?php if ($error_image) { ?>
    <span class="error"><?php echo $error_image; ?></span>
    <?php } ?>
    

    <!-- eof Mods -->

    <b><?php echo $entry_enquiry; ?></b>
    <textarea name="enquiry" cols="40" rows="10" style="width: 99%;"><?php echo $enquiry; ?></textarea>
    
    <?php if ($error_enquiry) { ?>
    <span class="error"><?php echo $error_enquiry; ?></span>
    <?php } ?>
    
    <h2>กรุณาคลิ๊กที่กล่องสี่เหลี่ยมด้านล่างเพื่อยืนยันว่าท่านไม่ใช่โปรแกรมอัตโนมัติ (Bot)</h2>
     
    <b><?php echo $entry_captcha; ?></b>
    <input type="text" name="captcha" value="<?php echo $captcha; ?>" />
    
    <img src="index.php?route=information/payment/captcha" alt="" />
    <?php if ($error_captcha) { ?>
    <span class="error"><?php echo $error_captcha; ?></span>
    <?php } ?>
    </div>
    <div class="buttons">
      <div class="center"><a onclick="$('#payment').submit();" class="button"><span>ส่งข้อมุูล</span></a></div>
    </div>
  </form>
  <?php echo $content_bottom; ?>
  
  <?php }else{ /* please login*/ ?>

  <?php echo $content_bottom; ?>
  <?php } /* end */?>
  
  <?php }else{/*permission not require*/ ?>
  

  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="payment">
        <div class="line"></div><h3 style="color: #0000ff; margin-bottom: 20px;"> ไม่ต้อง Fax หรือ Scan สลิปให้เสียเวลา ทางร้านเช็คผ่านระบบ Online ได้ค่ะ </h3>
    <div class="entry-content">
      <p class="block semibold margin-bottom_low">ทางร้านตัดรอบที่ 19.00 น. ของแต่ละวัน และจัดส่งในเช้าวันรุ่งขึ้นประมาณ 10.00 น. ยกเว้นวันอาทิตย์ที่หยุดหนึ่งวัน เพราะไปรษณีย์ปิดทำการค่ะ</p>
      <p class="block semibold margin-bottom_low"> กรุณาแจ้งการชำระเงิน <code>หลังจาก</code> ที่ได้ชำระเงินแล้ว โดยสามารถแจ้งได้ 2 วิธีดังนี้</p>

      <p class="block semibold margin-bottom_low"><strong>1. แบบฟอร์มแจ้งการชำระเงินด้านล่าง</strong></p>
      <span class="block semibold margin-bottom_low" style="text-align: center;">หรือ</span>
      <span class="block semibold margin-bottom_low"><strong>2. ทาง SMS โดย ส่ง SMS ไปที่เบอร์ 089-810-3755  พร้อมระบุ</strong></span>
      <pre>รหัสคำสั่งซื้อ ธนาคารที่โอน ยอดที่โอน วัน เวลาที่โอน</pre>
      <span class="block semibold margin-bottom_low">ยกตัวอย่าง</span>
      <pre>14569  กสิกร  650.12  9/11/55  20.30</pre>
      <blockquote style="text-align: left; font-size: 14px;">
        <cite>หมายเหตุ</cite>
        <ul>
            <li>ทางร้านไม่รับการแจ้งโอนเงินทาง SMS ผ่านตู้ ATM ของธนาคาร หรือ ผ่านทาง Internet Banking ทุกธนาคาร</li>
            <li>ทางร้านไม่รับการแจ้งโอนเงินทาง MMS</li>
            <li>หากท่านไม่ทราบรหัสคำสั่งซื้อ นั่นอาจเกิดจากท่านทำรายการสั่งซื้อไม่สำเร็จ โปรดตรวจสอบวิธีการสั่งซื้ออีกครั้งที่ <a href="http://goo.gl/sJnnt" target="_blank">http://goo.gl/sJnnt</a></li>
        </ul>
      </blockquote>
    </div>
    <p class="text-small" style="margin-bottom: 30px;"><span class="bold block">แบบฟอร์มแจ้งการชำระเงิน</span></p>
    <div class="input-field">
        <input type="text" name="name" id="name" value="<?php echo $name; ?>">
        <label for="name"><?php echo $entry_name; ?></label>
        <?php if ($error_name) { ?>
            <span class="error"><?php echo $error_name; ?></span>
        <?php } ?>
    </div>

    <div class="input-field">
        <input type="text" name="email" id="email" value="<?php echo $email; ?>">
        <label for="email"><?php echo $entry_email; ?></label>
        <?php if ($error_email) { ?>
            <span class="error"><?php echo $error_email; ?></span>
        <?php } ?>
    </div>

    <div class="input-field">
        <input type="text" name="telephone" id="telephone" value="<?php echo $telephone; ?>">
        <label for="telephone"><?php echo $entry_telephone; ?></label>
        <?php if ($error_telephone) { ?>
            <span class="error"><?php echo $error_telephone; ?></span>
        <?php } ?>
    </div>
    <div class="input-field">
                        <h3 class="out-label"><?php echo $entry_bank; ?></h3>
                        <select name="bank">
                            <option value="" selected><?php echo $text_select_bank; ?></option>
                            <?php foreach($payments as $payment){ ?>
                                <?php if($payment['name'] == $bank){ ?>
                                <option value="<?php echo $payment['name']; ?>" selected="selected"><?php echo $payment['name']; ?></option> 
                                <?php }else{ ?>
                                <option value="<?php echo $payment['name']; ?>"><?php echo $payment['name']; ?></option> 
                                <?php } ?>
                            <?php } ?>
                        </select>

                        <?php if ($error_bank) { ?>
                        <span class="error"><?php echo $error_bank; ?></span>
                        <?php } ?>
    </div>
    <!-- Mods -->
    
  
    <div class="input-field">
        <input type="text" name="order_id" id="order_id" value="<?php echo $order_id; ?>">
        <label for="order_id"><?php echo $entry_order; ?></label>
        <?php if ($error_telephone) { ?>
            <span class="error"><?php echo $error_order; ?></span>
        <?php } ?>
        <span class="desc">*** หากท่านไม่ทราบรหัสคำสั่งซื้อ นั่นอาจเกิดจากท่านทำรายการสั่งซื้อไม่สำเร็จ โปรดตรวจสอบวิธีการสั่งซื้ออีกครั้งที่ <a target="_blank" href="http://goo.gl/sJnnt">http://goo.gl/sJnnt</a></span>
    </div>

    <div class="input-field">
        <input type="text" name="total" id="total" value="<?php echo $total; ?>">
        <label for="total"><?php echo $entry_total; ?></label>
        <?php if ($error_total) { ?>
            <span class="error"><?php echo $error_total; ?></span>
        <?php } ?>
    </div>
    
    <div class="input-field">
        <h3 class="out-label"><?php echo $entry_paid_date; ?> <?php echo $paid_date; ?></h3>
        <input type="date" name="paid_date" class="datepicker">
        <?php if ($error_paid_date) { ?>
        <span class="error"><?php echo $error_paid_date; ?></span>
        <?php } ?>
    </div>

    <div class="input-field">
        <input type="text" name="paid_time" id="paid_time" value="<?php echo $paid_time; ?>">
        <label for="paid_time"><?php echo $entry_paid_time; ?> ( เช่น 22.41 น.)</label>
        <?php if ($error_paid_time) { ?>
            <span class="error"><?php echo $error_paid_time; ?></span>
        <?php } ?>
    </div>
    <!-- eof Mods -->

    <div class="input-field">
        <input type="text" name="paid_time" id="paid_time" value="<?php echo $paid_time; ?>">
        <label for="paid_time"><?php echo $entry_paid_time; ?> ( เช่น 22.41 น.)</label>
        <?php if ($error_paid_time) { ?>
            <span class="error"><?php echo $error_paid_time; ?></span>
        <?php } ?>
    </div>

    <div class="input-field">
        <span class="semibold block"><?php echo $entry_enquiry; ?></span>
        <textarea name="enquiry" cols="40" rows="4" style="width: 99%;"><?php echo $enquiry; ?></textarea>
        <?php if ($error_enquiry) { ?>
        <span class="error"><?php echo $error_enquiry; ?></span>
        <?php } ?>
    </div>
    
    
    <!--<h2>กรุณาคลิ๊กที่กล่องสี่เหลี่ยมด้านล่างเพื่อยืนยันว่าท่านไม่ใช่โปรแกรมอัตโนมัติ (Bot)</h2>
     
    <b><?php echo $entry_captcha; ?></b>
    <input type="text" name="captcha" value="<?php echo $captcha; ?>" />
    
    <img src="index.php?route=information/payment/captcha" alt="" />
    <?php if ($error_captcha) { ?>
    <span class="error"><?php echo $error_captcha; ?></span>
    <?php } ?>-->
	<!--new code-->
	<?php //echo $google_captcha; ?>
	<!--new code-->
  
    <div class="buttons">

<button type="button" id="button-confirmorder" class="btn green block" onclick="$('#payment').submit();">ส่งข้อมูล</button>
    </div>
  </form>
  <?php echo $content_bottom; ?>
  
  <?php } ?>
  
  </div>

<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon1.js"></script> 
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/i18n/jquery.ui.datepicker-th.js"></script> 
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
<script type="text/javascript"><!--
$(document).ready(function(){
	if ($.browser.msie && $.browser.version == 6) {
		$('.date, .datetime, .time').bgIframe();
	}

	$('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
	$('.time').timepicker({showSecond: true,timeFormat: 'h:m:s'});

});
//--></script>
<script type="text/javascript"><!--
new AjaxUpload('#button-attach', {
	action: 'index.php?route=information/payment/upload',
	name: 'file',
	autoSubmit: true,
	responseType: 'json',
	onSubmit: function(file, extension) {
		$('#button-attach').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
		$('#button-attach').attr('disabled', true);
	},
	onComplete: function(file, json) {
		$('#button-attach').attr('disabled', false);

		$('.error,#filename').remove();

		if (json['success']) {
			alert(json['success']);

			$('input[name=\'image\']').attr('value', json['file']);
			$('#button-attach').after('<p id="filename"><img width="300px" src="<?php echo HTTP_SERVER; ?>image/payment/' + json['file'] + '" /></p>');
		}

		if (json['error']) {
			$('#button-attach').after('<span class="error">' + json['error'] + '</span>');
		}

		$('.loading').remove();
	}
});
//--></script>
<?php echo $footer; ?>