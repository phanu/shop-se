﻿<?php echo $header; ?>

      <div class="breadcrumbs animated fadeIn">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>

      <h2 class="product-title animated fadeIn"><?php echo $heading_title; ?></h2>
  <h1><?php echo $heading_title; ?></h1>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="contact">
    <h2><?php echo $text_location; ?></h2>
    <div class="contact-info">
      <div class="content"><div class="left"><b><h3>ที่อยู่สำหรับรับส่งสินค้าออนไลน์</h3></b><br />
       ตู้ปณ.44  ปณฝ.ดินแดง กรุงเทพฯ 10407 <br /><br />
       หน้าเว็บเปิดรับ Order 24 ชั่วโมง </br> <a href="http://shop.se-update.com/%E0%B8%A7%E0%B8%B4%E0%B8%98%E0%B8%B5%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%AA%E0%B8%B1%E0%B9%88%E0%B8%87%E0%B8%8B%E0%B8%B7%E0%B9%89%E0%B8%AD%E0%B8%AA%E0%B8%B4%E0%B8%99%E0%B8%84%E0%B9%89%E0%B8%B2" target="_blank" >(วิธีการสั่งซื้อสินค้าผ่านหน้าเว็บ คลิ๊ก)</a><br /><br />
   Email : admin@se-update.com <br /><br /><b><h3>ที่อยู่หน้าร้าน (ไม่เปิดรับสินค้าทางไปรษณีย์)</h3></b><br />

ชั้น 2 (ตรงข้ามธนาคารกสิกรไทย) <br />เอสพลานาด ซีนีเพล็กซ์ รัชดาภิเษก<br />
99  ถนนรัชดาภิเษก แขวงดินแดง เขตดินแดง กทม. 10400</br>
<a href="http://shop.se-update.com/SE-Update-Shop-Map" target="_blank">วิธีการเดินทางแบบละเอียด </a><br />
เปิดบริการทุกวัน 10.30-20.00 น.<br /><br /><b><h3>Line@ :@SE-Update </h3></b><br />
<b><h3>Facebook Inbox : </h3></b><a href="https://facebook.com/messages/seupdate/" target="_blank" >https://facebook.com/messages/seupdate/</a><br />
</div>
      <div class="right">
        <?php if ($telephone) { ?>
        <b><h3>ติดต่อสอบถามสินค้าออนไลน์</h3></b><br />
      089-810-3755  </br> เวลาเปิดรับสาย จ-ศ 10.00-20.00 น. , ส 10.00-12.00 น. , ยกเว้นวันอาทิตย์<br />
        <br /> <br /><br /><br />
        <b><h3>ติดต่อสอบถามสินค้าหน้าร้าน</h3></b><br />
092-540-3300
   
        <?php } ?>
        <?php if ($fax) { ?>
        <b><?php echo $text_fax; ?></b><br />
        <?php echo $fax; ?>
        <?php } ?>
      </div>
    </div>
    </div>
    <h2><?php echo $text_contact; ?></h2>
    <div class="content">
    <b><?php echo $entry_name; ?></b><br />
    <input type="text" name="name" value="<?php echo $name; ?>" />
    <br />
    <?php if ($error_name) { ?>
    <span class="error"><?php echo $error_name; ?></span>
    <?php } ?>
    <br />
    <b><?php echo $entry_email; ?></b><br />
    <input type="text" name="email" value="<?php echo $email; ?>" />
    <br />
    <?php if ($error_email) { ?>
    <span class="error"><?php echo $error_email; ?></span>
    <?php } ?>
    <br />
    <b><?php echo $entry_enquiry; ?></b><br />
    <textarea name="enquiry" style="width: 99%;" rows="10"><?php echo $enquiry; ?></textarea>
    <br />
    <?php if ($error_enquiry) { ?>
    <span class="error"><?php echo $error_enquiry; ?></span>
    <?php } ?>
    <br />
    <!--<b><?php echo $entry_captcha; ?></b><br />
    <input type="text" name="captcha" value="<?php echo $captcha; ?>" autocomplete="off" />
    <br />
    <img src="index.php?route=information/contact/captcha" />
    <?php if ($error_captcha) { ?>
    <span class="error"><?php echo $error_captcha; ?></span>
    <?php } ?>-->
	<!--new code-->
	<?php echo $google_captcha; ?>
	<!--new code-->
    </div>
    <div class="buttons">
      <div class="right"><a onclick="$('#contact').submit();" class="btn light-blue darken-1"><span><?php echo $button_continue; ?></span></a></div>
    </div>
  </form><?php echo $footer; ?>