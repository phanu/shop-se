<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
body {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
}
body, td, th, input, textarea, select, a {
	font-size: 12px;
}
p {
	margin-top: 0px;
	margin-bottom: 20px;
}
a, a:visited, a b {
	color: #378DC1;
	text-decoration: underline;
	cursor: pointer;
}
a:hover {
	text-decoration: none;
}
a img {
	border: none;
}
#container {
	width: 680px;
}
#logo {
	margin-bottom: 20px;
}
div.mydiv {
  background-color: silver;
  width: 100%;
}

table.list {
	border-collapse: collapse;
	width: 100%;
	border-top: 1px solid #DDDDDD;
	border-left: 1px solid #DDDDDD;
	margin-bottom: 20px;
}
table.list td {
	border-right: 1px solid #DDDDDD;
	border-bottom: 1px solid #DDDDDD;
}
table.list thead td {
	background-color: #EFEFEF;
	padding: 0px 5px;
}
table.list thead td a, .list thead td {
	text-decoration: none;
	color: #222222;
	font-weight: bold;
}
table.list tbody td a {
	text-decoration: underline;
}
table.list tbody td {
	vertical-align: top;
	padding: 0px 5px;
}
table.list .left {
	text-align: left;
	padding: 7px;
}
table.list .right {
	text-align: right;
	padding: 7px;
}
table.list .center {
	text-align: center;
	padding: 7px;
}

</style>
</head>
<body>
<div id="container">
  <p><br><?php echo $message; ?></p><br><br>
  <table class="list">
    <thead>
      <tr>
        <td class="left">S.No</td>
        <td class="left">Product Name</td>
        <td class="left">Manufacturer</td>
        <td class="left">Model</td>
        <td class="left">SKU</td>
      </tr>
    </thead>
    <tbody>
      <?php $sno = 1; ?>
      <?php foreach ($products as $product) { ?>
      <tr>
        <td class="left"><?php echo $sno; ?></td>
        <td class="left"><?php echo $product['pname']; ?>
          <br /> <?php if(isset($product['oname'])) { ?>
          &nbsp;<small> - <?php echo $product['oname']; ?></small> <?php } ?>
        </td>
        <td class="right"><?php echo $product['mname']; ?></td>
        <td class="left"><?php echo $product['model']; ?></td>
        <td class="right"><?php echo $product['sku']; ?></td>
      </tr>
      <?php $sno = $sno + 1; ?>
      <?php } ?>
    </tbody>
  </table>
</div>
</body>
</html>
