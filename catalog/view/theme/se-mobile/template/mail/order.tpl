<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $title; ?></title>
</head>
<body style="font-size: 12px; font-family: Arial, Helvetica, sans-serif; color: #000000;">
<div style="width: 680px;"><a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>" style="cursor: pointer; font-size: 12px; text-decoration: underline; color: #378DC1;"><img src="<?php echo $logo; ?>" alt="<?php echo $store_name; ?>" style="margin-bottom: 20px; border: none;" /></a>
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_greeting; ?></p>
  <?php if ($customer_id) { ?>
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_link; ?></p>
  <p style="margin-top: 0px; margin-bottom: 20px;"><a href="<?php echo $link; ?>" style="cursor: pointer; font-size: 12px; text-decoration: underline; color: #378DC1;"><?php echo $link; ?></a></p>
  <?php } ?>
  <?php if ($download) { ?>
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_download; ?></p>
  <p style="margin-top: 0px; margin-bottom: 20px;"><a href="<?php echo $download; ?>" style="cursor: pointer; font-size: 12px; text-decoration: underline; color: #378DC1;"><?php echo $download; ?></a></p>
  <?php } ?>
  <table style="border-collapse: collapse; width: 100%; border-top-color: #DDDDDD; border-top-style: solid; border-top-width: 1px; border-left-color: #DDDDDD; border-left-style: solid; border-left-width: 1px; margin-bottom: 20px;">
    <thead>
      <tr>
        <td align="left" bgcolor="#EFEFEF" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; font-weight: bold; background-color: #EFEFEF; padding: 7px; color: #222222; text-decoration: none; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #DDDDDD; text-align: left; border-right-color: #DDDDDD;" colspan="2"><?php echo $text_order_detail; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td align="left" valign="top" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px; padding: 7px; border-bottom-color: #DDDDDD; vertical-align: top; text-align: left; border-right-color: #DDDDDD;"><b><?php echo $text_order_id; ?></b> <?php echo $order_id; ?><br />
          <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?><br />
          <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
          <?php if ($shipping_method) { ?>
          <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>
          <?php } ?></td>
        <td align="left" valign="top" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px; padding: 7px; border-bottom-color: #DDDDDD; vertical-align: top; text-align: left; border-right-color: #DDDDDD;"><b><?php echo $text_email; ?></b> <?php echo $email; ?><br />
          <b><?php echo $text_telephone; ?></b> <?php echo $telephone; ?><br />
          <b><?php echo $text_ip; ?></b> <?php echo $ip; ?><br /></td>
      </tr>
    </tbody>
  </table>
  <?php if ($comment) { ?>
    <table style="border-collapse: collapse; width: 100%; border-top-color: #DDDDDD; border-top-style: solid; border-top-width: 1px; border-left-color: #DDDDDD; border-left-style: solid; border-left-width: 1px; margin-bottom: 20px;">
    <thead>
      <tr>
        <td align="left" bgcolor="#EFEFEF" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; font-weight: bold; background-color: #EFEFEF; padding: 7px; color: #222222; text-decoration: none; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #DDDDDD; text-align: left; border-right-color: #DDDDDD;"><?php echo $text_instruction; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td align="left" valign="top" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px; padding: 7px; border-bottom-color: #DDDDDD; vertical-align: top; text-align: left; border-right-color: #DDDDDD;"><?php echo $comment; ?></td>
      </tr>
    </tbody>
  </table>
  <?php } ?>
  <table style="border-collapse: collapse; width: 100%; border-top-color: #DDDDDD; border-top-style: solid; border-top-width: 1px; border-left-color: #DDDDDD; border-left-style: solid; border-left-width: 1px; margin-bottom: 20px;">
    <thead>
      <tr>
        <td align="left" bgcolor="#EFEFEF" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; font-weight: bold; background-color: #EFEFEF; padding: 7px; color: #222222; text-decoration: none; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #DDDDDD; text-align: left; border-right-color: #DDDDDD;"><?php echo $text_payment_address; ?></td>
        <?php if ($shipping_address) { ?>
        <td align="left" bgcolor="#EFEFEF" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; font-weight: bold; background-color: #EFEFEF; padding: 7px; color: #222222; text-decoration: none; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #DDDDDD; text-align: left; border-right-color: #DDDDDD;"><?php echo $text_shipping_address; ?></td>
        <?php } ?>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td align="left" valign="top" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px; padding: 7px; border-bottom-color: #DDDDDD; vertical-align: top; text-align: left; border-right-color: #DDDDDD;"><?php echo $payment_address; ?></td>
        <?php if ($shipping_address) { ?>
        <td align="left" valign="top" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px; padding: 7px; border-bottom-color: #DDDDDD; vertical-align: top; text-align: left; border-right-color: #DDDDDD;"><?php echo $shipping_address; ?></td>
        <?php } ?>
      </tr>
    </tbody>
  </table>
  <table style="border-collapse: collapse; width: 100%; border-top-color: #DDDDDD; border-top-style: solid; border-top-width: 1px; border-left-color: #DDDDDD; border-left-style: solid; border-left-width: 1px; margin-bottom: 20px;">
    <thead>
      <tr>
        <td align="left" bgcolor="#EFEFEF" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; font-weight: bold; background-color: #EFEFEF; padding: 7px; color: #222222; text-decoration: none; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #DDDDDD; text-align: left; border-right-color: #DDDDDD;"><?php echo $text_product; ?></td>
        <td align="left" bgcolor="#EFEFEF" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; font-weight: bold; background-color: #EFEFEF; padding: 7px; color: #222222; text-decoration: none; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #DDDDDD; text-align: left; border-right-color: #DDDDDD;"><?php echo $text_model; ?></td>
        <td align="right" bgcolor="#EFEFEF" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; font-weight: bold; background-color: #EFEFEF; padding: 7px; color: #222222; text-decoration: none; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #DDDDDD; text-align: right; border-right-color: #DDDDDD;"><?php echo $text_quantity; ?></td>
        <td align="right" bgcolor="#EFEFEF" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; font-weight: bold; background-color: #EFEFEF; padding: 7px; color: #222222; text-decoration: none; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #DDDDDD; text-align: right; border-right-color: #DDDDDD;"><?php echo $text_price; ?></td>
        <td align="right" bgcolor="#EFEFEF" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; font-weight: bold; background-color: #EFEFEF; padding: 7px; color: #222222; text-decoration: none; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #DDDDDD; text-align: right; border-right-color: #DDDDDD;"><?php echo $text_total; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($products as $product) { ?>
      <tr>
        <td align="left" valign="top" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px; padding: 7px; border-bottom-color: #DDDDDD; vertical-align: top; text-align: left; border-right-color: #DDDDDD;"><?php echo $product['name']; ?>
          <?php foreach ($product['option'] as $option) { ?>
          <br />
          &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
          <?php } ?></td>
        <td align="left" valign="top" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px; padding: 7px; border-bottom-color: #DDDDDD; vertical-align: top; text-align: left; border-right-color: #DDDDDD;"><?php echo $product['model']; ?></td>
        <td align="right" valign="top" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px; padding: 7px; border-bottom-color: #DDDDDD; vertical-align: top; text-align: right; border-right-color: #DDDDDD;"><?php echo $product['quantity']; ?></td>
        <td align="right" valign="top" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px; padding: 7px; border-bottom-color: #DDDDDD; vertical-align: top; text-align: right; border-right-color: #DDDDDD;"><?php echo $product['price']; ?></td>
        <td align="right" valign="top" style="font-size: 12px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px; padding: 7px; border-bottom-color: #DDDDDD; vertical-align: top; text-align: right; border-right-color: #DDDDDD;"><?php echo $product['total']; ?></td>
      </tr>
      <?php } ?>
    </tbody>
    <tfoot>
      <?php foreach ($totals as $total) { ?>
       <?php if ($total['title'] == "จัดส่งด่วน Delivery") { $total['text'] = "ค่าจัดส่งตามระยะทาง"; } ?>
      <tr>
        <td align="right" style="padding: 7px; border-right-width: 1px; border-right-style: solid; font-size: 12px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #DDDDDD; text-align: right; border-right-color: #DDDDDD;" colspan="4"><b><?php echo $total['title']; ?>:</b></td>
        <td align="right" style="padding: 7px; border-right-width: 1px; border-right-style: solid; font-size: 12px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #DDDDDD; text-align: right; border-right-color: #DDDDDD;"><?php echo $total['text']; ?></td>
      </tr>
      <?php } ?>
    </tfoot>
  </table>
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_footer; ?></p>
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_powered; ?></p>
</div>
</body>
</html>
