<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
    <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
    <h1><?php echo $heading_title; ?></h1>
<div class="checkout-product">
  <table class="list">
    <thead>
      <tr>
        <td class="left" colspan="2"><?php echo $text_order_detail; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="left"><b><?php echo $text_order_id; ?></b> <?php echo $order_id; ?><br />
          <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?><br />
          <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
          <?php if ($shipping_method) { ?>
          <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>
          <?php } ?></td>
        <td class="left"><b><?php echo $text_email; ?></b> <?php echo $email; ?><br />
          <b><?php echo $text_telephone; ?></b> <?php echo $telephone; ?><br />
          <b><?php echo $text_ip; ?></b> <?php echo $ip; ?><br /></td>
      </tr>
    </tbody>
  </table>
  <?php if ($comment) { ?>
    <table class="list">
    <thead>
      <tr>
        <td class="left"><?php echo $text_instruction; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="left"><?php echo $comment; ?></td>
      </tr>
    </tbody>
  </table>
  <?php } ?>
  <table class="list">
    <thead>
      <tr>
        <td class="left">&nbsp;</td>
        <td class="left"><?php echo $text_payment_address; ?></td>
        <td class="left"><?php echo $text_shipping_address; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="left"><?php echo $entry_company; ?></td>
        <?php if ($payment_company) { ?>
        <td class="left"><?php echo $payment_company; ?></td>
        <?php } else { ?>
        <td class="left">&nbsp;</td>
        <?php } ?>
        <?php if ($shipping_company) { ?>
        <td class="left"><?php echo $shipping_company; ?></td>
        <?php } else { ?>
        <td class="left">&nbsp;</td>
        <?php } ?>
      </tr>
      <tr>
        <td class="left"><?php echo $entry_firstname; ?></td>
        <?php if ($payment_firstname) { ?>
        <td class="left"><?php echo $payment_firstname; ?></td>
        <?php } else { ?>
        <td class="left">&nbsp;</td>
        <?php } ?>
        <?php if ($shipping_firstname) { ?>
        <td class="left"><?php echo $shipping_firstname; ?></td>
        <?php } else { ?>
        <td class="left">&nbsp;</td>
        <?php } ?>
      </tr>
      <tr>
        <td class="left"><?php echo $entry_lastname; ?></td>
        <?php if ($payment_lastname) { ?>
        <td class="left"><?php echo $payment_lastname; ?></td>
        <?php } else { ?>
        <td class="left">&nbsp;</td>
        <?php } ?>
        <?php if ($shipping_lastname) { ?>
        <td class="left"><?php echo $shipping_lastname; ?></td>
        <?php } else { ?>
        <td class="left">&nbsp;</td>
        <?php } ?>
      </tr>
      <tr>
        <td class="left"><?php echo $entry_country; ?></td>
        <?php if ($payment_country) { ?>
        <td class="left"><?php echo $payment_country; ?></td>
        <?php } else { ?>
        <td class="left">&nbsp;</td>
        <?php } ?>
        <?php if ($shipping_country) { ?>
        <td class="left"><?php echo $shipping_country; ?></td>
        <?php } else { ?>
        <td class="left">&nbsp;</td>
        <?php } ?>
      </tr>
      <tr>
        <td class="left"><?php echo $entry_zone; ?></td>
        <?php if ($payment_zone) { ?>
        <td class="left"><?php echo $payment_zone; ?></td>
        <?php } else { ?>
        <td class="left">&nbsp;</td>
        <?php } ?>
        <?php if ($shipping_zone) { ?>
        <td class="left"><?php echo $shipping_zone; ?></td>
        <?php } else { ?>
        <td class="left">&nbsp;</td>
        <?php } ?>
      </tr>
      <tr>
        <td class="left"><?php echo $entry_address; ?></td>
        <?php if ($payment_address) { ?>
        <td class="left"><?php echo $payment_address; ?></td>
        <?php } else { ?>
        <td class="left">&nbsp;</td>
        <?php } ?>
        <?php if ($shipping_address) { ?>
        <td class="left"><?php echo $shipping_address; ?></td>
        <?php } else { ?>
        <td class="left">&nbsp;</td>
        <?php } ?>
      </tr>
      <tr>
        <td class="left"><?php echo $entry_postcode; ?></td>
        <?php if ($payment_postcode) { ?>
        <td class="left"><?php echo $payment_postcode; ?></td>
        <?php } else { ?>
        <td class="left">&nbsp;</td>
        <?php } ?>
        <?php if ($shipping_postcode) { ?>
        <td class="left"><?php echo $shipping_postcode; ?></td>
        <?php } else { ?>
        <td class="left">&nbsp;</td>
        <?php } ?>
      </tr>
      <tr>
        <td class="left"><?php echo $entry_city; ?></td>
        <?php if ($payment_city) { ?>
        <td class="left"><?php echo $payment_city; ?></td>
        <?php } else { ?>
        <td class="left">&nbsp;</td>
        <?php } ?>
        <?php if ($shipping_city) { ?>
        <td class="left"><?php echo $shipping_city; ?></td>
        <?php } else { ?>
        <td class="left">&nbsp;</td>
        <?php } ?>
      </tr>
    </tbody>
  </table>
  <table>
    <thead>
      <tr>
        <td class="name"><?php echo $column_name; ?></td>
        <td class="model"><?php echo $column_model; ?></td>
        <td class="quantity"><?php echo $column_quantity; ?></td>
        <td class="price"><?php echo $column_price; ?></td>
        <td class="total"><?php echo $column_total; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($products as $product) { ?>
      <tr>
        <td class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
          <?php foreach ($product['option'] as $option) { ?>
          <br />
          &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
          <?php } ?></td>
        <td class="model"><?php echo $product['model']; ?></td>
        <td class="quantity"><?php echo $product['quantity']; ?></td>
        <td class="price"><?php echo $product['price']; ?></td>
        <td class="total"><?php echo $product['total']; ?></td>
      </tr>
      <?php } ?>
      <?php foreach ($vouchers as $voucher) { ?>
      <tr>
        <td class="name"><?php echo $voucher['description']; ?></td>
        <td class="model"></td>
        <td class="quantity">1</td>
        <td class="price"><?php echo $voucher['amount']; ?></td>
        <td class="total"><?php echo $voucher['amount']; ?></td>
      </tr>
      <?php } ?>
    </tbody>
    <tfoot>
      <?php foreach ($totals as $total) { ?>
      <tr>
        <td colspan="4" class="price"><b><?php echo $total['title']; ?>:</b></td>
        <td class="total"><?php echo $total['text']; ?></td>
      </tr>
      <?php } ?>
    </tfoot>
  </table>
</div>
<div class="payment"><?php echo $payment; ?></div>
<?php //echo $content_bottom; ?></div>
<?php echo $footer; ?>