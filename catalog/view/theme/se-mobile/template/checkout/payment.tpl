<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($payment_methods) { ?>
<table class="form">
<tr>
<p><h4><?php echo $text_payment_method1; ?></h4></p>
</tr>
  <?php foreach ($payment_methods as $payment_method) { ?>
  <tr>
   <?php if ($payment_method['code'] == $code || !$code) { ?>
    
      <?php $code = $payment_method['code']; ?>

     <td style="width: 1px;">   <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" checked="checked" /></td>
       <?php } elseif ($payment_method['code']=="paysbuy") { ?>
<tr><td COLSPAN="2">
<p><h3><?php echo $text_payment_method2; ?></h3></p>
</td></tr>
 <td style="width: 1px;"> <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" /></td>
      <?php } else { ?>
    <td style="width: 1px;"> <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" /></td>
      <?php } ?>
    <td><label for="<?php echo $payment_method['code']; ?>"><?php echo $payment_method['title']; ?></label></td>
  </tr>
  <?php } ?>
</table>
<?php } ?>
<b><?php echo $text_comments; ?></b>
<textarea name="comment" rows="8" style="width: 98%;"><?php echo $comment; ?></textarea>
<br />

<br />
<?php if ($text_agree) { ?>
<div class="buttons">
  <div class="left"><?php echo $text_agree; ?>
    <?php if ($agree) { ?>
    <input type="checkbox" name="agree" value="1" checked="checked" />
    <?php } else { ?>
    <input type="checkbox" name="agree" value="1" />
    <?php } ?>
    <a id="button-payment" class="btn light-blue darken-1"><span><?php echo $button_continue; ?></span></a></div>
</div>
<?php } else { ?>
<div class="buttons">
  <div class="center"><a id="button-payment" class="btn light-blue darken-1"><span><?php echo $button_continue; ?></span></a></div>
</div>
<?php } ?>
<script type="text/javascript"><!--
$('.fancybox').fancybox({
	width: 560,
	height: 560,
	autoDimensions: false
});
//--></script>  