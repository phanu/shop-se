<?php echo $header; ?>

      <div class="breadcrumbs animated fadeIn">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>

      <h2 class="product-title animated fadeIn"><?php echo $heading_title; ?></h2>
  <h1><?php echo $heading_title; ?></h1>
  <?php echo $description; ?>
  <div class="buttons">
    <?php if (isset($cart)) { ?>
        <div class="right"><a href="<?php echo $clear_cart; ?>" class="button"><span><?php echo $button_clear_cart; ?></span></a></div>
        <div class="right"><a href="<?php echo $cart; ?>" class="button"><span><?php echo $button_cart; ?></span></a></div>
    <?php } else { ?>
        <div class="right"><a href="<?php echo $continue; ?>" class="btn light-blue darken-1"><span><?php echo $button_continue; ?></span></a></div>
    <?php } ?>
  </div>
<?php echo $footer; ?>