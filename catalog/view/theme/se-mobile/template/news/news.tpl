  <?php echo $header; ?>
  <style>
  /* menu */
.menu {
height: 33px;
border: 1px solid #DBDEE1;
border-top-width:0px;
background: url('catalog/view/theme/default/image/background.png') repeat-x;
padding: 0px 10px 0px 10px;
font-family: Arial, Helvetica, sans-serif;
font-size: 12px;
font-weight: bold;
line-height: 14px;
color: #333;

}
.menu ul {
	list-style: none;
	margin: 0;
	padding: 0;
}
.menu > ul > li {
min-width:120px;
height: 20px;
	position: relative;
	float: left;
border-right: 1px solid #DBDEE1;
	z-index: 20;
	padding: 7px 5px 6px 0px;
}
.menu > ul > li:hover {
}
.menu > ul > li > a {
	font-size: 12px;
	color: #333;
	line-height: 14px;
	text-decoration: none;
	display: block;
	padding: 3px 10px 9px 10px;
	margin-bottom: 0px;
	z-index: 6;
	position: relative;
}
.menu > ul > li:hover > a {
	color: #000;
}
.menu > ul > li > div {
	display: none;
	background: #FFFFFF;
	position: absolute;
	z-index: 5;
	padding: 5px;
	border: 1px solid #DBDEE1;
	-webkit-border-radius: 0px 0px 5px 5px;
	-moz-border-radius: 0px 0px 5px 5px;
	-khtml-border-radius: 0px 0px 5px 5px;
	border-radius: 0px 0px 5px 5px;
	background: url('catalog/view/theme/default/image/background.png');
}
.menu > ul > li:hover > div {
	display: table;
}
.menu > ul > li > div > ul {
	display: table-cell;
}
.menu > ul > li ul + ul {
	padding-left: 20px;
}
.menu > ul > li ul > li > a {
	text-decoration: none;
	padding: 4px;
	color: #333;
	display: block;
	white-space: nowrap;
	min-width: 120px;
}
.menu > ul > li ul > li > a:hover {
	color: #38b0e3;
}
.menu > ul > li > div > ul > li > a {
	color: #333;
}
 
  </style>
  <?php echo $column_left; ?>
  <?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <!-- --> 
  <?php foreach ($categories as $category_1) { ?> 
  <div class="box">      
  <div class="box-heading" id="news_heading<?php echo $category_1['article_category_id']; ?>"><a class="mparent" href="<?php echo $category_1['href']; ?>"><?php echo $category_1['name']; ?></a></div>
        <div class="menu" id="news_tab<?php echo $category_1['article_category_id']; ?>">
        <ul> 
        <?php foreach ($category_1['children'] as $category_2) { ?>
        <li><a href="<?php echo $category_2['href']; ?>"><?php echo $category_2['name']; ?></a>
        <?php if($category_2['children']){?>
        <div>
        <ul>         
        <?php foreach ($category_2['children'] as $category_3) { ?>
         <li> <a href="<?php echo $category_3['href']; ?>"><?php echo $category_3['name']; ?></a></li>        
        <?php } ?>
        </ul></div>
        <?php } ?>
        </li>
        <?php } ?>       
        </ul>
        </div>
   <div class="box-content">
   <!--box content --> 
    <div class="product-list" id="news_content<?php echo $category_1['article_category_id']; ?>">
    <?php if($articles){?>
      <?php foreach ($articles as $article) { ?>
          <div>  
        <?php if ($article['thumb']) { ?> <div class="image"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" /></a></div>
<?php } ?>
	
	<div class="name"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>
	
	<div class="description">
    
    <div style="font-size:11px;"><?php echo $text_date_added;?> <i><?php echo $article['date_added']; ?></i>
     <?php echo $text_author;?> <a href="<?php echo $article['author_href']; ?>" style="text-decoration:none; font-size:11px;"><?php echo $article['author']; ?></a>
     
      <?php echo $article['comments']; ?>
      <?php echo $article['viewed']; ?>
     </div>    
    <?php echo $article['description']; ?>    
    <a style="float:right; margin:5px 20px 0 0;" href="<?php echo $article['href']; ?>" class="button"><span><?php echo $text_more; ?></span></a>
   
    
    </div>
    <!--related --> 
        <?php if($article['related']){?>
        <span style="color:#999;font-size:11px;"><?php echo $text_related;?></span>
        <ul style="margin-left:<?php echo $width;?>px; margin-top:0px;"> 
        <?php foreach ($article['related'] as $relate) { ?>
                  <li><a href="<?php echo $relate['href'];?>" style="font-size:11px;  text-decoration:none;"><?php echo $relate['name']; ?></a></li> 
        <?php } ?>
        </ul>
        <?php } ?>    
    <!--end related --> 
	</div> 
      <?php } ?>
      <?php } ?>
    </div>    
    
  <?php if (!$articles) { ?>
  <div class="nocontent"><?php echo $text_empty; ?></div>
  <?php } ?>
   
   <!--end box content --> 
   
   </div>
   </div>
<script type="text/javascript"><!--
$('#news_heading<?php echo $category_1['article_category_id']; ?> a').live('click', function() {
	$('#news_tab<?php echo $category_1['article_category_id']; ?> a').removeClass("selected");
	$('#news_content<?php echo $category_1['article_category_id']; ?>').load(this.href);	
	return false;
});		
$('#news_tab<?php echo $category_1['article_category_id']; ?> a').live('click', function() {
$('#news_tab<?php echo $category_1['article_category_id']; ?> a').removeClass("selected");
 	$(this).addClass("selected");	
	$('#news_content<?php echo $category_1['article_category_id']; ?>').load(this.href);	
	return false;
});		
$('#news_content<?php echo $category_1['article_category_id']; ?> .pagination a').live('click', function() {
	$('#news_content<?php echo $category_1['article_category_id']; ?>').load(this.href);	
	return false;
});	
//--></script>
<?php } ?>
<!-- --> 
 <?php //echo $content_bottom; ?></div>
 <?php echo $footer; ?>