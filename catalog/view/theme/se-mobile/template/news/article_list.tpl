<?php echo $header; ?>
<style>
.ui-datepicker-trigger{display:none;}
.buttons a.button{ float:right;margin-right:10px;}
.list {
	border-collapse: collapse;
	width: 100%;
	border-top: 1px solid #DDDDDD;
	border-left: 1px solid #DDDDDD;
	margin-bottom: 20px;
}
.list td {
	border-right: 1px solid #DDDDDD;
	border-bottom: 1px solid #DDDDDD;
}
.list thead td {
	background-color: #EFEFEF;
	padding: 0px 5px;
}
.list thead td a, .list thead td {
	text-decoration: none;
	color: #222222;
	font-weight: bold;
}
.list tbody td a {
	text-decoration: underline;
}
.list tbody td {
	vertical-align: middle;
	padding: 0px 5px;
	background: #FFFFFF;
}
.list .left {
	text-align: left;
	padding: 7px;
}
.list .right {
	text-align: right;
	padding: 7px;
}
.list .center {
	text-align: center;
	padding: 7px;
}
.list .asc {
	padding-right: 15px;
	background: url('catalog/view/javascript/news/images/asc.png') right center no-repeat;
}
.list .desc {
	padding-right: 15px;
	background: url('catalog/view/javascript/news/images/desc.png') right center no-repeat;
}
.list tr.filter td {
	padding: 5px;
	background: #E7EFEF;
}
.pagination {
border-top: 1px solid #DDDDDD;
display: inline-block;
width: 100%;
padding-top: 6px;
}
</style>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="catalog/view/javascript/news/images/article.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
      <a onclick="$('form').submit();" class="button"><span><?php echo $button_delete; ?></span></a>
      <a onclick="location = '<?php echo $insert; ?>'" class="button"><span><?php echo $button_insert; ?></span></a>
      </div>
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td width="1"><?php echo $column_image; ?></td>
              <td class="aleft"><?php if ($sort == 'pd.name') { ?>
                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                <?php } ?></td>
                 <td class="aleft"><?php echo $column_category; ?></td>
               
              <td class="aleft"><?php if ($sort == 'p.status') { ?>
                <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                <?php } ?></td>
              <td class="aleft"><?php if ($sort == 'p.approved') { ?>
                <a href="<?php echo $sort_approved; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_approved; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_approved; ?>"><?php echo $column_approved; ?></a>
                <?php } ?></td>
                
                <td class="aleft" width="1"><?php if($sort=='p.date_added'){?>
                <a href="<?php echo $sort_date;?>" class="<?php echo strtolower($order);?>">Date Added</a>
                <?php }else{?>
                <a href="<?php echo $sort_date;?>">Date Added</a>
                <?php }?></td>
              <td class="aright"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td></td>
              <td></td>
              <td><input type="text" name="filter_article" value="<?php echo $filter_article; ?>" /></td><td>
              <select name="filter_article_category"  onchange="filter()">
              <option value=""></option>
              <?php foreach ($categories as $category){?>
                <?php if ($category['article_category_id']==$filter_article_category){?>
                  <option value="<?php echo $category['article_category_id'];?>" selected="selected"><?php echo $category['name'];?> <?php echo $category['total'];?></option>
                <?php } else{?>
                  <option value="<?php echo $category['article_category_id'];?>"><?php echo $category['name'];?> <?php echo $category['total'];?></option> 
                <?php }?>
              <?php }?>
              </select>
            </td>
        
               <td align="left"><select name="filter_status" onchange="filter()">
                  <option value="*"></option>
                  <?php if ($filter_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <?php } ?>
                  <?php if (!is_null($filter_status) && !$filter_status) { ?>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
                
               <td align="left"><select name="filter_approved" onchange="filter()">
                  <option value="*"></option>
                  <?php if ($filter_approved) { ?>
                  <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_yes; ?></option>
                  <?php } ?>
                  <?php if (!is_null($filter_approved) && !$filter_approved) { ?>
                  <option value="0" selected="selected"><?php echo $text_no; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_no; ?></option>
                  <?php } ?>
                </select></td>
              <td align="left"><input type="text" name="filter_date" value="<?php echo $filter_date;?>"  class="date" style="width:70px"  onchange="filter()" onclick="this.value = '';"/></td>
              <td align="right"><a onclick="filter();" class="button"><span><?php echo $button_filter; ?></span></a></td>
            </tr>
            <?php if ($articles) { ?>
            <?php foreach ($articles as $article) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($article['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $article['article_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $article['article_id']; ?>" />
                <?php } ?></td>
              <td><img src="<?php echo $article['image']; ?>" alt="<?php echo $article['name']; ?>" style="padding: 1px; margin:3px 0; border: 1px solid #DDDDDD;" /></td>
              <td class="aleft"><?php echo $article['name']; ?></td>
               <td><?php foreach ($categories as $category){?>
                <?php if (in_array($category['article_category_id'], $article['category'])){?>
                <?php echo $category['name'];?><br>
                <?php }?> <?php }?>
				</td>              
              <td class="aleft"><?php echo $article['status']; ?></td>
              <td class="aleft"><?php echo $article['approved']; ?></td>
              <td class="aleft"><?php echo $article['date'];?></td>
              <td class="aright">
              	<?php foreach ($article['action'] as $action) { ?>
                [ <a href="<?php echo $action['href']; ?>" target="<?php echo $action['target']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?>
                </td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="8"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=news/manager';	
	var filter_article = $('input[name=\'filter_article\']').attr('value');	
	if (filter_article) {
		url += '&filter_article=' + encodeURIComponent(filter_article);
	}	
	/*filter_article_category*/
	var filter_article_category = $('select[name=\'filter_article_category\']').attr('value');
	if (filter_article_category) {url += '&filter_article_category=' + encodeURIComponent(filter_article_category);	}
	/*date*/
	var filter_date = $('input[name=\'filter_date\']').attr('value');
	if (filter_date) {url += '&filter_date=' + encodeURIComponent(filter_date);}
	
	var filter_status = $('select[name=\'filter_status\']').attr('value');
	
	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}	

	var filter_approved = $('select[name=\'filter_approved\']').attr('value');
	
	if (filter_approved != '*') {
		url += '&filter_approved=' + encodeURIComponent(filter_approved);
	}	
	location = url;
}
//--></script> 
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_article\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=news/manager/autocomplete&filter_article=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.article_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_article\']').val(ui.item.label);
						
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});
//--></script> 
<script type="text/javascript"><!--
$('.date').datepicker({dateFormat: 'yy-mm-dd'});
//--></script> 
<?php echo $footer; ?>