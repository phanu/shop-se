<?php echo $header; ?>

      <div class="breadcrumbs animated fadeIn">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
      </div>

      <h2 class="product-title animated fadeIn"><?php echo $heading_title; ?></h2>
  <h1><?php echo $heading_title; ?></h1>
  <?php if ($categories) { ?>
  <p><b><?php echo $text_index; ?></b>
    <?php foreach ($categories as $category) { ?>
    &nbsp;&nbsp;&nbsp;<a href="index.php?route=news/author#<?php echo $category['name']; ?>"><b><?php echo $category['name']; ?></b></a>
    <?php } ?>
  </p>
  <div class="buttons" align="right">      
      <a href="<?php echo $register_author; ?>" class="button"><span><?php echo $text_register_author; ?></span></a>
      <a href="<?php echo $login; ?>" class="button"><span><?php echo $text_login; ?></span></a>
      </div>
  <?php foreach ($categories as $category) { ?>
  <div class="manufacturer-list"><a id="<?php echo $category['name']; ?>"></a>
    <div class="manufacturer-heading"><?php echo $category['name']; ?></div>
    <div class="manufacturer-content">
      <?php if ($category['author']) { ?>
      <?php for ($i = 0; $i < count($category['author']);) { ?>
      <ul style="width:96%;">
        <?php $j = $i + ceil(count($category['author']) / 1); ?>
        <?php for (; $i < $j; $i++) { ?>
        <?php if (isset($category['author'][$i])) { ?>
        <li style="width:120px; float:left" align="center"><div style="margin-bottom:10px;"><a href="<?php echo $category['author'][$i]['href']; ?>" style="text-decoration:none;">
        <img src="<?php echo $category['author'][$i]['image']; ?>" alt="<?php echo $category['author'][$i]['author']; ?>" style="padding: 3px; border: 1px solid #E7E7E7;"/>
        <br> 
        <?php echo $category['author'][$i]['author']; ?>
        </a><br> 
        (<?php echo $category['author'][$i]['total']; ?> <?php echo $text_article;?>) </div>
        </li>
        <?php } ?>
        <?php } ?>
      </ul>
      <?php } ?>
      <?php } ?>
    </div>
    <div style="clear:both"></div>
  </div>
  <?php } ?>
  <?php } else { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="btn light-blue darken-1"><span><?php echo $button_continue; ?></span></a></div>
  </div>
  <?php } ?><?php echo $footer; ?>