<?php echo $header; ?>
<style>
    .description .info{padding-bottom:5px; }
  .description .info *{ text-align:right;font-size:11px; vertical-align: middle;text-decoration:none; }
  .description .info span{margin-right:5px;}
  .description .info span.date{ background:url(catalog/view/theme/default/image/news/date.png) left bottom no-repeat; padding-left:15px;}
  .description .info span.author{ background:url(catalog/view/theme/default/image/news/author.png) left bottom no-repeat; padding-left:15px;}
  .description .info span.comment{ background:url(catalog/view/theme/default/image/news/comment.png) left bottom no-repeat; padding-left:15px;}
  .description .info span.viewed{ background:url(catalog/view/theme/default/image/news/viewed.png) left bottom no-repeat; padding-left:15px;}
  
   #content .product-grid .name{ text-align:center;min-height:30px;}
   #content .product-grid .rating{ display:none;}
   #content .product-list .rating{ float:right;}
   #content .product-list .related ul{margin-left:<?php echo $width;?>px;}
   
  </style>
<?php echo $column_left; ?>
<?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <?php if ($thumb || $description) { ?>
  <div class="category-info">
    <?php if ($thumb) { ?>
    <div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></div>
    <?php } ?>
    <?php if ($description) { ?>
    <?php echo $description; ?>
    <?php } ?>
  </div>
  <?php } ?>
  <?php if ($categories) { ?>
  <h2><?php echo $text_refine; ?></h2>
  <div class="category-list" style="overflow:hidden">
    <ul style="list-style:none; width:96%;">
      <?php foreach ($categories as $category) { ?>
      <li style="float:left; text-align:center; min-width:<?php echo $category_width+10;?>px; margin:10px 0;"><a style="text-decoration:none; font-size:14px;" href="<?php echo $category['href']; ?>"><img src="<?php echo $category['image']; ?>" alt="" style="border: 1px solid #E7E7E7;" /><br/><?php echo $category['name']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <?php } ?>
  
  <?php if ($articles) { ?>
  <div class="product-filter" style="margin-bottom:10px;">
    <div class="display"><b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display('grid');"><?php echo $text_grid; ?></a></div>
    <div class="limit"><b><?php echo $text_limit; ?></b>
      <select onchange="location = this.value;">
        <?php foreach ($limits as $limits) { ?>
        <?php if ($limits['value'] == $limit) { ?>
        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
    <div class="sort"><b><?php echo $text_sort; ?></b>
      <select onchange="location = this.value;">
        <?php foreach ($sorts as $sorts) { ?>
        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
  </div>
  <div class="product-list">
    <?php foreach ($articles as $article) { ?>
    <div style="min-width:<?php echo $width;?>px">
      <?php if ($article['thumb']) { ?>
      <div class="image"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" title="<?php echo $article['name']; ?>" alt="<?php echo $article['name']; ?>" /></a></div>
      <?php } ?>
      <div class="name"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>
      <?php if ($article['rating']) { ?>
      <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $article['rating']; ?>.png" alt="<?php echo $article['comments']; ?>" /></div>
      <?php } ?>
      
	<div class="description">
    
    <div class="info">
    <span class="author"><a href="<?php echo $article['author_href']; ?>" title="<?php echo $text_author;?> <?php echo $article['author']; ?>"><?php echo $article['author']; ?></a></span>
    <span class="date"><i><?php echo $article['date_added']; ?></i></span>
    <span class="comment"><?php echo $article['comments']; ?></span>
    <span class="viewed"><?php echo $article['viewed']; ?></span>
     </div>    
    <?php echo $article['description']; ?>    
    <a style="float:right; margin:5px 20px 0 0;" href="<?php echo $article['href']; ?>" class="button"><span><?php echo $text_more; ?></span></a> 
    
     <!--related --> 
        <?php if($article['related']){?>
     <div class="related">  
        <span style="color:#999;font-size:11px;"><?php echo $text_related;?></span>
        <ul> 
        <?php foreach ($article['related'] as $relate) { ?>
                  <li><a href="<?php echo $relate['href'];?>" style="font-size:11px;  text-decoration:none;"><?php echo $relate['name']; ?></a></li> 
        <?php } ?>
        </ul> 
    </div>
        <?php } ?>   
    <!--end related -->  
    </div>
    </div>
    <?php } ?>
  </div>
  <div class="pagination"><?php echo $pagination; ?></div>
  <?php } ?>
  <?php if (!$categories && !$articles) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="btn light-blue darken-1"><span><?php echo $button_continue; ?></span></a></div>
  </div>
  <?php } ?>
  <?php //echo $content_bottom; ?></div>
<script type="text/javascript"><!--
function display(view) {
	if (view == 'list') {
		$('#content .product-grid').attr('class', 'product-list');
		
		$('#content .product-list > div').each(function(index, element) {
			html = '<div class="left">';
			
			var image = $(element).find('.image').html();
			
			if (image != null) { 
				html += '<div class="image">' + image + '</div>';
			}
			
			html += '  <div class="name">' + $(element).find('.name').html() + '</div>';
					
			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="rating" align="right">' + rating + '</div>';
			}
			html += '  <div class="description">' + $(element).find('.description').html() + '</div>';
			html += '</div>';

						
			$(element).html(html);
		});		
		
		$('.display').html('<b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display(\'grid\');"><?php echo $text_grid; ?></a>');
		
		$.cookie('display', 'list'); 
	} else {
		$('#content .product-list').attr('class', 'product-grid');
		
		$('#content .product-grid > div').each(function(index, element) {
			html = '';
			
			var image = $(element).find('.image').html();
			
			if (image != null) {
				html += '<div class="image">' + image + '</div>';
			}
			
			html += '<div class="name">' + $(element).find('.name').html() + '</div>';
			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="rating" align="center">' + rating + '</div>';
			}
			html += '<div class="description">' + $(element).find('.description').html() + '</div>';
							
			$(element).html(html);
		});	
					
		$('.display').html('<b><?php echo $text_display; ?></b> <a onclick="display(\'list\');"><?php echo $text_list; ?></a> <b>/</b> <?php echo $text_grid; ?>');
		
		$.cookie('display', 'grid');
	}
}

view = $.cookie('display');

if (view) {
	display(view);
} else {
	display('list');
}
//--></script> 
<?php echo $footer; ?>