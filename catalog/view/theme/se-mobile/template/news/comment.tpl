<?php if ($comments) { ?>
<?php foreach ($comments as $comment) { ?>
<div class="review-list">
  <div class="author"><b><?php echo $comment['author']; ?></b> <?php echo $text_on; ?> <?php echo $comment['date_added']; ?></div>
  <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $comment['rating'] . '.png'; ?>" alt="<?php echo $comment['comments']; ?>" /></div>
  <div class="text"><?php echo $comment['text']; ?></div>
</div>
<?php } ?>
<div class="pagination"><?php echo $pagination; ?></div>
<?php } else { ?>
<div class="content"><?php echo $text_no_comments; ?></div>
<?php } ?>
