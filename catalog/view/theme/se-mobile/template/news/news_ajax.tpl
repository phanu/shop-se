<?php if($articles){?>
  <?php foreach ($articles as $article) { ?>
          <div>  
        <?php if ($article['thumb']) { ?> <div class="image"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" /></a></div>
<?php } ?>
	
	<div class="name"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>
	
	<div class="description">
    
    <div style="font-size:11px;"><?php echo $text_date_added;?> <i><?php echo $article['date_added']; ?></i> 
    <?php echo $text_author;?> <a href="<?php echo $article['author_href']; ?>" style="text-decoration:none; font-size:11px;"><?php echo $article['author']; ?></a>
    <?php echo $article['comments']; ?> 
      <?php echo $article['viewed']; ?></div>    
     <?php echo $article['description']; ?>    
    <a style="float:right; margin:5px 20px 0 0;" href="<?php echo $article['href']; ?>" class="button"><span><?php echo $text_more; ?></span></a>
   
    
    </div>
    <!--related --> 
        <?php if($article['related']){?>
        <span style="color:#999;font-size:11px;"><?php echo $text_related;?></span>
        <ul style="margin-left:<?php echo $width;?>px; margin-top:0px;"> 
        <?php foreach ($article['related'] as $relate) { ?>
                  <li><a href="<?php echo $relate['href'];?>" style="font-size:11px;  text-decoration:none;"><?php echo $relate['name']; ?></a></li> 
        <?php } ?>
        </ul>
        <?php } ?>    
    <!--end related --> 
	</div> 
      <?php } ?>
      
<div class="pagination" style=" width:97%;clear:both"><?php echo $pagination; ?></div>

 <?php } ?>