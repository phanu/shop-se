<?php echo $header; ?>
<style>
table.form > tbody > tr > td {
padding: 10px;
color: #000000;
border-bottom: 1px dotted #CCCCCC;
}
.scrollbox {
	border: 1px solid #CCCCCC;
	width: 350px;
	height: 100px;
	background: #FFFFFF;
	overflow-y: scroll;
}
.scrollbox img {
	float: right;
	cursor: pointer;
}
.scrollbox div {
	padding: 3px;
}
.scrollbox div input {
	margin: 0px;
	padding: 0px;
	margin-right: 3px;
}
.scrollbox div.even {
	background: #FFFFFF;
}
.scrollbox div.odd {
	background: #E4EEF7;
}
.buttons a.button{ float:right;margin-right:10px;}
</style>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="catalog/view/javascript/news/images/article.png" alt="" /> <?php echo $heading_title; ?></h1>    
    <div class="buttons" align="right">
      <a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a>
      <a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a>
      </div>
    </div>
    <div class="content">
      <div id="tabs" class="htabs">
      <a href="#tab-general"><?php echo $tab_general; ?></a>
      <a href="#tab-links"><?php echo $tab_links; ?></a>
      </div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
        <div id="tab-general">
         <table class="form">
         <tbody>
            <tr>
              <td><?php echo $entry_image; ?></td>
              <td><div class="image"><img src="<?php echo $thumb; ?>" alt="" id="thumb" /><br />
                  <input type="hidden" name="image" value="<?php echo $image; ?>" id="image" />
                  <a id="upload_image">Browse</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                  <a onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
            </tr>
           
            <tr>
              <td><?php echo $entry_status; ?></td>
              <td><select name="status">
                  <?php if ($status) { ?>
                  <option value="1" selected="selected"><?php echo $text_publish; ?></option>
                  <option value="0"><?php echo $text_unpublish; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_publish; ?></option>
                  <option value="0" selected="selected"><?php echo $text_unpublish; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
            
            <tr>
              <td><?php echo $entry_keyword; ?></td>
              <td><input type="text" name="keyword" id="keyword" value="<?php echo $keyword; ?>" size="50"/><a style="margin-left:10px;" onclick="geturl('title','keyword','.html');" class="button"><span>Create</span></a></td>

            </tr>
            <tr>
              <td><?php echo $entry_reference_url; ?></td>
              <td><?php echo $reference_url; ?><input type="hidden" type="text" name="reference_url"  value="<?php echo $reference_url; ?>"/></td>

            </tr>       
            <tr>
              <td><span class="required">*</span> <?php echo $entry_reference_title; ?></td>
              <td><input type="text" name="reference_title"  value="<?php echo $reference_title; ?>" size="80"/></td>
            </tr>
            </tbody>
            </table>
          <div id="languages" class="htabs">
            <?php foreach ($languages as $language) { ?>
            <a href="#language<?php echo $language['language_id']; ?>"><img src="image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
            <?php } ?>
          </div>
          <?php foreach ($languages as $language) { ?>
          <div id="language<?php echo $language['language_id']; ?>">
            <table class="form">
              <tr>
                <td><span class="required">*</span> <?php echo $entry_name; ?></td>
                <td><input onkeyup="countChar(this,'title<?php echo $language['language_id']; ?>')" id="title" type="text" name="article_description[<?php echo $language['language_id']; ?>][name]" size="80" value="<?php echo isset($article_description[$language['language_id']]) ? $article_description[$language['language_id']]['name'] : ''; ?>"/>
                  <?php if (isset($error_name[$language['language_id']])) { ?>
                  <span class="error"><?php echo $error_name[$language['language_id']]; ?></span>
                  <?php }else{?><div class="error" id="num_title<?php echo $language['language_id']; ?>"></div><?php }?></td>
              </tr>
              
              <tr>
                <td><?php echo $entry_meta_keyword; ?></td>
                <td><textarea name="article_description[<?php echo $language['language_id']; ?>][meta_keyword]" cols="40" rows="5"><?php echo isset($article_description[$language['language_id']]) ? $article_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea></td>
              </tr>         
              <tr>
                <td><?php echo $entry_meta_description; ?></td>
                <td><textarea name="article_description[<?php echo $language['language_id']; ?>][meta_description]" cols="40" rows="5"><?php echo isset($article_description[$language['language_id']]) ? $article_description[$language['language_id']]['meta_description'] : ''; ?></textarea></td>
              </tr>
              <tr>
                <td><?php echo $entry_description; ?></td>
                <td><textarea  name="article_description[<?php echo $language['language_id']; ?>][description]" id="description<?php echo $language['language_id']; ?>" cols="60" rows="15"><?php echo isset($article_description[$language['language_id']]) ? $article_description[$language['language_id']]['description'] : ''; ?></textarea>
                </td>
              </tr>
              <tr>
                <td><?php echo $entry_tag; ?></td>
                <td><input type="text" name="article_description[<?php echo $language['language_id']; ?>][tag]" value="<?php echo isset($article_description[$language['language_id']]) ? $article_description[$language['language_id']]['tag'] : ''; ?>" size="80" /></td>
              </tr>
            </table>
          </div>
          <?php } ?>
          
        </div>
        <div id="tab-links">
          <table class="form">
            <tr>
              <td><?php echo $entry_category; ?></td>
              <td>
              <?php if($this->config->get('config_article_category')==1){?>
              <select name="article_category[]">
              <?php foreach ($categories as $category) { ?>                
                 <?php if (in_array($category['article_category_id'], $article_category)) { ?>
                  <option value="<?php echo $category['article_category_id']; ?>" selected="selected"> <?php echo $category['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $category['article_category_id']; ?>"> <?php echo $category['name']; ?></option>
                  <?php } ?>                
              <?php } ?> 
              </select>
              <?php }else{?>
              <div class="scrollbox" style="height:200px;">
                  <?php $class = 'odd'; ?>
                  <?php foreach ($categories as $category) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array($category['article_category_id'], $article_category)) { ?>
                    <input type="checkbox" name="article_category[]" value="<?php echo $category['article_category_id']; ?>" checked="checked" />
                    <?php echo $category['name']; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="article_category[]" value="<?php echo $category['article_category_id']; ?>" />
                    <?php echo $category['name']; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
                <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
                <?php } ?> 
                </td>
            </tr>
            <tr>
              <td><span style="color:#FF0000"><?php echo $entry_new_category;?></span></td>
              <td><input type="text" name="new_category" id="new_category" value="<?php echo $new_category; ?>" size="50"/>
              </td>
              </tr>
            <tr>
          
           <!-- -->
             <tr>
            <td><?php echo $entry_related_article; ?></td>
            <td>
            <table>
                <tr>
                  <td style="padding: 0;" colspan="3"><select id="article_category" style="margin-bottom: 5px;" onchange="getArticles();">
                      <?php foreach ($categories as $category) { ?>
                      <option value="<?php echo $category['article_category_id']; ?>"><?php echo $category['name']; ?> <?php echo $category['total']; ?></option>
                      <?php } ?>
                    </select></td>
                </tr>
                <tr>
                  <td style="padding: 0;"><select multiple="multiple" id="article" size="10" style="width: 350px;">
                    </select></td>
                  <td style="vertical-align: middle;"><input type="button" value="--&gt;" onclick="addArticleRelated();" />
                    <br />
                    <input type="button" value="&lt;--" onclick="removeArticleRelated();" /></td>
                  <td style="padding: 0;"><select multiple="multiple" id="article_related" size="10" style="width: 350px;">
                    </select></td>
                </tr>
              </table>
              <div id="related_article">
                <?php foreach ($related_article as $article_related_id) { ?>
                <input type="hidden" name="related_article[]" value="<?php echo $article_related_id; ?>" />
                <?php } ?>
              </div></td>
          </tr>
           <!-- -->
             <tr>
            <td><?php echo $entry_related_product; ?></td>
            <td><table>
                <tr>
                  <td style="padding: 0;" colspan="3"><select id="product_category" style="margin-bottom: 5px;" onchange="getProducts();">
                      <?php foreach ($product_categories as $product_category) { ?>
                      <option value="<?php echo $product_category['category_id']; ?>"><?php echo $product_category['name']; ?> <?php echo $product_category['total']; ?></option>
                      <?php } ?>
                    </select></td>
                </tr>
                <tr>
                  <td style="padding: 0;"><select multiple="multiple" id="product" size="10" style="width: 350px;">
                    </select></td>
                  <td style="vertical-align: middle;"><input type="button" value="--&gt;" onclick="addProductRelated();" />
                    <br />
                    <input type="button" value="&lt;--" onclick="removeProductRelated();" /></td>
                  <td style="padding: 0;"><select multiple="multiple" id="product_related" size="10" style="width: 350px;">
                    </select></td>
                </tr>
              </table>
              <div id="related_product">
                <?php foreach ($related_product as $product_related_id) { ?>
                <input type="hidden" name="related_product[]" value="<?php echo $product_related_id; ?>" />
                <?php } ?>
              </div></td>
          </tr>
            <tr style="display:none">
              <td><?php echo $entry_store; ?></td>
              <td><div class="scrollbox">
                  <?php $class = 'even'; ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array(0, $article_store)) { ?>
                    <input type="checkbox" name="article_store[]" value="0" checked="checked" />
                    <?php echo $text_default; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="article_store[]" value="0" checked="checked"/>
                    <?php echo $text_default; ?>
                    <?php } ?>
                  </div>
                  <?php foreach ($stores as $store) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array($store['store_id'], $article_store)) { ?>
                    <input type="checkbox" name="article_store[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                    <?php echo $store['name']; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="article_store[]" value="<?php echo $store['store_id']; ?>" checked="checked"/>
                    <?php echo $store['name']; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div></td>
            </tr>
          </table>
        </div>
      
          <table class="form">  <tr>
              <td><?php echo $entry_captcha; ?></td>
   <td> <input type="text" name="captcha" value="<?php echo $captcha; ?>" />
   </td></tr>
   <tr><td></td><td>
    <img src="index.php?route=news/manager/captcha" alt="captcha" id="captcha" /> <a onclick="return change_captcha('captcha');" title="Reload">Reload</a>
    <?php if ($error_captcha) { ?>
    <span class="error"><?php echo $error_captcha; ?></span>
    <?php } ?>
    </td>
    </tr>
    </table>
      </form>
    </div>
    
    <div class="buttons" align="right">
      
      <a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a>
      <a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a>
      </div>
  </div>
</div>
<script type="text/javascript"><!--
function addProductRelated() {
	$('#product :selected').each(function() {
		$(this).remove();
		
		$('#product_related option[value=\'' + $(this).attr('value') + '\']').remove();
		
		$('#product_related').append('<option value="' + $(this).attr('value') + '">' + $(this).text() + '</option>');
		
		$('#related_product input[value=\'' + $(this).attr('value') + '\']').remove();
		
		$('#related_product').append('<input type="hidden" name="related_product[]" value="' + $(this).attr('value') + '" />');
	});
}

function removeProductRelated() {
	$('#product_related :selected').each(function() {
		$(this).remove();
		
		$('#related_product input[value=\'' + $(this).attr('value') + '\']').remove();
	});
}

function getProducts() {
	$('#product option').remove();
	
	$.ajax({
		url: 'index.php?route=news/manager/product_category&category_id=' + $('#product_category').attr('value'),
		dataType: 'json',
		success: function(data) {
			for (i = 0; i < data.length; i++) {
	 			$('#product').append('<option value="' + data[i]['product_id'] + '">' + data[i]['name'] + '</option>');
			}
		}
	});
}

function getProductsRelated() {
	$('#product_related option').remove();	
	$.ajax({
		url: 'index.php?route=news/manager/related_product',
		type: 'POST',
		dataType: 'json',
		data: $('#related_product input'),
		success: function(data) {
			$('#related_product input').remove();
			
			for (i = 0; i < data.length; i++) {
	 			$('#product_related').append('<option value="' + data[i]['product_id'] + '">' + data[i]['name'] + '</option>');
				
				$('#related_product').append('<input type="hidden" name="related_product[]" value="' + data[i]['product_id'] + '" />');
			} 
		}
	});
}

getProducts();
getProductsRelated();
//--></script>
<script type="text/javascript"><!--
function addArticleRelated() {
	$('#article :selected').each(function() {
		$(this).remove();
		
		$('#article_related option[value=\'' + $(this).attr('value') + '\']').remove();
		
		$('#article_related').append('<option value="' + $(this).attr('value') + '">' + $(this).text() + '</option>');
		
		$('#related_article input[value=\'' + $(this).attr('value') + '\']').remove();
		
		$('#related_article').append('<input type="hidden" name="related_article[]" value="' + $(this).attr('value') + '" />');
	});
}

function removeArticleRelated() {
	$('#article_related :selected').each(function() {
		$(this).remove();
		
		$('#related_article input[value=\'' + $(this).attr('value') + '\']').remove();
	});
}

function getArticles() {
	$('#article option').remove();
	
	$.ajax({
		url: 'index.php?route=news/manager/article_category&article_category_id=' + $('#article_category').attr('value'),
		dataType: 'json',
		success: function(data) {
			for (i = 0; i < data.length; i++) {
	 			$('#article').append('<option value="' + data[i]['article_id'] + '">' + data[i]['name'] + '</option>');
			}
		}
	});
}

function getArticlesRelated() {
	$('#article_related option').remove();
	
	$.ajax({
		url: 'index.php?route=news/manager/related_article',
		type: 'POST',
		dataType: 'json',
		data: $('#related_article input'),
		success: function(data) {
			$('#related_article input').remove();
			
			for (i = 0; i < data.length; i++) {
	 			$('#article_related').append('<option value="' + data[i]['article_id'] + '">' + data[i]['name'] + '</option>');
				
				$('#related_article').append('<input type="hidden" name="related_article[]" value="' + data[i]['article_id'] + '" />');
			} 
		}
	});
}

getArticles();
getArticlesRelated();
//--></script>
<script type="text/javascript" src="catalog/view/javascript/news/ckeditor/ckeditor.js"></script> 
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('description<?php echo $language['language_id']; ?>');
<?php } ?>
//--></script> 

<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
$('.date').datepicker({dateFormat: 'yy-mm-dd'});
$('.datetime').datetimepicker({
	dateFormat: 'yy-mm-dd',
	timeFormat: 'h:m'
});
$('.time').timepicker({timeFormat: 'h:m'});
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
$('#languages a').tabs(); 
$('#seo_languages a').tabs(); 
$('#vtab-option a').tabs();
//--></script> 

<script language="javascript" type="text/javascript">//<![CDATA[
/*strip*/ 
function geturl(obj,target,ext){
var str = (document.getElementById(obj).value);
str= str.toLowerCase();
str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
str= str.replace(/đ/g,"d");
str= str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\:|\'| |\"|\&|\#|\[|\]|~/g,"-");
str= str.replace(/_/g,"-");//repalce two '_' '_'to one 1 '-'
str= str.replace(/-+-/g,"-"); //repalce two '-' '-'to one 1 '-'
str= str.replace(/^\-+|\-+$/g,"");//cut '-' from first &end of the string
	if (str=='') {alert('You must enter Title!')}else{$('#'+target).val(str+ext);}
}
//]]></script>
<script>
      function countChar(val,obj) {
        var len = val.value.length;
        var obj = obj;
        $('#num_'+obj).text(len);
      };
</script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script> 
<script type="text/javascript">
new AjaxUpload('upload_image', {
	action: 'index.php?route=news/manager/image',
	name: 'file',
	autoSubmit: false,
	responseType: 'json',
	onChange: function(file, extension) {
		this.submit();
	},		
	onSubmit: function(file, extension) {
		$('#upload_image').after('<img src="catalog/view/theme/default/image/loading.gif" id="loading" style="padding-left: 5px;" />');
	},
	onComplete: function(file, json) {
		if (json['success']) {
			alert(json['success']);
			
			$('#image').attr('value', json['filename']);
			$('#thumb').attr('src', json['thumb']);
			//alert(obj);
		}
		
		if (json['error']) {
			alert(json['error']);
		}
	
		$('#loading').remove();	
	}
});	
//--></script> 
<script type="text/javascript"><!--
function change_captcha(id){
var obj=document.getElementById(id);
var src=obj.src;var date=new Date();
obj.src=src + '&' + date.getTime();
			return false;}
//--></script> 
<?php echo $footer; ?>