
<style>
.ui-progress {
  background-color: #339BB9!important;
  border: 1px solid #339BB9;
}.ui-progress {
  -moz-transition: background-color 0.5s ease-in, border-color 1.5s ease-out, box-shadow 1.5s ease-out;
  -webkit-transition: background-color 0.5s ease-in, border-color 1.5s ease-out, box-shadow 1.5s ease-out;
  -o-transition: background-color 0.5s ease-in, border-color 1.5s ease-out, box-shadow 1.5s ease-out;
  transition: background-color 0.5s ease-in, border-color 1.5s ease-out, box-shadow 1.5s ease-out;
}.ui-progress {
  /* Usual setup stuff */
  position: relative;
  display: block;
  overflow: hidden;
  /* Height should be 2px less than .ui-progress-bar so as to not cover borders and give it a look of being inset */
  height: 7px;
  /* Rounds the ends, we specify an excessive amount to make sure they are completely rounded */
  /* Adjust to your liking, and don't forget to adjust to the same amount in .ui-progress-bar */
  -moz-border-radius: 25px;
  -webkit-border-radius: 25px;
  -o-border-radius: 25px;
  -ms-border-radius: 25px;
  -khtml-border-radius: 25px;
  border-radius: 25px;
  /* Set the background size so the stripes work correctly */
  -webkit-background-size: 0px 0px;
  -moz-background-size: 36px 36px;
  /* Webkit */
  /* For browser that don't support gradients, we'll set a base background colour */
  background-color: #339BB9;
  /* Webkit background stripes and gradient */
  background: -webkit-gradient(linear, 0 0, 44 44, color-stop(0, rgba(255, 255, 255, 0.17)), color-stop(0.25, rgba(255, 255, 255, 0.17)), color-stop(0.26, rgba(255, 255, 255, 0)), color-stop(0.5, rgba(255, 255, 255, 0)), color-stop(0.51, rgba(255, 255, 255, 0.17)), color-stop(0.75, rgba(255, 255, 255, 0.17)), color-stop(0.76, rgba(255, 255, 255, 0)), color-stop(1, rgba(255, 255, 255, 0))), -webkit-gradient(linear, left bottom, left top, color-stop(0, rgba(255, 255, 255, 0)), color-stop(1, rgba(255, 255, 255, 0.35))), #339BB9;
  /* Mozilla (Firefox etc) background stripes */
  /* Note: Mozilla's support for gradients is more true to the original design, allowing gradients at 30 degrees, as apposed to 45 degress in webkit. */
  background: -moz-repeating-linear-gradient(top left -30deg, rgba(255, 255, 255, 0.17), rgba(255, 255, 255, 0.17) 15px, rgba(255, 255, 255, 0) 15px, rgba(255, 255, 255, 0) 30px), -moz-linear-gradient(rgba(255, 255, 255, 0.25) 0%, rgba(255, 255, 255, 0) 100%), #000000;
  -moz-box-shadow: inset 0px 1px 0px 0px rgba(255, 255, 255, 0.4), inset 0px -1px 1px rgba(0, 0, 0, 0.2);
  -webkit-box-shadow: inset 0px 1px 0px 0px rgba(255, 255, 255, 0.4), inset 0px -1px 1px rgba(0, 0, 0, 0.2);
  -o-box-shadow: inset 0px 1px 0px 0px rgba(255, 255, 255, 0.4), inset 0px -1px 1px rgba(0, 0, 0, 0.2);
  box-shadow: inset 0px 1px 0px 0px rgba(255, 255, 255, 0.4), inset 0px -1px 1px rgba(0, 0, 0, 0.2);
  /* Give it a higher contrast outline */
  border: 1px solid #339BB9;
  /* Animate */
  -moz-animation: animate-stripes 2s linear infinite;
  -webkit-animation: animate-stripes 2s linear infinite;
  -o-animation: animate-stripes 2s linear infinite;
  -ms-animation: animate-stripes 2s linear infinite;
  -khtml-animation: animate-stripes 2s linear infinite;
  animation: animate-stripes 2s linear infinite;
  /* Style status label */
}
</style>

<div class="box">
 <div class="box-heading"><img src="catalog/view/javascript/news/poll.png" style="margin-bottom:-3px;" width="16" height="16" alt="" /> <?php echo $heading_title;?></div>
 <div class="box-content" id="poll">
 <?php if($poll_data){?>
  <b><?php echo $poll_data['question'];?></b>
  <?php if(isset($answered)||isset($disabled)){?>
  <?php if(isset($reactions)){?>
   <table>
   <?php for ($i=0;
			$i < 15;
			$i++){?>
    <?php if(!empty($poll_data['answer_'.($i + 1)])){?>
    <tr>
    <td><strong style="float:left"><?php echo $percent[$i];?>%</strong></td>
     <td><div style="width:<?php echo $percent[$i]+1*1;?>px;height:8px;" class="ui-progress"></div></td>
    </tr>
    <tr>
     <td class="bottom" colspan="2"><?php echo $poll_data['answer_'.($i + 1)];?>   (<?php echo $vote[$i]?> )</td>
    </tr>
    <?php }?>
   <?php }?>
   <tr>
    <td colspan="2" style="text-align: center;"><?php echo $text_total_votes.$total_votes;?></td>
   </tr>
   </table>
   <div class="vote"><a href="<?php echo $poll_results;?>"  class="colorbox<?php echo $module;?>"><?php echo $text_poll_results;?></a></div>
  <?php }
		else{?>
   <div style="text-align: center;"><?php echo $text_no_votes;?></div>
  <?php }?>
  <?php }
		else{?>
  <form method="post" action="<?php echo $action;?>" id="vote<?php echo $module;?>">
   <table>
   <?php for ($i=0;
			$i < 15;
			$i++){?>
    <?php if(!empty($poll_data['answer_'.($i + 1)])){?>
    <tr>
    <td width="12%"><input type="radio" name="poll_answer" value="<?php echo $i + 1;?>" id="answer<?php echo $i + 1;?>"/></td>
    <td><label for="answer<?php echo $i + 1;?>"><?php echo $poll_data['answer_'.($i + 1)];?></label></td>
    </tr>
    <?php }?>
   <?php }?>
   <input type="hidden" name="poll_id" value="<?php echo $poll_id;?>"/>
   </table>
   <div class="vote"><a onclick="$('#vote<?php echo $module;?>').submit();" class="button"><span><?php echo $text_vote;?></span></a></div>
  </form>
  <div class="vote"><a href="<?php echo $poll_results;?>" class="colorbox<?php echo $module;?>" rel="colorbox"><?php echo $text_poll_results;?></a></div>
  <?php }?>
 <?php }
		else{?>
  <div style="text-align: center;"><?php echo $text_no_poll;?></div>
 <?php }?>
 </div>
</div>
<script type="text/javascript"><!--
$('.colorbox<?php echo $module;?>').colorbox({
	width: 640,
	height: 480
});
//--></script> 