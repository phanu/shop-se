<?php echo $header; ?>
<style>
    .description .info{padding-bottom:5px; }
  .description .info *{ text-align:right;font-size:11px; vertical-align: middle;text-decoration:none; }
  .description .info span{margin-right:5px;}
  .description .info span.date{ background:url(catalog/view/theme/default/image/news/date.png) left bottom no-repeat; padding-left:15px;}
  .description .info span.author{ background:url(catalog/view/theme/default/image/news/author.png) left bottom no-repeat; padding-left:15px;}
  .description .info span.comment{ background:url(catalog/view/theme/default/image/news/comment.png) left bottom no-repeat; padding-left:15px;}
  .description .info span.viewed{ background:url(catalog/view/theme/default/image/news/viewed.png) left bottom no-repeat; padding-left:15px;}
  
   #content .product-grid .name{ text-align:center;min-height:30px;}
   #content .product-grid .rating{ display:none;}
   #content .product-list .rating{ float:right;}
   #content .product-list .related ul{margin-left:<?php echo $width;?>px;}
   
  </style>
<?php echo $column_left; ?>
<?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <div class="product-info">
   <?php echo $description; ?>
  <div align="right" style="margin-bottom:10px; margin-right:15px;">
         <i><?php if($this->config->get('config_display_refer')==1&&$reference_title){?> 
       <?php echo $entry_reference;?>  <a href="<?php echo $reference_url;?>" style=" font-size:13px;color:#666; font-weight:bold; text-decoration:none;" target="_blank"><?php echo $reference_title;?></a>
        <?php } ?><br>
       <?php echo $text_date_added;?> <?php echo $date_added;?> <?php if ($author) { ?>
        <?php echo $text_author; ?> <a href="<?php echo $authors; ?>" style="text-decoration:none; font-weight:bold;"><?php echo $author; ?></a>
        <?php } ?></i>
         </div>
        
    <div class="right">
     
      
      <?php if ($comment_status) { ?>
      <div class="review">
        <div><img src="catalog/view/theme/default/image/stars-<?php echo $rating; ?>.png" alt="<?php echo $comments; ?>" />&nbsp;&nbsp;<a onclick="$('a[href=\'#tab-review\']').trigger('click');"><?php echo $comments; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('a[href=\'#tab-review\']').trigger('click');"><?php echo $text_write; ?></a>
        <div style="float:right"><!-- AddThis Button BEGIN -->
          <div class="addthis_default_style"><a class="addthis_button_compact"><?php echo $text_share; ?></a> <a class="addthis_button_email"></a><a class="addthis_button_print"></a> <a class="addthis_button_facebook"></a> <a class="addthis_button_twitter"></a></div>
          <script type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js"></script> 
          <!-- AddThis Button END --> 
        </div></div>
      </div>
      <?php } ?>
    </div>
  </div>
  <div id="tabs" class="htabs">
    <?php if ($comment_status) { ?>
    <a href="#tab-review"><?php echo $tab_comment; ?></a>
    <?php } ?>
  
    <?php if ($poll_id) { ?>
    <a href="#tab-poll"><?php echo $tab_poll; ?></a>
    <?php } ?>
     <?php if ($thumb || $images) { ?>
    <a href="#tab-image"><?php echo $tab_image; ?></a>
    <?php } ?>
    
     <?php if ($downloads) { ?>
    <a href="#tab-download"><?php echo $tab_download; ?> (<?php echo count($downloads); ?>)</a>
    <?php } ?>
  </div>
  
  <?php if ($thumb || $images) { ?>
  <div id="tab-image" class="tab-content"> 
  
  <div class="product-info">
  
      
      <?php if ($thumb) { ?>
      <div style="text-align:center; margin-bottom:10px;"><a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="fancybox" rel="fancybox"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image"  style="border: 1px solid #e7e7e7; padding:3px;"/></a></div>
      <?php } ?>
      <?php if ($images) { ?>
      <div style="text-align:center;">
        <?php foreach ($images as $image) { ?>
        <span><a href="<?php echo $image['popup']; ?>" title="<?php echo $image['title']; ?>" class="fancybox" rel="fancybox" style="margin:3px;"><img src="<?php echo $image['thumb']; ?>" title="<?php echo $image['title']; ?>" alt="<?php echo $image['title']; ?>" style="border: 1px solid #e7e7e7; padding:3px;"/></a></span>
        <?php } ?>
      </div>
      <?php } ?>
      
      </div>
      
    </div>
    <?php } ?>
    
  <?php if ($downloads) { ?>
  <div id="tab-download" class="tab-content"> 
   <?php if ($this->config->get('config_login_to_download')==1) { ?>
    <div class="attention"><?php echo $text_login_to_download;?></div>
    <?php } ?>
  <div class="box-product" align="center">
 		<?php foreach ($downloads as $download) { ?>
      <div>
        <?php if ($download['thumb']) { ?>
       <div class="image"><a href="<?php echo $download['href']; ?>" title="<?php echo $download['mask']; ?>"><img src="<?php echo $download['thumb']; ?>" alt="<?php echo $download['mask']; ?>" /></a></div>
        <?php } ?>
        <div class="name"><a href="<?php echo $download['href']; ?>"><?php echo $download['name']; ?></a></div>
       </div>
      <?php } ?>
      
      </div>
      
    </div>
    <?php } ?>
    <!-- start poll--> 
      <?php if ($poll_id) { ?>
  <div id="tab-poll" class="tab-content">   
  <div class="box">
 		
      <div>
      
<style>
.ui-progress {
  background-color: #339BB9!important;
  border: 1px solid #339BB9;
}.ui-progress {
  -moz-transition: background-color 0.5s ease-in, border-color 1.5s ease-out, box-shadow 1.5s ease-out;
  -webkit-transition: background-color 0.5s ease-in, border-color 1.5s ease-out, box-shadow 1.5s ease-out;
  -o-transition: background-color 0.5s ease-in, border-color 1.5s ease-out, box-shadow 1.5s ease-out;
  transition: background-color 0.5s ease-in, border-color 1.5s ease-out, box-shadow 1.5s ease-out;
}.ui-progress {
  /* Usual setup stuff */
  position: relative;
  display: block;
  overflow: hidden;
  /* Height should be 2px less than .ui-progress-bar so as to not cover borders and give it a look of being inset */
  height: 7px;
  /* Rounds the ends, we specify an excessive amount to make sure they are completely rounded */
  /* Adjust to your liking, and don't forget to adjust to the same amount in .ui-progress-bar */
  -moz-border-radius: 25px;
  -webkit-border-radius: 25px;
  -o-border-radius: 25px;
  -ms-border-radius: 25px;
  -khtml-border-radius: 25px;
  border-radius: 25px;
  /* Set the background size so the stripes work correctly */
  -webkit-background-size: 0px 0px;
  -moz-background-size: 36px 36px;
  /* Webkit */
  /* For browser that don't support gradients, we'll set a base background colour */
  background-color: #339BB9;
  /* Webkit background stripes and gradient */
  background: -webkit-gradient(linear, 0 0, 44 44, color-stop(0, rgba(255, 255, 255, 0.17)), color-stop(0.25, rgba(255, 255, 255, 0.17)), color-stop(0.26, rgba(255, 255, 255, 0)), color-stop(0.5, rgba(255, 255, 255, 0)), color-stop(0.51, rgba(255, 255, 255, 0.17)), color-stop(0.75, rgba(255, 255, 255, 0.17)), color-stop(0.76, rgba(255, 255, 255, 0)), color-stop(1, rgba(255, 255, 255, 0))), -webkit-gradient(linear, left bottom, left top, color-stop(0, rgba(255, 255, 255, 0)), color-stop(1, rgba(255, 255, 255, 0.35))), #339BB9;
  /* Mozilla (Firefox etc) background stripes */
  /* Note: Mozilla's support for gradients is more true to the original design, allowing gradients at 30 degrees, as apposed to 45 degress in webkit. */
  background: -moz-repeating-linear-gradient(top left -30deg, rgba(255, 255, 255, 0.17), rgba(255, 255, 255, 0.17) 15px, rgba(255, 255, 255, 0) 15px, rgba(255, 255, 255, 0) 30px), -moz-linear-gradient(rgba(255, 255, 255, 0.25) 0%, rgba(255, 255, 255, 0) 100%), #000000;
  -moz-box-shadow: inset 0px 1px 0px 0px rgba(255, 255, 255, 0.4), inset 0px -1px 1px rgba(0, 0, 0, 0.2);
  -webkit-box-shadow: inset 0px 1px 0px 0px rgba(255, 255, 255, 0.4), inset 0px -1px 1px rgba(0, 0, 0, 0.2);
  -o-box-shadow: inset 0px 1px 0px 0px rgba(255, 255, 255, 0.4), inset 0px -1px 1px rgba(0, 0, 0, 0.2);
  box-shadow: inset 0px 1px 0px 0px rgba(255, 255, 255, 0.4), inset 0px -1px 1px rgba(0, 0, 0, 0.2);
  /* Give it a higher contrast outline */
  border: 1px solid #339BB9;
  /* Animate */
  -moz-animation: animate-stripes 2s linear infinite;
  -webkit-animation: animate-stripes 2s linear infinite;
  -o-animation: animate-stripes 2s linear infinite;
  -ms-animation: animate-stripes 2s linear infinite;
  -khtml-animation: animate-stripes 2s linear infinite;
  animation: animate-stripes 2s linear infinite;
  /* Style status label */
}
</style>
 <?php if($poll_data){?>
  <b><?php echo $poll_data['question'];?></b>
  <?php if(isset($answered)||isset($disabled)){?>
  <?php if(isset($reactions)){?>
   <table>
   <?php for ($i=0;
			$i < 15;
			$i++){?>
    <?php if(!empty($poll_data['answer_'.($i + 1)])){?>
    <tr>
    <td><strong style="float:left"><?php echo $percent[$i];?>%</strong></td>
     <td><div style="width:<?php echo $percent[$i]+1*1;?>px;height:8px;" class="ui-progress"></div></td>
    </tr>
    <tr>
     <td class="bottom" colspan="2"><?php echo $poll_data['answer_'.($i + 1)];?>   (<?php echo $vote[$i]?> )</td>
    </tr>
    <?php }?>
   <?php }?>
   <tr>
    <td colspan="2" style="text-align: center;"><?php echo $text_total_votes;?> <?php echo $total_votes;?></td>
   </tr>
   </table>
   <div class="vote"><a href="<?php echo $poll_results;?>"  class="fancybox<?php echo $poll_id;?>"><?php echo $text_poll_results;?></a></div>
  <?php }
		else{?>
   <div style="text-align: center;"><?php echo $text_no_votes;?></div>
  <?php }?>
  <?php }
		else{?>
  <form method="post" action="<?php echo $poll_action;?>" id="vote<?php echo $poll_id;?>">
   <table>
   <?php for ($i=0;
			$i < 15;
			$i++){?>
    <?php if(!empty($poll_data['answer_'.($i + 1)])){?>
    <tr>
    <td width="12%"><input type="radio" name="poll_answer" value="<?php echo $i + 1;?>" id="answer<?php echo $i + 1;?>"/></td>
    <td><label for="answer<?php echo $i + 1;?>"><?php echo $poll_data['answer_'.($i + 1)];?></label></td>
    </tr>
    <?php }?>
   <?php }?>
   <input type="hidden" name="poll_id" value="<?php echo $poll_id;?>"/>
   </table>
   <div class="vote"><a onclick="$('#vote<?php echo $poll_id;?>').submit();" class="button"><span><?php echo $text_vote;?></span></a></div>
  </form>
  <div class="vote"><a href="<?php echo $poll_results;?>" class="fancybox<?php echo $poll_id;?>"><?php echo $text_poll_results;?></a></div>
  <?php }?>
 <?php }
		else{?>
  <div style="text-align: center;"><?php echo $text_no_poll;?></div>
 <?php }?>
  <script type="text/javascript"><!--
$('.fancybox<?php echo $poll_id;?>').fancybox({
	width: 640,
	height: 480,
	autoDimensions: false
});
//--></script> 

      </div>
      </div>
    </div>
    <?php } ?>
 <!-- end poll--> 
  <?php if ($comment_status) { ?>
  <div id="tab-review" class="tab-content">
    <div id="review"></div>
    <h2 id="review-title"><?php echo $text_write; ?></h2>
    <b><?php echo $entry_name; ?></b><br />
    <input type="text" name="name" value="" />
    <br />
    <br />
    <b><?php echo $entry_comment; ?></b>
    <textarea name="text" cols="40" rows="8" style="width: 98%;"></textarea>
    <span style="font-size: 11px;"><?php echo $text_note; ?></span><br />
    <br />
    <b><?php echo $entry_rating; ?></b> <span><?php echo $entry_bad; ?></span>&nbsp;
    <input type="radio" name="rating" value="1" />
    &nbsp;
    <input type="radio" name="rating" value="2" />
    &nbsp;
    <input type="radio" name="rating" value="3" />
    &nbsp;
    <input type="radio" name="rating" value="4" />
    &nbsp;
    <input type="radio" name="rating" value="5" />
    &nbsp;<span><?php echo $entry_good; ?></span><br />
    <br />
    <b><?php echo $entry_captcha; ?></b><br />
    <input type="text" name="captcha" value="" />
    <br />
    <img src="index.php?route=news/article/captcha" alt="" id="captcha" /> <a onclick="return change_captcha('captcha');" title="Reload">Reload</a><br />
    <br />
    <div class="buttons">
      <div class="right"><a id="button-review" class="btn light-blue darken-1"><span><?php echo $button_continue; ?></span></a></div>
    </div>
  </div>
  <?php } ?>
  <?php if ($products) { ?>
  <!-- relate product --> <div class="box">
  <div class="box-heading" style="border-color:#d4d4d4"><?php echo $text_related_product; ?> (<?php echo count($products); ?>)</div>
  <div class="box-content" style="border-color:#d4d4d4">
    <div class="box-product" align="center">
      <?php foreach ($products as $product) { ?>
      <div>
        <?php if ($product['thumb']) { ?>
       <div class="image"><?php if ($product['special']) { ?><div class="label"></div><?php }?><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
        <?php } ?>
        <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
        <?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
           <s><?php echo $product['price']; ?></s> <b><?php echo $product['special']; ?></b>
          <?php } ?>
        </div>
        <?php } ?>
        <?php if ($product['rating']) { ?>
        <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
        <?php } ?>
        <a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button"><span><?php echo $button_cart; ?></span></a></div>
      <?php } ?>
    </div>  
  </div>
</div>
  <!-- //relate product --> 
 <?php } ?>
  <?php if ($articles) { ?>
  <!-- relate article --> 
  <div class="box">
  <div class="box-heading" style="border-color:#d4d4d4"><?php echo $tab_related; ?> (<?php echo count($articles); ?>)</div>
  <div class="box-content" style="border-color:#d4d4d4">
    <div class="product-list">
      <?php foreach ($articles as $article) { ?>      
    <div style="min-width:<?php echo $width;?>px">
        <?php if ($article['thumb']) { ?>
       <div class="image"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" /></a></div>
        <?php } ?>
        <div class="name"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>
        
        <div class="description">
        <div class="info">
    <span class="author"><a href="<?php echo $article['author_href']; ?>" title="<?php echo $text_author;?> <?php echo $article['author']; ?>"><?php echo $article['author']; ?></a></span>
    <span class="date"><i><?php echo $article['date_added']; ?></i></span>
    <span class="comment"><?php echo $article['comments']; ?></span>
    <span class="viewed"><?php echo $article['viewed']; ?></span>
     </div>    
        <?php echo $article['description']; ?><br> 
        <a style="float:right" href="<?php echo $article['href']; ?>" class="button"><span><?php echo $text_more; ?></span></a></div>       
        <?php if ($article['rating']) { ?>
        <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $article['rating']; ?>.png" alt="<?php echo $article['comments']; ?>" /></div>
        <?php } ?></div>
      <?php } ?>
    </div>  
  </div>
</div>
  <!-- //relate article --> 
  <?php } ?>
  
  <?php if ($tags) { ?>
  <div class="tags"><b><?php echo $text_tags; ?></b>
    <?php for ($i = 0; $i < count($tags); $i++) { ?>
    <?php if ($i < (count($tags) - 1)) { ?>
    <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
    <?php } else { ?>
    <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
    <?php } ?>
    <?php } ?>
  </div>
  <?php } ?>
  <?php //echo $content_bottom; ?></div>
  <script type="text/javascript"><!--
$('.fancybox').fancybox({cyclic: true});
//--></script> 
<script type="text/javascript"><!--
$('#review .pagination a').live('click', function() {
	$('#review').fadeOut('slow');
		
	$('#review').load(this.href);
	
	$('#review').fadeIn('slow');
	
	return false;
});			

$('#review').load('index.php?route=news/article/comment&article_id=<?php echo $article_id; ?>');

$('#button-review').bind('click', function() {

	function clear_data(){ 
		$('input[name=\'name\']').val('');
			$('textarea[name=\'text\']').val('');
			$('input[name=\'rating\']:checked').attr('checked','');
			$('input[name=\'captcha\']').val('');
	}
	function change_captcha(){
		var obj=document.getElementById("captcha");
		var src=obj.src;
		var date=new Date();
		obj.src=src + '&' + date.getTime();
	}
	function show_review(){ 
		$.get("index.php?route=news/article/comment&article_id=<?php echo $article_id;?>",function(data){ 
		$("#review").html(data);
		});	
	}
	$.ajax({
		url: 'index.php?route=news/article/write&article_id=<?php echo $article_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-review').attr('disabled', true);
			$('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-review').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data['error']) {
				$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
			}
			
			if (data['success']) {
				$('#review-title').after('<div class="success">' + data['success'] + '</div>');
				clear_data();
				change_captcha();
				show_review();
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script> 
<script type="text/javascript"><!--
function change_captcha(id){
var obj=document.getElementById(id);
var src=obj.src;var date=new Date();
obj.src=src + '&' + date.getTime();
			return false;}
//--></script> 
<?php echo $footer; ?>