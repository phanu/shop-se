<?php
// -------------------------------------
// Product Tabs or Modules for OpenCart
// By Best-Byte
// www.best-byte.com
// -------------------------------------
?>
<?php if(!empty($tabs)){ ?>
<?php if($use_tab == 1){?>
<div id="product_tabs<?php echo $module; ?>" class="htabs" style="padding-top:5px;">
	<?php $numTab = 1; ?>
	 <?php foreach ($tabs as $tab) { ?>
	  <a href="#tab-<?php echo $numTab; ?><?php echo $module; ?>"><?php echo $tab['title']; ?></a>
	 <?php $numTab++; ?>
	<?php } ?>
</div>
<?php }?>
<?php $numTab = 1; ?>
<?php foreach ($tabs as $tab) { ?>
<div id="tab-<?php echo $numTab; ?><?php echo $module; ?>">
  <div class="box">
	  <?php if($use_tab == 0){ ?>
		<div class="box-heading"><?php echo $tab['title']; ?></div>
	  <?php }?>
	  <div class="box-content">
		<div class="box-product">
		  <?php if(!empty($tab['products'])){ ?>
		  <?php foreach ($tab['products'] as $product) { ?>
		    <div>
				<?php if ($product['thumb']) { ?>
			 
			   <div class="image"><?php if ($product['quantity'] <= 0) { ?><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['soldout']; ?>" alt="<?php echo $product['name']; ?>" class="soldout" /></a><?php } ?><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
				<?php } ?>
				<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
			

   <?php if ($product['zero_price'] > 0) { ?>
      <div class="price">

        <?php if (!$product['special']) { ?>
        <?php echo $product['price']; ?>
        <?php } else { ?>
        <s><?php echo $product['price']; ?></s> <n><?php echo $product['special']; ?><n>
        <?php } ?>
      </div>
      <?php } else { ?>
	  <div class="price"><img src="./image/call_for_price.png" align="absmiddle" /> <?php echo "Call for Price" ?></div>
	  <?php } ?>





				 <?php if (($product['quantity'] <= 0)&&($product['stock_status'] == "�Թ�����ش��ü�Ե")) { ?>
      <div class="cart"><a class="zero_button"><span><?php echo $product['stock_status']; ?></span></a></div>	  
	  <?php } else {?>
	  <div class="cart"><a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button"><span><?php echo $button_cart; ?></span></a></div>
	  <?php } ?>


        </div>
			  <?php } ?>
		  <?php } ?>
		</div>
	  </div>
  </div>
</div>
<?php $numTab++; ?>
<?php } ?>
<script type="text/javascript"><!--
$('#product_tabs<?php echo $module; ?> a').tabs();
//--></script>
<?php } ?>