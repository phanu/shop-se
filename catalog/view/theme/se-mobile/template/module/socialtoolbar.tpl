<?php
// -------------------------------------------
// Social Tool Bar for OpenCart v1.5.1.x
// By Best-Byte
// www.best-byte.com
// -------------------------------------------
?>
<link type="text/css" href="toolbar/themes/<?php echo $color; ?>/<?php echo $color; ?>.css" rel="stylesheet" />
<div id="fubar">
<ul><li>
<!-- Begin Home Button -->
<a href="./index.php?rout=common/home" class="tooltip" title="Home"><img src="./toolbar/icons/home.png" alt="" /></a>
<!-- End Home Button -->
   </li>
</ul>
<span class="jx-separator-left"></span>
   
<ul><li title="Subscribe">
<!-- Begin AddToAny Subscription Button -->

<a class="a2a_dd" href="http://www.addtoany.com/subscribe?linkname=&amp;linkurl=http%3A%2F%2Ffeeds.feedburner.com%2F<?php echo $subscribe; ?>"><img src="./toolbar/icons/subscribe.png" alt=""></img><span>Subscribe</span></a>
<script type="text/javascript">a2a_linkurl="http://feeds.feedburner.com/<?php echo $subscribe; ?>";</script><script type="text/javascript" src="http://static.addtoany.com/menu/feed.js"></script> 

<!-- End AddToAny Subscription Button -->			
		</li>
</ul>
<span class="jx-separator-left"></span>
   
<ul><li title="Share This">
<!-- Begin AddToAny Share Button -->

<a class="a2a_dd" href="http://www.addtoany.com/share_save"><img src="./toolbar/icons/share.png" alt=""></img><span>Share This</span></a>
<script type="text/javascript">a2a_linkname="<?php echo $sharethis; ?>";a2a_hide_embeds=0;</script><script type="text/javascript" src="http://static.addtoany.com/menu/page.js"></script> 
      
<!-- End AddToAny Share Button -->
		</li>
</ul>
<span class="jx-separator-left"></span>

<ul><li title="กด Like Fan Page เพื่อติดตามสินค้าใหม่ๆ">
<!-- Begin Facebook Share Widget -->
                
<a href="http://toolbar2.wibiya.com/fanpage/fanpage.php?toolbarAppId=58857&appId=27&toolbarId=8530&fpid=<?php echo $facebook; ?>;?iframe=true&width=840&height=450" rel="prettyPhoto[pp_gal]" class="tooltip" title="กด Like Fan Page เพื่อติดตามสินค้าใหม่ๆ"><img src="./toolbar/icons/facebook.png" alt=""></img><span>ติดตามสินค้าใหม่ๆ</span></a>

<!-- End Facebook Share Widget -->					
		</li>
</ul>

<span class="jx-separator-left"></span>

<ul><li title="รับชมรีวิวสินค้าของทาง SE-Update">
<!-- Begin YouTube -->

<a id="videoslink" href="./toolbar/videos/videos.html?iframe=true&width=640&height=480" rel="prettyPhoto[pp_gal]" class="tooltip" title="รับชมรีวิวสินค้าของทาง SE-Update"><img src="./toolbar/icons/youtube.png" alt=""></img><span>ดูคลิปรีวิวสินค้า</span></a>

<!-- End YouTube -->
		</li>
</ul>

<span class="jx-separator-left"></span>

<ul><li title="Your Language">  
<!-- Begin Translate -->

<div class="translate_this"><a href="http://www.translation-services-usa.com/" title="Translation" class="translate_this_drop"><span class="translate_this_d_86_21_1">Translation</span></a></div>
<script type="text/javascript">var translate_this_src = 'en';</script><script src="http://s1.translation-services-usa.com/_1_0/javascript/e.js" type="text/javascript"></script>

<!-- End Translate -->
		</li>
</ul>      
<span class="jx-separator-left"></span> 

<ul class="jx-bar-button-right">
    <li title="Links" style="margin-right:60px;"><a href="#" class="tooltip" title="Useful Links"><img src="./toolbar/icons/block.png" alt="" /><span>Links</span></a>
<ul>
<!-- Begin Links -->  
     <li><a href="./index.php?route=product/special"><img src="./toolbar/icons/specials.png" title="Special Offers" alt="Specials" /></a></li>
     <li><a href="./index.php?route=information/sitemap"><img src="./toolbar/icons/sitemap.png" title="Site Map" alt="Site Map" /></a></li>
     <li><a href="./index.php?route=account/account"><img src="./toolbar/icons/account.png" title="My Account" alt="My Account" /></a></li>
     <li><a href="./index.php?route=information/information&information_id=4"><img src="./toolbar/icons/info.png" alt="About Us" /></a></li>
     <li><a href="./index.php?route=information/information&information_id=3"><img src="./toolbar/icons/info.png" alt="Privacy Policy" /></a></li>
     <li><a href="./index.php?route=information/information&information_id=5"><img src="./toolbar/icons/info.png" alt="Terms & Conditions" /></a></li>
<!-- End Links -->
</ul>
    </li>            
</ul>

</div>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
	$("#fubar a[rel^='prettyPhoto']").prettyPhoto({theme:'facebook'});
});
</script>  
