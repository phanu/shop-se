<?php if ($products) { ?>
<div class="box">
<div class="box-heading"><?php echo $heading_title; ?> <?php echo $category_name; ?><?php if (!isset($this->request->get['path'])) { ?> - <a style="font-weight: bold; font-size: 13px;" href="<?php echo $category_url; ?>"><?php echo $viewall; ?></a><?php } ?></div>
<div class="box-content">
<div class="box-product">
<?php foreach ($products as $product) { ?>
    <div>
      <?php if ($product['thumb']) { ?>
      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
      <?php } ?>
      <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
      <?php if ($product['price']) { ?>
      <div class="price">
        <?php if (!$product['special']) { ?>
        <?php echo $product['price']; ?>
        <?php } else { ?>
           <b><?php echo $product['special']; ?></b><?php } ?>
      </div>
      <?php } ?>
     
	  <div class="cart"><a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button"><span><?php echo $button_cart; ?></span></a></div>
    </div>
    <?php } ?>
</div>
</div>
</div>
<?php } ?>