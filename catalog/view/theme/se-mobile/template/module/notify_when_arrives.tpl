<?php if ($out_stock_list) { ?>

<!--  <a href="#change-address" class="text-small modal-trigger">asdasdasds</a> -->
<!-- Modal Structure -->
<div id="change-address" class="modal modal-fixed-footer" style="background-color: #546e7a; 
    width: 100% !important; top: 0px !important;">

<div class="modal-content" style="padding: 0px;" >
                    <div class="card blue-grey darken-1" style="box-shadow: none;">
                        <div class="card-content white-text" style="padding: 5px;">
                            <span class="card-title" style="font-size: 19px; line-height: 30px;"><?php echo $heading_title; ?></span>
                            <p style="font-size: 12px;"><?php echo strip_tags("$text_description"); ?></p>
           

                            <?php if ($nwa_use_name){?>
                            <div class="input-field">
                                <input type="text"    name="notify_name" value="" id="nwa_list_name"  />
                                <label for="nwa_name" data-error="wrong" data-success="right" style="color:#fff;"><?php echo $nwa_entry_name; ?></label>
                            </div>
                            <?php }?>
                            <?php if ($nwa_use_phone){?>
                            <div class="input-field">
                                <input type="text"    name="notify_phone" value="" id="nwa_list_phone"  style="color:#fff;"/>
                                <label for="nwa_phone" data-error="wrong" data-success="right" style="color:#fff;"><?php echo $nwa_entry_phone; ?></label>
                            </div>
                            <?php }?>
                            <?php if ($nwa_use_custom){?>
                                <div class="input-field">
                                <input type="text"    name="notify_custom" id="nwa_list_custom"  style="color:#fff;"/>
                                <label for="nwa_phone" data-error="wrong" data-success="right" style="color:#fff;"><?php echo $nwa_entry_custom; ?></label>
                                <div/>
                                    <?php }?>
                                    <?php if ($nwa_entry_mail){?>
                                    <div class="input-field">
                                        <input type="text" name="nwa_email" id="nwa_list_email" value="" style="color:#fff;"/>
                                        <label for="nwa_email" data-error="wrong" data-success="right" style="color:#fff;"><?php echo $nwa_entry_mail; ?></label></div>
                                        <?php }?>
                                        
                                        <input type="hidden"  name="notify_product_id" id="nwa_list_product_id" value="<?php echo $notify_product_id; ?>"/>
                                    </div>
                                    <div class="card-action" style="padding: 5px;">
                                        <a id="nwa_list_register" class="btn waves-effect waves-light"><i class="material-icons"><?php echo $button_register; ?></i></a>

                                        <a href="#!" class="modal-action modal-close btn-flat " ><?php echo $button_close; ?></a>
                                        <!--<a id="nwa_product_msg"><?php echo $button_close; ?></a>-->
                                        <div  id="nwa_list_msg"></div>
                                    </div>
                                </div>
                        </div>
                    </div>
</div>

</div>

<script type="text/javascript">

$(function() {

$('input[onclick*="addToCart"],a[onclick*="addToCart"]').nwaList({
title:'<?php echo $heading_title; ?>',
text:'<?php echo $button_category; ?>',
out_stock_list:[<?php echo $out_stock_list; ?>],
gray_style:<?php echo ($gray_style) ? 'true' : 'false'; ?>,
show_mode: <?php echo $nwa_list_show_mode; ?>
})
});

</script>
<?php } ?>

<?php if ($out_stock_product || $out_stock_option) { ?>
<div id="nwa_product_container" class="modal modal-fixed-footer" >
    <div class="box">
    <div class="box-content" id="nwa_product">
        <div class="row">
            <div class="col s12 m6">
                    <div class="card blue-grey darken-1">
                        <div class="card-content white-text">
                            <span class="card-title"><?php echo $heading_title; ?></span>
                            <p><?php echo strip_tags("$text_description"); ?></p>
                            <br/>
                            
                            <?php if ($nwa_use_name){?>
                            <div class="input-field">
                                <input type="text"    name="notify_name"   value=""    id="nwa_name"  />
                                <label for="nwa_name" data-error="wrong" data-success="right" style="color:#fff;"><?php echo $nwa_entry_name; ?></label>
                            </div>
                            <?php }?>
                            <?php if ($nwa_use_phone){?>
                            <div class="input-field">
                                <input type="text"    name="notify_phone"    value=""   id="nwa_phone"  style="color:#fff;"/>
                                <label for="nwa_phone" data-error="wrong" data-success="right" style="color:#fff;"><?php echo $nwa_entry_phone; ?></label>
                            </div>
                            <?php }?>
                            <?php if ($nwa_use_custom){?>
                                <div class="input-field">
                                <input type="text"    name="notify_custom"      id="nwa_custom"  style="color:#fff;"/>
                                <label for="nwa_phone" data-error="wrong" data-success="right" style="color:#fff;"><?php echo $nwa_entry_custom; ?></label>
                                <div/>
                                    <?php }?>
                                    <?php if ($nwa_entry_mail){?>
                                    <div class="input-field">
                                        <input type="text" name="nwa_email" id="nwa_email" value="" style="color:#fff;"/>
                                        <label for="nwa_email" data-error="wrong" data-success="right" style="color:#fff;"><?php echo $nwa_entry_mail; ?></label></div>
                                        <?php }?>
                                        
                                  <input type="hidden"   name="nwa_product_id"      id="nwa_product_id"  value="<?php echo $notify_product_id; ?>"/>
                                        <input type="hidden"   name="nwa_option_id"       id="nwa_option_id"   value=""/>
                                        <input type="hidden"   name="nwa_option_value_id" id="nwa_option_value_id" value=""/>
                                    </div>
                                    <div class="card-action">
                                        <a id="nwa_product_register" class="btn waves-effect waves-light"><i class="material-icons"><?php echo $button_register; ?></i></a>


                                        <!--<a id="nwa_product_msg"><?php echo $button_close; ?></a>-->
                                        <div  id="nwa_product_msg"></div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <?php if ($out_stock_product) { ?>
        <script type="text/javascript">
        $(function() {
        $('body').nwaProduct({
        title:'<?php echo $heading_title; ?>',
        text:'<?php echo $button_category; ?>',
        out_stock_list:[<?php echo $out_stock_list; ?>],
        gray_style:<?php echo ($gray_style) ? 'true' : 'false'; ?>,
        show_mode: <?php echo $nwa_product_show_mode; ?>,
        button_cart: '#button-cart'
        });
        });
        </script>
    <?php } ?>
<?php } ?>