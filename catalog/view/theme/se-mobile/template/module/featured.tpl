<!-- Top Product Section -->
      <div class="page-block margin-bottom">

        <h2 class="block-title">
          <span><?php echo $heading_title; ?></span><!-- <span> tag to make blue border on this text only -->
          <a href="#" class="list-all">
            <i class="fa fa-th-list"></i>
          </a>
        </h2>

        <!-- Product Listing Slider -->
        <!--
        Use .top-product class to make blue border on each item
        -->
        <ol class="product-list-slider top-product">
        <?php foreach ($products as $product) { ?>
          <li><!-- Product Slider item -->
            <div class="thumb featured_prod">
              <a href="<?php echo $product['href']; ?>">
                <?php if ($product['quantity'] <= 0) { ?><img src="<?php echo $product['soldout']; ?>" alt="<?php echo $product['name']; ?>" class="soldout" /><?php } ?>
                <img src="<?php echo $product['thumb']; ?>" alt="">
              </a>
            </div>
            <div class="product-ctn product-ctn0">
              <div class="product-name">
                <a href="<?php echo $product['href']; ?>">
                  <?php echo $product['name']; ?> 
                </a>
              </div>
              <?php if ($product['price']) { ?>
              <div class="price">
                <?php if (!$product['special']) { ?>
                <span class="price-current"><?php echo $product['price']; ?></span>
                <?php } else { ?>
                <span class="price-before"><?php echo $product['price']; ?></span>
                <span class="price-current"><?php echo $product['special']; ?></span>
                <?php } ?>
              </div>
              <?php } ?>
            </div>
          </li><!-- End Product Slider item -->
          <?php } ?>
        </ol>
        <!-- End Product Listing Slider -->

        <!-- Use this class (.clear) to clearing float -->
        <div class="clear"></div>

      </div>
      <!-- End Top Product Section -->

<script type="text/javascript">
  $(document).ready(function(){

      // Cache the highest
      var highestBox = 0;
      
      // Select and loop the elements you want to equalise
      $('.product-ctn.product-ctn0 .product-name', this).each(function(){
        // If this box is higher than the cached highest then store it
        if($(this).height() > highestBox) {
          highestBox = $(this).height(); 
        }
      });  
            
      // Set the height of all those children to whichever was highest 
      $('.product-ctn.product-ctn0 .product-name',this).height(highestBox);
                    
});
</script>