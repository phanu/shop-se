 <?php $rand = md5(rand());?>
        <style>
            /* css for this page */
            #one_<?php echo $rand;?> { font: 14px/24px 'Helvetica Neue', Arial, sans-serif; color: #333; width: 960px; margin: 10px auto 10px; background: #eee }
            #one_<?php echo $rand;?> > h2 { margin: 30px 0 15px; text-shadow: 1px 1px 0 white; border-bottom: 2px solid #333; padding-bottom: 5px }
            #one_<?php echo $rand;?> > h1 { text-align: center; margin-bottom: 30px; text-shadow: 1px 1px 0 white }
            strong + p { margin-top: 0 }
            dt { font-weight: bold }
            dd { margin: 0 }
            figure { display: block; width: <?php echo $width; ?>px; height: <?php echo $height; ?>px; margin: 0 }
            .accordion.stitch .slide div { background: #333 }
            .social-buttons { width: 100%; text-align: center; margin-top: 30px }
            .social-buttons ul { display: inline-block; margin: 0 }
            .social-buttons li { float: left; list-style-type: none }
            .call_out {
                color: #444444;
                background: url('img-demo/callout_bg.gif') repeat;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                border-radius: 4px;
                -webkit-box-shadow: 1px 0 3px rgba(0, 0, 0, 0.08);
                -moz-box-shadow: 1px 0 3px rgba(0, 0, 0, 0.08);
                box-shadow: 1px 0 3px rgba(0, 0, 0, 0.08);
                margin-top: 20px;
            }

            .call_out .wrap {
                padding: 19px 20px 17px 20px;
                border: 1px solid #d7d4cb;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
            }

            .call_out h2 {
                font-family: Georgia, "Times New Roman", Times, serif;
                font-style: italic;
                text-shadow: 0 1px 0 rgba(255, 255, 255, 1);
                color: #444444;
                font-size: 18px;
                line-height: 20px;
                margin: 10px 0;
            }
            
            .call_out a.alt_btn {
                background: url('img-demo/sprite.png') 24px 356px;
                display: block;
                padding: 0 0 0 23px;
                float: right;
                margin: -45px 0 0 0;
                font-family: Georgia, "Times New Roman", Times, serif;
                font-style: italic;
                font-size: 14px;
                color: #252525;
                text-shadow: 0px 1px 1px rgba(255, 255, 255, 0.8);
                text-align: center;
                border: none;
                -webkit-box-shadow: 0;
                -moz-box-shadow: 0;
                box-shadow: 0;
                text-decoration: none;
                font-weight: bold;
            }

            .call_out a.alt_btn span {
                padding: 14px 23px 15px 0;
                background: url('img-demo/sprite.png') 100% 253px;
                display: block;
            }

            .call_out a:hover.alt_btn { background: url('img-demo/sprite.png') 24px 408px }
            .call_out a:hover.alt_btn span { background: url('img-demo/sprite.png') 100% 304px }
        </style>

        <!-- liteAccordion css -->
        <link href="catalog/view/theme/default/stylesheet/liteaccordion.css" rel="stylesheet" />

        <!-- easing -->
        <script src="catalog/view/javascript/jquery.easing.1.3.js"></script>

        <!-- liteAccordion js -->
        <script src="catalog/view/javascript/liteaccordion.jquery.js"></script>

        <!--[if lt IE 9]>
            <script>
                document.createElement('figure');
            </script>
        <![endif]-->         
    </head>
    <body>
        <div id="one_<?php echo $rand;?>">
            <ol>
             <?php foreach ($banners as $banner) { ?>
                <li>
                    <h2><span><?php echo $banner['title']; ?></span></h2>
                    <div>
                        <figure>
                            <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></a>
                        </figure>
                    </div>
                </li>
            <?php } ?>

            </ol>
            <noscript>
                <p>Please enable JavaScript to get the full experience.</p>
            </noscript>
        </div>
        <script>
            (function($, d) {
                $('#one_<?php echo $rand;?>').liteAccordion({
                        containerHeight: '<?php echo $height; ?>px',
                        maxContainerWidth: '980px',
                        containerWidth: <?php echo $width;?>,
                          
                        <?php if (isset($setting['autoPlay']) && $setting['autoPlay']) { echo "autoPlay : true,";} ?>
                        <?php if (isset($setting['activateOn']) && $setting['activateOn']) { echo "activateOn : '".$setting['activateOn']."',";} ?>
                        <?php if (isset($setting['slideSpeed']) && $setting['slideSpeed']) { echo "slideSpeed : ".(int)$setting['slideSpeed'].",";} ?>                       
                        <?php if (isset($setting['cycleSpeed']) && $setting['cycleSpeed']) { echo "cycleSpeed : ".(int)$setting['cycleSpeed'].",";} ?>                                               
                        <?php if (isset($setting['firstSlide']) && $setting['firstSlide']) { echo "firstSlide : ".(int)$setting['firstSlide'].",";} ?>                       
                          
                        <?php if (isset($setting['pauseOnHover']) && $setting['pauseOnHover']) { echo "pauseOnHover : true,";} ?>
                        <?php if (isset($setting['theme']) && $setting['theme']) { echo "theme : '".$setting['theme']."',";} ?>
                        <?php if (isset($setting['rounded']) && $setting['rounded']) { echo "rounded : true,";} ?>
                        <?php if (isset($setting['enumerateSlides']) && $setting['enumerateSlides']) { echo "enumerateSlides : true,";} ?>
                        <?php if (isset($setting['easting']) && $setting['easting']) { echo "easing : '".$setting['easting']."',";} ?>

                });
            })(jQuery, document);

        </script>

