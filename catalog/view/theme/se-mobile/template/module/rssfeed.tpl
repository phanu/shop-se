<script type="text/javascript" src="catalog/view/javascript/scroll.js"></script>

<script type="text/javascript">
$(function() {
	$(".newsticker-jcarousellite").jCarouselLite({
		vertical: <?php echo $direction; ?>,
		hoverPause: true,
		visible: 1,
		auto: 2000,
		speed: 4500
	});
});
</script>
<style>
	.newsticker-jcarousellite ul li {padding: 20px;}
</style>

<div class="box">
	<div class="box-heading"><?php echo $heading_title; ?></div>
	<div class="box-content">
		<div class="box-category" id="rss">
			<div class="newsticker-jcarousellite">
				<ul>
				<?php if ($news) { ?>

					<?php foreach ($news as $rss) { ?>
					<li>
						<b><?php echo $rss['title']; ?></b><a href="<?php echo $rss['link']; ?>" target="_blank">  <?php echo $text_more; ?></a><br /><br />
						<p><?php echo $rss['content']; ?></p>
						
					</li>
					<?php } ?>

				<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</div>
