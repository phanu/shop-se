<?php
//==============================================================================
// Dynamic Categories Module v201.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
// 
// All code within this file is copyright Clear Thinking, LLC.
// You may not copy or reuse code within this file without written permission.
//==============================================================================
?>
<!-- CONTENT CONTAINER -->

                <div class="searchtop">
                    <!-- collap Lists -->
                    <ul class="collapsible" data-collapsible="accordion">
                        <li>
                            <div class="collapsible-header"><?php echo $settings['heading_' . $language]; ?><span class="fa fa-angle-right"></span></div>
                            <div class="collapsible-body">

                              <div class="content-container-search">
                                <!-- Custom (se-update.com) form container -->
                                <div class="se-form">
                                    <div class="row" style="margin-bottom: 0px;">
                                       
                                           
        <?php foreach ($dropdowns as $dropdown) { ?>
         <div class="input-field"  <?php if (empty($dropdown['options'])){  echo 'style="display:none;"'; } ?>>	
			<select class="form-control<?php echo $dropdown['type']; ?>-dropdown" onchange="<?php echo (($dropdown['type'] == 'category') ? 'changeCategories' : 'redirectPage') . $module_id; ?>($(this))" title="<?php echo $dropdown['text']; ?>" <?php if (empty($dropdown['options'])){ echo 'disabled="disabled"'; }?>>
				<option value="X"><?php echo $dropdown['text']; ?></option>
				<?php foreach ($dropdown['options'] as $option) { ?>
					<?php $selected = ($dropdown['type'] == 'category' && in_array($option['category_id'], $paths)) || ($dropdown['type'] == 'product' && isset($this->request->get['product_id']) && $this->request->get['product_id'] == $option['product_id']); ?>
					<option value="<?php echo $option[$dropdown['type'].'_id']; ?>" <?php if ($selected) echo 'selected="selected"'; ?>><?php echo $option['name']; ?></option>
				<?php } ?>
			</select>
		</div>
		<?php } ?>
        <!-- .row -->
                                    <div class="input-field">
        <?php if (!empty($settings['go_button_' . $language])) { ?>
			<button type="button" class="btn btn-block green" onclick="redirectPage<?php echo $module_id; ?>($(this).parent())"><i class="fa fa-check"></i><?php echo $settings['go_button_' . $language]; ?></button>
		<?php } ?>
                                    </div>
                                </div>
                                </div>
        <!-- End Custom Form -->
                            </div>
                        </li>
                    </ul>
                    <!-- End collab Lists -->
                </div>
                <!-- END CONTENT CONTAINER -->
<script type="text/javascript"><!--
	function redirectPage<?php echo $module_id; ?>(dropdown) {
		if (dropdown.val() == 'X') return;
		
		var paths = '<?php if ($settings['first_dropdown'] != 0) echo $settings['first_dropdown'] . '_'; ?>';
		dropdown.parent().find('.category-dropdown').each(function(){
			if ($(this).val() != 'X' && $(this).val() != null) {
				paths += $(this).val() + '_';
			}
		});
		
		if (paths) {
			if (dropdown.hasClass('product-dropdown')) {
				var redirect = 'product/product';
				var args = 'path=' + paths.substr(0, paths.length - 1) + '&product_id=' + dropdown.val();
			} else {
				var redirect = 'product/category';
				var args = 'path=' + paths.substr(0, paths.length - 1);
			}
			
			$.ajax({
				type: 'POST',
				data: {redirect: redirect, args: args},
				url: 'index.php?route=<?php echo $type; ?>/<?php echo $name; ?>/rewriteURL',
				beforeSend: function() {
					dropdown.parent().parent().find('.loading').show();
				},
				success: function(data) {
					location = data;
				}
			});
		}
	}
	
	<?php if ($settings['category_loading'] == 'static') {
		echo "var categories = [];\n";
		foreach ($all_categories as $parent_id => $categories) {
			echo "categories[" . $parent_id . "] = '";
			foreach ($categories as $category) {
				echo '<option value="' . $category['category_id'] . '">' . $category['name'] . '</option>';
			}
			echo "';\n";
		}
	} ?>
	
	function changeCategories<?php echo $module_id; ?>(dropdown) {
		dropdown.nextAll('select').find('option').not(':first-child').remove();
		dropdown.nextAll('select').attr('disabled', 'disabled');
		
		category_id = dropdown.val();
		if (category_id == 'X') return;
		
		dropdown.parent().parent().find('.loading').show();
		var noNextDropdown = (dropdown.next().length == 0 || dropdown.next().hasClass('go-button'));
		
		<?php if ($settings['category_loading'] == 'static') { ?>
			if (!categories[category_id]) {
		<?php } else { ?>
			$.ajax({
				url: 'index.php?route=<?php echo $type; ?>/<?php echo $name; ?>/getSubCategories&category_id=' + category_id,
				dataType: 'json',
				success: function(data) {
					if (data[0] == null) {
		<?php } ?>
						<?php if ($settings['last_dropdown'] == 'products') { ?>
							$.ajax({
								url: 'index.php?route=<?php echo $type; ?>/<?php echo $name; ?>/getProducts&category_id=' + category_id,
								dataType: 'json',
								success: function(data) {
									var hasProducts = false;
									var html = "\n";
									if (noNextDropdown) {
										html += '<select class="browser-default product-dropdown" onchange="redirectPage<?php echo $module_id; ?>($(this))" title="<?php echo $text_select; ?>">';
										html += '<option value="X" selected="selected"><?php echo $text_select; ?></option>';
									} else {
										html += '<option value="X" selected="selected">' + dropdown.next().attr('title') + '</option>';
									}
									
									for (product in data) {
										hasProducts = true;
										html += '<option value="' + data[product]['product_id'] + '">' + data[product]['name'] + '</option>';
									}
									
									if (hasProducts) {
										if (noNextDropdown) {
											html += '</select>';
											dropdown.after(html);
										} else {
											dropdown.next().attr('class', 'form-control product-dropdown');
											dropdown.next().attr('onchange', 'redirectPage<?php echo $module_id; ?>($(this))');
											dropdown.next().html(html);
										}
										dropdown.next().removeAttr('disabled');
										dropdown.parent().parent().find('.loading').hide();
									} else {
										redirectPage<?php echo $module_id; ?>(dropdown);
									}
								}
							});
						<?php } else { ?>
							redirectPage<?php echo $module_id; ?>(dropdown);
						<?php } ?>
					} else {
						var html = "\n";
		<?php if ($settings['category_loading'] == 'static') { ?>
						if (noNextDropdown) {
							html += '<select class="browser-default category-dropdown" onchange="changeCategories<?php echo $module_id; ?>($(this))" title="<?php echo $text_select; ?>">';
							html += '<option value="X" selected="selected"><?php echo $text_select; ?></option>';
						} else {
							html += '<option value="X" selected="selected">' + dropdown.next().attr('title') + '</option>';
						}
						html += categories[category_id];
		<?php } else { ?>
						if (noNextDropdown) {
							html += '<div class="input-field"><select class="browser-default category-dropdown" onchange="changeCategories<?php echo $module_id; ?>($(this))" title="<?php echo $text_select; ?>">';
							html += '<option value="X" selected="selected"><?php echo $text_select; ?></option>';
						} else {
							html += '<option value="X" selected="selected">' + dropdown.next().attr('title') + '</option>';
						}
						for (category in data) {
							html += '<option value="' + data[category]['category_id'] + '">' + data[category]['name'] + '</option>';
						}
		<?php } ?>
						if (noNextDropdown) {
							html += '</select>';
							dropdown.after(html);
						} else {
							dropdown.next().attr('class', 'browser-default category-dropdown');
							dropdown.next().attr('onchange', 'changeCategories<?php echo $module_id; ?>($(this))');
							dropdown.next().html(html);
						}
						dropdown.next().removeAttr('disabled');
						dropdown.parent().parent().find('.loading').hide();
					}
		<?php if ($settings['category_loading'] != 'static') { ?>
				}
			});
		<?php } ?>
	}
//--></script>
