<?php 
$content_template = DIR_TEMPLATE . str_replace('ontime.tpl', 'ontime_content.tpl', $this->template);
if ($setting) { ?>
<div class="box">
	<div class="box-heading"><?php echo $heading_title; ?></div>
	<div id="ontime-dynamic-content" class="box-content">
<?php if ($config['cache_option']) { ?>	
		<div style="text-align:center">
			<img src="image/ontime_loader.gif">
		</div>
<?php } else {
			include_once($content_template);
		} ?>
</div>
</div>
<?php if ($config['cache_option'] || $config['dynamic_option']) { ?>
<script type="text/javascript">
<?php if ($config['dynamic_option'] && !$config['cache_option']) { ?>			
setTimeout(function(){onTimeInit()}, 25000);
<?php } else if ($config['cache_option']) { ?>
onTimeInit();
<?php } ?>
function onTimeInit() {			
	$.ajax({		
		url: 'index.php?route=module/ontime',		
		cache: false,				
		type: 'GET',		
		dataType: 'html',		
		success: function(html) {
			$('#ontime-dynamic-content').html(html);		
		},		
		complete: function() {			
<?php if ($config['dynamic_option']) { ?>			
			setTimeout(function(){onTimeInit()}, 25000);
<?php } ?>			
		},
		error: function(xhr, ajaxOptions, thrownError) {
			console.log(thrownError);	
		}	
	});		
}
</script>
<?php } ?>
<?php }  else { 
	include_once($content_template);
} ?>