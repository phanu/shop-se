<div id="load" style="display:none;">
<div id="features">
      <?php foreach ($products as $product) { ?>
      <div title="">
        <?php if ($product['thumb']) { ?>
        <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
        <?php } ?>
        <h2><a id="prodtitle" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h2>
        <p><?php echo $product['description']; ?></p>
        <a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="buttons-cart"><span><?php echo $button_cart; ?></span></a>
        <?php if ($product['price']) { ?> 
          <?php if (!$product['special']) { ?>     
          <span class="prices"><?php echo $product['price']; ?></span>    
          <?php } else { ?>
          <span class="prices-new"><?php echo $product['special']; ?></span><span class="prices-old"><?php echo $product['price']; ?></span> 
          <?php } ?>     
        <?php } ?>
        <?php if ($product['rating']) { ?>
        <div class="ratings" style="background:none;"><img style="border:none;" src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
        <?php } ?>
      </div>
      <?php } ?>
</div>
</div>
<style type="text/css">
#features, #slidingFeatures, #labelFeatures, #basicFeatures, #thumbFeatures {
	border:<?php echo $mainbordersize ?> solid #<?php echo $mainborder ?> ;
	-webkit-border-top-left-radius: <?php echo $bannerboxcorner ?>px;
	-webkit-border-top-right-radius: <?php echo $bannerboxcorner ?>px;
	-moz-border-radius-topleft: <?php echo $bannerboxcorner ?>px;
	-moz-border-radius-topright: <?php echo $bannerboxcorner ?>px;
	-khtml-border-top-left-radius: <?php echo $bannerboxcorner ?>px;
	-khtml-border-top-right-radius: <?php echo $bannerboxcorner ?>px;
	border-top-left-radius: <?php echo $bannerboxcorner ?>px;
	border-top-right-radius: <?php echo $bannerboxcorner ?>px;
	}
.jshowoff div {	
	<?php  if ($gradientbgcolor == 'blackgrad-bottom' || $gradientbgcolor == 'whitegrad-bottom' ) { ?>
	background: #<?php echo $bgcolor ?> url(catalog/view/javascript/jquery/slideprod/images/<?php echo $gradientbgcolor ?>.png) bottom repeat-x;	
	<?php } elseif ($gradientbgcolor == 'centerglare') { ?>
	background: #<?php echo $bgcolor ?> url(catalog/view/javascript/jquery/slideprod/images/<?php echo $gradientbgcolor ?>.png) center repeat-y;
	<?php } else { ?>
	background: #<?php echo $bgcolor ?> url(catalog/view/javascript/jquery/slideprod/images/<?php echo $gradientbgcolor ?>.png) top repeat-x;
	<?php } ?>
	}
.jshowoff img {
	-webkit-border-radius: <?php echo $imageprodcorner ?>px;
	-moz-border-radius: <?php echo $imageprodcorner ?>px;
	-khtml-border-radius: <?php echo $imageprodcorner ?>px;
	border-radius: <?php echo $imageprodcorner ?>px;
	border:#<?php echo $imageprodborder ?> solid <?php echo $imageprodbordersize ?>;
	}	
span.prices {
	color:#<?php echo $pricetextcolor ?>;
	background: #<?php echo $bgprice ?>;
	border:<?php echo $pricebordersize ?> solid #<?php echo $priceborder ?>;
	-webkit-border-radius: <?php echo $pricecorner ?>px;
	-moz-border-radius: <?php echo $pricecorner ?>px;
	-khtml-border-radius: <?php echo $pricecorner ?>px;
	border-radius: <?php echo $pricecorner ?>px;
} 	
span.prices-old {
	color:#<?php echo $priceoldtextcolor ?>;
	background: #<?php echo $bgprice ?>;
	border-left:<?php echo $pricebordersize ?> solid #<?php echo $priceborder ?>;
	border-top:<?php echo $pricebordersize ?> solid #<?php echo $priceborder ?>;
	border-bottom:<?php echo $pricebordersize ?> solid #<?php echo $priceborder ?>;
	-webkit-border-radius: <?php echo $pricecorner ?>px 0px 0px <?php echo $pricecorner ?>px;
	-moz-border-radius: <?php echo $pricecorner ?>px 0px 0px <?php echo $pricecorner ?>px;
	-khtml-border-radius: <?php echo $pricecorner ?>px 0px 0px <?php echo $pricecorner ?>px;
	border-radius: <?php echo $pricecorner ?>px 0px 0px <?php echo $pricecorner ?>px;
}
span.prices-new {
	color:#<?php echo $pricetextcolor ?>;
	background: #<?php echo $bgprice ?>;
	border-right:<?php echo $pricebordersize ?> solid #<?php echo $priceborder ?>;
	border-top:<?php echo $pricebordersize ?> solid #<?php echo $priceborder ?>;
	border-bottom:<?php echo $pricebordersize ?> solid #<?php echo $priceborder ?>;
	-webkit-border-radius: 0px <?php echo $pricecorner ?>px <?php echo $pricecorner ?>px 0px;
	-moz-border-radius: 0px <?php echo $pricecorner ?>px <?php echo $pricecorner ?>px 0px;
	-khtml-border-radius: 0px <?php echo $pricecorner ?>px <?php echo $pricecorner ?>px 0px;
	border-radius: 0px <?php echo $pricecorner ?>px <?php echo $pricecorner ?>px 0px;
}
a.buttons-cart {
	background: #<?php echo $bgbuttoncart ?>;
	color:#<?php echo $addcarttextcolor ?>;
	border:<?php echo $addcartbordersize ?> solid #<?php echo $addcartborder ?>;
	-webkit-border-radius: <?php echo $addcartcorner ?>px;
	-moz-border-radius: <?php echo $addcartcorner ?>px;
	-khtml-border-radius: <?php echo $addcartcorner ?>px;
	border-radius: <?php echo $addcartcorner ?>px;
}	
p.jshowoff-controls {
	background: #<?php echo $bottombgcolor ?> url(catalog/view/javascript/jquery/slideprod/images/<?php echo $gradientbottombgcolor ?>.png) top repeat-x;
	-moz-border-radius-bottomleft: <?php echo $bannerboxcorner ?>px;
	-moz-border-radius-bottomright: <?php echo $bannerboxcorner ?>px;
	-webkit-border-bottom-left-radius: <?php echo $bannerboxcorner ?>px;
	-webkit-border-bottom-right-radius: <?php echo $bannerboxcorner ?>px;
	-khtml-border-bottom-left-radius: <?php echo $bannerboxcorner ?>px;
	-khtml-border-bottom-right-radius: <?php echo $bannerboxcorner ?>px;
	border-bottom-left-radius: <?php echo $bannerboxcorner ?>px;
	border-bottom-right-radius: <?php echo $bannerboxcorner ?>px;
	}	
.jshowoff-slidelinks a, .jshowoff-controls a {
	background-color: #<?php echo $bgnavlinks ?>;
	color: #<?php echo $navlinkstextcolor ?>;
	-moz-border-radius: <?php echo $navlinkscorner ?>px;
	-webkit-border-radius: <?php echo $navlinkscorner ?>px;
	-khtml-border-radius: <?php echo $navlinkscorner ?>px;
	border-radius: <?php echo $navlinkscorner ?>px;
	border:<?php echo $navlinksbordersize ?> solid #<?php echo $navlinksborder ?>;
}
a#prodtitle {
	color:#<?php echo $prodnametextcolor ?>;
}
.jshowoff p {
	color:#<?php echo $shortdesctextcolor ?>;
	}	
</style>
<script type="text/javascript">	
$(document).ready(function(){	
	$('#load').fadeIn(2000);
});
</script>
<script type="text/javascript">		
				$(document).ready(function(){ $('#features').jshowoff({
					effect: '<?php echo $effect; ?>',
					hoverPause: <?php echo $hoverpause; ?>,
					speed: <?php echo $speed; ?>,
					changeSpeed: <?php echo $changespeed; ?>,
					controls: <?php echo $controls; ?>,
					links: <?php echo $links; ?>
				}); });
			</script>
