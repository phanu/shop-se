<?php
//-----------------------------------------------------
// BrandsMenu for Opencart v1.5.1    
// Created by villagedefrance                          
// contact@villagedefrance.net                             
//-----------------------------------------------------
?>

<style type="text/css" media="screen">
.box .box-heading img { float: left; margin-right: 8px; }
</style>

<?php if($box) { ?>
<div class="box">
	<div class="box-heading">
		<?php if($icon) { ?>
			<img src="catalog/view/theme/cl_default/image/brands.png" alt="" />
		<?php } ?>
		<?php if($title) { ?>
			<?php echo $title; ?>
		<?php } ?>
	</div>
	<div class="box-content" style="text-align:center; padding-bottom:10px;">
		<form>
			<select onchange="location = this.options[this.selectedIndex].value;">
				<option value=""><?php echo $text_select; ?></option>
				<?php foreach ($manufacturers as $manufacturer) { ?>
					<?php if ($manufacturer['manufacturer_id'] == $manufacturer_id) { ?>
						<option value="<?php echo $manufacturer['href']; ?>" selected="selected"><?php echo $manufacturer['name']; ?></option>
					<?php } else { ?>
						<option value="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></option>
					<?php } ?>
				<?php } ?>
			</select>
		</form>
		<div>&nbsp;</div>
		<?php foreach ($manufacturers as $manufacturer) { ?>
			<?php if ($manufacturer['manufacturer_id'] == $manufacturer_id) { ?>
				<a href="<?php echo $manufacturer['href']; ?>" title="<?php echo $manufacturer['name']; ?>"><img src="<?php echo $manufacturer['image']; ?>" alt="" /></a>
			<?php } ?>
		<?php } ?>
	</div>
</div>
<?php } else { ?>
	<div style="text-align:center; padding-bottom:10px;">
		<form>
			<select onchange="location = this.options[this.selectedIndex].value;">
				<option value=""><?php echo $text_select; ?></option>
				<?php foreach ($manufacturers as $manufacturer) { ?>
					<?php if ($manufacturer['manufacturer_id'] == $manufacturer_id) { ?>
						<option value="<?php echo $manufacturer['href']; ?>" selected="selected"><?php echo $manufacturer['name']; ?></option>
					<?php } else { ?>
						<option value="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></option>
					<?php } ?>
				<?php } ?>
			</select>
		</form>
		<div>&nbsp;</div>
		<?php foreach ($manufacturers as $manufacturer) { ?>
			<?php if ($manufacturer['manufacturer_id'] == $manufacturer_id) { ?>
				<a href="<?php echo $manufacturer['href']; ?>" title="<?php echo $manufacturer['name']; ?>"><img src="<?php echo $manufacturer['image']; ?>" alt="" /></a>
			<?php } ?>
		<?php } ?>
	</div>
<?php } ?>