<div class="fb-recommendations-bar" data-href="<?php echo $current_url; ?>" data-trigger="<?php echo $trigger . ( ($trigger == 'onvisible') ? '' : '%' ); ?>" data-read-time="<?php echo $read_time; ?>" data-action="<?php echo $verb; ?>" data-side="<?php echo $side; ?>"></div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo $app_id; ?>";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>