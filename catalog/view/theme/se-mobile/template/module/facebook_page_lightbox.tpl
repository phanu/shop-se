<div class="fb-like-box" id="facebook-page-lightbox" style="display: none;" data-href="<?php echo $page_url; ?>" data-width="<?php echo $width; ?>" data-height="<?php echo $height; ?>" data-show-faces="<?php if ($show_faces == 1) { ?>true<?php } else { ?>false<?php } ?>" data-stream="<?php if ($show_stream == 1) { ?>true<?php } else { ?>false<?php } ?>" data-header="<?php if ($show_header == 1) { ?>true<?php } else { ?>false<?php } ?>" <?php if ($color_scheme == 'dark') { ?>data-colorscheme="dark"<?php } ?> <?php if ($border_color) { ?>data-border-color="<?php echo $border_color; ?>"<?php } ?>></div>
<div id="fb-root"></div>
<script type="text/javascript"><!--
$(document).ready(function() {
	if ($('.ocx-deal').length == 0) {
		window.fbAsyncInit = function() {
			FB.init({
				status     : true,
				cookie     : true, 
				xfbml      : true 
			});
		};

		// Load the SDK Asynchronously
		(function(d){
			var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = "//connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);
		}(document));
	}
	
	setTimeout("$.colorbox({inline: true,href: '#facebook-page-lightbox',overlayClose: false,opacity: 0.5,<?php if ($width > 0) { ?>innerWidth: <?php echo $width + 20; ?><?php } ?>,<?php if ($height > 0) { ?>innerHeight: <?php echo $height; ?><?php } ?>,open: true}); $('#facebook-page-lightbox').show();", 4500);
	
	<?php if ($seconds > 0) { ?>
	$(document).bind('cbox_complete', function(){
		setTimeout("$.colorbox.close();", <?php echo $seconds; ?>);
	});
	<?php } ?>
	
	$(document).bind('cbox_closed', function(){
		$('#facebook-page-lightbox').hide();
	});
});
//--></script>