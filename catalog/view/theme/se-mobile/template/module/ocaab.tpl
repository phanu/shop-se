<?php if ($banners) { ?>
	<style type="text/css">
		.oca-banner { text-align: center; width: 100%; height:100%; display:-moz-inline-stack; display:inline-block; margin: 0px; margin-bottom: 10px; }
		.oca-clearfix:after { content: "."; display: block;	clear: right; visibility: hidden; line-height: 0; height: 0; }
		.oca-clearfix {	/*display: inline-block;*/ }
		html[xmlns] .oca-clearfix {	display: block; }
		* html .oca-clearfix { height: 1%; }
	</style>
	<div class="oca-banner oca-clearfix">
		<?php $x = 1; ?>
		<?php foreach ($banners as $banner) { ?>
			<div id="banner-<?php echo $module . '-' . $x; ?>" style="float: <?php echo $banner['float']; ?>; clear: <?php echo $banner['clear']; ?>; margin-left: <?php echo $banner['margin_left']; ?>px; margin-right: <?php echo $banner['margin_right']; ?>px; margin-top: <?php echo $banner['margin_top']; ?>px; margin-bottom: <?php echo $banner['margin_bottom']; ?>px; height: <?php echo $banner['height']; ?>px;">
				<?php foreach ($banner['banners'] as $key => $result) { ?>
					<?php if ($result['link']) { ?>
						<div><a href="<?php echo $result['link']; ?>"><img src="<?php echo $result['image']; ?>" alt="<?php echo $result['title']; ?>" title="<?php echo $result['title']; ?>" /></a></div>
					<?php } else { ?>
						<div><img src="<?php echo $result['image']; ?>" alt="<?php echo $result['title']; ?>" title="<?php echo $result['title']; ?>" /></div>
					<?php } ?>
				<?php } ?>
			</div>
			<?php $x++; ?>
		<?php } ?>
	</div>
	
	<script type="text/javascript"><!--
		<?php $x = 1; ?>
		<?php foreach ($banners as $banner) { ?>
			$(document).ready(function() {
				$('#banner-<?php echo $module . '-' . $x; ?>').cycle({
					height: '<?php echo $banner['height']; ?>',
					width: '<?php echo $banner['width']; ?>',
					effect: '<?php echo $banner_settings['effect']; ?>',
					speed: <?php echo $banner_settings['speed']; ?>,
					timeout: <?php echo $banner_settings['timeout']; ?>,
					pause: <?php echo $banner_settings['pause']; ?>
				});
			});
			<?php $x++; ?>
		<?php } ?>
	//--></script>
<?php } ?>