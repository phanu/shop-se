<html>
<head>
	<title><?php echo $heading_title; ?></title>
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $currenttemplate?>/stylesheet/twitterlogin.css" />
    <script type="text/javascript">
		window.resizeTo(540,350);
	</script>
</head>
<body style="background-color: #fbfdff; margin: 0; padding: 0 20px;">
	<div class="requireEmailFormBox">
	    <div class="infoNoteBox"><?php echo html_entity_decode($twitter_email_desc); ?></div>
	    <form class="twitRequireEmail" method="POST" action="<?php echo $this->url->link('account/twitterlogin/register', '', 'SSL'); ?>">
	    	<input type="text" name="email" placeholder="email@example.com"/>
	        <input class="submitEmailBtn" type="submit" value="<?php echo $twitter_email_submit; ?>" />
	    </form>
	    <?php if (!empty($this->session->data['twitter_error'])) : ?>
	    	<p class="notvalidEmailNotification"><?php echo $this->session->data['twitter_error']; unset($this->session->data['twitter_error']); ?></p>
	    <?php endif; ?>
	</div>
</body>
</html>