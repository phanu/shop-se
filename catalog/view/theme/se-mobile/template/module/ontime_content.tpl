		<div style="width:100%;overflow:auto">
<?php if ($config['enable_time']) {

		$default_timezone = date_default_timezone_get();
		
		if ($config['timezone']) {
			@date_default_timezone_set($config['timezone']);
		}
	
		$week_times = $config['week_times'];
		$period_days = $config['period_days'];
		$text_period_method = $text_period_method[$config['period_method']];
		$upcoming_holidays = array();	
		$next_work_day = false;
		$skip_today = false;
		$skip_tomorrow = false;
		
		$_message = '';
		$_today = date('N');
		$_tomorrow = date('N', strtotime(' +1 day'));

		$_day = ($week_times[$_today]['day'] != $week_times[$_tomorrow]['day']) ? 'today' : 'tomorrow';
		$_start = strtotime('today ' . $week_times[$_today]['start']);	
		$_end = strtotime($_day . ' ' . $week_times[${"_$_day"}]['end']);
		
		$holidays = explode(',', $config['holidays']);
		
		$working_days = array_diff_key($week, array_flip($holidays));

		if ($holidays) {
			
			if (in_array($_today, $holidays)) {
				$skip_today = true;
			}
			
			$upcoming_holidays = onTime::getUpcomingHolidays($_day, $holidays);
			
			$next_work_day = onTime::getNextWeekDay(${"_$_day"}, $working_days, true);
			
		} else {
			
			$next_work_day = onTime::getNextWeekDay(${"_$_day"}, $week, true);
		}
		
		if ($next_work_day) {

			$now = time();
			$next_work_day['date'] = onTime::getDateFormat('next ' . $next_work_day['name'], 'd.m.Y');

			if ($config['custom_holidays']) {
				
				if (in_array(onTime::getDateFormat($_day, 'Y-m-d'), $config['custom_holidays'])) {
					if (${"_$_day"} == $_today) {
						$skip_today = true;
						$_start = $_end = 0; 
						unset($config['custom_holidays'][0]);
					} else {
						if (in_array(onTime::getDateFormat($_today, 'Y-m-d'), $config['custom_holidays'])) {
							$skip_today = true;
							$_start = $_end = 0;
							unset($config['custom_holidays'][0]);
						} else {	
							$_day = 'today';
							$_end = strtotime($_day .' ' . '23:59');
						}	
					}
				}
				
				$custom_holidays = array_unique(array_merge($upcoming_holidays, $config['custom_holidays']));
				
				while($upcoming_custom_holidays = onTime::getCustomUpcomingHolidays($_day, $custom_holidays)) {	
					$upcoming_holidays = array_unique(array_merge($upcoming_holidays, $upcoming_custom_holidays), SORT_STRING);				
					$_day = end($upcoming_custom_holidays);

					if ($additional_upcoming_holidays = onTime::getUpcomingHolidays($_day, $holidays)) {
						$upcoming_holidays = array_merge($upcoming_holidays, $additional_upcoming_holidays);
						$_day = end($additional_upcoming_holidays);
					} 
					$last_upcoming_holiday = end($upcoming_holidays);
					$next_work_day = array(
						'day' => onTime::getDateFormat($last_upcoming_holiday . '+ 1 day', 'N'),
						'name' => onTime::getDateFormat($last_upcoming_holiday . '+ 1 day', 'l'),
						'date' => onTime::getDateFormat($last_upcoming_holiday . '+ 1 day', 'd.m.Y')
					);	
				}
			}
			
			if ($upcoming_holidays) {
				$skip_tomorrow = true;
			}

			$number_upcoming_holidays = count($upcoming_holidays);

			if ($number_upcoming_holidays <= 3) {
				foreach($upcoming_holidays as &$holiday) {
					$holiday = $week_lang[onTime::getDateFormat($holiday, 'N')];
				}
			}

			$_period_time = strtotime(($now < $_end && !$skip_today ? 'today' : $next_work_day['date']) . ' + ' . $period_days . ' days');
			$_period_day = $period_days ? $week_lang[date('N', $_period_time)] . ', ' . date('d.m.Y', $_period_time) : false;
			
			if ($now >= $_start && $now < $_end) {		
				$timeLeft = $_end - $now;
				$timeLeftText = $text_order_next;
			} else {
				
				if ($now > $_start) {
					$_start = strtotime($next_work_day['date'] . ' ' . $week_times[$next_work_day['day']]['start']);
					if (!$skip_today) {
						$_message .= $text_workday_end;
					}	
				}
				
				$timeLeft = $_start - $now;
				$timeLeftText = $text_order_after;
			}
			
			$_hours = (int)($timeLeft >= 3600) ? floor($timeLeft/3600) : 0;
			
			if ($_hours < 10) {
				
				$_hours = '0' . $_hours; 
			}
			
			$on_time_info = sprintf("%s\n$text_hours<br> %s\n%s", $_hours, gmdate("i", $timeLeft), $text_minutes);

			if ((!$skip_today && !$skip_tomorrow) || ($skip_tomorrow && $now < $_end) || $config['enable_timer_holiday']) { 
?>
		<div style="float:left"><img src="./image/clock.png" width="57px" height="84px"></div>
		<?php echo $timeLeftText; ?><br>
		<div style="height:2px"></div>
		<span style="font-size:16px;color:<?php echo $config['color']; ?>"><b><?php echo $on_time_info; ?></b></span>
		<br>
		<div style="clear:both;visibility:hidden;height:0"></div>
		<hr>
<?php		
			}
			if (!$skip_today) {
				if ($skip_tomorrow) {  		
					
					if ($now >= $_end) {
						$text_tomorrow_holiday = $text_tomorrow_holiday_end;
						$text_tomorrow_holidays = $text_tomorrow_holidays_end;
						$text_tomorrow_holidays_number = $text_tomorrow_holidays_end_number;
					}
					
					if ($number_upcoming_holidays > 3) {
						$_message .= sprintf($text_tomorrow_holidays_number, $number_upcoming_holidays, $week_lang[$next_work_day['day']] . ', ' . $next_work_day['date']);
					} elseif ($number_upcoming_holidays > 1) {
						$_message .= sprintf($text_tomorrow_holidays, implode(', ', $upcoming_holidays), $week_lang[$next_work_day['day']]);
					} else {
						$_message .= sprintf($text_tomorrow_holiday, array_shift($upcoming_holidays), $week_lang[$next_work_day['day']]);
					}
					
				} else {
				
					if ($now < $_end) {
						$_message .= sprintf($text_today, $week_lang[$next_work_day['day']]);
					} else {
						$_message .= sprintf($text_today_end, $week_lang[$next_work_day['day']]);
					} 
				}
			} else {
				if ($number_upcoming_holidays > 3) {
					$_message .= sprintf($text_today_holidays_number, $number_upcoming_holidays, $week_lang[$next_work_day['day']] . ', ' . $next_work_day['date']);
				} elseif ($number_upcoming_holidays >= 1) {
					$_message .= sprintf($text_today_holidays, implode(', ', $upcoming_holidays), $week_lang[$next_work_day['day']]);
				} else {
					$_message .= sprintf($text_today_holiday, $week_lang[$next_work_day['day']]);
				}					
			}
			if ($_period_day) {
				$_message .= '<hr>' . sprintf($text_period_method, $_period_day);
			}
			echo $_message;
		} else {
?>
		<b><?php echo $text_no_work; ?></b>
<?php			
		}
		
		if ($config['timezone']) {
			@date_default_timezone_set($default_timezone);
		}
	} 
		if ($config['custom_text']) { 	
			echo ($config['enable_time'] ? '<hr>' : '') . html_entity_decode($config['custom_text_content'], ENT_QUOTES, 'UTF-8');		
		}
?>
</div>