<?php if ($products) { ?>
    <ul>
        <?php foreach ($products as $product) { ?>
            <li>
                <?php if ($product['thumb']) { ?>
                    <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
                <?php } ?>
                <br/>
                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                <br/>
                <?php if ($product['price']) { ?>
                    <?php if (!$product['special']) { ?>
                        <?php echo $product['price']; ?>
                    <?php } else { ?>
                        <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                    <?php } ?>
                <?php } ?>
                <?php if ($product['rating']) { ?>
                    <br/><img src="<?php echo HTTPS_SERVER; ?>/catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" />
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
<?php } ?>