<?php if (!empty($iCustomFooter)) { echo $iCustomFooter; ?>

<div id="icustomfooter_default_footer">
		<?php if ($idata['Settings']['UseFooterWith'] == 'defaultocwithicons') : ?>
        <div class="column grid_footer_3">
            <h3><?php echo $text_information; ?></h3>
            <ul>
              <?php foreach ($informations as $information) { ?>
              <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
              <?php } ?>
            </ul>
            </div>
            <div class="column grid_footer_3">
            <h3><?php echo $text_service; ?></h3>
            <ul>
              <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
              <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
              <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
            </ul>
            </div>
            <div class="column grid_footer_3">
            <h3><?php echo $text_extra; ?></h3>
            <ul>
              <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
              <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
              <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
              <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
            </ul>
            </div>
            <div class="column grid_footer_3">
            <h3><?php echo $text_account; ?></h3>
            <ul>
              <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
              <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
              <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
              <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
      <div class="footerDivider"></div>
      <?php endif; ?>
      
      <?php if ($idata['Settings']['UseFooterWith'] == 'defaultocwithicons' || $idata['Settings']['UseFooterWith'] == 'icons') : ?>
      <div class="wrapper">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <?php if ($idata['Settings']['SocialButtons']['Show'] == 'true'): ?>
                    <ul class="iButtons">
                        <?php if ($idata['Settings']['SocialButtons']['PinterestPin']['Show'] == 'true'): ?>
                        <li class="iPinterest iButton">
                            <a href="http://pinterest.com/pin/create/button/" class="pin-it-button" count-layout="horizontal"><img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>
                            <script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
                        </li>
                        <?php endif; ?> 
                        <?php if ($idata['Settings']['SocialButtons']['GooglePlus']['Show'] == 'true'): ?>
                        <li class="iGooglePlus iButton">
                            <g:plusone size="medium" annotation="none"></g:plusone>
                            <script type="text/javascript">
                              (function() {
                                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                                po.src = 'https://apis.google.com/js/plusone.js';
                                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                              })();
                            </script>
                        </li>
                        <?php endif; ?>
                        <?php if ($idata['Settings']['SocialButtons']['TwitterPin']['Show'] == 'true'): ?>
                        <li class="iTwitterPin iButton" style="width:90px;">	
                            <a href="https://twitter.com/share" class="twitter-share-button" data-lang="en">Tweet</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        </li>
                        <?php endif; ?>
                        <?php if ($idata['Settings']['SocialButtons']['FacebookLike']['Show'] == 'true'): ?>
                        <li class="iFacebook iButton">
                            <div id="fb-root"></div>
                            <script>(function(d, s, id) {
                              var js, fjs = d.getElementsByTagName(s)[0];
                              if (d.getElementById(id)) return;
                              js = d.createElement(s); js.id = id;
                              js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=159650554163037";
                              fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                            <fb:like send="false" layout="button_count" width="200" show_faces="false"></fb:like>
                        </li>
                        <?php endif; ?>
                        <li class="clearfix"></li>
                    </ul>
                    <?php endif; ?>
                </td>  
                <td style="width: 180px">
                    <?php $poweredClass = ($idata['Settings']['SocialButtons']['Show'] == 'true') ? ($idata['Settings']['PaymentIcons']['Show'] == 'true') ? '' : 'rightAlign' : 'leftAlign'; ?>
                    <?php $poweredClass = ($idata['Settings']['PaymentIcons']['Show'] == 'false' && $idata['Settings']['SocialButtons']['Show'] == 'false') ? '' : $poweredClass; ?>
                    <div id="powered" style="width: 180px;background: transparent;" class="<?php echo $poweredClass; ?>"><?php echo $powered; ?></div>
                </td>
                <td>     
                      <?php if ($idata['Settings']['PaymentIcons']['Show'] == 'true'): ?>
                      <?php 
                    
                        $fulldir = 'image/icustomfooter/paymenticons/';
                        $raw_image_files = scandir($fulldir);
                        $imgurl = 'image/icustomfooter/paymenticons/';
                    
                        $image_files = array();
                        foreach ($raw_image_files as $key => $value) {
                            if (strstr($value,'.png') !== false || strstr($value,'.jpg') !== false || strstr($value,'.gif') !== false) {
                                $name = $value;  $name = str_replace('.png','',$name); $name = str_replace('.jpg','',$name);
                                $name = str_replace('.gif','',$name); $name = preg_replace('/[^a-z ]/i', '', $name);
                                array_push($image_files,array('path' => $imgurl. $value, 'name'=>$name, 'origname' => $value));
                            }
                        }
                        
                      
                      ?> 
                      <div class="payment-methods">
                        <table>
                            <tr>
                                <td>
                                    <?php foreach ($image_files as $key => $img) { ?>
                                        <img src="<?php echo $img['path']; ?>" title="<?php echo $img['name']; ?>"/>
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                        <div class="clearfix"></div>
                      </div>
                    <?php endif; ?>
                </td>
            </tr>
          </table>
      </div>
      <?php endif; ?>
    </div>

<?php } ?>

</div>


<script type='text/javascript'>var fc_CSS=document.createElement('link');fc_CSS.setAttribute('rel','stylesheet');var isSecured = (window.location && window.location.protocol == 'https:');var lang = document.getElementsByTagName('html')[0].getAttribute('lang'); var rtlLanguages = ['ar','he']; var rtlSuffix = (rtlLanguages.indexOf(lang) >= 0) ? '-rtl' : '';fc_CSS.setAttribute('type','text/css');fc_CSS.setAttribute('href',((isSecured)? 'https://d36mpcpuzc4ztk.cloudfront.net':'http://assets1.chat.freshdesk.com')+'/css/visitor'+rtlSuffix+'.css');document.getElementsByTagName('head')[0].appendChild(fc_CSS);var fc_JS=document.createElement('script'); fc_JS.type='text/javascript'; fc_JS.defer=true;fc_JS.src=((isSecured)?'https://d36mpcpuzc4ztk.cloudfront.net':'http://assets.chat.freshdesk.com')+'/js/visitor.js';(document.body?document.body:document.getElementsByTagName('head')[0]).appendChild(fc_JS);window.freshchat_setting= 'eyJ3aWRnZXRfc2l0ZV91cmwiOiJzZXVwZGF0ZS5mcmVzaGRlc2suY29tIiwicHJvZHVjdF9pZCI6bnVsbCwibmFtZSI6IuC4h+C4suC4meC4muC4o+C4tOC4geC4suC4o+C4q+C4peC4seC4h+C4geC4suC4o+C4guC4suC4oiDguYHguKXguLDguJrguKPguLTguIHguLLguKPguKXguLnguIHguITguYnguLIiLCJ3aWRnZXRfZXh0ZXJuYWxfaWQiOm51bGwsIndpZGdldF9pZCI6IjNiODY1NWU3LTY5NWEtNDYxMC05NTI4LTY3MjcwMjg0YWUwNiIsInNob3dfb25fcG9ydGFsIjpmYWxzZSwicG9ydGFsX2xvZ2luX3JlcXVpcmVkIjpmYWxzZSwiaWQiOjUwMDAwNzIwNzMsIm1haW5fd2lkZ2V0Ijp0cnVlLCJmY19pZCI6IjliYzg5MjRjZTEwZDkyNzk4NTA3ZWYxMmE4MTkyNzczIiwic2hvdyI6MSwicmVxdWlyZWQiOjIsImhlbHBkZXNrbmFtZSI6InNlLXVwZGF0ZSIsIm5hbWVfbGFiZWwiOiJOYW1lIiwibWFpbF9sYWJlbCI6IkVtYWlsIiwibWVzc2FnZV9sYWJlbCI6Ik1lc3NhZ2UiLCJwaG9uZV9sYWJlbCI6IlBob25lIE51bWJlciIsInRleHRmaWVsZF9sYWJlbCI6IlRleHRmaWVsZCIsImRyb3Bkb3duX2xhYmVsIjoiRHJvcGRvd24iLCJ3ZWJ1cmwiOiJzZXVwZGF0ZS5mcmVzaGRlc2suY29tIiwibm9kZXVybCI6ImNoYXQuZnJlc2hkZXNrLmNvbSIsImRlYnVnIjoxLCJtZSI6Ik1lIiwiZXhwaXJ5IjowLCJlbnZpcm9ubWVudCI6InByb2R1Y3Rpb24iLCJkZWZhdWx0X3dpbmRvd19vZmZzZXQiOjMwLCJkZWZhdWx0X21heGltaXplZF90aXRsZSI6IkNoYXQgaW4gcHJvZ3Jlc3MiLCJkZWZhdWx0X21pbmltaXplZF90aXRsZSI6IkxldCdzIHRhbGshIiwiZGVmYXVsdF90ZXh0X3BsYWNlIjoiWW91ciBNZXNzYWdlIiwiZGVmYXVsdF9jb25uZWN0aW5nX21zZyI6IldhaXRpbmcgZm9yIGFuIGFnZW50IiwiZGVmYXVsdF93ZWxjb21lX21lc3NhZ2UiOiJIaSEgSG93IGNhbiB3ZSBoZWxwIHlvdSB0b2RheT8iLCJkZWZhdWx0X3dhaXRfbWVzc2FnZSI6Ik9uZSBvZiB1cyB3aWxsIGJlIHdpdGggeW91IHJpZ2h0IGF3YXksIHBsZWFzZSB3YWl0LiIsImRlZmF1bHRfYWdlbnRfam9pbmVkX21zZyI6Int7YWdlbnRfbmFtZX19IGhhcyBqb2luZWQgdGhlIGNoYXQiLCJkZWZhdWx0X2FnZW50X2xlZnRfbXNnIjoie3thZ2VudF9uYW1lfX0gaGFzIGxlZnQgdGhlIGNoYXQiLCJkZWZhdWx0X2FnZW50X3RyYW5zZmVyX21zZ190b192aXNpdG9yIjoiWW91ciBjaGF0IGhhcyBiZWVuIHRyYW5zZmVycmVkIHRvIHt7YWdlbnRfbmFtZX19IiwiZGVmYXVsdF90aGFua19tZXNzYWdlIjoiVGhhbmsgeW91IGZvciBjaGF0dGluZyB3aXRoIHVzLiBJZiB5b3UgaGF2ZSBhZGRpdGlvbmFsIHF1ZXN0aW9ucywgZmVlbCBmcmVlIHRvIHBpbmcgdXMhIiwiZGVmYXVsdF9ub25fYXZhaWxhYmlsaXR5X21lc3NhZ2UiOiJPdXIgYWdlbnRzIGFyZSB1bmF2YWlsYWJsZSByaWdodCBub3cuIFNvcnJ5IGFib3V0IHRoYXQsIGJ1dCBwbGVhc2UgbGVhdmUgdXMgYSBtZXNzYWdlIGFuZCB3ZSdsbCBnZXQgcmlnaHQgYmFjay4iLCJkZWZhdWx0X3ByZWNoYXRfbWVzc2FnZSI6IldlIGNhbid0IHdhaXQgdG8gdGFsayB0byB5b3UuIEJ1dCBmaXJzdCwgcGxlYXNlIHRlbGwgdXMgYSBiaXQgYWJvdXQgeW91cnNlbGYuIiwiYWdlbnRfdHJhbnNmZXJlZF9tc2ciOiJZb3VyIGNoYXQgaGFzIGJlZW4gdHJhbnNmZXJyZWQgdG8ge3thZ2VudF9uYW1lfX0iLCJhZ2VudF9yZW9wZW5fY2hhdF9tc2ciOiJ7e2FnZW50X25hbWV9fSByZW9wZW5lZCB0aGUgY2hhdCIsInZpc2l0b3Jfc2lkZV9pbmFjdGl2ZV9tc2ciOiJUaGlzIGNoYXQgaGFzIGJlZW4gaW5hY3RpdmUgZm9yIHRoZSBwYXN0IDIwIG1pbnV0ZXMuIiwiYWdlbnRfZGlzY29ubmVjdF9tc2ciOiJ7e2FnZW50X25hbWV9fSBoYXMgYmVlbiBkaXNjb25uZWN0ZWQiLCJzaXRlX2lkIjoiOWJjODkyNGNlMTBkOTI3OTg1MDdlZjEyYTgxOTI3NzMiLCJhY3RpdmUiOnRydWUsIndpZGdldF9wcmVmZXJlbmNlcyI6eyJ3aW5kb3dfY29sb3IiOiIjNzc3Nzc3Iiwid2luZG93X3Bvc2l0aW9uIjoiQm90dG9tIFJpZ2h0Iiwid2luZG93X29mZnNldCI6IjMwIiwibWluaW1pemVkX3RpdGxlIjoiTGV0J3MgdGFsayEiLCJtYXhpbWl6ZWRfdGl0bGUiOiJDaGF0IGluIHByb2dyZXNzIiwidGV4dF9wbGFjZSI6IllvdXIgTWVzc2FnZSIsIndlbGNvbWVfbWVzc2FnZSI6IkhpISBIb3cgY2FuIHdlIGhlbHAgeW91IHRvZGF5PyIsInRoYW5rX21lc3NhZ2UiOiJUaGFuayB5b3UgZm9yIGNoYXR0aW5nIHdpdGggdXMuIElmIHlvdSBoYXZlIGFkZGl0aW9uYWwgcXVlc3Rpb25zLCBmZWVsIGZyZWUgdG8gcGluZyB1cyEiLCJ3YWl0X21lc3NhZ2UiOiJPbmUgb2YgdXMgd2lsbCBiZSB3aXRoIHlvdSByaWdodCBhd2F5LCBwbGVhc2Ugd2FpdC4iLCJhZ2VudF9qb2luZWRfbXNnIjoie3thZ2VudF9uYW1lfX0gaGFzIGpvaW5lZCB0aGUgY2hhdCIsImFnZW50X2xlZnRfbXNnIjoie3thZ2VudF9uYW1lfX0gaGFzIGxlZnQgdGhlIGNoYXQiLCJhZ2VudF90cmFuc2Zlcl9tc2dfdG9fdmlzaXRvciI6IllvdXIgY2hhdCBoYXMgYmVlbiB0cmFuc2ZlcnJlZCB0byB7e2FnZW50X25hbWV9fSIsImNvbm5lY3RpbmdfbXNnIjoiV2FpdGluZyBmb3IgYW4gYWdlbnQifSwicm91dGluZyI6eyJjaG9pY2VzIjp7Imxpc3QxIjpbIjAiXSwibGlzdDIiOlsiMCJdLCJsaXN0MyI6WyIwIl0sImRlZmF1bHQiOlsiNTAwMDIxMjQ4OCJdfSwiZHJvcGRvd25fYmFzZWQiOiJmYWxzZSJ9LCJwcmVjaGF0X2Zvcm0iOnRydWUsInByZWNoYXRfbWVzc2FnZSI6IldlIGNhbid0IHdhaXQgdG8gdGFsayB0byB5b3UuIEJ1dCBmaXJzdCwgcGxlYXNlIHRlbGwgdXMgYSBiaXQgYWJvdXQgeW91cnNlbGYuIiwicHJlY2hhdF9maWVsZHMiOnsibmFtZSI6eyJ0aXRsZSI6Ik5hbWUiLCJzaG93IjoiMiJ9LCJlbWFpbCI6eyJ0aXRsZSI6IkVtYWlsIiwic2hvdyI6IjIifSwicGhvbmUiOnsidGl0bGUiOiJQaG9uZSBOdW1iZXIiLCJzaG93IjoiMCJ9LCJ0ZXh0ZmllbGQiOnsidGl0bGUiOiJUZXh0ZmllbGQiLCJzaG93IjoiMCJ9LCJkcm9wZG93biI6eyJ0aXRsZSI6IkRyb3Bkb3duIiwic2hvdyI6IjAiLCJvcHRpb25zIjpbImxpc3QxIiwibGlzdDIiLCJsaXN0MyJdfX0sImJ1c2luZXNzX2NhbGVuZGFyIjpudWxsLCJub25fYXZhaWxhYmlsaXR5X21lc3NhZ2UiOnsidGV4dCI6Ik91ciBhZ2VudHMgYXJlIHVuYXZhaWxhYmxlIHJpZ2h0IG5vdy4gU29ycnkgYWJvdXQgdGhhdCwgYnV0IHBsZWFzZSBsZWF2ZSB1cyBhIG1lc3NhZ2UgYW5kIHdlJ2xsIGdldCByaWdodCBiYWNrLiIsInRpY2tldF9saW5rX29wdGlvbiI6IjAiLCJjdXN0b21fbGlua191cmwiOiIifSwicHJvYWN0aXZlX2NoYXQiOmZhbHNlLCJwcm9hY3RpdmVfdGltZSI6MTUsInNpdGVfdXJsIjoic2V1cGRhdGUuZnJlc2hkZXNrLmNvbSIsImV4dGVybmFsX2lkIjpudWxsLCJkZWxldGVkIjpmYWxzZSwib2ZmbGluZV9jaGF0Ijp7InNob3ciOiIxIiwiZm9ybSI6eyJuYW1lIjoiTmFtZSIsImVtYWlsIjoiRW1haWwiLCJtZXNzYWdlIjoiTWVzc2FnZSJ9LCJtZXNzYWdlcyI6eyJ0aXRsZSI6IkxlYXZlIHVzIGEgbWVzc2FnZSEiLCJ0aGFuayI6IlRoYW5rIHlvdSBmb3Igd3JpdGluZyB0byB1cy4gV2Ugd2lsbCBnZXQgYmFjayB0byB5b3Ugc2hvcnRseS4iLCJ0aGFua19oZWFkZXIiOiJUaGFuayB5b3UhIn19LCJtb2JpbGUiOnRydWUsImNyZWF0ZWRfYXQiOiIyMDE2LTAyLTE4VDA2OjE0OjU3LjAwMFoiLCJ1cGRhdGVkX2F0IjoiMjAxNi0wMi0xOFQxNDoxMzozNi4wMDBaIn0=';</script><!-- Google Code for &#3612;&#3641;&#3657;&#3648;&#3586;&#3657;&#3634;&#3594;&#3617; --><!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup --><script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1004740404;
var google_conversion_label = "8aAJCMS0xV4QtL6M3wM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script><script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script><noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1004740404/?value=1.00&amp;currency_code=THB&amp;label=8aAJCMS0xV4QtL6M3wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body></html>