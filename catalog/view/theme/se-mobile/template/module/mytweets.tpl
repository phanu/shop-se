<div id="twitter-ticker">
	<div id="top-bar">
		<div id="twitIcon"><img src="<?php echo $tweet_icon;?>" width="64" height="64" alt="Twitter icon" /></div>
		<h2 class="tut"><?php echo $heading_title; ?></h2>
	</div>

	<div id="tweet-container" class="tweet-container-<?php echo $module; ?>">
		<?php foreach($tweets as $tweet){ ?>
			<div class="tweet">
				<div class="avatar"><a href="<?php echo $tweet['username_link'];?>" target="blank"><img src="<?php echo $tweet['avatar_image']; ?>"></a></div>
				<div class="user"><a href="<?php echo $tweet['username_link'];?>" target="blank"><?php echo $tweet['username']; ?></a></div>
				<div class="time"><?php echo $tweet['time']; ?></div>
				<div class="txt"><?php echo $tweet['message']; ?></div>
			</div>
		<?php } ?>
	</div>	
	 <div id="scroll"></div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('.tweet-container-<?php echo $module; ?>').jScrollPane();
});
</script>