
asdasdas
<?php if ($custom_footer_status == '1') { ?>
	<?php $str_https = '';
	if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
		$str_https = 's';
	} ?>

<?php if ($this->config->get('custom_footer_hide_default')) { ?>
<script type="text/javascript"><!--
$(document).ready(function(){
	$('#footer').hide();
});
//--></script>
<?php } ?>
<?php if ($hide_columns_default_footer && !$this->config->get('custom_footer_hide_default')) { ?>
<script type="text/javascript"><!--
$(document).ready(function(){
	<?php
	$a = '';

	foreach ($hide_columns_default_footer as $column) {
		$a .= '"' . $column . '",';
	}

	echo'var bs = [' . rtrim($a, ',') . '];';
	?>

	$('#footer .column, #footer .column_small, #footer .grid').each(function (i, e) {
		if ($.inArray("column_" + i, bs) > -1) {
			$(this).hide();
		} else {
			$(this).find('ul li').each(function (j, e) {
				if ($.inArray("item_" + i + "_" + j, bs) > -1) {
					$(this).hide();
				}
			});
		}
	});
});
//--></script>



<?php } ?>
<div id="custom-footer">
	<?php if ($facebook_status == '1' && $facebook_position == 'top') { ?>
    <div id="facebook-footer" class="cf-block cf-facebook cf-facebook-top">
	  <?php if ((!$this->config->get('custom_footer_hide_header')) && (!$this->config->get('cf_facebook_hide_header'))) { ?>
	  <h2><?php echo ($this->config->get('custom_footer_header_icon')) ? '<img src="catalog/view/theme/' . $this->config->get('config_template') . '/image/custom_footer/header_facebook.png" />' : ''; ?><?php echo $facebook_header; ?></h2>
	  <?php } ?>
	  <div>
	    <div class="fb-like-box" data-href="http://www.facebook.com/<?php echo $facebook_id; ?>" data-width="<?php echo $this->config->get('cf_width_box_facebook'); ?>" data-height="<?php echo $this->config->get('cf_height_box_facebook'); ?>" border-color="<?php echo $this->config->get('cf_border_box_facebook'); ?>" data-show-faces="true" data-stream="false" data-header="false"></div>
      </div>
	</div>
    <?php } ?>
	<?php if ($twitter_status == '1' && $facebook_position == 'top') { ?>
    <div id="twitter-footer" class="cf-block cf-twitter">
	  <?php if (!$this->config->get('custom_footer_hide_header')) { ?>
	  <h2><?php echo ($this->config->get('custom_footer_header_icon')) ? '<img src="catalog/view/theme/' . $this->config->get('config_template') . '/image/custom_footer/header_twitter.png" />' : ''; ?><?php echo $twitter_header; ?></h2>
	  <?php } ?>
	  <div id="twitter_update_list"></div>
	  <script type="text/javascript" charset="utf-8" src="<?php echo HTTPS_SERVER; ?>twitter/jquery.tweet.js"></script>
	  <script type="text/javascript">
	  $(document).ready(function() {
		$('#twitter_update_list').tweet({
			modpath: '<?php echo $catalog; ?>/twitter/',
			count: <?php echo $twitter_count; ?>,
			username: '<?php echo $twitter_username; ?>',
			loading_text: 'loading twitter feed...',
			twitter_consumer_key: '<?php echo $twitter_consumer_key; ?>',
			twitter_consumer_secret: '<?php echo $twitter_consumer_secret; ?>',
			twitter_user_token: '<?php echo $twitter_user_token; ?>',
			twitter_user_secret: '<?php echo $twitter_user_secret; ?>'
		});
	  });
	  </script>
	</div>
    <?php } ?>
    <div class="clearfix"></div>
    <div id="about-us-footer" class="cf-block cf-first">
	  <?php if (!$this->config->get('custom_footer_hide_header')) { ?>
      <h2><?php echo ($this->config->get('custom_footer_header_icon')) ? '<img src="catalog/view/theme/' . $this->config->get('config_template') . '/image/custom_footer/header_about.png" />' : ''; ?><?php echo $about_header; ?></h2>
	  <?php } ?>
      <p><?php echo $about_text; ?></p>
	  <?php if ($socials) { ?>
	  <p class="social">
	  <?php foreach ($socials as $social) { $gmaps = (preg_match('/maps.google/i', $social['url'])) ? 'class="gmaps_fancybox iframe"' : 'target="_blank"'; ?>
	    <?php echo'<a href="' . $social['url'] . '" ' . $gmaps . '><img src="catalog/view/theme/' . $this->config->get('config_template') . '/image/custom_footer/social_' . $social['name'] . '.png" title="' . $social['name'] . '" alt="' . $social['name'] . '" /></a>'; ?>
	  <?php } ?>
	  <script type="text/javascript"><!--
	    $('.gmaps_fancybox').fancybox({
			width: <?php echo $gmaps_dimension_w; ?>,
			height: <?php echo $gmaps_dimension_h; ?>,
			'transitionIn'	: 'none',
			'transitionOut'	: 'none',
			autoDimensions: false
	    });
	  //--></script>
	  </p>
	  <?php } ?>
    </div>
  <?php if ($information_status == '1') { ?>
    <div id="information-footer" class="cf-block">
	  <?php if (!$this->config->get('custom_footer_hide_header')) { ?>
	  <h2><?php echo ($this->config->get('custom_footer_header_icon')) ? '<img src="catalog/view/theme/' . $this->config->get('config_template') . '/image/custom_footer/header_information.png" />' : ''; ?><?php echo $information_header; ?></h2>
	  <?php } ?>
	  <?php if ($information_text) { ?>
	  <p><?php echo $information_text; ?></p>
	  <?php } ?>
	  <ul>
	    <?php foreach ($informations as $information) { ?>
	    <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
		<?php } ?>
	  </ul>
	</div>
  <?php } ?>
  <?php if ($contact_status == '1') { ?>
    <style type="text/css">
#contact-footer  #footer-telephone {
	background: url("catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/image/custom_footer/<?php echo $contact_icon; ?>/telephone.png") no-repeat scroll left center transparent;
}
#contact-footer  #footer-email {
	background: url("catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/image/custom_footer/<?php echo $contact_icon; ?>/mail.png") no-repeat scroll left center transparent;
	margin-top: 15px;
}
#contact-footer  #footer-skype {
	background: url("catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/image/custom_footer/<?php echo $contact_icon; ?>/skype.png") no-repeat scroll left center transparent;
	margin-top: 15px;
}
#contact-footer  #footer-fax {
	background: url("catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/image/custom_footer/<?php echo $contact_icon; ?>/fax.png") no-repeat scroll left center transparent;
	margin-top: 15px;
}
    </style>
    <div id="contact-footer" class="cf-block">
	  <?php if (!$this->config->get('custom_footer_hide_header')) { ?>
      <h2><?php echo ($this->config->get('custom_footer_header_icon')) ? '<img src="catalog/view/theme/' . $this->config->get('config_template') . '/image/custom_footer/header_contact.png" />' : ''; ?><?php echo $contact_header; ?></h2>
	  <?php } ?>
	  <?php if ($this->config->get('cf_contact_display') == 'form') { ?>
	  <ul>
        <?php if (!empty($contact_telephone1)) { ?> 
          <li id="footer-telephone"><?php echo $contact_telephone1; ?></li>
        <?php } ?>
		<?php if (!empty($contact_email1)) { ?>
		  <li id="footer-email"><?php echo $contact_email1; ?></li>
		<?php } ?>
		  <li id="footer-contact-form"><textarea name="message"></textarea><input type="text" name="email" value="<?php echo $contact_inputholder; ?>" onfocus="javascript:if(this.value=='<?php echo $contact_inputholder; ?>')this.value='';" onblur="javascript:if(this.value=='')this.value='<?php echo $contact_inputholder; ?>';" /><a onClick="email_sent();"><?php echo $button_sent; ?></a><div id="sent_result"></div></li>
	  </ul>
	  <script language="javascript">
	  function email_sent(){
		$.ajax({
		  type: 'post',
		  url: 'index.php?route=module/custom_footer/sent',
		  dataType: 'html',
		  data: { email: $('#footer-contact-form input[name=email]').val(), message: $('#footer-contact-form textarea[name=message]').val() },
		  success: function (html) {
		    eval(html);
		  }
		}); 
	  }
	  </script>
	  <?php } else { ?>
      <ul>
        <?php if (!empty($contact_telephone1)) { ?> 
          <li id="footer-telephone"><?php echo $contact_telephone1; ?></li>
        <?php } ?>
		<?php if (!empty($contact_telephone2)) { ?> 
		  <li id="footer-telephone2"><?php echo $contact_telephone2; ?></li>
		<?php } ?>
		<?php if (!empty($contact_fax)) { ?>
		  <li id="footer-fax"><?php echo $contact_fax; ?></li>
		<?php } ?>
		<?php if (!empty($contact_email1)) { ?>
		  <li id="footer-email"><?php echo $contact_email1; ?></li>
		<?php } ?>
		<?php if (!empty($contact_email2)) { ?>
		  <li id="footer-email2"><?php echo $contact_email2; ?></li>
		<?php } ?>
		<?php if (!empty($contact_skype)) { ?>
		  <li id="footer-skype"><?php echo $contact_skype; ?></li>
		<?php } ?>
      </ul>
	  <?php } ?>
    </div>
  <?php } ?>
  <?php if ($slider_status == '1') { ?>
    <div id="slider_footer" class="cf-block">
	  <?php if (!$this->config->get('custom_footer_hide_header')) { ?>
      <h2><?php echo ($this->config->get('custom_footer_header_icon')) ? '<img src="catalog/view/theme/' . $this->config->get('config_template') . '/image/custom_footer/header_slider.png" />' : ''; ?><?php echo $slider_header; ?></h2>
	  <?php } ?>
	  <ul class="jcarousel-skin-cf">
	  <?php
		if ($products) {
		  foreach ($products as $product) {
		    echo'<li><div class="name"><a href="' . $product['url'] . '">' . $product['name'] . '</a></div>';

		    if ($product['thumb'])
			  echo'<a href="' . $product['url'] . '"><img src="' . $product['thumb'] . '" alt="' . $product['name'] . '" title="' . $product['name'] . '" /></a>';

		      echo'<div class="cf-cart">' . $product['price'] . ' <a onclick="addToCart(\'' . $product['product_id'] . '\');" class="to-cart"></a></div></li>';
		  }
		} elseif ($banners) {
		  foreach ($banners as $banner) {
		    echo'<li><div class="name"></div><a href="' . $banner['url'] . '"><img src="' . $banner['image'] . '" alt="' . $banner['title'] . '" title="' . $banner['title'] . '" /></a></li>';
		  }
		} elseif ($latest_orders) {
			foreach ($latest_orders as $order) {
				echo'<li style="text-align: left"><b>' . $order['customer'] . '</b><div class="cf-lo-price">' . $order['total'] . '</div><div class="cf-lo-country">' . $order['country'] . '</div>' . $order['zone'] . '';

				echo'<div class="cf-lo-products">';
				$str_products = '';
				$i = 0;

				foreach ($order['products'] as $product) {
					++$i;
					$str_products .= '<a href="' . $this->url->link('product/product', 'product_id=' . $product['product_id']) . '">' . html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8') . '</a>, ';
				}
	
				echo rtrim(trim($str_products), ',');
				echo'</div>';

				echo'<span class="cf-lo-info">' . $order_date . ' ' . $order['date_added'] . '<br />' . sprintf($order_total, $i) . '</span></li>';
			}
		} else {
		  foreach ($texts as $text) {
		    echo'<li><div class="name"></div>';

			if ($text['url']) {
			  echo'<a href="' . $text['url'] . '">';
			}

			echo $text['value'];

			if ($text['url']) {
			  echo'</a>';
			}

			echo'</li>';
		  }
		}
	  ?>
	  </ul>
    </div>
	<script type="text/javascript"><!--
      $('#slider_footer ul').jcarousel({
		  vertical: false,
		  visible: 1,
		  auto: 2,
		  wrap: "both",
		  animation: "slow",
		  scroll: 1
      });

	  $('#slider_footer .jcarousel-skin-cf .jcarousel-clip').css("width", "<?php echo ($this->config->get('cf_width_box') - 75); ?>");
	  $('#slider_footer .jcarousel-skin-cf .jcarousel-item').css("width", "<?php echo ($this->config->get('cf_width_box') - 75); ?>");
    //--></script>
  <?php } ?>
  <div class="clearfix"></div>
  <?php if ($facebook_status == '1' && $facebook_position == 'bottom') { ?>
    <div id="facebook-footer" class="cf-block cf-facebook cf-facebook-bottom">
	  <?php if ((!$this->config->get('custom_footer_hide_header')) && (!$this->config->get('cf_facebook_hide_header'))) { ?>
	  <h2><?php echo ($this->config->get('custom_footer_header_icon')) ? '<img src="catalog/view/theme/' . $this->config->get('config_template') . '/image/custom_footer/header_facebook.png" />' : ''; ?><?php echo $facebook_header; ?></h2>
	  <?php } ?>
	  <div>
        <div class="fb-like-box" data-href="http://www.facebook.com/<?php echo $facebook_id; ?>" data-width="<?php echo $this->config->get('cf_width_box_facebook'); ?>" data-height="<?php echo $this->config->get('cf_height_box_facebook'); ?>" border-color="<?php echo $this->config->get('cf_border_box_facebook'); ?>" data-show-faces="true" data-stream="false" data-header="false"></div>
      </div>
	</div>
  <?php } ?>
  <?php if ($twitter_status == '1' && $facebook_position == 'bottom') { ?>
    <div id="twitter-footer" class="cf-block cf-twitter">
	  <?php if (!$this->config->get('custom_footer_hide_header')) { ?>
	  <h2><?php echo ($this->config->get('custom_footer_header_icon')) ? '<img src="catalog/view/theme/' . $this->config->get('config_template') . '/image/custom_footer/header_twitter.png" />' : ''; ?><?php echo $twitter_header; ?></h2>
	  <?php } ?>
	  <div id="twitter_update_list"></div>
	  <script type="text/javascript" charset="utf-8" src="<?php echo HTTPS_SERVER; ?>twitter/jquery.tweet.js"></script>
	  <script type="text/javascript">
	  $(document).ready(function() {
		$('#twitter_update_list').tweet({
			modpath: '<?php echo $catalog; ?>/twitter/',
			count: <?php echo $twitter_count; ?>,
			username: '<?php echo $twitter_username; ?>',
			loading_text: 'loading twitter feed...',
			twitter_consumer_key: '<?php echo $twitter_consumer_key; ?>',
			twitter_consumer_secret: '<?php echo $twitter_consumer_secret; ?>',
			twitter_user_token: '<?php echo $twitter_user_token; ?>',
			twitter_user_secret: '<?php echo $twitter_user_secret; ?>'
		});
	  });
	  </script>
	</div>
  <?php } ?>
  <div class="clearfix"></div>
</div>
<?php if ($this->config->get('custom_footer_background_shoppica')) { ?>
</div></div>
<?php } ?>
<style type="text/css">
#custom-footer {
    margin: 0 auto;
	width: <?php echo $this->config->get('custom_footer_width_wrap'); ?>px;
}

#custom-footer .cf-block {
	width: <?php echo $this->config->get('cf_width_box'); ?>px;
}

#custom-footer .cf-block.cf-first {
	width: <?php echo $this->config->get('cf_width_box_first'); ?>px;
	margin: 0px;
}

#custom-footer .cf-block.cf-first p {
	text-align: <?php echo $this->config->get('cf_about_alignment'); ?>;
}

#custom-footer .cf-block.cf-facebook {
	width: <?php echo $this->config->get('cf_width_box_facebook'); ?>px;
}

#custom-footer .cf-block.cf-twitter {
	<?php if ($facebook_status == '0') { ?>
	margin: 0px 0px 20px 0px;
	width: <?php echo $this->config->get('cf_width_box_facebook'); ?>px;
	<?php } ?>
}

#custom-footer .cf-block.cf-facebook div {
	width: <?php echo ($this->config->get('cf_width_box_facebook') + 2); ?>px;
	overflow:hidden;
	margin: 0px;
	position: relative;
}
#custom-footer .cf-block.cf-facebook div .fb-like-box {
	background: #f4f4f4;
	width: <?php echo ($this->config->get('cf_width_box_facebook')); ?>px;
	padding: 0px;
	margin: 0px 0px;
}
#fb-root {
	display: none;
}
.fb_iframe_widget, .fb_iframe_widget span, .fb_iframe_widget span iframe[style] {
	width: 100% !important;
}
</style>
<?php if ($facebook_status == '1') { ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_EN/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php } ?>
<?php } ?>

<!-- End Category Section -->
</div>
 <!-- FOOTER -->
    <div class="footer">
      
      <!-- Footer main Section -->
      <div class="footer-main">
        <p>
          <span class="block text-small">โอนผ่านบัญชี / โอนผ่าน ATM / Online Banking <br>
ตัดเงิน Credit Card หรือ ชำระเคาน์เตอร์เซอร์วิส ผ่าน PaySbuy </span>
          <i class="fa fa-cc-amex"></i>
          <i class="fa fa-cc-mastercard"></i>
          <i class="fa fa-credit-card"></i>
          <i class="fa fa-cc-paypal"></i>
          <i class="fa fa-cc-visa"></i>
          <i class="fa fa-google-wallet"></i>
          <i class="fa fa-cc-discover"></i>
          <i class="fa fa-cc-jcb"></i>
        </p>
        <p class="text-small">
          <span class="block text-small">มีปัญหาอะไรไหม? </span>
          089-810-3755 | admin@se-update.com | <a href="#">คำถามที่โดนถามบ่อย</a>
        </p>

        <div class="social-footer">
          <a href="https://www.facebook.com/seupdate/" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
          <a href="https://twitter.com/se_update" target="_blank"  class="twitter"><i class="fa fa-twitter"></i></a>
          <a href="https://plus.google.com/111107825328846974348/" target="_blank"  class="gplus"><i class="fa fa-google-plus"></i></a>
        </div>
      </div>
      <!-- End Footer main Section -->

      <!-- Copyright Section -->
      <div class="copyright">
        <span class="block">&copy; 2016 SE-Update.com mobile</span>
        <div class="navigation">
          <a href="http://shop.se-update.com/วิธีการสั่งซื้อ">วิธีการสั่งซื้อ</a>
          <a href="http://shop.se-update.com/index.php?route=information%2Fpayment">แจ้งชำระเงิน</a>
        </div>
      </div>
      <!-- End Copyright Section -->

    </div>
    <!-- End FOOTER -->

    <!-- Back to top Link -->
    <div id="to-top" class="main-bg"><i class="fa fa-long-arrow-up"></i></div>

  </div>
  <!-- END MAIN PAGE -->

</div><!-- #main -->
<script type="text/javascript" src="catalog/view/theme/se-mobile/js/materialize.min.js"></script>
<script type="text/javascript" src="catalog/view/theme/se-mobile/js/slick.min.js"></script>
<script type="text/javascript" src="catalog/view/theme/se-mobile/js/jquery.slicknav.js"></script>
<script type="text/javascript" src="catalog/view/theme/se-mobile/js/jquery.swipebox.js"></script>
<script type="text/javascript" src="catalog/view/theme/se-mobile/js/custom.js"></script>

</body>
</html>