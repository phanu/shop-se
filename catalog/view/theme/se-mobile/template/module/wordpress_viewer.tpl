<?php $is_vertical=($params['position']=='column_left' OR $params['position']=='column_right'); ?>
<div class="box presscart">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
  <?php $ceil_with=round(100/count($posts['content']))?>
  <table width="100%" class="category-info" style="margin: 0;">
    <tr valign="top">
    <?php  foreach($posts['content'] as $post) { ?> 
        <td <?php echo (!$is_vertical)?'style="padding: 0 20px;"':''?> <?php echo (!$is_vertical)?'width="'.$ceil_with.'%"':''?>>
            <?php  echo ($params['post_title']>0)?('<h2><a href="'.$posts['site_url'].'/?p='.$post['ID'].'">'.substr($post['post_title'],0,$params['post_title']).'</a></h2>'):''?>
            <?php if (($params['thumbnail_width']>0) AND ($params['thumbnail_height']>0) AND (preg_match('/<img.*?src="(.*?)".*?\/?>/si',$post['post_content'],$temp_data)>0)){?>
                <p class="pc-excerpt"><a href="<?= $posts['site_url']?>/?p=<?= $post['ID']?>"><img src="<?php echo $temp_data[1]?>" width="<?php echo $params['thumbnail_width']?>" height="<?php echo $params['thumbnail_height']?>" <?php echo (!$is_vertical)?'class="image"':'class="image"'?>></a>
                <?php echo ($is_vertical)?'':''?>
            <?php } ?>
            <?php if ($params['post_excerpt']>0){ echo substr(preg_replace('/\[.*?\]/si','',strip_tags($post['post_content'])),0,$params['post_excerpt']).'...'; }?></p>
            <div style="clear: both;"></div>
            <p class="pc-read"><a href="<?= $posts['site_url']?>/?p=<?= $post['ID']?>"><?php echo $text_read_article?></a></p>
        <?php echo ($is_vertical)?'<hr />':''?>
        </td><?php echo ($is_vertical)?'</tr><tr valign="top">':''?>
        <?php } ?>
    </tr>
  </table>
  </div>
</div>