<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content"><div class="box-product">
  	<div id="ocs_featured_carousel_content<?php echo $module_count; ?>">
  		<ul id="ocs_featured_carousel<?php echo $module_count; ?>" class="jcarousel-skin-opencartsoft">
      <?php foreach ($products as $product) { ?>
      <li>
      <div>
        <?php if ($product['thumb']) { ?>
        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
        <?php } ?>
        <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
        <?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
            <s><?php echo $product['price']; ?></s> <b><?php echo $product['special']; ?></b>
          <?php } ?>
        </div>
        <?php } ?>
        <?php if ($product['rating']) { ?>
        <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
        <?php } ?>
        <div class="cart"><a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button"><span><?php echo $button_cart; ?></span></a></div>
      </div>
      </li>
      <?php } ?>
      </ul>
    </div></div>
  </div>
</div>
<script type="text/javascript"><!--
<?php if($scroll_auto) { ?>
function featured_carousel_initCallback<?php echo $module_count; ?> (carousel)
{
    // Disable autoscrolling if the user clicks the prev or next button.
    carousel.buttonNext.bind('click', function() {
        carousel.startAuto(0);
    });

    carousel.buttonPrev.bind('click', function() {
        carousel.startAuto(0);
    });

    // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function() {
        carousel.stopAuto();
    }, function() {
        carousel.startAuto();
    });
};
<?php } ?>

<?php if($effect == 'easing') { ?>
jQuery.easing['BounceEaseOut'] = function(p, t, b, c, d) {
	if ((t/=d) < (1/2.75)) {
		return c*(7.5625*t*t) + b;
	} else if (t < (2/2.75)) {
		return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
	} else if (t < (2.5/2.75)) {
		return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
	} else {
		return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
	}
};
<?php } ?>

jQuery(document).ready(function() {
  jQuery('#ocs_featured_carousel<?php echo $module_count; ?>').jcarousel({    		
    vertical: <?php echo ($axis == 'horizontal' ? 'false' : 'true'); ?>,
    visible: <?php echo $scroll_limit; ?>,
    scroll: <?php echo $scroll; ?>,
    <?php if($effect == 'easing') { ?>
    	easing: 'BounceEaseOut',
    	animation: 1000,
    <?php } ?>
    <?php if($scroll_auto) { ?>
    auto: 3,
    wrap: 'last',
    initCallback: featured_carousel_initCallback<?php echo $module_count; ?>
    <?php } ?>
  });
});
//--></script>