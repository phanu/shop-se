<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
    <div id="fb-wall-<?php echo $module; ?>" class="<?php echo $fw_only_owner; ?> <?php echo $fw_only_owner_left; ?>"></div>
  </div>
</div>

<script type="text/javascript">
$(function(){
	$('#fb-wall-<?php echo $module; ?>').fbWall({
		id: '<?php echo $id; ?>',
		accessToken: '<?php echo $access_token; ?>',
		max: <?php echo $max; ?>,
		<?php if ($show_guest_entries) { ?>
		showGuestEntries: true,
		<?php } else { ?>
		showGuestEntries: false,
		<?php } ?>
		<?php if ($show_comments) { ?>
		showComments: true,
		<?php } else { ?>
		showComments: false,
		<?php } ?>		
		timeConversion: <?php echo $time_conversion; ?>,
		translateAt : '<?php echo addslashes($text_at); ?>',
		translateLikeThis : '<?php echo addslashes($text_like); ?>',
		translateLikesThis : '<?php echo addslashes($text_likes); ?>',
		translateErrorNoData : '<?php echo addslashes($text_no_data); ?>',
		translatePeople : '<?php echo addslashes($text_people); ?>'
	});
});


function startFBWallSlider(){	
	$('#fb-wall-<?php echo $module; ?>').jScrollPane();
}	

</script>