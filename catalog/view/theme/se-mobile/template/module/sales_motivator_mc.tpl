<div class="sm-user-area">
	<div id="sm-user-notification"></div>
	<div class="sm-user-actions">
		<div class="sm-action">
			<i class="sm-radius">1</i> <?php echo $text_share_product; ?>
			<a class="sm-action-button" id="sm-share"><?php echo $button_share_now;?></a>
		</div>
		<div class="sm-action">
			<i class="sm-radius">2</i> <?php echo $text_invite_fb_friend; ?>
			<a class="sm-action-button" id="sm-invite-fb"><?php echo $button_invite_friend; ?></a>
		</div>	
		<?php if ($fb_fan_page) { ?>
		<div class="sm-action">
			<i class="sm-radius">3</i> <?php echo $text_like_fan_page; ?>
			<div class="fb-like sm-fan-right" data-href="<?php echo $fb_fan_page; ?>" data-width="70" data-layout="button_count" data-show-faces="false" data-send="false"></div>
		</div>	
		<?php } ?>
		<div class="sm-action center invite-link">
			<strong><?php echo $text_your_invite_link; ?></strong><br />
			<textarea id="ta-invite-link" readonly><?php echo $invite_link; ?></textarea>
		</div>
	</div>
		
</div>

<div class="sm-invite-friends">
<!-- AddThis Button BEGIN -->
	<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
	<a class="addthis_button_twitter" addthis:url="<?php echo $invite_link; ?>"></a>
	<a class="addthis_button_pinterest_share" addthis:url="<?php echo $invite_link; ?>"></a>
	<a class="addthis_button_google_plusone_share" addthis:url="<?php echo $invite_link; ?>"></a>
	<a class="addthis_button_myspace" addthis:url="<?php echo $invite_link; ?>"></a>
	<a class="addthis_button_linkedin" addthis:url="<?php echo $invite_link; ?>"></a>
	<a class="addthis_button_yahoomail" addthis:url="<?php echo $invite_link; ?>"></a>
	<a class="addthis_button_hotmail" addthis:url="<?php echo $invite_link; ?>"></a>
	<a class="addthis_button_aolmail" addthis:url="<?php echo $invite_link; ?>"></a>
	<a class="addthis_button_email" addthis:url="<?php echo $invite_link; ?>"></a>
	<a class="addthis_button_more" addthis:url="<?php echo $invite_link; ?>"></a>
	</div>
<!-- AddThis Button END -->
</div>

<script type="text/javascript">
	var sm_at_script = 'https://s7.addthis.com/js/300/addthis_widget.js#domready=1';
	
	if (window.addthis) {
		window.addthis = null;
		window._adr = null;
		window._atc = null;
		window._atd = null;
		window._ate = null;
		window._atr = null;
		window._atw = null;
	}
	
	$.getScript(sm_at_script);
</script> 

<script type="text/javascript">
$('#sm-share').bind('click', function(){
	var publish = {
		method: 'feed',
		message: '',
		name: '<?php echo addslashes($product_name . ' - ' . $product_price); ?>',
		caption: '',
		description: ('<?php echo addslashes($product_description); ?>'),
		link: '<?php echo $invite_link; ?>', 
		picture: '<?php echo $product_image; ?>',
		actions: [
					{ name: '<?php echo addslashes($product_name . ' - ' . $product_price); ?>', link: '<?php echo $invite_link; ?>' }
		         ],
		user_message_prompt: ''
	};
	
	FB.ui(publish, checkShareStatus);
});

$('#sm-invite-fb').bind('click', function(){
	
	FB.ui({
		method: 'send',
		name: '<?php echo addslashes($product_name . ' - ' . $product_price); ?>',
		description: ('<?php echo addslashes($product_description); ?>'),
		link: '<?php echo $invite_link; ?>', 
		picture: '<?php echo $product_image; ?>',
	 }, checkInviteStatus);
	
});

function checkShareStatus(response){
	if (response && response.post_id) {
		
		$.ajax({
			type: 'POST',
			url: 'index.php?route=module/sales_motivator/checkreward',
			data: response,
			dataType: 'json',
			success: function(json){
				$('.success, .attention').remove();
				
				if (json['success']){
					$('#sm-user-notification').html('<div class="success">' + json['success'] + '</div>');
					$('#sm-user-notification').hide();
					$('#sm-user-notification').slideDown();
				}

				if (json['error']){
					$('#sm-user-notification').html('<div class="attention">' + json['error'] + '</div>');
					$('#sm-user-notification').hide();
					$('#sm-user-notification').slideDown();
				}		
			}
		});
	 } 
}

function checkInviteStatus(response){
	if (response) {
	
		$.ajax({
			type: 'POST',
			url: 'index.php?route=module/sales_motivator/invitefriend',
			data: response,
			dataType: 'json',
			success: function(json){
				$('.success, .attention').remove();
				
				if (json['success']){
					$('#sm-user-notification').html('<div class="success">' + json['success'] + '</div>');
					$('#sm-user-notification').hide();
					$('#sm-user-notification').slideDown();
				}	
			}
		});
	}	
}
</script>