<?php if($data['customPageBackground']['Enabled'] != 'false'): ?>
	<!-- START customPageBackground -->
    <div id="customPageBackgroundWrapper">
  
    </div>
	<script language="javascript" type="text/javascript">
	
	</script>
    <style type="text/css">
	<?php foreach($data['customPageBackground']['backgrounds'] as $background) : ?>
		<?php if (utf8_strtolower($background['url']) == utf8_strtolower((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https://' : 'http://').$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'])) : ?>
			body {
				<?php if (!empty($background['image'])) : ?>
				background-image: url('./image/<?php echo $background['image']; ?>');
				<?php endif; ?>
				<?php if (!empty($background['color'])) : ?>
				background-color: <?php echo $background['color']; ?>;
				<?php endif; ?>
				<?php if (!empty($background['attachment'])) : ?>
				background-attachment: <?php echo $background['attachment']; ?>;
				<?php endif; ?>
				<?php if (!empty($background['position']['x']) || !empty($background['position']['y'])) : ?>
				background-position: <?php echo $background['position']['x'].' '.$background['position']['y']; ?>;
				<?php endif; ?>
				<?php if (!empty($background['repeat'])) : ?>
				background-repeat: <?php echo $background['repeat']; ?>;
				<?php endif; ?>
				<?php if (!empty($background['clip'])) : ?>
				background-clip: <?php echo $background['clip']; ?>;
				<?php endif; ?>
				<?php if (!empty($background['origin'])) : ?>
				background-origin: <?php echo $background['origin']; ?>;
				<?php endif; ?>
			}
		<?php endif; ?>
	<?php endforeach; ?>
	</style>
	<!-- END customPageBackground -->
<?php endif; ?>