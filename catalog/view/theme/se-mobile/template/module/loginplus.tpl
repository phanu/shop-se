<?php if (!$logged) { ?>
    
<div class="box">
 <div class="box-heading"><?php echo $heading_title_1; ?></div>
  <div class="box-content">
    
          
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="module_loginplus">
     
              <div style="text-align: center;"><?php echo $text_welcome; ?><?php echo $text_guest; ?></div><br />
              
	      
	      <b><?php echo $entry_email_address; ?></b>
              <input type="text" name="email" />
              <br />
              <br />
              
              <b><?php echo $text_password; ?></b>
              <input type="password" name="password" />
              <br /> <br />
              <span class="clear"></span>
              
              <div style="float:left; text-align: right;"><a onclick="$('#module_loginplus').submit();" class="button"><span><?php echo $text_login; ?></span></a></div>
               <br /><br />  <br />
<?php if (!$this->customer->isLogged()) { ?>
<a class="ocx-facebook-login-trigger ocx-stay-here ocx-fbl-button ocx-rounded ocx-icon" href="javascript:void(0);"><?php echo $this->config->get('facebook_login_button_name_' . $this->config->get('config_language_id')); ?></a>
<?php } ?>
              <br /><br />
            <ul>
              
              <li><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></li>
              <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
           </ul>
          </form> 
         
         <script type="text/javascript"><!--
  $('#module_loginplus input').keydown(function(e) {
    if (e.keyCode == 13) {
      $('#module_loginplus').submit();
    }
  });
  //--></script>
          
  </div>
</div>

<?php } ?> 
           
<?php if ($logged) { ?>
 <div class="box">
   <div class="box-heading"><?php echo $heading_title_2; ?></div>
       <div class="box-content">
            <div style="text-align: center;"><?php echo $text_greeting; ?><br />

	    ( <a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a> )</div>
                <br />
		
 <br />

                <b><?php echo $text_my_account; ?></b>
		
                <ul>
                <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
		 <li><a href="<?php echo $address; ?>"><?php echo $text_edit_address; ?></a></li>
                <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
                <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                </ul>
                <br />
                <b><?php echo $text_my_orders; ?></b>
                <ul>
                <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
                
              
                <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
                </ul>
                <br />
                <b><?php echo $text_my_newsletter; ?></b>
                <ul>
                <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
                </ul>
   </div>
</div>                            
 
 <?php } ?>
 
 