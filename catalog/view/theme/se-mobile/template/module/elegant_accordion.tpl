<div class="slideshow" style="width:<?php echo $accordion_width;?>px; height:<?php echo $accordion_height;?>px;">
	<ul class="accordion" id="accordion-<?php echo $module; ?>" style="height:<?php echo $image_height; ?>px;">
	<?php foreach($sections as $section){ ?>	
		<li style="background-image: url('<?php echo $section['image']; ?>'); height:<?php echo $image_height; ?>px; width:<?php echo $partial_view; ?>px;">
			<?php if ($section['title']) { ?>
			<div class="heading"><?php echo $section['title']; ?></div>
			<?php } ?>
			
			<?php if ($section['title'] || $section['description'] != "..."){ ?>
			<div class="bgDescription" style="height:<?php echo $description_bg_height;?>px;"></div>
			<?php } ?>
			<div class="description" style="width:<?php echo $image_width;?>px; height:<?php echo $description_height;?>px;">
				<?php if ($section['title']) { ?>
				<h2 style="font-size:<?php echo $h2_font_size; ?>px;"><?php echo $section['title']; ?></h2>
				<?php } ?>
				
				<?php if ($section['description'] != "..."){ ?>
				<p><?php echo $section['description']; ?></p>
				<?php } ?>
				
				<?php if ($section['link']) { ?>
				<a href="<?php echo $section['link']; ?>"><?php echo $text_more; ?> &rarr;</a>
				<?php } ?>
			</div>
		</li>
	<?php } ?>	
	</ul>
</div>

	
<script type="text/javascript">
 $(document).ready(function() {
    $('#accordion-<?php echo $module; ?>').kwicks({
		max : <?php echo $image_width; ?>,
		duration: 700
	});
 });
</script>			