<div id="owlslider<?php echo $module; ?>" class="owlslider">
  <?php foreach ($banners as $banner) { ?>
  <?php if ($banner['link']) { ?>
  <div><a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" title="<?php echo $banner['title']; ?>" /></a></div>
  <?php } else { ?>
  <div><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" title="<?php echo $banner['title']; ?>" /></div>
  <?php } ?>
  <?php } ?>
</div>


<script type="text/javascript"><!--

$(document).ready(function() {
 
  $("#owlslider<?php echo $module; ?>").owlCarousel({
    navigation : true,
    singleItem : true,
	responsive : true,
	autoHeight : false,
	navigationText:	["<?php echo $text_prev; ?>","<?php echo $text_next ; ?>"],	
	pagination : <?php echo $pagination; ?>,
	navigation : <?php echo $navigation; ?>,
	autoPlay   : <?php echo $autoplay; ?>,
    transitionStyle : "<?php echo $transition; ?>",
  });
 
});

//--></script>

<style>
.owlslider > div img {
    display: block;
    width: 100%;
    height: auto;
}
</style>