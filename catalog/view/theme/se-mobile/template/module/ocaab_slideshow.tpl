<?php if ($banners) { ?>
	<style type="text/css">
		.oca-clearfix:after { content: "."; display: block;	clear: right; visibility: hidden; line-height: 0; height: 0; }
		.oca-clearfix {	/*display: inline-block;*/ }
		html[xmlns] .oca-clearfix {	display: block; }
		* html .oca-clearfix { height: 1%; }
	</style>
	<div class="slideshow oca-clearfix" style="margin-bottom: 10px;">
		<?php $x = 1; ?>
		<?php foreach ($banners as $banner) { ?>
			<div id="slideshow-<?php echo $module . '-' . $x; ?>" style="float: <?php echo $banner['float']; ?>; clear: <?php echo $banner['clear']; ?>; margin-left: <?php echo $banner['margin_left']; ?>px; margin-right: <?php echo $banner['margin_right']; ?>px; margin-top: <?php echo $banner['margin_top']; ?>px; margin-bottom: <?php echo $banner['margin_bottom']; ?>px; width: <?php echo $banner['width']; ?>px; height: <?php echo $banner['height']; ?>px;">
				<?php foreach ($banner['banners'] as $key => $result) { ?>
					<?php if ($result['link']) { ?>
						<div><a href="<?php echo $result['link']; ?>"><img src="<?php echo $result['image']; ?>" alt="<?php echo $result['title']; ?>" title="<?php echo $result['title']; ?>" /></a></div>
					<?php } else { ?>
						<div><img src="<?php echo $result['image']; ?>" alt="<?php echo $result['title']; ?>" title="<?php echo $result['title']; ?>" /></div>
					<?php } ?>
				<?php } ?>
			</div>
			<?php $x++; ?>
		<?php } ?>
	</div>
	
	<script type="text/javascript"><!--
		<?php $x = 1; ?>
		<?php foreach ($banners as $banner) { ?>
			$(document).ready(function() {
				$('#slideshow-<?php echo $module . '-' . $x; ?>').nivoSlider({
					effect: '<?php echo $slideshow_settings['effect']; ?>',
					animSpeed: <?php echo $slideshow_settings['animSpeed']; ?>,
					pauseTime: <?php echo $slideshow_settings['pauseTime']; ?>,
					startSlide: <?php echo $slideshow_settings['startSlide']; ?>,
					directionNav: <?php echo $slideshow_settings['directionNav']; ?>,
					controlNav: <?php echo $slideshow_settings['controlNav']; ?>,
					pauseOnHover: <?php echo $slideshow_settings['pauseOnHover']; ?>,
					manualAdvance: <?php echo $slideshow_settings['manualAdvance']; ?>,
					prevText: '<?php echo $slideshow_settings['prevText']; ?>',
					nextText: '<?php echo $slideshow_settings['nextText']; ?>',
					randomStart: <?php echo $slideshow_settings['randomStart']; ?>
				});
			});
			<?php $x++; ?>
		<?php } ?>
	//--></script>
<?php } ?>