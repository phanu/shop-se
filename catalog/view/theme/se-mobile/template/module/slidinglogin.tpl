<?php
// -------------------------------------------
// Sliding Login for OpenCart v1.5.1.x
// By Best-Byte
// www.best-byte.com
// -------------------------------------------
?>
<style type="text/css" media="screen">
.slide-out {
  height: <?php echo $height; ?>px;
  width: <?php echo $width; ?>px;
  background: #<?php echo $color; ?>;
  border: #<?php echo $border; ?>;
  padding: <?php echo $padding; ?>px;
  z-index: 999999;
}
.handle {
  z-index: 999999;
}
textarea {
  resize: none;
}
</style>
<script> 
  $(document).ready(function(){
  $('.slide-out').tabSlideOut({
        tabHandle: '.handle',                            
        pathToTabImage: 'catalog/view/theme/default/image/sliding-tabs/<?php echo $image; ?>',
        imageHeight: '<?php echo $image_height; ?>px',    
        imageWidth: '<?php echo $image_width; ?>px',       
        tabLocation: '<?php echo $location; ?>',         
        speed: <?php echo $speed; ?>,                     
        <?php if ($trigger) { ?>action: 'click', <?php } else { ?>action: 'hover', <?php } ?>              
        topPos: '<?php echo $from_top; ?>px',             
        leftPos: '<?php echo $from_left; ?>px',           
        <?php if ($fixed) { ?>fixedPosition: true, <?php } else { ?>fixedPosition: false, <?php } ?>          
        <?php if ($loadout) { ?>onLoadSlideOut: true <?php } else { ?>onLoadSlideOut: false <?php } ?>
  });
});
</script>
<?php if ($display1) { ?>
<div class="slide-out">
<div align="center">
<a class="handle" href="./index.php?route=account/login">Login</a>
<form id="login2" enctype="multipart/form-data" method="post" action="./index.php?route=account/login">
<b>Email Address:</b>
<br />
<input type="text" value="" name="email" class="">
<br>
<b>Password:</b>
<br />
<input type="password" value="" name="password" class="">
<br>
<br />
<a href="./index.php?route=account/forgotten">Forgotten Password</a>
<br />
<br />
<a href="./index.php?route=account/register">Create an Account</a>
<br />
<br />
<br />
<a class="button" onclick="$('#login2').submit();"><span>Login</span></a>
</form> 
</div></div> 
<script type="text/javascript">
<!--
$('#login2 input').keydown(function(e) {
if (e.keyCode == 13) {
$('#login2').submit();
}
});
//-->
</script>
<?php } else { ?>
<div class="slide-out">
<a class="handle" href="./index.php?route=information/contact">Contact</a>
<br/><div align="right"><a href="http://shop.se-update.com/index.php?route=information/faq_system" target="_blank"><h2>คำถามที่ถูกถามบ่อย คลิ๊กที่นี่</h2></a></div> 
<br/>
<form action="./index.php?route=information/contact" method="post" enctype="multipart/form-data" id="contact1">

    <div class="content">
    <b>ชื่อ:</b><br />
    <input type="text" name="name" size="30" maxlength="30" value="" />
    <br />
    <br />
    <b>อีเมล:</b><br />
    <input type="text" name="email" size="30" maxlength="30" value="" />
    <br />
    <br />
    <b>ข้อความ</b><br />
    <textarea name="enquiry" cols="10" rows="7" style="width: 95%;"></textarea>
    <br />
    <br />
    <b>กรุณากรอกรหัสยืนยันในกล่องข้างล่าง:</b><br />
    <input type="text" name="captcha" value="" />
    <br />
    <img src="index.php?route=information/contact/captcha" alt="" /> 
    </div>
    <div class="buttons">
      <div style="text-align: center;"><a onclick="$('#contact1').submit();" class="button"><span>ส่ง</span></a></div>
    </div>
  </form>
</div> 
<?php } ?>