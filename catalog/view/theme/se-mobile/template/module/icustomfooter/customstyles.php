<style type="text/css" rel="stylesheet">
	.iCustomFooter ul>li.grid_footer_3,
	#icustomfooter_default_footer .column { width:<?php echo $idata['Settings']['ColumnWidth']?>px; }
	.iWidgets .iWidget .belowTitleContainer { height:<?php echo ($idata['Settings']['ColumnHeight']-70)?>px; }
	.iWidgets .iContactForm input[type="submit"] {
		<?php if ($idata['Settings']['BackgroundPattern'] == 'usebackgroundcolor') : ?>
		padding: 2px 8px;
		<?php echo (!empty($idata['Settings']['BackgroundColor'])) ? 'color:'.$idata['Settings']['BackgroundColor'].';' : ''; ?>
		<?php echo (!empty($idata['Settings']['ColumnColor'])) ? 'background-color:'.$idata['Settings']['ColumnColor'].';' : ''; ?>
		border: 1px solid <?php echo (!empty($idata['Settings']['BackgroundColor'])) ? $idata['Settings']['BackgroundColor'] : ''; ?>;
		outline: 2px solid <?php echo (!empty($idata['Settings']['ColumnColor'])) ? $idata['Settings']['ColumnColor'] : ''; ?>;
		font-family: Trebuchet MS;
		font-size: 12px;
		margin-top: 5px;
		<?php endif; ?>
	}
	#powered { <?php echo $idata['Settings']['HidePoweredBy']?> }
	.iCustomFooter { width:<?php echo $idata['Settings']['FooterWidth']?>px; }
	.iCustomFooter .iWidgets .iWidget h2 {
		<?php echo (!empty($idata['Settings']['ColumnColor']) && $idata['Settings']['BackgroundPattern'] == 'usebackgroundcolor') ? 'color:'.$idata['Settings']['ColumnColor'].'; border-bottom: 1px ' . $idata['Settings']['ColumnLineStyle'] . ' '.$idata['Settings']['ColumnColor'].';' : '' ?>
	}
	.iCustomFooter {
		<?php echo (!empty($idata['Settings']['BackgroundColor']) && $idata['Settings']['BackgroundPattern'] == 'usebackgroundcolor') ? 'background-color:'.$idata['Settings']['BackgroundColor'].';' : ''; ?>
		<?php echo (!empty($idata['Settings']['TextColor']) && $idata['Settings']['BackgroundPattern'] == 'usebackgroundcolor') ? 'color:'.$idata['Settings']['TextColor'] : ''.';' ?>
	}
</style>