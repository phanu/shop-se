<div class="box">
	<div class="box-heading"><?php echo $heading_title; ?></div>
	<div class="box-content">
		<table>
			<?php if ($open_orders) { ?>
				<?php foreach ($open_orders as $order) { ?>
					<tr>
						<td><a style="text-decoration: none; font-weight: bold; color: red;" href="<?php echo $order['href']; ?>">#<?php echo $order['order_id']; ?> - <?php echo $order['order_status']; ?>&nbsp;&nbsp;<?php echo $order['balance']; ?></a></td>
					</tr>
				<?php } ?>
			<?php } else { ?>
				<tr>
					<td><?php echo $text_no_open_orders; ?></td>
				</tr>
			<?php } ?>
		</table>
		<table style="border-bottom: 2px dotted #000 !important; margin-top: 10px !important; margin-bottom: 10px !important; width: 100% !important;"><tr><td></td></tr></table>
		<table>
			<?php if ($pending_orders) { ?>
				<?php foreach ($pending_orders as $order2) { ?>
					<tr>
						<td><a style="text-decoration: none; font-weight: bold; color: orange;" href="<?php echo $order2['href']; ?>">#<?php echo $order2['order_id']; ?> - <?php echo $order2['order_status']; ?>&nbsp;&nbsp;</a></td>
					</tr>
				<?php } ?>
			<?php } else { ?>
				<tr>
					<td><?php echo $text_no_pending_orders; ?></td>
				</tr>
			<?php } ?>
		</table>
		<table style="border-bottom: 2px dotted #000 !important; margin-top: 10px !important; margin-bottom: 10px !important; width: 100% !important;"><tr><td></td></tr></table>
		<table>
			<?php if ($closed_orders) { ?>
				<?php foreach ($closed_orders as $order3) { ?>
					<tr>
						<td><a style="text-decoration: none; font-weight: bold; color: black;" href="<?php echo $order3['href']; ?>">#<?php echo $order3['order_id']; ?> - <?php echo $order3['order_status']; ?>&nbsp;&nbsp;</a></td>
					</tr>
				<?php } ?>
			<?php } else { ?>
				<tr>
					<td><?php echo $text_no_closed_orders; ?></td>
				</tr>
			<?php } ?>
		</table>
	</div>
</div>
