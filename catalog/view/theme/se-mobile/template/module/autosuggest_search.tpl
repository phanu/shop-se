<div id="popupAutoSuggest">
	<a id="popupAutoSuggestClose"></a>
	<div id="popupAutoSuggestContent"></div>
</div>

<script type="text/javascript">
function OCXAutosuggestSearch(){
	var current_obj = $('<?php echo addslashes($selector); ?>');
	
	$.ajax({
		url: 'index.php?route=module/autosuggest_search/search',
		data: 'filter_name=' + encodeURIComponent($(current_obj).val()),
		success: function(data){
			$('#popupAutoSuggestContent').html(data);
			
			customSetLocationAutoSPopup(current_obj);
			
			if (autoSPopupStatus == 0  ){  
				loadAutoSPopup();
			}	
		}
	});
}

$(document).ready(function(){
	$('<?php echo addslashes($selector); ?>').typeWatch({
		callback: OCXAutosuggestSearch,
		wait: 750,
		highlight: false,
		captureLength: <?php echo $chars_start; ?>
	});

	$('<?php echo addslashes($selector); ?>').bind('keyup', function(){
		if ( ($(this).val().length < <?php echo $chars_start; ?>) && autoSPopupStatus ){
			disableAutoSPopup();
		}
	});
	
	$("#popupAutoSuggestClose").click(function(){
		disableAutoSPopup();
	});

	//Press Escape event!
	$(document).keypress(function(e){
		if(e.keyCode==27 && autoSPopupStatus==1){
			disableAutoSPopup();
		}
	});

});
</script>
