<?php if ($products) { ?>
<style type="text/css">
  .counter_desc_lr {
    margin-bottom:5px;
  }  
  .counter_desc_lr .cntSeparator {
	font-size: 13px;
	margin: 5px auto;
	height: <?php echo $digit_height;?>
	color: #000;
  }
  .counter_desc_lr .desc { margin: 7px 3px; } 
  .counter_desc .desc div {
	float: left;
	font-family: Arial;
	width: 70px;
	margin-right: 65px;
	font-size: 13px;
	font-weight: bold;
	color: #000;
  }
</style>

<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
    <div class="box-product" id="special_countdown_<?php echo $module; ?>">
      <?php foreach ($products as $product) { ?>
      <div style="padding:0px; margin: 0px;">
	    <?php if ($product['show_special_countdown']){ ?>
			<div class="counter_desc_lr" id="counter_<?php echo $module;?>_<?php echo $product['product_id']; ?>"></div>
		<?php } ?>
        <?php if ($product['thumb']) { ?>
        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
        <?php } ?>
        <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
        <?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
          <?php } ?>
        </div>
        <?php } ?>
        <?php if ($product['rating']) { ?>
        <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
        <?php } ?>
        <div class="cart"><a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button"><span><?php echo $button_cart; ?></span></a></div>
		
      </div>
      <?php } ?>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  <?php foreach($products as $product){ ?>
    <?php if ($product['show_special_countdown']) { ?>  
	  $('#counter_<?php echo $module; ?>_<?php echo $product['product_id'];?>').countdown({
		  image: '<?php echo $digit_image; ?>',
		  digitWidth: <?php echo $digit_width; ?>,
		  digitHeight: <?php echo $digit_height; ?>,
		  format: '<?php echo $product['countdown_format']; ?>',
		  <?php if ($product['show_days']) { ?>
		  startTime: '<?php echo $product['days']; ?>:<?php echo $product['hours'];?>:<?php echo $product['mins']; ?>:<?php echo $product['secs']; ?>'
		  <?php } else { ?>
		  startTime: '<?php echo $product['hours'];?>:<?php echo $product['mins'] ?>:<?php echo $product['secs']; ?>'
		  <?php } ?>	
		});
	<?php } ?>	
  <?php } ?>	
});
</script>


<script type="text/javascript">
  $(document).ready(function(){
    $('#special_countdown_<?php echo $module; ?>').bxSlider({
		mode: '<?php echo $transition; ?>',
		speed: '500',
		pause: '5000',
		infiniteLoop: true,
		controls: false,
		auto: true,
		autoDirection: '<?php echo $slide_direction; ?>',
		autoHover: true,
		displaySlideQty: <?php echo $show_once; ?>,
		moveSlideQty: <?php echo $slide_step; ?>
	});
  });
</script>
<?php } ?>


