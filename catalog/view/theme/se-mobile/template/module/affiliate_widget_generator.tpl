<div id="awg" class="awg-form">
  
  <div id="configuration">
	<div class="configuration-left">
		<table class="form" style="margin-bottom: 0px;">
			<tr>
				<td class="center"><span class="required">* </span><?php echo $entry_tracking_code; ?></td>
				<td><input type="text" name="tracking_code" value="<?php echo $tracking_code; ?>" readonly /></td>
			</tr>
			<tr>	
				<td class="center"><span class="required">* </span><?php echo $entry_widget_dimension; ?></td>
				<td><input type="text" name="widget_width" value="167" size="7" /> x <input type="text" name="widget_height" value="255" size="7" /></td>
			</tr>
			<tr>
				<td class="center"><?php echo $entry_limit; ?></td>
				<td><input type="text" name="limit" size="7" value="10" /></td>
			</tr>	
		</table>
	</div>
	<div class="configuration-right">
		<table class="form" style="margin-bottom: 0px;">
			<tr>
				<td class="center"><span class="required">* </span><?php echo $entry_language; ?></td>
				<td><select name="language_id">
				<?php foreach($languages as $language) { ?>
				<?php if ($language['language_id'] == $current_language_id) { ?>
				<option value="<?php echo $language['language_id']; ?>" selected="selected"><?php echo $language['name']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $language['language_id']; ?>"><?php echo $language['name']; ?></option>
				<?php } ?>
				<?php } ?>
				</select></td>
			</tr>
			<tr>	
				<td class="center"><span class="required">* </span><?php echo $entry_currency; ?></td>
				<td><select name="currency_code">
				<?php foreach($currencies as $currency) { ?>
				<?php if ($currency['code'] == $current_currency_code) { ?>
				<option value="<?php echo $currency['code']; ?>" selected="selected"><?php echo $currency['code']; ?></option>
				<?php } else { ?>
				<option value="<?php echo $currency['code']; ?>"><?php echo $currency['code']; ?></option>
				<?php } ?>
				<?php } ?>
				</select></td>
			</tr>	
			<tr>
				<td class="center"><?php echo $entry_slider_time; ?></td>
				<td><input type="text" name="slider_time" size="7" value="5" /></td>
			</tr>
		</table>
	</div>
  </div>
  
  <div id="choose_category">
	<div class="left"><?php echo $entry_available_categories; ?>
		<?php if ($categories){ ?>
		<ul id="available_categories" class="connectedSortable">
		<?php foreach($categories as $category){ ?>
			<li id="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></li>
		<?php } ?>
		</ul>	
		<?php } ?>
	</div>
	<div class="right"><?php echo $entry_selected_categories; ?>
		<ul id="selected_categories" class="connectedSortable"></ul>
	</div>
  </div>
  
  <input type="hidden" id="sel_categories" name="sel_categories" />
  
  <div class="buttons">
	<div class="left"><?php echo $text_instructions; ?></div>
	<div class="right"><a class="button" id="widget_get_code"><span><?php echo $button_generate_widget; ?></span></a></div>
  </div>  
  
  <div id="widget_code"><textarea id="widget_code_text" rows="4" cols="98" readonly></textarea></div>
  
  
  <div class="iborder"><iframe id="preview" src="" frameborder="0" scrolling="no"></iframe></div>
</div>  	  
   


<script type="text/javascript"><!--
$(document).ready(function() {

	$('a[href*="<?php echo $trigger_url; ?>"]').click(function(e) {
		e.preventDefault();
		var url = $(this).attr('href');
		
		$.colorbox({
			inline: true,
			overlayClose: false,
			opacity: 0.5,
			width: '800px',
			height: '770px',
			href: '#awg',
			onLoad: function() { 
				$('#awg').show(); 
			},
			onClosed: function() { 
				$('#awg').hide();
			}	
		});
	});	
});
//--></script>
  
<script type="text/javascript"><!--
$(function() {
	$( "#available_categories, #selected_categories" ).sortable({
		connectWith: ".connectedSortable"
	}).disableSelection();
});

$('#widget_get_code').live('click', function() {
	getWidgetSettingsAndPreview();
});

function getWidgetSettingsAndPreview(module){
	var sel_categories = new Array();
	
	$('#selected_categories li').each( function(index){
		sel_categories.push( $(this).attr('id') );
	});
	
	var widget_width  = $('#awg input[name=\'widget_width\']').val();
	var widget_height = $('#awg input[name=\'widget_height\']').val();
	
	$('#preview').attr('width', widget_width);
	$('#preview').attr('height', widget_height);
	
	$('#sel_categories').val(sel_categories.join(','));
	
	$.ajax({
		url: 'index.php?route=module/affiliate_widget_generator/widget',
		type: 'GET',
		data: $('#awg input[type=\'text\'], #awg input[type=\'hidden\'], #awg select'),
		beforeSend: function() {
			$('#widget_get_code').attr('disabled', true);
			$('#widget_get_code').before('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},	
		complete: function() {
			$('#widget_get_code').attr('disabled', false); 
			$('.wait').remove();
		},			
		success: function(data) {
			//$('#widget_preview').html(data);
			//$('#widget_generated_code').val(generateCode());
		}
	});
	
	$('body').bind('ajaxComplete',function(event,request,settings){
		if (settings.type == 'GET') {
			if ( settings.url.match(/.*module\/affiliate_widget_generator\/widget/)){
				$('#preview').attr('src', settings.url); 
				$('.iborder').show();
				$('#widget_code_text').text('<iframe src="<?php echo $server; ?>' + settings.url + '" frameborder="0" scrolling="no" width="' + widget_width + '" height="' + widget_height + '"></iframe>'); 
				$('#widget_code').show();
			}
		}	
	});
}

//--></script> 