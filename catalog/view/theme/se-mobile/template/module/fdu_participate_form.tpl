<a class="fb-deal-close-dialog" id="fb-deal-close-dialog"></a>
<div id="fb-deal-notification-info"></div>
<div class="steps">
	<div class="step">
		<div class="action-name"><span class="round-number">1</span> <?php echo $text_like;  ?></div>
		<div class="fb-area"><fb:like layout="box_count" href="<?php echo $like_href; ?>"></fb:like></div>
		<div class="action-type"><?php echo $text_more_info;  ?></div>
	</div>
	<div class="step">
		<div class="action-name"><span class="round-number">2</span> <?php echo $text_share;  ?></div>
		<div class="fb-area">
			<div class="fdu-share"></div>
		</div>
		<div class="action-type"><?php echo $text_more_info;  ?></div>
	</div>
	<div class="step">
		<div class="action-name"><span class="round-number">3</span> <?php echo $text_invite;  ?></div>
		<div class="fb-area">
			<div class="fdu-invite-friends"></div>
		</div>
		<div class="action-type"><?php echo $text_more_info;  ?></div>
	</div>
</div>
<div class="participate-info"><?php echo $text_participate_info; ?></div>
<div class="rules-info">
	<div class="close"></div>
	<?php echo $text_rules_info; ?>
</div>

<script type="text/javascript">

$(document).ready(function(){
<?php if ($like_required) {?>
    checkAlreadySubscribed();
<?php } ?>
});

<?php if ($share_limit || $invite_limit) { ?>
checkLimits();
<?php } ?>

$('#fb-deal-close-dialog').bind('click', function(){
	$('#dialog-deal-like-share').dialog("close");
});

$('.rules-info .close, .step .action-type').bind('click', function(){
	$('#dialog-deal-like-share .rules-info').toggle('slow');
});

FB.Event.subscribe('edge.create', function (response) {
	likeOrUnlikeDeal('like');
});

FB.Event.subscribe('edge.remove', function (response) {
	likeOrUnlikeDeal('unlike');
});

$('.fdu-share').bind('click', function(){
	var publish = {
		method: 'feed',
		message: '',
		name: '<?php echo addslashes($feed_name); ?>',
		caption: '<?php echo addslashes($feed_caption); ?>',
		description: ('<?php echo addslashes($feed_description); ?>'),
		link: '<?php echo $feed_product_link; ?>',
		picture: '<?php echo $image; ?>',
		actions: [
					{ name: '<?php echo addslashes($feed_name); ?>', link: '<?php echo $feed_link; ?>' }
		         ],
		user_message_prompt: ''
	};
	
	if ( !$(this).hasClass('fdu-step-locked')) {

		FB.getLoginStatus( function(response){
			if (response.status === 'connected') {
				FB.ui(publish, checkShareStatus);
			} else {
				FB.login(function(response) {
					if (response.status === 'connected') {
						FB.ui(publish, checkShareStatus);
					} 
				}, { scope: 'email'});
			}
		});
	}	
});

$('.fdu-invite-friends').bind('click', function(){
	if ( !$(this).hasClass('fdu-step-locked')) {

		FB.getLoginStatus( function(response){
			if (response.status === 'connected') {
			
				FB.ui({
					method: 'send',
					name: '<?php echo addslashes($feed_name); ?>',
					description: ('<?php echo addslashes($feed_description); ?>'),
					link: '<?php echo $feed_product_link; ?>',
					picture: '<?php echo $image; ?>'
				 }, checkInviteStatus);
				 
			} else {
				FB.login(function(response) {
					if (response.status === 'connected') {
						FB.ui({
							method: 'send',
							name: '<?php echo addslashes($feed_name); ?>',
							description: ('<?php echo addslashes($feed_description); ?>'),
							link: '<?php echo $feed_product_link; ?>',
							picture: '<?php echo $image; ?>'
						 }, checkInviteStatus);
					} 
				}, { scope: 'email'});
			}
		});
	}
});

function checkInviteStatus(invite_response){
	if ( typeof(invite_response) != 'undefined' && invite_response !== null && invite_response.success == true){
		
		var no_invited_friends =  1;
		
		FB.api('/me?fields=id,name,email,first_name,last_name', function(response) {
			
			response.deal_id = <?php echo $deal_id; ?>;
			response.point_action = 'invite';
			response.no_invited_friends = no_invited_friends;
			
			$.ajax({
				type: 'POST',
				url: 'index.php?route=module/fb_deal_unlocker/updatePoints',
				data: response,
				dataType: 'json',
				success: function(json){
					$('.success, .attention').remove();
					
					if (json['success']){
						$('#fb-deal-notification-info').html('<div class="success-info">' + json['success'] + '</div>');
						$('#fb-deal-notification-info .success-info').hide();
						$('#fb-deal-notification-info .success-info').slideDown();
					}	
					
					setRemainingNeededPeople(<?php echo $deal_id; ?>, json['remaining_needed_people']);
				}
			});
		});
		
		<?php if ($share_limit || $invite_limit) { ?>
		checkLimits();	
		<?php } ?>
	}
}

function checkShareStatus(response){
	 if (response && response.post_id) {
	 
		FB.api('/me?fields=id,name,email,first_name,last_name', function(response) {
			
			response.deal_id = <?php echo $deal_id; ?>;
			response.point_action = 'share';
			
			$.ajax({
				type: 'POST',
				url: 'index.php?route=module/fb_deal_unlocker/updatePoints',
				data: response,
				dataType: 'json',
				success: function(json){
					$('.success, .attention').remove();
					
					if (json['success']){
						$('#fb-deal-notification-info').html('<div class="success-info">' + json['success'] + '</div>');
						$('#fb-deal-notification-info .success-info').hide();
						$('#fb-deal-notification-info .success-info').slideDown();
					}

					setRemainingNeededPeople(<?php echo $deal_id; ?>, json['remaining_needed_people']);	
				}
			});
		});
		
		<?php if ($share_limit || $invite_limit) { ?>
		checkLimits();	
		<?php } ?>
	 }
}

function likeOrUnlikeDeal(action_type){
	//FB.logout();
	FB.getLoginStatus( function(response){
		if (response.status === 'connected') {
			getInfoAndLikeOrUnlike(action_type);
		} else {
			FB.login(function(response) {
				if (response.status === 'connected') {
					getInfoAndLikeOrUnlike(action_type);	
				} 
			}, { scope: 'email'});
		}
	});
}

function getInfoAndLikeOrUnlike(action_type){
	
	FB.api('/me?fields=id,name,email,first_name,last_name', function(response) {
		response.deal_id = <?php echo $deal_id; ?>;
		response.point_action = action_type;
		
		$.ajax({
			type: 'POST',
			url: 'index.php?route=module/fb_deal_unlocker/updatePoints',
			data: response,
			dataType: 'json',
			success: function(json){
				$('.success, .attention').remove();
					
				if (json['success']){
					$('#fb-deal-notification-info').html('<div class="success-info">' + json['success'] + '</div>');
					$('#fb-deal-notification-info .success-info').hide();
					$('#fb-deal-notification-info .success-info').slideDown();
				}	
				
				setRemainingNeededPeople(<?php echo $deal_id; ?>, json['remaining_needed_people']);
			
			<?php if ($like_required) { ?>
				if (action_type == 'like'){
					$('.fdu-share').removeClass('fdu-step-locked');
					$('.fdu-invite-friends').removeClass('fdu-step-locked');
				
				} else {
					$('.fdu-share').addClass('fdu-step-locked');
					$('.fdu-invite-friends').addClass('fdu-step-locked');
				}
			<?php } ?>	
			}
		});
	});
}

function checkLimits(){
	FB.api('', function(response) {
		response.deal_id = <?php echo $deal_id; ?>;
		
		$.ajax({
			type: 'POST',
			url: 'index.php?route=module/fb_deal_unlocker/checkLimits',
			data: response,
			dataType: 'json',
			success: function(json){
				if (json['allow_share']){
					$('.fdu-share').removeClass('fdu-step-locked');
				} else {
					$('.fdu-share').addClass('fdu-step-locked');
				}	
				
				if (json['allow_invite']){
					$('.fdu-invite-friends').removeClass('fdu-step-locked');
				} else {
					$('.fdu-invite-friends').addClass('fdu-step-locked');
				}
			}
		});
	});
}

function checkAlreadySubscribed(){
	FB.api('/me?fields=id,name,email,first_name,last_name', function(response) {
		response.deal_id = <?php echo $deal_id; ?>;
		
		$.ajax({
			type: 'POST',
			url: 'index.php?route=module/fb_deal_unlocker/checkAlreadySubscribed',
			data: response,
			dataType: 'json',
			success: function(json){
				if (json['subscribed']){
					$('.fdu-share').removeClass('fdu-step-locked');
					$('.fdu-invite-friends').removeClass('fdu-step-locked');
				
				} else {
					$('.fdu-share').addClass('fdu-step-locked');
					$('.fdu-invite-friends').addClass('fdu-step-locked');
				}	
			}
		});
	});
}

function setRemainingNeededPeople(deal_id, remaining_people){
	$('#rnp-' + deal_id).html(remaining_people);
	updateParticipantsPhotoList(deal_id);
}

function updateParticipantsPhotoList(deal_id){
	$.ajax({
		type: 'POST',
		url: 'index.php?route=module/fb_deal_unlocker/getPhotoList',
		data: 'deal_id=' + deal_id,
		success: function(data){
			$('#photo-list-' + deal_id).html(data);
		}
	});
}

</script>
