<div id="content">  
    <div class="content" id="fcontact">
		<b><?php echo $entry_name; ?></b><br />
		<input type="text" name="name" value="" size="40" />
		<br />
		<br />
		<b><?php echo $entry_email; ?></b><br />
		<input type="text" name="email" value="" size="40" />
		<br />
		<br />
		<b><?php echo $entry_enquiry; ?></b><br />
		<textarea name="enquiry" cols="40" rows="4" style="width: 99%;"></textarea>
		<br />
		<br />
		<b><?php echo $entry_captcha; ?></b><br />
		<input type="text" name="captcha" value="" />
		<br />
		<img src="index.php?route=information/contact/captcha" alt="" />
    </div>
	
    <div class="buttons">
      <div class="right"><a id="fixb-contact-submit"  onclick="submitContact();" class="btn light-blue darken-1"><span><?php echo $button_continue; ?></span></a></div>
    </div>
</div> 

<script type="text/javascript">
function submitContact(){
	$.ajax({
		type: 'POST',
		url: 'index.php?route=module/fixed_bar/contact',
		data: $('#fcontact input[type=\'text\'], #fcontact textarea'),
		dataType: 'json',
		success: function(json){
			$('.warning').remove();
			$('.success').remove();
			
			if (json['error']){
				$('#popupFixedBarContent .content').prepend('<div class="warning" style="display: none;">' + json['error'] + '</div>');
				$('.warning').fadeIn('slow');
			}
			
			if (json['success']){
				$('#popupFixedBarContent .content').prepend('<div class="success" style="display: none;">' + json['success'] + '</div>');
				$('.success').fadeIn('slow');
				$('#popupFixedBarContent input, #popupFixedBarContent textarea ').each(function(){
					$(this).val('');
				});
			}
		}
	})
}
</script> 

