<div id="info-popup-bg"></div>

<div id="info-popup">
	<div class="inner">
		<form name="infopopup" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
		<div class="message"><?php echo $message; ?></div>
		
		<?php if ($currency || $language || $country) { ?>
		<div class="selector">
			<?php if ($country) { ?>
			<div class="choose country">
				<label for="popup_country"><?php echo $text_country; ?></label>
				<select id="popup_country" name="popup_country_id">
				<?php foreach ($countries as $country) { ?>
				<option value="<?php echo $country['country_id']; ?>" <?php if ($country['country_id'] == $default_country) { echo ' selected="selected"'; } ?>><?php echo $country['name']; ?></option>
				<?php } ?>
				</select>
			</div>
			<?php } ?>
			
			<?php if ($language) { ?>
			<div class="choose language">
				<label for="popup_language"><?php echo $text_language; ?></label>
				<select id="popup_language" name="popup_language_id">
				<?php foreach ($languages as $language) { ?>
				<option value="<?php echo $language['code']; ?>" <?php if ($language['code'] == $default_language) { echo ' selected="selected"'; } ?>><?php echo $language['name']; ?></option>
				<?php } ?>
				</select>
			</div>
			<?php } ?>
			
			<?php if ($currency) { ?>
			<div class="choose currency">
				<label for="popup_currency"><?php echo $text_currency; ?></label>
				<select id="popup_currency" name="popup_currency_id">
				<?php foreach ($currencies as $currency) { ?>
				<option value="<?php echo $currency['code']; ?>" <?php if ($currency['code'] == $default_currency) { echo ' selected="selected"'; } ?>><?php echo $currency['title']; ?></option>
				<?php } ?>
				</select>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
		
		<div class="buttons">
			<input type="hidden" name="popup_cookie" value="<?php echo $popup_cookie; ?>" />
			<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
			<input type="hidden" name="route" value="<?php echo $route; ?>" />
			<?php if ($decline_url) { ?><input id="info-popup-accept" type="submit" value="<?php echo $button_accept; ?>" /><input onclick="window.location.href = '<?php echo $decline_url; ?>';" id="info-popup-decline" type="button" value="<?php echo $button_decline; ?>" />
			<?php } else { ?><input id="info-popup-ok" type="submit" value="<?php echo $button_ok; ?>" /><?php } ?>
		</div>
		</form>
	</div>
</div>

<script type="text/javascript">$(document).ready( function() { loadInfoPopup('<?php echo $transition; ?>'); });</script>