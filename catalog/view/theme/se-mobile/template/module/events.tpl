<?php if ($events) { ?>
  <?php foreach ($events as $event) { ?>
    <?php if ($event['message']) { ?>
      <div class="content">
        <div class="events"><?php echo $event['message']; ?></div>
        <?php if ($event['details'] != '') { ?>
          <div class="details"><a href="<?php echo $event['href']; ?>"><?php echo $event['details']; ?></a></div>
        <?php } ?>
      </div>
    <?php } ?>
  <?php } ?>
<?php } ?>
