<?php
$style = '';

if ($setting['background_color']) {
	$style .= ' background-color: ' . $setting['background_color'] . ';';
}

if ($setting['border_style']) {
	$style .= ' border: ' . $setting['border_style'] . ' ' . $setting['border_size'] . ' ' . $setting['border_color'] . ';';
}

if ($setting['border_radius_status']) {
	$style .= ' -webkit-border-radius: ' . $setting['border_radius_size'] . '; -moz-border-radius: ' . $setting['border_radius_size'] . '; border-radius: ' . $setting['border_radius_size'] . ';';
}

$style .= ' padding: ' . $setting['padding_top'] . ' ' . $setting['padding_right'] . ' ' . $setting['padding_bottom'] . ' ' . $setting['padding_left']. ';';
$style .= ' margin: ' . $setting['margin_top'] . ' ' . $setting['margin_right'] . ' ' . $setting['margin_bottom'] . ' ' . $setting['margin_left']. ';';
?>
<div class="SP_section SP_section_order" style="width: <?php echo $setting['wrapper']; ?>;">
    <div class="SP_section_wrapper" style="<?php echo $style; ?>">
        <div class="SP_table_responsive">
			<table class="table table-bordered SP_table">
				<thead>
					<tr>
						<td class="text-left" colspan="2"><?php echo isset($setting[$language_id]['title']) ? $setting[$language_id]['title'] : ''; ?></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-left" style="width: 50%;">
							<?php if ($setting['invoice_status'] && $order_info['invoice_no']) { ?>
							<b><?php echo isset($setting[$language_id]['detail']['invoice']) ? $setting[$language_id]['detail']['invoice'] : ''; ?></b> <?php echo $order_info['invoice_no']; ?><br />
							<?php } ?>
							<?php if ($setting['order_id_status']) { ?>
							<b><?php echo isset($setting[$language_id]['detail']['order_id']) ? $setting[$language_id]['detail']['order_id'] : ''; ?></b> #<?php echo $order_info['order_id']; ?><br />
							<?php } ?>
							<?php if ($setting['date_added_status']) { ?>
							<b><?php echo isset($setting[$language_id]['detail']['date_added']) ? $setting[$language_id]['detail']['date_added'] : ''; ?></b> <?php echo date("Y-m-d H:i:s", strtotime($order_info['date_added'])); ?><br />
							<?php } ?>
							<?php if ($setting['telephone_status']) { ?>
							<b><?php echo isset($setting[$language_id]['detail']['telephone']) ? $setting[$language_id]['detail']['telephone'] : ''; ?></b> <?php echo $order_info['telephone']; ?>
							<?php } ?>
						</td>
						<td class="text-left">
							<?php if ($setting['payment_method_status'] && $order_info['payment_method']) { ?>
							<b><?php echo isset($setting[$language_id]['detail']['payment_method']) ? $setting[$language_id]['detail']['payment_method'] : ''; ?></b> <?php echo $order_info['payment_method']; ?><br />
							<?php } ?>
							<?php if ($setting['shipping_method_status'] && $order_info['shipping_method']) { ?>
							<b><?php echo isset($setting[$language_id]['detail']['shipping_method']) ? $setting[$language_id]['detail']['shipping_method'] : ''; ?></b> <?php echo $order_info['shipping_method']; ?><br />
							<?php } ?>
							<?php if ($setting['email_status']) { ?>
							<b><?php echo isset($setting[$language_id]['detail']['email']) ? $setting[$language_id]['detail']['email'] : ''; ?></b> <?php echo $order_info['email']; ?>
							<?php } ?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
    </div>
</div>