<?php
$style = '';

if ($setting['background_color']) {
	$style .= ' background-color: ' . $setting['background_color'] . ';';
}

if ($setting['border_style']) {
	$style .= ' border: ' . $setting['border_style'] . ' ' . $setting['border_size'] . ' ' . $setting['border_color'] . ';';
}

if ($setting['border_radius_status']) {
	$style .= ' -webkit-border-radius: ' . $setting['border_radius_size'] . '; -moz-border-radius: ' . $setting['border_radius_size'] . '; border-radius: ' . $setting['border_radius_size'] . ';';
}

$style .= ' padding: ' . $setting['padding_top'] . ' ' . $setting['padding_right'] . ' ' . $setting['padding_bottom'] . ' ' . $setting['padding_left']. ';';
$style .= ' margin: ' . $setting['margin_top'] . ' ' . $setting['margin_right'] . ' ' . $setting['margin_bottom'] . ' ' . $setting['margin_left']. ';';
$style .= ' text-align: ' . $setting['align'] . ';';
?>
<div class="SP_section SP_section_social" style="width: <?php echo $setting['wrapper']; ?>;">
    <div class="SP_section_wrapper" style="<?php echo $style; ?>">
        <ul>
			<?php if ($setting['social']['pinterest_status']) { ?>
			<li>
			<a href="http://pinterest.com/pin/create/button/" class="pin-it-button" count-layout="horizontal"><img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>
			<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
			</li>
			<?php } ?>
			<?php if ($setting['social']['googleplus_status']) { ?>
			<li>
			<g:plusone size="medium" annotation="none"></g:plusone>
			<script type="text/javascript">
			(function() {
			var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			po.src = 'https://apis.google.com/js/plusone.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			})();
			</script>
			</li>
			<?php } ?>
			<?php if ($setting['social']['linkedin_status']) { ?>
			<li>
			<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
			<script type="IN/Share" data-counter="right"></script>
			</li>
			<?php } ?>
			<?php if ($setting['social']['facebook_status']) { ?>
			<li>
			<div id="fb-root"></div>
			<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=159650554163037";
			fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			<fb:like send="false" layout="button_count" width="200" show_faces="false"></fb:like>
			</li>
			<?php } ?>
			<?php if ($setting['social']['twitter_status']) { ?>
			<li>
			<a href="https://twitter.com/share" class="twitter-share-button" data-lang="en">Tweet</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</li>
			<?php } ?>
		</ul>
    </div>
</div>