<?php
$style = '';

if ($setting['background_color']) {
	$style .= ' background-color: ' . $setting['background_color'] . ';';
}

if ($setting['border_style']) {
	$style .= ' border: ' . $setting['border_style'] . ' ' . $setting['border_size'] . ' ' . $setting['border_color'] . ';';
}

if ($setting['border_radius_status']) {
	$style .= ' -webkit-border-radius: ' . $setting['border_radius_size'] . '; -moz-border-radius: ' . $setting['border_radius_size'] . '; border-radius: ' . $setting['border_radius_size'] . ';';
}

$style .= ' padding: ' . $setting['padding_top'] . ' ' . $setting['padding_right'] . ' ' . $setting['padding_bottom'] . ' ' . $setting['padding_left']. ';';
$style .= ' margin: ' . $setting['margin_top'] . ' ' . $setting['margin_right'] . ' ' . $setting['margin_bottom'] . ' ' . $setting['margin_left']. ';';

$i = 0;
?>
<div class="SP_section SP_section_cart" style="width: <?php echo $setting['wrapper']; ?>;">
	<div class="SP_section_wrapper" style="<?php echo $style; ?>">
        <div class="SP_table_responsive">
			<table class="table table-bordered SP_table">
				<thead>
					<tr>
						<?php if ($setting['image_status']) { ?>
						<?php $i++; ?>
						<td class="text-center" style="width: <?php echo $setting['width']; ?>"><?php echo isset($setting[$language_id]['column']['image']) ? $setting[$language_id]['column']['image'] : ''; ?></td>
						<?php } ?>
						<?php if ($setting['product_status']) { ?>
						<?php $i++; ?>
						<td class="text-left"><?php echo isset($setting[$language_id]['column']['product']) ? $setting[$language_id]['column']['product'] : ''; ?></td>
						<?php } ?>
						<?php if ($setting['model_status']) { ?>
						<?php $i++; ?>
						<td class="text-left"><?php echo isset($setting[$language_id]['column']['model']) ? $setting[$language_id]['column']['model'] : ''; ?></td>
						<?php } ?>
						<?php if ($setting['sku_status']) { ?>
						<?php $i++; ?>
						<td class="text-left"><?php echo isset($setting[$language_id]['column']['sku']) ? $setting[$language_id]['column']['sku'] : ''; ?></td>
						<?php } ?>
						<?php if ($setting['quantity_status']) { ?>
						<?php $i++; ?>
						<td class="text-left"><?php echo isset($setting[$language_id]['column']['quantity']) ? $setting[$language_id]['column']['quantity'] : ''; ?></td>
						<?php } ?>
						<?php if ($setting['price_status']) { ?>
						<?php $i++; ?>
						<td class="text-right"><?php echo isset($setting[$language_id]['column']['price']) ? $setting[$language_id]['column']['price'] : ''; ?></td>
						<?php } ?>
						<?php if ($setting['total_status']) { ?>
						<?php $i++; ?>
						<td class="text-right"><?php echo isset($setting[$language_id]['column']['total']) ? $setting[$language_id]['column']['total'] : ''; ?></td>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($products as $product) { ?>
					<tr>
						<?php if ($setting['image_status']) { ?>
						<td class="text-center">
							<?php if ($product['thumb']) { ?>
							<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail SP_image_thumbnail" />
							<?php } ?>
						</td>
						<?php } ?>
						<?php if ($setting['product_status']) { ?>
						<td class="text-left"><?php echo $product['name']; ?>
							<?php if ($product['option']) { ?>
							<?php foreach ($product['option'] as $option) { ?>
							<br />
							<small>- <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
							<?php } ?>
							<?php } ?>
						</td>
						<?php } ?>
						<?php if ($setting['model_status']) { ?>
						<td class="text-left"><?php echo $product['model']; ?></td>
						<?php } ?>
						<?php if ($setting['sku_status']) { ?>
						<td class="text-left"><?php echo $product['sku']; ?></td>
						<?php } ?>
						<?php if ($setting['quantity_status']) { ?>
						<td class="text-left"><?php echo $product['quantity']; ?></td>
						<?php } ?>
						<?php if ($setting['price_status']) { ?>
						<td class="text-right"><?php echo $product['price']; ?></td>
						<?php } ?>
						<?php if ($setting['total_status']) { ?>
						<td class="text-right"><?php echo $product['total']; ?></td>
						<?php } ?>
					</tr>
					<?php } ?>
					<?php $j = $i; ?>
					<?php foreach ($vouchers as $voucher) { ?>
					<tr>
						<?php if ($setting['image_status']) { ?>
						<?php $j = $i - 1; ?>
						<td></td>
						<?php } ?>
						<td class="text-left" colspan="<?php echo ($j - 1 > 0) ? $j - 1 : ''; ?>"><?php echo $voucher['description']; ?></td>
						<td class="text-right"><?php echo $voucher['amount']; ?></td>
					</tr>
					<?php } ?>
					<?php if ($setting['totals_status']) { ?>
					<?php foreach ($totals as $total) { ?>
					<tr>
						<td class="text-right" colspan="<?php echo ($i - 1 > 0) ? $i - 1 : ''; ?>"><b><?php echo $total['title']; ?></b></td>
						<td class="text-right"><?php echo $total['text']; ?></td>
					</tr>
					<?php } ?>
					<?php } ?>
				</tbody>
			</table>
		</div>
    </div>
</div>