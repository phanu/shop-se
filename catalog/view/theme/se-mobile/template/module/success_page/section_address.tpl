<?php
$style = '';

if ($setting['background_color']) {
	$style .= ' background-color: ' . $setting['background_color'] . ';';
}

if ($setting['border_style']) {
	$style .= ' border: ' . $setting['border_style'] . ' ' . $setting['border_size'] . ' ' . $setting['border_color'] . ';';
}

if ($setting['border_radius_status']) {
	$style .= ' -webkit-border-radius: ' . $setting['border_radius_size'] . '; -moz-border-radius: ' . $setting['border_radius_size'] . '; border-radius: ' . $setting['border_radius_size'] . ';';
}

$style .= ' padding: ' . $setting['padding_top'] . ' ' . $setting['padding_right'] . ' ' . $setting['padding_bottom'] . ' ' . $setting['padding_left']. ';';
$style .= ' margin: ' . $setting['margin_top'] . ' ' . $setting['margin_right'] . ' ' . $setting['margin_bottom'] . ' ' . $setting['margin_left']. ';';
?>
<div class="SP_section SP_section_address" style="width: <?php echo $setting['wrapper']; ?>;">
    <div class="SP_section_wrapper" style="<?php echo $style; ?>">
        <div class="SP_table_responsive">
			<table class="table table-bordered SP_table">
				<thead>
					<tr>
						<?php if ($payment) { ?>
						<td class="text-left"><?php echo isset($setting[$language_id]['payment_title']) ? $setting[$language_id]['payment_title'] : ''; ?></td>
						<?php } ?>
						<?php if ($shipping) { ?>
						<td class="text-left"><?php echo isset($setting[$language_id]['payment_title']) ? $setting[$language_id]['shipping_title'] : ''; ?></td>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
					<tr>
						<?php if ($payment) { ?>
						<td class="text-left"><?php echo $payment; ?></td>
						<?php } ?>
						<?php if ($shipping) { ?>
						<td class="text-left"><?php echo $shipping; ?></td>
						<?php } ?>
					</tr>
				</tbody>
			</table>
		</div>
    </div>
</div>