<?php
$style = '';

if ($setting['background_color']) {
	$style .= ' background-color: ' . $setting['background_color'] . ';';
}

if ($setting['border_style']) {
	$style .= ' border: ' . $setting['border_style'] . ' ' . $setting['border_size'] . ' ' . $setting['border_color'] . ';';
}

if ($setting['border_radius_status']) {
	$style .= ' -webkit-border-radius: ' . $setting['border_radius_size'] . '; -moz-border-radius: ' . $setting['border_radius_size'] . '; border-radius: ' . $setting['border_radius_size'] . ';';
}

$style .= ' padding: ' . $setting['padding_top'] . ' ' . $setting['padding_right'] . ' ' . $setting['padding_bottom'] . ' ' . $setting['padding_left']. ';';
$style .= ' margin: ' . $setting['margin_top'] . ' ' . $setting['margin_right'] . ' ' . $setting['margin_bottom'] . ' ' . $setting['margin_left']. ';';
?>
<div class="SP_section SP_section_product" style="width: <?php echo $setting['wrapper']; ?>;">
    <div class="SP_section_wrapper" style="<?php echo $style; ?>">
        <div style="margin-bottom: 20px; font-size: <?php echo $setting['size']; ?>; color: <?php echo $setting['color']; ?>; font-weight: <?php echo $setting['weight']; ?>;"><?php echo isset($setting[$language_id]['title']) ? $setting[$language_id]['title'] : ''; ?></div>
		<?php if ($products) { ?>
		<?php if (version_compare(VERSION, '2.0') < 0) { ?>
		<div class="box">
			<div class="box-content">
				<div class="box-product">
					<?php foreach ($products as $product) { ?>
					<div>
						<?php if ($product['thumb']) { ?>
						<div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
						<?php } ?>
						<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
						<div class="description"><?php echo $product['description']; ?></div>
						<?php if ($product['price']) { ?>
						<div class="price">
							<?php if (!$product['special']) { ?>
							<?php echo $product['price']; ?>
							<?php } else { ?>
							<span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
							<?php } ?>
						</div>
						<?php } ?>
						<div class="cart"><a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="SP_button"><?php echo isset($setting[$language_id]['button']) ? $setting[$language_id]['button'] : ''; ?></a></div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } else { ?>
		<div class="row">
			<?php foreach ($products as $product) { ?>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="product-thumb transition">
					<div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
					<div class="caption">
						<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
						<p><?php echo $product['description']; ?></p>
						<?php if ($product['price']) { ?>
						<p class="price">
							<?php if (!$product['special']) { ?>
							<?php echo $product['price']; ?>
							<?php } else { ?>
							<span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
							<?php } ?>
						</p>
						<?php } ?>
						<a onclick="cart.add('<?php echo $product['product_id']; ?>');" class="SP_button"><?php echo isset($setting[$language_id]['button']) ? $setting[$language_id]['button'] : ''; ?></a>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
		<?php } ?>
    </div>
</div>