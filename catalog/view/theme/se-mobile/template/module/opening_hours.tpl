<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
	<table cellspacing-right="4" width="100%">
		<tbody >  
		
		<?php if($show_notes == 1) : { ?> 
		<tr> 
			<td align="center" colspan="2"><?php echo $notes; ?></td>
		</tr>
		<?php } endif; ?>

		<?php
		foreach( $day_times as $key => $value){ ?>
			<tr>
			<td valign="top" <?php echo $highlight_today && $key == $today ? 'style="' .$highlight_style. '"' :  ''; ?>><?php echo $key; ?></td>
			<td valign="top" <?php echo $highlight_today && $key == $today ? 'style="' .$highlight_style. '"' :  ''; ?>><?php echo $value; ?></td>
			</tr>
		<?php } ?>

		<?php if($show_notes == 2) : { ?> 
			<tr> 
				<td align="center" colspan="2"><br /><?php echo $notes; ?></td>
			</tr>
		<?php } endif; ?>

		</tbody>
	</table>
  </div>
</div>


