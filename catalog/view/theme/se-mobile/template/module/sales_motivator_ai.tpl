<div id="sm-more-info-notification"><?php echo $text_explain_info; ?></div>

<div id="smmoreinfo">
  <table class="form">
	<tr>
	  <td><span class="required">*</span> <?php echo $entry_telephone; ?></td>
	  <td><input type="text" name="telephone" value="" /></td>
	</tr>
	<tr>
	  <td><span class="required">*</span> <?php echo $entry_address_1; ?></td>
	  <td><input type="text" name="address_1" value="" /></td>
	</tr>
	<tr>
	  <td><?php echo $entry_address_2; ?></td>
	  <td><input type="text" name="address_2" value="" /></td>
	</tr>
	<tr>
	  <td><span class="required">*</span> <?php echo $entry_city; ?></td>
	  <td><input type="text" name="city" value="" /></td>
	</tr>
	<tr>
	  <td><span id="postcode-required" class="required">*</span> <?php echo $entry_postcode; ?></td>
	  <td><input type="text" name="postcode" value="" /></td>
	</tr>
	<tr>
	  <td><span class="required">*</span> <?php echo $entry_country; ?></td>
	  <td><select name="country_id">
		  <option value=""><?php echo $text_select; ?></option>
		  <?php foreach ($countries as $country) { ?>
		  <?php if ($country['country_id'] == $country_id) { ?>
		  <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
		  <?php } else { ?>
		  <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
		  <?php } ?>
		  <?php } ?>
		</select>
	  </td>
	</tr>
	<tr>
	  <td><span class="required">*</span> <?php echo $entry_zone; ?></td>
	  <td><select name="zone_id">
		</select></td>
	</tr>
  </table>
  
  <input type="hidden" name="sm_additional_info" value="1" />
  
  <div class="buttons">
	<div class="right">
		<a id="sm-register-now" class="sm-button"><?php echo $button_register; ?></a>
	</div>
  </div>
</div>

<script type="text/javascript"><!--
$('#sm-register-now').bind('click', function() { 
	$.ajax({
		type: 'POST',
		url: 'index.php?route=module/sales_motivator/accountExtraInfo',
		data: $('#smmoreinfo input[type=\'text\'], #smmoreinfo input[type=\'hidden\'], #smmoreinfo select'),
		dataType: 'json',
		success: function(json) {
			$('.success, .warning').remove();
			
			if (json['error']) {
				$('#sm-more-info-notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
				$('.warning').fadeIn('slow');
			}

			if (json['output']) {
				$('#salesmotivator-display-info').html(json['output']);
			}			
		}
	});
});

$('select[name=\'country_id\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=module/sales_motivator/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#postcode-required').show();
			} else {
				$('#postcode-required').hide();
			}
			
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script>