<div class="sm-intro">
	<div class="sm-intro-content">
		<div class="sm-announcer"></div>
		<div class="sm-message"><?php echo $text_you_can_get; ?></div>
		<div class="sm-actions"><a class="sm-button colorbox lightbox" href="<?php echo $rules_href; ?>">?</a></div>
		<div class="sm-actions"><a class="sm-button" id="sm-get-discount"><?php echo $button_get_discount; ?></a></div>
		<div class="sm-discount-info">
			<div class="sm-discount-value"><?php echo $friend_reward; ?><span class="details"><?php echo $text_for_friend; ?></span></div>
			<div class="sm-separator"></div>
			<div class="sm-discount-value"><?php echo $share_reward; ?><span class="details"><?php echo $text_for_share; ?></span></div>
		</div>
	</div>
</div>

<div id="sm-get-discount-modal" class="salesmotivator-modal">
	<div class="salesmotivator-modal-content">
		<div id="salesmotivator-display-info"></div>
		<a class="close-salesmotivator-modal">&#215;</a>
	</div>	
</div>

<div id="fb-root"></div>
<script type="text/javascript"><!--

window.fbAsyncInit = function() {
    FB.init({
		appId      : '<?php echo $fb_app_id; ?>', // App ID
		status     : true, // check login status
		cookie     : true, // enable cookies to allow the server to access the session
		xfbml      : true  // parse XFBML
    });
};

// Load the SDK Asynchronously
(function(d){
    var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    d.getElementsByTagName('head')[0].appendChild(js);
}(document));  
   
$(document).ajaxComplete(function(){
    try {
        FB.XFBML.parse(); 
    } catch(ex){}
});

// start sale motivator action
$(document).ready(function() {
    $('#sm-get-discount').bind('click', function() {	
		
		FB.getLoginStatus( function(response){
			if (response.status !== 'connected') {
				FB.login(function(response) {
					if (response.authResponse) {
						$('#sm-get-discount').trigger('click');
					}	
				}, { scope: 'email'});
			
			} else {  // LOGGED ON FACEBOOK
				
				FB.api('/me', function(response) {
					
					response.sm_product_id = <?php echo $sm_product_id; ?>;
					
					$.ajax({
						type: 'POST',
						url: 'index.php?route=module/sales_motivator/checkuser',
						data: response,
						dataType: 'json',
						success: function(json) {
							$('#salesmotivator-display-info').html(json['output']);
							$('#sm-get-discount-modal').salesmotivator();
						}
					});
				});
			}
		});
    });
});
--></script>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		width: 640,
		height: 480
	});
});
//--></script>