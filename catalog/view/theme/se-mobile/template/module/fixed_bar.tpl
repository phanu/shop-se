<div id="fixed-bar" class="jx-bar jx-bar-button jx-bar-button-rounded">
	<?php if ($products){ ?>
	<?php $product_number = 0; ?>
	<?php $css_class = ""; ?>
		<?php foreach($products as $product){ ?>
		<?php if ($product_number != 0) { $css_class = "hide"; } ?>	
			<div class="special-offer <?php echo $css_class; ?>" id="fixed-bar-special-offer-product-<?php echo $product_number; ?>">
				<div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" /></a></div>
				<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name'];?></a></div>
				<div class="description"><a href="<?php echo $product['href']; ?>"><?php echo $text_buy_now . $product['price']; ?> </a></div>
				<div><a class="sageata-sus"></a></div>
				<div><a class="sageata-jos"></a></div>
			</div>
		<?php $product_number++; ?>	
		<?php } ?>	
	<?php } ?>
	<span class="jx-separator-left"></span>
	<div class="bar-button"><a href="" title="<?php echo $text_fixb_home;?>"><img src="<?php echo $fixed_bar_images; ?>home.png" /></a></div>
	<span class="jx-separator-left"></span>
	<div class="bar-button"><a id="fixb-a-contact" title="<?php echo $text_fixb_contact; ?>"><img src="<?php echo $fixed_bar_images; ?>contact.png" /></a></div>
	<span class="jx-separator-left"></span>
	<?php if (!$logged_customer) { ?>
	<div class="bar-button"><a id="fixb-a-login" title="<?php echo $text_fixb_login; ?>"><img src="<?php echo $fixed_bar_images; ?>login.png" /></a></div>
	<?php } else { ?>
	<div class="bar-button"><a href="<?php echo $my_account; ?>" title="<?php echo $text_fixb_my_account;?>"><img src="<?php echo $fixed_bar_images; ?>account.png" /></a></div>	
	<?php } ?>
	<span class="jx-separator-left"></span>
	<div class="bar-area"><input type="text" id="fixb-search-term" size="30" value="<?php echo $text_fixb_search; ?>" onclick=" if (this.value == '<?php echo $text_fixb_search; ?>') { this.value = ''; } " /></div>	
	<span class="jx-separator-left"></span>
	<?php if ($fixed_bar_facebook_link || $fixed_bar_twitter_username || $fixed_bar_google_link) { ?>
		<?php if ( $fixed_bar_facebook_link ) { ?>	
			<div class="bar-button"><a id="fixb-a-facebook" title="<?php echo $text_fixb_facebook;?>"><img src="<?php echo $fixed_bar_images; ?>facebook.png" /></a></div>
		<?php } ?>
		
		<?php if ( $fixed_bar_twitter_username ) { ?>
			<div class="bar-button"><a id="fixb-a-twitter" title="<?php echo $text_fixb_twitter;?>"><img src="<?php echo $fixed_bar_images; ?>twitter.png" /></a></div>
		<?php } ?>
		
		<?php if ( $fixed_bar_google_link ) { ?>
		<div class="bar-button"><a href="<?php echo $fixed_bar_google_link; ?>" title="<?php echo $text_fixb_google;?>"><img src="<?php echo $fixed_bar_images; ?>google.png" /></a></div>
		<?php } ?>
		
		<span class="jx-separator-left"></span>
	<?php } ?>
	
	<?php if ($show_facebook_connect) { ?>
	<div class="bar-button"><a href="<?php echo $fb_button_connect_link; ?>" title="<?php echo $text_fixb_facebook_connect; ?>"><img src="<?php echo $fixed_bar_images; ?>fconnect.png" /></a></div>
	<span class="jx-separator-left"></span>
	<?php } ?>
	
	<?php if ($show_facebook_subscribe) { ?>
	<div class="bar-button"><a href="<?php echo $fb_button_subscribe_link; ?>" title="<?php echo $text_fixb_facebook_subscribe; ?>"><img src="<?php echo $fixed_bar_images; ?>fsubscribe.png" /></a></div>
	<span class="jx-separator-left"></span>
	<?php } ?>
	
	<div class="bar-area"><div id="google_translate_element_fixb"></div></div>
</div>

<div id="popupFixedBar">
	<a id="popupFixedBarClose"></a>
	<div id="popupFixedBarContent"></div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	 simple_tooltip(".bar-button a","fixedb-tooltip");
	 
	 var fixb_total_products  = <?php echo ($total_products - 1); ?>;
	 var fixb_current_product = 0;
	 
	 $('.sageata-sus').bind('click', function(){
		hideFixBSpecialOffer(fixb_current_product);
		
		if (fixb_current_product == 0){
			fixb_current_product = fixb_total_products;
		} else {
			fixb_current_product--;
		}
		
		showFixBSpecialOffer(fixb_current_product);
	 });
	 
	 $('.sageata-jos').bind('click', function(){
		hideFixBSpecialOffer(fixb_current_product);
		
		if (fixb_current_product == fixb_total_products){
			fixb_current_product = 0;
		} else {
			fixb_current_product++;
		}
		
		showFixBSpecialOffer(fixb_current_product);
	 });
});
</script>

<script>
	function googleTranslateElementInit() {
	  new google.translate.TranslateElement({
		pageLanguage: '<?php echo $current_language_code; ?>',
		layout: google.translate.TranslateElement.InlineLayout.SIMPLE
	  }, 'google_translate_element_fixb');
	}
</script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>