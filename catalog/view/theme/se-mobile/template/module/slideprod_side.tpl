<div id="page_effect" style="display:none;">
<div id="features" style="text-align:center;">
      <?php foreach ($products as $product) { ?>
      <div title="">
        <?php if ($product['thumb']) { ?>
        <a href="<?php echo $product['href']; ?>"><img style="float:none;margin:0 auto;padding:0;margin-top:8px;" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a>
        <?php } ?>
        <a id="prodtitle" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
        <?php if ($product['price']) { ?> 
          <?php if (!$product['special']) { ?>     
          <span class="prices-side"><?php echo $product['price']; ?></span>      
          <?php } else { ?>
          <span class="prices-old-side"><?php echo $product['price']; ?></span><span class="prices-new-side"><?php echo $product['special']; ?></span> 
          <?php } ?> 
        <?php } ?>
        <?php if ($product['rating']) { ?>
        <img class="rating" style="float:none;margin:0 auto;background:none;padding:0;display:block;border:none;" src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" />
        <?php } ?>
        <a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" style="margin-top:4px;margin-bottom:8px;"><span><?php echo $button_cart; ?></span></a>
      </div>
      <?php } ?>
</div>
</div>
<style type="text/css">
#features, #slidingFeatures, #labelFeatures, #basicFeatures, #thumbFeatures {
	border:<?php echo $mainbordersize ?> solid #<?php echo $mainborder ?> ;
	-webkit-border-top-left-radius: <?php echo $bannerboxcorner ?>px;
	-webkit-border-top-right-radius: <?php echo $bannerboxcorner ?>px;
	-moz-border-radius-topleft: <?php echo $bannerboxcorner ?>px;
	-moz-border-radius-topright: <?php echo $bannerboxcorner ?>px;
	-khtml-border-top-left-radius: <?php echo $bannerboxcorner ?>px;
	-khtml-border-top-right-radius: <?php echo $bannerboxcorner ?>px;
	border-top-left-radius: <?php echo $bannerboxcorner ?>px;
	border-top-right-radius: <?php echo $bannerboxcorner ?>px;
	}
.jshowoff div {	
	<?php  if ($gradientbgcolor == 'blackgrad-bottom' || $gradientbgcolor == 'whitegrad-bottom' ) { ?>
	background: #<?php echo $bgcolor ?> url(catalog/view/javascript/jquery/slideprod/images/<?php echo $gradientbgcolor ?>.png) bottom repeat-x;	
	<?php } elseif ($gradientbgcolor == 'centerglare') { ?>
	background: #<?php echo $bgcolor ?> url(catalog/view/javascript/jquery/slideprod/images/<?php echo $gradientbgcolor ?>.png) center repeat-y;
	<?php } else { ?>
	background: #<?php echo $bgcolor ?> url(catalog/view/javascript/jquery/slideprod/images/<?php echo $gradientbgcolor ?>.png) top repeat-x;
	<?php } ?>
	}
.jshowoff img {
	-webkit-border-radius: <?php echo $imageprodcorner ?>px;
	-moz-border-radius: <?php echo $imageprodcorner ?>px;
	-khtml-border-radius: <?php echo $imageprodcorner ?>px;
	border-radius: <?php echo $imageprodcorner ?>px;
	border:#<?php echo $imageprodborder ?> solid <?php echo $imageprodbordersize ?>;
	}	
span.prices-side {
	color:#<?php echo $pricetextcolor ?>;
} 	
span.prices-old-side {
	color:#<?php echo $priceoldtextcolor ?>;
}
span.prices-new-side {
	color:#<?php echo $pricetextcolor ?>;
}
a.buttons-cart {
	background: #<?php echo $bgbuttoncart ?>;
	color:#<?php echo $addcarttextcolor ?>;
	border:<?php echo $addcartbordersize ?> solid #<?php echo $addcartborder ?>;
	-webkit-border-radius: <?php echo $addcartcorner ?>px;
	-moz-border-radius: <?php echo $addcartcorner ?>px;
	-khtml-border-radius: <?php echo $addcartcorner ?>px;
	border-radius: <?php echo $addcartcorner ?>px;
}	
p.jshowoff-controls {
	background: #<?php echo $bottombgcolor ?> url(catalog/view/javascript/jquery/slideprod/images/<?php echo $gradientbottombgcolor ?>.png) top repeat-x;
	-moz-border-radius-bottomleft: <?php echo $bannerboxcorner ?>px;
	-moz-border-radius-bottomright: <?php echo $bannerboxcorner ?>px;
	-webkit-border-bottom-left-radius: <?php echo $bannerboxcorner ?>px;
	-webkit-border-bottom-right-radius: <?php echo $bannerboxcorner ?>px;
	-khtml-border-bottom-left-radius: <?php echo $bannerboxcorner ?>px;
	-khtml-border-bottom-right-radius: <?php echo $bannerboxcorner ?>px;
	border-bottom-left-radius: <?php echo $bannerboxcorner ?>px;
	border-bottom-right-radius: <?php echo $bannerboxcorner ?>px;
	}	
.jshowoff-slidelinks a, .jshowoff-controls a {
	background-color: #<?php echo $bgnavlinks ?>;
	color: #<?php echo $navlinkstextcolor ?>;
	-moz-border-radius: <?php echo $navlinkscorner ?>px;
	-webkit-border-radius: <?php echo $navlinkscorner ?>px;
	-khtml-border-radius: <?php echo $navlinkscorner ?>px;
	border-radius: <?php echo $navlinkscorner ?>px;
	border:<?php echo $navlinksbordersize ?> solid #<?php echo $navlinksborder ?>;
}
a#prodtitle {
	color:#<?php echo $prodnametextcolor ?>;
}
.jshowoff p {
	color:#<?php echo $shortdesctextcolor ?>;
	}	
</style>
<script type="text/javascript">	
$(document).ready(function(){	
	$('#page_effect').fadeIn(2000);
});
</script>


<script type="text/javascript">		
				$(document).ready(function(){ $('#features').jshowoff({
					effect: '<?php echo $effect; ?>',
					hoverPause: <?php echo $hoverpause; ?>,
					speed: <?php echo $speed; ?>,
					changeSpeed: <?php echo $changespeed; ?>,
					controls: <?php echo $controls; ?>,
					links: <?php echo $links; ?>
				}); });
			</script>
