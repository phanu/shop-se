<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
    <div id="qatc_fields">
	  <?php echo $entry_model; ?><br />
	  <input type="text" name="qatc_search" id="qatc_search" value="" <?php if ($show_qty) { ?> style="width:60%" <?php } ?> />
	  <?php if ($show_qty == 'text') { ?> x
	  <input type="text" name="qatc_quantity" id="qatc_quantity" value="" style="width:20%" />
	  <?php } elseif ($show_qty == 'select') { ?> x
	  <select name="qatc_quantity" id="qatc_quantity" style="width:25%;"><?php for($i=1;$i<=9;$i++){ ?><option value="<?php echo $i; ?>"><?php echo $i; ?></option><?php } ?></select>
	  <?php } ?>
	  <div class="cart"><a id="qatc_submit" onclick="qaddToCart(); $('#qatc_search').select(); return false;" class="button"><span><?php echo $button_cart; ?></span></a></div>
    </div>
  </div>
</div>
<script type="text/javascript">
function qaddToCart() {
	$.ajax({
		url: 'index.php?route=module/quick_atc/callback',
		type: 'post',
		data: $('#qatc_fields input, #qatc_fields select'),
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information, .error').remove();

			if (json['redirect']) {
				location = json['redirect'];
			}

			if (json['error']) {
				if (json['error']['warning']) {
					$('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

					$('.warning').fadeIn('slow');

					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}
			}

			if (json['success']) {

				//addToCart(json['product_id'], $('#qatc_quantity').val());
				addToCartQty(json['product_id'], $('#qatc_quantity').val());
				$('#qatc_quantity').val(0);

				//$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

				//$('.success').fadeIn('slow');

				//$('#cart_total').html(json['total']);

				//$('html, body').animate({ scrollTop: 0 }, 'slow');
			}
		}
	});
}

function addToCartQty(product_id, quantity) {
	quantity = typeof(quantity) != 'undefined' ? quantity : 1;

	<?php if (defined('VERSION') && (version_compare(VERSION, '1.5.2', '<') == true)) { //151x ?>
		urlx = 'index.php?route=checkout/cart/update';
	<?php } else { //v152x ?>
		urlx = 'index.php?route=checkout/cart/add';
	<?php } ?>

	$.ajax({
		//url: 'index.php?route=checkout/cart/update',
		url: urlx,
		type: 'post',
		data: 'product_id=' + product_id + '&quantity=' + quantity,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information, .error').remove();

			if (json['redirect']) {
				location = json['redirect'];
			}

			if (json['error']) {
				if (json['error']['warning']) {
					$('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

					$('.warning').fadeIn('slow');

					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}
			}

			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

				$('.success').fadeIn('slow');

				$('#cart_total').html(json['total']); //v151x

				$('#cart-total').html(json['total']); //v152x

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}
		}
	});
}

$('#qatc_fields input').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#qatc_submit').click();
	}
});
</script>