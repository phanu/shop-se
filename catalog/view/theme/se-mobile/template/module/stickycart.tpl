<div class="box" id="stickycart">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
  <span id="stickycart_total"></span>
  <span id="stickycart_checkout"></span>
  </div>
</div>
<script src="catalog/view/javascript/stickycart/js/jquery.easing.1.3.js"></script>
<script src="catalog/view/javascript/stickycart/js/stickysidebar.jquery.js"></script>
<script type="text/javascript">
  $(function () {
  
	$("#stickycart").stickySidebar({
		timer: 400
	  , easing: "easeInOutBack"
	});
		
		
	$(document).ajaxComplete(function (event, request, settings){
		if(settings.url == 'index.php?route=checkout/cart/add'){
			$('#stickycart_total').html($('#cart-total').html());
			$('#stickycart_checkout').html('<?php echo $stickycart_checkout?>');
		}
	});
		
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		dataType: 'json',
		success: function(json) {
			if (json['output']) {
				$('#stickycart_total').html(json['total']);
				$('#stickycart_checkout').html('<?php echo $stickycart_checkout?>');
			}
		}
	});	
		
  })
</script>