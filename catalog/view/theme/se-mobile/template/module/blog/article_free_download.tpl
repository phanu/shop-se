<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
 <div class="box-product" align="center">
 		<?php foreach ($downloads as $download) { ?>
      <div>
        <?php if ($download['thumb']) { ?>
       <div class="image"><a href="<?php echo $download['href']; ?>" title="<?php echo $download['mask']; ?>"><img src="<?php echo $download['thumb']; ?>" alt="<?php echo $download['mask']; ?>" /></a></div>
        <?php } ?>
        <div class="name"><a style="font-weight:normal" href="<?php echo $download['href']; ?>"><?php echo $download['name']; ?></a></div>
       </div>
      <?php } ?>
      
      </div>
  </div>
</div>
