<div class="box">
  <div class="box-heading"><a href="<?php echo $author_list;?>"><?php echo $heading_title; ?></a> </div>
  <div class="box-content">
  <div class="box-category">    
  <?php if($style=='list'){?> 
      <ul class="box-category">
      <?php foreach ($authors as $author) { ?>
      <li><a href="<?php echo $author['href']; ?>"><?php echo $author['author']; ?></a></li>
      <?php } ?>
    </ul>
    <?php }else{ ?>
     <select name="author" onchange="window.location.href=this.options [this.selectedIndex].value">
      <option>--Select an Author--</option>
      <?php foreach ($authors as $author) { ?>
        <?php if($author['author_id']==$author_id){?> 
      <option value="<?php echo $author['href']; ?>" selected="selected"><?php echo $author['author']; ?></option>
      <?php }else{ ?>      
      <option value="<?php echo $author['href']; ?>"><?php echo $author['author']; ?></option>
       <?php } ?>
       <?php } ?>
    </select>
    <?php } ?>
    <p style="margin:0; text-align:right; padding-top:10px"><a style="text-decoration:none;" href="<?php echo $author_list;?>"><?php echo $text_all_author; ?></a></p>
     <ul>
      <?php if (!$logged) { ?>
      <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a> / <a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
      <li><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></li>
      <?php } ?>
      <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
      <?php if ($logged) { ?>
      <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
      <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
      <?php } ?>
      <?php if ($logged) { ?>
      <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
      <?php } ?>
    </ul>
    </div>
  </div>
</div>
