<div class="box">
  <div class="box-heading"><img src="catalog/view/javascript/news/shout.png" width="16" height="16" alt="" /> <?php echo $heading_title; ?></div>
  <div class="box-content sbox">
  
   <div id="view_message" style="min-height:300px"></div><br/> 
   <div id="shout_alert"></div>
   <?php if(!$logged&&$config_login==1){ echo $text_please_login; }else{ ?>  
  <span><?php echo $entry_your_name;?></span> 
  <input type="text" name="name" value="<?php echo $name;?>" style="width:140px;"/><br>
  <span><?php echo $entry_message;?></span> 
  <input type="text" name="message" value="" style="width:140px;" /><br> 
  
  <?php if($config_captcha==1){?>
  <span><?php echo $entry_captcha;?></span>
  <input type="text" name="captcha" value="" style="width:140px;"><br> 
  <p align="center"><a onclick="return change_captcha('captcha');" title="Reload"><?php echo $text_reload_captcha;?></a></p> 
  <img src="index.php?route=news/shoutbox/captcha" alt="Captcha" id="captcha" width="165" height="35"/><br> 
	<?php } ?>
  <p align="center"><a id="button_post_message" class="button"><?php echo $button_post_message;?> </a></p>
  <?php } ?>  
   <?php if($user_logged){?>  
   <input type="hidden" name="shout_username" value="<?php echo $user_name;?>"/>
   <input type="hidden" name="shout_message" value="<?php echo $user_message;?>"/>
   <p><a id="button_clear_message"><?php echo $text_clear_message;?></a></p>
  <?php } ?>  
 
</div>
</div>
<script type="text/javascript">//<![CDATA[
function view_shout(){ 
	$('#view_message').load('index.php?route=news/shoutbox');
	setTimeout("view_shout()", <?php echo $reload_time;?>);
}view_shout();
<?php if($config_captcha){?>
/*post_message*/ 
$('#button_post_message').bind('click',function(){
	function changecaptcha(){
		var obj=document.getElementById("captcha");
		var src=obj.src;
		var date=new Date();
		obj.src=src + '&' + date.getTime();
	}
	function clear_data(){ 
		$('input[name=\'name\']').val('');
		$('input[name=\'message\']').val('');
		$('input[name=\'captcha\']').val('');
			
	}
	function viewshout(){
		$('#view_message').load('index.php?route=news/shoutbox');
	}
		$.ajax({
		url: 'index.php?route=news/shoutbox/write',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&message=' + encodeURIComponent($('input[name=\'message\']').val()) + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function(){ 
		$('.success, .warning').remove();
			$('#button_post_message').attr('disabled',true);
			$('#shout_alert').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt=""/>Please wait...</div>');
		},
		complete: function(){ 
		$('#button_post_message').attr('disabled',false);
			$('.attention').remove();
		},
		success: function(data){
			if(data['error']){ 
		$('#shout_alert').after('<div class="warning">' + data['error']+'</div>');}

		if(data['success']){ 
		$('#shout_alert').after('<div class="success">' + data['success']+'</div>');
				viewshout();
				clear_data();
				changecaptcha();
			}
		}
	});
});
function change_captcha(id){
var obj=document.getElementById(id);
var src=obj.src;
var date=new Date();
obj.src=src + '&' + date.getTime();
return false;}
<?php }else {?>
/*post_message*/ 
$('input[name=\'message\']').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#button_post_message').trigger('click');
	}
});
$('#button_post_message').bind('click',function(){
	function clear_data(){ 
		$('input[name=\'name\']').val('');
		$('input[name=\'message\']').val('');			
	}
	function viewshout(){
		$('#view_message').load('index.php?route=news/shoutbox');
	}
		$.ajax({
		url: 'index.php?route=news/shoutbox/write',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&message=' + encodeURIComponent($('input[name=\'message\']').val()),
		beforeSend: function(){ 
		$('.success, .warning').remove();
			$('#button_post_message').attr('disabled',true);
			$('#shout_alert').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt=""/>Please wait...</div>');
		},
		complete: function(){ 
		$('#button_post_message').attr('disabled',false);
			$('.attention').remove();
		},
		success: function(data){
			if(data['error']){ 
		$('#shout_alert').after('<div class="warning">' + data['error']+'</div>');}

		if(data['success']){ 
		$('#shout_alert').after('<div class="success">' + data['success']+'</div>');
				viewshout();
				clear_data();
			}
		}
	});
});
<?php } ?>
<?php if($user_logged){?>$('#button_clear_message').bind('click',function(){
		$.ajax({
		url: 'index.php?route=news/shoutbox/clear_message',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'shout_username\']').val()) + '&message=' + encodeURIComponent($('input[name=\'shout_message\']').val()),
		beforeSend: function(){ 
		$('.success, .warning').remove();
			$('#button_clear_message').attr('disabled',true);
			$('#shout_alert').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt=""/><?php echo $text_wait;?></div>');
		},
		complete: function(){ 
		$('#button_clear_message').attr('disabled',false);
			$('.attention').remove();
		},
		success: function(data){
			if(data['error']){ 
		$('#shout_alert').after('<div class="warning">' + data['error']+'</div>');}

		if(data['success']){ 
		$('#shout_alert').after('<div class="success">' + data['success']+'</div>');
			}
		}
	});
});
<?php } ?>
//]]></script>