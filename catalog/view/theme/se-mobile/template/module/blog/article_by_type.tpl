<style>
  .description .info{padding-bottom:5px; }
  .description .info *{ text-align:right;font-size:11px; vertical-align: middle;text-decoration:none; }
  .description .info span{margin-right:5px;}
  .description .info span.date{ background:url(catalog/view/theme/default/image/news/date.png) left bottom no-repeat; padding-left:15px;}
  .description .info span.author{ background:url(catalog/view/theme/default/image/news/author.png) left bottom no-repeat; padding-left:15px;}
  .description .info span.comment{ background:url(catalog/view/theme/default/image/news/comment.png) left bottom no-repeat; padding-left:15px;}
  .description .info span.viewed{ background:url(catalog/view/theme/default/image/news/viewed.png) left bottom no-repeat; padding-left:15px;}
  	#column-left .description .info,
   #column-right .description .info{ display:none;}
  #article_list .cart, #article_list .wishlist, #article_list .compare{display:none;}
  </style>

<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content" id="article_list">
    <div class="product-list" style="overflow:hidden;">
      <?php foreach ($articles as $article) { ?>
      <div style="min-width:<?php echo $width;?>px">
        <?php if ($article['thumb']) { ?>
       <div class="image" style="margin-bottom:5px;"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" /></a></div>
        <?php } ?>    
        <div id="type<?php echo $module;?>" class="name"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>
         <div class="description">
    <div class="info">
    <span class="author"><a href="<?php echo $article['author_href']; ?>" title="<?php echo $text_author;?> <?php echo $article['author']; ?>"><?php echo $article['author']; ?></a></span>
    <span class="date"><i><?php echo $article['date_added']; ?></i></span>
    <span class="comment"><?php echo $article['comments']; ?></span>
    <span class="viewed"><?php echo $article['viewed']; ?></span>
     </div>   
                            <?php echo $article['description']; ?><br> 
       
           <a style="float:right" href="<?php echo $article['href']; ?>" class="button"><span><?php echo $text_more;?></span></a>
          
        </div>
        
    <!--related --> 
   <div class="related">
        <?php if($article['related']){?>
        <span style="color:#999;font-size:11px;"><?php echo $text_related;?></span>
        <ul> 
        <?php foreach ($article['related'] as $relate) { ?>
                  <li><a href="<?php echo $relate['href'];?>" style="font-size:11px;  text-decoration:none;"><?php echo $relate['name']; ?></a></li> 
        <?php } ?>
        </ul>
        <?php } ?>   
     </div> 
    <!--end related --> 
      </div>
      <?php } ?>
    </div>
  </div>
</div>
