<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content" style="display:block; min-height:<?php echo $height+130;?>px;">
		  <script>
                jQuery(function(){
                    
                    jQuery('#blog_slide<?php echo $module; ?>').camera({
                        loader: 'bar',
						height:'<?php echo $height+60;?>px',
                        fx: 'scrollLeft', 
                        pagination: false,
                        thumbnails: true
                    });
        
                });
            </script>
        <div class="camera_wrap camera_emboss" id="blog_slide<?php echo $module; ?>">
        
                <?php foreach($articles as $article){?>
            <div data-thumb="<?php echo $article['thumb5'];?>" data-src="<?php echo $article['thumb'];?>" data-time="1500" data-trasPeriod="4000" data-link="<?php echo $article['href'];?>" data-target="_self">
                <div class="camera_caption fadeFromBottom">
                <h3><a href="<?php echo $article['href'];?>"><?php echo $article['name'];?></a></h3>
                 <?php echo $article['description'];?>
                </div>
            </div>    
              <?php }  ?>	   		       
        </div>    
 </div> 
</div>