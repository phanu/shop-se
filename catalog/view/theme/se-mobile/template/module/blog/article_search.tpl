<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
  <div>
            <input type="text" value="<?php echo $filter_name; ?>" name="article_search" onclick="this.value = '';" onkeydown="this.style.color = '#000000'" style="color: #999; width:152px;" />
           <br/> 
             <select name="filter_article_category_id" id="filter_article_category_id" style="width:158px;position:static;z-index:-1;">
        <option value="0"><?php echo $text_category; ?></option>
        <?php foreach ($categories as $category_1) { ?>
        <?php if ($category_1['article_category_id'] == $filter_article_category_id) { ?>
        <option value="<?php echo $category_1['article_category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $category_1['article_category_id']; ?>"><?php echo $category_1['name']; ?></option>
        <?php } ?>
        <?php foreach ($category_1['children'] as $category_2) { ?>
        <?php if ($category_2['article_category_id'] == $filter_article_category_id) { ?>
        <option value="<?php echo $category_2['article_category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $category_2['article_category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
        <?php } ?>
        <?php foreach ($category_2['children'] as $category_3) { ?>
        <?php if ($category_3['article_category_id'] == $filter_article_category_id) { ?>
        <option value="<?php echo $category_3['article_category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $category_3['article_category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
        <?php } ?>
        <?php } ?>
        <?php } ?>
        <?php } ?>
      </select><br/> <a href="<?php echo $advanced; ?>" style="float:left; margin:5px 0 0 5px;"><?php echo $text_advanced; ?></a>
  <a onclick="articleSearch();" id="article-search" class="button" style=" float:right; margin:5px 5px 0 0;"><span><?php echo $button_go; ?></span></a> 
  
     </div><div style="clear:both"></div>
  </div>
</div>
<script type="text/javascript">
$('input[name=\'article_search\']').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#article-search').trigger('click');
	}
});
 function articleSearch() {
	url = 'index.php?route=news/search';
	
	var filter_name = $('input[name=\'article_search\']').attr('value');
	
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_article_category_id = $('select[name=\'filter_article_category_id\']').attr('value');
	
	if (filter_article_category_id > 0) {
		url += '&filter_article_category_id=' + encodeURIComponent(filter_article_category_id);
	}

	location = url;
};
</script>