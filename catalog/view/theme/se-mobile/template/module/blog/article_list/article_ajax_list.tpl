<div class="product-list">
<?php if($articles){?>
  <?php foreach ($articles as $article) { ?>
          <div style="min-width:<?php echo $width;?>px"> 
        <?php if ($article['thumb']) { ?> <div class="image"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" /></a></div>
<?php } ?>
	
	<div class="name"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>
	
	<div class="description">
    
    <div class="info">
    <span class="author"><a href="<?php echo $article['author_href']; ?>" title="<?php echo $text_author;?> <?php echo $article['author']; ?>"><?php echo $article['author']; ?></a></span>
    <span class="date"><i><?php echo $article['date_added']; ?></i></span>
    <span class="comment"><?php echo $article['comments']; ?></span>
    <span class="viewed"><?php echo $article['viewed']; ?></span>
     </div>    
    <?php echo $article['description']; ?>    
    <a style="float:right; margin:5px 20px 0 0;" href="<?php echo $article['href']; ?>" class="button"><span><?php echo $text_more; ?></span></a> 
    
    </div>
   <!--related --> 
        <?php if($article['related']){?>
     <div class="related">  
        <span style="color:#999;font-size:11px;"><?php echo $text_related;?></span>
        <ul> 
        <?php foreach ($article['related'] as $relate) { ?>
                  <li><a href="<?php echo $relate['href'];?>" style="font-size:11px;  text-decoration:none;"><?php echo $relate['name']; ?></a></li> 
        <?php } ?>
        </ul> 
    </div>
        <?php } ?>   
    <!--end related -->  
	</div> 
      <?php } ?>
      

 <?php } ?>
 </div>
<div class="pagination" style=" width:97%;clear:both"><?php echo $pagination; ?></div>