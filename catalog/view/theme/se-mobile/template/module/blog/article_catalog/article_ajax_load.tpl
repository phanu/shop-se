  <style>.htabs {margin-top:2px;}
  .htabs a{display:block;}
    .description .info{padding-bottom:5px; }
  .description .info *{ text-align:right;font-size:11px; vertical-align: middle;text-decoration:none; }
  .description .info span{margin-right:5px;}
  .description .info span.date{ background:url(catalog/view/theme/default/image/news/date.png) left bottom no-repeat; padding-left:15px;}
  .description .info span.author{ background:url(catalog/view/theme/default/image/news/author.png) left bottom no-repeat; padding-left:15px;}
  .description .info span.comment{ background:url(catalog/view/theme/default/image/news/comment.png) left bottom no-repeat; padding-left:15px;}
  .description .info span.viewed{ background:url(catalog/view/theme/default/image/news/viewed.png) left bottom no-repeat; padding-left:15px;}
 .box-content .product-list{text-align:left;}
   #content .product-list .related ul{margin-left:<?php echo $width;?>px;}   
  #box_content<?php echo $module;?> .cart, #box_content<?php echo $module;?> .wishlist, #box_content<?php echo $module;?> .compare{display:none;}
  </style>
  <div class="box">
  <div class="box-heading" id="najax_heading<?php echo $module;?>"> <a rel="nofollow" href="<?php echo $href; ?>"><?php echo $heading_title; ?></a></div>
 <?php if ($categories) { ?>
  <div class="htabs" id="najax_tab<?php echo $module;?>">
   <?php foreach ($categories as $category) { ?>
       <?php if ($category['total']>0){ ?>
              <a rel="nofollow" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
        <?php } ?>
   <?php } ?>
  </div>
  <?php } ?>
  <div class="box-content" id="box_content<?php echo $module;?>">
    <div class="product-list">
    <?php if($articles){?>
      <?php foreach ($articles as $article) { ?>
          <div style="min-width:<?php echo $width;?>px">
        <?php if ($article['thumb']) { ?> <div class="image"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" /></a></div>
<?php } ?>
	
	<div class="name"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>
	
	<div class="description">
    
    <div class="info">
    <span class="author"><a href="<?php echo $article['author_href']; ?>" title="<?php echo $text_author;?> <?php echo $article['author']; ?>"><?php echo $article['author']; ?></a></span>
    <span class="date"><i><?php echo $article['date_added']; ?></i></span>
    <span class="comment"><?php echo $article['comments']; ?></span>
    <span class="viewed"><?php echo $article['viewed']; ?></span>
     </div>    
    <?php echo $article['description']; ?>    
    <a style="float:right; margin:5px 20px 0 0;" href="<?php echo $article['href']; ?>" class="button"><span><?php echo $text_more; ?></span></a> 
    
    </div>
  <!--related --> 
        <?php if($article['related']){?>
     <div class="related">  
        <span style="color:#999;font-size:11px;"><?php echo $text_related;?></span>
        <ul> 
        <?php foreach ($article['related'] as $relate) { ?>
                  <li><a href="<?php echo $relate['href'];?>" style="font-size:11px;  text-decoration:none;"><?php echo $relate['name']; ?></a></li> 
        <?php } ?>
        </ul> 
    </div>
        <?php } ?>   
    <!--end related -->  
	</div> 
      <?php } ?>
      <?php } ?>
    </div>    
    
      <div class="pagination" style=" width:97%;clear:both"><?php echo $pagination; ?></div>
  <?php if (!$articles) { ?>
  <div class="nocontent"><?php echo $text_empty; ?></div>
  <?php } ?>
  
  </div>
</div>
<script type="text/javascript"><!--
$('#najax_heading<?php echo $module;?> a').live('click', function() {
	$('#najax_tab<?php echo $module;?> a').removeClass("selected");
	$('#box_content<?php echo $module;?>').load(this.href);	
	return false;
});		
$('#najax_tab<?php echo $module;?> a').live('click', function() {
$('#najax_tab<?php echo $module;?> a').removeClass("selected");
 	$(this).addClass("selected");	
	$('#box_content<?php echo $module;?>').load(this.href);	
	return false;
});		
$('#box_content<?php echo $module;?> .pagination a').live('click', function() {
	$('#box_content<?php echo $module;?>').load(this.href);	
	return false;
});	
//--></script>