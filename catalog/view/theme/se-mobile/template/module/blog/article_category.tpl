<?php if($categories) { ?>
<div class="box">
  <div class="box-heading"><?php if($href){?><a href="<?php echo $href;?>"><?php echo $heading_title; ?></a><?php }else {?><?php echo $heading_title; ?><?php } ?></div>
  <style>
  .box-heading>a{text-decoration:none; font-size:14px; color:#333;}
  </style>
  <div class="box-content">
    <div class="box-category">
      <ul class="box-category">
        <?php foreach ($categories as $category) { ?>
        <li>
          <?php if ($category['article_category_id'] == $article_category_id) { ?>
          <a href="<?php echo $category['href']; ?>" class="active"><?php echo $category['name']; ?></a>
          <?php } else { ?>
          <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
          <?php } ?>
          <?php if ($category['children']) { ?>
          <ul>
            <?php foreach ($category['children'] as $child) { ?>
            <li>
              <?php if ($child['article_category_id'] == $child_id) { ?>
              <a href="<?php echo $child['href']; ?>" class="active"> - <?php echo $child['name']; ?></a>
              <?php } else { ?>
              <a href="<?php echo $child['href']; ?>"> - <?php echo $child['name']; ?></a>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          <?php } ?>
        </li>
        <?php } ?>
      </ul>
    </div>
  </div>
</div>
 <?php } ?>