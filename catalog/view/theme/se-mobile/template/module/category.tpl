<!-- Category Section --> 
      <div class="page-block margin-bottom">
        <h2 class="block-title">
          <span><?php echo $heading_title; ?></span><!-- <span> tag to make blue border on this text only -->
          <a href="#" class="list-all">
            <i class="fa fa-th-list"></i>
          </a>
        </h2>

        <!-- Category Listing -->
        <ol class="category-list">

          <?php $i=1; foreach ($categories as $category) { ?>
          <li><!-- Category list item # -->
            <div class="thumb">
              <a href="<?php echo $category['href']; ?>">
                <img src="catalog/view/theme/se-mobile/maincategory/<?php echo $i; ?>.svg" alt="<?php echo $category['name']; ?>">
              </a>
            </div>
            <div class="category-ctn">
              <div class="cat-name">
                <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
              </div>
            </div>
          </li><!-- End Category list item # -->
          <?php $i++; } ?>

        </ol>

        <div class="clear"></div><!-- Use this class (.clear) to clearing float -->

      </div>
<!-- End Category Section -->