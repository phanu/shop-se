<div class="featured-slider animated fadeInRight">
      <?php foreach ($banners as $banner) { ?>
      <?php if ($banner['link']) { ?>
    <a href="<?php echo $banner['link']; ?>">
      <?php } ?>
      <div class="featured-item"><!-- 1. Featured Slider Item -->
        <div class="thumb" >
          <img src="<?php echo $banner['image']; ?>">
        </div>
      </div><!-- 1. End Featured Slider Item -->
      <?php if($banner['link']) { ?>
              </a>
      <?php } ?>
      <?php } ?>
      </div>