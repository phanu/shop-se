<style type="text/css" rel="stylesheet">
.clearfix:after { 
  content: "."; 
  display: block; 
  height: 0; 
  clear: both; 
  visibility: hidden; 
}
.clearfix { 
  display: inline-block;  
}
* html .clearfix {  
  height: 1%;  
} /* Hides from IE-mac \*/
.clearfix {  
  display: block;  
}

.text-left { text-align: left; }
.text-center { text-align: center; }
.text-right { text-align: right; }

.success_page {
	width: 100%;
	padding: 0;
	margin: 0;
	background-color: transparent;
	background-image: none;
	font-size: 12px;
}

.success_page, .success_page * {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	line-height: 1.42857143;
}

.success_page .SP_sections {
	width: 100%;
	position: relative;
	vertical-align: top;
}

.success_page .SP_sections .SP_section_wrapper {
	
}

@media (max-width: 767px) {
	.success_page .SP_sections .SP_section_wrapper .SP_table_responsive {
		width: 100%;
		margin-bottom: 12px;
		overflow-y: hidden;
		-ms-overflow-style: -ms-autohiding-scrollbar;
		border: 1px solid #eee;
	}
}

.success_page .SP_sections .SP_section_wrapper .SP_table_responsive {
	overflow-x: auto;
}

.success_page .SP_sections .SP_section_social ul {
	margin: 0px;
	padding: 0px;
	width: 100%;
	min-height: 30px;
	list-style-type: none;
}

.success_page .SP_sections .SP_section {
	vertical-align: top;
}

.success_page .SP_sections .SP_section_social ul li {
	display: inline-block;
}

.success_page .SP_sections .SP_section .SP_section_wrapper h2 {
	margin: 0 0 17px 0;
	padding: 0px;
	font-size: 17px;
	font-weight: 600;
	color: #444;
}

.success_page .SP_sections .SP_section .SP_section_wrapper .SP_button {
	background: #229ac8;
	color: #ffffff;
	text-decoration: none;
	display: inline-block;
	margin-top: 10px;
	margin-bottom: 20px;
	padding: 6px 10px;
	cursor: pointer;
	font-size: 13px;
	font-weight: 400;
	border: 1px solid #1f90bb;
	border-radius: 4px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	white-space: no-wrap;
	text-align: center;
}

.success_page .SP_sections .SP_section .SP_section_wrapper .SP_button:hover {
	text-decoration: none;
}

.success_page .SP_sections .SP_section .SP_section_wrapper p {
	margin: 0 0 5px 0;
}

.success_page .SP_sections .SP_section_wrapper table.SP_table {
	width: 100%;
	background-color: transparent;
	border: 1px solid #eee;
	border-collapse: collapse;
	border-spacing: 0;
	margin-bottom: 0px;
}

.success_page .SP_sections .SP_section_wrapper table.SP_table thead td {
	font-weight: bold;
	background-color: #eeeeee;
	padding: 8px;
	vertical-align: top;
}

.success_page .SP_sections .SP_section_wrapper table.SP_table td {
	border: 1px solid #eee;
	background-color: #ffffff;
	padding: 8px;
	vertical-align: top;
}

.success_page .SP_sections .SP_section_wrapper table.SP_table td .SP_image_thumbnail {
	vertical-align: middle;
	padding: 4px;
	line-height: 1.428571429;
	background-color: #ffffff;
	border: 1px solid #dddddd;
	border-radius: 4px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	-webkit-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
	display: inline-block;
	max-width: 100%;
	height: auto;
}

.box .box-content {
	background: #FFFFFF;
	border: none;
	padding: 0px;
}
</style>
<div class="clearfix"></div>
<div id="success_page" class="success_page">
	<div class="SP_sections">
		<?php if ($sections) { ?>
		<?php foreach ($sections as $section) { ?>
		<?php echo $section; ?>
		<?php } ?>
		<?php } ?>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
</div>
<style type="text/css" rel="stylesheet">
<?php echo $setting['css']; ?>
</style>
<?php if ($setting['adwords_status']) { ?>
<script type="text/javascript">
var google_conversion_id = <?php echo $setting['adwords_conversion_id']; ?>;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "<?php echo $setting['adwords_conversion_label']; ?>";
var google_conversion_value = <?php echo round($order_info['total'], 2); ?>;
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display: none; visibility: hidden;">
	<img height="1" width="1" style="border-style: none;" alt="" src="//www.googleadservices.com/pagead/conversion/<?php echo $setting['adwords_conversion_id']; ?>/?value=<?php echo round($order_info['total'], 2); ?>&amp;label=<?php echo $setting['adwords_conversion_label']; ?>&amp;guid=ON&amp;script=0" />
</div>
</noscript>
<?php } ?>
<?php if ($setting['ecommerce_status']) { ?>
<script type="text/javascript">
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', '<?php echo $setting['ecommerce_tracking']; ?>', 'auto');
ga('send', 'pageview');
ga('require', 'ecommerce', 'ecommerce.js');

ga('ecommerce:addTransaction', {
  'id': '<?php echo $order_info['order_id']; ?>',
  'affiliation': '<?php echo addslashes($order_info['store_name']); ?>',
  'revenue': '<?php echo round($order_info['total'], 2); ?>',
  'shipping': '<?php echo round($order_info['shipping_cost'], 2); ?>',
  'tax': '<?php echo round($order_info['order_tax'], 2); ?>'
});

<?php foreach ($order_info['order_product'] as $product) { ?>
ga('ecommerce:addItem', {
  'id': '<?php echo $product['order_id']; ?>',
  'name': <?php echo json_encode(html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8')); ?>,
  'sku': '<?php if (isset($product['sku'])) { ?><?php echo addslashes($product['sku']); ?><?php } else { ?><?php echo addslashes($product['model']); ?><?php } ?>',
  'category': <?php echo json_encode(html_entity_decode($product['category_name'], ENT_QUOTES, 'UTF-8')); ?>,
  'price': '<?php echo round($product['price'], 2); ?>',
  'quantity': '<?php echo $product['quantity']; ?>'
});
<?php } ?>

<?php if (false) { ?>
<?php foreach ($order_info['order_product_options'] as $product) { ?>
ga('ecommerce:addItem', {
  'id': '<?php echo $product['order_id']; ?>',
  'name': <?php echo json_encode(html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8')); ?>,
  'sku': '<?php if (isset($product['sku'])) { ?><?php echo addslashes($product['sku']); ?><?php } else { ?><?php echo addslashes($product['model']); ?><?php } ?>',
  'category': <?php echo json_encode(html_entity_decode($product['category_name'], ENT_QUOTES, 'UTF-8')); ?>,
  'price': '<?php echo round($product['price'], 2); ?>',
  'quantity': '<?php echo $product['quantity']; ?>'
});
<?php } ?>
<?php } ?>

ga('ecommerce:send');
</script>
<?php } ?>
<?php if ($setting['salesmedia_status']) { ?>
<iframe src="https://cubegroup.go2cloud.org/aff_l?offer_id=<?php echo $setting['salesmedia_offer_id']; ?>&adv_sub=<?php echo $order_info['order_id']; ?>&amount=<?php echo round($order_info['total'], 2); ?>" scrolling="no" frameborder="0" width="1" height="1"></iframe>
<?php } ?>
<?php echo html_entity_decode($javascript, ENT_QUOTES, 'UTF-8'); ?>