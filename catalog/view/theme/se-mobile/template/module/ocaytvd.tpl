<?php if ($videos) { ?>
	<div class="box">
		<div class="box-heading"><?php echo $heading_title; ?></div>
		<div class="box-content">
			<div class="box-product2" style="text-align: center; width: 100%;">
				<?php foreach ($videos as $video) { ?>
					<div style="text-align: center; margin: 10px;">
						<?php if ($video['image_width']) { ?>
							<div class="image"><a class="video" rel="video" title="<?php echo $video['name']; ?>" href="http://www.youtube.com/embed/<?php echo $video['video_id']; ?>?fs=1&amp;autoplay=1" /><img width="<?php echo $video['image_width']; ?>" src="http://img.youtube.com/vi/<?php echo $video['video_id']; ?>/2.jpg" title="<?php echo $video['name']; ?>" alt="<?php echo $video['name']; ?>" /></a></div>
						<?php } ?>
						<?php if ($video['name']) { ?>
							<div class="name"><?php echo $video['name']; ?></div>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
<?php } ?>

<?php if ($version >= 152) { ?>
	<script type="text/javascript" src="catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js"></script>
	<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/colorbox/colorbox.css" media="screen">
	<script type="text/javascript">
		$(document).ready(function(){
			$(".video").colorbox({
				iframe		: true,
				innerWidth	: 640,
				innerHeight	: 480
			});
		});
	</script>
<?php } else { ?>
	<script type="text/javascript">
		$(".video").click(function() {
			$.fancybox({
					'padding'		: 0,
					'autoScale'		: false,
					'transitionIn'	: 'none',
					'transitionOut'	: 'none',
					'title'			: this.title,
					'width'			: 680,
					'height'		: 495,
					'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
					'type'			: 'iframe'
				});
			return false;
		});
	</script>
<?php } ?>