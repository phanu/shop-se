<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link href="http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:extralight,light,regular,bold&amp;subset=latin" rel="stylesheet" type="text/css" />
<!-- main stylesheet file is set from controller -->
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />

<script type='text/javascript' src='catalog/view/javascript/jquery/jquery.easing.1.3.js'></script> 
<script type='text/javascript' src='catalog/view/javascript/jquery/jquery.hoverIntent.minified.js'></script>
<script type='text/javascript' src='catalog/view/javascript/jquery/diapo.min.js'></script>

<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie7.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie6.css" />
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->
</head>
<body>
<div class="container">
<?php if ($allow_display) { ?>
  <div style="width:<?php echo $widget_width; ?>px; height:<?php echo $widget_height; ?>px;">	
	<div class="pix_diapo" style="width:<?php echo $widget_width; ?>px; height:<?php echo $widget_height; ?>px;">
		<?php foreach($arr_chunk as $products) { ?>
		<div style="text-align:center;">	
			<div class="product-grid">
			  <?php foreach ($products as $product) { ?>
			  <div>
				<div class="name">
					<?php if ($product['show_title']) { ?>
					<a href="<?php echo $product['href']; ?>" title="<?php echo $product['fullname']; ?>" target="blank"><?php echo $product['name']; ?></a>
					<?php } else { ?>
					<a href="<?php echo $product['href']; ?>" target="blank"><?php echo $product['name']; ?></a>
					<?php } ?>
				</div>
				<?php if ($product['thumb']) { ?>
				<div class="image"><a href="<?php echo $product['href']; ?>" target="blank"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
				<?php } ?>
				<?php if ($product['price']) { ?>
				<div class="price">
				  <?php if (!$product['special']) { ?>
				  <?php echo $product['price']; ?>
				  <?php } else { ?>
				  <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
				  <?php } ?>
				</div>
				<?php } ?>
				<div class="cart"><a href="<?php echo $product['href']; ?>" class="button" target="blank"><span><?php echo $button_buy_now; ?></span></a></div>
			  </div>
			  <?php } ?>
			</div>
		</div>	
		<?php } ?>
	</div>
  </div>	
</div>

<?php if (count($arr_chunk) > 1) { ?>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.pix_diapo').diapo({
		fx: 'random',
		slideOn: 'random',
		loader: 'bar',
		loaderColor: '#297eb9',
		time : <?php echo $slider_time; ?>,
		pagination: false,
		commands: false
	});
});
--></script>
<?php } ?>

<?php } else { // else allow display ?>
<div class="attention"><?php echo $error_affiliate_code; ?></div>
<?php } ?>	



</body>
</html>
