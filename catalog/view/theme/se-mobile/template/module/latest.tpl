<div class="page-block margin-bottom">

        <h2 class="block-title">
          <span><?php echo $heading_title; ?></span><!-- <span> tag to make blue border on this text only -->
          <a href="#" class="list-all">
            <i class="fa fa-th-list"></i>
          </a>
        </h2>

        <!-- Product Small List -->
                <ol class="product-small-list animated fadeInLeft">
                  <?php foreach ($products as $product) { ?>
                    <li>
                        <!-- Item # -->
                        <?php if ($product['thumb']) { ?>
                        <div class="thumb">
                        <?php if ($product['quantity'] <= 0) { ?>
                            <a href="<?php echo $product['href']; ?>">
                                <img src="<?php echo $product['soldout']; ?>" alt="<?php echo $product['name']; ?>" class="soldout"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
                            </a>
                        <?php }else{ ?>
                          <a href="<?php echo $product['href']; ?>">
                                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
                            </a>
                        <?php } ?>
                        </div>
                        <?php } ?>
                        <div class="product-ctn product-ctn1">
                            <div class="product-name">
                            <a href="<?php echo $product['href']; ?>">
                <?php echo $product['name']; ?>
              </a>
                            </div>
                            <?php if ($product['zero_price'] > 0) { ?>
                            <div class="price">
                              <?php if ($product['special']) { ?>
                                <span class="price-before">
                                <?php 
                                  echo $text_price; echo $product['price']; 
                    ?>
                    </span>
                                <span class="price-current"><?php echo $text_price; ?> <?php echo $product['special']; ?></span>
                                <?php }else{ ?>
                                <span class="price-current"><?php echo $text_price;?> <?php echo $product['price']; ?></span>
                                <?php } ?>
                                <?php if ($product['tax']) { ?>
                                <span class="price-current"><?php echo $text_tax;?> <?php echo $product['tax']; ?></span>
                                <?php } ?>
                            </div>
                            <?php }else{ ?>
                            <div class="price">
                             <img src="./image/call_for_price.png" align="absmiddle" /> <?php echo $text_zero_price; ?>
                            </div>
                            <?php } ?>

  <?php if (($product['quantity'] <= 0)&&($product['stock_status'] == "สินค้าหยุดการผลิต")) { ?>
      <div class="cart"><a class="zero_button"><span><?php echo $product['stock_status']; ?></span></a></div>   
    <?php } else {?>
    <div class="cart"><a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button"><span><?php echo $button_cart; ?></span></a></div>
    <?php } ?>



      <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist; ?></a></div>
    </div>
  
                    </li>
                    <!-- End Item # -->
                    <?php } ?>
                </ol>
</div>



