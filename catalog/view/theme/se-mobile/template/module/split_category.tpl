<?php if (!empty($categories)) { ?>
<div class="box<?php echo ($css_class) ? (' ' . $css_class) : ''; ?>">
  <div class="box-heading"><?php echo html_entity_decode($heading_title); ?></div>
  <div class="box-content">
<?php if ($style == 'select') { // Select style ?>
    <div class="box-category">
	  <select onchange="location=this.value;" style="width:100%;">
	  <option value=""><?php echo $text_select; ?></option>
	  <?php foreach ($categories as $category) { ?>
	  <option value="<?php echo $category['href']; ?>" <?php if ($category['category_id'] == $category_id) { echo 'selected="selected"'; }?>><?php echo $category['name']; ?></option>
	  <?php if ($category['children']) { ?>
	  <?php foreach ($category['children'] as $child) { ?>
	  <option value="<?php echo $child['href']; ?>" <?php if ($child['category_id'] == $child_id) { echo 'selected="selected"'; }?>>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $child['name']; ?></option>
	  <?php } ?>
	  <?php } ?>
  	  <?php } ?>
	  </select>
	</div>
<?php } elseif ($style == 'graphic') { // Graphical style ?>
    <div class="box-product">
      <?php foreach ($categories as $category) { ?>
      <div>
      <?php if ($category['thumb']) { ?>
      <div class="image"><a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" /></a></div>
	  <?php } ?>
	  <div class="name"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></div>
	  </div>
	  <?php } ?>
	</div>
<?php } else { // List style ?>

<?php if (version_compare(VERSION, '1.5.5', '<')) { //v1.5.4.1 or earlier ?>
	<div class="box-category">
	  <ul>
<?php } else { // v156 or later?>
      <ul class="box-category">
<?php } ?>
        <?php foreach ($categories as $category) { ?>
        <li>
          <?php $exp = $always_expand; ?>
          <?php if ($category['category_id'] == $category_id) { ?>
          <?php $exp = true; ?>
          <a href="<?php echo $category['href']; ?>" class="active"><?php echo $category['name']; ?></a>
          <?php } else { ?>
		  <?php $exp = $always_expand; ?>
          <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
          <?php } ?>
          <?php if ($category['children']) { ?>
          <ul <?php echo ($exp) ? 'style="display:block"' : 'style="display:none"' ?>>
            <?php foreach ($category['children'] as $child) { ?>
            <li>
              <?php if ($child['category_id'] == $child_id) { ?>
              <a href="<?php echo $child['href']; ?>" class="active"> - <?php echo $child['name']; ?></a>
              <?php } else { ?>
              <a href="<?php echo $child['href']; ?>"> - <?php echo $child['name']; ?></a>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
          <?php } ?>
        </li>
        <?php } ?>
      </ul>
    <?php if (version_compare(VERSION, '1.5.5', '<')) { //v1.5.4.1 or earlier ?>
	</div>
	<?php } ?>
<?php } ?>
  </div>
</div>
<?php } ?>