<div class="box ordertrackingsubmit">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">

	<div id="ordertrack"></div>
    <h2 id="ordertrack-title"></h2>
    <?php echo $entry_order_id; ?>
      <input type="text" name="order_id" value="" />
	<?php echo $entry_email; ?>
	<input type="text" name="email" value="" /></br></br>
	    <br />
    <h2>กรุณาคลิ๊กที่กล่องสี่เหลี่ยมด้านล่างเพื่อยืนยันว่าท่านไม่ใช่โปรแกรมอัตโนมัติ (Bot)</h2>
     <br />
    <!--<?php echo $entry_captcha; ?> 
    <input type="text" name="ord_captcha" value="" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src="index.php?route=module/ordertrack/captcha" alt="" id="captcha" /><br />
	-->
	<!--new code-->
		       <script src="https://www.google.com/recaptcha/api.js"></script>
							<div class="g-recaptcha" data-sitekey="6Lfa-CgTAAAAAH-r1jvn1hGqw_WUG6mqlOCgywK-"></div>
	<!--new code-->
    <div class="buttons">
      <div class="center"><a id="button-ordertrack" class="button"><span><?php echo $button_ordertrack; ?></span></a></div>
    </div>

  </div>
</div>
<script type="text/javascript"><!--
$('#button-ordertrack').bind('click', function() {
	$.ajax({
		type: 'POST',
		url: 'index.php?route=module/ordertrack/trackorder',
		dataType: 'json',
		data: $('.ordertrackingsubmit input[type=\'text\'], .ordertrackingsubmit input[type=\'hidden\'],  .ordertrackingsubmit input[type=\'radio\']:checked,  .ordertrackingsubmit input[type=\'checkbox\']:checked,  .ordertrackingsubmit select,  .ordertrackingsubmit textarea'),
		//data: 'order_id=' + encodeURIComponent($('input[name=\'ord_order_id\']').val()) + '&email=' + encodeURIComponent($('input[name=\'ord_email\']').val()) + '&captcha=' + encodeURIComponent($('#recaptcha-token"').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-ordertrack').attr('disabled', true);
			$('#ordertrack-title').after('<div class="attention"><img src="catalog/view/theme/cl_default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-ordertrack').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data.error) {
				$('#ordertrack-title').after('<div class="warning">' + data.error + '</div>');
			}
			
			if (data.success) {
				$('#ordertrack-title').after('<div class="success">' + data.success + '</div>');
								
				$('input[name=\'ord_order_id\']').val('');
				$('input[name=\'ord_email\']').val('');
				$('input[name=\'ord_captcha\']').val('');
				
				if (data.redirect) {
					location = data.redirect;
				}
			}
		}
	});
});
//--></script> 