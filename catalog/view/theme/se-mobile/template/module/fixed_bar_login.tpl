<div id="content">
	<div class="content" id="flogin">
	  <b><?php echo $entry_email; ?></b><br />
	  <input type="text" name="email" value="" size="40" />
	  <br />
	  <br />
	  <b><?php echo $entry_password; ?></b><br />
	  <input type="password" name="password" value="" size="40" />
	  <br />
	  <br />
	  <div class="buttons">
		<div class="right">
			<a onclick="submitLogin();" class="button"><span><?php echo $button_login; ?></span></a>
		</div>	
	  </div>
	</div>
</div>

<script type="text/javascript">
function submitLogin(){
	$.ajax({
		type: 'POST',
		url: 'index.php?route=module/fixed_bar/login',
		data: $('#flogin input[type=\'text\'], #flogin input[type=\'password\']'),
		dataType: 'json',
		success: function(json){
			$('.warning').remove();
			$('.success').remove();
			
			if (json['redirect']){
				location = json['redirerct'];
			}
			
			if (json['error']){
				$('#popupFixedBarContent .content').prepend('<div class="warning" style="display: none;">' + json['error'] + '</div>');
				$('.warning').fadeIn('slow');
			}
		}
	})
}
</script>