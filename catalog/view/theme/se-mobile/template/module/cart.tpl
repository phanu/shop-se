<?php if ($products || $vouchers) { ?>
<ol class="cart-item">
		  <?php foreach ($products as $product) { ?>
		  <li>
      		<?php if ($product['thumb']) { ?>
      		<div class="thumb">
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" >
            </div>
        	<?php } ?>		  
		    <div class="cart-delete">
                <a onclick="removeCart('<?php echo $product['key']; ?>');">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <div class="cart-detail">
                <h3 class="product-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>
                <?php foreach ($product['option'] as $option) { ?>
		        - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small><br />
		        <?php } ?>
                   
                <div class="qty">
                    <span>จำนวน</span>
                    <input type="number" value="<?php echo $product['quantity']; ?>" readonly>
                </div>
                 <div class="price">
                        <span>ราคา</span> <?php echo $product['total']; ?>
                  </div>
            </div>
		    
		    </li>
		  <?php } ?>
		  <?php foreach ($vouchers as $voucher) { ?>
		  	<div class="cart-delete">
                <a onclick="removeCart('<?php echo $product['key']; ?>');">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <div class="cart-detail">
                <h3 class="product-name"><a href="<?php echo $product['href']; ?>" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" onclick="removeVoucher('<?php echo $voucher['key']; ?>');"><?php echo $voucher['description']; ?></a></h3>
                <?php foreach ($product['option'] as $option) { ?>
		        - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small><br />
		        <?php } ?>
                   
                <div class="qty">
                    <span>จำนวน</span>
                    <input type="number" value="1" readonly>
                </div>
                 <div class="price">
                        <span>ราคา</span> <?php echo $voucher['amount']; ?>
                  </div>
            </div>

		  <?php } ?>  
</ol>
		<div class="cart-action">
			<?php foreach ($totals as $total) { ?>
                        <?php if ($total['title']=="รวมทั้งหมด :"){ ?>
                    	<div class="total">
	                    <?php }else{ ?>
	                    <div class="subtotal">
	                    <?php } ?>
                    			<?php echo $total['title']; ?></span>
                                <span class="price"><?php echo $total['text']; ?></span>
                            </div>
            <?php } ?>
            <a href="index.php?route=checkout/cart" class="btn light-blue.darken-1 btn-block" style="margin-bottom: 10px;">ดูตะกร้าแบบละเอียด</a>
            <a href="<?php echo $checkout; ?>" class="btn green btn-block"><?php echo $button_checkout; ?></a>
        </div>
	<?php } else { ?>
		<div class="empty"><?php echo $text_empty; ?></div>
	<?php } ?>

