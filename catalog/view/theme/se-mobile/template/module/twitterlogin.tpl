<?php if($data['TwitterLogin']['Enabled'] != 'no'): ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $currenttemplate?>/stylesheet/twitterlogin.css" />

<!-- START TwitterLogin -->
<div id="twitterButtonWrapper" style="display:none;">
	<div class="twitterButton">
    <?php if($data['TwitterLogin']['WrapIntoWidget'] == 'Yes') { ?>
    <div class="box box-twitterConnect">
      <div class="box-heading"><?php echo $data['TwitterLogin']['WrapperTitle']; ?></div>
      <div class="box-content">
        <a href="javascript:void(0)" class="twitterLoginAnchor <?php echo $data['TwitterLogin']['ButtonDesign']; ?>"><span></span><?php echo $data['TwitterLogin']['ButtonLabel']; ?></a>
      </div>
    </div>
    <?php } else { ?>
    	<a href="javascript:void(0)" class="twitterLoginAnchor <?php echo $data['TwitterLogin']['ButtonDesign']; ?>"><span></span><?php echo $data['TwitterLogin']['ButtonLabel']; ?></a>
    <?php } ?>
    </div>
</div>
<style type="text/css">
<?php echo htmlspecialchars_decode($data['TwitterLogin']['CustomCSS']); ?>
</style>
<script language="javascript" type="text/javascript">
<!--

var posSelector = '<?php echo $data['TwitterLogin']['ButtonPosition']; ?>';
var twitterURL = '<?php echo $data['TwitterLogin']['TwitterConnectURL']; ?>';
var positionTwitterButton = function() {
	if (posSelector) {
		$(posSelector).prepend($('#twitterButtonWrapper').html());	
	} else {
		$('#content').prepend($('#twitterButtonWrapper').html());	
	}	
}

$(document).ready(function() {
	positionTwitterButton();
});

$(document).ajaxComplete(function() {
	var tries = 0;
	var tryAppendButton = setInterval(function() {
		tries++;
		if ($(posSelector).find('.twitterButton').length == 0) {
			positionTwitterButton();
		}
		console.log('Try number '+tries);
		if ((tries > 20) || ($(posSelector).find('.twitterButton').length > 0)) {
			clearInterval(tryAppendButton);	
		}
	},300);
});

$('.twitterLoginAnchor').live('click',function() {
	newwindow=window.open(twitterURL,'name','height=740,width=630');
	if (window.focus) {newwindow.focus()}
	return false;
});

// -->
</script>

<!-- END TwitterLogin -->



<?php endif; ?>

