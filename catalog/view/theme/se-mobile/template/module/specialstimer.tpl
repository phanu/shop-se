<!-- 
This module, Specials Timer, is by MarketInSG (http://www.marketinsg.com)
//-->
<div style="border:none;" class="box">
  <div style="display:none;" class="box-heading">
	<?php echo $heading_title; ?>
  </div>
  <div class="box-content" style="border:none;">
    <div class="box-product specialstimerbox">
	  <?php $count = 0; ?>
      <?php foreach ($products as $product) { ?>
	    <div style="position:relative; width: auto;">
		  <?php if ($product['image']) { ?>
		    <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
		  <?php } ?>
		  <div class="show">
		    <?php echo $product['description']; ?>
			<div class="cart"><input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" /></div>
		    <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['rating']; ?>" /></div>
		  </div>
		  <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
		  <?php if ($product['price']) { ?>
		    <div class="price">
			  <?php if (!$product['special']) { ?>
			    <?php echo $product['price']; ?>
			  <?php } else { ?>
			    <span class="price-old"><?php echo $product['price']; ?></span><br /><span class="price-new"><?php echo $product['special']; ?></span>
			  <?php } ?>
            </div>
		  <?php } ?>
		  <?php echo $text_expires; ?>
	      <div class="specialstimer" id="scounter_<?php echo $count; ?>" style="width:auto; display:block;"></div>
  	    </div>
		<?php $count++; ?>
      <?php } ?>
	</div>
  </div>
</div> 
<script type="text/javascript">
$(document).ready(function() {
	$('.show').hover(function() {
		$(this).stop().animate({opacity: '1'}, 'slow');
	}, function() {
		$(this).stop().animate({opacity: '0'}, 'slow');
	});
	<?php $count = 0; ?>
	
	<?php foreach ($products as $product) { ?>
	$('#scounter_<?php echo $count; ?>').countdown({
		image: '<?php echo $digit_image; ?>',
		digitWidth: <?php echo $digit_width; ?>,
		digitHeight: <?php echo $digit_height; ?>,
		format: 'dd:hh:mm:ss',
		startTime: '<?php echo ($product['days'] < 10) ? "0" . $product['days'] : $product['days']; ?>:<?php echo ($product['hours'] < 10) ? "0" . $product['hours'] : $product['hours']; ?>:<?php echo ($product['minutes'] < 10) ? "0" . $product['minutes'] : $product['minutes']; ?>:<?php echo ($product['seconds'] < 10) ? "0" . $product['seconds'] : $product['seconds']; ?>'
	});
  
	<?php $count++; ?>
	<?php } ?>
});
</script>
<style type="text/css">
.specialstimerbox {
	text-align: center;
}
.specialstimerbox > div {
	text-align: left;
}
.specialstimerbox > div .show {
	background: url('catalog/view/theme/default/image/menu.png') repeat;
	padding: 10px;
	opacity: 0;
	color: #ffffff;
	position: absolute;
	top: 0px;
	width: <?php echo ($this->config->get('specialstimer_imagewidth')) - 10; ?>px;
	height: <?php echo ($this->config->get('specialstimer_imageheight')) - 10; ?>px;
}
.specialstimerbox > div .price {
	float: right;
	text-align: right;
}
.specialstimerbox > div .rating {
	margin-top: 20px;
	float: right;
}
.specialstimerbox > div .cart {
	margin-top: 15px;
	float: left;
}
.specialstimerbox > div .cart input{
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
}
.specialstimer .specialstimerdot {
	font-size: <?php echo $font_size;?>px;
	margin: 5px auto;
	height: 30px;
	color: #000;
}
.specialstimer .desc {
	margin: 7px 3px;
}
.specialstimer .desc div {
	float: left;
	font-family: Arial;
	width: 70px;
	margin-right: 65px;
	font-size: 13px;
	font-weight: bold;
	color: #000;
}
</style>