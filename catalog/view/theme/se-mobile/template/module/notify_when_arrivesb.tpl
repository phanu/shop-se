<?php if ($out_stock_list) { ?>
    <div class="box" id="nwa_list" 
         style="
         width: 200px;
         text-align: center;
         display:none;
         z-index: 10000;
         position: absolute;
         background-color: white;
         ">
        <div class="box-content"  style="border-top:1px solid #dbdee1;" >
	    <?php echo $text_description; ?>
    	<input style="width:95%;" type="text" id="nwa_list_email" name="notify_email" value="<?php echo $notify_email; ?>"/><br/><br/>
    	<input type="hidden"  name="notify_product_id" id="nwa_list_product_id" value="<?php echo $notify_product_id; ?>"/>
    	<a class="button" id="nwa_list_register" ><span><?php echo $button_register; ?></span></a>
    	<a class="button" id="nwa_list_button-close" ><span><?php echo $button_close; ?></span></a>
    	<div  id="nwa_list_msg"></div>
        </div>
    </div>
    <script type="text/javascript"><!--
        function nwa_list(product_id,y,x){
         
	    $('#nwa_list_product_id').val(product_id);

	    var w_width = $(window).width() - 15;

	    if ((x + $('#nwa_list').width()) > w_width){ x = (w_width - $('#nwa_list').width()); }
 
	    $('#nwa_list_msg').hide();

	    $('#nwa_list').css('top',y+'px');
	    $('#nwa_list').css('left',x+'px');

	    $('#nwa_list').show('slow');

	    $('#nwa_list_email').focus();

        }
            
        var out_stock_list = [<?php echo $out_stock_list; ?>];
    	
        var nwa_view =   $.cookie('display');
          
        $(function() { 
         
    	$('.button,.button2').each(function() {

    	    var  click_action = $(this).attr('onclick');

    	    if (click_action != undefined &&  click_action.toString().indexOf('addToCart') != -1){

		var id = null;
		
		var myregexp = /addToCart\('([0-9]*?)'\)/m;
		var match = myregexp.exec(click_action.toString());
 
		if (match != null) {
			id =  parseInt(match[1]);
		} 

    		if ($.inArray(id, out_stock_list) != -1){

    		    $(this).before('<span style="color:red;font-weight:bold"><?php echo $heading_title; ?></span><br/>');

    		    if ($(this).is('input')){

    			$(this).val('<?php echo $button_category; ?>');

    		    }else{

    			$(this).html('<span><?php echo $button_category; ?></span>');
    		    }


    		    $(this).attr('onclick','');
		    
		        
		   <?php if($notify_button){ ?>
			
			  if ($(this).is('input')){
			      
			       $(this).css('background','url(\'image/nwa_button.png\') repeat-x scroll left top transparent');
			       
			  }else{
			      
				$(this).css('background','url(\'image/nwa_button-left.png\') top left no-repeat');

				$(this).find('span').css('background','url(\'image/nwa_button-right.png\') top right no-repeat');
			  }
  
		  <?php  } ?>
 
		    <?php if ($nwa_list_show_mode == '1') { ?>

			$(this).mouseenter(function(e){

			    nwa_list(id,e.pageY,e.pageX);

			    e.stopPropagation();

			});
			
			
		    <?php } ?>
			
		    <?php if ($nwa_list_show_mode == '1' || $nwa_list_show_mode == '2') { ?>

			$(this).click(function(e){

			    nwa_list(id,e.pageY,e.pageX);

			    e.stopPropagation();
			});
		    
		    <?php } ?>
			
		  <?php if ($nwa_list_show_mode == '3'){ ?>

			 $(this).click(function(e){
 
			  location = 'index.php?route=product/product&product_id='+id;;

			});
		    
		    <?php } ?>
      
    		}

    	    }

    	});

        });

        $('#nwa_list_button-close').click(function(){
          
	    $('#nwa_list').hide('slow');

        });
            
        $(document.body).click(function() {
	    
	    $('#nwa_list').hide('slow');
	    
        });

     
        $('.display').live('hover',function(){
    	    
	    setTimeout(function(){
    	       
    	    if (nwa_view !=  $.cookie('display')){
    	
    		location.reload();

    	    }
	    
	    },200);

        });

        $('#nwa_list').click(function(e) {
	    e.stopPropagation();
        });

        $('#nwa_list_register').click(function() {   
    	$.ajax({
    	    type: 'POST',
    	    url: 'index.php?route=module/notify_when_arrives/register',
    	    data: 'notify_email='+$('#nwa_list_email').val()+'&notify_product_id='+$('#nwa_list_product_id').val(),
    	    dataType: 'json',		
    	    beforeSend: function() {

    		$('#nwa_list_msg').hide();
    		$('#nwa_list_register').attr('disabled', true); 
    		$('#nwa_list_register').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');

    	    },
    	    complete: function() {

    		$('#nwa_list_register').attr('disabled', false);
    		$('.wait').remove();

    	    },	
    	    success: function(json) {

    		if (json['sucess']) {

    		    $('#nwa_list_msg').html('<div style="color:green">' + json['msg'] +'</div>');

    		}else{

    		    $('#nwa_list_msg').html('<div style="color:red">' + json['msg'] +'</div>');
    		}

    		$('#nwa_list_msg').show(); 

    	    }

    	});
        });
        //--></script>
<?php } ?>
 
    <div class="box-content" style="font-size:11px;display:none;" id="nwa">
        <b style="color:red;" id="nwa_title"><?php echo $heading_title; ?></b>
        <br/>
	<?php echo $text_description; ?>
        <br />
        <input type="text" style="width:60%;margin-right:5px;" name="nwa_email" id="nwa_email" value="<?php echo $notify_email; ?>"/>
        <input type="hidden"  name="nwa_product_id" id="nwa_product_id" value="<?php echo $notify_product_id; ?>">
        <input type="hidden"   name="nwa_option_id" id="nwa_option_id" value="">
        <input type="hidden"  name="nwa_option_value_id" id="nwa_option_value_id" value="">
        <a class="button" id="nwa_register"><span><?php echo $button_register; ?></span></a>
        <div  id="nwa_msg"></div>
    </div>

    <script type="text/javascript"><!--
<?php if ($out_stock_product) {?>	
	$(function() { 
	    $('#content .cart').html($('#nwa'));
	    $('#nwa').show();
	    $('head').append('<style type="text/css">#shipping{display:none;}</style>');
	});
<?php } ?>	
        $('#nwa_register').live('click', function() {   
	    $.ajax({
		type: 'POST',
		url: 'index.php?route=module/notify_when_arrives/register',
		data: 'notify_email='+$('#nwa_email').val()+'&notify_product_id='+$('#nwa_product_id').val()+'&notify_option_id='+$('#nwa_option_id').val()+'&notify_option_value_id='+$('#nwa_option_value_id').val(),
		dataType: 'json',		
		beforeSend: function() {

		    $('#nwa_msg').hide();
		    $('#nwa_register').attr('disabled', true); 
		    $('#nwa_register').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');

		},
		complete: function() {

		    $('#nwa_register').attr('disabled', false);
		    $('.wait').remove();

		},	
		success: function(json) {

		    if (json['sucess']) {

			$('#nwa_msg').html('<div style="color:green">' + json['msg'] +'</div>');

		    }else{

			$('#nwa_msg').html('<div style="color:red">' + json['msg'] +'</div>');
		    }

		    $('#nwa_msg').show(); 

		}

	    });
        });
	
	
	
	 function nwa(a,b,c,d,e){

	    if (nwa_options[b] != undefined && nwa_options[b][c] != undefined && <?php echo  ($out_stock_option) ? 'true' : 'false'; ?>) {

		$('#nwa').hide();

		$('#nwa_option_id').val(b);
		$('#nwa_option_value_id').val(nwa_options[b][c]);

		if(nwa_title == ''){

		    nwa_title = $('#nwa_title').text();

		}

		$('#nwa_title').text(d+':'+e+' '+ nwa_title);

		if ($(a).is('input')){

		    $(a).attr('checked',false);

		}else{

		    $(a).val("");

		}

		$('html, body').animate({
			    scrollTop: $("#nwa").offset().top - 200
		}, 500,function(){

		    $('#nwa').prependTo('.options,#product_options').show('slow');

		});

	    }
	}
        //--></script>
