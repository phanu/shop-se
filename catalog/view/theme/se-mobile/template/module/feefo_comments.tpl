<?php if ($error) { ?>
	<div class="feefo_center"><?php echo $error; ?></div>
<?php } else { ?>
	<?php foreach ($comments as $comment) : ?>
    <div class="feefo-comments-list">
        <div class="date"><b><?php echo $comment['date']; ?></b></div>
        <div class="rating"><img src="catalog/view/theme/<?php echo $this->config->get('config_template'); ?>/image/stars-<?php echo $comment['rating'] . '.png'; ?>" alt="<?php echo $comment['rating']; ?>" /></div>
        <div class="text"><?php echo $comment['comment']; ?></div>
    </div>
    <?php endforeach; ?>
<?php } ?>