<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content" id="calendar">
    <div id="month"></div>
    <div id="clock"><input name="clock" id="clock_time" /></div>
  </div>
</div>
<script type="text/javascript"><!--
$(document).ready(function() {
	getCalendar();
	getTime();
});
//--></script>
<script type="text/javascript"><!--
function getCalendar() {
	var today = getClockDate(<?php echo $timezone_offset; ?>);
	$('#month').load('index.php?route=module/calendar/show_month&date=' + today);
}
function getTime() {
	document.getElementById('clock_time').value = getClockTime(<?php echo $timezone_offset; ?>);
	setTimeout('getTime()', 1000);
}
//--></script>
