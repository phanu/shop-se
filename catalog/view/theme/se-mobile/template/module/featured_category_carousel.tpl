<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
  	<div id="ocs_featured_category_carousel_content<?php echo $module_count; ?>">
  		<ul id="ocs_featured_category_carousel<?php echo $module_count; ?>" class="jcarousel-skin-opencartsoft">
      <?php foreach ($categories as $category) { ?>
      <li>
      <div>
        <?php if ($category['thumb']) { ?>
        <div class="image"><a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" /></a></div>
        <?php } ?>
        <div class="name"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></div>        
      </div>
      <div class="price">&nbsp;</div>
      </li>
      <?php } ?>
      </ul>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
<?php if($scroll_auto) { ?>
function featured_category_carousel_initCallback<?php echo $module_count; ?> (carousel)
{
    // Disable autoscrolling if the user clicks the prev or next button.
    carousel.buttonNext.bind('click', function() {
        carousel.startAuto(0);
    });

    carousel.buttonPrev.bind('click', function() {
        carousel.startAuto(0);
    });

    // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function() {
        carousel.stopAuto();
    }, function() {
        carousel.startAuto();
    });
};
<?php } ?>

<?php if($effect == 'easing') { ?>
jQuery.easing['BounceEaseOut'] = function(p, t, b, c, d) {
	if ((t/=d) < (1/2.75)) {
		return c*(7.5625*t*t) + b;
	} else if (t < (2/2.75)) {
		return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
	} else if (t < (2.5/2.75)) {
		return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
	} else {
		return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
	}
};
<?php } ?>

jQuery(document).ready(function() {
  jQuery('#ocs_featured_category_carousel<?php echo $module_count; ?>').jcarousel({    		
    vertical: <?php echo ($axis == 'horizontal' ? 'false' : 'true'); ?>,
    visible: <?php echo $scroll_limit; ?>,
    scroll: <?php echo $scroll; ?>,
     <?php if($effect == 'easing') { ?>
    	easing: 'BounceEaseOut',
    	animation: 1000,
    <?php } ?>
    <?php if($scroll_auto) { ?>
    auto: 3,
    wrap: 'last',
    initCallback: featured_category_carousel_initCallback<?php echo $module_count; ?>
    <?php } ?>
  });
});
//--></script>