<?php if ($deals) { ?>  
<div class="ocx-deal">
<div class="ribbon"></div>	
<div class="ocx-deal-content">
<?php foreach($deals as $deal) { ?>  
    <div id="ocx_deal_<?php echo $deal['deal_id']; ?>" class="fb-deal">
		<div class="contest-info">
			<div class="image"><a href="<?php echo $deal['product_href']; ?>"><img src="<?php echo $deal['image']; ?>" width="<?php echo $image_height;?>px;" height="<?php echo $image_height;?>px;" /></a></div>
			<div class="deal-info">
				<div class="intro"><?php echo $text_participate_win; ?></div>
				<div class="details">
					<div class="dbox">
						<span class="gray"><?php echo $text_join_and; ?></span>
						<span class="white"><?php echo $text_win; ?></span>
						<span class="gray"><?php echo $deal['text_prize']; ?></span>
					</div>
					<div class="dbox">
						<span class="gray"><?php echo $text_if_you; ?></span>
						<span class="white"><?php echo $text_like_share; ?></span>
						<span class="gray"><?php echo $text_the_product; ?></span>
					</div>
					<div class="dbox">
						<span class="gray"><?php echo $text_are_needed; ?></span>
						<span class="white remaining-people-needed" id="rnp-<?php echo $deal['deal_id']; ?>"><?php echo $deal['still_need']; ?></span>
						<span class="gray"><?php echo $text_people; ?></span>
					</div>
				</div>
				<div class="dbuttons">
					<a class="participate-button" id="participate-button-<?php echo $deal['deal_id']; ?>"><span><?php echo $button_participate; ?></span></a>
				</div>
				<div class="dcounter-area">
					<div class="dcounter">
						<div id="dcounter_<?php echo $deal['deal_id']; ?>"></div>
					</div>	
				</div>
			</div>
		</div>	
		<div class="contest-users" style="min-height:<?php echo $image_height;?>px;">
			<div class="np-text"><?php echo $text_new_participants; ?><a class="participants-view-all" id="view-all-<?php echo $deal['deal_id']; ?>">[<?php echo $text_view_all; ?>]</a></div>
			<div class="deal-separator"></div>
			<div class="photo-list" id="photo-list-<?php echo $deal['deal_id']; ?>">
			<?php foreach($deal['participants'] as $participant) { ?>
				<div class="avatar">
					<?php if ($show_participant_points) { ?>
					<span class="points"><?php echo $participant['points']; ?></span>
					<?php } ?>
					<?php if ($show_participant_photo) { ?>
					<img src="http://graph.facebook.com/<?php echo $participant['fb_id']; ?>/picture" />
					<?php } else { ?>
					<img class="no-photo" />
					<?php } ?>
				</div>
			<?php } ?>	
			
			<?php for ($counter = 0; $counter < $deal['still_need']; $counter++ ) { ?>
				<div class="avatar"></div>
			<?php } ?>	
			</div>
			<div class="deal-separator"></div>
			<div class="deal-help-info">
			<?php if ($deal['custom_description']) { ?>
				<div class="deal-help-info-name"><?php echo $deal['deal_name']; ?></div>
				<div class="deal-help-info-desc"><?php echo $deal['deal_description']; ?></div>
            <?php } else { ?>			
				<?php echo $text_how_to_win; ?>
				<div class="deal-separator"></div>
				<?php echo $text_points_rules; ?>
			<?php } ?>	
			</div>
		</div>
    </div>
<?php } ?>  
</div>
<div id="dialog-deal-like-share"></div>
</div>

<div id="fb-root"></div>
<?php } ?>  

<script type="text/javascript"><!--

<?php if ($deals) { ?>
var fb_user_info = null;

window.fbAsyncInit = function() {
    FB.init({
		appId      : '<?php echo $fb_app_id; ?>', // App ID
		status     : true, // check login status
		cookie     : true, // enable cookies to allow the server to access the session
		xfbml      : true  // parse XFBML
    });
	
	
};

// Load the SDK Asynchronously
(function(d){
    var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    d.getElementsByTagName('head')[0].appendChild(js);
}(document));

function checkFBUserStatus(response) {
    if (response.status === 'connected') {
		getFBUserInfo();
    } else {
  
        FB.login(function(response) {
            if (response.status === 'connected') {
				getFBUserInfo();
            } 
        }, { scope: 'email'});
    }
}


(function($) {
	$.dealcountdown.regional['fbdeal'] = {
		labels: [' ', ' ', ' ', '<?php echo $text_days; ?>', '<?php echo $text_hours; ?>', '<?php echo $text_minutes; ?>', '<?php echo $text_seconds; ?>'],
		labels1: [' ', ' ', ' ', '<?php echo $text_day; ?>', '<?php echo $text_hour; ?>', '<?php echo $text_minute; ?>', '<?php echo $text_second; ?>'],
		compactLabels: ['A', 'L', 'S', 'Z'],
		whichLabels: null,
		digits: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
		timeSeparator: ':', isRTL: false};
	$.dealcountdown.setDefaults($.dealcountdown.regional['fbdeal']);	
})(jQuery);

	<?php foreach($deals as $deal) { ?>
		$('#dcounter_<?php echo $deal['deal_id']; ?>').dealcountdown({
			until: new Date(<?php echo $deal['stop_year']; ?>, <?php echo $deal['stop_month']; ?>-1, <?php echo $deal['stop_day'];?>),
			<?php if ($use_server_time) { ?>
			serverSync: FBDealTimeServerSync(),
			<?php } ?>
			format: 'DHMS'
		});
	<?php } ?>
	
	var column_left_right = false;
	var isFacebookStore = false;
	<?php if ($isFacebookStore) { ?>
		isFacebookStore = true;
	<?php } ?>
	
 
	
	if ( ($('#column-left').length > 0 && $('#column-right').length > 0) || ( $('#column-left').length > 0 && isFacebookStore ) ){
		column_left_right = true;
	}
	
	
	var deal_help_height = $('.contest-info').height() - ($('.np-text').height() + $('.photo-list').height() + 2* $('.deal-separator').height() + 25);
	var deal_help_width  = $('.np-text').width() - 10;
	
	$('.deal-help-info').attr('style', 'width:'+ deal_help_width +'px; height:'+deal_help_height +'px;');
	
	if (column_left_right ){
		$('.contest-info').addClass('one-column');
		$('.contest-info .image').addClass('one-column');
		
		<?php if ($hide_help){ ?>
		$('.deal-help-info').hide();
		$('.contest-users').removeAttr('style');
		<?php } ?>
	}

	
	$(document).ready(function(){
		if ($('.deal-help-info').is(":visible")) {
			$('.deal-help-info').jScrollPane();
		}
	});

	var ocx_deal_content_height = $('.ocx-deal-content div:first-child').height();
	$('.ocx-deal-content').css('height', ocx_deal_content_height);
	
	$('.ocx-deal-content').rhinoslider({
		controlsMousewheel: false,
		controlsKeyboard: false,
		controlsPlayPause: false,
		<?php if (count($deals) < 2) {?>
		showControls: 'never',
		<?php } else { ?>
		showControls: 'hover',
		<?php } ?>
		<?php if (count($deals) < 2) {?>
		showBullets: 'never'
		<?php } else { ?>
		showBullets: 'hover'
		<?php } ?>
	});
	

<?php } ?>

function FBDealTimeServerSync(){
	var fb_deal_time = new Date('<?php echo $current_time; ?>'); 
    
	return fb_deal_time; 
}

$('.participate-button').bind('click', function(){
	
	var deal_id = $(this).attr('id').replace('participate-button-', '');
	
	FB.getLoginStatus( function(response){
		if (response.status !== 'connected') {
			FB.login(function(response) {
				if (response.authResponse) {
					$('.participate-button').trigger('click');
				}	
			}, { scope: 'email'});
		
		} else {
			
			$.ajax({
				url: 'index.php?route=module/fb_deal_unlocker/getParticipateForm',
				data: 'deal_id=' + deal_id,
				success: function(data){
					$('#dialog-deal-like-share').html(data);
				}
			});
			
			$('#dialog-deal-like-share').dialog({
				minWidth: 520,
				modal: true,
				resizable: false
			});
			
			$(".ui-dialog-titlebar").hide();		
		}
	});
});

$('.participants-view-all').bind('click', function(){
	var photoListHtml = '<div class="photo-list" style="background: #FFF;">' + $('#photo-list-' + $(this).attr('id').replace('view-all-', '')).html() + '</div>';
	
	$(photoListHtml).dialog({
		title: '<?php echo addslashes($text_deal_participants); ?>',
		minWidth: 490,
		height: 300,
		modal: true,
		resizable: true
	});
});

function getFBUserInfo(){
	FB.api('/me?fields=id,name,email,first_name,last_name', function(response) {
		fb_user_info = response;
	});
}   
   
$(document).ajaxComplete(function(){
    try {
        FB.XFBML.parse(); 
    } catch(ex){}
});
--></script>