// Functions for current date and time
var startDST = new Date();
var endDST = new Date();
startDST.setMonth(9);
startDST.setHours(2);
startDST.setDate(31);
var dayDST = startDST.getDay();
startDST.setDate(31 - dayDST);
endDST.setMonth(2);
endDST.setHours(2);
endDST.setDate(31);
dayDST = endDST.getDay();
endDST.setDate(31 - dayDST);

function getClockTime(timezone_offset) {
	var dst = 0;
	var time = new Date();
	var gmtMS = time.getTime() + (time.getTimezoneOffset() * 60000);
	var gmtTime = new Date(gmtMS);
	var hours = gmtTime.getHours() + timezone_offset;
	var minutes = gmtTime.getMinutes();
	var seconds = gmtTime.getSeconds();
	if (hours >= 24) {
		hours = hours - 24;
	}
	if (hours < 0) {
		hours -= -24;
	}
	var currentTime = new Date();
	currentTime.setHours(hours);
	if ((currentTime >= startDST) && (currentTime < endDST)) {
		dst = 1;
	}
	if (dst == 1) {
		hours -= -1;
		if (hours >= 24) {
			hours = hours - 24;
		}
	}
	if (hours == 0 && minutes == 0 && seconds == 0) {
		$('#month').load('index.php?route=module/calendar/view&month=' + (currentTime.getMonth() + 1));
	}
	var hrs = hours;
	if (hours == 0) {
		hours = 12;
	}
	var clocktime = '' + ((hours > 12) ? (hours - 12) : hours);
	clocktime += ((minutes < 10) ? ':0' : ':') + minutes;
	clocktime += ((seconds < 10) ? ':0' : ':') + seconds;
	clocktime += (hrs >= 12) ? ' pm' : ' am';
	return clocktime;
}

function getClockDate(timezone_offset) {
	var dst = 0;
	var time = new Date();
	var gmtMS = time.getTime() + (time.getTimezoneOffset() * 60000);
	var gmtTime = new Date(gmtMS);
	var day = gmtTime.getDate();
	var month = gmtTime.getMonth();
	var year = gmtTime.getYear();
	if (year < 1000) {
		year += 1900;
	}
	var monthDays = new Array('31', '28', '31', '30', '31', '30', '31', '31', '30', '31', '30', '31');
	if ((year % 4) == 0) {
		monthDays = new Array('31', '29', '31', '30', '31', '30', '31', '31', '30', '31', '30', '31');
	}
	if ((year % 100) == 0 && (year % 400) != 0) {
		monthDays = new Array('31', '28', '31', '30', '31', '30', '31', '31', '30', '31', '30', '31');
	}
	var hours = gmtTime.getHours() + timezone_offset;
	if (hours >= 24) {
		day -= -1;
	}
	if (hours < 0) {
		day -= 1;
	}
	if (day <= 0) {
		if (month == 0) {
			month = 11;
			year -= 1;
		} else {
			month = month - 1;
		}
		day = monthDays[month];
	}
	if (day > monthDays[month]) {
		day = 1;
		if (month == 11) {
			month = 0;
			year -= -1;
		} else {
			month -= -1;
		}
	}
	var currentTime = new Date();
	currentTime.setMonth(month);
	currentTime.setYear(year);
	currentTime.setDate(day);
	currentTime.setHours(hours);
	if ((currentTime >= startDST) && (currentTime < endDST)) {
		dst = 1;
	}
	if (dst == 1) {
		hours -= -1;
		if (hours >= 24) {
			day -= -1;
		}
		if (day > monthDays[month]) {
			day = 1;
			if (month == 11) {
				month = 0;
				year -= -1;
			} else{
				month -= -1;
			}
		}
	}
	month = month + 1;
	if (month < 10) {
		month = '0' + month;
	}
	return year + '-' + month + '-' + day;
}
