/*post_message*/ 
$('#button_post_message').bind('click',function(){
	function clear_data(){ 
		$('input[name=\'name\']').val('');
		$('input[name=\'message\']').val('');			
	}
	function viewshout(){
		$('#view_message').load('index.php?route=shoutbox/view_message');
	}
		$.ajax({
		url: 'index.php?route=shoutbox/write',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&message=' + encodeURIComponent($('input[name=\'message\']').val()),
		beforeSend: function(){ 
		$('.success, .warning').remove();
			$('#button_post_message').attr('disabled',true);
			$('#shout_alert').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt=""/>Please wait...</div>');
		},
		complete: function(){ 
		$('#button_post_message').attr('disabled',false);
			$('.attention').remove();
		},
		success: function(data){
			if(data['error']){ 
		$('#shout_alert').after('<div class="warning">' + data['error']+'</div>');}

		if(data['success']){ 
		$('#shout_alert').after('<div class="success">' + data['success']+'</div>');
				viewshout();
				clear_data();
			}
		}
	});
});