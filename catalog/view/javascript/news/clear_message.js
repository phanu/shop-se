$('#button_clear_message').bind('click',function(){
		$.ajax({
		url: 'index.php?route=shoutbox/clear_message',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'shout_username\']').val()) + '&message=' + encodeURIComponent($('input[name=\'shout_message\']').val()),
		beforeSend: function(){ 
		$('.success, .warning').remove();
			$('#button_clear_message').attr('disabled',true);
			$('#shout_alert').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt=""/><?php echo $text_wait;?></div>');
		},
		complete: function(){ 
		$('#button_clear_message').attr('disabled',false);
			$('.attention').remove();
		},
		success: function(data){
			if(data['error']){ 
		$('#shout_alert').after('<div class="warning">' + data['error']+'</div>');}

		if(data['success']){ 
		$('#shout_alert').after('<div class="success">' + data['success']+'</div>');
			}
		}
	});
});