$(document).ready(function () {
	var cartDialogElem = $('<div/>').attr('id','cart_dialog');
  $('body').append(cartDialogElem);
  $('#cart_dialog').dialog({
    title: A2C_TITLE,
    bgiframe: false,
    width: 400,
    position: 'center',
    buttons: [{
        text: A2C_VIEW_CART,
        click: function() { location = 'index.php?route=checkout/cart'; }
      },{
        text: A2C_CONTINUE,
        click: function() { $(this).dialog("close"); }
      }
    ],
    height: 220,
    resizable: false,
    modal: false,
    autoOpen: false
  });
});

function addToCart(product_id) {
	$.ajax({
		url: 'index.php?route=checkout/cart/update',
		type: 'post',
		data: 'product_id=' + product_id,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information, .error').remove();

			if (json['redirect']) {
				location = json['redirect'];
			}

			if (json['error']) {
				if (json['error']['warning']) {
					$('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

					$('.warning').fadeIn('slow');

					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}
			}

			if (json['success']) {
				$('#cart_total').html(json['total']);

        $('#cart_dialog').html(json['success_dialog']);
        $('#cart_dialog').dialog("open");
			}
		}
	});
}