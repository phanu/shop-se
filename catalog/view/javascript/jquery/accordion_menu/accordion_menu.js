/*
 * Accordion Menu v2.1
 * for OpenCart 1.5.1 - 1.5.4.1
 *
 * Copyright 2013, iDiY
 * Support: idiy.webmaster@gmail.com
 *
 */

$(document).ready(function() {

	$('li.active > .am-nav-btn, li.active > .category-link').addClass('open');
	$('ul').closest('li').find('> .category-link').removeAttr('href');

	$('.am-nav-btn').removeAttr('href').closest('li').find('> .category-btn').css("padding-right", "2.35em");
	$('.am-nav-link').closest('li').find('> .category-link').css("padding-right", "2.35em");

	$('.am-nav-btn, .category-link').click(function() {
		$(this).toggleClass('open').closest('li').find('> ul').slideToggle(200, "linear");
	});

	$('.accordion .am-nav-btn, .accordion .category-link').click(function() {
		$(this).closest('ul').find('.open').not(this).toggleClass('open').closest('li').find('> ul').slideToggle(200, "linear");
	});
});