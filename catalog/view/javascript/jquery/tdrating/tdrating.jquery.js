/****
@Name :       	tdrating  
@License :		 Open Source - MIT License : http://www.opensource.org/licenses/mit-license.php
****/
(function($) {
	$.fn.tdrating = function(op) {
		var defaults = {
			bigStarsPath : 'catalog/view/javascript/jquery/tdrating/icons/stars.png',
			smallStarsPath : 'catalog/view/javascript/jquery/tdrating/icons/stars.png',
			phpPath : 'index.php?route=product/tdrating/write&product_id=',
			type : 'big',
			step:false,
			isDisabled:false,
			showRateInfo: true,			
			length:5,
			decimalLength : 0,
			rateMax : 20,
			rateInfosX : -45,
			rateInfosY : 5,
			onSuccess : null,
			onError : null
		}; 
		
		if(this.length>0)
		return this.each(function() {
			var opts = $.extend(defaults, op),    
			newWidth = 0,
			starWidth = 0,
			starHeight = 0,
			bgPath = '';
			
			if($(this).hasClass('jDisabled') || opts.isDisabled)
				var jDisabled = true;
			else
				var jDisabled = false;

			getStarWidth();
			$(this).height(starHeight);

			var average = parseFloat($(this).attr('id').split('_')[1]),
			idBox=2;
			widthRatingContainer = starWidth*opts.length,
			widthColor = average/opts.rateMax*widthRatingContainer,
			
			quotient = 
			$('<div>', 
			{
				'class' : 'tdratingColor',
				css:{
					width:widthColor
				}
			}).appendTo($(this)),
			
			average = 
			$('<div>', 
			{
				'class' : 'tdratingAverage',
				css:{
					width:0,
					top:- starHeight
				}
			}).appendTo($(this)),

			 jstar =
			$('<div>', 
			{
				'class' : 'jStar',
				css:{
					width:widthRatingContainer,
					height:starHeight,
					top:- (starHeight*2),
					background: 'url('+bgPath+') repeat-x'
				}
			}).appendTo($(this));

			$(this).css({width: widthRatingContainer,overflow:'hidden',zIndex:1,position:'relative'});

			if(!jDisabled)
			$(this).bind({
				mouseenter : function(e){
					var realOffsetLeft = findRealLeft(this);
					var relativeX = e.pageX - realOffsetLeft;
					if (opts.showRateInfo)
					var tooltip = 
					$('<p>',{
						'class' : 'tdratingInfos',
						html : getNote(relativeX)+' <span class="maxRate">/ '+opts.rateMax+'</span>',
						css : {
							top: (e.pageY + opts.rateInfosY),
							left: (e.pageX + opts.rateInfosX)
						}
					}).appendTo('body').show();
				},
				mouseover : function(e){
					$(this).css('cursor','pointer');	
				},
				mouseout : function(){
					$(this).css('cursor','default');
					average.width(0);
				},
				mousemove : function(e){
					var realOffsetLeft = findRealLeft(this);
					var relativeX = e.pageX - realOffsetLeft;
					if(opts.step) newWidth = Math.floor(relativeX/starWidth)*starWidth + starWidth;
					else newWidth = relativeX;
					average.width(newWidth);					
					if (opts.showRateInfo)
					$("p.tdratingInfos")
					.css({
						left: (e.pageX + opts.rateInfosX)
					})
					.html(getNote(newWidth) +' <span class="maxRate">/ '+opts.rateMax+'</span>');
				},
				mouseleave : function(){
					$("p.tdratingInfos").remove();
				},
				click : function(e){
					$(this).unbind().css('cursor','default').addClass('jDisabled');
					if (opts.showRateInfo) $("p.tdratingInfos").fadeOut('fast',function(){$(this).remove();});
					e.preventDefault();
					var rate = getNote(newWidth);
					average.width(newWidth);					
					$.post(opts.phpPath,{
							idBox : idBox,
							rate : rate,
							action : 'rating'
						},
						function(data) {
							if(!data.error)
							{
							$('.msg').prepend('<div class="success" style="display: none;width:auto;">'+data.message+'<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
							//$('.success').fadeIn(2000).fadeOut(2000);
							var j=parseInt(data.avgrating)/opts.rateMax*widthRatingContainer
							$('.tdreview').fadeOut(200,function() {
    $('.success').fadeIn(700).fadeOut(1000,function(){$('.tdreview').fadeIn(700);} );});
	$('[class^=tdratingColor]'). css('width',j);
$('span[itemprop=ratingValue]').text(data.avgrating);
$('span[itemprop=reviewCount]').text(data.totalrating);	
			if($('[class^=box-product]').find('[class=name]').children('a[href$="'+data.product+'"]').parent().siblings('[class=price]').siblings().hasClass('rating')){
			$('[class^=box-product]').find('[class=name]').children('a[href$="'+data.product+'"]').parent().siblings('[class=rating]').find('img').attr('src',$('[class^=box-product]').find('[class=name]').children('a[href$="'+data.product+'"]').parent().siblings('[class=rating]').find('img').attr('src').replace(/[0-9]+/, parseInt(data.avgrating)));}
			else
			{
			$('[class^=box-product]').find('[class=name]').children('a[href$="'+data.product+'"]').parent().siblings('[class=price]').after('<div class="rating">'+
'<img alt="Based on 6 reviews." src="catalog/view/theme/default/image/stars-'+data.avgrating+'.png" >'
+'</div>');
			}
            $('.tdratingAverage').css('width',j);
							}
							else
							{
							
							$('.msg').prepend('<div class="warning" style="display: none;width:auto;">'+data.message+$('div#content h1').text()+'<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
							$('.msg').css('top','60px');
							$('.tdreview').fadeOut(200,function() {
    $('.warning').fadeIn(700).fadeOut(2000,function(){$('.tdreview').fadeIn(700);} );});
							}
						},
						'json'
					);
				}
			});

			function getNote(relativeX) {
				var noteBrut = parseFloat((relativeX*100/widthRatingContainer)*opts.rateMax/100);
				switch(opts.decimalLength) {
					case 1 :
						var note = Math.round(noteBrut*10)/10;
						break;
					case 2 :
						var note = Math.round(noteBrut*100)/100;
						break;
					case 3 :
						var note = Math.round(noteBrut*1000)/1000;
						break;
					default :
						var note = Math.round(noteBrut*1)/1;
				}
				return note;
			};

			function getStarWidth(){
				switch(opts.type) {
					case 'small' :
						starWidth = 12;
						starHeight = 10;
						bgPath = opts.smallStarsPath;
					break;
					default :
						starWidth = 23;
						starHeight = 20;
						bgPath = opts.bigStarsPath;
				}
			};
			
			function findRealLeft(obj) {
			  if( !obj ) return 0;
			  return obj.offsetLeft + findRealLeft( obj.offsetParent );
			};
		});

	}
})(jQuery);