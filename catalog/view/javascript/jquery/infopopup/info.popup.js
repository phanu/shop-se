function positionModalPopup() {
	var popupHeight = $('#info-popup').outerHeight();
	var popupWidth = $('#info-popup').outerWidth();
	var bottomPos = (popupHeight + parseInt($('#info-popup').css('top')));
	var viewportHeight = $(window).height();
	var viewportWidth = $(window).width();
	
	if (popupWidth > viewportWidth) {
		$('#info-popup').css('margin-left','0');
		$('#info-popup').css('top','0');
		$('#info-popup').css('left','0');
		$('#info-popup').css('width', (viewportWidth - 41) + 'px');
	}
	
	if (bottomPos > viewportHeight) {
		$('#info-popup').css('top','0');
		$('#info-popup').css('position','absolute');
	}
}

function areCookiesEnabled() {
    var cookieEnabled = (navigator.cookieEnabled) ? true : false;
    if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled) { 
        document.cookie="testcookie";
        cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
    }
    return (cookieEnabled);
}

function loadInfoPopup(transition) {
	
	if (areCookiesEnabled()) {
		positionModalPopup();
		$(window).resize(function() {
			positionModalPopup();
		});
		
		if (transition == 'fold_in') {
			$("#info-popup").show("slow");
			$("#info-popup").fadeIn("slow");
		} 
		else if (transition == 'fade_in') {
			$("#info-popup").fadeIn(2000);
		}
		else if (transition == 'animate_in_easeoutexpo') {
			popup_height = $("#info-popup").height();
			popup_top = $("#info-popup").css("top");
			$("#info-popup").css({
				"opacity": "0",
				"top": 	   "-" + popup_height + "px"}
			);
			$("#info-popup").show();
			$("#info-popup").animate({
				"opacity": "1",
				"top": 	   popup_top},
				1500,
				'easeOutExpo'
			);
		}
		else if (transition == 'animate_in_easeoutback') {
			popup_top = $("#info-popup").css("top");
			$("#info-popup").css({
				"opacity": "0",
				"top": 	   "-100px"}
			);
			$("#info-popup").show();
			$("#info-popup").animate({
				"opacity": "1",
				"top": 	   popup_top},
				1800,
				'easeOutBack'
			);
		}
		else if (transition == 'animate_in_easeoutbounce') {
			popup_height = $("#info-popup").height();
			popup_top = $("#info-popup").css("top");
			$("#info-popup").css({
				"opacity": "0",
				"top": 	   "-" + popup_height + "px"}
			);
			$("#info-popup").show();
			$("#info-popup").animate({
				"opacity": "1",
				"top": 	   popup_top},
				1800,
				'easeOutBounce'
			);
		}
		else if (transition == 'animate_in_easeoutelastic') {
			popup_height = $("#info-popup").height();
			popup_top = $("#info-popup").css("top");
			$("#info-popup").css({
				"opacity": "0",
				"top": 	   "-" + popup_height + "px"}
			);
			$("#info-popup").show();
			$("#info-popup").animate({
				"opacity": "1",
				"top": 	   popup_top},
				2000,
				'easeOutElastic'
			);
		}
		else {
			$("#info-popup").show();
		}
	} else {
		$("#info-popup-bg").remove();
		$("#info-popup").remove();
	}
}