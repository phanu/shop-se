function simple_tooltip(target_items, name){
 $(target_items).each(function(i){
		$("body").append("<div class='"+name+"' id='"+name+i+"'><p>"+$(this).attr('title')+"</p></div>");
		var my_tooltip = $("#"+name+i);
		
		$(this).removeAttr("title").mouseover(function(){
				my_tooltip.css({opacity:0.8, display:"none"}).fadeIn(400);
		}).mousemove(function(kmouse){
				my_tooltip.css({left:$(this).offset().left - ( (my_tooltip.width() - $(this).width()) / 2) , top:$(this).offset().top-60});
		}).mouseout(function(){
				my_tooltip.fadeOut(400);				  
		});
	});
}

//SETTING UP OUR POPUP
//0 means disabled; 1 means enabled;
var popupStatus = 0;
var autosuggest = 0;

//loading popup with jQuery magic!
function loadPopup(){
	//loads popup only if it is disabled
	if(popupStatus==0){
		$("#popupFixedBar").slideDown("slow");
		popupStatus = 1;
	}
}

//disabling popup with jQuery magic!
function disablePopup(){
	//disables popup only if it is enabled
	if(popupStatus==1){
		//$("#popupFixedBar").fadeOut("slow");
		$("#popupFixedBar").slideUp("slow");
		popupStatus = 0;
	}
}


function customSetLocationPopup(object){
	$("#popupFixedBar").css({
		"left": $(object).offset().left - 10
	});
}

//centering popup
function centerPopup(){
	//request data for centering
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $("#popupFixedBar").height();
	var popupWidth = $("#popupFixedBar").width();
	//centering
	$("#popupFixedBar").css({
		"left": windowWidth/2-popupWidth/2
	});
	
}

function hideFixBSpecialOffer(offer_number){
    $('#fixed-bar-special-offer-product-' + offer_number + ' .image').slideUp('slow');
	$('#fixed-bar-special-offer-product-' + offer_number).hide();
}

function showFixBSpecialOffer(offer_number){
	$('#fixed-bar-special-offer-product-' + offer_number).show();
	$('#fixed-bar-special-offer-product-' + offer_number + ' .image').slideDown('slow');
}


//CONTROLLING EVENTS IN jQuery
$(document).ready(function(){
	$('#fixb-a-contact').bind('click', function(){
		var current_obj = $(this);
		
		$.ajax({
			url: 'index.php?route=module/fixed_bar/contact',
			dataType: 'json',
			success: function(json){
				$('#popupFixedBarContent').html(json['output']);
				customSetLocationPopup(current_obj);
				//load popup
				loadPopup();
			}
		});
	});
	
	$('#fixb-a-login').bind('click', function(){
		var current_obj = $(this);
		
		$.ajax({
			url: 'index.php?route=module/fixed_bar/login',
			dataType: 'json',
			success: function(json){
				$('#popupFixedBarContent').html(json['output']);
				customSetLocationPopup(current_obj);
				//load popup
				loadPopup();
			}
		});
	});
	
	$('#fixb-a-facebook').bind('click', function(){
		var current_obj = $(this);
		
		$.ajax({
			url: 'index.php?route=module/fixed_bar/facebook',
			dataType: 'json',
			success: function(json){
				$('#popupFixedBarContent').html(json['output']);
				customSetLocationPopup(current_obj);
				//load popup
				loadPopup();
			}
		});
	});
	
	$('#fixb-a-twitter').bind('click', function(){
		var current_obj = $(this);
		
		$.ajax({
			url: 'index.php?route=module/fixed_bar/twitter',
			dataType: 'json',
			success: function(json){
				$('#popupFixedBarContent').html(json['output']);
				customSetLocationPopup(current_obj);
				//load popup
				loadPopup();
			}
		});
	});
	
	$('#fixb-search-term').bind('keyup', function(){
		var current_obj = $(this);
		
		if ($(this).val().length > 0){
			$.ajax({
				url: 'index.php?route=module/fixed_bar/search',
				data: 'filter_name=' + encodeURIComponent($(this).val()),
				success: function(data){
					$('#popupFixedBarContent').html(data);
					
					customSetLocationPopup(current_obj);
					
					if (popupStatus == 0  ){  
						loadPopup();
					}	
				}
			});
		} else {
			disablePopup();
		}		
		
	});
	
	//LOADING POPUP
	/*
	$("#button").bind('click', function(){
		//centering with css
		centerPopup();
		//load popup
		loadPopup();
	});
	
	*/
				
	//CLOSING POPUP
	//Click the x event!
	$("#popupFixedBarClose").click(function(){
		disablePopup();
	});

	//Press Escape event!
	$(document).keypress(function(e){
		if(e.keyCode==27 && popupStatus==1){
			disablePopup();
		}
	});

});