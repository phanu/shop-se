$(document).ready(function() {
	
	$('#fb-login').bind('click', function(){
		FB.login(function(response) {}, { scope: 'email,publish_stream'});   
	}); 
	
	/* Help button */
	$('#help-button').bind('click', function(){
		$('#dialog-help').dialog({
			modal: true,
			width: 500,
			height: 500,
			resizable: false
		});
		
		$(".ui-dialog-titlebar").hide();
	}); 
	
	/* Help button */
	$('#rank-button').bind('click', function(){
		$.ajax({
			type: 'POST',
			url: 'index.php?route=facebook_game/home/rank',
			dataType: 'json',
			success: function(json){
				$('#rank-table').html(json['output']);
			}
		});
		
		$('#dialog-rank').dialog({
			modal: true,
			width: 500,
			height: 500,
			resizable: false
		});
		
		$(".ui-dialog-titlebar").hide();
	}); 
	
	$('.dialog-close').bind('click', function(){
		var dialog_target = $(this).attr('id').replace('close-', '');
	
		$('#' + dialog_target).dialog('close');	
	}); 
	
});




