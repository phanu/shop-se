<?php
/*--------------------------------------------------------
# Opening Hours 1.2 - OpenCart Module
# --------------------------------------------------------
# For OpenCart 1.5.1.x - 1.5.4.x
# Copyright (C) 2012 ThyConsultants.net. All Rights Reserved.
# @license Copyrighted Commercial Software
# Demo: http://demo.thyconsultants.net/opencart
# Website: http://www.thyconsultants.net/
# Support: info@thyconsultants.net
-------------------------------------------------------- */

// Heading 
$_['heading_title']    = 'Opening Hours';

// Text
$_['text_mon']             = 'จันทร์';
$_['text_tue']             = 'อังคาร';
$_['text_wed']             = 'พุธ';
$_['text_thur']             = 'พฤหัสบดี';
$_['text_fri']        = 'ศุกร์';
$_['text_sat']        = 'เสาร์';
$_['text_sun']        = 'อาทิตย์';
?>
