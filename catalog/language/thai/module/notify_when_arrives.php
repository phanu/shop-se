<?php

$_['heading_title']     = 'สินค้าหมด' ;

#text---------------------------------------------------------------------------

$_['text_description']  = '<h3>เพียงกรอกข้อมูลแล้ว กด Send เมื่อสินค้าชิ้นนี้เพิ่มเข้ามาในล็อตใหม่ ระบบจะส่งอีเมลเพื่อแจ้งท่านโดยอัตโนมัติ :</h3>';

$_['text_description_2']  = '<h3>เพียงกรอกข้อมูลแล้ว กด Send เมื่อสินค้าชิ้นนี้เพิ่มเข้ามาในล็อตใหม่ ระบบจะส่งอีเมลเพื่อแจ้งท่านโดยอัตโนมัติ :</h3>';

$_['text_success'] =  'ลงทะเบียนเรียบร้อย!<br/>ท่านจะได้รับอีเมลแจ้ง เมื่อสินค้าล็อตใหม่มา ขอบคุณค่ะ';

$_['mail_admin_subject'] =  'New request for out of stock product - {product_name}';

$_['mail_admin_body'] =  '
<b>Request Details</b>
<br/><br/>
Customer name: {customer_name}
<br/><br/>
Customer e-mail: {customer_email}
<br/><br/>
Customer phone: {customer_phone}
<br/><br/>
Customer {custom_field}: {customer_custom}
<br/><br/>
Product: {product_name}
';

$_['text_error_mail']  = 'รูปแบบ Email ไม่ถูกต้อง!';
$_['text_error_name']  = 'รูปแบบ Name ไม่ถูกต้อง!';
$_['text_error_phone']  = 'รูปแบบ Phone ไม่ถูกต้อง!';
$_['text_error_custom']  = '{custom_name} data is not valid!';
$_['text_error_captcha']  = 'Captcha code ที่กรอกไม่ตรงกับรูปภาพ!';

$_['text_error_data'] = 'Invalid form data.';
#entry
$_['nwa_entry_name'] = 'ชื่อ';
$_['nwa_entry_phone'] = 'หมายเลขโทรศัพท์มือถือ';
$_['nwa_entry_mail'] = 'E-mail';
$_['nwa_entry_captcha'] = 'กรุณาพิมพ์ตามรูปภาพด้านล่าง:';
#buttons---------------------------------------------------------------------------

$_['button_register']  = 'Send';
$_['button_category'] = 'แจ้งเตือนเมื่อสินค้ามา!';
$_['button_close'] = 'Close';

?>