<?php
// Heading 
$_['heading_title']  = 'TwitterLogin';
$_['twitter_email_desc'] = '<p><strong>Please provide your email address in order to finish the login process. Your email will be required only when you login for first time.</strong></p><p>You will be able to edit this information from your OpenCart Account Information.</p>';
$_['twitter_email_submit'] = 'Submit';
?>