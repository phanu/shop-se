<?php

$_['heading_title'] = 'Out of stock!';

#text---------------------------------------------------------------------------

$_['text_description']  = 'Enter your email here to be informed when this product arrives:';
$_['text_success'] =  'E-mail registered!<br/>You will be informed when this product arrives. Thank You!';
$_['text_error_mail']  = 'This e-mail is not valid!';
$_['text_error_data'] = 'Invalid form data.';

#buttons---------------------------------------------------------------------------

$_['button_register']  = 'Send';
$_['button_category'] = 'Notify me!';
$_['button_close'] = 'Close';

?>