<?php
// Heading 
$_['heading_title']    = 'พันธมิตร';

// Text
$_['text_register']    = 'ลงทะเบียน';
$_['text_login']       = 'เข้าระบบ';
$_['text_logout']      = 'ออกระบบ';
$_['text_forgotten']   = 'ลืมรหัสผ่าน';
$_['text_account']     = 'บัญชีพันธมิตรการค้า';
$_['text_edit']        = 'แก้ไขบัญชี';
$_['text_password']    = 'รหัสผ่าน';
$_['text_payment']     = 'ตัวเลือกการจ่ายเงิน';
$_['text_tracking']    = 'ลิงค์ติดตาม';
$_['text_transaction'] = 'ธุรกรรม';
?>
