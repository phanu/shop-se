<?php
//-----------------------------------------------------
// OpenCart backup to Google Drive™
// Created by DmitryNek
// exmail.Nek@gmail.com
//-----------------------------------------------------

// Log
$_['log_start_backup']				= 'Started';
$_['log_error_zip']					= 'Can\'t create zip archive %s';
$_['log_error_upload']				= 'Failed to upload file %s to Google Drive!';
$_['log_error_add_to_zip']			= 'File was nor archived %s';
$_['log_error_common']				= 'Error: %s';
$_['log_uploaded_file']				= 'File %s uploaded to Google Drive';
$_['log_done']						= 'Finished';
$_['log_error_auth']				= 'Backup is stopped because authrization is failed!';
$_['log_error_directory_writable']	= 'Backup cannot be created because the backup directory (\'%s\') is not writable';
$_['log_error_file_size']			= 'File is too big %s';
$_['log_error_file_not_readable']	= 'File is not readable %s';
$_['log_error_folder']				= 'Module does not have write access to \'%s\', please create the folder \'%s\' manually';
$_['log_error_htaccess']			= 'Backup cannot be created because the backup directory (\'%s\') is not writable';
$_['log_error_token']				= 'Did not receive an access token from Google!';
$_['log_error_media_link']			= 'Could not retrieve resumable create media link';
$_['log_error_response']			= 'Bad response: %s';
$_['log_error_bad_response']		= 'Bad response: %s Response header: %s Response body: %s Request URL: %s';
$_['log_error_no_response']			= 'Received no response from %s while trying to upload bytes %s';
$_['log_error_unable_request']		= 'Unable to request file from %s';
$_['log_error_xml']					= 'Could not create SimpleXMLElement from %s';
$_['log_zip_progress']				= 'Creating zip archive: %s';
$_['log_upload_progress']			= 'Uploading to Google Drive: %s uploaded';
$_['log_db_progress']				= 'Creating database backup: %s';

?>
