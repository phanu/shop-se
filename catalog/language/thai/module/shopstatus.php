<?php
// Heading 
$_['heading_title'] = 'STATISTICS';

// Text
$_['text_start']  = 'เปิดร้านเมื่อ';
$_['text_update']  = 'ร้านค้าอัพเดท';
$_['text_pageviews']  = 'หน้าที่เข้าชม';
$_['text_visitors']  = 'สถิติผู้เยี่ยมชม';

$_['text_products']  = 'สินค้าทั้งหมด';
$_['text_categories']  = 'หมวดหมู่ทั้งหมด';
$_['date_format_short']     = 'd/m/Y';
?>