<?php
//Button
$_['button_register_now']  = 'บันทึกข้อมูล';

// Entry

$_['entry_telephone']      = 'หมายเลขโทรศัพท์:';
$_['entry_fax']            = 'หมายเลขโทรศัพท์สำรอง(ถ้ามี):';
$_['entry_company']        = 'Company:';
$_['entry_company_id']     = 'Company ID:';
$_['entry_tax_id']         = 'Tax ID:';
$_['entry_address_1']      = 'เลขที่ และ ถนน/ซอย/หมู่:';
$_['entry_address_2']      = 'ตำบล/แขวง :';
$_['entry_postcode']       = 'รหัสไปรษณีย์:';
$_['entry_city']           = 'อำเภอ/เขต:';
$_['entry_country']        = 'ประเทศ:';
$_['entry_zone']           = 'จังหวัด:';


// Error
$_['error_telephone']      = 'โทรศัพท์ต้องมี 3 ถึง 32 ตัวอักษร!';
$_['error_fax']            = 'โทรศัพท์ต้องมี 3 ถึง 32 ตัวอักษร!';
$_['error_company']        = 'Company required!';
$_['error_company_id']     = 'Company ID required!';
$_['error_tax_id']         = 'Tax ID required!';
$_['error_address_1']      = 'ช่องเลขที่บ้านต้องมี 3 ถึง 128 ตัวอักษร!';
$_['error_address_2']      = 'ตำบลต้องมี 3 ถึง 128 ตัวอักษร!';
$_['error_city']           = 'อำเภอต้องมี 3 ถึง 128 ตัวอักษร!';
$_['error_postcode']       = 'รหัสไปรษณีย์ต้องมี 2 ถึง 10 ตัวอักษร!';
$_['error_country']        = 'กรุณาเลือกประเทศ!';
$_['error_zone']           = 'กรุณาเลือกจังหวัด!';

?>