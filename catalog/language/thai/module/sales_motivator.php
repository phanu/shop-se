<?php

// Text
$_['text_you_can_get']    = 'Share and you can get discount';
$_['text_for_friend']     = 'for friend'; 
$_['text_for_share']      = 'for share'; 
$_['text_explain_info']   = 'Please fill Adress and telephone because wasn\'t posible to get from your Facebook account.<br />This informations are required for shipping your orders.';

$_['text_share_product']  = 'Share this product (required)';
$_['text_invite_fb_friend']= 'Invite your Facebook friends (optional)';
$_['text_like_product']   = 'Like this product (optional)';
$_['text_like_fan_page']  = 'Like our fan page (optional)';
$_['text_rules']          = 'Rules';
$_['text_your_invite_link']= 'Your Invite link (promote using tool below or any other tool)';

// share status action text

$_['text_success_reward']         = 'Success: Message posted to your Facebook wall and your account was rewarded.'; 
$_['text_error_share_limit']      = 'Message was posted to your Facebook wall but share reward is set to 1 reward at %s hours / customer'; 
$_['text_error_already_rewarded'] = 'You already received reward for sharing this product'; 
$_['text_success_invite']         = 'Message was sent to your friend(s). For each friend who click that link and add an order you will receive an bonus.'; 

// Button
$_['button_get_discount'] = 'Get Discount Now';
$_['button_register']     = 'Register Now';
$_['button_share_now']    = 'Share now';
$_['button_invite_friend']= 'Invite friends';
$_['button_read_rules']   = 'Read Rules';

// Entry more data form

$_['entry_telephone']      = 'Telephone:';
$_['entry_address_1']      = 'Address 1:';
$_['entry_address_2']      = 'Address 2:';
$_['entry_postcode']       = 'Post Code:';
$_['entry_city']           = 'City:';
$_['entry_country']        = 'Country:';
$_['entry_zone']           = 'Region / State:';


// Error
$_['error_telephone']      = 'Telephone must be between 3 and 32 characters!';
$_['error_address_1']      = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']           = 'City must be between 2 and 128 characters!';
$_['error_postcode']       = 'Postcode must be between 2 and 10 characters!';
$_['error_country']        = 'Please select a country!';
$_['error_zone']           = 'Please select a region / state!';

?>