<?php
// Text
$_['text_participate_win']         = 'แค่กด  Like / Share / invite ก็มีสิทธิ์รับของรางวัลฟรี ... ';
$_['text_join_and']                = 'แค่ร่วม ก็มีสิทธิ์';
$_['text_win']                     = 'รับฟรี';
$_['text_a_product']               = 'ของรางวัล';
$_['text_discount']                = 'credits';
$_['text_if_you']                  = 'แค่คุณ';
$_['text_like_share']              = 'like';
$_['text_the_product']             = 'ของรางวัลชิ้นนี้';
$_['text_are_needed']              = 'ดีลนี้ต้องการอีก';
$_['text_people']                  = 'คน เพื่อปลดล็อค';
$_['text_x_people_needed']         = 'จำนวนคนที่ใช้ปลดล็อคดีลสินค้าชิ้นนี้.';
$_['text_invite_friends']          = 'Invite เพื่อนของคุณ!';
$_['text_new_participants']        = 'ผู้เข้าร่วมล่าสุด:';
$_['text_remaining_time']          = 'Remaining time';
$_['text_view_all']                = 'ดูทั้งหมด';
$_['text_deal_participants']       = 'ผู้เข้าร่วม';
$_['text_how_to_win']              = '<strong>กติกาเลือกผู้โชคดี</strong><br /><i>กดปุ่ม\'เข้าร่วมกิจกรรม\' และกด Like ,Share หรือ Invite<br /> เพื่อสะสม Deal Point เมื่อสิ้นสุดกิจกรรม ผู้ที่มีคะแนน Deal Point มากที่สุดจะเป็นผู้ที่ได้รับรางวัล</i>';
$_['text_points_rules']            = '<strong>กฏการให้คะแนน:</strong><br /><i>Like -  เพื่อรับสิทธิ์ในการ Share / Invite รับ %s Deal Point <br />Share -  1 ครั้ง รับ %s Deal Point<br />Invite friends - 1 ครั้ง รับ %s Deal Point</i>';


// Counter
$_['text_days']                    = 'วัน';
$_['text_hours']                   = 'ชั่วโมง';
$_['text_minutes']                 = 'นาที';
$_['text_seconds']                 = 'วินาที';
$_['text_day']                     = 'วัน';
$_['text_hour']                    = 'ชั่วโมง';
$_['text_minute']                  = 'นาที';
$_['text_second']                  = 'วินาที';

// Participate form
$_['text_like']                    = 'LIKE *';
$_['text_share']                   = 'SHARE';
$_['text_invite']                  = 'INVITE FRIENDS';
$_['text_required']                = 'required - ดูรายละเอียด';
$_['text_optional']                = 'optional - ดูรายละเอียด';
$_['text_more_info']               = 'ข้อมูลเพิ่มเติม';
$_['text_participate_info']        = 'Share และ Invite จะเปิดให้กดเมื่อท่านกด Like แล้วเท่านั้น ';
$_['text_rules_info']              = 'กด Like  -  เพื่อรับสิทธิ์ในการ Share / Invite ได้รับ %s Deal Point<br />กด Share - ต่อ 1 ครั้ง รับ %s Deal Point<br />Invite เพื่อน -  ต่อ 1 ครั้ง รับ  %s Deal Point';
$_['text_feed_name']               = '%s - Facebook Deal Offer'; 
$_['text_feed_caption']            = 'If you want this product JOIN NOW and can be yours'; 
$_['text_feed_description']        = 'Facebook Deal is an contest where you can win products/discount using your Facebook Account. Goto %s like product, share, invite friend and increase your chance to win.'; 
$_['text_action_success']          = 'ขอแสดงความยินดี. ขณะนี้คุณมีคะแนนสะสมรวม %s คะแนน';

// Button
$_['button_participate']           = 'เข้าร่วมกิจกรรม';

?>