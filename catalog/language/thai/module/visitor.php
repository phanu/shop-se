<?php
// Heading
$_['heading_title']	= 'ผู้เยี่ยมชม';
$_['text_link'] = '<img src="catalog/view/theme/default/image/counter/counter.gif"  />';
// Texts
$_['text_today']	= 'คน ในวันนี้';
$_['text_week']		= 'คน ในสัปดาห์นี้';
$_['text_month']	= 'คน ในเดือนนี้';
$_['text_year']		= 'คน ในปีนี้';
$_['text_all']		= 'คนทั้งหมด';
$_['text_online']	= 'คน ออนไลน์';
?>