<?php
// Heading 
$_['heading_title']    = 'ตรวจสอบสถานะการบรรจุสินค้าของทางร้าน';

$_['text_no_order']		= 'There are no order for this combination.';
$_['text_wait']         = 'Please Wait!';
$_['text_error']        = 'Order not found!';
$_['text_success']      = 'Success. <a href="%s">Click here</a>.';

// Entry
$_['entry_order_id']    = 'กรอก รหัสคำสั่งซื้อ ';
$_['entry_email']       = 'กรอก Email:';
$_['entry_captcha']     = 'กรอกรหัสให้เหมือนด้านขวา:';

// Button
$_['button_ordertrack']   = 'เช็คสถานะ';

// Error
$_['error_order_id']      = 'Error: Order id must be numeric!';
$_['error_tracking_order']= 'Error: Invalid Order id or Email! You cannot try more then 3 times!';
$_['error_order_msg']     = 'Error: Invalid Order id! Same order you cannot try more than 3 Times!';
$_['error_email']         = 'Error: E-Mail Address does not appear to be valid!';
$_['error_captcha']       = 'Error: Verification code does not match the image!';

?>
