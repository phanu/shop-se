<?php
// Heading 
$_['heading_title_1']    = 'User Login';
$_['heading_title_2']    = 'My account';

// Text
$_['text_register']         = 'สมัครสมาชิก';
$_['text_login']            = 'Login';
$_['text_logout']           = 'Logout';
$_['text_forgotten']        = 'ลืมรหัสผ่าน';
$_['text_account']          = 'หน้าแรกสำหรับสมาชิก';
$_['text_edit']             = 'แก้ไขข้อมูลสมาชิก';
$_['text_edit_address']     = 'แก้ไขข้อมูลที่อยู่';
$_['text_password']         = 'แก้ไขรหัสผ่าน:</br>';
$_['text_wishlist']         = 'แก้ไขรายการโปรด';
$_['text_order']            = 'ประวัติการสั่งซื้อ และ Tracking Number';
$_['text_download']         = 'ดาวน์โหลด';
$_['text_return']           = 'ดูคำขอตีคืน';
$_['text_transaction']      = 'ประวัติการใช้เครดิตเงินสด';
$_['text_newsletter']       = 'สมัคร / ยกเลิก จดหมายข่าว';
$_['entry_email_address']	= 'อีเมลล์:</br>';
$_['entry_password']    	= 'เปลี่ยนรหัสผ่าน:';
$_['text_welcome']			= 'ยินดีต้อนรับค่ะ คุณ';
$_['text_logged']   	    = 'ยินดีต้อนรับเข้าสู่สังคมชาวอารยธรรม Sony ค่ะ คุณ, <strong>%s</strong>';
$_['text_guest']	        = ', <strong>ลูกค้าผู้มาเยือน</strong>.';
$_['text_my_newsletter']    = 'Newsletter';
$_['text_my_account']       = 'My Account';
$_['text_my_orders']        = 'My Orders';
$_['text_reward']           = ''; 
?>
