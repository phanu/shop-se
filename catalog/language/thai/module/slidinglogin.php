<?php
// -------------------------------------------
// Sliding Login for OpenCart v1.5.1.x
// By Best-Byte
// www.best-byte.com
// -------------------------------------------

// Heading
$_['heading_title']    			= 'Sliding Login';

// Text
$_['text_module']      			= 'Modules';
$_['text_success']     			= 'Success: You have modified module Sliding Login!';
$_['text_module_settings'] 	= 'Note: These settings will be applied to all the modules in the list below.';

// Entry
$_['entry_height']	      = 'Height of content box in pixels:<br /><span class="help">(eg. 300)</span> If left blank height is automatic.'; 
$_['entry_width']	        = 'Width of content box in pixels:<br /><span class="help">(eg. 300)</span> If left blank width is automatic.'; 
$_['entry_color']	        = 'Background color of content box in hex:<br /><span class="help">(eg. ffffff)</span>'; 
$_['entry_border']	      = 'Border around content box. Color in hex, size and style:<br /><span class="help">(eg. 000000 1px solid)</span>Leave blank for no border.'; 
$_['entry_padding']	      = 'Padding inside content box in pixels:<br /><span class="help">(eg. 20)</span>'; 
$_['entry_image']	        = 'Name of the image for the tab:<br /><span class="help">(egs. login-left.png, login-right.png, login-top-bot.png, contact-left.png, contact-right.png, contact-top-bot.png)</span> Tab images are located in the catalog/view/theme/default/image/sliding-tabs folder. Place the images you create in this folder and place the name for them here.'; 
$_['entry_image_height']	= 'Height of the image for the tab in pixels:<br /><span class="help">(egs. 120, 122, 32, 40)</span>'; 
$_['entry_image_width']  	= 'Width of the image for the tab in pixels:<br /><span class="help">(egs. 32, 40, 120, 122)</span>';
$_['entry_location']   		= 'Location of the content box:';
$_['entry_speed']   		  = 'Speed of the slide in milliseconds:<br /><span class="help">(eg. 300)</span>';
$_['entry_trigger']   		= 'Action to trigger slide:';
$_['entry_from_top']   		= 'For left or right positions.<br />Position of tab from top in pixels:<br /><span class="help">(eg. 200)</span> Leave blank for top or bottom positions.';
$_['entry_from_left']   	= 'For top or bottom positions.<br />Position of tab from left in pixels:<br /><span class="help">(eg. 200)</span> Leave blank for left or right positions.';
$_['entry_fixed']       	= 'Fixed position for tab:';
$_['entry_loadout']       = 'Load the tab content expanded:';
$_['entry_display1']      = 'Which form content do you want displayed:'; 
$_['entry_click']	        = 'Click'; 
$_['entry_hover']	        = 'Hover'; 
$_['entry_true']	        = 'Yes'; 
$_['entry_false']	        = 'No'; 
$_['entry_left']	        = 'Left'; 
$_['entry_right']	        = 'Right'; 
$_['entry_top']	          = 'Top'; 
$_['entry_bottom']	      = 'Bottom'; 
$_['entry_login']	        = 'Login'; 
$_['entry_contact']	      = 'Contact';
$_['entry_template']			= 'Active Template:';
$_['entry_layout']        = 'Layout';
$_['entry_status']        = 'Status:';

// Error
$_['error_permission'] 		= 'Warning: You do not have permission to modify module Sliding Login!';       

?>