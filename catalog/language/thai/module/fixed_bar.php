<?php
// Text 
$_['text_fixb_home']        = 'Home';
$_['text_fixb_contact']     = 'Contact Us';
$_['text_fixb_login']       = 'Login';
$_['text_fixb_my_account']  = 'My Account';
$_['text_fixb_search']      = 'Search products';
$_['text_fixb_facebook']    = 'Facebook Fan Page';
$_['text_fixb_twitter']     = 'Latest Tweets';
$_['text_fixb_google']      = 'Google+ Page';
$_['text_fixb_youtube']     = 'Store Youtube Channel';
$_['text_loading_tweets']   = 'Loading tweets...';
$_['text_buy_now']          = 'buy now for ';
$_['text_fixb_facebook_connect']    = 'Login with Facebook';
$_['text_fixb_facebook_subscribe']  = 'Subscribe with Facebook';
?>