<?php
// Texts
$_['text_country']    = 'Country';
$_['text_language']   = 'Language';
$_['text_currency']   = 'Currency';

// Buttons
$_['button_accept']   = 'Accept';
$_['button_decline']  = 'Decline';
$_['button_ok']  	  = 'รับทราบ';
?>