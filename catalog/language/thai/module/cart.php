<?php
// Heading
$_['heading_title'] = 'ตะกร้าสินค้า';

// Text 
$_['text_subtotal'] = 'รวมย่อย:';
$_['text_empty']      = 'ตะกร้าสินค้าคุณยังว่าง โปรดเลือกสินค้าแล้วคลิ๊กที่ปุ่ม  "ใส่ตะกร้า"';
$_['text_confirm']  = 'Confirm?';
$_['text_remove']   = 'Remove';
$_['text_cart']  	= 'ดูตะกร้าแบบละเอียด';
$_['text_checkout'] = 'ชำระเงิน';
?>