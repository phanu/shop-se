<?php
//Heading 
$_['heading_price_match'] = 'ศูนย์รับแจ้งการพบเห็นราคาที่ถูกกว่า - %s'; 

// Text
$_['text_price_match'] = 'พบสินค้าชิ้นนี้ที่ไหนราคาถูกกว่าเรา บอกเราสิคะ ถ้าข้อมูลน่าสนใจเราจะติดต่อกลับเพื่อให้ราคาดังกล่าวกับลูกค้าคะ <a id="price-match-trigger">คลิ๊กที่นี่เพื่อบอกเรา</a>';
$_['text_success_price_match'] = 'Message sent to store owner. Thanks';

// Entry
$_['entry_price_match_customer_name']      = 'ชื่อ:';
$_['entry_price_match_customer_email']     = 'Email:';
$_['entry_price_match_customer_telephone'] = 'โทรศัพท์:';
$_['entry_price_match_competitor_link']    = 'ลิงค์ของสินค้าดังกล่าว';
$_['entry_price_match_competitor_price']   = 'ราคาที่พบเห็น';
$_['entry_price_match_message']            = 'ข้อความเพิ่มเติม';

// Button
$_['button_price_match_send_now'] = 'ส่งข้อมูล';

// Error
$_['error_price_match_name']             = 'ชื่อ - จำเป็นต้องกรอก';
$_['error_price_match_email']            = 'Email - จำเป็นต้องกรอก';
$_['error_price_match_competitor_link']  = 'ลิงค์ของสินค้าดังกล่าว - จำเป็นต้องกรอก';
$_['error_price_match_competitor_price'] = 'ราคาที่พบเห็น - ราคาผิดพลาด. โปรดตรวจสอบ.';

?>