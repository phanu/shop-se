<?php
// Heading 
$_['heading_title']  = 'Categories';
// Text
$_['text_date_added']      = 'Addded:';
$_['text_more']      = 'Details';
$_['text_empty']        = 'There are no articles to list in this category.';
$_['text_author']     = 'Author:';
$_['text_comments']      = ' %s comments'; 
$_['text_viewed']      = '  %s viewed';  
$_['text_default']      = 'Default';
$_['text_related']      = 'Related Post:';
?>