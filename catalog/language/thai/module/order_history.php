<?php

// Heading 
$_['heading_title']				= 'ประวัติการสั่งซื้อ';

// Text
$_['text_no_open_orders']		= 'ไม่มี order ที่เพิ่งสั่งซื้อ';
$_['text_no_pending_orders']	= 'ยังไม่มี order ที่เข้าสู่กระบวนการบรรจุ';
$_['text_no_closed_orders']		= 'ไม่มี  order ที่จัดส่งแล้ว';

?>
