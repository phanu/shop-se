<?php
// Heading 
$_['heading_title']    = 'บัญชี';

// Text
$_['text_register']    = 'ลงทะเบียน';
$_['text_login']       = 'เข้าระบบ';
$_['text_logout']      = 'ออกระบบ';
$_['text_forgotten']   = 'ลืมรหัสผ่าน';
$_['text_account']     = 'บัญชีของฉัน';
$_['text_edit']        = 'แก้ไขบัญชี';
$_['text_password']    = 'รหัสผ่าน';
$_['text_wishlist']    = 'รายการโปรด';
$_['text_order']       = 'ประวัติสั่งซื้อ';
$_['text_download']    = 'ดาวน์โหลด';
$_['text_return']      = 'ตีคืน';
$_['text_transaction'] = 'ธุรกรรม';
$_['text_newsletter']  = 'จดหมายข่าว';
?>
