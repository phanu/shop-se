<?php
// Heading 
$_['heading_title'] = 'Author';
$_['text_all_author'] = 'All Author';
$_['text_filter_post'] = 'Filter Post:';
$_['text_all_author'] = 'All Author';

$_['text_register']    = 'Register';
$_['text_login']       = 'Login';
$_['text_logout']      = 'Logout';
$_['text_forgotten']   = 'Forgotten Password';
$_['text_account']     = 'My Account';
$_['text_edit']        = 'Edit Account';
$_['text_password']    = 'Password';
?>