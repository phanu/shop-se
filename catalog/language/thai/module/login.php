<?php
// Heading 
$_['heading_title']				= 'เข้าระบบ';

// Entry
$_['entry_email_address']		= 'อีเมล์:';
$_['entry_password']			= 'รหัสผ่าน:';

// Buttons
$_['button_logout']				= 'ออกระบบ';
$_['button_create']			= 'สมัคร';
?>