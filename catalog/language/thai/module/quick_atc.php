<?php
// Entry
$_['entry_model'] 		= 'Enter Item Number:';

// Error
$_['error_blank'] 		= 'Please enter an item number!';
$_['error_out_of_stock']= 'Item Out of Stock!';
$_['error_not_found'] 	= 'Item not found!';
?>