﻿<?php
// Text
$_['text_order_next'] 	= 'เวลาก่อนการตัดรอบเหลืออีก';
$_['text_hours'] 		= 'ชั่วโมง &amp;';
$_['text_minutes'] 		= 'นาที';
$_['text_order_after'] 	= 'เวลาก่อนการตัดรอบเหลืออีก';
$_['text_no_work'] 		= 'Temporarily we are not accepting new orders. Please come back again later.';

// Today messages
$_['text_today'] 		= 'หากลูกค้าสั่งซื้อสินค้าและแจ้งชำระเงินก่อนเวลา 19.00 น ของวันนี้. สินค้าจะถูกจัดส่งในเช้า 10.00 น. วันถัดไป (ยกเว้นวันอาทิตย์ และวันหยุดนักขัตฤกษ์).';
$_['text_today_end'] 	= 'หากลูกค้าสั่งซื้อสินค้าและแจ้งชำระเงินก่อนเวลา 19.00 น. ของวันพรุ่งนี้ สินค้าจะถูกจัดส่งในเช้า 10.00 น. วันถัดไป (ยกเว้นวันอาทิตย์ และวันหยุดนักขัตฤกษ์).';

// Today Holiday messages
$_['text_today_holiday'] 		 = 'วันนี้อยู่ในช่วงวันหยุด. หากลูกค้าทำการสั่งซื้อสินค้าแล้วแจ้งการชำระเงินก่อน 19.00 น. สินค้าจะถูกจัดส่งในวัน %s.';
$_['text_today_holidays'] 	     = 'วันนี้อยู่ในช่วงวันหยุดจนถึงวัน %s หากลูกค้าทำการสั่งซื้อสินค้าแล้วแจ้งการชำระเงินในช่วงนี้สินค้าจะถูกจัดส่งในวัน %s.';
$_['text_today_holidays_number'] = 'วันนี้เป็นวันหยุด. ซึ่งทางร้านยังหยุดจัดส่งสินค้าอีกเป็นเวลา %s วัน หากคุณสั่งซื้อสินค้าและแจ้งชำระเงินภายในวันนี้ สินค้าจะถูกจัดส่งในวัน %s.';

// Tomorrow Holiday messages
$_['text_tomorrow_holiday'] 			= '%s is holiday. If you order after the end of the workday, your order will be processed on %s.';
$_['text_tomorrow_holidays'] 			= 'We have upcoming holidays on: %s. If you order after the end of the workday, your order will be processed on %s.';
$_['text_tomorrow_holidays_number'] 	= 'We have upcoming %s holidays. If you order after the end of the workday, your order will be processed on %s.';
$_['text_tomorrow_holiday_end'] 		= '%s is holiday. If you order now, your order will be processed on %s.';
$_['text_tomorrow_holidays_end'] 		= 'We have upcoming holidays on: %s. If you order now, your order will be processed on %s.';
$_['text_tomorrow_holidays_end_number'] = 'We have upcoming %s holidays. If you order now, your order will be processed on %s.';

// Text addons 
$_['text_workday_end'] = 'ขณะนี้ได้ผ่านเวลาตัดรอบ 19.00 น. ของวันนี้แล้ว  ';

// Delivery messages
$_['text_period_method'][1] = 'สินค้าจะถึงมือท่านไม่เกินประมาณวัน %s';

// Shipping messages
$_['text_period_method'][2] = ' ';

// Arrays
$_['text_week_days'] = array(
	1 => 'จันทร์', 
	2 => 'อังคาร', 
	3 => 'พุธ', 
	4 => 'พฤหัสบดี', 
	5 => 'ศุกร์', 
	6 => 'เสาร์', 
	7 => 'อาทิตย์'
);
?>