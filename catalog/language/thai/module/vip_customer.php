<?php
// text enclosed in curly bracket {} will be replaced by actual value.  For example, {vip_name} will be replaced by Gold

$_["title"] = "สถานะ VIP";

$_["text_login"] = "Please <a href='index.php?route=account/login'>login</a> to view your VIP status.";

$_["text_vip_name"] = "ระดับ VIP ปัจจุบัน:";
$_["text_vip_total"] = "ยอดสั่งซื้อรวม:";
$_["text_vip_discount"] = "ส่วนลดปัจจุบัน:";
$_["text_vip_discount_end_date"] = "";
$_["text_next_vip_discount_start_date"] = "";
$_["text_next_vip_name"] = "ระดับ VIP ถัดไป:";
$_["text_next_vip_discount"] = "ส่วนลดในระดับถัดไป:";
$_["text_amount_to_next"] = "ระดับ VIP ถัดไปยังต้องการยอดใช้จ่ายรวมอีก:";
$_["text_next_cutoff_date"] = "";
$_["text_highest_level"] = "ขอแสดงความยินดี ขณะนี้คุณอยุ่ในระดับ VIP สูงสุด";
$_["text_highest_vip_price"] = "Highest VIP Level Price:";
$_["text_footer_message"] = "* ส่วนลด VIP จะถูกคิดคำนวนกับสินค้าราคาปกติเท่านั้น .";

$_["vip_discount_total"] = "ส่วนลด VIP {vip_discount} (เฉพาะสินค้าราคาปกติ) ";
$_["vip_price"] = "<div style='color:green;'>ราคา VIP: {vip_price}</div>";
$_["vip_price_html"] = "
<style>
.vip_price_table {font-weight:normal; font-size:12px; margin-top:10px;}
.vip_row {display:table-row; background:#F0F0F0;}
.vip_name, .vip_price {display:table-cell; padding:2px 20px; border:1px solid white;}
.vip_title {font-weight:bold; font-size:14px;}
</style>

<div class='vip_price_table'><span class='vip_title'>ส่วนลด VIP (<a href='http://shop.se-update.com/SE-Update-VIP-Program' target='_blank'>ส่วนลด VIP คืออะไร</a>)</span> {vip_price_html}</div>
";

$_["email_subject"] = "{store_name} VIP Membership Notification";
$_["email_message"] = "
<div><a href='{store_url}'><img src='{store_logo_url}' alt='{store_name}'></a></div>
<div>
  Dear {customer_name},<br/>
  Your VIP level is now <b>{vip_name}</b> and your discount on regular price product is <b>{vip_discount}</b>.  Please <a href='{store_url}'>visit the store</a> and enjoy your new saving.<br/>
  Best Regards,<br/>
  {store_name}
</div>
";

?>
