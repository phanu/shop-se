<?php
// Heading 
$_['heading_title']        = 'ลงทะเบียน';

// Text
$_['text_account']         = 'บัญชี';
$_['text_register']        = 'ลงทะเบียน';
$_['text_account_already'] = 'หากเป็นลูกค้าเดิมของเรา, กรุณากดไปที่หน้า <a href="%s">เข้าสู่ระบบ</a>.';
$_['text_your_details']    = 'รายละเอียดของคุณ';
$_['text_your_address']    = 'ที่อยู่ของคุณ';
$_['text_newsletter']      = 'จดหมายข่าว';
$_['text_your_password']   = 'รหัสผ่าน';
$_['text_agree']           = 'ได้อ่านข้อตกลงการใช้งานแล้ว [ติ๊กเพื่อยอมรับ]<a class="thickbox" href="%s" alt="%s"><b>%s</b></a>';

// Entry
$_['entry_firstname']      = 'ชื่อ:';
$_['entry_lastname']       = 'นามสกุล:';
$_['entry_email']          = 'อีเมล:';
$_['entry_telephone']      = 'หมายเลขโทรศัพท์มือถือ:';
$_['entry_fax']            = 'หมายเลขโทรศัพท์สำรอง:';
$_['entry_company']        = 'หน่วยงาน/บริษัท:';
$_['entry_address_1']      = 'เลขที่ และ ถนน/ซอย/หมู่:';
$_['entry_address_2']      = 'ตำบล/แขวง :';
$_['entry_postcode']       = 'รหัสไปรษณีย์:';
$_['entry_city']           = 'อำเภอ/เขต:';
$_['entry_country']        = 'ประเทศ:';
$_['entry_zone']           = 'จังหวัด :';
$_['entry_newsletter']     = 'รับจดหมายข่าว:';
$_['entry_password']       = 'รหัสผ่าน:';
$_['entry_confirm']        = 'รหัสผ่าน อีกครั้ง:';

// Error
$_['error_exists']         = 'ผิดพลาด: มีคนใช้อีเมล์นี้แล้ว!';
$_['error_firstname']      = 'ชื่อต้องมี 1 ถึง 32 ตัวอักษร!';
$_['error_lastname']       = 'นามสกุลต้องมี 1 ถึง 32 ตัวอักษร!';
$_['error_email']          = 'รูปแบบอีเมลไม่ถูกต้อง!';
$_['error_telephone']      = 'โทรศัพท์ต้องมี 3 ถึง 32 ตัวอักษร!';
$_['error_password']       = 'รหัสผ่านต้องมี 3 ถึง 20 ตัวอักษร!';
$_['error_confirm']        = 'รหัสผ่าน 2 ครั้งไม่เหมือนกัน!';
$_['error_address_1']      = ' ต้องมี 3 ถึง 128 ตัวอักษร!';
$_['error_address_2']      = ' ต้องมี 3 ถึง 128 ตัวอักษร!';
$_['error_city']           = 'ต้องมี 3 ถึง 128 ตัวอักษร!';
$_['error_postcode']       = 'รหัสไปรษณีย์ต้องมี 2 ถึง 10 ตัวอักษร!';
$_['error_country']        = 'กรุณาเลือกประเทศ!';
$_['error_zone']           = 'กรุณาเลือกจังหวัด!';
$_['error_agree']          = 'ผิดพลาด:คุณต้อง ติ๊ก ที่กล่องยอมรับด้วย %s!';
?>