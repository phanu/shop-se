<?php
// Heading 
$_['heading_title']    = 'จดหมายข่าว';

// Text
$_['text_account']     = 'บัญชี';
$_['text_newsletter']  = 'จดหมายข่าว';
$_['text_success']     = 'สำเร็จ: จดหมายข่าวของคุณได้รับการปรับปรุง!';

// Entry
$_['entry_newsletter'] = 'สมัคร:';
?>
