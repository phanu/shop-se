<?php
// Heading 
$_['heading_title']      = 'เครดิตเงินสด';

// Column
$_['column_date_added']  = 'วันที่มี';
$_['column_description'] = 'คำอธิบาย';
$_['column_amount']      = 'รวม (%s)';

// Text
$_['text_account']       = 'บัญชี';
$_['text_transaction']   = 'ประวัติการใช้เครดิตเงินสดของคุณ';
$_['text_total']         = 'เครดิตเงินสด ของคุณคือ:';
$_['text_empty']         = 'คุณยังเครดิตเงินสด!';
?>