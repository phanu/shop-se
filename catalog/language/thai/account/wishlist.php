<?php
// Heading 
$_['heading_title'] = 'รายการโปรด';

// Text
$_['text_account']  = 'บัญชี';
$_['text_instock']  = 'มีสินค้าพร้อมจัดส่ง';
$_['text_success']  = 'สำเร็จ: คุณได้เพิ่ม <a href="%s">%s</a> ไปใน <a href="%s">รายการโปรดของคุณ</a>!';
$_['text_wishlist'] = 'รายการโปรด (%s)';
$_['text_login']    = 'คุณต้อง <a href="%s">เข้าระบบ</a> หรือ <a href="%s">สร้างบัญชีใหม่</a> ในการเก็บ <a href="%s">%s</a> เข้าสู่ <a href="%s">รายการโปรดของคุณ</a>!';
$_['text_empty']    = 'รายการโปรดของคุณ ตอนนี้ว่าง.';

// Column
$_['column_remove'] = 'เอาออก';
$_['column_image']  = 'ภาพ';
$_['column_name']   = 'ชื่อสินค้า';
$_['column_model']  = 'รุ่น';
$_['column_stock']  = 'สต็อค';
$_['column_price']  = 'ราคา';
$_['column_cart']   = 'หยิบใส่ตะกร้า';
?>