<?php
// Heading 
$_['heading_title']         = 'ประวัติการสั่งซื้อ และ Tracking Number';

// Text
$_['text_account']          = 'บัญชี';
$_['text_order']            = 'ข้อมูลการสั่งซื้อ';
$_['text_order_detail']     = 'รายละเอีียดการสั่งซื้อ';
$_['text_invoice_no']       = 'เลขที่อินวอยซ์:';
$_['text_order_id']         = 'รหัสคำสั่งซื้อ:';
$_['text_status']           = 'สถานะ:';
$_['text_date_added']       = 'วันที่สั่ง:';
$_['text_customer']         = 'ลูกค้า:';
$_['text_shipping_address'] = 'ที่อยู่จัดส่ง';
$_['text_shipping_method']  = 'วิธีการจัดส่ง:';
$_['text_payment_address']  = 'ที่อยู่บิล';
$_['text_payment_method']   = 'วิธีการชำระ:';
$_['text_products']         = 'สินค้า:';
$_['text_total']            = 'รวมทั้งหมด:';
$_['text_comment']          = 'คอมเม้นท์คำสั่งซื้อ';
$_['text_history']          = 'ประวัติการสั่งซื้อ และ Tracking Number';
$_['text_empty']            = 'ยังไม่มีการสั่งซื้อ!';
$_['text_error']            = 'ไม่พบการสั่งซื้อ!' ;
$_['text_error']            = 'ไม่พบการสั่งซื้อ!';
$_['text_action']            = 'หากต้องการปรับปรุงรายการ หรือคืนสินค้า กรุณาเลือก' ;
$_['text_reorder']            = 'ปรับปรุงรายการ' ;
$_['text_return']            = 'คืนสินค้า' ;
$_['text_selected']            = 'เลือก' ;
// Column
$_['column_name']           = 'ชื่อสินค้า';
$_['column_model']          = 'รุ่น';
$_['column_quantity']       = 'ปริมาณ';
$_['column_price']          = 'ราคา';
$_['column_total']          = 'รวม';
$_['column_date_added']     = 'วันสั่ง';
$_['column_status']         = 'สถานะ';
$_['column_comment']        = 'คอมเม้นท์';
?>
