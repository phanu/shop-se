<?php
// Heading 
$_['heading_title']      = 'ตีคืนสินค้า';

// Text
$_['text_account']       = 'บัญชี';
$_['text_return']        = 'ข้อมูลตีคืน';
$_['text_description']   = '<p>โปรดกรอกแบบฟอร์ม ด้านล่างเพื่อขอ หมายเลข RMA.</p>';
$_['text_order']         = 'ข้อมูลออเดอร์';
$_['text_product']       = 'ข้อมูลสินค้า &amp; เหตุผลการตีคืน';
$_['text_additional']    = 'ข้อมูลเพิ่มเติม';
$_['text_message']       = '<p>คำร้องขอตีคืนสินค้าได้ถูกส่งให้ เจ้าของร้าน!</p><p>คุณจะได้รับการติดต่อพร้อมคำแนะนำจากทางร้านต่อไป';
$_['text_return_id']     = 'รหัสตีคืน:';
$_['text_order_id']      = 'รหัสออเดอร์:';
$_['text_date_ordered']  = 'วันที่สั่ง:';
$_['text_status']        = 'สถานะ:';
$_['text_date_added']    = 'วันที่เพิ่ม:';
$_['text_customer']      = 'ลูกค้า:';
$_['text_comment']       = 'คอมเม้นท์การตีคืน';
$_['text_products']      = 'สินค้า:';
$_['text_history']       = 'ประวัติการตีคืน';
$_['text_empty']         = 'ยังไมมีรายการตีคืน!';
$_['text_error']         = 'ไม่พบรายการตีคืน!';

// Column
$_['column_name']        = 'ชื่อสินค้า';
$_['column_model']       = 'รุ่น';
$_['column_quantity']    = 'ปริมาณ';
$_['column_price']       = 'ราคา';
$_['column_opened']      = 'เปิด';
$_['column_comment']     = 'คอมเม้นท์';
$_['column_reason']      = 'เหตุล';
$_['column_action']      = 'กระทำ';
$_['column_date_added']  = 'วันที่ทำ';
$_['column_status']      = 'สถานะ';

// Entry
$_['entry_order_id']     = 'รหัส คำสั่งซื้อ:';
$_['entry_date_ordered'] = 'วันที่สั่ง:';
$_['entry_firstname']    = 'ชื่อ:';
$_['entry_lastname']     = 'นามสกุล:';
$_['entry_email']        = 'อีเมล์:';
$_['entry_telephone']    = 'โทรศัพท์:';
$_['entry_product']      = 'ชื่อสินค้า:';
$_['entry_model']        = 'รหัสสินค้า:';
$_['entry_quantity']     = 'ปริมาณ:';
$_['entry_reason']       = 'ช่วงในการตีคืน:';
$_['entry_opened']       = 'สินค้าเปิดแล้ว:';
$_['entry_fault_detail'] = 'ข้อบกพร่องหรืออื่นๆ:';
$_['entry_captcha']      = 'ใส่รหัสใต้กล่องด้านล่าง:';

// Error
$_['error_order_id']     = 'ต้องใส่รหัสคำสั่งซื้อ!';
$_['error_firstname']    = 'ชื่อ ต้องมี 1 ถึง 32 ตัวอักษร!';
$_['error_lastname']     = 'นามสกุล ต้องมี 1 ถึง 32 ตัวอักษร!';
$_['error_email']        = 'รูปแบบอีเมล์ไม่ถูกต้อง!';
$_['error_telephone']    = 'โทรศัพท์ ต้องมี 3 ถึง 32 ตัวอักษร!';
$_['error_product']      = 'คุณต้องเลืกสินค้าอย่างน้อย 1 รายการ!';
$_['error_name']         = 'ชื่อสินค้า ต้องมี 3 ถึง 255 ตัวอักษร!';
$_['error_model']        = 'รุ่นสินค้า ต้องมี 3 ถึง 64 ตัวอักษรs!';
$_['error_reason']       = 'คุณต้องเลือกช่วงของสินค้าตีคืน!';
$_['error_captcha']      = 'รหัสตรวจสอบไมถูกต้อง!';
?>