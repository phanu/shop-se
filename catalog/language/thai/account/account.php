	<?php
// Heading 
$_['heading_title']      = 'บัญชีสมาชิกของฉัน';

// Text
$_['text_account']       = 'บัญชีสมาชิก';
$_['text_my_account']    = 'บัญชีสมาชิกของฉัน';
$_['text_my_orders']     = 'คำสั่งซื้อของฉัน';
$_['text_my_newsletter'] = 'จดหมายข่าว';
$_['text_edit']          = 'แก้ไขข้อมูลบัญชี';
$_['text_password']      = 'เปลี่ยนรหัสผ่าน';
$_['text_address']       = 'แก้ไขข้อมูลที่อยู่';
$_['text_wishlist']      = 'แก้ไขรายการโปรด';
$_['text_order']         = 'ประวัติการสั่งซื้อ และ Tracking Number';
$_['text_download']      = 'ดาวน์โหลด';
$_['text_reward']        = 'คะแนนของคุณ'; 
$_['text_return']        = 'ดูคำขอตีคืน'; 
$_['text_transaction']   = 'ดูประวัติการใช้เครดิตเงินสด'; 
$_['text_newsletter']    = 'สมัคร / ยกเลิก จดหมายข่าว';
?>