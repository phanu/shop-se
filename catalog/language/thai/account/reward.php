<?php
// Heading 
$_['heading_title']      = 'คะแนนรางวัลของคุณ';

// Column
$_['column_date_added']  = 'วันที่ได้';
$_['column_description'] = 'คำอธิบาย';
$_['column_points']      = 'คะแนน';

// Text
$_['text_account']       = 'บัญชี';
$_['text_reward']        = 'คะแนนรางวัล';
$_['text_total']         = 'คะแนนรางวัลของคุณคือ:';
$_['text_empty']         = 'คุณยังไม่มีคะแนนรางวัล!';
?>