<?php
// text enclosed in asterisks * will be replaced by actual value.  For example, *vip_name* will be replaced by Gold

$_["heading_title"] = "VIP Status";

$_["email_subject"] = "VIP Membership Notification";
$_["email_message"] = "*customer_name* VIP level is now *vip_name*";

?>
