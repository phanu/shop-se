<?php
// Heading 
$_['heading_title']   = 'ลืมรหัสผ่าน?';

// Text
$_['text_account']    = 'บัญชี';
$_['text_forgotten']  = 'ลืมรหัสผ่าน';
$_['text_your_email'] = 'อีเมลของคุณ';
$_['text_email']      = 'ป้อน อีเมลล์ของคุณ ที่ใช้สมัคร และ กดปุ่มถัดไป เพื่อรับรหัสผ่านทางอีเมล (กรุณาตรวจสอบกล่องอีเมลขยะ)';
$_['text_success']    = 'สำเร็จ:  รหัสผ่านใหม่ได้ถูกส่งให้คุณทางอีเมลล์เรียบร้อยแล้ว';

// Entry
$_['entry_email']     = 'E-Mail ของคุณ:';

// Error
$_['error_email']     = 'ผิดพลาด: ไม่พบอีเมลล์ในฐานข้อมูล, กรุณาลองใหม่อีกครั้ง!';
?>