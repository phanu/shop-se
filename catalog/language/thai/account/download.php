<?php
// Heading 
$_['heading_title']   = 'บัญชี ดาวน์โหลด';

// Text
$_['text_account']    = 'บัญชี';
$_['text_downloads']  = 'ดาวน์โหลด';
$_['text_order']      = 'รหััส:';
$_['text_date_added'] = 'วันที่สั่ง:';
$_['text_name']       = 'ชื่อ:';
$_['text_remaining']  = 'คงเหลือ:';
$_['text_size']       = 'ขนาด:';
$_['text_download']   = 'ดาวน์โหลด';
$_['text_empty']      = 'คุณไม่มีคำสั่งซื้อรายการดาวน์โหลด!';
?>