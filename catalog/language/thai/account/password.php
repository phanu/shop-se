<?php
// Heading 
$_['heading_title']  = 'เปลี่ยนรหัสผ่าน';

// Text
$_['text_account']   = 'บัญชีลูกค้า';
$_['text_password']  = 'รหัสผ่านของคุณ';
$_['text_success']   = 'สำเร็จ: รหัสผ่านของคุณได้ถูกแก้ไขเรียบร้อยแล้ว';

// Entry
$_['entry_password'] = 'รหัสผ่าน:';
$_['entry_confirm']  = 'ยืนยันรหัสผ่าน:';

// Error
$_['error_password'] = 'รหัสผ่านจะต้องอยู่ระหว่าง 3 - 20 ตัวอักษร !';
$_['error_confirm']  = 'ยืนยันรหัสผ่านไม่ตรงกัน!';
?>