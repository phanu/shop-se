<?php
// Heading 
$_['heading_title']                = 'บัญชีลูกค้า';

// Text
$_['text_account']                 = 'บัญชี';
$_['text_login']                   = 'เข้าสู่ระบบสมาชิก';
$_['text_new_customer']       = 'ลูกค้าใหม่';
$_['text_register']                = 'สมัครสมาชิก';
$_['text_register_account']        = 'ในกรณีที่ท่านสมัครสมาชิก ท่านจะสามารถตรวจสอบการสั่งซื้อ เช็คประวัติการสั่งซื้อ รวมถึงมีการสะสมยอดซื้อเพื่อเลื่อนระดับ VIP';
$_['text_returning_customer']      = 'ลูกค้าเก่าเข้าระบบ';
$_['text_i_am_returning_customer'] = 'เราคือลูกค้าเดิมจะเข้าระบบ.';
$_['text_forgotten']      = 'ลืมรหัสผ่าน';

// Entry
$_['entry_email']          = 'อีเมล์:';
$_['entry_password']               = 'รหัสผ่าน:';

// Error
$_['error_login']                  = 'ผิดพลาด:  อีเมล์และรหัสผ่านไม่ถูกต้อง';
?>