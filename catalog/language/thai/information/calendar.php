<?php
// Part of Events Calendar by Fido-X (http://www.fido-x.net)
// Heading
$_['heading_title']      = 'Events for the Year';
$_['heading_products']   = 'Related Products';

// Text
$_['text_event']	     = 'Events for %s';
$_['text_error']	     = 'No Events Listed!';
$_['text_reviews']       = 'Based on %s reviews.'; 
?>
