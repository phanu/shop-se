<?php
// Heading
$_['heading_title']      = 'ติดต่อเรา';

// Text 
$_['text_location']  = 'ที่อยู่ของเรา';
$_['text_contact']   = 'แบบติดต่อ';
$_['text_address']       = 'ที่อยู่:';
$_['text_email']         = 'อีเมลล์:';
$_['text_telephone']     = 'โทรศัพท์:';
$_['text_fax']           = 'แฟกซ์:';
$_['text_message']       = '<p>คำถามของคุณได้ถูกส่งไปยังเจ้าของร้านเรียบร้อยแล้ว!</p>';

// Entry Fields
$_['entry_name']         = 'ชื่อ:';
$_['entry_email']        = 'อีเมล์:';
$_['entry_enquiry']      = 'คำถาม:';
$_['entry_captcha'] = 'กรุณากรอกรหัสยืนยันในกล่องข้างล่าง:';

// Email
$_['email_subject']      = 'คำถาม %s';

// Errors
$_['error_name']         = 'ชื่อของคุณจะต้องอยู่ระหว่าง 4-20 ตัวอักษร!';
$_['error_email']        = 'อีเมลล์ไม่ถูกต้อง!';
$_['error_enquiry']      = 'คำถามจะต้องอยู่ระหว่าง 11-1000 ตัวอักษร!';
$_['error_captcha'] = 'กรุณาป้อนรหัสยืนยันให้ถูกต้อง!';
?>