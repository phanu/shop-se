<?php
// Heading 
$_['heading_title']                  = 'สั่งซื้อสินค้า';

// Text
$_['text_none']            			 = ' --- None --- ';
$_['text_cart']                      = 'ตะกร้าสินค้า';
$_['text_noaddress']                 = 'Please add your Primary address before you checkout.';
$_['text_checkout_option']           = 'เป็นสมาชิกอยู่แล้ว ?';
$_['text_checkout_account']          = 'รายละเอียดผู้สั่งซื้อ';
$_['text_checkout_payment_address']  = 'รายละเอียดผู้สั่งซื้อ';
$_['text_checkout_shipping_address'] = 'ชื่อ-ที่อยู่ สำหรับจัดส่งสินค้า';
$_['text_checkout_shipping_method']  = 'วิธีการจัดส่ง';
$_['text_checkout_payment_method']   = 'วิธีการชำระเงิน';
$_['text_checkout_confirm']          = 'รายการสินค้า [<a href="http://shop.se-update.com/index.php?route=checkout/cart">แก้ไขรายการสินค้า</a>]';
$_['text_checkout_comment']          = 'Comments';
$_['button_confirmorder']            = 'ถัดไป';
$_['text_modify']                    = 'แก้ไข &raquo;';
$_['text_already_reg']		         = '<h3>***หากเป็นสมาชิกอยู่แล้ว Click ที่นี่เพื่อทำการเข้าสู่ระบบ</h3>';
$_['text_reg']                       = '<b>โปรดใช้ข้อมูลด้านบน สมัครสมาชิกให้ฉัน</b>';
$_['text_new_customer']              = 'New Customer';
$_['text_returning_customer']        = 'Returning Customer';
$_['text_i_am_returning_customer']   = 'ฉันเป็นลูกค้าเก่า';
$_['text_register']                  = 'สมัครสมาชิก';
$_['text_guest']                     = 'Guest Checkout';
$_['text_register_account']          = 'By creating an account you will be able to shop faster, be up to date on an order\'s status, and keep track of the orders you have previously made.';
$_['text_forgotten']                 = 'ลืมรหัสผ่าน';
$_['text_logged']                    = 'คุณเข้าใช้งานในชื่อ <a href="%s">%s</a> <b>(</b> <a href="%s">Logout</a> <b>)</b>';
$_['text_items']                     = '%s item(s) - %s';
$_['text_your_details']              = 'Your Personal Details';
$_['text_your_address']              = 'Your Address';
$_['text_your_password']             = 'Your Password';
$_['text_agree']                     = 'ฉันได้อ่านข้อตกลงเกี่ยวกับ <a href="%s" alt="%s" target="_blank"><b>%s</b></a> เป็นที่เรียบร้อยแล้ว';
$_['text_address_new']               = 'ฉันต้องการใช้ที่อยู่ใหม่';
$_['text_address_existing']          = 'ฉันต้องการใช้ที่อยู่เดิมในระบบ';
$_['text_shipping_method']           = 'กรุณาเลือกวิธีการจัดส่งสินค้าสำหรับคำสั่งซื้อนี้';
$_['text_not_shipping_method']       = 'Please fill in the address first,then shipping method will auto show up.';
$_['text_payment_method']            = 'กรุณาเลือกวิธีการจ่ายเงินสำหรับคำสั่งซื้อนี้';
$_['text_payment_method1']            = '2. โอนเงินผ่าน ATM / โอนผ่าน Internet Banking / เคาน์เตอร์ธนาคาร';
$_['text_payment_method2']            = '3. ชำระผ่านบัตรเครดิต, เคาน์เตอร์เซอร์วิส, 7-11,  Tesco Lotus,mPay';
$_['text_payment_method3']            = '1. รับสินค้าที่หน้าร้าน';
$_['text_comments']                  = 'เพิ่ม Comment เกี่ยวกับคำสั่งซื้อของคุณ';
$_['text_survey']                  	 =  'ท่านใช้มือถือแบรนด์ไหน นอกจาก Sony ';
$_['survey_heading_title']			 = 'แบบสำรวจ';

//button
$_['button_confirm']    		     = 'ยืนยันคำสั่งซื้อ';

//insurance fee
$_['text_insurance_fee'] = 'Insurance Fee';
$_['entry_insurance_fee'] = 'Insurance Fee';

//payment fee
$_['text_onecheckout_payment_fee'] = 'ค่าธรรมเนียม';

// Column
$_['column_name']                    = 'ชื่อสินค้า';
$_['column_model']                   = 'รุ่น';
$_['column_quantity']                = 'จำนวน';
$_['column_price']                   = 'ราคา';
$_['column_total']                   = 'รวม';
$_['column_remove']   = 'Remove';

//cart module
$_['heading_title_coupon']			 = 'ใช้รหัสส่วนลด';
$_['text_coupon']   				 = 'Coupon(%s)';
$_['text_success_coupon'] 			 = 'สำเร็จ: รหัสส่วนลดได้ถูกใช้งานเรียบร้อย';
$_['entry_coupon'] 					 = 'ใส่รหัสคูปองส่วนลดตรงนี้:';
$_['error_coupon'] 					 = 'คำเตือน: รหัสส่วนลดไม่ถูกต้อง หมดอายุ หรือจำกัดจำนวนการใช้งาน';
$_['heading_title_voucher']			 = 'Use Gift Voucher';
$_['text_voucher'] 					 = 'Voucher(%s)';
$_['text_success_voucher'] 			 = 'Success: Your gift voucher discount has been applied!';
$_['entry_voucher']					 = 'Enter your gift voucher code here:';
$_['error_voucher']					 = 'Warning: Gift Voucher is either invalid or the balance has been used up!';
$_['heading_title_reward']			 = 'Use Reward Points (Available %s)';
$_['text_reward']  					 = 'Reward Points(%s)';
$_['text_order_id_reward']			 = 'Order ID: #%s';
$_['text_success_reward'] 			 = 'Success: Your reward points discount has been applied!';
$_['entry_reward'] 					 = 'Points to use (Max %s):';
$_['error_empty_reward'] 			 = 'Warning: Please enter the amount of points you wish to use!';
$_['error_points_reward'] 			 = 'Warning: You don\'t have %s reward points!';
$_['error_maximum_reward']			 = 'Warning: The maximum number of points that can be applied is %s!';
$_['heading_title_shipping']     = 'Estimate Shipping &amp; Taxes';
$_['text_shipping']     = 'Enter your destination to get a shipping estimate.';
$_['text_success_shipping']      = 'Success: Your shipping estimate has been applied!';
$_['entry_country_shipping']     = 'Country:';
$_['entry_zone_shipping']        = 'Region / State:';
$_['entry_postcode_shipping']    = 'Post Code:';
$_['error_postcode_shipping']    = 'Postcode must be between 2 and 10 characters!';
$_['error_country_shipping']     = 'Please select a country!';
$_['error_zone_shipping']        = 'Please select a region / state!';
$_['error_shipping_shipping']    = 'Warning: Shipping method required!';
$_['error_no_shipping_shipping'] = 'Warning: No Shipping options are available. Please <a href="%s">contact us</a> for assistance!';

// Entry
$_['entry_email_address']            = 'อีเมล:';
$_['entry_email']                    = 'อีเมล:';
$_['entry_password']                 = 'รหัสผ่าน:';
$_['entry_confirm']                  = 'กรอกรหัสผ่านอีกครั้ง:';
$_['entry_firstname']                = 'ชื่อ:';
$_['entry_lastname']                 = 'นามสกุล:';
$_['entry_telephone']                = 'โทรศัพท์:  ตัวอย่าง 081-123-4567 (ใส่แค่หมายเลขเดียวเท่านั้น) ';
$_['entry_fax']                      = 'หมายเลขโทรศัพท์สำรอง:';
$_['entry_account']                  = 'Account:';
$_['entry_company']                  = 'หน่วยงาน/บริษัท:';
$_['entry_company_id']               = 'Company ID:';
$_['entry_tax_id']                   = 'Tax ID:';
$_['entry_address_1']                = 'เลขที่บ้าน แล้วตามด้วย หมู่ ซอย หรือถนน: ';
$_['entry_address_2']                = 'ตำบล/แขวง:';
$_['entry_postcode']                 = 'รหัสไปรษณีย์:';
$_['entry_city']                     = 'อำเภอ/เขต:';
$_['entry_country']                  = 'ประเทศ:';
$_['entry_zone']                     = 'จังหวัด';
$_['entry_newsletter']               = '<b>ฉันต้องการรับจดหมายข่าวจาก %s</b>';
$_['entry_shipping'] 	             = '<b>ที่อยู่ด้านบนคือที่อยู่เดียวกันกับที่อยู่ในการจัดส่งสินค้า</b>';

// Error
$_['error_warning']                  = 'There was a problem while trying to process your order! If the problem persists please try selecting a different payment method or you can contact the store owner by <a href="%s">clicking here</a>.';
$_['error_login']                    = 'Warning: No match for E-Mail Address and/or Password.';
$_['error_exists']                   = 'คำเตือน : E-Mail นี้ ได้ถูกใช้สมัครสมาชิกไปแล้ว!';
$_['error_firstname']                = 'ชื่อ ต้องมี 1 ถึง 32 ตัวอักษร !';
$_['error_lastname']                 = 'นามสกุล ต้องมี 1 ถึง 32 ตัวอักษร !';
$_['error_email']                    = 'รูปแบบอีเมลไม่ถูกต้อง !';
$_['error_telephone']                = 'หมายเลขโทรศัพท์ ต้องมี 3 ถึง 32 ตัวอักษร !';
$_['error_password']                 = 'รหัสผ่าน ต้องมี 3 ถึง 20 ตัวอักษร !';
$_['error_confirm']                  = 'รหัสผ่านทั้งสองช่องไม่เหมือนกัน !';
$_['error_company_id']               = 'Company ID required!';
$_['error_tax_id']                   = 'Tax ID required!';
$_['error_vat']                      = 'VAT number is invalid!';
$_['error_address_1']                = 'ต้องมี 3 ถึง 128 ตัวอักษร !';
$_['error_address_2']                = 'ต้องมี 3 ถึง 128 ตัวอักษร !';
$_['error_city']                     = 'ต้องมี 2 ถึง 128 ตัวอักษร !';
$_['error_postcode']                 = 'ต้องมี 2 ถึง 10 ตัวอักษร ! ';
$_['error_country']                  = 'Please select a country!';
$_['error_zone']                     = 'กรุณาเลือกจังหวัด';
$_['error_agree']                    = 'คำเตือน: ท่านจะต้องทำการอ่าน [%s!]';
$_['error_address']                  = 'Warning: You must select address!';
$_['error_shipping']                 = 'Warning: Shipping method required!';
$_['error_no_shipping']              = 'No Shipping options are available. Please <a href="%s">contact us</a> for assistance!';
$_['error_payment']                  = 'Warning: Payment method required!';
$_['error_no_payment']               = 'Warning: No Payment options are available. Please <a href="%s">contact us</a> for assistance!';
?>