<?php
// Heading
$_['heading_title']          = 'Articles'; 

// Text  
$_['text_enabled']         = 'Enabled';
$_['text_disabled']         = 'Disabled';
$_['text_edit']         = 'Edit';
$_['text_view']         = 'View';
$_['button_filter']         = 'Filter';
$_['button_copy']         = 'Copy';
$_['button_insert']         = 'Insert';
$_['button_save']         = 'Save';
$_['button_cancel']         = 'Cancel';
$_['tab_general']         = 'General';
$_['tab_links']         = 'Links';
$_['text_select_all']         = 'Select All';
$_['text_unselect_all']         = 'Unselect All';
$_['text_no_results']         = 'No results!';
$_['text_article']         = 'Article';
$_['text_author']         	= 'Author';
$_['text_download']         = 'Download';
$_['text_comment']         = 'Comment';
$_['text_setting']         = 'Settings';
$_['text_image_upload']         = 'Image Upload Successfull';
$_['error_filename']         = 'Error Filename';

$_['text_publish']       = 'Publish';
$_['text_unpublish']       = 'Unpublish';
$_['text_approved']       = 'Yes';
$_['text_unapproved']       = 'No';
$_['text_account']       = 'Author';

$_['text_success']           = 'Success: You have modified products!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Default';
$_['text_image_manager']     = 'Image Manager';
$_['text_browse']            = 'Browse Files';
$_['text_clear']             = 'Clear Image';
$_['text_option']            = 'Option';
$_['text_option_value']      = 'Option Value';
$_['text_percent']           = 'Percentage';
$_['text_amount']            = 'Fixed Amount';
$_['tab_seo']            = 'SEO';

// Column
$_['column_name']            = 'Article Title';
$_['column_author']           = 'Author';
$_['column_image']           = 'Image';
$_['column_category']           = 'Category';
$_['column_status']          = 'Publish';
$_['column_approved']          = 'Approved';
$_['column_action']          = 'Action';
 
// Entry 
$_['entry_reference_url']             = 'Reference url:<br /><span class="help">Author can we send traffic to your website</span>';
$_['entry_reference_title']             = 'Reference Title:';
$_['entry_captcha']             = 'Enter the code in the box below:';
$_['entry_new_category']             = 'Suggest a new category:';
$_['entry_name']             = 'Article Title:';
$_['entry_description']      = 'Description:';
$_['entry_sub_description']  = 'Sub Description:';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords:';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_store']            = 'Stores:';
$_['entry_keyword']          = 'SEO Keyword:<br /><span class="help">Do not use spaces instead replace spaces with - and make sure the keyword is globally unique.</span>';
$_['entry_author']            = 'Author:';
$_['entry_author']     = 'Author:';
$_['entry_image']            = 'Image:';
$_['entry_status']           = 'Publish Status:';
$_['entry_sort_order']       = 'Sort Order:';
$_['entry_category']         = 'Categories:';
$_['entry_download']         = 'Downloads:';
$_['entry_related_article']          = 'Related Articles:';
$_['entry_related_product']          = 'Related Products:';
$_['entry_tag']          	 = 'Article Tags:<br /><span class="help">comma separated</span>';
$_['entry_reward']           = 'Reward Points:';
$_['entry_layout']           = 'Layout Override:';

// Error
$_['error_captcha']           = 'Verification code does not match the image';
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify article!';
$_['error_name']             = 'Article Name must be greater than 3 and less than 255 characters!';
$_['error_sub_description']             = 'Sub Description must be greater than 5 and less than 255 characters!';
$_['error_author']            = 'Please Select an Author!';
?>