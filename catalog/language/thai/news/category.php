<?php
// Text

$_['text_related'] = 'Related Post:';
$_['text_refine']       = 'Sub Categories';
$_['text_more']       = 'Details';
$_['text_article']      = 'Articles';
$_['text_error']        = 'Category not found!';
$_['text_empty']        = 'There are no articles to list in this category.';
$_['text_date_added']     = 'Added:';
$_['text_author'] = 'by author';
$_['text_comments']      = '%s comments'; 
$_['text_viewed']      = '%s viewed'; 
$_['text_display']      = 'Display:';
$_['text_list']         = 'List';
$_['text_grid']         = 'Grid';
$_['text_sort']         = 'Sort By:';
$_['text_default']      = 'Default';
$_['text_date_desc']    = 'Recently Added';
$_['text_name_asc']     = 'Name (A - Z)';
$_['text_name_desc']    = 'Name (Z - A)';
$_['text_sort_asc']     = 'Sort Order (ASC)';
$_['text_sort_desc']    = 'Sort Order (DESC)';
$_['text_viewed_asc']   = 'Viewed (Lowest)';
$_['text_viewed_desc']  = 'Viewed (Highest)';
$_['text_rating_asc']   = 'Rating (Lowest)';
$_['text_rating_desc']  = 'Rating (Highest)';
$_['text_author_asc']    = 'Author (A - Z)';
$_['text_author_desc']   = 'Author (Z - A)';
$_['text_limit']        = 'Show:';
?>