<?php
// Heading
$_['heading_title']			= 'Price List';

// Header Bar Title
$_['text_all_products']		= 'All Products';

// Text
$_['text_limit']			= 'Products per Page:';
$_['text_empty']			= 'Products Not Found!';
$_['text_category']			= 'Category:';
$_['text_print']			= 'Print';
$_['text_discount']			= '%s or more: %s';
$_['text_pdf']				= 'Save as PDF';
$_['text_qty']				= 'Qty:';
$_['text_select']			= '- Select -';
$_['text_or']				= '- OR -';
$_['text_error']			= 'Price List not found!';
$_['text_instock']			= 'In Stock';

//Errors
$_['error_customer_group']	= 'Your current customer account does not have the necessary privileges to view the price list.';
?>