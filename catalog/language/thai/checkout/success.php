<?php
// Heading
$_['heading_title'] = 'รายการสั่งของคุณอยู่ระหว่างการดำเนินการ!';

// Text
$_['text_customer'] = '<p><h2>กระบวนการสั่งซื้อของคุณสำเร็จเรียบร้อย!</H2></p>
</br>

<p>- ทางร้านได้รับคำสั่งซื้อของคุณและจะเริ่มกระบวนการจัดส่งสินค้าหลังจากได้รับการยืนยันการจ่ายค่าสินค้า
โดยท่านสามารถยืนยันการจ่ายค่าสินค้าได้ที่ <a href="http://goo.gl/NdwMAu" target="_blank">หรือคลิ๊กแจ้งการชำระเงินที่นี่</a></p>
<p>- หากท่านทำรายการได้ถึงหน้านี้ นั่นหมายถึงระบบได้ทำการล็อคสินค้าไว้ให้ท่านแล้ว(ระบบตัดสินค้าออกจากสต็อคอัตโนมัติ) ท่านจึงสามารถโอนเงินได้ตามที่ระบบแจ้ง</p>
<p>- ระบบจะทำการส่ง Email ยืนยันการสั่งซื้อไปหาท่านตาม Email ที่ท่านได้ให้ไว้ หากไม่พบกรุณาตรวจสอบใน Junk Box (กล่องอีเมลขยะ)</p>
</br>
<p><h3>กรณีที่ท่านเลือกชำระด้วยเงินสดและรับสินค้าที่หน้าร้าน</h3> </p>
<p>- กรณีที่ทำรายการก่อน 1 ทุ่ม ท่านสามารถรับสินค้าได้ที่หน้าร้านในวันรุ่งขึ้น (หลังเวลา 14.00 น.) หรือหากทำรายการหลังเวลา 1 ทุ่ม ท่านสามารถรับสินค้าได้ที่หน้าร้านในอีก 1 วันถัดไป (หลังเวลา 14.00 น.) หรือหากมีข้อสงสัยสามารถโทรติดต่อหน้าร้านได้ที่ 092-540-3300 ( ทุกวัน 10.30-20.00 น.)</p>
</br>
<p>คุณสามารถเข้าไปดูการสั่งซื้อของคุณที่<a href="%s">บัญชีใช้งาน</a> แล้วคลิ๊กไปที่ <a href="%s">ประวัติสั่งซื้อ</a>.</p>
<p>หากมีข้อสงสัย สามารถสอบถามได้ที่  admin@se-update.com หรือ  089-810-3755 ( จ-ศ. 10.00-20.00 น.  ส. 10.00-12.00 น. )</p>
<p>ขอขอบพระคุณในการสนับสนุนร้านค้าของเรา</p>

<!-- Google Code for Purchase Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1004740404;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "p9JsCOXTtlYQtL6M3wM";
var google_conversion_value = 1.00;
var google_conversion_currency = "THB";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1004740404/?value=1.00&amp;currency_code=THB&amp;label=p9JsCOXTtlYQtL6M3wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>';

$_['text_guest']  = '<p><h2>กระบวนการสั่งซื้อของคุณสำเร็จเรียบร้อย!</H2></p>
</br>

<p>- ทางร้านได้รับคำสั่งซื้อของคุณและจะเริ่มกระบวนการจัดส่งสินค้าหลังจากได้รับการยืนยันการจ่ายค่าสินค้า
โดยท่านสามารถยืนยันการจ่ายค่าสินค้าได้ที่ <a href="http://goo.gl/NdwMAu" target="_blank">หรือคลิ๊กแจ้งการชำระเงินที่นี่</a></p>
<p>- หากท่านทำรายการได้ถึงหน้านี้ นั่นหมายถึงระบบได้ทำการล็อคสินค้าไว้ให้ท่านแล้ว(ระบบตัดสินค้าออกจากสต็อคอัตโนมัติ) ท่านจึงสามารถโอนเงินได้ตามที่ระบบแจ้ง</p>
<p>- ระบบจะทำการส่ง Email ยืนยันการสั่งซื้อไปหาท่านตาม Email ที่ท่านได้ให้ไว้ หากไม่พบกรุณาตรวจสอบใน Junk Box (กล่องอีเมลขยะ)</p>
</br>
<p><h3>กรณีที่ท่านเลือกชำระด้วยเงินสดและรับสินค้าที่หน้าร้าน</h3> </p>
<p>-  กรณีที่ทำรายการก่อน 1 ทุ่ม ท่านสามารถรับสินค้าได้ที่หน้าร้านในวันรุ่งขึ้น (หลังเวลา 14.00 น.) หรือหากทำรายการหลังเวลา 1 ทุ่ม ท่านสามารถรับสินค้าได้ที่หน้าร้านในอีก 1 วันถัดไป (หลังเวลา 14.00 น.) หรือหากมีข้อสงสัยสามารถโทรติดต่อหน้าร้านได้ที่ 092-540-3300 ( ทุกวัน 10.30-20.00 น.)</p>
</br>

<p>หากมีข้อสงสัย สามารถสอบถามได้ที่  admin@se-update.com หรือ  089-810-3755 ( จ-ศ. 10.00-20.00 น.  ส. 10.00-12.00 น. )</p>
<p>ขอขอบพระคุณในการสนับสนุนร้านค้าของเรา</p>

<!-- Google Code for Purchase Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1004740404;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "p9JsCOXTtlYQtL6M3wM";
var google_conversion_value = 1.00;
var google_conversion_currency = "THB";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1004740404/?value=1.00&amp;currency_code=THB&amp;label=p9JsCOXTtlYQtL6M3wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>';

$_['text_basket']   = 'ตะกร้า';
$_['text_checkout'] = 'ชำระเงิน';
$_['text_success']  = 'สำเร็จ';
?>