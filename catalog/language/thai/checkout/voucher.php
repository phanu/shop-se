<?php
// Heading 
$_['heading_title']    = 'ซื้อบัตรของขวัญ';

// Text
$_['text_voucher']     = 'บัตรของขวัญ';
$_['text_description'] = 'บัตรของขวัญนี้ จะส่งไปยังผู้รับหลังจากคุณได้จ่ายแล้ว';
$_['text_agree']       = 'ฉันเข้าใจในข้อตกลงในการใช้บัตรของขวัญ';
$_['text_message']     = '<p>ขอบคุณในการซื้อบัตรของขวัญ! บัตรนี้จะสมบูรณ์เมื่อผุ้รับบัตร นำบัตรมาเป็นส่วนลดหรือซื้อสินค้าในระบบ</p>';
$_['text_for']         = '%s บัตรของขวัญสำหรับ %s';

// Entry
$_['entry_to_name']    = 'ชื่อผู้รับ:';
$_['entry_to_email']   = 'อีเมล์ผู้รับ:';
$_['entry_from_name']  = 'ชื่อของคุณ:';
$_['entry_from_email'] = 'อีเมล์ของคุณ:';
$_['entry_message']    = 'ข้อความ:<br /><span class="help">(เพิ่มเติม)</span>';
$_['entry_amount']     = 'มูลค่า:<br /><span class="help">(มูลคาระหว่าง 1.00 บาท ถึง 1,000.00 บาท)</span>';
$_['entry_theme']      = 'ธีมของ บัตรของขวัญ:';

// Error
$_['error_to_name']    = 'ชื่อผู้รับ ต้องอยู่ระหว่าง 1 ถึง 64 ตัวอักษร!';
$_['error_from_name']  = 'ชื่อของคุณ ต้องอยู่ระหว่าง 1 ถึง 64 ตัวอักษร!';
$_['error_email']      = 'รูปแบบอีเมล์ไม่ถูกต้อง!';
$_['error_amount']     = 'มูลค่าต้องอยู่ %s ถึง %s!';
$_['error_theme']      = 'คุณต้องเลือกธีม!';
$_['error_agree']      = 'ผิดพลาด: คุณต้องติ๊กตรงเข้าใจในข้อตกลง!';
?>