﻿<?php
// Heading 
$_['heading_title']                  = 'สั่งซื้อสินค้า';

// Text
$_['text_cart']                    = 'ตะกร้า';
$_['text_checkout_option']           = 'ขั้นที่ 1: ออปชั่นการชำระเงิน';
$_['text_checkout_account']          = 'ขั้นที่ 2: รายละเอียดผู้สั่งซื้อ';
$_['text_checkout_payment_address']  = 'ขั้นที่ 2: รายละเอียดผู้สั่งซื้อ';
$_['text_checkout_shipping_address'] = 'ขั้นที่ 3: รายละเอียดสำหรับจัดส่งสินค้า';
$_['text_checkout_shipping_method']  = 'ขั้นที่ 4: วิธีการจัดส่ง';
$_['text_checkout_payment_method']   = 'ขั้นที่ 5: วิธีการชำระเงิน';
$_['text_checkout_confirm']          = 'ขั้นที่ 6: ยืนยันคำสั่งซื้อ';
$_['text_modify']                    = 'แก้ไข &raquo;';
$_['text_new_customer']              = 'ลูกค้าใหม่';
$_['text_returning_customer']        = 'ลูกค้าเดิมที่เคยสมัครสมาชิกแล้ว';
$_['text_checkout']                  = 'ออปชั่นการสมัครสมาชิก:';
$_['text_i_am_returning_customer']   = 'ฉันคือลูกค้าเดิม';
$_['text_register']                  = 'สมัครสมาชิก';
$_['text_guest']                     = 'ไม่สมัครสมาชิก';
$_['text_register_account']          = ' ในกรณีที่ท่านสมัครสมาชิก ท่านจะสามารถตรวจสอบการสั่งซื้อ เช็คประวัติการสั่งซื้อ รวมถึงจะได้รับคะแนนสะสมเมื่อซื้อสินค้าแต่ละชิ้น';
$_['text_forgotten']                 = 'ลืมรหัสผ่าน';
$_['text_your_details']              = 'รายละเอียดส่วนบุคคล';
$_['text_your_address']              = 'ที่อยู่ของคุณ';
$_['text_your_password']             = 'รหัสผ่าน';
$_['text_agree']                     = 'กรุณาอ่าน <a class="thickbox" target="_blank" href="%s" alt="%s"><b>%s</b></a> แล้วติ๊กเพื่อรับทราบ ก่อน กดปุ่ม "ถัดไป" >> ';
$_['text_address_new']               = 'ต้องการใช้ที่อยู่ใหม่';
$_['text_address_existing']          = 'ต้องการใช้ที่อยู่ที่มีในข้อมูล';
$_['text_shipping_method']           = 'กรุณาเลือกวิธีการจัดส่งสำหรับคำสั่งซื้อนี้';
$_['text_payment_method']            = 'กรุณาเลือกวิธีการจ่ายเงินสำหรับคำสั่งซื้อนี้';
$_['text_payment_method1']            = 'โอนเงินผ่าน ATM หรือโอนผ่าน Internet Banking';
$_['text_payment_method2']            = 'ชำระผ่านบัตรเครดิต / ชำระผ่าน Counter service / 7-11 / Tesco Lotus (ค่าธรรมเนียม 4%)';
$_['text_comments']                  = 'เพิ่มคอมเม้นท์เกี่ยวกับคำสั่งซื้อของคุณ';

// Column
$_['column_name']                    = 'ชื่อสินค้า';
$_['column_model']                   = 'รุ่น';
$_['column_quantity']                = 'ปริมาณ';
$_['column_price']                   = 'ราคา';
$_['column_total']                   = 'รวม';

// Entry
$_['entry_email_address']            = 'อีเมล์:';
$_['entry_email']                    = 'อีเมล์:';
$_['entry_password']                 = 'รหัสผ่าน:';
$_['entry_confirm']                  = 'รหัสผ่าน:';
$_['entry_firstname']                = 'ชื่อ:';
$_['entry_lastname']                 = 'นามสกุล:';
$_['entry_telephone']                = 'โทรศัพท์:';
$_['entry_fax']                      = 'แฟ็กซ์:';
$_['entry_company']                  = 'บริษัท:';
$_['entry_address_1']                = 'เลขที่ และ ถนน/ซอย/หมู่:';
$_['entry_floor_number']                = 'เลขที่:';
$_['entry_address_2']                = 'ตำบล/แขวง:';
$_['entry_postcode']                 = 'รหัสไปรษณีย์:';
$_['entry_city']                     = 'อำเภอ/เขต:';
$_['entry_country']                  = 'ประเทศ:';
$_['entry_zone']                     = 'จังหวัด:';
$_['entry_newsletter']               = 'ฉันต้องการรับจดหมายข่าวของ %s ';
$_['entry_shipping'] 	             = 'ที่อยู่จัดส่งและที่อยู่ออกบิลที่เดียวกัน';

// Error
$_['error_warning']                  = 'พบปัญหาเกิดขึ้น! กรุณาเลือกวิธีการชำระเงินอื่นหรือแจ้งเจ้าของร้านโดยact t <a href="%s">คลิ๊กตรงนี้</a>.';
$_['error_minimum']                  = 'คำสั่งซื้อต่ำสุดของ %s คือ %s!';	
$_['error_login']                    = 'ผิดพลาด: ไม่พบอีเมล์หรือรหัสผ่านผิด';
$_['error_exists']                   = 'ผิดพลาด: อีเมล์นี้มีคนใช้แล้ว!';
$_['error_firstname']                = 'ชื่อ ต้องมี 1 ถึง 32 ตัวอักษร!';
$_['error_lastname']                 = 'นามสกุล ต้องมี 1 ถึง 32 ตัวอักษร!';
$_['error_email']                    = 'รูปแบบอีเมล์ไมถูกต้อง!';
$_['error_telephone']                = 'โทรศัพท์ ต้องมี 3 ถึง 32 ตัวอักษร!';
$_['error_password']                 = 'รหัสผ่าน ต้องมี 3 ถึง 20 ตัวอักษร!';
$_['error_confirm']                  = 'รหัสผ่าน 2 ครั้งไม่เหมือนกัน!';
$_['error_address_1']                = ' ต้องมี 3 ถึง 128 ตัวอักษร!';
$_['error_address_2']                = ' ต้องมี 3 ถึง 128 ตัวอักษร!';
$_['error_floor_number']                = ' ต้องมี 1 ถึง 128 ตัวอักษร!';
$_['error_city']                     = ' ต้องมี 3 ถึง 128 ตัวอักษร!';
$_['error_postcode']                 = 'รหัสไปรษณีย์ ต้องมี 2 ถึง 10 ตัวอักษร!';
$_['error_country']                  = 'กรุณาเลือกประเทศ!';
$_['error_zone']                     = 'กรุณาเลือกจังหวัด!';
$_['error_agree']                    = 'ผิดพลาด: คุณต้องติ๊กว่าอ่านข้อตกลงแล้ว %s!';
$_['error_address']                  = 'ผิดพลาด: คุณต้องเลือกที่อยู่!';
$_['error_shipping']                 = 'ผิดพลาด: ต้องเลือกวิธีการจัดส่ง!';
$_['error_no_shipping']              = 'ไม่มีตัวเลือกการจัดส่ง กรุณาแจ้ง <a href="%s">ติดต่อร้าน</a> !';
$_['error_payment']                  = 'ผิดพลาด: เลือกวิธิการจ่ายเงิน!';
$_['error_no_payment']               = 'ผิดพลาด: ไม่มีตัวเลือกการชำระเงิน กรุณาแจ้ง <a href="%s">ติดต่อร้าน</a> !';
?>