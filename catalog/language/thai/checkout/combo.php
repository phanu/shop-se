<?php
//OpenCart Extension
//Project Name: OpenCart Combo/Bundle
//Author: Fanha Giang a.k.a fanha99
//Email (PayPal Account): fanha99@gmail.com
//License: Commercial
?>
<?php
// Text
$_['text_items']             = '%s item(s) - %s';

$_['text_success'] = 'สำเร็จ: คุณได้หยิบสินค้าจำนวน %d ชิ้น เข้าสู่ <a href="%s">ตะกร้าของคุณ</a>!';
$_['text_error'] = 'Warning: Combo Set ที่คุณเลือก จำเป็นต้องมีการเลือก Option สินค้า!';

$_['error_required']         = 'ท่านไม่ได้เลือก  %s';
$_['error_profile_required'] = 'Please select a payment profile!';
?>