<?php
// Text
$_['text_home']             = 'หน้าหลัก';
$_['text_level']            = 'Level:';
$_['text_game_finished']    = 'Games Finished:';
$_['text_moves']            = 'Moves:';

// bar buttons
$_['text_instruction']      = 'อ่านคำแนะนำ';
$_['text_rank']             = 'ดูตารางอันดับคะแนน';

$_['text_login_required']   = 'อนุญาติให้ใช้ข้อมูลจาก facebook เพื่อใช้ในการเพิ่ม SE-Update Credit ลงในระบบของ SE-Update Shop';
$_['button_ok_agree']       = 'ตกลง';

// dialog help
$_['text_dtitle_help']      = 'คำแนะนำ';

$_['text_help_instructions'] = 'ทำการกด Like ที่ปุ่มขวามือด้านบน <br/> 1 .คลิ๊กที่ภาพเพื่อจดจำรูปภาพ<br />2. คลิ๊กภาพถัดไปให้เหมือนกับภาพที่เปิดไว้<br /> 3. หากภาพที่เลือกเหมือนกับภาพแรกที่เปิดไว้ ภาพดังกล่าจะถูกลบออกจากกระดาน<br />4. คุณจะจบเกมส์ได้ก็ต่อเมื่อภาพถูกลบออกหมดกระดาน (พยายามเปิดภาพให้น้อยที่สุดเพื่อลดค่า Move ที่จะส่งผลต่อคะแนนรวม) </br>***การพยายามคลิ๊กภาพเร็วเกินไป จะทำให้ระบบหยุดเกมส์โดยอัตโนมัติ </br>';
$_['text_win_type']          = 'คุณสามารถลุ้น SE-Update Credit ได้ทุกๆ ';
$_['text_day']               = 'วัน';
$_['text_week']              = 'สัปดาห์';
$_['text_month']             = 'เดือน';

//dialog rank
$_['text_dtitle_rank']      = 'อันดับ';
$_['tab_today']             = 'วันนี้';
$_['tab_week']              = 'สัปดาห์นี้';
$_['tab_month']             = 'เดือนนี้';
$_['column_participant']    = 'ผู้เข้าร่วมแข่งขัน';
$_['column_points']         = 'คะแนน';

// dialog like
$_['text_unlock_game']      = 'กด Like Fanpage เพื่อเข้าร่วมเล่นเกมส์ ';
$_['text_play_win']         = 'ร่วมสนุกเพื่อลุ้นรางวัล SE-Update Credit';

$_['text_how_to_play']      = 'วิธีการเล่น';
$_['text_play_instruction'] = 'ทำการกด Like ที่ปุ่มขวามือด้านบน <br/>1 .คลิ๊กที่ภาพเพื่อจดจำรูปภาพ <br />2. คลิ๊กภาพถัดไปให้เหมือนกับภาพที่เปิดไว้<br /> 3. หากภาพที่เลือกเหมือนกับภาพแรกที่เปิดไว้ ภาพดังกล่าจะถูกลบออกจากกระดาน<br />4. คุณจะจบเกมส์ได้ก็ต่อเมื่อภาพถูกลบออกหมดกระดาน (พยายามเปิดภาพให้น้อยที่สุดเพื่อลดค่า Move ที่จะส่งผลต่อคะแนนรวม) </br>***การพยายามคลิ๊กภาพเร็วเกินไป จะทำให้ระบบหยุดเกมส์โดยอัตโนมัติ </br>';

$_['text_prizes']           = 'You can win ...';
$_['text_prize_details']    = 'พยายามเล่นเกมส์ให้ได้คะแนนมากกว่าผู้เข้าแข่งขันท่านอื่น.<br /><br /><strong>รายละเอียดรางวัล:</strong><br />';
$_['text_credits']          = 'บาท <br />';
$_['text_daily_winner']     = 'ผู้ชนะประจำวันจะได้รับ SE-Update Credit : ';
$_['text_weekly_winner']    = 'ผู้ชนะประจำสัปดาห์จะได้รับ SE-Update Credit : ';
$_['text_monthly_winner']   = 'ผู้ชนะประจำเดือนจะได้รับ SE-Update Credit : ';
$_['text_credit_transform'] = '<br />หมายเหตุ : สำหรับผู้ที่ได้รับรางวัล หาก Email Account ที่ท่านใช้สมัครกับทางร้าน SE-Update Shop และทาง Facebook ตรงกัน ระบบจะทำการเพิ่ม Credit ให้อัตโนมัติ แต่หากท่านใช้ Email คนละ Email หรือเป็น สมาชิกใหม่ ระบบจะทำการสร้าง Account ให้โดยอัตโนมัติ ';


//dialog player won
$_['text_dtitle_win']       = 'CONGRATULATIONS !!!';
$_['button_play_again']     = 'รอให้ข้อมูลคะแนนขึ้น แล้วค่อยกดนะ';
$_['text_player_won']       = 'Congratulations <strong>%s</strong> <br /><br />คะแนนจากเกมนี้: <strong>%s</strong><br />คะแนนรวมในวันนี้: <strong>%s</strong><br />คะแนนรวมในสัปดาห์นี้: <strong>%s</strong><br />คะแนนรวมในเดือนนี้: <strong>%s</strong>';

$_['text_feed_message']     = 'ร่วมเล่นเกมส์กับ SE-Update Shop เพื่อลุ้นรางวัล SE-Update Credit ที่นี่ %s.  คะแนนที่ได้รับวันนี้คือ %s. ถ้าคุณคิดว่าสามารถเล่นได้คะแนนมากกว่านี้ ลองดูมั้ย ?';
$_['text_feed_name']        = 'SE-Update Game';
$_['text_feed_caption']     = 'กิจกรรมร่วมสนุกจาก %s';
$_['text_feed_description'] = 'ร่วมกิจกรรมเพื่อค้นหาแชมป์ในทุกวัน/สัปดาห์/เดือน เพื่อรับรางวัล SE-Update Credit ใช้เป็นส่วนลดเงินสดซื้อสินค้าที่ร้าน SE-Update Shop อย่ารอช้า ไปร่วมสนุกกัน !!!! '; 

?> 