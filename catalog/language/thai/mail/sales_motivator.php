<?php
// Text
$_['text_subject']        = '%s - Thank you for registering';
$_['text_welcome']        = 'Welcome and thank you for registering at %s!';
$_['text_login']          = 'Your account has now been created and you can log in by using your email address and password by visiting our website or at the following URL: ';
$_['text_login_info']     = 'Login info:';
$_['text_login_email']    = 'E-Mail: %s';
$_['text_login_password'] = 'Password: %s';
$_['text_services']       = 'Upon logging in, you will be able to access other services including reviewing past orders, printing invoices and editing your account information.';
$_['text_thanks']         = 'Thanks,';
$_['text_new_customer']   = 'New customer';
$_['text_signup']         = 'A new customer has signed up via Sales Motivator:';
$_['text_website']        = 'Web Site:';
$_['text_customer_group'] = 'Customer Group:';
$_['text_firstname']      = 'First Name:';
$_['text_lastname']       = 'Last Name:';
$_['text_company']        = 'Company:';
$_['text_email']          = 'E-Mail:';
$_['text_telephone']      = 'Telephone:';

// store credit mail
$_['text_transaction_subject']  = '%s - Account Credit';
$_['text_transaction_received'] = 'You have received %s credit!';
$_['text_transaction_total']    = 'Your total amount of credit is now %s.' . "\n\n" . 'Your account credit will be automatically deducted from your next purchase.';
$_['text_friends_helped']       = 'Received this reward with help from this friends (Name - #Order ID): %s';


// Admin Log
$_['text_log_subject']    = 'Sales Motivator - Report for Admin';
$_['text_hi_admin']       = 'Hi Admin,';
$_['text_below_have']     = 'Below you can find details about customers rewarded (by Sales Motivator Extension) for orders added by invited friends.';
$_['text_nothing_reward'] = 'At this moment no customer need to be rewarded.';
$_['text_disable_log']    = 'This report is sent by Sales Motivator Extension. If you don\'t want to receive this logs from Admin > Extensions -> Modules -> Sales Motivator -> in tab Additional Setting set Option \'Send log to admin\' to No';
?>