<?php
// Text
$_['text_subject']              = '[%s] - You are Facebook Deal Winner';
$_['text_hi']                   = 'Hi %s,';
$_['text_deal_info']            = 'You are the winner of Facebook Deal contest.';
$_['text_prize_details']        = 'Prize details: ';
$_['text_discount']             = ' credits for ';
$_['text_how_to_use']           = 'Credits are transformed in store currency and added to a coupon (see coupon details below). Login into your account, add %s in your cart. Goto Shopping cart page and apply coupon code.';
$_['text_coupon_code']          = 'Coupon code: %s';
$_['text_thanks']               = 'Thanks,';

// Admin notification
$_['text_admin_subject']        = 'You need to approve new winner';
$_['text_admin_hi']             = 'Hi,';
$_['text_admin_winner_info']    = 'Winner Info:';
$_['text_admin_deal_id']        = 'Deal ID: ';
$_['text_admin_prize']          = 'Prize: ';
$_['text_how_to_approve']       = 'To approve winner goto Admin-> Exntesion -> Module -> Facebook Deal Unlocker -> select tab \'Winner List\' and click approve button on winner row.';

?>