<?php
// Text
$_['text_subject']    = '%s - ขอขอบคุณที่ร่วมสนุกกับกิจกรรม SE-Update Game';
$_['text_welcome']    = 'ยินดีต้อนรับและขอขอบคุณที่สนใจร่วมกิจกรรม SE-Update Game ค่ะ';
$_['text_login']      = 'เนื่องจากอีเมลที่ท่านใช้ในการเล่นเกมส์ ไม่ถูกพบในระบบทางทางร้าน SE-Update Shop ระบบจึงได้ทำการสร้าง Account ใหม่ให้ท่าน เพื่อใช้ในการนำ Credit';
$_['text_approval']   = 'Your account must be approved before you can login. Once approved you can log in by using your email address and password by visiting our website or at the following URL:';
$_['text_login_url']  = 'Login URL: %s'; 
$_['text_login_data'] = 'คุณสามารถ Login โดยใช้อีเมล email: %s และ password: %s'; 


$_['text_transaction_received'] = 'คุณได้รับ %s credit!';
$_['text_transaction_total']    = 'ยอดรวม credit ของคุณขณะนี้คือ %s.' . "\n\n" . 'ยอดเครดิตของคุณจะถูกนำไปใช้โดยอัตโนมัติในการสั่งซื้อสินค้าครั้งต่อไป';

$_['text_thanks']     = 'Thanks,';
?>