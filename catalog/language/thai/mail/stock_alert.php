<?php
// Text
$_['text_low_stock_alert']  = 'Low Stock Alert';
$_['text_email_subject']  	= 'Low Stock Alert';
$_['text_auto_msg']       	= 'This is auto generated message from system, please do not reply. Thank you.';

// Column
$_['column_product']        = 'Product';
$_['column_model']          = 'Model';
$_['column_quantity']       = 'Quantity';
?>