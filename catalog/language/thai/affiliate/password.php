<?php
// Heading 
$_['heading_title']  = 'เปลี่ยนรหัสผ่าน';

// Text
$_['text_account']   = 'บัญชี';
$_['text_password']  = 'รหัสผ่านของคุณ';
$_['text_success']   = 'สำเร็จ: แก้ไขรหัสผ่านของคุณแล้ว';

// Entry
$_['entry_password'] = 'รหัสผ่าน:';
$_['entry_confirm']  = 'รหัสผ่าน อีกครั้ง:';

// Error
$_['error_password'] = 'รหัสผ่านต้องมี 3 ถึง 20 ตัวอักษร!';
$_['error_confirm']  = 'รหัสผ่าน 2 ครั้ง ไม่เหมือนกัน!';
?>