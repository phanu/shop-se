<?php
// Heading 
$_['heading_title']        = 'บัญชี พันธมิตรการค้า';

// Text
$_['text_account']         = 'บัญชี';
$_['text_my_account']      = 'บัญชี พันธมิตรการค้า';
$_['text_my_tracking']     = 'ข้อมูลการติดตาม';
$_['text_my_transactions'] = 'การทำธุรกรรม';
$_['text_edit']            = 'แก้ไขข้อมูล';
$_['text_password']        = 'เปลี่ยนรหัสผ่าน';
$_['text_payment']         = 'ตั้งค่าการจ่ายเงิน';
$_['text_tracking']        = 'รหัสการติดตามของพันธมิตรการค้า';
$_['text_transaction']     = 'ดูประวัติธุรกรรม';
?>