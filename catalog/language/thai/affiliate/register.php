<?php
// Heading 
$_['heading_title']             = 'ส่วนพันธมิตร';

// Text
$_['text_account']              = 'บัญชี';
$_['text_register']             = 'ลงทะเบียนพันธมิตรการค้า';
$_['text_account_already']      = 'หากเป็นสมาชิกอยู่แล้วกรุณาไปที่ <a href="%s">เข้าสู่ระบบพันธมิตรการค้า</a>.';
$_['text_signup']               = 'ในการสร้างบัญชพันธมิตรกาค้า, กรุณาเติมข้อมูลข้างล่างให้สมบูรณ์:';
$_['text_your_details']         = 'รายละเอียดส่วนบุคคล';
$_['text_your_address']         = 'รายละเอียดที่อยู่';
$_['text_payment']              = 'ข้อมูลการจ่ายเงิน';
$_['text_your_password']        = 'รหัสผ่าน';
$_['text_cheque']               = 'เช็ค';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'โอนเงินผ่านธนาคาร';
$_['text_agree']                = 'ฉันได้อ่านข้อตกลง <a class="thickbox" href="%s" alt="%s"><b>%s</b></a>';

// Entry
$_['entry_firstname']           = 'ชื่อ:';
$_['entry_lastname']            = 'นามสกุล:';
$_['entry_email']               = 'อีเมล:';
$_['entry_telephone']           = 'โทรศัพท์:';
$_['entry_fax']                 = 'แฟ็กซ์:';
$_['entry_company']             = 'บริษัท:';
$_['entry_website']             = 'เว็บไซต์:';
$_['entry_address_1']           = 'ที่อยู่ 1:';
$_['entry_address_2']           = 'ที่อยู่ 2:';
$_['entry_postcode']            = 'รหัสไปรษณีย์:';
$_['entry_city']                = 'อำเภอ:';
$_['entry_country']             = 'ประเทศ:';
$_['entry_zone']                = 'จังหวัด:';
$_['entry_tax']                 = 'หมายเลขประจำตัวประชาชน:';
$_['entry_payment']             = 'วิธีจ่ายเงิน:';
$_['entry_cheque']              = 'ชื่อบัญชี:';
$_['entry_paypal']              = 'อีเมล์ PayPal :';
$_['entry_bank_name']           = 'ชื่อธนาคาร:';
$_['entry_bank_branch_number']  = 'สาขา:';
$_['entry_bank_swift_code']     = 'ประเภทบัญชี:';
$_['entry_bank_account_name']   = 'ชื่อบัญชี:';
$_['entry_bank_account_number'] = 'เลขบัญชี:';
$_['entry_password']            = 'รหัสผ่าน:';
$_['entry_confirm']             = 'รหัสผ่านอีกครั้ง:';

// Error
$_['error_exists']              = 'ผิดพลาด: อีเมล์นี้มีคนใช้แล้ว!';
$_['error_firstname']           = 'ชื่อ ต้องมี 1 ถึง 32 ตัวอักษร!';
$_['error_lastname']            = 'นามสกุล ต้องมี 1 ถึง 32 ตัวอักษร!';
$_['error_email']               = 'รูปแบบอีเมล ไม่ถูกต้อง!';
$_['error_telephone']           = 'Telephone ต้องมี 3 ถึง 32 ตัวอักษร!';
$_['error_password']            = 'รหัสผ่าน ต้องมี 3 ถึง 20 ตัวอักษร!';
$_['error_confirm']             = 'รหัสผ่าน 2 ครั้งไม่เหมือนกัน!';
$_['error_address_1']           = 'ที่อยู่ ต้องมี 3 ถึง 128 ตัวอักษร!';
$_['error_city']                = 'อำเภอ ต้องมี 3 ถึง 128 ตัวอักษร!';
$_['error_country']             = 'กรุณาเลือกประเภท!';
$_['error_zone']                = 'กรุณาเลือกจังหวัด!';
$_['error_postcode']            = 'รหัสไปรษณีย์  ต้องมี 2 ถึง 10 ตัวอักษร!';
$_['error_agree']               = 'ผิดพลาด: คุณต้องติ๊กว่าเข้าใจ ข้อตกลง %s!';

// Mail
$_['mail_subject']              = '%s - ส่วนพันธมิตรการค้า';
$_['mail_welcome']              = 'ขอบคุณในการเข้าร่วมเป็นพันธมิตรการค้ากับ %s ';
$_['mail_approval']             = 'การเป็นสมาชิกของคุณต้องรอเจ้าของร้าน อนุมัติก่อน ถึงจะสามารถใช้งานได้อย่างสมบูรณ์แบบ:';
$_['mail_services']             = 'ในการเข้าสู่ระบบ, คุณสามารถสร้าง ลิงค์ติดตาม ,การจ่ายค่าคอมมิสชั่น และแ้ไขข้อมูลของคุณ';
$_['mail_thanks']               = 'ขอบคุณ,';
?>