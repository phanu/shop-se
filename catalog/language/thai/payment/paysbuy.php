<?php
// Text
$_['text_title'] = 'เพย์สบาย  <br> <img src="http://se-update.com/shop/catalog/view/theme/cl_default/image/paysbuylogo.gif">';
$_['text_backup_title'] = 'Paysbuy ';

$_['heading_title00'] = 'ได้รับการชำระค่าสินค้าจาก Paysbuy';
$_['heading_title01'] = 'มีปัญหาเรื่องยอดการจ่ายจาก Paysbuy';
$_['heading_title99'] = 'เกิดข้อผิดพลาดจาก Paysbuy';

$_['text_message00'] = 'ขอบคุณสำหรับการชำระค่าสินค่าผ่านทาง Paysbuy';
$_['text_message01'] = 'มีปัญหาเรื่องยอดเงินจาก Paysbuy';
$_['text_message99'] = 'เกิดข้อผิดพลาดจาก Paysbuy';

$_['text_pay_code'] = 'รหัสของ Paysbuy : ';
$_['text_pay_amt'] = 'ยอดเงินที่จ่ายจาก Paysbuy : ';

$_['text_pay_method1'] = 'ชำระเงินด้วยบัญชีของ Paysbuy ';
$_['text_pay_method2'] = 'ชำระด้วยบัตรเครดิต';
$_['text_pay_method3'] = 'ชำระเงินด้วย Paypal';
$_['text_pay_method4'] = 'ชำระเงินด้วย American Express';
$_['text_pay_method5'] = 'ชำระเงินด้วย Online Direct Debit';
$_['text_pay_method6'] = 'ชำระเงินด้วย Counter Service';
$_['text_pay_method'] = 'วิธีการชำระจาก Paysbuy : ';

$_['text_payment']  = '<h2>ข้อแนะนำเมื่อเข้าสู่ระบบการชำระเงินของทาง Paysbuy</h2></ br><li>หลังจากท่านกดยืนยันคำสั่งซื้อแล้ว ระบบจะถูกส่งเข้าสู่ระบบการชำระเงินของทาง Paysbuy </ br> และเมื่อสิ้นสุดการกรอกข้อมูลของทาง Paysbuy แล้ว ให้ท่านกดปุ่ม "<b>สิ้นสุดการทำรายการ</b>" ทุกครั้ง เพื่อการสั่งซื้อที่สมบูรณ์ </li></ br> &nbsp;</br>
<img src="http://shop.se-update.com/image/data/pro/paysbuy.jpg">';




$_['text_pay_error'] = '<h3>การสั่งสินค้าจะเสร็จสมบูรณ์ กดปุ่ม "ถัดไป" แล้วค่อยทำการชำระค่าสินค้า!</h3>';
$_['text_pay_comp'] = '<h3>การสั่งสินค้าจะเสร็จสมบูรณ์ โปรดกดปุ่ม "ถัดไป" เพื่อการสั่งซื้อที่สมบูรณ์! </h3>';

?>