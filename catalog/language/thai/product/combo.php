<?php
//OpenCart Extension
//Project Name: OpenCart Combo/Bundle
//Author: Fanha Giang a.k.a fanha99
//Email (PayPal Account): fanha99@gmail.com
//License: Commercial
?>
<?php
// Heading
$_['heading_title']     = 'โปรโมชั่น Combo Set';
$_['text_empty']        = 'There are no special offer products to list.';
$_['text_tax']          = 'Ex Tax:'; 
$_['text_display']      = 'Display:';
$_['text_list']         = 'List';
$_['text_grid']         = 'Grid';
$_['text_limit']        = 'Show:';
$_['tab_combo']      = '<font color="red"> โปรโมชั่น Combo Set</font>';
$_['button_combo_cart']     = 'หยิบ Combo Set นี้ใส่ตะกร้า';
$_['this_item']     = 'สินค้าชิ้นนี้:';
$_['both'] = 'both';
$_['all_three'] = 'all three';
$_['all'] = 'all';
$_['Price_for_all'] = 'Price For All';
$_['this_item'] = 'สินค้าชิ้นนี้: ';
$_['save_percent'] = 'ช่วยคุณประหยัด %.0f %%';
$_['save_amount'] = 'ช่วยคุณประหยัด %s';
$_['combo_items'] = 'กรุณาเลือก Option ของสินค้า:';
?>