<?php
// Heading
$_['heading_title']     = 'หายี่ห้อโปรด';
// Text
$_['text_brand']        = 'ยี่ห้อ';
$_['text_index']        = 'ดรรชนี ยี่ห้อ:';
$_['text_error']        = 'ไม่พบยี่ห้อ!';
$_['text_empty']        = 'ไม่มียี่ห้อในรายการ.';
$_['text_quantity']     = 'ปริมาณ:';
$_['text_manufacturer'] = 'ยี่ห้อ:';
$_['text_model']        = 'รหัส สินค้า:'; 
$_['text_points']       = 'คะแนนสะสม:'; 
$_['text_price']        = 'ราคา:'; 
$_['text_tax']          = 'ภาษี:'; 
$_['text_reviews']      = 'ความคิดเห็น %s รายการ.'; 
$_['text_compare']      = 'สินค้าเปรียบเทียบ (%s)'; 
$_['text_display']      = 'แสดง:';
$_['text_list']         = 'แบบรายการ';
$_['text_grid']         = 'แบบกรอง';
$_['text_sort']         = 'จัดเรียงโดย:';
$_['text_default']      = 'ค่าเริ่มต้น';
$_['text_name_asc']     = 'ชื่อจาก A - Z';
$_['text_name_desc']    = 'ชื่อจาก Z - A'; 
$_['text_price_asc']    = 'ราคา ต่ำ &gt; สูง';
$_['text_price_desc']   = 'ราคา สูง &gt; ต่ำ';
$_['text_rating_asc']   = 'อัตราต่ำสุด'; 
$_['text_rating_desc']  = 'อัตราสูงสุด';
$_['text_model_asc']    = 'รุ่น (A - Z)';
$_['text_model_desc']   = 'รุ่น (Z - A)';
$_['text_limit']        = 'แสดง:';
?>