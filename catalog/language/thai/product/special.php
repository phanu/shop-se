<?php
// Heading
$_['heading_title']     = 'ข้อเสนอพิเศษ';

// Text
$_['text_empty']        = 'ไม่มีข้อเสนอพิเศษ';
$_['text_quantity']     = 'ปริมาณ:';
$_['text_manufacturer'] = 'ยี่ห้อ:';
$_['text_model']        = 'รหัสสินค้า:'; 
$_['text_points']       = 'คะแนนสะสม:'; 
$_['text_price']        = 'ราคา:'; 
$_['text_tax']          = 'ภาษี:'; 
$_['text_reviews']      = 'ความคิดเห็นใน %s รายการ'; 
$_['text_compare']      = 'สินค้าเปรียบเทียบ (%s)'; 
$_['text_display']      = 'แสดง:';
$_['text_list']         = 'รายการ';
$_['text_grid']         = 'ตะแกรง';
$_['text_sort']         = 'จัดเรียงโดย:';
$_['text_default']      = 'ค่าเริ่มต้น';
$_['text_name_asc']     = 'ชื่อจาก A - Z';
$_['text_name_desc']    = 'ชื่อจาก Z - A'; 
$_['text_price_asc']    = 'ราคา ต่ำ &gt; สูง';
$_['text_price_desc']   = 'ราคา สูง &gt; ต่ำ';
$_['text_rating_asc']   = 'อัตราต่ำสุด'; 
$_['text_rating_desc']  = 'อัตราสูงสุด';
$_['text_model_asc']    = 'รุ่น (A - Z)';
$_['text_model_desc']   = 'รุ่น (Z - A)';
$_['text_limit']        = 'แสดง:';
?>