<?php
// Text
$_['text_refine']       = 'หมวดสินค้าย่อย';
$_['text_product']      = 'สินค้า';
$_['text_error']        = 'ไม่พบหมวดหมู่!';
$_['text_empty']        = 'ไม่มีรายการสินค้าอยู่ในหมวดสินค้านี้';
$_['text_quantity']     = 'ปริมาณ:';
$_['text_manufacturer'] = 'ยี่ห้อ:';
$_['textpage'] = 'หน้า ' ;
$_['text_model']        = 'รหัสสินค้า:'; 
$_['text_points']       = 'คะแนนสะสม:'; 
$_['text_price']        = 'ราคา:'; 
$_['text_zero_price']   = 'Call for Price';
$_['text_tax']          = 'ภาษี:'; 
$_['text_reviews']      = 'ความคิดเห็น %s รายการ'; 
$_['text_compare']      = 'สินค้าเปรียบเทียบ (%s)'; 
$_['text_display']      = 'การแสดง :';
$_['text_list']         = 'แบบรายการ';
$_['text_grid']         = 'แบบกรอง';
$_['text_sort']         = 'เรียงลำดับโดย:';
$_['text_default']      = 'ค่าเริ่มต้น';
$_['text_name_asc']     = 'ชื่อจาก A - Z';
$_['text_name_desc']    = 'ชื่อจาก Z - A';
$_['text_price_asc']    = 'ราคา ต่ำ &gt; สูง';
$_['text_price_desc']   = 'ราคา สูง &gt; ต่ำ';
$_['text_rating_asc']   = 'อัตราต่ำสุด';
$_['text_rating_desc']  = 'อัตราสูงสุด';
$_['text_model_asc']    = 'รุ่น (A - Z)';
$_['text_model_desc']   = 'รุ่น (Z - A)';
$_['text_limit']        = 'แสดง:';
?>