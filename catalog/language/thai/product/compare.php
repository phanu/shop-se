<?php
// Heading
$_['heading_title']     = 'สินค้าเปรียบเทียบ';
 
// Text
$_['text_product']      = 'รายละเอียดสินค้า';
$_['text_name']         = 'สินค้า';
$_['text_image']        = 'ภาพ';
$_['text_price']        = 'ราคา';
$_['text_model']        = 'รุ่น';
$_['text_manufacturer'] = 'ยิ่ห้อ';
$_['text_availability'] = 'มีสินค้า';
$_['text_instock']      = 'ในสต็อค';
$_['text_rating']       = 'อัตรา';
$_['text_reviews']      = 'ความคิดเห็น %s รายการ'; 
$_['text_summary']      = 'ผลรวม';
$_['text_weight']       = 'น้ำหนัก';
$_['text_dimension']    = 'มิติ (ก x ย x ส)';
$_['text_remove']       = 'เอาออก';
$_['text_compare']      = 'เปรียบเทียบสินค้า (%s)';
$_['text_success']      = 'สำเร็จ: คุณได้เพิ่ม <a href="%s">%s</a> เข้าสู่ <a href="%s">การเปรียบเทียบสินค้า</a>!';
$_['text_empty']        = 'คุณไม่ได้เลือกสินค้าใดๆในการเปรียบเทียบ.';
?>