<?php
// Text
$_['text_hidelines']       = 'ซ่อนสาย';
$_['text_designertool']    = 'เครื่องมือออกแบบ';
$_['text_undo']            = 'แก้';
$_['text_redo']            = 'ทำซ้ำ';
$_['text_reset']           = 'ตั้งใหม่';
$_['text_selectcolor']     = 'เลือก สี';
$_['text_imageartwork']    = 'เพิ่มภาพและงานศิลปะ';
$_['text_started']         = 'ขอเริ่มต้น';
$_['text_casecolor']       = 'กรณีสี';
$_['text_hexcode']         = 'รหัสฐานสิบหก';
$_['text_uploadimage']     = 'อัปโหลดภาพ';
$_['text_artwork']         = 'งานศิลปะ';
$_['text_uploadimage']     = 'อัปโหลดภาพ';
$_['text_terms']           = 'แง่';
$_['text_uploadagree']     = 'โดยการอัปโหลดคุณเห็นด้วยกับเรา';
$_['text_pleasenote']      = 'โปรดทราบ';
$_['text_guarantee']       = 'เพื่อรับประกันการพิมพ์เก็บรายละเอียดภายในเส้นสีเขียว';
$_['text_printcoverage']   = 'สำหรับความคุ้มครองการพิมพ์เต็มรูปแบบ, ภาพ extand กับสายสีน้ำเงิน';
$_['text_addtocart']       = 'สั่งซื้อ';
$_['text_sharecreate']     = 'ประหยัด 5% ตอนนี้-ร่วมกันสร้างของคุณกับเพื่อน!';
$_['text_facebook']        = 'facebook';
$_['text_tweet']           = 'ทวีต';
$_['text_email']           = 'อีเมล์';
$_['text_link']            = 'ลิงค์';
$_['text_writetext']       = 'เขียนข้อความ';
$_['text_applytext']       = 'คลิกที่นี่เพื่อนำข้อความของคุณ';
$_['text_enteryourtext']   = 'คุณใส่ข้อความ';
?>