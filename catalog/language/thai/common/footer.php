<?php
// Text
$_['text_information']  = 'ข้อมูล';
$_['text_service']      = 'บริการของเรา';
$_['text_extra']        = 'พิเศษ';
$_['text_webboard']        = 'เว็บบอร์ด';
$_['text_account']      = 'บัญชี';
$_['text_contact']      = 'ติดต่อเรา';
$_['text_return']       = 'ตีกลับ';
$_['text_sitemap']      = 'แผนผังเว็บไซต์';
$_['text_manufacturer'] = 'ยี่ห้อ';
$_['text_voucher']      = 'บัตรส่วนลด';
$_['text_affiliate']    = 'พันธมิตรการค้า';
$_['text_special']      = 'ข้อเสนอพิเศษ';
$_['text_account']        = 'เข้าระบบ';
$_['text_login']        = 'เข้าระบบ';
$_['text_order']        = 'ประวัติสั่งซื้อ';
$_['text_wishlist']     = 'รายการโปรด';
$_['text_newsletter']   = 'จดหมายข่าว';
$_['text_powered'] = '© 2012 SE-Update.com';
$_['device_version_mobile_desktop'] = '<center><a rel="external" href="index.php?change_device=1&device_name=mobile_desktop">Desktop Version</a></center>';
	$_['device_version_mobile'] = '<center><h1><a rel="external" href="index.php?change_device=1&device_name=mobile">กลับไป Mobile Version</a></h1></center>';
	$_['device_version_tablet_desktop'] = '<center><a rel="external" href="index.php?change_device=1&device_name=tablet_desktop">Desktop Version</a></center>';
	$_['text_omtex_powered'] =  '';
	$_['device_version_tablet']  = '<center><h1><a rel="external" href="index.php?change_device=1&device_name=tablet">กลับไป Tablet Version</a></h1></center>';
?>