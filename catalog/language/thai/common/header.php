<?php
//Text
$_['text_home']     = 'หน้าแรก';
$_['text_wishlist'] = 'รายการโปรด (%s)';
$_['text_cart']     = 'ตะกร้าสินค้า';
$_['text_items']     = '%s รายการ - %s';
$_['text_search']   = 'หาสินค้าไม่พบ พิมพ์ชื่อสินค้าตรงนี้เลย';
$_['text_welcome']   = 'ยินดีต้อนรับชาวอารยธรรม <a href="%s">เข้าสู่ระบบสมาชิก</a> หรือ <a href="%s">สมัครสมาชิกใหม่</a>.';
$_['text_logged']   = 'คุณเข้าใช้งานในชื่อคุณ <a href="%s">%s</a>&nbsp; &nbsp;&nbsp;<b>(</b> <a href="%s">ออกจากระบบ</a> <b>)</b>';
$_['text_account']  = 'บัญชีสมาชิก';
$_['text_checkout'] = 'ชำระเงิน';
$_['text_language'] = 'ภาษา';
$_['text_currency'] = 'สกุลเงิน';
$_['text_webboard'] = 'Pre-order/จองสินค้า';
$_['text_nav_categories'] = 'หมวดหมู่';
	$_['text_nav_contact'] = 'ติดต่อร้านค้า';
	$_['text_nav_info'] = 'ข้อมูล';
	$_['text_nav_support'] = 'Support';
	$_['text_nav_cart'] = 'ตระกร้าสินค้า';
	$_['text_i_agree'] = 'ยอมรับ';
$_['text_main_categories'] = 'หมวดหมู่สินค้าหลัก';
?>