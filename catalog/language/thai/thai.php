<?php
// Locale
$_['code']                  = 'th';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['decimal_point']         = '.';
$_['thousand_point']        = ',';

// Text
$_['text_home']             = 'หน้าแรก';
$_['text_yes']              = 'ใช่';
$_['text_no']               = 'ไม่';
$_['text_none']             = ' --- ไม่มี --- ';
$_['text_select']           = ' --- โปรดเลือก --- ';
$_['text_all_zones']        = 'ทุกโซน';
$_['text_pagination']       = 'แสดงสินค้าตั้งแต่ {start} ถึง {end} จากทั้งหมด {total} รายการ({pages} หน้า)';
$_['textpage']  = 'หน้า ';
$_['text_separator']        = ' &gt; ';

// Buttons
$_['button_add_address']    = 'เพิ่มที่อยู่';
$_['button_back']           = 'ย้อนกลับ';
$_['button_continue']       = 'ถัดไป';
$_['button_cart']           = 'หยิบใส่ตะกร้า';
$_['button_compare']        = 'ใส่เปรียบเทียบ';
$_['button_wishlist']       = 'บันทึกสินค้ารายการโปรด';
$_['button_checkout']       = 'สั่งซื้อสินค้า';
$_['button_confirm']        = 'ยืนยันคำสั่งซื้อ';
$_['button_coupon']         = 'ใช้คูปอง';
$_['button_delete']         = 'ลบ';
$_['button_edit']           = 'แก้ไข';
$_['button_new_address']    = 'เพิ่มที่อยู่ใหม่';
$_['button_change_address'] = 'เปลี่ยนแปลงที่อยู่';
$_['button_add_product']    = 'เพิ่มสินค้า';
$_['button_remove']         = 'เอาออก';
$_['button_reviews']        = 'ตรวจสอบ';
$_['button_reorder']        = 'สั่งใหม่';
$_['button_write']          = 'เขียนความคิดเห็น';
$_['button_login']          = 'เข้าสู่ระบบ';
$_['button_update']         = 'ปรับปรุงรายการ';
$_['button_shopping']       = 'ซื้อของต่อ';
$_['button_search']         = 'ค้นหา';
$_['button_shipping']       = 'เลือกวิธีจัดส่ง';
$_['button_guest']          = 'คิดเงินแบบผู้มาเยือน';
$_['button_voucher']        = 'ใช้บัตรส่วนลด';
$_['button_view']           = 'ดูรายละเอียด';
$_['button_upload']         = 'อัพโหลด';
$_['button_reward']         = 'ใช้คะแนน';
$_['button_quote']          = 'ใช้ข้อความ';

// Error
$_['error_upload_1']        = 'คำเตือน: ขนาดไฟล์ใหญ่เกินไปจาก php.ini กำหนด!';
$_['error_upload_2']        = 'คำเตือน: ขนาดไฟล์ใหญ่เกินไป!';
$_['error_upload_3']        = 'คำเตือน: อัพโหลดได้เพียงบางส่วน!';
$_['error_upload_4']        = 'คำเตือน: ไม่มีไฟล์!';
$_['error_upload_6']        = 'คำเตือน: โฟลเดอร์ผิด!';
$_['error_upload_7']        = 'คำเตือน: เขียนไฟล์ไม่ได้!';
$_['error_upload_8']        = 'คำเตือน: หยุดโดยนามสกุลไฟล์!';
$_['error_upload_999']      = 'คำเตือน: ไม่มีรหัส!';
$_['text_payment_address']      = 'รายละเอียดผู้สั่งซื้อ';
$_['text_shipping_address']      = 'รายละเอียดสำหรับจัดส่งสินค้า';
$_['text_warning']      = 'สำคัญ!! โปรดตรวจสอบให้ถูกต้อง<br>ข้อมูลส่วนนี้ใช้ในการพิมพ์ชื่อ-ที่อยู่ลงบนหน้ากล่องพัสดุ';
$_['text_product_detail']      = '<br>รายละเอียดสินค้า';
$_['text_notice_address']      = 'คำแนะนำหากชื่อ-ที่่อยู่ไม่ถูกต้อง';
$_['text_correct_address']      = '
- สำหรับลูกค้าที่ทำรายการสั่งซื้อ<b>แบบผู้มาเยือน</b>ให้กลับไปแก้ไขที่ <b>ขั้นที่ 3: รายละเอียดสำหรับจัดส่งสินค้า</b><br>
- สำหรับลูกค้าที่ทำรายการ<b>แบบสมัครสมาชิก</b>ให้ทำการกลับไปแก้ไขที่ <a href="http://shop.se-update.com/index.php?route=account/address">แก้ไขที่อยู่</a>


';

$_['text_focus_button']      = '<br>คำสั่งซื้อจะสมบูรณ์ เมื่อท่านกดปุ่ม <u>ยืนยันคำสั่งซื้อ</u>บริเวณด้านล่างสุดแล้วเท่านั้น';

?>