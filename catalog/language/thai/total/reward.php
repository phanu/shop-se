<?php
// Heading 
$_['heading_title'] = 'ใช้คะแนนสะสม (มีอยู่ %s)';

// Text
$_['text_reward']   = 'คะแนนสะสม(%s):';
$_['text_order_id'] = 'รหัส สั่งซื้อ: #%s';
$_['text_success']  = 'สำเร็จ: คะแนนสะสมได้ถูกนำมาใช้แล้ว!';

// Entry
$_['entry_reward']  = 'คะแนนที่ใช้ (สูงสุด %s):';

// Error
$_['error_empty']   = 'ผิดพลาด: กรุณาใส่คะแนนที่คุณจะใช้!';
$_['error_points']  = 'ผิดพลาด: คุณไม่มี %s คะแนน!';
$_['error_maximum'] = 'ผิดพลาด: คะแนนสูงสุดที่คุณใช้ได้คือ %s!';
?>