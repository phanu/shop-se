<?php
#############################################################################
#  Module Mini Webboard for Opencart 1.4.x From Team SiamOpencart.com		  													   #
#  เว็บผู้พัฒนา www.siamopencart.com ,www.thaiopencart.com                                                                                 #
#  โดย Somsak2004 วันที่ 15 กุมภาพันธ์ 2553                                                                                                            #
#############################################################################
# โดยการสนับสนุนจาก                                                                                                                                            #
# Unitedsme.com : ผู้ให้บริการเช่าพื้นที่เว็บไซต์ จดโดเมน ระบบ Linux                                                                              #
# Net-LifeStyle.com : ผู้ให้บริการเช่าพื้นที่เว็บไซต์์ จดโดเมน ระบบ Linux																           #
# SiamWebThai.com : SEO ขั้นเทพ โปรโมทเว็บขั้นเซียน ออกแบบ พัฒนาเว็บไซต์ / ตามความต้องการ และถูกใจ Google 		   #
#############################################################################
// Text
$_['heading_title']     = 'New Post Webboard';
$_['text_new']  = 'Title of post';
$_['text_detail']  = 'Detail';
$_['text_send']  = 'Send New Post';
$_['entry_enquiry']  = 'Enetr Detail';
$_['entry_captcha']  = 'Enetr Captcha Code';
$_['text_message']  = '<h1>Complete for save your post in webboard Thank!</h1>';
$_['error_captcha']  = 'Please Enter Captcha Code';
$_['error_enquiry']  = 'Please Enetr Detail';
$_['error_title']  = 'please Enter Title of Post';
$_['button_continue']  = 'Go Webboard';
$_['error_name'] = '***Enter your name';
$_['error_email']  = '***Enter your e-mail';
$_['text_name']  = 'Name:';
$_['text_email']  = 'Email:';
$_['text_email_alert']  = 'Notify me: Notify me when someone reply your post(please put your mail).';
$_['text_image_low']      = 'Could not be delete!!!';
$_['entry_image']  = 'Image:';
$_['entry_recommend']  = 'Not Over 300Kb(gif,jpg,png)';
$_['btn_add']  = 'Add';
$_['btn_remove']  = 'Remove ';
?>