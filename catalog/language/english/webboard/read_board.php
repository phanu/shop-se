<?php
#############################################################################
#  Module Mini Webboard for Opencart 1.4.x From Team SiamOpencart.com		  													   #
#  เว็บผู้พัฒนา www.siamopencart.com ,www.thaiopencart.com                                                                                 #
#  โดย Somsak2004 วันที่ 15 กุมภาพันธ์ 2553                                                                                                            #
#############################################################################
# โดยการสนับสนุนจาก                                                                                                                                            #
# Unitedsme.com : ผู้ให้บริการเช่าพื้นที่เว็บไซต์ จดโดเมน ระบบ Linux                                                                              #
# Net-LifeStyle.com : ผู้ให้บริการเช่าพื้นที่เว็บไซต์์ จดโดเมน ระบบ Linux																           #
# SiamWebThai.com : SEO ขั้นเทพ โปรโมทเว็บขั้นเซียน ออกแบบ พัฒนาเว็บไซต์ / ตามความต้องการ และถูกใจ Google 		   #
#############################################################################
// Text
$_['heading_title']     = 'Read Post';
$_['text_new']  = 'Title : ';
$_['text_detail']  = 'Detail of post :';
$_['text_send']  = 'Reply Post';
$_['text_quess']  = 'Reply';
$_['text_send_alert']  = 'Only Member';
$_['text_quess']  = 'Reply post';
$_['text_author']  = 'Author :';
$_['text_reply']  = 'Reply By :';
$_['text_time']  = 'Date/Time';
$_['text_nobody']  = 'No body reply your post!!';
$_['entry_enquiry']  = 'Reply Post : ';
$_['entry_captcha']  = 'Enetr Captcha Code';
$_['text_message']  = '<h1>Complete for save your reply in webboard Thank!</h1>';
$_['error_captcha']  = 'Please Enter Captcha Code';
$_['error_enquiry']  = 'Please Enetr Detail';
$_['error_title']  = 'please Enter Title of Post';
$_['button_continue']  = 'Go Webboard';
$_['alert'] = 'Are you sure to delete reply of toic ?';
$_['text_name']  = 'Name:';
$_['text_email']  = 'Email:';
$_['error_name'] = '***Enter your name';
$_['error_email']  = '***Enter your e-mail';
$_['email_subject']      = 'reply webboard from %s';
$_['text_lock']  = 'Locked!!!';
$_['text_image_low']      = 'Could not be delete!!!';
$_['entry_image']  = 'Image:';
$_['entry_recommend']  = 'Not Over 300Kb(gif,jpg,png)';
$_['btn_add']  = 'Add';
$_['btn_remove']  = 'Remove ';
?>