<?php
#############################################################################
#  Module Mini Webboard for Opencart 1.4.x From Team SiamOpencart.com		  													   #
#  เว็บผู้พัฒนา www.siamopencart.com ,www.thaiopencart.com                                                                                 #
#  โดย Somsak2004 วันที่ 15 กุมภาพันธ์ 2553                                                                                                            #
#############################################################################
# โดยการสนับสนุนจาก                                                                                                                                            #
# Unitedsme.com : ผู้ให้บริการเช่าพื้นที่เว็บไซต์ จดโดเมน ระบบ Linux                                                                              #
# Net-LifeStyle.com : ผู้ให้บริการเช่าพื้นที่เว็บไซต์์ จดโดเมน ระบบ Linux																           #
# SiamWebThai.com : SEO ขั้นเทพ โปรโมทเว็บขั้นเซียน ออกแบบ พัฒนาเว็บไซต์ / ตามความต้องการ และถูกใจ Google 		   #
#############################################################################
// Text
$_['heading_title']     = 'Webboard';
$_['text_title']     = 'Webboard';
$_['text_order']  = 'No.';
$_['text_title_board']  = 'Title';
$_['text_read']  = 'Read';
$_['text_reply']  = 'Reply';
$_['text_date']    = 'Date';
$_['text_author']   = 'Author';
$_['text_delete']     = 'Delete';
$_['text_mark'] = 'Mark';
$_['text_newpost'] = 'New Post';
$_['text_newpost_alert'] = 'New Post only Member';
$_['alert'] = 'Are you sure to delete topic ?';
$_['text_room']   = 'Room>>';
$_['text_cask']   = 'Ask Title';
$_['text_cquess']   = 'Replies';
?>