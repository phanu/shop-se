<?php
#############################################################################
#  Module Mini Webboard for Opencart 1.4.x From Team SiamOpencart.com		  													   #
#  เว็บผู้พัฒนา www.siamopencart.com ,www.thaiopencart.com                                                                                 #
#  โดย Somsak2004 วันที่ 15 กุมภาพันธ์ 2553                                                                                                            #
#############################################################################
# โดยการสนับสนุนจาก                                                                                                                                            #
# Unitedsme.com : ผู้ให้บริการเช่าพื้นที่เว็บไซต์ จดโดเมน ระบบ Linux                                                                              #
# Net-LifeStyle.com : ผู้ให้บริการเช่าพื้นที่เว็บไซต์์ จดโดเมน ระบบ Linux																           #
# SiamWebThai.com : SEO ขั้นเทพ โปรโมทเว็บขั้นเซียน ออกแบบ พัฒนาเว็บไซต์ / ตามความต้องการ และถูกใจ Google 		   #
#############################################################################
// Text
$_['heading_title']     = 'Edit Post Webboard';
$_['text_new']  = 'Title of post to edit';
$_['text_detail']  = 'Detail';
$_['text_send']  = 'Send edit Post';
$_['entry_enquiry']  = 'Enetr Detail';
$_['entry_captcha']  = 'Enetr Captcha Code';
$_['text_message']  = '<h1>Complete for save your post in webboard Thank!</h1>';
$_['error_captcha']  = 'Please Enter Captcha Code';
$_['error_enquiry']  = 'Please Enetr Detail';
$_['error_title']  = 'please Enter Title of Post';
$_['button_continue']  = 'Go Webboard';
?>
