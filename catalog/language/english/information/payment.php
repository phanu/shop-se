<?php
//������駡�ê����Թ ����Ѻ Opencart 1.5.1.2 ����
//�� Amdev elect.tu@gmail.com
//http://www.opencart2u.com
//ʧǹ�Ԣ�Է��� ����ᨡ���� ��˹��� ��������Ѻ���͹حҵ
// Heading
$_['heading_title']      = 'Payment Form';

// Text 
$_['text_location']      = 'Our Location:';
$_['text_address']       = 'Address:';
$_['text_email']         = 'Email:';
$_['text_telephone']     = 'Telephone:';
$_['text_fax']           = 'Fax:';
$_['text_message']       = '<p>Success : Your payment information has been send to store owner!</p>';
$_['text_select_bank']       = 'Payment Method';
$_['text_member_only']       = 'Oops! Access denied , please login to view this page';
$_['text_upload']       = 'Your file was successfully uploaded!';

// Entry Fields
$_['entry_name']         = 'Customer Name:';
$_['entry_email']        = 'Customer Email:';
$_['entry_bank']        = 'Payment Method:';
$_['entry_total']        = 'Total:';
$_['entry_paid_time']        = 'Pay time:';
$_['entry_paid_date']           = 'dd-mm-Y Pay date:';
$_['entry_enquiry']      = 'Comment:';
$_['entry_captcha'] = 'Captcha:';
$_['entry_no_comment']        = 'No comment';
$_['entry_image']        = 'Attach File:';
$_['entry_order']        = 'Order ID:';
$_['entry_file']      = 'Not over %s';


// Email
$_['email_subject']      = 'Payment From %s';
$_['text_thanks'] = 'Thank you %s';
$_['text_detail'] = '%s receive your payment information , and will proceed to verify the information as soon as possible.';
$_['text_reply'] = 'If you have any questions.Please reply to this letter at all.';
$_['text_thanks_footer'] = 'Send by %s';
$_['text_confirm'] = 'Payment inform confirmation';

// Errors
$_['error_enquiry']        = 'Enquiry is require! ';
$_['error_bank']         = 'Please select a payment meyhod! ';
$_['error_total']         = 'Total field is require! ';
$_['error_paid_time']         = 'Pay time field is require!';
$_['error_name']         = 'Customer name is require!';
$_['error_email']        = 'Email is incorrect!';
$_['error_paid_date']        = 'Pay date is require!';
$_['error_captcha'] = 'Captcha code is incorrect!';
$_['error_image']         = 'Files filed is require!';
$_['error_order']         = 'Order ID is require OR this Order ID does not exist in the system!';
$_['error_telephone']        = 'Telephone is require! ';

//upload
$_['error_file_name']        = 'File \'s name is invalid OR File field is require! ';
$_['error_file_type']        = 'File type is invalid! ';
$_['error_writable_image']        = 'File directory can not writable! ';
$_['error_upload_image']        = 'File upload is error! ';
$_['error_max']        = 'File upload is too large! ';

?>