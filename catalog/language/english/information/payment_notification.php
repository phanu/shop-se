<?php
// Heading
$_['heading_title']  = 'Payment Notification Form';

// Text 
$_['text_customer_information']  = 'Customer Information';
$_['text_payment_details']= 'Payment Details';
$_['text_choose']= 'Please Choose';
$_['text_saudi_investment_bank']= 'Saudi Investment Bank';
$_['text_al_rajhi_bank']= 'Al-Rajhi Bank';
$_['text_alahli_bank']= 'Alahli Bank';
$_['text_bank_samba']= 'Bank samba';
$_['text_banque_saudi_fransi']= 'Banque Saudi Fransi';
$_['text_saudi_british_bank']= 'Saudi British Bank';
$_['text_saudi_hollandi_bank']= 'Saudi Hollandi Bank';
$_['text_al_bilad_bank']= 'Al Bilad Bank';
$_['text_payment_slip']= '<br>(File format: .doc, .docx, .pdf, .gif, .jpg, .png, .bmp)<br>(Maximum upload file size: 1MB)';
$_['text_message']   = '<p>Submitted to the customer care service department and you will be answered soon.Thank you</p>';

$_['text_name']     = 'Name:';
$_['text_phone']    = 'Phone:';
$_['text_order_id']  = 'Order ID:';
$_['text_amount_paid']  = 'Amount Paid:';
$_['text_payment_date']  = 'Payment Date:';
$_['text_payment_method']  = 'Payment Method:';
$_['text_payment_transaction']  = 'Payment Transaction Details:';
// Entry Fields
$_['entry_name']     = 'Name:';
$_['entry_email']    = 'E-Mail Address:';
$_['entry_phone']    = 'Phone:';
$_['entry_order_id']  = 'Order ID:';
$_['entry_amount_paid']  = 'Amount Paid:';
$_['entry_payment_date']  = 'Payment Date:';
$_['entry_payment_method']  = 'Payment Method:';
$_['entry_payment_transaction']  = 'Payment Transaction Details:';
$_['entry_payment_slip']  = 'Upload Payment Slip:';
 
  
$_['entry_captcha']  = 'Enter the code in the box below:';

// Email
$_['email_subject']  = 'Payment Notification %s';

// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_phone']  = 'Please enter your telephone number!';
$_['error_order_id']  = 'Please enter your Order ID!';
$_['error_amount_paid']  = 'Please enter the total amount paid!';
$_['error_payment_date']  = 'Please enter date of payment!';
$_['error_payment_method']  = ' Please select payment method!';
$_['error_payment_transaction']  = 'Please enter payment transaction details!';
$_['error_captcha']  = 'Verification code does not match the image!';

?>
