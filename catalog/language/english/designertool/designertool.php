<?php
// Text
$_['text_hidelines']       = 'hide lines';
$_['text_designertool']    = 'Designer Tool';
$_['text_undo']            = 'undo';
$_['text_redo']            = 'redo';
$_['text_reset']           = 'reset';
$_['text_selectcolor']     = 'select colors';
$_['text_imageartwork']    = 'add images & artwork';
$_['text_started']         = 'let\'s get started';
$_['text_casecolor']       = 'case color';
$_['text_hexcode']         = 'hex code';
$_['text_uploadimage']     = 'upload image';
$_['text_artwork']         = 'artwork';
$_['text_uploadimage']     = 'Upload an image';
$_['text_terms']           = 'terms';
$_['text_uploadagree']     = 'By uploading you agree to our';
$_['text_pleasenote']      = 'please note';
$_['text_guarantee']       = 'To guarantee print, keep details within green lines';
$_['text_printcoverage']   = 'For full print coverage, extand image to blue line';
$_['text_addtocart']       = 'add to cart';
$_['text_sharecreate']     = 'SAVE 5% NOW -share your creation with friends!';
$_['text_facebook']        = 'facebook';
$_['text_tweet']           = 'tweet';
$_['text_email']           = 'email';
$_['text_link']            = 'link';
$_['text_writetext']       = 'write text';
$_['text_applytext']       = 'click here to apply your text';
$_['text_enteryourtext']   = 'Enter your text';
?>