<?php

// Text  
$_['text_subject']					= 'Low Stock - Order# %s';
$_['text_message']					= 'You received an order which has caused the below products quantity to fall below their set reorder alarm';
$_['text_low_stock_heading']		= 'Low Stock Report';

// Column
$_['column_name']					= 'Product Name';
$_['column_model']					= 'Model Number';
$_['column_options']				= 'Product Options';
$_['column_qty']					= 'Current Quantity';
$_['column_ordered_qty']			= 'Ordered Quantity';
$_['column_reorder_qty']			= 'Reorder Quantity';

?>