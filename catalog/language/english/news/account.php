<?php
// Heading 
$_['heading_title']      = 'My Account';

// Text
$_['text_account']       = 'Author';
$_['text_my_account']    = 'My Account';
$_['text_my_orders']     = 'My Orders';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit']          = 'Edit your account information';
$_['text_password']      = 'Change your password';
$_['text_article_manager']       = 'Manager your article';
$_['text_article_detailt']      = 'Listing';
$_['text_insert_article']         = 'Submit New Article';
$_['text_logout']      = 'Logout';
$_['text_reward']        = 'Your Reward Points'; 
$_['text_return']        = 'View your return requests'; 
$_['text_transaction']   = 'Your Transactions'; 
$_['text_newsletter']    = 'Subscribe / unsubscribe to newsletter';
?>