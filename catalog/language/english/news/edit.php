<?php
// Heading 
$_['heading_title']     = 'My Account Information';

// Text
$_['entry_image']      = 'Your Photo:';
$_['text_clear']      = 'Clear';
$_['text_browse']      = 'Browse';
$_['text_account']      = 'Account';
$_['text_account']      = 'Account';
$_['text_edit']         = 'Edit Information';
$_['text_your_details'] = 'Your Personal Details';
$_['text_your_address']    = 'Your Address';
$_['text_success']      = 'Success: Your account has been successfully updated.';

// Entry
$_['entry_author']      = 'Author Username:';
$_['entry_firstname']      = 'First Name:';
$_['entry_lastname']       = 'Last Name:';
$_['entry_email']          = 'E-Mail:';
$_['entry_telephone']      = 'Telephone:';
$_['entry_website']            = 'Your Website:';
$_['entry_company']        = 'Company:';
$_['entry_address']      = 'Address:';
$_['entry_postcode']       = 'Post Code:';
$_['entry_city']           = 'City:';
$_['entry_country']        = 'Country:';
$_['entry_zone']           = 'Region / State:';

// Error
$_['entry_description'] = 'Author Info:<br /><span class="help">Can you please give us a brief summary of your history, experience and qualifications.</span>';
$_['error_description']        =  'Author Info required!';
$_['error_username_not_allowed']         = 'Author Username is already registered!';
$_['error_exists']     = 'Warning: E-Mail address is already registered!';
$_['error_firstname']  = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']   = 'Last Name must be between 1 and 32 characters!';
$_['error_email']      = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']  = 'Telephone must be between 3 and 32 characters!';
?>