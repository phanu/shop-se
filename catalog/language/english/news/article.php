<?php
// Text
$_['text_search']       = 'Search';
$_['text_date_added']       = 'Added';
$_['text_more']        = 'Details';
$_['text_vote'] = 'Vote';
$_['text_total_votes'] = 'Total votes:';
$_['text_poll_results'] = 'View Poll Results';
$_['text_author'] = 'by';
$_['text_by']       = 'by '; 
$_['entry_reference']       = 'Reference:'; 
$_['text_points']       = 'Price in reward points:';
$_['text_stock']        = 'Availability:';
$_['text_instock']      = 'In Stock';
$_['text_price']        = 'Price:'; 
$_['text_tax']          = 'Ex Tax:'; 
$_['text_discount']     = '%s or more %s';
$_['text_option']       = 'Available Options';
$_['text_qty']          = 'Qty:';
$_['text_minimum']      = 'This article has a minimum quantity of %s';
$_['text_or']           = '- OR -';
$_['text_comments']      = '%s comments'; 
$_['text_viewed']      = '%s viewed'; 
$_['text_write']        = 'Write a comment';
$_['text_no_comments']   = 'There are no comments for this article.';
$_['text_on']           = ' on ';
$_['text_note']         = '<span style="color: #FF0000;">Note:</span> HTML is not translated!';
$_['text_share']        = 'Share';
$_['text_success']      = 'Thank you for your review.';
$_['text_wait']         = 'Please Wait!';
$_['text_tags']         = 'Tags:';
$_['text_error']        = 'Article not found!';
$_['text_related_product']        = 'Retated Product';
$_['text_login_to_download'] = 'You must login to download this attachment!';
// Entry
$_['entry_name']        = 'Your Name:';
$_['entry_comment']      = 'Your Comment:';
$_['entry_rating']      = 'Rating:';
$_['entry_good']        = 'Good';
$_['entry_bad']         = 'Bad';
$_['entry_captcha']     = 'Enter the code in the box below:';

// Tabs
$_['tab_description']   = 'Description';
$_['tab_download']   = 'Download';
$_['tab_poll']     = 'Community Poll';
$_['tab_image']     = 'Image';
$_['tab_comment']        = 'Comment (%s)';
$_['tab_related']       = 'Related Articles'; 

// Error
$_['error_name']        = 'Warning: Review Name must be between 3 and 25 characters!';
$_['error_text']        = 'Warning: Review Text must be between 25 and 1000 characters!';
$_['error_rating']      = 'Warning: Please select a review rating!';
$_['error_captcha']     = 'Warning: Verification code does not match the image!';
$_['error_upload']      = 'Upload required!';
$_['error_filename']    = 'Filename must be between 3 and 64 characters!';
$_['error_filetype']    = 'Invalid file type!';
?>