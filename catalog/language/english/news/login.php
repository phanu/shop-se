<?php
// Heading 
$_['heading_title']                = 'Author Login';

// Text
$_['text_account']                 = 'Account';
$_['text_login']                   = 'Login';
$_['text_new_author']            = 'New Author';
$_['text_register']                = 'Register Author';
$_['text_register_account']        = 'By creating an Author account you will written any articles that you believe will benefit people then we would be delighted to share this for you through our network.';
$_['text_returning_author']      = 'Returning Author';
$_['text_i_am_returning_author'] = 'I am a returning author';
$_['text_forgotten']               = 'Forgotten Password';

// Entry
$_['entry_email']                  = 'E-Mail Address:';
$_['entry_password']               = 'Password:';

// Error
$_['error_login']                  = 'Warning: No match for E-Mail Address and/or Password.';
$_['error_approved']               = 'Warning: Your account requires approval before you can login.'; 
?>