<?php
//OpenCart Extension
//Project Name: OpenCart Combo/Bundle
//Author: Fanha Giang a.k.a fanha99
//Email (PayPal Account): fanha99@gmail.com
//License: Commercial
?>
<?php
// Heading
$_['heading_title']     = 'Combos Set';
$_['text_empty']        = 'There are no special offer products to list.';
$_['text_tax']          = 'Ex Tax:'; 
$_['text_display']      = 'Display:';
$_['text_list']         = 'List';
$_['text_grid']         = 'Grid';
$_['text_limit']        = 'Show:';
$_['tab_combo']     = 'Combo set';
$_['button_combo_cart']     = 'Add all to Cart';
$_['this_item']     = 'This item:';
$_['both'] = 'both';
$_['all_three'] = 'all three';
$_['all'] = 'all';
$_['Price_for_all'] = 'Price For All';
$_['this_item'] = 'This item: ';
$_['save_percent'] = 'Save %.0f &#37;';
$_['save_amount'] = 'Save %s';
$_['combo_items'] = 'This Combo set includes the following items:';
?>