<?php
// Heading 
$_['heading_title'] = 'สถิติร้านค้า';

// Text
$_['text_start']  = 'เปิดร้านเมื่อ';
$_['text_update']  = 'ปรับปรุงร้านเมื่อ';
$_['text_pageviews']  = 'PageViews';
$_['text_visitors']  = 'สถิติผู้เยี่ยมชม';

$_['text_products']  = 'สินค้าทั้งหมด';
$_['text_categories']  = 'หมวดหมู่ทั้งหมด';
$_['date_format_short']     = 'd/m/Y';
?>