<?php
/*--------------------------------------------------------
# Opening Hours 1.2 - OpenCart Module
# --------------------------------------------------------
# For OpenCart 1.5.1.x - 1.5.4.x
# Copyright (C) 2012 ThyConsultants.net. All Rights Reserved.
# @license Copyrighted Commercial Software
# Demo: http://demo.thyconsultants.net/opencart
# Website: http://www.thyconsultants.net/
# Support: info@thyconsultants.net
-------------------------------------------------------- */

// Heading 
$_['heading_title']    = 'Opening Hours';

// Text
$_['text_mon']             = 'Monday';
$_['text_tue']             = 'Tuesday';
$_['text_wed']             = 'Wednesday';
$_['text_thur']             = 'Thursday';
$_['text_fri']        = 'Friday';
$_['text_sat']        = 'Saturday';
$_['text_sun']        = 'Sunday';
?>
