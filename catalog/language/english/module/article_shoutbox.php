<?php
// Heading 
$_['heading_title']    = 'Shoutbox';

// Entry
$_['entry_your_name']   = 'Your Name:';
$_['entry_message']   = 'Message:';
$_['entry_captcha']   = 'Captcha:';
$_['please_login']        = 'Please <a href="%s">login</a> to post message!';
// Text 
$_['text_clear_message']    = 'Clear Message';
$_['text_success']    = 'Success';
$_['text_user_message']    = 'Discuss now.';
$_['text_on']      = 'on';
$_['text_said']      = 'say:';
$_['text_no_messages']      = 'No message in shoutbox.';
$_['text_wait']   = 'Please wait...';
$_['button_post_message']   = 'Post Message';
$_['text_reload_captcha']        = 'Reload Captcha';
$_['error_captcha']    = 'Verification code does not match the image!';
$_['error_name']     = 'Name must be between 2 and 25 characters!';
$_['error_message']     = 'Message must be between 5 and 255 characters!';
?>
