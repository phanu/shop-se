<?php
// Text
$_['text_order_next'] 	= 'Remaining time on the workday';
$_['text_hours'] 		= 'hours &amp;';
$_['text_minutes'] 		= 'minutes';
$_['text_order_after'] 	= 'Remaining time until the next workday';
$_['text_no_work'] 		= 'Temporarily we are not accepting new orders. Please come back again later.';

// Today messages
$_['text_today'] 		= 'If you order after the end of the workday, your order will be processed on %s.';
$_['text_today_end'] 	= 'If you order now, your order will be processed on %s.';

// Today Holiday messages
$_['text_today_holiday'] 		 = 'Today is holiday. If you order now, your order will be processed on %s.';
$_['text_today_holidays'] 	     = 'Today is holiday. We have also upcoming holidays on: %s. If you order now, your order will be processed on %s.';
$_['text_today_holidays_number'] = 'Today is holiday. We have also upcoming %s holidays. If you order now, your order will be processed on %s.';

// Tomorrow Holiday messages
$_['text_tomorrow_holiday'] 			= '%s is holiday. If you order after the end of the workday, your order will be processed on %s.';
$_['text_tomorrow_holidays'] 			= 'We have upcoming holidays on: %s. If you order after the end of the workday, your order will be processed on %s.';
$_['text_tomorrow_holidays_number'] 	= 'We have upcoming %s holidays. If you order after the end of the workday, your order will be processed on %s.';
$_['text_tomorrow_holiday_end'] 		= '%s is holiday. If you order now, your order will be processed on %s.';
$_['text_tomorrow_holidays_end'] 		= 'We have upcoming holidays on: %s. If you order now, your order will be processed on %s.';
$_['text_tomorrow_holidays_end_number'] = 'We have upcoming %s holidays. If you order now, your order will be processed on %s.';

// Text addons 
$_['text_workday_end'] = 'Our working hours for today has ended. ';

// Delivery messages
$_['text_period_method'][1] = 'Delivery on %s';

// Shipping messages
$_['text_period_method'][2] = 'Shipping on %s';

// Arrays
$_['text_week_days'] = array(
	1 => 'Monday', 
	2 => 'Tuesday', 
	3 => 'Wednesday', 
	4 => 'Thursday', 
	5 => 'Friday', 
	6 => 'Saturday', 
	7 => 'Sunday'
);
?>