<?php

// Text
$_['text_description']                = 'Use the tools below to generate products widgets for your site.';
$_['text_preview']                    = 'Preview';
$_['text_not_logged']                 = 'Please <a href="%s">login as affiliate</a> or <a href="%s">create an affiliate account</a>';
$_['text_instructions']               = 'Copy generated code and use it in your site.';

// Entry
$_['entry_tracking_code']             = 'Your Tracking Code:';
$_['entry_widget_dimension']          = 'Widget (Width x Height):<span class="help">min: 167 x 255</span>';
$_['entry_image_dimension']           = 'Image (Width x Height):';
$_['entry_limit']                     = 'Products Limit:<span class="help">leave blank = unlimited</span>';
$_['entry_language']                  = 'Language:';
$_['entry_currency']                  = 'Currency:';
$_['entry_slider_time']               = 'Slider Time:<span class="help">seconds</span>';
$_['entry_available_categories']      = 'Available categories: (drag & drop)';
$_['entry_selected_categories']       = 'Selected categories: (drag & drop)';
$_['entry_widget_code'] 			  = 'Widget code:';


// Button
$_['button_generate_widget']          = 'Generate Widget & Preview';
?>