<?php

// Text
$_['text_right_now']           = 'right now'; 
$_['text_seconds_ago']         = ' seconds ago'; 
$_['text_about_1_minute_ago']  = 'about 1 minute ago'; 
$_['text_minutes_ago']         = ' minutes ago';
$_['text_about_1_hour_ago']    = 'about 1 hour ago';  
$_['text_hours_ago']           = ' hours ago'; 
$_['text_yesterday']           = 'yesterday'; 
$_['text_days_ago']            = ' days ago'; 
$_['text_over_a_year_ago']     = 'over a year ago'; 

?>