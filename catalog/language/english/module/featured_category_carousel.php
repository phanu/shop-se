<?php
/*------------------------------------------------------------------------
# Featured Category Carousel Module for Opencart 1.5.1.x
# ------------------------------------------------------------------------
# Copyright (C) 2011 OpenCartSoft.com. All Rights Reserved.
# @license - Copyrighted Commercial Software
# Author: www.OpenCartSoft.com
# Websites:  http://www.opencartsoft.com -  Email: admin@opencartsoft.com
# This file may not be redistributed in whole or significant part.
-------------------------------------------------------------------------*/

// Heading 
$_['heading_title'] = 'เลือกสินค้าตามรุ่นมือถือที่ท่านใช้                                                              <a href="http://shop.se-update.com/index.php?route=information/information&information_id=15">[ดูทุกรุ่นคลิ๊กที่นี่] </a>';

// Text
$_['text_reviews']  = 'Based on %s reviews.'; 
?>