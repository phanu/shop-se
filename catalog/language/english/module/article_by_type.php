<?php
// Heading 
$_['text_featured_article'] = 'Featured Post';
$_['text_all_featured_article'] = 'Featured Post';
$_['text_recent_article'] = 'Recent Post';
$_['text_most_viewed'] = 'Most Viewed';

// Text
$_['text_comments']  = ' %s comments'; 
$_['text_viewed']  = ' %s viewed'; 
$_['text_date_added']  = 'Added'; 
$_['text_author']  = 'Author:'; 
$_['text_more']  = 'Details'; 
$_['text_related']  = 'Related Post:'; 
?>