<?php
// Text
$_['text_participate_win']         = 'Participate and you can win ... ';
$_['text_join_and']                = 'join and';
$_['text_win']                     = 'win';
$_['text_a_product']               = 'a product';
$_['text_discount']                = 'credits';
$_['text_if_you']                  = 'if you';
$_['text_like_share']              = 'like';
$_['text_the_product']             = 'the product';
$_['text_are_needed']              = 'still need';
$_['text_people']                  = 'people';
$_['text_x_people_needed']         = 'people are needed to unlock this promotion.';
$_['text_invite_friends']          = 'Invite your friends!';
$_['text_new_participants']        = 'Newest participants:';
$_['text_remaining_time']          = 'Remaining time';
$_['text_view_all']                = 'view all';
$_['text_deal_participants']       = 'Deal Participants';
$_['text_how_to_win']              = '<strong>How to win?</strong><br /><i>Click \'Participate\' button and like, share, invite your friends.<br /> To obtain more points click \'Participate\' button, invite more friends and to share how many times you want this offer.</i>';
$_['text_points_rules']            = '<strong>Points rules:</strong><br /><i>Like button - %s Deal points<br />Share - for each share you receive %s Deal points<br />Invite friends - for each friend invited receive %s Deal points.</i>';


// Counter
$_['text_days']                    = 'Days';
$_['text_hours']                   = 'Hours';
$_['text_minutes']                 = 'Minutes';
$_['text_seconds']                 = 'Seconds';
$_['text_day']                     = 'Day';
$_['text_hour']                    = 'Hour';
$_['text_minute']                  = 'Minute';
$_['text_second']                  = 'Second';

// Participate form
$_['text_like']                    = 'LIKE *';
$_['text_share']                   = 'SHARE';
$_['text_invite']                  = 'FB INVITE';
$_['text_mail']                    = 'MAIL';
$_['text_required']                = 'required - more info';
$_['text_optional']                = 'optional - more info';
$_['text_more_info']               = 'more info';
$_['text_participate_info']        = 'Share & Invite friends are optional, but can increase your chance to win.';
$_['text_rules_info']              = 'Like button - %s Deal points<br />Share - for each share you receive %s FB Deal points<br />Invite friends - for each friend invited receive %s FB Deal points';
$_['text_action_success']          = 'Action complete. For these deal you have %s FB deal points.';

// Button
$_['button_participate']           = 'participate';

?>