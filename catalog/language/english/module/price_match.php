<?php
//Heading 
$_['heading_price_match'] = 'Price Match - %s'; 

// Text
$_['text_price_match'] = 'Did you find somewhere same quality and cheaper? <a id="price-match-trigger">Let us to know</a>';
$_['text_success_price_match'] = 'Message sent to store owner. Thanks';

// Entry
$_['entry_price_match_customer_name']      = 'Name:';
$_['entry_price_match_customer_email']     = 'Email:';
$_['entry_price_match_customer_telephone'] = 'Telephone:';
$_['entry_price_match_competitor_link']    = 'Competitor link';
$_['entry_price_match_competitor_price']   = 'Competitor price';
$_['entry_price_match_message']            = 'Message';

// Button
$_['button_price_match_send_now'] = 'Send now';

// Error
$_['error_price_match_name']             = 'Name - Required';
$_['error_price_match_email']            = 'Email - Required';
$_['error_price_match_competitor_link']  = 'Competitor Link - Required';
$_['error_price_match_competitor_price'] = 'Competitor Price - incorrect value. Please check.';

?>