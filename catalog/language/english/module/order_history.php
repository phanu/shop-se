<?php

// Heading 
$_['heading_title']				= 'Order History';

// Text
$_['text_no_open_orders']		= 'No open orders!';
$_['text_no_pending_orders']	= 'No processing orders!';
$_['text_no_closed_orders']		= 'No completed orders!';

?>
