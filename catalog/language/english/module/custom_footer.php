<?php
$_['sent_success']        = 'Email has been sent. Thank you!';
$_['contact_inputholder'] = 'Your email';
$_['mail_subject']        = 'Email subject';
$_['order_date']          = 'Date:';
$_['order_total']         = 'Total: %s item (s)';

$_['button_sent'] = 'Send';

$_['error_invalid'] = 'Fields are required!';
?>