<?php
// Text
$_['text_home']             = 'Home';
$_['text_level']            = 'Level:';
$_['text_game_finished']    = 'Games Finished:';
$_['text_moves']            = 'Moves:';

// bar buttons
$_['text_instruction']      = 'Read help instructions';
$_['text_rank']             = 'See Rank table';

$_['text_login_required']   = 'You need to allow us to use your facebook info to create an account on our store.';
$_['button_ok_agree']       = 'OK, I agree';

// dialog help
$_['text_dtitle_help']      = 'Game Instructions';

$_['text_help_instructions'] = 'Click on a card to turn it over and reveal an image.<br /><br /> Then select a second card, trying to find the matching image.<br /><br /> If the images match, the cards are removed from the board.<br /> You win when all cards are removed. (do this with minimal number of moves - Number of moves are counted in top-right corner)<br /><br />Number of moves are converted in points.';
$_['text_win_type']          = 'You can win store credit every ';
$_['text_day']               = 'day';
$_['text_week']              = 'week';
$_['text_month']             = 'month';

//dialog rank
$_['text_dtitle_rank']      = 'Ranking';
$_['tab_today']             = 'Today';
$_['tab_week']              = 'This Week';
$_['tab_month']             = 'This Month';
$_['column_participant']    = 'Participant';
$_['column_points']         = 'Points';

// dialog like
$_['text_unlock_game']      = 'Like us to unlock the game,';
$_['text_play_win']         = 'play and win store credit';

$_['text_how_to_play']      = 'How to play?';
$_['text_play_instruction'] = 'Click on a card to turn it over and reveal an image.<br /> Then select a second card, trying to find the matching image.<br /> If the images match, the cards are removed from the board.<br /> You win when all cards are removed. (do this with minimal number of moves - Number of moves are counted in top-right corner)';

$_['text_prizes']           = 'You can win ...';
$_['text_prize_details']    = 'Try to collect more points that anyone.<br /><br /><strong>Prize details:</strong><br />';
$_['text_credits']          = 'credits <br />';
$_['text_daily_winner']     = 'Daily Winner : ';
$_['text_weekly_winner']    = 'Weekly Winner : ';
$_['text_monthly_winner']   = 'Monthly Winner : ';
$_['text_credit_transform'] = '<br />Your account credit will be automatically deducted from your next purchase.';


//dialog player won
$_['text_dtitle_win']       = 'CONGRATULATIONS !!!';
$_['button_play_again']     = 'Play again';
$_['text_player_won']       = 'Congratulations <strong>%s</strong> <br /><br />This game points: <strong>%s</strong><br />Today total points: <strong>%s</strong><br />This week total points: <strong>%s</strong><br />This month total points: <strong>%s</strong>';

$_['text_feed_message']     = 'Play this game and you can win store credits on %s. Until now my score for today is %s. You think you can beat my score?';
$_['text_feed_name']        = 'Play, have fun & win';
$_['text_feed_caption']     = 'a game from %s';
$_['text_feed_description'] = 'Play this game, collect as many points you can an you can win store credit that can be used to purchase products you like from our store. Have fun !!!'; 

?> 