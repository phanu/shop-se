<?php
// Heading  
$_['heading_title_token']   = 'Invalid token';
$_['heading_title_products']   = 'Cart not empty';
$_['heading_title_order_id']   = 'Unknown order';
$_['heading_title_one_page']   = 'Checkout';
$_['heading_title_customer_id']   = 'Privacy warning';

// Text
$_['text_continue']   = 'Continue';
$_['description_one_page']   = 'Our one page checkout';
$_['title_one_page']   = 'One page checkout';

// Column

// buttons
$_['button_clear_cart']   = 'Clear cart';
$_['button_cart']   = 'Continue to cart';

// Error
$_['error_token']   = 'Sorry, your token is invalid we cannot proccess this checkout.';
$_['error_products']   = 'You already have an active shopping cart with products';
$_['error_order_id']   = 'Sorry, unknown order we can not process this request.';
$_['error_customer_id']   = 'Sorry, this is not your order!.';

$_['text_new_subject']          = '%s - Order %s';
$_['text_new_order_detail']     = 'Order Details';
$_['text_new_instruction']      = 'Instructions';
$_['text_new_order_id']         = 'Order ID:';
$_['text_new_date_added']       = 'Date Added:';
$_['text_new_order_status']     = 'Order Status:';
$_['text_new_payment_method']   = 'Payment Method:';
$_['text_new_shipping_method']  = 'Shipping Method:';
$_['text_new_email']  		= 'Email:';
$_['text_new_telephone']  	= 'Telephone:';
$_['text_new_ip']  		= 'IP Address:';
$_['text_new_payment_address']  = 'Payment Address';
$_['text_new_shipping_address'] = 'Shipping Address';
$_['text_new_comment']          = 'The comments for your order are:';

// Entry
$_['entry_firstname']                = 'First Name:';
$_['entry_lastname']                 = 'Last Name:';
$_['entry_company']                  = 'Company:';
$_['entry_address']                  = 'Address:';
$_['entry_postcode']                 = 'Post Code:';
$_['entry_city']                     = 'City:';
$_['entry_country']                  = 'Country:';
$_['entry_zone']                     = 'Region / State:';
?>