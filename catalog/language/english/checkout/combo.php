<?php
//OpenCart Extension
//Project Name: OpenCart Combo/Bundle
//Author: Fanha Giang a.k.a fanha99
//Email (PayPal Account): fanha99@gmail.com
//License: Commercial
?>
<?php
// Text
$_['text_items']             = '%s item(s) - %s';

$_['text_success'] = 'Success: You have added %d product(s) to your <a href="%s">shopping cart</a>!';
$_['text_error'] = 'Warning: You have to select required option(s)!';

$_['error_required']         = '%s required!';	
$_['error_profile_required'] = 'Please select a payment profile!';
?>