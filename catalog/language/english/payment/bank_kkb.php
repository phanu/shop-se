<?php
// Text
$_['text_title']       = 'Kasikorn Bank Transfer';
$_['text_instruction'] = 'Kasikorn Bank Transfer Instructions';
$_['text_description'] = 'Please transfer the total amount to the following Kasikorn Bank account.';
$_['text_payment']     = 'Your order will not ship until we receive payment.';
?>