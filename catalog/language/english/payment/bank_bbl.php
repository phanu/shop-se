<?php
// Text
$_['text_title']       = 'Bangkok Bank Transfer';
$_['text_instruction'] = 'Bangkok Bank Transfer Instructions';
$_['text_description'] = 'Please transfer the total amount to the following Bangkok Bank account.';
$_['text_payment']     = 'Your order will not ship until we receive payment.';
?>