<?php
// Text
$_['text_title']       = 'Bank of Ayudhya Transfer';
$_['text_instruction'] = 'Bank of Ayudhya Transfer Instructions';
$_['text_description'] = 'Please transfer the total amount to the following Bank of Ayudhya account.';
$_['text_payment']     = 'Your order will not ship until we receive payment.';
?>