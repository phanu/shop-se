<?php
// Text
$_['text_title']    = 'PayPal';
$_['text_reason'] 	= 'REASON';
$_['text_testmode']	= 'Warning: The Payment Gateway is in \'Sandbox Mode\'. Your account will not be charged.';
$_['text_total']	= 'Shipping, Handling, Discounts & Taxes';
$_['text_payment']  = '<b>สำคัญมาก สำหรับการชำระผ่าน Paypal</b></br > หลังจากที่ท่านได้กดปุ่ม ยืนยันคำสั่งซื้อ แล้ว ระบบจะทำการเชื่อมต่อไปยังระบบของ Paypal ซึ่งในขั้นตอนการชำระของ Paypal ท่านจะต้องกรอก ชื่อผู้รับสินค้าและที่อยู่ในระบบ Paypal ให้ตรงกับในระบบของทางร้านที่ท่านได้กรอกไป  ไม่เช่นนั้นทางร้านจะไม่สามารถจัดส่งสินค้าให้ท่านได้ ';

?>