<?php
// Text
$_['text_title']       = 'Government Savings Bank Transfer';
$_['text_instruction'] = 'Government Savings Bank Transfer Instructions';
$_['text_description'] = 'Please transfer the total amount to the following Government Savings Bank account.';
$_['text_payment']     = 'Your order will not ship until we receive payment.';
?>