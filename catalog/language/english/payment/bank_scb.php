<?php
// Text
$_['text_title']       = 'Siam Commercial Bank Transfer';
$_['text_instruction'] = 'Siam Commercial Bank Transfer Instructions';
$_['text_description'] = 'Please transfer the total amount to the following Siam Commercial Bank account.';
$_['text_payment']     = 'Your order will not ship until we receive payment.';
?>