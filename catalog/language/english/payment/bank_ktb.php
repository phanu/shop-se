<?php
// Text
$_['text_title']       = 'Krungthai Bank Transfer';
$_['text_instruction'] = 'Krungthai Bank Transfer Instructions';
$_['text_description'] = 'Please transfer the total amount to the following Krungthai Bank account.';
$_['text_payment']     = 'Your order will not ship until we receive payment.';
?>