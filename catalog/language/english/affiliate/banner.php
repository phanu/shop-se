<?php
// Heading 
$_['heading_title']                 = 'Create your banner';
$_['heading_title_edit']            = 'Edit banner';
$_['heading_title_code']            = 'Banner Code';
$_['text_getbanner']                = 'Create banner';
$_['text_edit_banner']              = 'Edit banner options';
$_['text_account']                  = 'Account';
$_['button_continue']               = 'Continue';
$_['heading_title_option']          = 'Banner Type';
$_['select_banner_type']            = '- Select a Banner Type -';
$_['most_recent_products']          = 'Most Recent Products';
$_['featured_products']             = 'Most Recent Featured Products';
$_['recent_special']                = 'Most Recent Special Offers';
$_['random_featured']               = 'Random Featured Products';
$_['random_special']                = 'Random Special Offers';  
$_['random']                        = 'Random';  
$_['all_products']                  = 'All Products';
$_['text_select_all']               = 'Select All';  
$_['text_unselect_all']             = 'Unselect All';

$_['heading_categories']            = 'Select Categories';
$_['without_category']              = 'Without Category';    
$_['heading_manufacturer']          = 'Select Manufacturers'; 
$_['without_manufacturer']          = 'Without Manufacturer';    
$_['layout_header']                 = 'Select a Layout';
$_['select_a_layout']               = '- Select a Layout -';
$_['sl_leaderboard']                = 'Leaderboard (798 x 102)';
$_['sl_banner']                     = 'Banner (468 x 98)';
$_['sl_half_banner']                = 'Half Banner (234 x 98)';
$_['sl_skyscraper']                 = 'Skyscraper (120 x 600)';
$_['sl_wide_skyscraper']            = 'Wide Skyscraper (160 x 600)';
$_['sl_small_rectangle']            = 'Small Rectangle (180 x 150)';
$_['sl_vertical_banner']            = 'Vertical Banner (120 x 240)';
$_['sl_small_square']               = 'Small Square (200 x 200)';
$_['sl_square']                     = 'Square (250 x 250)';
$_['sl_medium_rectangle']           = 'Medium Rectangle (300 x 250)';
$_['sl_large_rectangle']            = 'Large Rectangle (336 x 280)';
$_['sl_preview_banner']             = 'Preview Widget Layouts'; 
$_['preview_txt']                   = 'Preview'; 
$_['banner_language']               = 'Select Language of banner';
$_['banner_language_select']        = ' - Select a language - ';
$_['cs_border']                     = 'Border';
$_['cs_title']                      = 'Title';
$_['cs_title_background']           = 'Title Background';
$_['cs_background']                 = 'Background';
$_['cs_text']                       = 'Text';
$_['cs_link']                       = 'Link';
$_['cs_link_hover']                 = 'Link (mouse hover)';
$_['cs_link_bottom']                = 'Bottom Link';         
$_['cs_footer_background']          = 'Footer Background'; 
$_['cs_link_footer']                = 'Footer Link';
$_['banner_title']                  = 'Banner Title';
$_['banner_domain_name']            = 'Banner Website Domain Name';

$_['get_code_txt']                  = 'Get code';
$_['get_code_txt_edit']             = 'Save and preview';  
$_['back_to_options']               = 'Back to banner options';
$_['banners_list_txt']              = 'Your banners list';
$_['create_new_text']               = 'Create another banner';
$_['use_txt']                       = 'Copy this code and paste into the code of the page where you are going to have your banner.';
$_['banner_code_txt']               = 'Banner Code';
$_['banner_htmlcode_txt']           = 'HTML Code for banner';

$_['text_bannerlist']               = 'Banners List';
$_['edit_txt']                      = 'Edit';
$_['delete_txt']                    = 'Delete';
$_['action_txt']                    = 'Action';
$_['delete_quest']                  = 'Are you really want to delete?';
$_['list_empty']                    = 'You do not have any banner yet.';
$_['txt_calculator']                = 'Earnings calculator';
$_['txt_statistics_for']            = 'Statistics for banner';

$_['txt_commission_for']            = 'Commission for banner';
$_['txt_will_be_shown']             = 'Products will be shown:';
$_['txt_how_much_1']                = 'How much you will earn if visitors of your site will purchase each product one time:';
$_['txt_how_much_2']                = 'How much you will earn if visitors of your site will purchase each product 2 times:';
$_['txt_how_much_3']                = 'How much you will earn if visitors of your site will purchase each product 3 times:';
$_['txt_how_much_5']                = 'How much you will earn if visitors of your site will purchase each product 5 times:';
$_['txt_how_much_10']                = 'How much you will earn if visitors of your site will purchase each product 10 times:';
// Error
$_['error_type']                    = 'Banner Type is required!';
$_['error_categories']              = 'Select at least one category!';
$_['error_manufacturers']           = 'Select at least one manufacturer!';
$_['error_layout']                  = 'Banner Layout is required!';
$_['error_language']                = 'Banner Language is required!';
$_['error_color']                   = 'Please enter valid collor!';
$_['error_banner_title']            = 'Banner Title must be between 1 and 200 characters!';
$_['error_domain_name']             = 'Please enter valid domain name!';

$_['num_txt']                       = 'Num';
$_['id_txt']                        = 'ID';
$_['txt_layout']                    = 'Layout';
$_['txt_how_many']                  = 'Products will be shown';
$_['txt_statistics']                = 'Statistics';

$_['txt_showcount']                 = 'How many times the banner was displayed?';
$_['txt_transfer']                  = 'How many was transitions to a site from banner?';
$_['txt_productswasshow']           = 'How many products have been shown?';
$_['txt_productspurchased']         = 'How many products were purchased?';

$_['txt_last_day']                  = 'Last Day';
$_['txt_7_day']                     = '7 days';
$_['txt_30_days']                   = '30 days';
$_['txt_1_year']                    = '1 Year';
?>