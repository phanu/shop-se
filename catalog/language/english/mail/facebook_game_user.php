<?php
// Text
$_['text_subject']    = '%s - Thank you for playing our game';
$_['text_welcome']    = 'Welcome and thank you for playing our game!';
$_['text_login']      = 'An account has been created. Playing this game you can win store credit (asociated with this account) that will allow you to purchase products from our store at super prices.';
$_['text_approval']   = 'Your account must be approved before you can login. Once approved you can log in by using your email address and password by visiting our website or at the following URL:';
$_['text_login_url']  = 'Login URL: %s'; 
$_['text_login_data'] = 'You can login using email: %s and password: %s'; 


$_['text_transaction_received'] = 'You have received %s credit!';
$_['text_transaction_total']    = 'Your total amount of credit is now %s.' . "\n\n" . 'Your account credit will be automatically deducted from your next purchase.';

$_['text_thanks']     = 'Thanks,';
?>