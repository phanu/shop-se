<?php
// Text
$_['text_subject']  = 'New article are awaiting your approval!';
$_['text_dear']  = 'Dear ... ';
$_['text_author']  = '%s just posted a new article!';
$_['text_title']    = 'Article Title: %s';
$_['text_thanks']   = 'Thanks';
?>