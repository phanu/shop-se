<?php
// Text
$_['text_subject']  = '%s - Thank you for registering';
$_['text_welcome']  = 'Welcome and thank you for registering at %s!';
$_['text_login']    = 'Your Author account has now been created and you can log in by using your email address and password by visiting our website or at the following URL:';
$_['text_approval'] = 'Your Author account must be approved before you can login. Once approved, we will send a notification email to you!';
$_['text_services'] = 'Upon logging in, you can start posting your articles.';
$_['text_thanks']   = 'Thanks,';

$_['text_admin_subject']  = '%s - New customer registering an author!';
$_['text_dear']  = 'Dear administrator, customer %s register become an author! Detailts:';
$_['text_display_name']  = 'Display Name: %s';
$_['text_first_name']  = 'First Name: %s';
$_['text_last_name']  = 'Last Name: %s';
$_['text_email']  = 'Email: %s';
?>