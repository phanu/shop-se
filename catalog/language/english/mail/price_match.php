<?php
// Text
$_['text_subject']            = 'Price Match - %s';
$_['text_hi']                 = 'Hi,';
$_['text_inform']             = '%s has informed you that a competitor has a better price for product %s. See details below:';
$_['text_customer_name']      = 'Customer Name: %s';
$_['text_customer_email']     = 'Customer Email: %s';
$_['text_customer_telephone'] = 'Customer Telephone: %s';
$_['text_competitor_link']    = 'Competitor link: %s';
$_['text_choosen_currency']   = 'Choosen Currency: %s';
$_['text_competitor_price']   = 'Competitor price: %s';
$_['text_your_price']         = 'Your price (seen by customer): %s';
$_['text_comment']            = 'Comment: %s';

?>