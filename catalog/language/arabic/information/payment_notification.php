<?php
// Heading
$_['heading_title']     = 'تبليغ الدفع';

// Text
$_['text_success']      = 'نجاح: لقد قمت بتعديل المعلومات!';
$_['text_default']      = 'الافتراضي';
$_['text_choose']= 'الرجاء اختيار';
$_['text_saudi_investment_bank']= 'البنك السعودي للاستثمار';
$_['text_al_rajhi_bank']= 'بنك الراجحي';
$_['text_alahli_bank']= 'البنك الاهلي';
$_['text_bank_samba']= 'سامبا';
$_['text_banque_saudi_fransi']= 'البنك السعودي الفرنسي';
$_['text_saudi_british_bank']= 'بنك ساب';
$_['text_saudi_hollandi_bank']= 'البنك السعودي الهولندي';
$_['text_al_bilad_bank']= 'بنك البلاد';
$_['text_payment_slip']= '<br>(File format: .doc, .docx, .pdf, .gif, .jpg, .png, .bmp)<br>(Maximum upload file size: 1MB)';
$_['text_message']   = '<p>Submitted to the customer care service department and you will be answered soon.Thank you</p>';
$_['text_name']     = 'Name:';
$_['text_phone']    = 'Phone:';
$_['text_order_id']  = 'Order ID:';
$_['text_amount_paid']  = 'Amount Paid:';
$_['text_payment_date']  = 'Payment Date:';
$_['text_payment_method']  = 'Payment Method:';
$_['text_payment_transaction']  = 'Payment Transaction Details:';
// Entry Fields
$_['entry_name']     = 'اسم:';
$_['entry_email']    = 'عنوان البريد الإلكتروني:';
$_['entry_phone']    = 'رقم الهاتف:';
$_['entry_order_id']  = 'Order ID:';
$_['entry_amount_paid']  = 'Amount Paid:';
$_['entry_payment_date']  = 'Payment Date:';
$_['entry_payment_method']  = 'Payment Method:';
$_['entry_payment_transaction']  = 'Payment Transaction Details:';
$_['entry_payment_slip']  = 'Upload Payment Slip:';
$_['entry_captcha']  = 'أدخل الرمز في المربع أدناه:';

// Email
$_['email_subject']  = 'اPayment Notification %s';

// Errors
$_['error_name']     = 'يجب أن يكون الاسم بين 3 و 32 حرفا!';
$_['error_email']    = 'لا يظهر عنوان البريد الإلكتروني لتكون صالحة!';
$_['error_order_id']  = 'Please enter your Order ID!';
$_['error_amount_paid']  = 'Please enter the total amount paid!';
$_['error_payment_date']  = 'Please enter date of payment!';
$_['error_payment_method']  = ' Please select payment method!';
$_['error_payment_transaction']  = 'Please enter payment transaction details!';
$_['error_captcha']  = 'رمز التحقق لا يتطابق مع الصورة!';

?>
