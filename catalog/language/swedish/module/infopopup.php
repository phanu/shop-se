<?php
// Texts
$_['text_country']    = 'Land';
$_['text_language']   = 'Språk';
$_['text_currency']   = 'Valuta';

// Buttons
$_['button_accept']   = 'Acceptera';
$_['button_decline']  = 'Neka';
$_['button_ok']  	  = 'Ok';
?>