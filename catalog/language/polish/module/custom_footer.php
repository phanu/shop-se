<?php
$_['sent_success']        = 'Email został wysłany. Dziękujemy!';
$_['contact_inputholder'] = 'Twój email';
$_['mail_subject']        = 'Tytuł emaila';
$_['order_date']          = 'Data:';
$_['order_total']         = 'Suma: %s produkt (ów)';

$_['button_sent'] = 'Wyślij';

$_['error_invalid'] = 'Wszystkie pola są wymagane!';
?>