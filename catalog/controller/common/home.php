<?php  
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));

		$this->data['heading_title'] = $this->config->get('config_title');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/home.tpl';
		} else {
			$this->template = 'default/template/common/home.tpl';
		}
		
		$this->children = array(
			/* google captcha*/
			'captcha/google_captcha',
			/* google captcha*/
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_top_se_mobile',
			'common/content_bottom_se_mobile',
			'common/content_bottom',
			'common/footer',
			'common/footer_mobile',
			'common/header',
			'common/header_mobile'
		);


		$this->data['category_mobile'] = $module = $this->getChild('module/category_mobile');

		$this->response->setOutput($this->render());
	}
}
?>